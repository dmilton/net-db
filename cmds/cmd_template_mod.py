#
# Version information for this command.
#

g_version='dd mmm yyyy - 1.0.0'

# 15 Aug 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print( """
usage: %s --help | --version
usage: %s [--flag] [--opt=x] pos-arg
usage: %s [--flag] [--opt=x] --update pos-arg key/key-value
usage: %s [--flag] [--opt=x] --delete pos-arg
usage: %s --action [--flag] [--opt=x]
    --help              display this help message
    --version           show version information and exit.
    --action            perform a command specific action.
    --flag              a command line flag (boolean)
    --opt=x             a command line option with value
    pos-arg             a positional command line argument.
    """ % (gCommand, gCommand, gCommand, gCommand, gCommand) )
    return(exit_code)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"] # [ "netdisco", "intermapper"]
    # Change to look for "viewing" commands.
    result['dbUserClass'] = "user"  # look

    # this command wants add, update, delete, and keys actions?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options:
    # Cannot use a/add, u/update, d/delete, and k/keys if setArgs is true.
    # If you don't want a coresponding short option then use ""
    result['optionList'] = [ ("f", "flag"), ("o:", "opt="),
            ("a", "action") ]

    # For commands that do not implement the "add, update, delete, keys"
    # commands, what are the required positional arguments.
    #result['requiredArgs'] = []
    #result['minArgs'] = len(result['requiredArgs'])

    # For commands that implement the "add, update, delete, keys"
    # commands, what are the required positional arguments for each.
    # The add requires two arguments or the entry is pretty much useless.
    # list of table columns that are mandatory.
    result['addRequired'] = ["reqd_pos_arg"]
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires the table column that is the primary key and
    # one key/value pair to update.
    result['updateRequired'] = ["reqd_pos_arg"]
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete uses the room_id field.
    result['delRequired'] = ["reqd_pos_arg"]
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmd_line_info - all command line flags and options processed thus far.
#    arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmd_line_info, arg_list):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # # #
    # For commands that implement add, update, delete, and keys:
    #

    # initialize tables and set input keys.
    try:
        building_info.initialize()
        result['inputKeys'] = building_info.get_building_fields()
    except db_access.DbException as db_error:
        fatal_error()
        sys.exit(-3)

    if ( cmd_line_info['action'] in ["update", "delete"] ):
        # Delete requires one argument - the reqd_pos_arg
        # Update requires reqd_pos_arg plus something to update.
        result['reqd_pos_arg'] = arg_list[0]
        result['argsUsed'] = 1
    elif ( cmd_line_info['action'] == "add" ):
        # Add requires reqd_pos_arg and whatever else is needed
        # to form a complete row in the table.
        result['reqd_pos_arg'] = arg_list[0]
        result['argsUsed'] = 2

    # # #
    # For commands that do not implement add, update, delete, and keys.
    #
    #result['reqd_pos_arg'] = arg_list[0]
    #result['argsUsed'] = 1
    # And the rest of the positional arguments. Could let the
    # key/key-value processor handle them if this is table data.
    #result['pos_arg_list'] = []
    #for argNbr in list(range(1,len(arg_list))):
    #    result['pos_arg_list'].append(arg_list[argNbr])
    #    result['argsUsed'] += 1

    return(result)
## end process_positional()

#
##############################################################################
# process_option
# This routine must process the command specific options one at a time
# and may return anything in default_options such a change in action.

def process_option(option, argument):

    # Optional: If there are no command line options which change
    # the default behaviour (action) or alter the arguments required
    # for operation then this function can be removed.

    result = {}

    # If a flag makes a database required (or not) define this key.
    #result['late_db_open'] = True

    # Handle options that change the action.
    if ( option in ["--action","-a"] ):
        result[option] = True
        result['action'] = 'new-action'
        result['requiredArgs'] = []
        result['minArgs'] = 0
    elif ( option in ["--opt","-o"] ):
        result[option] = argument
    else:
        # Fail here, caller handles these options.
        raise ValueError(option + " is not recognized")

    return(result)
## end process_option()

##############################################################################
# verify_options
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.
# It is not required for a command that implements add, delete, keys, update.
# Entry:
#       commandLineInfo - the processed command line as generated by
#       process_command_line, default_options and process_positional.
# Exit:
#       none.
# Exceptions:
#       raise a ValueError exception if there is something wrong with
#       the values of the data provided on the command line. Raise a
#       CommandError if there is something wrong with the combination
#       of options/arguments provided.
#

def verify_options(cmd_line_info):

    # Optional: If there are no command line options which conflict
    # with each other then you can remove this routine.

    # Check theOptions to see if there are some which are options
    # which are mutually exclusive. 
    # Verify the values received are within bounds.
    # Set default values based on the command line received so far.
    # raise ValueError("invalid input for x")
    # raise CommandError("option X cannot be used with option Y")

    return(cmd_line_info)
## end verify_options()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # # # # # # # # # # # # # # # # # # # #
    # For show commands, initialize tables.
    #try:
    #    building_info.initialize()
    #except:
    #    print("Unable to initialize dababase tables; Check with your DBA.")
    #    sys.exit(-3)

    # Do your main processing here.
    print("This command does nothing but process command line arguments.")
    print("The processed command line information is:")
    print("The action requested is: " + cmd_line_info['action'])
    print("Command line options are:")
    print(str(cmd_line_info))
    print("The user defaults are:")
    print(str(user_defaults))

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

