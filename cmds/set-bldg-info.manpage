.\"
.\"	set-bldg-info.l,v 2.0 2010/04/08 15:59:23 milton
.\"
.TH set-bldg-info "March 26, 2014" "3.0.2" "Update Building Information
.UC 6
.SH NAME
set-bldg-info Add, update or change building information in the database.
.SH SYNOPSIS
.B set-bldg-info [--help | --version | --keys]
.P
.B set-bldg-info bldg-code bldg-name 'Building Name' [key key-value]...
.P
.B set-bldg-info --update bldg-code key key-value [key key-value]...
.P
.B set-bldg-info --delete bldg-code
.P

.SH DESCRIPTION
The set-bldg-info command allows for the creation or change of information
regarding buildings in the networking database. The default behaviour is to
add new building.

.SH OPTIONS
.TP 8 8
.BI \--help
Display a summary of help/usage information.
.TP 8 8
.BI \--version
List the revision information of this command.
.TP 8 8
.BI \--keys
List the input keys and the value type (plus default if defined) for each
field in the building table.
.TP 8 8
.BI \--update
The information provided is to update an existing building. The default
is to add a new building and generate an error if the building code already
exists in the table.
.TP 8 8
.BI --delete
Not yet implemented, but eventually you will be able to delete a building
from the database. Much cleanup is required since there are switches,
networks to delete or free, wireless network installations, sensors, etc.
that need to be altered and the rules for each is different.

.PP
.SH KEYS AND VALUES
All string values containing non alphabetic characters must be enclosed
in quotes. Where a maximum length is indicated, longer strings will be
truncated to the maximum length.
Boolean values are not case sensitive and can be represented as True, T, true,
t, False, F, false, or f.

.TP 8 8
.BI bldg-code
The code assigned to the building. Devices in the building will use this
value as a prefix for their names. Values are case sensitive and a maximum
length of 8 characters and must be unique.

.TP 8 8
.BI bldg-name
The name of the building. A maximum of 64 characters may be used. The
value must be unique.


.TP 8 8
.BI campus-id
The campus code for this building. 1 = Fort Garry, 2 = Bannatyne, etc.
See show-campus-info for the complete list.

.TP 8 8
.BI core-bldg
Boolean flag indicating if this building code represents a core router.

.TP 8 8
.BI bldg-router
The name of the router for the building.

.TP 8 8
.BI switch-ct
Count of desktop switches installed in the building. Using this value
one can determine how many desktop ports are available for use, aiding
in the assignment of IPv4 network address spaces.

.TP 8 8
.BI net-zone
The network zone which should be used for assigning networks to the building.

.TP 8 8
.BI next-user-vlan
The VLAN tag to use for assignment of the next user VLAN in the building.

.TP 8 8
.BI next-prot-vlan
The VLAN tag to use for assignment of the next protected VLAN in the building.

.TP 8 8
.BI ipv6-netid
The network ID (see main documentation for the network mask exhaustion
algorithm) assigned to this building. The default value is -1 and indicates
that an IPv6 network id has not yet been assigned.

.TP 8 8
.BI ipv6-nextnet
The id of the next IPv6 network to assign to a vlan in the building. The
default value is -1 with the first network assigned to the building being
zero.

.TP 8 8
.BI ospf-area
The OSPF area ID the building is a part of. The value can be any positive
integer.

.TP 8 8
.BI vtp-domain
For Cisco based building networks, this is the VTP domain of the building.
Value can be any string to a maximum of 16 characters.

.TP 8 8
.BI web-visible
Boolean value indicating if this building information should appear on the
networking web page.

.TP 8 8
.BI web-dir
The netmon URL path for building information.
Value can be any string to a maximum of 128 characters.

.TP 8 8
.BI map-code
The InterMapper Map code for this building. Used to extract current outages
for the building in the networking web pages. Value is a string to a maximum
of 64 characters.

.TP 8 8
.BI vd-room
Add a Voice/Data room to the building. The value consists of two parts:
the room number (a string of N characters) and a flag to indicate if this
is the MDC or not. The flag can be True or False.

.PP
.SH EXAMPLES
To add a new building "Straw Bale Building" to the database:
.P
.B set-bldg-info --add sb bldg-name """Straw Bale Building - Alternative Village""
.P
This adds a new building "Straw Bale Building" to the database with the
building code of sb.
.P
Set the campus for the straw bale building to 1 (Fort Garry)
.P
.B set-bldg-info sb campus 1
.P
Set the network web directory for the Straw Bale building.
.P
.B set-bldg-info sb webdir /network/straw-bale web-visible true

As many key and key-value pairs as desired can be used on the command line.
Use the --keys command line option to show the keys currently defined.

.SH DIAGNOSTIC OUTPUT
.PP
If the input is invalid an error indicating which key/key-value pair was
rejected.

