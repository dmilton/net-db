# Version information for this command.

g_version='22 Jan 2015 -   3.0.4'

# 22 Jan 2015   Updates weren't working properly because the data passed
#               to update didn't contain enough information. This actually
#               caused nothing to be done on update.
# 26 Mar 2014   Cleanup formatting in Usage.
# 28 Nov 2013   Add some output after the add/update.
# 11 Oct 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)


#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s [--help | --version | --keys]
usage: %s [--mdc] bldg-code room-nbr
usage: %s --update room-id key key-value
usage: %s --delete room-id
    --help              display this help message
    --version           show version information and exit.
    --keys              display the update keys for the command.
    --mdc               this room is the building MDC.
    bldg-code           the building code for this room.
    room-nbr            the room number for this room.
    """ % (gCommand, gCommand, gCommand, gCommand))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options:
    result['optionList'] = [("m", "mdc")]

    # There are four actions available - keys, add, update, and delete.
    # The add requires two arguments or the entry is pretty much useless.
    result['addRequired'] = ['bldg_code', 'room']
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires room id and one key/value pair to update.
    result['updateRequired'] = ['room_id']
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete uses the room_id field.
    result['delRequired'] = ['room_id']
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdInfo, argList):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize tables and set input keys.
    building_info.initialize()
    result['inputKeys'] = building_info.get_vdroom_fields()

    if ( cmdInfo['action'] in ["update", "delete"] ):
        # Delete requires one argument - the room_id.
        result['room_id'] = int(argList[0])
        result['argsUsed'] += 1
    elif ( cmdInfo['action'] == "add" ):
        # Add requires bldg_code and room
        result['bldg_code'] = argList[0]
        result['room'] = argList[1]
        result['argsUsed'] += 2

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Now handle what the user gave us on the command line.
    if ( cmdLineInfo['action'] == "add" ):
        # Try and get the building code. If it doesn't exist then
        # we received an invalid building code on the command line.
        bldgInfo = building_info.get_building_info(cmdLineInfo['bldg_code'])
        # Try and get the list of voice/data rooms for the building.
        bldgRoomList = building_info.get_vd_rooms(cmdLineInfo['bldg_code'])
        # Check that the room isn't a duplicate.
        for aRoom in bldgRoomList:
            if ( aRoom['room'] == cmdLineInfo['room'] ):
                raise ValueError("This room already exists.")
        # room being added is not a duplicate so add it.
        roomInfo = building_info.add_vd_room(cmdLineInfo)
    elif ( cmdLineInfo['action'] == "update" ):
        # update a row in the db.
        # This is an existing room so the building code should be good.
        # MDC is a bit of a problem as we have no reliable way to know
        # if it was passed as a flag (--mdc) or as mdc true/false. This
        # means we must always have mdc on the command line for updates.
        roomInfo = {}
        roomInfo['room_id'] = int(cmdLineInfo['room_id'])
        if ( "room" in cmdLineInfo ):
            roomInfo['room'] = cmdLineInfo['room']
        if ( "mdc" in cmdLineInfo ):
            roomInfo['mdc'] = cmdLineInfo['mdc']
        elif ( cmdLineInfo['--mdc'] ):
            roomInfo['mdc'] = 'T'
        roomInfo = building_info.update_vd_room(roomInfo)
    elif ( cmdLineInfo['action'] == "delete" ):
        # delete a row from the db.
        building_info.delete_vd_room(cmdLineInfo)

    if ( cmdLineInfo['action'] in ["add","update"] ):
        print(("Room ID: " + str(roomInfo['room_id'])))
        print(("Building: " + roomInfo['bldg_code']))
        theRoom = "Room Nbr: " + roomInfo['room']
        if ( roomInfo['mdc'] ):
            theRoom += " (MDC)"
        print(theRoom)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

