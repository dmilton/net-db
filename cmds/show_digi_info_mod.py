# Version information for this command.

g_version='19 Dec 2013 - 3.0.0'

# 27 Nov 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:

    import building_info
    import console_info

except ImportError as msg:

    print("The NetTrackDB utilities are not properly installed.")
    print(msg)
    sys.exit(-1)

except Exception as exc:

    fatal_error()
    sys.exit(-2)


#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s bldg-code
usage: %s --mac mac-address
usage: %s --host host-name
    --help        display this help message
    --version        show version information and exit.
    --mac        find DIGI by MAC address.
    --host        find DIGI by host name.
    --serial        list serial port info for the digi.
    --parse        output suitable for scripts to process.
    bldg-code        find all DIGIs by building code.
    """ % ( gCommand, gCommand, gCommand, gCommand )))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# command_options()
# This routine returns a dictionary containing two lists.
# The lists are used to build the strings required for command line
# processing by the main wrapper.

def command_options():

    result = {}
    result['short'] = "mhps"
    result['long'] = ["mac", "host", "parse", "serial"]
    result['minArgs'] = 1

    return(result)
## end command_options()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['action'] = 'list'
    result['findMAC'] = False
    result['findHost'] = False
    result['listSerial'] = False
    result['parseOutput'] = False

    return(result)
## end default_options()

#
##############################################################################
# ProcessOption
# This routine must process the command specific options one at a time
# and return any dictionary values which Main will use for processing.

def process_option(option, argument):

    result = {}

    if option in ("-m", "--mac"):
        result['findMAC'] = True
    elif option in ("-h", "--host"):
        result['findHost'] = True
    elif option in ("-p", "--parse"):
        result['parseOutput'] = True
    elif option in ("-s", "--serial"):
        result['listSerial'] = True
    else:
        raise ValueError("unhandled option:" + str(option))

    return(result)
## end process_option()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    result['findString'] = argList[0]

    # If there is a problem with an argument:
    # raise ValueError, ("kind of value error")

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    if ( theOptions['findMAC'] and theOptions['findHost'] ):
        raise ValueError("--mac and --host options are mutually exclusive.")

    return
## end verify_options()

#
##############################################################################
# Main

def main(commandInfo):

    # initialize any tables here.
    building_info.initialize()
    console_info.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    if ( commandInfo['findMAC'] == False and commandInfo['findHost'] == False ):
        bldgCode = commandInfo['findString']
        digiInfo = console_info.FindDigis(commandInfo['findString'])
    else:
        if ( commandInfo['findMAC'] ):
            macAddr = cmd_line_input.check_mac_addr(commandInfo['findString'])
            digiInfo = console_info.FindDigiMac(macAddr)
        elif ( commandInfo['findHost'] ):
            hostInfo = cmd_line_input.check_host_name(commandInfo['findString'])
            digiInfo = console_info.FindDigiName(hostInfo[2][0])

        if ( len(digiInfo) > 0 ):
            bldgCode = digiInfo[0]['bldg_code']

    bldgInfo = building_info.get_building_info(bldgCode)
    if ( commandInfo['parseOutput'] ):
        for aDigi in digiInfo:
            if ( commandInfo['listSerial'] ):
                portInfo = console_info.FindDigiPorts(aDigi['digi_code'])
                for aPort in portInfo:
                    parseLine = aPort['digi_code'] + '\t'
                    parseLine += str(aPort['port_nbr']) + '\t'
                    parseLine += aPort['device'] + '\t'
                    parseLine += aPort['console_name'] + '\t'
                    parseLine += aPort['console_type'] + '\t'
                    parseLine += str(aPort['active'])
                    print(parseLine)
            else:
                parseLine = aDigi['bldg_code'] + '\t'
                parseLine += aDigi['digi_code'] + '\t'
                parseLine += str(aDigi['digi_ip']) + '\t'
                parseLine += aDigi['digi_type'] + '\t'
                parseLine += aDigi['room'] + " " + bldgInfo['name']
                print(parseLine)
    else:
        print(("Building (" + bldgCode + "):" + bldgInfo['name']))
        if ( len(digiInfo) == 0 ):
            print("No DIGIs found in this building.")
        else:
            if ( commandInfo['listSerial'] ):
                for aDigi in digiInfo:
                    #digiRoomInfo = building_info.get_room_info(
                    #                               aDigi['room_id'])
                    #digiRoom = digiRoomInfo['room']
                    print(("serial ports for digi code: " \
                        + aDigi['digi_code'] + " # " + str(aDigi['digi_nbr'])\
                        + " in room " + aDigi['room']))
                    portInfo = \
                        console_info.FindDigiPorts(aDigi['digi_code'], active=True)
                    console_info.PrintDigiPorts(portInfo)
                    #console_info.ConserverConfig(bldgInfo, aDigi, portInfo)
            else:
                console_info.PrintDigi(digiInfo)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
