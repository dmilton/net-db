# Version information for this command.

g_version="dd mmm yyyy - 1.0.0"

# 17 Jun 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#\
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s [campus-id]
    --help          display this help message
    --version       show version information and exit.
    campus-id       check a particular campus for integrety.
    """ % ( gCommand, gCommand) ))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# command_options()
# This routine returns a dictionary containing two lists.
# The lists are used to build the strings required for command line
# processing by the main wrapper.

def command_options():

    result = {}
    result['short'] = ""
    result['long'] = []
    result['minArgs'] = 0

    return(result)
## end command_options()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking", "netdisco"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['action'] = 'check'
    result['campus'] = -1

    return(result)
## end default_options()

#
##############################################################################
# ProcessOption
# This routine must process the command specific options one at a time
# and return any dictionary values which Main will use for processing.

def process_option(option, argument):

    result = {}

    raise ValueError("unhandled option:" + str(option))

    return(result)
## end process_option()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    if ( len(argList) == 1 ):
        result['campus'] = int(argList[0])

    # If there is a problem with an argument:
    # raise ValueError("what is the error?")

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError("invalid option combination description")

    return
## end verify_options()

#
##############################################################################
# Main

def main(commandInfo):

    # initialize any tables here.
    building_info.initialize()
    network_info.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Get the list of network types so we can exclude anything
    # that is not family IP.
    netTypeDetail = network_types.get_type_detail(0)

    # get the list of campus locations
    campusList = building_info.get_campus_info(commandInfo['campus'], False)

    # For each campus location, check the buildings.
    for aCampus in campusList:
        print("Processing buildings on campus: " + aCampus['location'])
        buildingList = building_info.get_campus_buildings(aCampus['campus_id'])
        # For each building, verify the networks defined.
        for aBuilding in buildingList:
            print("Processing building: " + aBuilding['bldg_name'])
            # Get the list of networks in the building.
            try:
                networkList = network_info.get_networks(aBuilding['bldg_code'])
            except NetException as theErr:
                if ( theErr.name == "NoNetworksDefined" ):
                    print("No networks in the building.")
                else:
                    raise
            except:
                raise
            # Check each network.
            for aNetwork in networkList:
                if ( aNetwork['net_type'] in netTypeDetail['family_ip'] ):
                    # Does the network contain another one?
                    netTest1 = network_info.get_network_info(
                        "cidrContains", aNetwork['network'])
                    # Is the network contained by another one?
                    res1 = compare_net(aNetwork, netTest1)
                    netTest2 = network_info.get_network_info(
                        "cidrContainedBy", aNetwork['network'])
                    res2 = compare_net(aNetwork, netTest2)


## end main()

#
##############################################################################
# Local function definitions...

def compare_net(net1, net2):
    if ( "network" in net1 ):
        if ( "network" in net2 ):
            if ( net1['network'] > net2['network'] ):
                result = 1
            elif ( net1['network'] < net2['network'] ):
                result = -1
            else:
                result = 0
        else:
            result = 1
    else:
        result = -1

    if ( result != 0 ):
        print("Network " + str(net1['network']) +
            " allocated to " + net1['bldg_code'] +
            " contains/contained by " + str(net2['network']) +
            " allocated to " + net2['bldg_code'])

    return(result)
## end compare_net()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
