#
# Version information for this command.

g_version='11 Dec 2014 - 3.0.5'

# Merge smaller IPv4 networks to form one larger network.

# 10 Dec 2014   set_prefix_len always returs a single network so updated
#               code so that is the result rather than a list.
# 20 Aug 2014   Integrate changes to the module code from mainline.
#               Change status messages to the user. Preserve the net-zone
#               when the network is free.
# 15 Jul 2014   Correct column name passed to print_table
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 13 May 2014   Qualify the NetException handling.
# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
#  5 Nov 2013   Update warning message for improved clairity.
# 23 Sep 2013   Add code to conditionally debug after init done.
#  7 Jun 2013   free_network now needs the type information as argument.
# 13 May 2013   get_type_info now returs a list, in this instance that
#               is a list of one entry. Need just the record itself so
#               added [0] to fix this. Cleanup usage output.
#  1 May 2013   Add some instructions and comments on what may or
#               did take place with the merge.
#  4 Feb 2013   Rewrite for new database schema.
#  2 Feb 2012   Changed vlan to vlan_tag to reflect db changes.
# 16 Aug 2010   Add VerifyOptions function.
# 19 Apr 2010   Finished writing. Changed default behaviour to only
#               show what the command will do rather than actually
#               performing the work.
# 16 Apr 2010   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--merge] [--in-place] net-cidr
    --help          display this help message
    --version       show version information and exit.
    --merge         merge all networks within net-cidr into one network.
                    The default behaviour is to show what the command will
                    do. When merged, the new larger address space is marked
                    as free unless --in-place is used.
    --in-place      take all networks and merge them into the first
                    network in the list. The only thing that is changed in
                    the resulting network is the mask/cidr value.
    net-cidr        merge anything within to form this network.
    """ % ( gCommand, gCommand ) )
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"] 
    result['dbUserClass'] = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = "info"

    # command line options:
    result['optionList'] = [ ("m", "merge"), ("i", "in-place") ]

    # Required and minimum arguments
    result['requiredArgs'] = ["network"]
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#            the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    result['network'] = ipaddress.ip_network(argList[0])
    result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise CommandError("invalid option combination")

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # Initialie the network tables.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Split these out for readability.
    inPlace = cmdLineInfo['--in-place']

    # Get the list of networks we need to merge.
    netList = networks.get_networks_within(
            cmdLineInfo['network'], reverse=True)

    # If we have a list of networks, list them.
    if ( len(netList) == 1 ):
        print("Only one network matches that criteria. Nothing to merge.")
    elif ( len(netList) > 1 ):
        mergeTitle = "Found the following networks to merge:"
        netKeys = ["bldg_code", "net_name", "vlan_tag", "network"]
        netHead = ("Building", "Name", "VLAN tag", "network")
        cmd_output.print_table(mergeTitle, netHead, netKeys, netList)

        # Check the building code for each of the networks and
        # complain if they are from different locations.
        firstBldg = netList[0]['bldg_code']
        multiBldgs = False
        for aNet in netList[1:]:
            if ( firstBldg != aNet['bldg_code'] ):
                multiBldgs = True

        # Save the network we are going to keep.
        netToUpdate = netList[0]
        # Extract the new prefix length.
        newPrefix = cmdLineInfo['network'].prefixlen

        updateBldgInfo = building_info.get_building_info(netToUpdate['bldg_code'])

        if ( not cmdLineInfo['--in-place'] ):
            # Only need to do this when the result is a 'free' network.
            # Get the building info so we can get the 'free' zone.
            if ( 'net_zone' not in netToUpdate ):
                # Network not in the free table, set the zone
                netToUpdate['net_zone'] = updateBldgInfo['net_zone']

            # Get the type info so we can find the free table.
            typeInfo = network_types.get_type_info(netToUpdate['net_type'])[0]

            # Set all values to their defaults.
            netToUpdate = network_info.set_default_info(netToUpdate, typeInfo)

        # The destination building code may have been updated
        # by set_default_info so update the destination if necessary.
        if ( netToUpdate['bldg_code'] != updateBldgInfo['bldg_code'] ):
            updateBldgInfo = building_info.get_building_info(
                    netToUpdate['bldg_code'])

        if ( cmdLineInfo['--merge'] ):
            # Delete all but the first network.
            for netToZap in netList[1:]:
                network_info.delete_network(netToZap, typeInfo['net_family'])

            # Update the prefix length on the remaining network.
            mergedNet = network_info.set_prefix_len(netToUpdate, newPrefix)

            if ( not cmdLineInfo['--in-place'] ):
                # Place the new network into the free table.
                try:
                    mergedNet = network_info.free_network(mergedNet,
                            typeInfo, updateBldgInfo['net_zone'])[0]
                except NetException as err:
                    # it is not an error to merge a free network.
                    if ( err.name != "NetIsFree" ):
                        raise
                except:
                    raise

            mergeTitle = "The above networks have been merged to form:"

        else: # if ( not cmdLineInfo['--in-place'] ):

            mergeTitle = "The above networks will be merged to form:"
            # Set the new prefix length.
            oldPrefix = netToUpdate['network'].prefixlen
            mergedNet = netToUpdate
            try:
                mergedNet['network'] = \
                    mergedNet['network'].supernet(new_prefix=newPrefix)
            except AttributeError:
                mergedNet['network'] = \
                    mergedNet['network'].supernet(oldPrefix - newPrefix)

        ## end else if ( cmdAction == 'merge' ):

        # Display the result for the user.
        cmd_output.print_table(mergeTitle, netHead, netKeys, [mergedNet])

        # Say what actually took place.
        mergeWill = \
            "If the above is what you expect, use the --merge option\n" + \
            "to complete the merge. Once complete the new network will be"
        mergeComplete = "The merge has been completed and the new network is"
        mergeFreePool = "in the free pool."
        mergeInPlace = "allocated to the building:\n" \
                    + updateBldgInfo['bldg_name'] \
                    + " (" + updateBldgInfo['bldg_code'] + ")"

        if ( cmdLineInfo['--merge'] and not cmdLineInfo['--in-place'] ):

            print(mergeComplete)
            print(mergeFreePool)

        elif ( cmdLineInfo['--merge'] and cmdLineInfo['--in-place'] ):

            print(mergeComplete)
            print(mergeInPlace)

        elif ( not cmdLineInfo['--merge'] and not cmdLineInfo['--in-place'] ):

            print(mergeWill)
            print(mergeFreePool)

        elif ( not cmdLineInfo['--merge'] and cmdLineInfo['--in-place'] ):

            print(mergeWill)
            print(mergeInPlace)

        # Warn before merging multiple buildings.
        if ( not cmdLineInfo['--merge'] and multiBldgs == True ):

            print("##############################################")
            print("# The above networks are currently allocated #")
            print("#         in multiple buildings!             #")
            print("#    Are you SURE this is what you want?     #")
            print("##############################################")

    ## end elif ( len(netList) > 1 ):
    
## end main()

##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

