#

# 27 Jan 2014   initial creation

##############################################################################
# initialize()
# Perform initialization process for the db_version table. 
# Entry: none. initializes internal variables for use.
# Exit: db_version table is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _schemaVersion
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _schemaVersion.update(db_access.init_db_table(_schemaVersion))

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 showmatch et :

