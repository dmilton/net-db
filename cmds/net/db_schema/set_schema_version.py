#\L

# 27 Jan 2014   initial creation.

############################################################################
# set_schema_version
# Entry: none
# Exit: 
# Exceptions:

def set_schema_version(new_major,new_minor):
    global _schemaVersion

    # Get the current version.
    theVer = get_schema_version()
    if ( new_major > 0 ):
        theVer['major_version'] = new_major
    if ( new_minor > 0 ):
        theVer['minor_version'] = new_minor

    # Now set a new date.
    theVer['reference_date'] = datetime.datetime.now().strftime('%Y%j')

    return(theVer)
## end set_schema_version()

# vim: syntax=python ts=4 sw=4 showmatch et :

