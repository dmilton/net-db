#
gVersion=''

import datetime

from net import db_access

###############################################################################
# Network Types table.
#
global _tablesInitialized
_tablesInitialized = False

global _schemaVersion
_schemaVersion = {
	'database': 'networking',
	'role': 'netmon',
	'table': 'schema_version',
	'index': 'schema_version'
}

from . import initialize
from . import get_schema_version
from . import set_schema_version

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

