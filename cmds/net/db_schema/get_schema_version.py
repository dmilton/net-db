#

# 27 Jan 2014   initial creation.

############################################################################
# get_schema_version
# Extract the current schema version information from the database.
# Input:  none.
# Output: The version number is returned.
# 
def get_schema_version():
    global _schemaVersion

    # Initialize the table,
    initialize()

    # Fetch the information from the database.
    dbResult = db_access.get_table_rows(_schemaVersion, '', '')

    if ( len(dbResult) == 0 ):
        # No version found.
        result = {'minor_version': 0, 'major_version': 0}
    else:
        result = dbResult[0]
        if ( 'version' in result ):
            result['major_version'] = result['version']
            del result['version']

    return(result)
## end def get_schema_version():

# vim: syntax=python ts=4 sw=4 showmatch et :

