#\L
# dd mmm yyyy        initial creation.

############################################################################
# get_junos_chassis_info
def get_junos_chassis_info(theDevice, snmpComm):

    # The Chassis MIB variables we want are:
    # jnxVirtualChassisMemberTable.jnxVirtualChassisMemberEntry.xx
    # Where the xx is:
    #   1 = jnxVirtualChassisMemberId
    #   2 = jnxVirtualChassisMemberSerialnumber - table.
    #       1.3.6.1.4.1.2636.3.40.1.4.1.1.1.2
    #   4 = jnxVirtualChassisMemberMacAddBase - table.
    #       1.3.6.1.4.1.2636.3.40.1.4.1.1.1.4
    #   8 = model
    #       1.3.6.1.4.1.2636.3.40.1.4.1.1.1.8.[vc-id]
    jnxVcInfo = ".1.3.6.1.4.1.2636.3.40.1.4.1.1.1."
    vcDone = False
    serialOid = jnxVcInfo + "2"
    macOid = jnxVcInfo + "4"
    modelOid = jnxVcInfo + "8"

    # Get the serial numbers from the virtual chassis.
    serialNbrs = snmp_walk(snmpComm, theDevice, serialOid)
    # ASN strings are b"xyz" so drop the b' at the start and ' at the end.
    serialList = []
    for aSerial in serialNbrs:
        serialList.append( aSerial['value'][2:-1] )

    macAddrs = snmp_walk(snmpComm, theDevice, macOid)
    macList = []
    for oneMac in macAddrs:
        # ASN mac address is 0x format so drop the 0x
        aMac =  oneMac['value'][2:]
        # Now turn the hex nibbles into colon separated octects.
        macHex = aMac[0:2]
        for i in list( range(2, len(aMac), 2) ):
            macHex += ":" + aMac[i:i+2]
        macList.append(macHex)

    modelNbrs = snmp_walk(snmpComm, theDevice, modelOid)
    modelList = []
    # ASN strings are b'xyz' so drop the b' at the start and ' at the end.
    for oneModel in modelNbrs:
        modelList.append( oneModel['value'][2:-1] )

    result = {}
    result['serial-nbrs'] = serialList
    result['mac-addresses'] = macList
    result['model-nbrs'] = modelList

    return(result)
## end get_junos_chassis_info()


# vim: syntax=python ts=4 sw=4 showmatch et :

