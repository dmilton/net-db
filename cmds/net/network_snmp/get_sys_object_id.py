#\L
# 8 Apr 2013                initial creation.

############################################################################
# get_sys_object_id
def get_sys_object_id(theDevice, snmpComm):

    sysObjectID_OID = '.1.3.6.1.2.1.1.2.0'
    sysObjId = snmp_get(snmpComm, theDevice, sysObjectID_OID)
    result = sysObjId.strip('["]').strip(string.whitespace)

    return(result)
## end get_sys_object_id()


# vim: syntax=python ts=4 sw=4 showmatch et :

