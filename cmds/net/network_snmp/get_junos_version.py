#\L
# 8 Apr 2013        initial creation.

############################################################################
# get_junos_version
def get_junos_version(theDevice, snmpComm):

    codeVerOid = '.1.3.6.1.4.1.2636.3.40.1.4.1.1.1.5.0'
    firmwareVer = snmp_get(snmpComm, theDevice, codeVerOid)
    # ASN format after 'prettyPrint' is "b'12.3R6.6'"
    result = firmwareVer[2:-1]

    return(result)
## end get_junos_version()

# vim: syntax=python ts=4 sw=4 showmatch et :

