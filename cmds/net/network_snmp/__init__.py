#
gVersion=''

import string
from socket import gethostbyaddr
from os import popen

from net.network_error import NetException

global pySnmpExists

# Write exception check around netsnmp and use comand line tools if
# the python netsnmp module is not installed.
try:
    from pysnmp.entity.rfc3413.oneliner import cmdgen
    pySnmpExists = True
except ImportError:
    print("The package works better when library pysnmp is installed.")
    print("See https://pypi.python.org/packages/source/p/pysnmp/")
    pySnmpExists = False
    ##raise

from . import get_junos_chassis_info
from . import get_junos_version
from . import get_sys_object_id
from . import snmp_get
from . import snmp_walk
from . import sys_obj_2_model

# vim: syntax=python ts=4 sw=4 showmatch et :

