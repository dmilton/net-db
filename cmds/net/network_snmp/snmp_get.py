#\L

#  3 Apr 2013        Rewrite to use netsnmp.
# 24 Jan 2013        Add community string as argument.
# 19 Dec 2012        initial creation.

############################################################################
# snmp_get
# Entry:
#       community       the snmp community to use
#       device          the fully qualified host name to query
#       oid             the SNMP OID to obtain.
# Exit:
#       The device has been queried and the value found at the specified
#       OID is returned as the result.

#http://pysnmp.sourceforge.net/examples/current/v3arch/oneliner/
#   manager/cmdgen/get-v2c.html
def snmp_get(community, device, oid):

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
            cmdgen.CommunityData(community),
            cmdgen.UdpTransportTarget((device, 161)),
            oid, lookupNames=False, lookupValues=True,
    )

    # Check for errors and print out results
    if errorIndication:
        raise NetException("SnmpGetFailed", errorIndication)
    else:
        if errorStatus:
            raise NetException("SnmpGetFailed", \
                "snmp get request failed for " + device + " oid: " + oid)
        else:
            result = varBinds[0][1].prettyPrint()

    return(result)
## end def snmp_get():

def snmp_get_old(community, device, oid):

    result = ''

    myCommand = "snmpget -v2c -c" + community + " -ObentU " \
                + device + " " + oid
    p = popen(myCommand,"r")
    while 1:
        line = p.readline()
        if not line: break
        result += line
    ## end while 1:

    if ( not result > '' ):
        raise NetException("SnmpGetFailed", \
                "snmp get request failed for " + device + " oid: " + oid)

    return(result)
## end def snmp_get_old()

def snmp_get_v6():
    # http://pysnmp.sourceforge.net/examples/current/v3arch/oneliner
    # /manager/cmdgen/get-v3-over-ipv6-with-mib-lookup.html
    #from pysnmp.entity.rfc3413.oneliner import cmdgen

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
        cmdgen.UsmUserData('usr-md5-des', 'authkey1', 'privkey1'),
        cmdgen.Udp6TransportTarget(('::1', 161)),
        cmdgen.MibVariable('1.3.6.1.2.1.1.1.0'),
        '1.3.6.1.2.1.1.2.0',
        '1.3.6.1.2.1.1.3.0'
    )

    # Check for errors and print out results
    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1] or '?'
                )
            )
        else:
            for name, val in varBinds:
                print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))

## end def snmp_get_v6():

# vim: syntax=python ts=4 sw=4 showmatch et :

