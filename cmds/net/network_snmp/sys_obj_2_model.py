#\L

# 11 Jun 2014   Remove leading period since the pysnmp utilty does not
#               return the OIDs with them.
# 15 Apr 2013   initial creation.

############################################################################
# sys_obj_2_model
def sys_obj_2_model(oid):

    result = 'unknown'

    if ( oid == '1.3.6.1.4.1.9.1.220' ):
        result = 'WS-2924M-XL-EN'
    elif ( oid == '1.3.6.1.4.1.9.1.283' ):
        result = 'WS-C6509-E'
    elif ( oid == '1.3.6.1.4.1.9.1.287' ):
        result = 'WS-C3524-XL-EN-PWR'
    elif ( oid == '1.3.6.1.4.1.9.1.366' ):
        result = 'WS-C3550-24-EMI'
    elif ( oid == '1.3.6.1.4.1.9.1.400' ):
        result = 'WS-C6513'
    elif ( oid == '1.3.6.1.4.1.9.1.431' ):
        result = 'WS-C3550-12G'
    elif ( oid == '1.3.6.1.4.1.9.1.449' ):
        result = 'WS-C6503-E'
    elif ( oid == '1.3.6.1.4.1.9.1.516' ):
        result = 'WS-C3750-24PS-S'
        result = 'WS-C3750G-24T-S'
    elif ( oid == '1.3.6.1.4.1.9.1.563' ):
        result = 'WS-C3560-24PS-S'
    elif ( oid == '1.3.6.1.4.1.9.1.832' ):
        result = 'WS-C6509-V-E'
    elif ( oid == '1.3.6.1.4.1.3224.1.34' ):
        result = 'SSG5-v92-WLAN'
    elif ( oid == '1.3.6.1.4.1.1916.2.28' ):
        result = 'Summit48si'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.29' ):
        result = 'mx240'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.30' ):
        result = 'ex3200'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.31' ):
        result = 'ex4200'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.34' ):
        result = 'srx3600'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.36' ):
        result = 'srx210'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.59' ):
        result = 'ex8208'

    return(result)
## end sys_obj_2_model()


# vim: syntax=python ts=4 sw=4 showmatch et :

