#\L

#  6 Jun 2014   rewrite to use pysnmp; netsnmp api changed and the output
#               results were vastly different. hopefully pysnmp will be more
#               predictable since it is manually installed rather than part
#               of MacOS where a system update could break our calls.
# 24 Jan 2013   add community string as argument.
# 19 Dec 2012   initial creation.

############################################################################
# snmp_walk
# Entry:
#       community - the SNMP community to use for the query
#       device      - the FQDN of the device to query
#       oid         - the SNMP OID to query.
# Exit:
#       the device has been queried and the results are returned.

def snmp_walk(community, device, oid):
    # http://pysnmp.sourceforge.net/examples/
    #   current/v3arch/oneliner/manager/cmdgen/getnext-v2c.html
    # from pysnmp.entity.rfc3413.oneliner import cmdgen

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
        cmdgen.CommunityData(community),
        cmdgen.UdpTransportTarget((device, 161)),
        oid,
    )

    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            raise NetException("snmp-error", errorStatus.prettyPrint())
        else:
            result = []
            for varBindTableRow in varBindTable:
                mibEntry = {}
                mibEntry['oid'] = varBindTableRow[0][0].prettyPrint()
                mibEntry['value'] = varBindTableRow[0][1].prettyPrint()
                result.append(mibEntry)

    return(result)
## end def snmp_walk():

def snmp_walk_v6():
    # http://pysnmp.sourceforge.net/examples/current/
    # v3arch/oneliner/manager/cmdgen/getnext-v3-over-ipv6-with-mib-lookup.html
    from pysnmp.entity.rfc3413.oneliner import cmdgen

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
        cmdgen.UsmUserData('usr-md5-des', 'authkey1', 'privkey1'),
        cmdgen.Udp6TransportTarget(('::1', 161)),
        cmdgen.MibVariable('IF-MIB', 'ifEntry'),
        lookupNames=True, lookupValues=True
    )

    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
                )
            )
        else:
            for varBindTableRow in varBindTable:
                for name, val in varBindTableRow:
                    print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
## end def snmp_walk_v6():

# vim: syntax=python ts=4 sw=4 showmatch et :

