#\L

#  2 May 2013   initial creation.

############################################################################
# get_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_fields():
    global _columnInfo

    result = {}

    # Get the list of fields/input values for this table.
    tableInfo = db_access.get_table_keys(_columnInfo)

    # Copy the table information so we can modify it.
    for aKey in list(tableInfo.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = tableInfo[aKey]

    return(result)
## end get_fields()

# vim: syntax=python ts=4 sw=4 showmatch et :

