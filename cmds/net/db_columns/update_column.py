#\L

# 20 Sep 2013		set_table_row now returns the updated/inserted
#					values so a get_table_rows is not required.
#  2 May 2013		initial creation.

############################################################################
# update_column
def update_column(theColumn):
	global _columnInfo

	# generate a where clause.
	myWhere = "table_name = '" + newColumn['table_name'] + "'"
	myWhere += "AND table_column = '" + newColumn['table_column'] + "'"

	# Update an existing network with new values.
	columnExists = db_access.get_row_count(_theColumn, myWhere) == 1

	if ( columnExists == False ):
		raise NetException("not found", \
			"the table column " + newColumn['table_name'] + ":" + \
			newColumn['table_column'] + " already exists.")

	# If there is something to update, update it.
	result = db_access.set_table_row(_columnInfo, theColumn, myWhere)

	return(result)
## end update_column()

# vim: syntax=python ts=4 sw=4 showmatch et :

