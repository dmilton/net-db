#

#  2 May 2013		initial writing.

############################################################################
# add_column
# Add a column description to the database
# Entry: newColumn - the dictionary of the column description to add
#		to the database.
# Exit: The column description as identified by the table and column names
#		has been added to the database.

def add_column(newColumn):
	"""
	# Start with an empty template
	newColumn = new_template()
	# The only mandatory field
	newColumn['table_name'] = 'network_zones'
	newColumn['table_column'] = 'network'
	newColumn['column_descr'] = "Network address space to use for zone."
	# 
	add_column(newColumn)
	"""
	# generate a where clause.
	myWhere = "table_name = '" + newColumn['table_name'] + "'"
	myWhere += "AND table_column = '" + newColumn['table_column'] + "'"

	# Check if the type already exists.
	columnInfo = db_access.get_table_rows(_columnInfo, myWhere)
	if ( len(columnInfo) > 0 ):
		raise NetException("duplicate", \
				"the table column " + newColumn['table_name'] + ":" + \
				newColumn['table_column'] + " already exists.")

	# Insert the new network into the table.
	result = db_access.set_table_row(_columnInfo, newColumn)

	return result
## end def add_column():

# vim: syntax=python ts=4 sw=4 showmatch et :

