#\L

#  2 May 2013		initial creation.

############################################################################
# new_db_column
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#		type entry in the database.

def new_db_column():
	global _columnInfo

	result = db_access.new_table_row(_columnInfo)

	return(result)
## end new_db_column()

# vim: syntax=python ts=4 sw=4 showmatch et :

