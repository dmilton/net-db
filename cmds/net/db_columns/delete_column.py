#

#  2 May 2013		initial creation.

############################################################################
# delete_column
# Given a type dictionary, delete the type from the database.
# Entry:
#		theColumn - the type dictionary to delete.
# Exit: the type has been deleted from the database.

# 4 Feb 2012	initial creation

def delete_column(theColumn):
	global _columnInfo

	# Create the where clause
	myWhere = "table_name = '" + theColumn['table_name'] + "'"
	myWhere += "AND table_column = '" + theColumn['table_column'] + "'"

	# delete the row from the table.
	db_access.delete_table_rows(_columnInfo, myWhere)

	return()
## end def delete_column():

# vim: syntax=python ts=4 sw=4 showmatch et :

