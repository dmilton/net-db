#
gVersion='May 4, 2013 - 1.0.0'

from net import db_access
from net.network_error import NetException
from net.cmd_line_io import print_table

# Flag to track if we have been initialized.
_tablesinitialized = False

###############################################################################
# column information table
# This table provides documentation for all columns in the database.

_columnInfo = {
	'database': 'networking',
	'table': 'column_info',
	'role': 'netmon',
	'index': 'table_name, table_column'
}

from . import add_column
from . import delete_column
from . import get_fields
from . import initialize
from . import new_template
from . import print_column
from . import update_column

# vim: syntax=python ts=4 sw=4 showmatch et :

