#\L

# 27 Aug 2014        initial creation.

############################################################################
# new_template
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        user default entry in the database.

def new_template():
    global _userDefaults

    result = db_access.new_table_row(_userDefaults)

    return(result)
## end new_template()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

