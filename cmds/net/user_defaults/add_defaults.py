#

# 9 Apr 2014    Initial creation

##############################################################################
# add_defaults(userDefaults)
# Add a new building to the building_detail table.
# Entry:    campusInfo - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_defaults(userDefaults):

    global _userDefaults

    # Create a new record.
    newDefaults = db_access.new_table_row(_userDefaults)

    # Copy matching values from campusInfo into our new record.
    # This eliminates any extraneous vaules in campusInfo.
    for aKey in list(newDefaults.keys()):
        if ( aKey in userDefaults ):
            newDefaults[aKey] = userDefaults[aKey]
        else:
            del newDefaults[aKey]
        ## end else if ( aKey in campusInfo ):
    ## end for aKey in newDefaults.keys():

    # Build a where clause to identify this default setting.
    myWhere = ""

    # Add the record into the database.
    result = db_access.set_table_row(_userDefaults, newDefaults, myWhere)[0]

    return(result)
## end add_defaults():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

