#

#  9 Apr 2014   initial writing

##############################################################################
# initialize()
# Perform initialization process for user default information table. 
# Entry: none. initializes internal variables for use.
# Exit: user default information table is now ready to use.

def initialize():
    """
    initialize the user defaults table.
    """

    # Our view and associated tables.
    global _userDefaults
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Note that the parent routine must have initialized the
    # database (networking) in order for this routine to work.
    # initialize the fieldList (for SELECT) and keyList (query dictionary)
    _userDefaults.update(db_access.init_db_table(_userDefaults))

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 showmatch et :

