#

# 27 Nov 2013   Add check to ensure the location is unique.
# 18 Sep 2013   initial creation

##############################################################################
# update_defaults(defaultInfo)
# Add a new building to the building_detail table.
# Entry:    defaultInfo - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def update_defaults(defaultInfo):

    global _userDefaults

    # Build a where clause to identify this setting.
    myWhere = "user_name = " + str(defaultInfo['user_name'])

    # Update the record in the database.
    result = db_access.set_table_row(_userDefaults, defaultInfo, myWhere)[0]

    return(result)
## end update_defaults():

# vim: syntax=python ts=4 sw=4 showmatch et :

