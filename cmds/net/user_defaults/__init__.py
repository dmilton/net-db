#
gVersion=''

from .. import db_access

#\
#  9 Apr 2014	initial creation.

###############################################################################
# Default Settings Table
#
_userDefaults = {
	'database': 'networking',
	'role': 'netmon',
	'table': 'default_settings',
	'index': 'user'
};

from . import add_defaults
from . import delete_defaults
from . import get_defaults
from . import get_defaults_fields
from . import initialize
from . import update_defaults

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

