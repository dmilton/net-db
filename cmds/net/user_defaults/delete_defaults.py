#

#  9 Apr 2014   initial creation

##############################################################################
# delete_defaults(defaultInfo)
# Delete the default info record.
# Entry:    defaultInfo - dictionary of values for the default settings
# Exit:        the specified default info record has been deleted.

def delete_defaults(defaultInfo):

    global _userDefaults

    # Build a where clause to identify this building.
    myWhere = "user_name = " + str(defaultInfo['user_name'])

    # Delete the record.
    db_access.delete_table_rows(_userDefaults, myWhere)

    return()
## end delete_defaults():

# vim: syntax=python ts=4 sw=4 showmatch et :

