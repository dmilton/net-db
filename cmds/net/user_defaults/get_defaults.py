#\L

# 26 Sep 2014   Update to accept the username as an argument.
#               Rewrite logic to allow individual list entries to
#               be changed rather than replacing the entire list.
#  9 Apr 2014   initial creation.

############################################################################
# get_defaults
# Determine the default values for a given session.
# Entry: userName - the username to retrieve defaults for.
# Exit: A list of campus records is returned.

def get_defaults(userName=getpass.getuser()):

    global _userDefaults

    # Initialie the table.
    initialize()

    # Start with an empty record.
    result = new_template()

    userNameList = ["net-defaults"]
    if ( userName != None ):
        userNameList.append( userName )

    for aName in userNameList:
        lastUserName = aName # save the last user name entry in the list.
        myWhere = "user_name = '" + aName + "'"
        updateSet = db_access.get_table_rows(_userDefaults, myWhere)
        if ( len(updateSet) == 1 ):
            updateDict = updateSet[0]
            # Override the core basics with the the db values.
            for aKey in result.keys():
                if ( aKey in updateDict ):
                    checkValue = updateDict[aKey]
                    changeValue = _update_value_check(updateDict[aKey])
                    if ( changeValue and type(checkValue) == list ):
                        if ( result[aKey] == None ):
                            # Start with a list of 'None' values.
                            result[aKey] = []
                            for aVal in checkValue:
                                result[aKey].append(None)
                        # Now update any values in the list that aren't None.
                        for valIndex in list(range(len(checkValue))):
                            changeValue = _update_value_check(
                                    checkValue[valIndex])
                            if ( changeValue ):
                                result[aKey][valIndex] = checkValue[valIndex]
                    elif ( changeValue ):
                        result[aKey] = updateDict[aKey]

    # Now force the user value.
    result['user_name'] = lastUserName

    return(result)
## end get_defaults()

def _update_value_check(checkValue):
    if ( checkValue == None ):
        changeValue = False
    elif ( type(checkValue) in [str, list] ):
        if ( len(checkValue) > 0 ):
            changeValue = True
    elif ( type(checkValue) == int ):
        if ( checkValue > 0 ):
            changeValue = True
    else:
        changeValue = True
    return( changeValue )
## end def _update_value_check()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

