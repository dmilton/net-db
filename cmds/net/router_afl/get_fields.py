#\L

# 26 Mar 2013        initial creation.

############################################################################
# get_fields
def get_fields():

    global _routerAFL

    result = db_access.get_table_keys(_routerAFL)

    return(result)
## end get_fields()


# vim: syntax=python ts=4 sw=4 showmatch et :

