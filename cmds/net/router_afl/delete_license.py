#\L

# 11 Apr 2013       update exception to make it consistient amongst modules.
# 31 Mar 2013        initial creation.

############################################################################
# delete_license
# Remove an AFL license from the database.
# Entry: theLicense - the license record to remove from the database.
# Exit: the license has been removed from the database.
# 
def delete_license(theLicense):
    """
    Delete the AFL license row from the network_afl table..
    """

    global _routerAFL

    # Grab the license from the database.
    myWhere = "auth_code = '%s'" % (theLicense['auth_code'],)

    # Does the record exist?
    licenseInDb = db_access.get_row_count(_routerAFL, myWhere)

    if ( licenseInDb == 1 ):
        db_access.delete_table_rows(_routerAFL, myWhere)
    else:
        raise NetException("not found", \
                "auth_code " + theLicense['auth_code'] )

    return()
## end delete_license()


# vim: syntax=python ts=4 sw=4 showmatch et :

