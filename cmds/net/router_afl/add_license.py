#\L

# 20 Sep 2013        set_table_row now returns the updated/inserted
#                    values from the database. get_table_rows call not needed.
# 11 Apr 2013        update exception to make it consistient amongst modules.
#  8 Apr 2013        db_access.set_table_row no longer returns a result so
#                    a call to get_table_rows is required.
# 27 Mar 2013        initial creation.

############################################################################
# add_license
# Add an AFL license to the database.
# Entry: theLicense - the license record to add to the database.
# Exit: the database has been updated to reflect the license provided.
# 
def add_license(theLicense):
    """
    Update the fields of an AFL license row.
    """

    global _routerAFL

    # Grab the license from the database.
    myWhere = "auth_code = '%s'" % (theLicense['auth_code'],)

    licenseInDb = db_access.get_row_count(_routerAFL, myWhere)

    if ( licenseInDb > 0 ):
        raise NetException("duplicate", \
                "auth_code " + theLicense['auth_code'])

    # Pass this to the database routines to do the update.
    result = db_access.set_table_row(_routerAFL, theLicense, myWhere)

    return(result)
## end add_license()


# vim: syntax=python ts=4 sw=4 showmatch et :

