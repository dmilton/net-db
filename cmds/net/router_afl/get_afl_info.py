#

# 12 Apr 2013        Change argument to use the router name rather than
#                    a building code. There is no guarantee that a building
#                    router name is always the building code followed by b1.
# 12 Oct 2012        Initial writing.

############################################################################
# get_afl_info
#
# Input: router - the building code to get the list of licenses for.
# Output: list of license dictionaries for all switches in the router.
# Exception: NoAflAssigned
#

def get_afl_info(router):
    """
    Return the AFL license info for a specific building code.
    """

    global _routerAFL

    myWhere = "router = '" + router + "'"
    myOrder = "member_id"
    result = db_access.get_table_rows(_routerAFL, myWhere, myOrder)

    return(result)
## end get_afl_info()


# vim: syntax=python ts=4 sw=4 showmatch et :

