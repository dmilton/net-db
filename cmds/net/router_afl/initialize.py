#

# 6 Nov 2012        initial creation

##############################################################################
# initialize()
# Perform initialization process for network afl information table. 
# Entry: none. initializes internal variables for use.
# Exit: network afl table is now ready to use.

def initialize():
    """
    initialize the network afl table access field variables.
    """

    # The table of AFL license assignments.
    global _routerAFL
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _routerAFL.update(db_access.init_db_table(_routerAFL))

    _tables_initialized = True

## end initialize()


# vim: syntax=python ts=4 sw=4 showmatch et :

