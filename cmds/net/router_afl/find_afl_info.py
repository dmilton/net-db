#

# 13 Dec 2012        get_table_rows returs a list but the result is supposed
#                    to be a dicitonary. Extract dictionary from the list and
#                    return that as a result.
# 13 Dec 2012        Initial writing.

############################################################################
# find_afl_info
#
# Input: serial number - the switch serial number the licenses for.
# Output: dictionary for the license assigned to the switch.
#         if no license is assigned, None is returned.
#
#

def find_afl_info(searchValue, searchField='serial_nbr'):
    """
    Return the AFL license info for the specified serial number or
    authorization code.
    """

    global _routerAFL

    if ( searchField == "serial_nbr" ):
        myWhere = "serial_nbr = '" + searchValue + "'"
    elif ( searchField == "auth_code" ):
        myWhere = "auth_code = '" + searchValue + "'"
    else:
        raise ValueError("unknown search field: " + searchField)

    result = db_access.get_table_rows(_routerAFL, myWhere)

    # result is always a list from get_table_rows so...
    if ( len(result) == 0 ):
        result = None
    else:
        result = result[0]
    ## end if ( len(result) == 0 ):

    return(result)
## end find_afl_info()


# vim: syntax=python ts=4 sw=4 showmatch et :

