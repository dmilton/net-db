#
gVersion=''

from net.network_error import NetException
from net import db_access

# Flag to track if we have been initialized.
_tables_initialized = False

# 30 Oct 2011    Initial creation.

###############################################################################
# Network AFL License table
#
global _router_afl
_router_afl = {
    'database': 'networking',    # name of the database
    'role': 'netmon',            # role for database access
    'table': 'router_afl',        # name of the table
    'index': 'auth_code',        # name of index column
    'indexType': 'string',        # data type of index column
}

from . import add_license
from . import delete_license
from . import find_afl_info
from . import get_afl_info
from . import get_fields
from . import get_licenses
from . import initialize
from . import new_template
from . import update_license

# vim: syntax=python ts=4 sw=4 showmatch et :

