#\L

# 16 Jun 2014   initial creation.

############################################################################
# license_model
# Entry:
#       model - switch model number
# Exit:
#       the model number has been converted into the type of authorization
#       code (AFL Part Number) required for the type of switch.
# Exceptions:
#       if the model provided is unrecognized an exception is raised.

def license_model(switch_model):
    global _aflModels

    # Here is what should really happen:
    #myWhere = "switch_model = '" + switch_model + "'"
    #aflModelInfo = db_access.get_table_rows(_aflModels, myWhere)
    #if ( len(aflModelInfo) == 0 ):
    #    raise ValueError("Unknown switch model type: " + switch_model)
    #else:
    #    result = aflModelInfo['afl_model']

    # This is a quick fix to handle switch model types in AFL licenses.
    # What is really required is a new table to map the switch model
    # type as returned via SNMP to the AFL model number type as
    # documented here:
    # http://www.juniper.net/techpubs/en_US/junos12.3/topics/concept/
    #           ex-series-software-licenses-overview.html
    # The table would then be referenced here making future updates
    # to this code for new model types unnecessary.
    if ( switch_model.upper() in ['EX3200-24P', 'EX3200-24T', 'EX4200-24F',
                'EX4200-24P', 'EX4200-24PX', 'EX4200-24T'] ):
        result = 'EX-24-AFL'
    elif ( switch_model.upper() in ['EX3200-48P', 'EX3200-48T', 'EX4200-48F',
                'EX4200-48P', 'EX4200-48PX', 'EX4200-48T',
                'EX4500-40F-BF', 'EX4500-40F-BF-C',
                'EX4500-40F-FB', 'EX4500-40F-FB-C'] ):
        result = 'EX-48-AFL'
    elif ( switch_model.upper() == 'EX4550' ):
        result = 'EX4550-AFL'
    elif ( switch_model.upper() == 'EX6210' ):
        result = 'EX6210-AFL'
    elif ( switch_model.upper() == 'EX8208' ):
        result = 'EX8208-AFL'
    elif ( switch_model.upper() == 'EX8216' ):
        result = 'EX8216-AFL'
    elif ( switch_model.upper() == 'EX-XRE200' ):
        result = 'EX-XRE200-AFL'
    elif ( switch_model.upper() == 'EX9204' ):
        result = 'EX9204-AFL'
    elif ( switch_model.upper() == 'EX9208' ):
        result = 'EX9208-AFL'
    elif ( switch_model.upper() == 'EX9214' ):
        result = 'EX9214-AFL'
    else:
        raise ValueError("Unknown switch model type: " + switch_model)

    return(result)
## end license_model()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

