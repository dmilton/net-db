#\L

# 20 Sep 2013        set_table_row now returns the updated/inserted
#                    values from the database. get_table_rows call not needed.
# 11 Apr 2013       update exception to make it consistient amongst modules.
#  8 Apr 2013        db_access.set_table_row no longer returns a result so
#                    a call to get_table_rows is required.
#  1 Apr 2013        Result from get_table_rows could be a list with zero
#                    entries, cannot index [0]. Need index [0] once a
#                    result is verified.
# 31 Mar 2013        Change routine name to update_license and return a
#                    result containing the updated record. Return an
#                    exception when the record does not exist.
# 18 Dec 2012        get_table_rows returs a list and in this instance that
#                    list should always have only one entry.
# 10 Dec 2012        initial creation.

############################################################################
# update_license
# Update an AFL license to reflect the changes that have been applied to
# the dictionary. 
# Entry: theLicense - the license record from the database.
# Exit: the database has been updated to reflect the license provided.
# 
def update_license(theLicense):
    """
    Update the fields of an AFL license row.
    """

    global _routerAFL

    # Grab the license from the database.
    myWhere = "auth_code = '%s'" % (theLicense['auth_code'],)

    licenseInDb = db_access.get_table_rows(_routerAFL, myWhere)

    if ( len(licenseInDb) != 1 ):
        raise NetException("not found", \
                "auth code " + theLicense['auth_code'])

    # turn the list returned by get_table_rows into a singe entry.
    licenseInDb = licenseInDb[0]

    # Create an empty row to populate with updated data.
    updatedLicense = db_access.new_table_row(_routerAFL)

    # Compare the license we got to the one from the database
    # and build a third one which only has the key (auth-code)
    # and the updated fields present.
    for aKey in list(licenseInDb.keys()):
        if ( (aKey in theLicense) == True ):
            if ( licenseInDb[aKey] != theLicense[aKey] ):
                updatedLicense[aKey] = theLicense[aKey]
            else:
                del updatedLicense[aKey]
            ## end if ( licenseInDb[aKey] != theLicense[aKey] ):
        ## end if ( theLicense.has_key(aKey) == True ):
    ## end for aKey in licenseInDb.keys():
    updatedLicense['auth_code'] = theLicense['auth_code']

    # Pass this to the database routines to do the update.
    result = db_access.set_table_row(_routerAFL, updatedLicense, myWhere)

    return(result)
## end update_license()


# vim: syntax=python ts=4 sw=4 showmatch et :

