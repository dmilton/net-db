#

# 16 Jun 2014   Now have multiple kinds of licenses to handle so not only
#               must a count be used, the correct kind of license must be
#               returned to the caller.
# 11 Apr 2013   update exception to make it consistient amongst modules.
# 18 Dec 2012   print count properly, needed string conversion.
#               passed wrong argument for sort order to get_table_rows.
#               query returned empty result because switch_model is not
#               an empty string '', seem to be null.
# 13 Dec 2012   sort auth codes by rtu_serial for assignment.
# 15 Nov 2012   initial writing.

############################################################################
# get_licenses
# Find liceneses in the database that have not been assigned yet.
#
# Input:
#       model - the type of key required.
#       count - how many licenses are required?
#
# Output: list of 'count' license dictionaries which have not been assigned.
# Exception: NoAflAvailable
#

def get_licenses(model, count):
    """
    Return a list of count licenses for a switch type model.
    """

    global _routerAFL

    myWhere = "member_id = -1 AND serial_nbr = ''"
    myWhere += " AND LOWER(afl_model) = LOWER('" + license_model(model) + "')"
    result = db_access.get_table_rows(_routerAFL, myWhere, \
                                sortOrder='rtu_serial', limit=count)

    # Verify that we have a list of licenses.
    aflCount = len(result)
    if ( aflCount != count ):
        raise NetException("not found", "insufficient free licenses, " + \
                str(count) + " needed, " + str(aflCount) + " available.")

    return(result)
## end get_licenses()

# vim: syntax=python ts=4 sw=4 showmatch et :

