#\L

#  4 Sep 2014   Shorten length of ISO network to just the network zone
#               and enough room for an IPv4 address in hex.
# 25 Aug 2014   Cleanup the ISO address generation part and remove
#               the network table section - this will get added later
#               before inserting it into the database - or after the
#               insertion has completed successfully. Allow for future
#               use of multiple areas, perhaps based on the building zone.
# 21 Aug 2014   Check that a valid loopback address has been provided.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 12 May 2014   Add more details on the format of an ISO address and
#               extend the system id portion to the full 6 bytes.
# 28 Apr 2014   Add optional isoType argument to save the lookup when
#               the caller has already done this.
#  5 Mar 2014   Remove dependancy on IPv6 and use v6 or v4.
#               Compute using IPv6 when available.
# 23 Oct 2013   Change to use name_network to generate the name.
# 17 Oct 2013   ISO networks are now in their own table and new_template is
#               aware of this and sets values appropriately. This routine
#               now returns a network dictionary that could be added to
#               the ISO networks table.
# 11 Sep 2013   Use type info for name extension.
# 19 Apr 2013   Add network_table entry to response record.
# 18 Apr 2013   use zfill string routine instead of loop.
# 17 Apr 2013   initial creation.

############################################################################
# calc_iso_network
# Entry: 
#       loopNetList - the list of networks assigned as type 'loop'
#       isoType - type info for 'iso' from network_types.
# Exit:
#       returns an ISO network structure. This will be computed on one of
#       two values. If the loopNetList contains an IPv6 address then the
#       ISO network will be the based on the host portion of that address.
#       If the loopNetList contains an IPv4 address then the ISO address
#       will be based on the full IPv4 address. Preference will always be
#       given to the IPv6 address.
#
def calc_iso_network(loopNetList, isoType={}):
    
    # Which network are we using for the basis of the ISO address?
    baseNet = None
    for aNet in loopNetList:
        if ( aNet['network'].version == 6 and aNet['net_type'] == 'loop' ):
            baseNet = aNet
            break
        elif ( aNet['network'].version == 4 and aNet['net_type'] == 'loop'
                and baseNet == None ):
            baseNet = aNet

    # Test that we have a list.
    if ( baseNet == None ):
        raise NetException("NoLoopback", "ISO network addresses are computed"
                + " based on the loopback address but none provided.")

    # If we didn't receive the type info, get it now.
    if ( 'net_type' not in isoType ):
        isoType = network_types.get_type_info('iso')[0]

    # Get a new ISO network template.
    result = new_template(isoType)

    # Assign all common values.
    result['vlan_tag'] = abs(isoType['base_vlan_tag'])
    result['bldg_code'] = baseNet['bldg_code']
    result['net_name'] = name_network(result, isoType, baseNet['net_name'])

    # 49.0001.0000.0000.00
    # isoNet = "49."                # AFI
    # isoNet += "0001."             # area id. (take from zone)
    # isoNet += "0000.0000"         # system id.
    areaId = 1
    isoNetStart = "49." + str(areaId).zfill(4) + "."
    if ( baseNet['network'].version == 6 ):
        # Extract the last two bytes of the IPv6 address.
        netBit = str(baseNet['network'].network_address).split(":")[-1]
        isoAddrPart = "0000." + netBit.zfill(4)
    else:
        # Use the entire IPv4 address represented in HEX.
        netAsHex = hex(int(baseNet['network'].network_address))[2:]
        isoAddrPart = netAsHex[0:4] + '.' + netAsHex[4:8]
    result['network'] = isoNetStart + isoAddrPart + ".00"

    return(result)
## end calc_iso_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

