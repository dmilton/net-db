#\L

#  6 Nov 2041   Change call to net_id_2_bldg_net to use calc_range_net.
# 22 Aug 2014   Complete rewrite based on get_v4_networks and the info
#               gathered in get_bldg_net_detail.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Apr 2014   initial creation.

############################################################################
# get_v6_networks
# Entry:
#       bldgInfo - dictionary of information about the building to
#               assign networks to.
#       bldgNetDetail - dictionary of information about networks in the
#               building. Output of get_bldg_net_detail.
#       infoOnly - flag to indicate if information should be updated.
#               When allocating networks, values in the building
#               and/or address range may also need updates to reflect
#               the assignment.
#
# Exit:
#       If this is the first time call for a building then bldgInfo will
#       have an invalid ipv6_netid value; one must be computed and 
#       ipv6_nextnet set to zero. The zone address range must also be
#       updated to indicate a building has been 'consumed'.
#       When infoOnly is true, the next available network id will be
#       determined but not consumed (ie range not updated). Similarly,
#       the building info changes will not be saved either.
#
# Exceptions:

def get_v6_networks(bldgInfo, bldgNetDetail, infoOnly):

    # Start with an empty list of networks to assign/allocate.
    netsToAllocate = []

    # Get all the address spaces for this zone.
    zoneAddrList = network_zones.get_range_info(
                    bldgInfo['net_zone'], "", 6)

    # If we only get one response then all types come from the same block.
    if ( len(zoneAddrList) == 1 ):
        userZoneInfo = zoneAddrList[0]
    else:
        # Where any required user networks will come from.
        for anAddressZone in zoneAddrList:
            if ( "user" in anAddressZone['net_type_list'] ):
                userZoneInfo = network_zones.get_user_zone_info(
                                bldgInfo['net_zone'], ipVersion=6)

    # do we have a base address assigned to the building?
    if ( bldgInfo['ipv6_netid'] < 0 ):
        # no, get the next free one for the building's zone.
        bldgInfo['ipv6_netid'] = network_zones.get_next_bldg(
                userZoneInfo, infoOnly)
        bldgInfo['ipv6_nextnet'] = 0

        # Update the building to show the allocation of a base address.
        if ( infoOnly == False ):
            buildings.update_building(bldgInfo)

    netTypeInfoList = bldgNetDetail['net_type_detail']['type_info_by_name']
    
    needISO = False
    for typeToAdd in bldgNetDetail['types_to_assign']:
        if ( typeToAdd.lower() == "iso" ):
            needISO = True
        elif ( typeToAdd.lower() == "loop" ):
            newLoop = calc_v6_loop(bldgInfo, userZoneInfo,
                            netTypeInfoList['loop'], infoOnly)
            netsToAllocate.append(newLoop)
            loopNetList.append(newLoop)
        elif ( typeToAdd.lower() == "link" ):
            newLinks = calc_v6_links(bldgInfo, epCode='',
                        zoneInfo=userZoneInfo,
                        linkType=netTypeInfoList['link'],
                        doNotUpdate=infoOnly)
            for aNet in newLinks:
                netsToAllocate.append(aNet)
        else:
            typeInfo = netTypeInfoList[typeToAdd]
            # calculate a network to assign.
            if ( len(zoneAddrList) == 1 ):
                zoneRange = zoneAddrList[0]['network_range']
            else:
                # multiple address spaces for this zone, figure out
                # which one to use for this network type.
                zoneFound = False
                anyType = {}
                for anAddressZone in zoneAddrList:
                    if ( typeToAdd in anAddressZone['net_type_list'] ):
                        zoneRange = anAddressZone['network_range']
                        zoneFound = True
                        break
                    elif ( anAddressZone['net_type_list'] == "" ):
                        # Check for a zone that can be used for any type.
                        anyTypeNetwork = anAddressZone['network_range']
                if ( not zoneFound ):
                    # If we didn't find a specific zone,
                    # use the 'any type' default
                    zoneRange = anyTypeNetwork

            # Get a new network entry.
            netToAdd = new_template(typeInfo)

            # Populate the network entry.
            netToAdd['network'] = network_zones.calc_range_net(
                    zoneRange, 64, bldgInfo['ipv6_netid'],
                    bldgInfo['ipv6_nextnet'])
            netToAdd['bldg_code'] = bldgInfo['bldg_code']
            netToAdd['vlan_tag'] = get_next_vlan(
                                    bldgInfo['bldg_code'],
                                    typeInfo['base_vlan_tag'], 6)
            netToAdd['net_name'] = name_network(netToAdd, typeInfo)
            # Do not set the table, let assign_network or assign_network
            # figure out which table to insert the network into.
            netToAdd['network_table'] = None

            netsToAllocate.append(netToAdd)
        ## end else if ( typeToAdd.lower() == "iso" ):

        # If this is a loop network, make sure we record it so
        # generation of an ISO network will use it.

    ## end for typeToAdd in needTypesList:

    # If we need an ISO network, generate that too.
    if ( needISO ):
        isoNetwork = calc_iso_network(loopNetList, netTypeInfoList['iso'])
        netsToAllocate.append(isoNetwork)

    return(netsToAllocate)
## end get_v6_networks()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

