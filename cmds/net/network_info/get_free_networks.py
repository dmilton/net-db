#

#  5 Nov 2014   Check the CIDR value provided and raise an exception
#               for illegal or unsupported values.
# 25 Aug 2014   Change setting of the network table based on the
#               table structure rather than a constant. This way the
#               table name in the database could change without also
#               requiring changes here.
# 30 Jan 2014   Initial writing.

############################################################################
# get_free_networks
#
# Input:
#        cidrSize - cidr minimum mask size.
#        netType - network type.
#        netZone - the zone to seach.
# Output: list of network dictionaries for all networks matching
#        the criteria provided.
#

def get_free_networks(cidrSize, netType, netZone):
    """
    Return free networks of a given size (or larger) and/or type.
    """
    # table being searched.
    global _networkFree

    # If cidrSize in invalid, raise exception
    if ( cidrSize > 32 or cidrSize < 16 ):
        raise VauleError("A CIDR mask value of " + str(cidrSize) + \
                " is invalid. CIDR must be >= 16 and <= 32")

    # Order list by network.
    sortOrder = "network"

    # Build our search.
    myWhere = ''
    if ( cidrSize == 0 ):
        cidrSize = 16

    if ( cidrSize >= 16 and cidrSize <= 32 ):
        myWhere = "masklen(network) >= 16 and masklen(network) <= " + \
                str(cidrSize)

    if ( netType > '' ):
        myWhere += " and net_type = \'" + netType + "\'"

    if ( netZone >= 0 ):
        myWhere += " and net_zone = " + str(netZone)

    # Get our list of user and building centric networks.
    freeNets = db_access.get_table_rows(_networkFree, myWhere, sortOrder)

    result = []
    # If we have an IPv6 loopback address, add an ISO network.
    for aNet in freeNets:
        aNet['network_table'] = _networkFree['table']
        result.append( aNet )

    return(result)
## end get_free_networks()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

