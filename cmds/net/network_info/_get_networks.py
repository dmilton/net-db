#

# 20 May 2015   Logic was reversed for finding networks contained within
#               and contained by. Add sort functionality so results are
#               returned in network address order.
# 10 Sep 2014   In the case of a exception, the search value may not
#               always be a string so force conversion.
#  4 Sep 2014   ISO search causes SQL syntax error when compared against
#               cidr or inet. Exclude those checks when network type is ISO.
# 25 Aug 2014   Update to use the table column from the table record
#               rather than the hard coded value for network_table.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 22 Jan 2014   ISO networks are in a table, they shouldn't be generated.
#               removed the option to have an ISO network generated.
# 19 Dec 2013   ISO search is different from other networks since
#               a match by network of <<= or >>= doesn't make sense.
# 17 Dec 2013   ISO networks don't have a net_type field, add it so
#               networks of that type can be properly identified.
# 17 Oct 2013   Add support for network_iso table.
# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 14 Jun 2013   Searching for name where clause was broken. Fixed.
#  7 Jun 2013   A bridged vlan doesn't have an address space so a
#               check needs to be made to avoid issues.
# 25 Apr 2013   Initial writing.

############################################################################
# _get_networks
#
# Input:
#        findKey - What to use to search for networks.
#        findValue - the value to seach for
#        sortOrder - the order to return networks in.
#        withFree - include networks from the free table?
# Output: list of network dictionaries for all networks matching
#        the criteria provided.
# NOTE: networks are grouped by the order clause so within the building
#        table they will be ordered but if more than one table has matches
#        then they could appear 'out of sequence.'
#

def _get_networks(findKey, findValue, sortOrder, withFree=False):
    """
    Return the active networks for a specific building code.
    """
    # tables being searched.
    global _networkBuildings, _networkExternals, _networkLinks
    global _networkISO, _networkFree

    myWhere = ""
    linkWhere = ""
    isoWhere = ""
    externWhere = ""
    # Build our search.
    if ( findKey == "bldgCode" ):
        myWhere = "bldg_code = \'%s\'" % (findValue)
        linkWhere = myWhere + " OR ep_code = \'%s\'" % (findValue)
        externWhere = myWhere + " OR remote_code = \'%s\'" % (findValue)
        isoWhere = myWhere
    else:
        if ( findKey == "cidrEqual" ):
            myWhere = "network = '" + str(findValue) + "'"
        elif ( findKey == "cidrContains" ):
            # myWhere = "network <<= '" + str(findValue) + "'"
            myWhere = "network >>= '" + str(findValue) + "'"
        elif ( findKey == "cidrContainedBy" ):
            # myWhere = "network >>= '" + str(findValue) + "'"
            myWhere = "network <<= '" + str(findValue) + "'"
        elif ( findKey == "name" ):
            myWhere = "net_name = '" + findValue + "'"
            isoWhere = myWhere
        elif ( findKey == "isoEqual" ):
            isoWhere = "network = '" + str(findValue) + "'"
        else:
            raise NetException("KeyUknown", "Invalid search criteria " \
                    + findKey + ":" + str(findValue))
        if ( not findKey == "isoEqual" ):
            # Only the building code search requires different clauses
            # for links and external networks.
            externWhere = myWhere
            linkWhere = myWhere

    # Start with an empty list.
    result = []

    # Get our list of user and building centric networks.
    if ( myWhere > "" ):
        bldgNets = db_access.get_table_rows(
                _networkBuildings, myWhere, sortOrder)
        for aNet in bldgNets:
            aNet['network_table'] = _networkBuildings['table']
            result.append( aNet )

    # Get our ISO network if it exists.
    if ( isoWhere > '' ):
        isoNets = db_access.get_table_rows(_networkISO, isoWhere)
    else:
        isoNets = []
    if ( len(isoNets) > 0 ):
        for aNet in isoNets:
            # Add fields not in the database as they are constants.
            aNet['net_type'] = 'iso'
            aNet['vlan_tag'] = 1
            aNet['network_table'] = _networkISO['table']
            result.append(aNet)

    # Add networks which are routed by this router but are not in this building.
    if ( externWhere > "" ):
        externalNets = db_access.get_table_rows(
                _networkExternals, externWhere, sortOrder)
        for aNet in externalNets:
            aNet['network_table'] = _networkExternals['table']
            result.append( aNet )

    # Add links to the core.
    if ( linkWhere > "" ):
        linkNets = db_access.get_table_rows(_networkLinks, linkWhere, sortOrder)
        for aNet in linkNets:
            aNet['network_table'] = _networkLinks['table']
            result.append( aNet )

    # Add any networks which are in the free table.
    if ( myWhere > "" and withFree ):
        freeNets = db_access.get_table_rows(_networkFree, myWhere, sortOrder)
        for aNet in freeNets:
            aNet['network_table'] = _networkFree['table']
            result.append( aNet )

    # Sort the list by network since the multi-table approach
    # will give results unsorted results.
    result = sorted( result , key=lambda elem: "%04d" % elem['vlan_tag'] )
    # result = sorted( result , key=lambda elem: "%s" % (elem['network']) )
    # The sort fails when a "bridge" network type is encountered. 
    # The network field value may be "None"???
    #result = sorted( result , 
    #        key=lambda item: ipaddress.get_mixed_type_key(item['network']) )

    return(result)
## end _get_networks()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

