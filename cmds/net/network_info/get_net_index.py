#\L

# 26 Mar 2014   The contained within comparision changed with the python3
#               ipaddress class. Fixed the comparisons. Rewrote to use
#               recursion to calculate the depth so a restriction of 16
#               bits isn't required any more.
# 22 Oct 2013   initial creation.

############################################################################
# get_net_index
# Given a base network and a target network, determine how many subnets
# of target.prefexlen size exist between the start of base network and
# the target network.
#
# Entry:
#    baseNet - the base network.
#    targetNet - the target network. Must be in baseNet.
# Exit:
#    The index of the subnet is determined and returned as the result.

def get_net_index(baseNet, targetNet):

    # Check that baseNet contains targetNet
    if ( not baseNet.overlaps(targetNet) ):
        raise ValueError(str(baseNet) + " does not contain " + str(targetNet))

    # Start with 0 to prime the recursive routine.
    result = _get_net_index(baseNet, targetNet, '0')

    return(result)
## end get_net_index()

# Recurse down the tree to find the index rather than looping through
# all of the subnets.

def _get_net_index(baseNet, targetNet, binIndex):

    # A really, really slow way:
    # result = 0
    # for aNet in baseNet.subnet(bitsOfNet):
    #     if ( aNet == targetNet ):
    #         break
    #     result += 1

    try:
        lowerHalf, upperHalf = list(baseNet.subnets())
    except AttributeError:
        lowerHalf, upperHalf = baseNet.subnet(1)

    # Are we on the last step?
    if ( lowerHalf.prefixlen == targetNet.prefixlen ):
        # Yes. Which half is the correct one?
        if ( lowerHalf == targetNet ):
            result = int(binIndex + '0', 2)
        else:
            result = int(binIndex + '1', 2)
    else:
        # Build the index on the way down, one bit at a time.
        if ( lowerHalf.overlaps(targetNet) ):
            binIndex += '0'
            result = _get_net_index(lowerHalf, targetNet, binIndex)
        else:
            binIndex += '1'
            result = _get_net_index(upperHalf, targetNet, binIndex)

    return(result)
## end _get_net_index()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

