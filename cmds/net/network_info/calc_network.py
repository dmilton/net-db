#\L

# 23 Apr 2014   Mark this as a network that does not exist in the database
#               so add_network or assign_network work properly.
# 29 Dec 2013   no need for a list as a result, just return the
#               single computed network.
# 22 Oct 2013   initial creation.

############################################################################
# calc_network
# Given a starting network, determine what network to use for the specified
# type in the provided zone.
# Entry:
#    netInfo - the network (dict) used as a starting point.
#    typeInfo - the type of (dict) network to compute.
#    bldgInfo - the building info (dict) for the network.
# Exit:
#    The result is a network which is based on netInfo.

def calc_network(netInfo, typeInfo, bldgInfo):

    # Start with an empty template.
    result = new_template(typeInfo)
    result['net_type'] = typeInfo['net_type']
    if ( typeInfo['base_vlan_tag'] != 0 ):
        result['vlan_tag'] = abs(typeInfo['base_vlan_tag'])
    else:
        result['vlan_tag'] = netInfo['vlan_tag']
    result['net_zone'] = bldgInfo['net_zone']
    result['bldg_code'] = bldgInfo['bldg_code']

    # Get the zone information for our starting network.
    userRangeInfo = network_zones.find_user_range(
            bldgInfo['net_zone'], netInfo['network'].version )

    # Figure out what index in the range the starting network is.
    netIndex = get_net_index(
            userRangeInfo['network_range'], netInfo['network'])

    # Get the type specific zone information
    rangeInfoList = network_zones.get_range_info(
            bldgInfo['net_zone'], typeInfo['net_type'],
            netInfo['network'].version )

    # get_range_info returns a list but we expect only one.
    if ( len(rangeInfoList) > 1 ):
        raise NetException("TooManyRanges", "More than one IPv"
                + str(netInfo['network'].version)
                + " address range matches for the zone "
                + bldgInfo['net_zone'] + " and type " + typeInfo['net_type'] )
    else:
        rangeInfo = rangeInfoList[0]

    if ( rangeInfo['net_type_list'] > "" ):
        types_for_zone = rangeInfo['net_type_list'].split(',')

    # Find the same index in the type specific zone.
    newNetAddr = network_zones.calc_indexed_net(
            rangeInfo['network_range'], netInfo['network'].prefixlen, netIndex)

    # If we have multiple types in this zone then split evenly amongst
    # each type. This means the number of types in a zone should be a
    # power of 2 for optimal assignment utilization.
    if ( len(types_for_zone) <= 1 ):
        result['network'] = newNetAddr
    else:
        # How many bits do we need.
        numberTypes = len(types_for_zone)
        for i in range(1, numberTypes+1):
            if ( 2 ** i >= numberTypes ):
                maskSize = i
                break
        # Determine the subnets.
        subnets = list(newNetAddr.subnets(prefixlen_diff=maskSize))

        # Which one do we need?
        for i in range(numberTypes):
            if ( typeInfo['net_type'] == types_for_zone[i] ):
                result['network'] = subnets[i]
                break

    # Set an appropriate name.
    result['net_name'] = name_network(result,
            typeInfo, netInfo['net_name'])

    return(result)
## end calc_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

