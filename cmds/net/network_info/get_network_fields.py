#\L

# 20 Dec 2013   loop through all of the different network tables so
#               any possible input key for all network types can be
#               used on the command line.
# 17 Oct 2013   initial creation.

############################################################################
# get_network_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to networks in the network_free table.
# Entry:    none.
# Exit:
#       a dictionary containing all the fields of a network as if it were
#       to be inserted into any table in the networking database. This is
#       somewhat misleading because each of the network_xxx tables has
#       unique fields specific to the kinds of networks kept there. Not
#       all fields are appropirate for all networks. Ie the free_date is
#       not appropriate for a network in active use.

def get_network_fields():

    global _networkTables

    result = {}

    for aTable in _networkTables:
        # Get the list of fields/input values for this table.
        tableInfo = db_access.get_table_keys(aTable)

        # Copy the table information so we can modify it.
        for aKey in list(tableInfo.keys()):
            # translate _ to - so we can accept input with a hyphen.
            key2 = aKey.translate(str.maketrans('-','_'))
            result[key2] = tableInfo[aKey]

    return(result)
## end get_network_fields()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

