#

# 21 Aug 2014   Update column names.
# 16 Jun 2014   endpoint priority can now be zero so an endpoint can
#               be defined but excluded from the link assignment process.
# 17 Oct 2013   Initial writing

############################################################################
# add_endpoint
# Add a new endpoint to a zone.
# Entry:
#       epInfo - endpoint information dictionary.
# Exit:
#       a new endpoint corresponding to epInfo has been added.

def add_endpoint(epInfo):
    """
    # Add the specified endpoint to the networking database.
    epInfo = networks.new_endpoint()
    epInfo['ep_code'] = 'ep1'
    epInfo['core_priority'] = -1
    epInfo['net_zone'] = 999
    networks.add_endpoint(epInfo)
    """

    global _networkEndpoints

    # Make sure the name is not a duplicate.
    myWhere = "ep_code = '" + epInfo['ep_code'] + "'"
    epCount = db_access.get_row_count(_networkEndpoints, myWhere)
    if ( epCount != 0 ):
        raise NetException( "EndpointExists", epInfo['ep_code'] )

    # Default the priority if it isn't already set.
    if ( epInfo['core_priority'] == None ):
        epInfo['core_priority'] = -1

    # Endpoints within a zone must all have unique priorities if they
    # are greater than zero. If one exists with the same value as provided,
    # change it to force a difference.
    if ( epInfo['core_priority'] > 0 ):
        myWhere = "net_zone = " + str(epInfo['net_zone'])
        myWhere += "AND core_priority = " + str(epInfo['core_priority'])
        epCount = db_access.get_row_count(_networkEndpoints, myWhere)
        if ( epCount != 0 ):
            # Have matching endpoint priority. Make it something different.
            epInfo['core_priority'] += 10

    result = db_access.set_table_row(_networkEndpoints, epInfo)[0]

    return(result)
## end def add_endpoint():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

