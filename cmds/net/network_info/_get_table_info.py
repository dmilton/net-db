#\L

# 25 Aug 2014   initial creation.

############################################################################
# _get_table_info
# Given a table name, return the full table information dictionary.
# Entry:
#       netTableName - the name of the table to return details on.
# Exit:
#       If the table name is valid, the table info is returned.
#

def _get_table_info(netTableName):
    global _networkTables, _networkFree, _networkISO

    if ( netTableName == None ):
        result = None
    elif ( netTableName.lower() == _networkFree['table'].lower() ):
        result = _networkFree
    elif ( netTableName.lower() == _networkISO['table'].lower() ):
        result = _networkISO
    else:
        # From the table name find the table info.
        result = None
        for aNetTable in _networkTables:
            if ( netTableName == aNetTable['table'] ):
                result = aNetTable
                break

    if ( result == None ):
        raise NetException( "TableUnknown", \
                "Could not find table information for '" 
                + str(netTableName) + "'" )

    return(result)
## end _get_table_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

