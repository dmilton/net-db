#\L

# 14 Apr 2014   Add check for vlan tag being None and generate error.
# 26 Sep 2013   Add ipVer argument to allow selection by version.
#               Expand range to allow for 64 user vlans in the building.
#  2 May 2013   initial creation.

############################################################################
# get_next_vlan
# Find the next available vlan in a building.
#
# Entry:
#        bldgCode - the building code to search within.
#        vlanTag - the starting vlan tag to search from.
#        ipVer - restrict search to IPv4 or IPv6
# Exit:
#        the database has been searched and the next available free vlan id
#        in the building has been determined and returned.

def get_next_vlan(bldgCode, vlanTag, ipVer=4):
    global _networkAssigns

    if ( vlanTag == None or vlanTag == 0 ):
        # we should never get called with this value
        raise ValueError("the vlan tag cannot be None or zero.")
    elif ( vlanTag < 0 ):
        # this should be unique in the building so return the absolute value
        result = abs(vlanTag)
    else:
        # This has a positive starting value. Search the currently allocated
        # vlans (in the building) starting with this value and use the next
        # available one.
        myWhere = "bldg_code = '" + bldgCode + "'"
        myWhere += " AND family(network) = " + str(ipVer)

        for i in range(vlanTag, vlanTag + 64):
            vlanWhere = myWhere + " AND vlan_tag = " + str(i)
            netCount = db_access.get_row_count(_networkAssigns, vlanWhere)
            if ( netCount == 0 ):
                result = i
                break

    return(result)
## end get_next_vlan()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

