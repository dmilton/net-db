#\L

# 25 Jan 2015   Stop testing for no user network present and let other
#               routines handle the exception if necessary.
#  9 Jan 2015   When assigning specific networks, prime_net was not
#               being set.
#  4 Nov 2014   When a network type is specifically requested, that
#               type can have more than one in a building, and the building
#               already has one of those types - the type got eliminated.
#               Changed so that requested types are always returned as a
#               type to assign.
# 21 Aug 2014   initial creation.

############################################################################
# get_bldg_net_detail
# Get the details of the networks currently assigned to a building.
#
# Entry:
#       bldgCode - the building code to examine assignments for.
#       requestedTypes - set of network types desired in addition to required.
#       ipVersion - 4 or 6 - look for IPv4 or IPv6 network types.
#
# Exit:
#       A dictionary of information is returned about the building:
#       assigned_networks - list of networks currently assigned.
#       assigned_types - set of network types currently assigned.
#       required_type_names - set of network types which are required.
#       multi_type_names - set of types which can have multiple in bldg.
#       type_info_by_name - dictionary of all types, key is type name.
#       prime_net - primary user network (40 for v6, flagged for v4)
#       loop_nets - list of link networks for this ipVersion
#       link_nets - list of link networks for this ipVersion
#
# Exceptions:
#       For user buildings and IPv4, if no primary user address space assigned.

def get_bldg_net_detail(bldgCode, requestedTypes, ipVersion):

    result = {}
    # Produce a list of network types we already have.
    # Get the list of network types we can allocate.
    netTypeDetail = network_types.get_type_detail(ipVersion)
    ipFamily = 'family_ipv' + str(ipVersion)

    # Get the existing list of networks for this building.
    bldgNets = get_networks(bldgCode)

    # Start with the required types and those which were explicitly
    # requested by the caller.
    typesToAssign = set(netTypeDetail['required_type_names'])

    # loop through the networks already assigned, figuring out what
    # type and family it belongs in so we can remove it from the types
    # to assign. This will eliminate 'requestedTypes' when that type is
    # already assigned but there can be multiple of that type assigned
    # in a building.
    assignedTypes = set()
    loopNets = []
    linkNets = []
    for aNetwork in bldgNets:

        netTypeName = aNetwork['net_type']
        netTypeInfo = netTypeDetail['type_info_by_name'][netTypeName]
        netFamily = netTypeInfo['net_family']

        if ( netTypeName in netTypeDetail['family_other'] ):
            # Non IP types should always be recorded but in most cases
            # these should not be _required_ types.
            assignedTypes.add(netTypeName)
            if ( netTypeName in netTypeDetail['uniq_type_names'] ):
                # These networks are always unique in the building.
                typesToAssign.discard(netTypeName)

        elif ( netTypeName in netTypeDetail['family_ip']
                or netTypeName in netTypeDetail[ipFamily] ):
            # We have an IP family, check that we have the right version.
            if ( aNetwork['network'].version ==  ipVersion ):
                # we have a match for a unique type, remove from the list.
                typesToAssign.discard(netTypeName)

            # These can be version specific (ISO/bridge has no .version attr)
            thisNetVer = aNetwork['network'].version

            # Only capture networks of the IP version of address we
            # are assigning: ie - need IPv6 loopback and link for IPv6 user.
            if ( thisNetVer == ipVersion ):
                assignedTypes.add(netTypeName)

            if ( netTypeName == 'loop' ):
                loopNets.append(aNetwork)
            elif ( netTypeName == 'link' ):
                linkNets.append(aNetwork)
            elif ( netTypeName == 'user' ):
                # capture the primary network.
                if (( ipVersion == 4 and aNetwork['prime_net'] ) or
                    ( ipVersion == 6 and aNetwork['vlan_tag'] == 40 ) ):
                    primeNet = aNetwork
        ## end else if ( thisNetFamily not in ['ip', 'ipv4', 'ipv6'] ):
    ## end for aNetwork in bldgNets:

    # Raise an exception if we don't have a prime network.
    try:
        result['prime_net'] = primeNet
    except:
        result['prime_net'] = None

    result['assigned_networks'] = bldgNets
    result['assigned_types'] = assignedTypes
    # Ensure the requested types are part of the list.
    result['types_to_assign'] = typesToAssign.union(requestedTypes)
    result['net_type_detail'] = netTypeDetail
    result['loop_nets'] = loopNets
    result['link_nets'] = linkNets

    return(result)
## end get_bldg_net_detail()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

