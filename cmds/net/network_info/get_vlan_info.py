#\L

# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Add search of the external networks table to allow addition
#               of secondary addresses on external networks.
# 22 May 2013   initial creation.

############################################################################
# get_vlan_info
#       Given a building code and vlan tag, find the network in the database
#       that matches the building code and vlan tag.
#       Functionally this is to allow addition of a secondary address space
#       to a network. This should find both the primary and any secondary
#       address spaces associated with that vlan.
# Entry:
#       bldgCode - the building code to get information about.
#       vlanTag - the vlan tag to look for.
# Exit:
#       Only two tables network_bldgs and network_extern are
#       searched. network_free, network_link, and network_iso are not.

def get_vlan_info(bldgCode, vlanTag):
    global _networkBuildings, _networkExternals

    # Only search the network buildings table.
    bldgWhere = "bldg_code = '" + bldgCode + "'"
    rmtWhere = " OR remote_code = '" + bldgCode + "'"
    vlanWhere = " AND vlan_tag = " + str(vlanTag)
    myWhere = bldgWhere + vlanWhere
    extWhere = "(" + bldgWhere + rmtWhere + ")" + vlanWhere

    myOrder = "network"

    # Check the buildings table
    bldgNets = db_access.get_table_rows(_networkBuildings, myWhere, myOrder)
    for aNet in bldgNets:
        aNet['network_table'] = _networkBuildings['table']
    # Check the external networks table
    extNets = db_access.get_table_rows(_networkExternals, extWhere, myOrder)
    for aNet in extNets:
        aNet['network_table'] = _networkExternals['table']

    result = bldgNets + extNets

    return(result)
## end get_vlan_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

