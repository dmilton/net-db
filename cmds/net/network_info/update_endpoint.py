#\L

# 17 Oct 2013   initial creation.

############################################################################
# update_endpoint
# Update an existing endpoint.
# Entry:
#       epInfo - the endpoint to update.
# Exit:
#       the endpoint has been updated.

def update_endpoint(epInfo):
    global _networkEndpoints

    myWhere = "ep_code = '" + epInfo['ep_code'] + "'"
    result = db_access.set_table_row(_networkEndpoints, epInfo, myWhere)[0]

    return(result)
## end update_endpoint()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

