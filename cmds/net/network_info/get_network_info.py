#\L

# 17 Jun 2014   Raise an exception if more than one match is found.
# 24 Jan 2014   Get information from the free table too.
# 18 Oct 2013   initial creation.

############################################################################
# get_network_info
# Look in the network_xxx tables to find the network specified.
# Entry:
#    searchKey - what value are we searching for? cidrEqual? isoEqual? name?
#    searchValue - the value to search for.
# Exit: The network_xxx tables have been searched, looking for a network
#    that matches searchValue. If one is found it is returned.

def get_network_info(searchKey, searchValue):

    myOrder = "network"
    netList = _get_networks(searchKey, searchValue, myOrder, withFree=True)

    netsFound = len(netList)
    if ( netsFound == 0 ):
        raise ValueError("Network not found in the database")
    elif ( netsFound > 1 ):
        raise ValueError("Multiple networks found in the database.")
    else:
        result = netList[0]

    return(result)
## end get_network_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

