#\L

# 15 Sep 2015   initial creation.

############################################################################
# ISO Address class.

"""A minimal implementation of ISO host/network addreses.

This library is suitable for accepting and verifying ISO formatted
addresses when used to implement an IS-IS routing infrastructure.

"""

class isoaddress():
    """ISO network addresses"""
    # default area should be 49     49 = private.
    # 49.0001.00a0.c96b.c490.00     hex values.
    # ^^                            AFI (one byte)
    #    ^^^^                       area (0 to 12 bytes)
    #         ^^^^^^^^^^^^^^        station id (6 bytes)
    #                        ^^     selector
    __afi = 0
    __area = 0
    __station_id = 0
    __sel = 0

    def __init__(self, iso_addr):
        # AFI (Authority and Format Indicator) 0x49 is private AFI.
        self.__afi = int(iso_addr[:2],16)
        # Remove the AFI and Selector from the address to get the
        # bytes representing the area and station identifiers.
        area_station = iso_addr[3:-3].split('.')
        # Area is a variable length of 0 to 12 bytes.
        if ( len(area_station) < 3 ):
            raise ValueError("Station Identifier is less than 6 bytes.")
        elif ( len(area_station) > 9 ):
            raise ValueError("Area Identifier is greater than 12 bytes.")
        elif ( len(area_station) == 3 ):
            self.__area = None
            self.__station_id = int(''.join(area_station),16)
        else:
            self.__area = int(''.join(area_station[:len(area_station)-3]),16)
            # Station ID is always 6 bytes, each hex word separated by periods.
            self.__station_id = int(''.join(area_station[-3:]),16)
        if ( self.__station_id == 0 ):
            raise ValueError("Station Identifier cannot be 0")
        # Selector is the last byte/two nibbles.
        self.__sel__ = int(iso_addr[-2:],16)

    def iso_address(self):
        # AFI is always one byte.
        afi = hex(self.__afi)[2:].zfill(2)
        # Area can be ZERO up to 12 bytes! typically 2.
        if ( self.__area <= 255 ):   # 1 byte
            area = area.zfill(hex(self.__area)[2:])
        else:
            area = __add_periods(hex(self.__area)[2:])
        # Station is always 6 bytes.
        station = __add_periods(hex(self.__station_id)[2:].zfill(12))
        # Selector is always 1 byte.
        selector = hex(self.__sel)[2:].zfill(2)
        # Put them all together...
        return(afi + "." + area + "." + station + "." + selector)

    def str(self):
        return(iso_address(self))

    def get_mixed_type_key(self):
        """Return a key suitable for sorting between addresses.
        """
        if isinstance(self, isoaddress):
            return(self.iso_address_exploded())

    def iso_address_exploded(self):
        # Produces the full length address suitable for sorting.
        # AFI is always one byte.
        afi = hex(self.__afi)[2:].zfill(2)
        # Area is maximum 12 bytes so make it so.
        area = hex(self.__area)[2:].zfill(24)
        # Station is always 6 bytes.
        station = __add_periods(hex(self.__station_id)[2:].zfill(12))
        # Selector is always 1 byte.
        selector = hex(self.__sel)[2:].zfill(2)
        # Put them all together...
        return(afi + "." + area + "." + station + "." + selector)

    def compare_networks(self,other):
        left = get_mixed_type_key(self)
        right = get_mixed_type_key(other)
        if ( left == right ):
            result = 0
        elif ( left < right ):
            result = -1
        else:
            result = 1
        return(result)

    def is_private(self):
        if ( self.__afi == int("49",16) ):
            return(true)
        else:
            return(false)
        
    def version(self):
        return(1)

    def area(self):
        return(self.__area)

    def __add_periods(iso_addr_string):
        # Must have an even number of nibbles.
        if ( len(iso_addr_string) % 2 == 1 ):
            addr_string = "0" + iso_addr_string
        else:
            addr_string = iso_addr_string
        # Form string from right to left, inserting periods as we go.
        result = addr_string[-4:]
        for i in list(range(len(addr_string),4,-4)):
            ni = i * -1
            if ( i % 4 == 2 ):
                nie = ni + 2
            else:
                nie = ni + 4
            result = addr_string[ni:nie] + "." + result
        return(result)

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

