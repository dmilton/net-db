#\L

# 10 Jun 2014   Kludge around the net_name/name column rename until
#               the change reaches production.
#  7 Nov 2013   initial creation.

############################################################################
# list_networks
# Entry:
#    bldgCode - the building code to list networks for.
# Exit:
#    the list of networks allocated to the building has been printed.

def list_networks(bldgCode):
    # Get our list of networks in the building.
    netList = get_networks(bldgCode)

    # Change any values we want for output formatting.
    for aNetwork in netList:
        # Mark primnet so it shows on output.
        if ( 'prime_net' in aNetwork ):
            if ( aNetwork['prime_net'] ):
                aNetwork['vlan_tag'] = str(aNetwork['vlan_tag']) + '*'
        theDate = str(aNetwork['assign_date'])[0:10]
        try:
            theUser = aNetwork['assign_by'].strip()
        except:
            theUser = 'netmon'
        aNetwork['assign_date'] = theUser + ":" + theDate

    # Print our table.
    netTitle = "Networks Allocated:"
    netHead = ("Name", "VLAN", "Network", "Type", "Assigned")
    netKeys = ("net_name", "vlan_tag", "network", "net_type", "assign_date")
    from cmd_output import print_table
    print_table(netTitle, netHead, netKeys, netList)

    return()
## end list_networks():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

