#\L

#  2 Sep 2014   initial creation.

############################################################################
# _set_iso_info
# If this is an ISO network then set the appropriate table and other
# fixed values which are expected in a network info dictionary.
#
# Entry:
#        netToSet - network dictionary to set info in.
#
# Exit:
#       The vlan_tag and table have been set.
#       fields have been set to force them to default values.
#        
def _set_iso_info(netToSet, typeInfo):
    global _networkISO

    result = {}
    # Copy our input network.
    for aKey in list(netToSet.keys()):
        result[aKey] = netToSet[aKey]

    if ( typeInfo['net_type'] == "iso" ):
        # Add constants not stored in the database but required for display.
        result['net_type'] = typeInfo['net_type']
        result['vlan_tag'] = 1
        result['network_table'] = _networkISO['table']

    return(result)
## end _set_iso_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

