#
# Network database table handlers.
#
#
gVersion=''

# 21 May 2014   Add __all__ variable definition so the package can be
#               installed as a directory with files rather than one file.
#               Move network_tables definitions into this file.
# 29 Apr 2014   Add string and cmd_line_io imports.
# 17 Dec 2013   Change network_log import to import only find_who.
# 19 Oct 2013   Add datetime for nicer time stamp output.
# 10 Apr 2013   Add network_log import
#  5 Feb 2013   Add Google IP address (ipaddr) class import.

import datetime
import ipaddress
from string import ascii_letters, digits

from net import db_access
from net.cmd_line_io import print_table
from net.network_error import NetException
from net import network_zones
from net import network_types
from net import buildings

# Flag to track if we have been initialized.
_tablesinitialized = False

#  5 Feb 2014   ISO table cannot be part of _networkTables because the network
#               field is a string, not cidr.
# 17 Oct 2013   Add support for network_iso and build a new _networkTables
#               list which contains all of the tables to search when looking
#               for networks in a building.
# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 26 Apr 2013   Remove type and zone tables as these have been
#               moved off into their own libraries.
# 28 Feb 2013   Update the index fields for various tables.
#  7 Feb 2013   Drop the network view and add the network zap table.
# 15 Nov 2012   Add the network_zones table. Correct typo in the view
#               definition.
#  6 Nov 2012   Update to use new dictionary based db access routines
# 29 Oct 2012   Update so the 'table' key only exists in the tableDetail
#               portion of the table. Then specifically call it tableDetail
#               everywhere else. Now the name has a meaning and is not
#               being used differently in different places.
#  3 May 2012   Update network-assigns table for new structure. Remove
#               the networkDetail and networkVlans tables as they are now
#               integrated into the network assigns table and/or children.
#  2 Mar 2012   Add network-free table as another child to network-assigns.
# 24 Feb 2012   Add the network-assigns table and children. Update to
#               reflect the current table definitions.
# 12 Sep 2011   Change the 'number' field to 'vlan_tag' to
#               more accurately    describe what the field means.
# 27 Sep 2010   Add the component table information to the
#               networks view for more generic db_access use.
# 17 Sep 2010   Remove the 'type' field from the table definitions.
# 31 Aug 2010   sensor_location table 'sensor' field needs to permit
#               changes, 'name' needs to deny changes as it is the
#               key field.
# 18 Aug 2010   Initial creation.

###############################################################################
# Network Endpoint Table
# This table contains building codes that are valid endpoints.
# The priority is used to identify prescendence when a link has an endpoint
# at both ends - highest priority gets assigned to bldg_code.

_networkEndpoints = {
    'database': 'networking',
    'table': 'network_endpoints',
    'role': 'netmon',
    'index': 'ep_code'
}

###############################################################################
# ISO networks table
# This table holds a record of any building that has an ISO network assigned.
# The purpose of this is to track and allow use of IS-IS.

_networkISO = {
    'database': 'networking',
    'table': 'network_iso',
    'role': 'netmon',
    'index': 'name'
}

###############################################################################
# Network Assignment Table
# This is a parent table and should be considered read only.

_networkAssigns = {
    'database': 'networking',
    'table': 'network_assigns',
    'role': 'netview',
    'index': 'name'
}

###############################################################################
# Network Table List
# This is a list of all the network table information where the networks
# for various uses can be stored.

_networkTables = []
# As network tables are defined below, append them to this list.

###############################################################################
# Network Building Assignment Table
# This is a child table to the network_assigns table and is the table
# which contains all end user/building network address spaces.
#

_networkBuildings = {
    'database': 'networking',
    'table': 'network_bldgs',
    'role': 'netmon',
    'index': 'name'
}

_networkTables.append(_networkBuildings)


###############################################################################
# Network Links Assignment Table
# This is a child table to the network_assigns table and is the table
# which contains all links used between network devices.
#

_networkLinks = {
    'database': 'networking',
    'table': 'network_links',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkLinks)

###############################################################################
# Network Externals table
# This is a child table to the network_assigns table and is for networks
# that are assigned to external groups. We route them but have no real
# control over the address space itself. Rez is an example, as are the
# networks in GlenLea, Straw Bale, etc.

_networkExternals = {
    'database': 'networking',
    'table': 'network_extern',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkExternals)

###############################################################################
# Free Network Table
# This is a child table to the network_assigns table and is the table
# which contains all networks that are currently unallocated.
#

_networkFree = {
    'database': 'networking',
    'table': 'network_free',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkFree)

###############################################################################
# Free Zap Table
# This table holds a record of any network which has been deleted from
# one of the above active tables. This is an audit trail table.
#

_networkZap = {
    'database': 'networking',
    'table': 'network_zap',
    'role': 'netmon',
    'index': 'id'
}

__all__ = [
    "initialize",
	"new_template",
	"get_networks",
	"get_free_networks",
	"get_needed_types",
	"get_networks_within",
	"get_network_info",
	"_get_networks",
	"set_default_info",
	"get_mgmt_info",
	"find_network",
	"get_v4_networks",
	"get_v6_networks",
	"calc_iso_network",
	"calc_v6_loop",
	"calc_v6_links",
	"name_network",
	"_check_network",
	"get_endpoints",
	"get_endpoint_info",
	"get_next_vlan",
	"_net_in_table",
	"get_network_fields",
	"add_network",
	"update_network",
	"set_prefix_len",
	"assign_network",
	"delete_network",
	"print_network",
	"change_bldgs",
	"free_network",
	"calc_network",
	"get_net_index",
	"add_vlan",
	"update_vlan",
	"get_vlan_info",
	"get_endpoint_fields",
	"new_endpoint",
	"add_endpoint",
	"update_endpoint",
	"delete_endpoint",
	"list_networks"
    ]

from . import _check_network
from . import _get_networks
from . import _net_in_table
from . import add_endpoint
from . import add_network
from . import add_vlan
from . import assign_network
from . import calc_iso_network
from . import calc_network
from . import calc_v6_links
from . import calc_v6_loop
from . import change_bldgs
from . import delete_endpoint
from . import delete_network
from . import find_network
from . import free_network
from . import get_endpoint_fields
from . import get_endpoint_info
from . import get_endpoints
from . import get_free_networks
from . import get_mgmt_info
from . import get_needed_types
from . import get_net_index
from . import get_network_fields
from . import get_network_info
from . import get_networks
from . import get_networks_within
from . import get_next_vlan
from . import get_v4_networks
from . import get_v6_networks
from . import get_vlan_info
from . import initialize
from . import list_networks
from . import name_network
from . import new_endpoint
from . import new_template
from . import print_network
from . import set_default_info
from . import set_prefix_len
from . import update_endpoint
from . import update_network
from . import update_vlan

# vim: syntax=python ts=4 sw=4 showmatch et :

