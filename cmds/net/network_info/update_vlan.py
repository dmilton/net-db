#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 13 Jun 2013   initial creation.

############################################################################
# update_vlan
# Update a vlan in the network table.
# Entry:
#       vlanInfo - the information to update.
#       typeInfo - the type information to use for the updated vlan.
# Exit:
#       the vlan has been updated.

def update_vlan(vlanInfo, typeInfo):

    try:
        theTable = _net_in_table(vlanInfo)
    except NetException as errInfo:
        if ( errInfo.name == "TableUnknown" ):
            raise ValueError("the vlan " + vlanInfo['net_name'] + \
                " specifies an invalid update table:" + activeTable)

    # Update the vlan with new values.
    myWhere = "bldg_code = '" + vlanInfo['bldg_code'] + "'"
    myWhere += " AND vlan_tag = " + str(vlanInfo['vlan_tag'])
    result = db_access.set_table_row(theTable, vlanInfo, myWhere)

    # Result is a list, fix the network_table for each one.
    for aNet in result:
        aNet['network_table'] = theTable['table']

    return(result)
## end update_vlan()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

