#\L

# 17 Oct 2013    initial creation.

############################################################################
# new_endpoint
# Entry:    none
# Exit:
#    a dictionary containing the fields necessary for creation of a new
#    network endpoint in the database.

def new_endpoint():
    global _networkEndpoints

    result = db_access.new_table_row(_networkEndpoints)

    return(result)
## end new_endpoint()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

