#\L

# 28 Aug 2014   initial creation.

############################################################################
# _set_assign_info
# Entry:
#        netToSet - network dictionary to set info in.
#
# Exit:
#       the network has been adjusted and the user and timestamp
#       fields have been set to force them to default values.
#        
def _set_assign_info(netToSet):

    # Regardless of what happens, always force the user field to
    # be that of the current user and default the assign date to now.
    if ( "assign_by" in netToSet ):
        netToSet['assign_by'] = getuser()
    if ( "assign_date" in netToSet ):
        netToSet['assign_date'] = "now()"
    if ( "free_date" in netToSet ):
        netToSet['free_date'] = "now()"

    return(netToSet)
## end _set_assign_info()

