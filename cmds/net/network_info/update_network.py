#\L

# 28 Apr 2015   Trap duplicate key constraint errors and handle cleanly.
#  4 Jun 2014   Update for new column names which identify table somewhat.
#  5 Feb 2014   When changing the network (or mask) the where clause must
#               be based on the network name. Similar issues exist when
#               updating the name, must use the network. Both must be unique
#               so there isn't an issue in that regard but I don't like
#               the idea of changing the carpet you are standing on.
# 19 Dec 2013   The changed _check_network syntax contained a reference
#               to newNet rather than netInfo.
# 17 Dec 2013   Correct for ISO networks not having a type or vlan tag
#               stored in the database table.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network in set-net-info.
# 24 Oct 2013   For consistency with add_network, perform a _check_network
#               here before doing anything.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 10 Jun 2013   Drop _check_network call to avoid duplication. Assumption is
#               this routine will be called with valid information.
# 13 May 2013   Check for a current table of 'None' which is what happens
#               when assigning a private network.
#  2 May 2013   This should not return a list, only one record should
#               ever be updated by this command.
# 18 Apr 2013   initial creation.

############################################################################
# update_network
# Update a network in the database.
# Entry:
#       netInfo - the network to update
#       typeInfo - the network type information for the updated network.
#       netChanging - is the actual network value changing?
# Exit:
#       the network has been updated.

def update_network(netInfo, typeInfo, netChanging=False):

    # Check for the table.
    try:
        theTable = _net_in_table(netInfo)
    except NetException as errInfo:
        if ( errInfo.name == "TableUnknown" ):
            raise ValueError("the network " + netInfo['network'] + \
                " specifies an invalid table:" + netInfo['network_table'])

    # Make sure everything is valid.
    _check_network(netInfo, typeInfo, \
            checkType="update", netChanging=netChanging)

    # Update the network with new values.
    if ( netChanging ):
        # The network itself is changing so use name as the key.
        myWhere = "net_name = '" + netInfo['net_name'] + "'"
    else:
        myWhere = "network = '" + str(netInfo['network']) + "'"

    try:
        updatedNet = db_access.set_table_row(theTable, netInfo, myWhere)[0]
    except psycopg2.IntegrityError as dbError:
        if ( "duplicate key" in dbError.args[1] ):
            raise ValueError(dbError.args)

    # result is directly from the table so preserve that information
    # before returning to the caller.
    updatedNet['network_table'] = theTable['table']
    result = _set_iso_info(updatedNet, typeInfo)

    return(result)
## end update_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

