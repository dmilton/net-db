#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 28 Apr 2014   initial creation.

############################################################################
# calc_v6_loop
# Create a loopback interface network ready to add to the database.
#
# Entry:
#       bldgInfo - the information for this building.
#       zoneInfo - the user address space zone information for the building.
#       loopType - network type information for 'loop'
#       doNotUpdate - flag to indicate if information should be updated.
#
# Exit:
#       a new loopback interface address and assocatied network has
#       been generated and is returned as a result.
#    
def calc_v6_loop(bldgInfo, zoneInfo={}, loopType={}, doNotUpdate=False):

    if ( 'net_type' not in loopType ):
        # Get the type information for a link network.
        loopType = network_types.get_type_info('loop')[0]

    if ( 'net_zone' not in zoneInfo ):
        # Get the zone information for link network (or default)
        zoneInfo = network_zones.get_user_zone_info(bldgInfo['net_zone'], 6)

    newLoop = new_template(loopType)
    newLoop['network'] = network_zones.get_next_loop(zoneInfo, doNotUpdate)
    newLoop['bldg_code'] = bldgInfo['bldg_code']
    newLoop['net_type'] = loopType['net_type']
    newLoop['vlan_tag'] = abs(loopType['vlan_tag'])
    newLoop['prime_net'] = False
    newLoop['net_name'] = name_network(newLoop, loopType)

    return(newLoop)
## end def calc_v6_loop():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

