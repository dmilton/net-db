#\L

# 20 Mar 2015   Merge together common elements of range and indexed nets.
# 26 Jan 2015   The main user network could be None so check before
#               calling to calculate networks based upon that address space.
#  4 Nov 2014   Arguments for calc_range_net changed to permit more
#               complete network record results.
# 20 Aug 2014   Rewrite to use the network type assign_method field
#               exclusively for computing new networks or finding
#               existing ones to assign.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Apr 2014   initial creation.

############################################################################
# get_v4_networks
# Entry:
#
#       bldg_info - dictionary of information about the building to
#               assign networks to.
#       bldg_net_detail - dictionary of information about networks in the
#               building. Output of get_bldg_net_detail.
#       update_range - should a range assignment actually update the range
#               to indicate a network has been taken? This allows one to
#               see what would happen without actually allocating the net.
#
# Exit: A list of networks matching the needTypesList will be returned. In
#       the case of link, multiple networks could be returned depending on
#       how the network_endpoints for the zone is constructed; one for every
#       endpoint in that zone will be returned. In the case of ISO, a result
#       could be based on an IPv4 or IPv6 loopback address depending on what
#       already exists in the buiding and what was requested in this call. 
# Exceptions:
#       If a specific type of network could not be found in the database
#       then an exception is raised.


def get_v4_networks(bldg_info, bldg_net_detail, update_range):

    result = []
    net_type_detail = bldg_net_detail['net_type_detail']
    types_to_assign = bldg_net_detail['types_to_assign']
    loop_nets = bldg_net_detail['loop_nets']
    main_user_net = bldg_net_detail['prime_net']

    if ( main_user_net == None ):
        raise NetException("CannotComputeNet",
                "The primary user network must be assigned manually.")

    # At this point we have a set of types to assign (types_to_assign), the
    # main user network (main_user_net), the loopback (loop_nets), and the
    # links (linkNets). This should all have been figured out in
    # get_bldg_net_detail.
    # Go through the type list in assign_priority order and determine
    # those networks which are needed.
    for type_name in net_type_detail['types_by_priority']:
        if ( type_name in types_to_assign ):
            type_info = net_type_detail['type_info_by_name'][type_name]
            if ( type_info['assign_method'] == "manual" ):
                raise NetException("CannotComputeNet", "networks of type " 
                        + type_name + " must be assigned manually." )
            elif ( type_info['assign_method'] in ["next", "nextn"] ):
                try:
                    nets_to_add = find_network(type_info,
                            bldg_info['bldg_code'], bldg_info['net_zone'])
                    result += nets_to_add
                except NetException as the_err:
                    if ( the_err.name == "CannotFind" ):
                        print(the_err.detail)
                    else:
                        raise

                # Record loopback interface networks so ISO can be assigned.
                if ( type_name == "loop" ):
                    loop_nets += nets_to_add
            elif ( type_info['assign_method'] == "calc" ):
                if ( type_name == "iso" ):
                    result.append(calc_iso_network(loop_nets, type_info))
                else:
                    if ( main_user_net != None ):
                        result.append(
                            calc_network(main_user_net, type_info, bldg_info) )
            elif ( type_info['assign_method'] in ["range","index"] ):
                new_net = new_template(type_info)
                new_net['bldg_code'] = bldg_info['bldg_code']
                new_net['vlan_tag'] = type_info['base_vlan_tag']
                if ( type_info['assign_method'] == "range" ):
                    new_net['network'] = network_zones.get_range_net(
                                bldg_info, type_info, 4, update_range)
                elif ( type_info['assign_method'] == "index" ):
                    new_net['network'] = network_zones.get_indexed_net(
                            bldg_info, type_info, 4, update_range) 
                new_net['net_name'] = name_network(new_net, type_info)
                result.append( new_net )
            else:
                raise NetException("UnknownAssignMethod",
                    "Unknown assignment method " + type_info['assign_method'])

    return(result)
## end get_v4_networks()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

