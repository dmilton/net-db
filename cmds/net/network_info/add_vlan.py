#

# 25 Aug 2014   Similar to add_network, add_vlan is always the addition
#               of something new. Use the network_types call to figure out
#               where the vlan should be added.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 19 Dec 2013   _check_network call was referenceing netToADd rather than
#               using vlanInfo.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network.
#  7 Nov 2013   Checks to find the correct insert table fail because the
#               type (bridge) is not mapping to the appropriate table.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 13 Jun 2013   Initial writing

############################################################################
# add_vlan
# Add a vlan to a building.
# Entry:
#       vlanInfo - the information about the vlan itself.
#       typeInfo - the network type information (usually 'extern')
# Exit:
#       The specified vlan has been added to the appropriate table.

def add_vlan(vlanInfo, typeInfo):
    """
    #Add the specified vlan to the networking database.
    typeInfo = network_types.get_type_info('bridge')
    vlanInfo = networks.new_template(typeInfo)
    vlanInfo['net_name'] = "test-vlan"
    vlanInfo['vlan_tag'] = 289
    vlanInfo['bldg_code'] = 'ci'
    vlanInfo['remote_code'] = 'qa'
    # or vlanInfo['remote_code'] = '00'    # parking space.
    # Now add the vlan.
    networks.add_vlan(vlanInfo, typeInfo)
    """

    # Make sure everything we have is valid.
    _check_network(vlanInfo, typeInfo, checkType='add')

    # Get the table that an assigned vlan should be inserted into.
    assignedTableName = network_types.get_type_tables(typeInfo)[0]

    # Convert the name into a table info structure.
    theTable = _get_table_info(assignedTableName)

    # Insert the VLAN into the appropriate table.
    vlanInfo = _set_assign_info(vlanInfo)
    result = db_access.set_table_row(theTable, vlanInfo)[0]

    # Set the table the vlan was added to.
    result['network_table'] = assignedTableName

    return(result)
## end def add_vlan():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

