#\L

# 12 Sep 2014   Change logic for hidden endpoints so hidden endpoints
#               can be extracted for just one network zone.
# 21 Aug 2014   Update column names.
# 28 May 2014   Restrict endpoint list to those with a priority greater
#               than zero so using 0 or -1 can exclude an endpoint from
#               the link assignment process. This permits an ep_code or
#               bldg_code to be in a given zone and have core network
#               attributes but not aggregator attributes.
#  6 Feb 2014   Add detail flag to return list of endpoint records rather
#               than the list of endpoint codes used for establishing links.
# 13 May 2013   initial creation.

############################################################################
# get_endpoints
# Get a list of endpoints in priority order to be used for link assignment.
# Entry:
#        netZone - the zone to get endpoints for.
# Exit:
#        a list of endpoint codes in order of priority is returned.

def get_endpoints(netZone, showDetail=False, showHidden=False):

    global _networkEndpoints

    # If the netZone is < 0 then return all visible endpoints.
    if ( netZone < 0 and not showHidden ):
        # show endpoints for all zones, visible endpoints.
        myWhere =  "core_priority > 0"
    elif ( netZone < 0 and showHidden ):
        # show endpoints for all zones, include hidden endpoints.
        myWhere = ""
    elif ( netZone >= 0 and showHidden ):
        # show endpoints for specific zone, visible endpoints.
        myWhere = "net_zone = " + str(netZone)
    elif ( netZone >= 0 and not showHidden ):
        # show endpoints for specific zone, include hidden endpoints.
        myWhere = "net_zone = " + str(netZone) + " and core_priority > 0"

    # Sort by zone and priority.
    myOrder = "net_zone, core_priority"

    epList = db_access.get_table_rows(_networkEndpoints, myWhere, myOrder)

    # If showDetail is true then return the full endpoint record rather
    # than just the endpoint codes themselves.
    if ( showDetail ):
        result = epList
    else:
        result = []
        for anEndpoint in epList:
            result.append(anEndpoint['ep_code'])

    return(result)
## end get_endpoints()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

