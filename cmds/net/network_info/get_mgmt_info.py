#\L

# 24 Apr 2013   Make response a list which includes both the vlan 2
#               subnet and the router loopback address. This permits
#               finding hybrid building/desktop routers in netdisco.
# 19 Apr 2013   Add the source table to the response.
# 15 Apr 2013   correct typo, new db schema uses network_assigns.
# 26 Feb 2013   consistient naming for address version vs family.
# 29 Jan 2013   initial creation.

############################################################################
# get_mgmt_info
# Entry:
#       bldgCode - the building code to get the management network for.
#       version - find IPv4 or IPv6 network?
# Exit:
#       a query is made against the database and the network address space
#       which is being used for the specified building is returned.

def get_mgmt_info(bldgCode, version=4):
    global _networkBuildings

    myWhere = "bldg_code = '" + bldgCode + \
            "' AND ( vlan_tag = 1 OR vlan_tag = 2 ) AND family(network) = " + \
            str(version)
    try:
        result = db_access.get_table_rows(_networkBuildings, myWhere)
        for aNet in result:
            aNet['network_table'] = 'network_bldgs'
        if ( len(result) == 0 ):
            raise NetException("NoMgmtInfo", \
                "Could not find management information about this building.")
    except:
        raise
    ## end try

    return(result)
## end get_mgmt_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

