#\L

# 10 Dec 2014   Code originally written for merging/supernetting of 
#               multiple networks into one but there are instances where
#               the opposite is desired. Modify to permit either case.
#               Since result always consisted of a list of one network,
#               change to return the single network rather than a list.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 24 Apr 2013   initial creation.

############################################################################
# set_prefix_len
# Update the CIDR mask length of a network.
# Entry:
#        networkInfo - the network as currently found in the database.
#        newPrefix - the prefix to apply to the network.
# Exit: The network prefix has been changed to match newPrefix.

def set_prefix_len(networkInfo, newPrefix):
    # Figure out which table this network is in.
    theTable = _net_in_table(networkInfo)

    # Figure out what the new network will look like
    currentPrefix = networkInfo['network'].prefixlen
    if ( newPrefix > currentPrefix ):
        # the network is getting smaller. In this instance, always use
        # the first network in the subnet list.
        newNetwork = list( networkInfo['network'].subnets(
                            new_prefix=newPrefix) )[0]
    else:
        # The network is getting bigger. This means the base network
        # address could be different. ie 192.168.1.128/25 changing
        # mask to 23 would be 192.168.0.0/23.
        newNetwork = networkInfo['network'].supernet(new_prefix=newPrefix)

    # This is what actually needs to happen to set the mask in postgres.
    # set_masklen('192.168.1.0/24'::cidr, 16)
    networkInfo['network'] = newNetwork

    # Names must be unique and since we are updating the network itself
    # (the primary key) we need to use the name instead.
    myWhere = "net_name = '" + str(networkInfo['net_name']) + "'"

    # Update an existing network with new values.
    result = db_access.set_table_row(theTable, networkInfo, myWhere)[0]

    # set the table entry befor returning.
    result['network_table'] = theTable['table']

    return(result)
## end set_prefix_len()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

