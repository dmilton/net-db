#\L

# 25 Aug 2014   New networks should always be marked as being in
#               network_table 'None'. Removed logic to set the table here
#               as it was confusing other routines.
#  5 Mar 2014   Add check for ISO network table. Add more usage comments.
# 17 Oct 2013   Change to use the _networkTables list so additional types
#               of networks (and tables) do not need updates here.
# 15 Oct 2013   If the type is provided, set that value in the dictionary.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 21 Jun 2013   Allow a string type name in addition to a type dictionary.
#  9 May 2013   When a type is not passed, the free table is returned
#               as the default but that was not being set in the result.
#  2 May 2013   Need the network_table field added to a template so
#               we are assured it will be there for add/update/assign.
# 30 Apr 2013   Change name to be consistient with other libraries.
# 21 Apr 2103   update to add network type. This way the record produced
#               contains all fields for that particular type.
# 14 Feb 2013   initial creation.

############################################################################
# new_network
# Create a new network template based on the network type information
# provided.
#
# Entry:
#   netTypeInfo - typeInfo dictionary of type to create.
# Exit:
#   a dictionary containing the fields necessary for creation of a new
#   network entry in the database.
#
#   NOTE: This routine assumes the network is going to be a new network
#   that does not currently exist in any table of the database.
#

def new_network(netTypeInfo):

    # Figure out which table this type belongs in when active.
    netTableName = network_types.get_type_tables(netTypeInfo)[0]

    # Convert the table name into a table structure.
    netTableInfo = _get_table_info(netTableName)

    # Now figure out what fields this table contains.
    result = db_access.new_table_row(netTableInfo)

    # Set the type
    result['net_type'] = netTypeInfo['net_type']

    # New networks are not in any table. Note that when making
    # a copy of a network structure the network_table value must
    # also be copied.
    result['network_table'] = None

    return(result)
## end new_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

