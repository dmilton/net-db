#

# 17 Sep 2014   When deleting a vlan (set-net-info --delete) the
#               where clause must be constructed using net_name.
# 20 Aug 2014   Test for network type was wrong; was testing family
#               rather than the type. Add the netFamily argument so
#               a copy of ip family networks can be made in the zap table.
# 24 Apr 2014   When deleting iso networks the insert into the zap table
#               causes problems. Only insert ip family in the zap table.
# 10 Apr 2013   Remove network_log import, this is done globally.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
#  4 Feb 2012   initial creation

############################################################################
# delete_network
# Given a network dictionary, delete the network from the database.
# Entry:
#       theNetwork - the network dictionary to delete.
#       netFamily - ip, ipv4, or ipv6 - the network type family.
# Exit: the network has been deleted from the database.


def delete_network(theNetwork, netFamily):
    global _networkZap

    # copy all fields of the network into the network_zap table
    # so we could restore it if required. Note that we should
    # preserve the assign by, assing date, and free date values
    # should they exist in theNetwork.
    # At the moment, only IP networks can be restored; there's no
    # facility to restore an ISO network.
    if ( netFamily in ['ip','ipv4','ipv6'] ):
        # Make a copy of the network in the network_zap table.
        if ( "zap_date" in theNetwork ):
            del theNetwork['zap_date']
        theNetwork['zap_by'] = getuser()
        db_access.set_table_row(_networkZap, theNetwork)
    
    # If this raises an exception we have serious problems!
    tableInfo = _net_in_table(theNetwork)

    if ( theNetwork['network'] == None ):
        # deleting a vlan
        myWhere = "net_name = '" + theNetwork['net_name'] + "'"
    else:
        myWhere = "network = '" + str(theNetwork['network']) + "'"
    db_access.delete_table_rows(tableInfo, myWhere)

    return
## end def delete_network():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

