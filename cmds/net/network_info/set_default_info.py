#\L

# 18 Aug 2014   Type info vlan tag column renamed to base_vlan_tag.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 13 May 2013   vlan tags in the network_types table can now be
#               negative to represent network types that are unique
#               to a building. Make sure the absolute value in used
#               when assigning the vlan tag to a network.
# 25 Apr 2013   initial creation.

############################################################################
# set_default_info
# Given a network record, set all values to their normal default as if
# this were a free network.
# Entry:
#        netInfo - the network to set to default values.
#        typeInfo - the type information to use for default values.
# Exit: all values required for a free network have been set to defaults.
def set_default_info(netInfo, typeInfo):

    # Give the default network-bogon name.
    netInfo['net_name'] = name_network(netInfo, typeInfo, bogon=True)

    # Only one IPv4 network per building is prime so assume False.
    # No concept of prime for IPv6.
    netInfo['prime_net'] = False
    # Assign the default catch-all building code for free networks.
    netInfo['bldg_code'] = "00"
    # Set the network type
    netInfo['net_type'] = typeInfo['net_type']
    # set the default vlan_tag for this network type.
    netInfo['vlan_tag'] = abs(typeInfo['base_vlan_tag'])

    # Clear the assign by since this one has not been assigned.
    netInfo['assign_by'] = ""
    netInfo['assign_date'] = "-infinity"

    return(netInfo)
## end set_default_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

