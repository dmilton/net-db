#\L

# 21 May 2014   initial creation.

############################################################################
# change_bldgs
# Update all networks in a building to be a new building.
# Both the old and new building code must already be defined. If the
# building code is an endpoint, those entries will be updated as well.
# Entry:
#       old_bldg_code - the building to change from.
#       new_bldg_code - the building to change to.
# Exit:
#       all networks assigned to old_bldg_code are now assigned
#       to new_bldg_code.

def change_bldgs(old_bldg_code, new_bldg_code):
    # The tables we will change.
    global _networkTables
    global _networkISO, _networkEndpoints, _networkLinks, _networkExtern

    updated_networks = 0

    # Start with the allocation tables.
    for a_net_table in _networkTables:
        updated_networks += db_access.change_table_rows(
                a_net_table, 'bldg_code', old_bldg_code, new_bldg_code, True)

    # Update any ISO networks.
    updated_networks += db_access.change_table_rows(
            _networkISO, 'bldg_code', old_bldg_code, new_bldg_code, True)

    # Now get links, endpoints, and links.
    myWhere = "ep_code = '" + old_bldg_code + "'"
    oldEp = get_endpoint_info(old_bldg_code)
    myWhere = "ep_code = '" + new_bldg_code + "'"
    new_ep_count = db_access.get_row_count(_networkEndpoints, myWhere)
    if ( new_ep_count == 0 ):
        # must have an endpoint to move links to.
        newEp = {}
        for aKey in oldEp.keys():
            newEp[aKey] = oldEp[aKey]
        newEp['ep_code'] = new_bldg_code
    # Need to ensure the new_bldg_code exists as an endpoint.
    updated_networks += db_access.change_table_rows(
            _networkEndpoints, 'ep_code', old_bldg_code, new_bldg_code, True)
    updated_networks += db_access.change_table_rows(
            _networkLinks, 'ep_code', old_bldg_code, new_bldg_code, True)
    updated_networks += db_access.change_table_rows(
            _networkExtern, 'remote_code', old_bldg_code, new_bldg_code, True)

    return(updated_networks)
## end change_bldgs()

# vim: syntax=python ts=4 sw=4 showmatch et :

