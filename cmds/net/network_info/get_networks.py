#

# 20 May 2015   Change sort so sorting by IP address actually works.
# 22 Jan 2014   ISO networks now in their own table, remove the argument
#               to generate an ISO network.
# 17 Oct 2013   Add a sort to the end since the multi-table approach
#               may result in a table not sorted by vlan_tag and network.
# 25 Apr 2013   Extract common code and move to _get_networks.
# 19 Apr 2013   Add the source table to the dictionary for each network.
# 17 Apr 2013   Add code to generate the ISO network when an IPv6
#               loopback address exists.
# 13 Feb 2013   Rewrite to use the new table structure.
# 15 Nov 2012   Rather than using the parent table (assigns) use the
#               child tables so we get all the information.
#  6 Nov 2012   Rewrite to use db_access routines that return dictionaries.
# 29 Oct 2012   Initial writing for v2 table format.

############################################################################
# GetNetworks
#
# Input:
#        theCode - the building code to get the list of networks for.
# Output: list of network dictionaries for all networks in the building.
# Exception: NoNetworksDefined
#

def get_networks(theCode):
    """
    Return the active networks for a specific building code.
    """

    sortOrder = "vlan_tag, network"
    result = _get_networks("bldgCode", theCode, sortOrder, withFree=False)

    # Raise an exception if there isn't anything in our result.
    if ( len(result) == 0 ):
        raise NetException("NoNetworksDefined", \
                "No networks were found allocated to building code " + theCode)
    ## if ( len(result) == 0 ):

    # Sort the list by vlan tag and network since the multi-table approach
    # may give results which are not ordered by vlan_tag and network.
    result = sorted( result , key=lambda elem: "%04d" % elem['vlan_tag'] )
    # The sort fails when a "bridge" network type is encountered.
    #result = sorted( result ,
    #        key=lambda item: ipaddress.get_mixed_type_key(item['network']) )

    return(result)
## end get_networks()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

