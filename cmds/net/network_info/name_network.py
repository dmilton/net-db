#

# This whole routine needs to be rewritten. Too many dependencies
# on the network type and variations based on the type. Not enough
# done to ensure the name is unique.

# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 19 Feb 2014   For 2nd networks, sometimes basename is set, sometimes not.
# 18 Feb 2014   Rather than checking for 'prime_net', check the type and
#               force all non-user networks to be lower case.
#               Make sure -v4/-v6 is appended last (2nd networks are wrong)
#  5 Feb 2014   Missing str call for ip version extension.
#  4 Feb 2014   Change name scheme for prime user network. Strip a few
#               characters from the name like quotes, periods, etc.
# 27 Jan 2014   Bogon network name missing str call.
# 23 Oct 2013   Add rules for bogon networks.
# 21 Oct 2013   for 2nd type, accept basename arg.
# 19 Oct 2013   names now contain the type and IP version.
#  1 May 2013   fold names to lower case.

#############################################################################
# name_network
# figure out what to use for the name of a network.
# Entry:
#    netInfo - dictionary of network information.
#    typeInfo - the type info for the network.
#    basename - if provided, use this as the base (for 2nd type networks)
#    bogon - if provided, the name is 'ip-bogon'
# Exit:
#    The name has been determined and is returned as a result.

def name_network(netInfo, typeInfo, basename='', bogon=False):

    # How the name is constructed:
    # var-var2-var3
    # ['bldg_code', "-", "net_type", "-", "2nd"]
    # nameParts = []

    result = ""

    # Figure out the name.
    if ( bogon ):
        try:
            netName = str(netInfo['network'].network_address) + "-bogon"
        except AttributeError:
            netName = str(netInfo['network'].ip) + "-bogon"
        # Now translate periods into hyphens.
        netName = netName.translate(str.maketrans('.','-'))

    elif ( typeInfo['net_type'] == 'loop' ):
        netName = netInfo['bldg_code'] + typeInfo['name_extension']
    elif ( typeInfo['net_type'] == 'iso' ):
        netName = basename.split('-')[0] + typeInfo['name_extension']
    elif ( typeInfo['net_type'] == 'link' ):
        netName = netInfo['ep_code'] + "-" + netInfo['bldg_code'] + "b-link"
    elif ( typeInfo['net_type'] == 'user' ):
        if ( netInfo['prime_net'] == True ):
            netName = netInfo['net_name'].rsplit('-',1)[0]
        else:
            netName = netInfo['bldg_code'] + "-user"
    elif ( typeInfo['net_type'] == '2nd' ):
        if ( netInfo['net_name'] == None ):
            nameBase = basename.rsplit('-',1)[0]
        else:
            nameBase = netInfo['net_name'].rsplit('-',1)[0]
        if ( nameBase == '' ):
            nameBase = netInfo['bldg_code']
        netName = nameBase + typeInfo['name_extension']

    else:
        netName = netInfo['bldg_code'] + typeInfo['name_extension']

    # Add -v4/-v6 on the end. iso causes an error because the network value
    # is a string, not an ipaddr object.
    if ( typeInfo['net_type'] != 'iso' ):
        netName += "-v" + str(netInfo['network'].version)

    # Remove any invalid characters.
    for aChar in list(netName):
        if ( aChar in ascii_letters or aChar in digits or aChar == '-' ):
            result += aChar

    # With the exception of user networks in the building,
    # all network names shoud be lower case.
    if ( typeInfo['net_type'] not in ['user', '2nd'] ):
        result = result.lower()

    return(result)
## end def name_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

