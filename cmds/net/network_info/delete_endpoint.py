#

# 17 Oct 2013   initial creation.

############################################################################
# delete_endpoint
# Given an endpoint code, delete the endpoint from the database.
# Entry:
#        epCode - the endpoint to delete.
# Exit: the endpoint has been deleted from the database.

# 4 Feb 2012    initial creation

def delete_endpoint(epCode):
    global _networkEndpoints

    myWhere = "ep_code = '" + epCode + "'"
    db_access.delete_table_rows(_networkEndpoints, myWhere)

    return()
## end def delete_endpoint():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

