#\L

# 25 Aug 2014   When freeing a private address space, use the python
#               library to determine if the address space is private
#               rather than using the type info flag.
# 20 Aug 2014   When merging networks, the base network is likely already
#               free. Change exception to use NetException so it is easier
#               to handle the expected and normal condition.
# 24 Oct 2013   Use free table from typeInfo rather than hard coded constant.
# 22 May 2013   typeInfo now an explicit argument and an update
#               is performed before moving tables.
# 25 Apr 2013   Update for new table schema.
# 01 May 2012   initial creation.

############################################################################
# free_network
# Return a network to the free pool/table. If the network is one based on
# a calculation then the network is simply deleted frmo the database.
# Entry:
#       netInfo - the network that is to be released.
#       typeInfo - the type information to use for the free network.
#       netZoneID - the zone the network belongs to.
# Exit:
#       the network has been removed from whatever active table it was
#       originally in and placed into the free table if appropriate.
#
def free_network(netInfo, typeInfo, netZoneID):
    global _networkFree

    # Only do something if the current table is not the free table.
    freeTableName = network_types.get_type_tables(typeInfo)[1]

    if ( netInfo['network_table'] == freeTableName ):
        # Trap this so when this is the expected condition it can be handled.
        raise NetException("NetIsFree", "The network "
                + str(netInfo['network']) + " is already free.")


    # Primary key is network so the where is identical whether we are
    # moving into the free table or deleting from the active table.
    myWhere = "network = '" + str(netInfo['network']) + "'"

    # Figure out which table the network is in now.
    srcTableInfo = _net_in_table(netInfo)

    if ( freeTableName in [None, ''] ):
        # No free table so just delete the network.
        db_access.delete_table_rows(srcTableInfo, myWhere)
    else:
        # Get the free table information.
        freeTableInfo = _get_table_info(freeTableName)
        # Move our network to the 'free' table.
        movedNet = db_access.move_table_rows(
                        srcTableInfo, freeTableInfo, myWhere)
        # Set defaults for a free network.
        movedNet = set_default_info(netInfo, typeInfo)
        movedNet['net_zone'] = netZoneID
        # Update the record to match our default values.
        db_access.set_table_row(freeTableInfo, movedNet, myWhere)

    return()
## end free_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

