#

# 25 Aug 2014   Change the table logic entirely. Use the network_types
#               call and base the decision entirely on that. New networks
#               should not exist in any table, proceed on that basis.
# 20 Aug 2014   When adding a network and the table is unknown or not
#               specified, the table chosen was the active rather than
#               the free one.
# 19 Feb 2014   Some networks don't have the assign date field so deleting
#               it causes errors, check and only delete if it exists.
#  5 Feb 2014   Networks were not having their assign date timestap updated.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network.
# 24 Oct 2013   More thorough error checking and generation on the table
#               information.
# 18 Oct 2013   Update documentation.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
# 26 Feb 2013   remove typeid references, now using the name.
# 19 Feb 2013   initial writing.

############################################################################
# add_network
# Add the network specified to the database. Unless otherwise specified
# all networks are added to their appropriate 'active' table.
# Entry:
#    netToAdd - the dictionary of the network to add to the database.
#    typeInfo - the network type information.
#    ignoreDup - flag to indicate if the network is inside an alreday
#               existing network - this happens when splitting networks.
#    addToTable - the table to add the network to, should not be set
#                 by routines outside of the network module.
# Exit: The information provided is merged and verified and if valid
#       the network will be added to the database.

def add_network(netToAdd, typeInfo, ignoreDup=False, addToTable=""):
    """
    Add the specified network to the networking database.
    netToAdd = new_template()
    netToAdd['network'] = "10.0.0.0/8"
    # required for adding assigned links.
    netToAdd['ep_code'] = "code from network_endpoints"
    # required for adding external networks.
    netToAdd['remote_code'] = "00"
    # addToTable needs to be one of the valid network tables
    addedNet = add_network(netToAdd, typeInfo)
    """

    # Check the network, adding a network is always a new network
    # but when splitting a network into subnets we need to avoid
    # checking for 'is contained within' which are 'duplicates'
    if ( ignoreDup ):
        _check_network(netToAdd, typeInfo, checkType='split')
    else:
        _check_network(netToAdd, typeInfo, checkType='add')

    if ( addToTable == "" ):
        # Get the table that an assigned network of this type
        # should be inserted into.
        assignedTableName = network_types.get_type_tables(typeInfo)[0]
    else:
        assignedTableName = addToTable

    # Convert the name into a table info structure.
    theTable = _get_table_info(assignedTableName)
    
    # Insert the new network into the table.
    netToAdd = _set_assign_info(netToAdd)
    addedNet = db_access.set_table_row(theTable, netToAdd)[0]

    # set the table the network came from.
    addedNet['network_table'] = assignedTableName
    result = _set_iso_info(addedNet, typeInfo)

    return(result)
## end def add_network():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

