#\L

#  2 Sep 2014   Fix issues where a network is being updated and assigned
#               at the same time. Moving tables didn't always get the
#               end result it was supposed to.
# 20 Aug 2014   Change the current table logic to avoid issues where the
#               network does not exist in the database.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network.
#  4 Jul 2013   The assign date was not been correctly updated when
#               a network was assigned. Used now() as the value.
# 10 Jun 2013   Must move the network to the destination table before
#               doing the update or not all fields get set. The current
#               table (network_table) field was not getting updated when
#               the record was moved from one table to another.
#  7 Jun 2013   assign_date may not exist in all netInfo dictionaries so
#               check before attempting to delete it.
# 14 May 2013   Change logic so assignment of a private network, which
#               is an Add gets handled properly.
# 22 Apr 2013   initial creation.

############################################################################
# assign_network
# Assign a network to a specific building.
# This routine can accept two basic classes of network to assign:
# - one that does not exist in the database - computed network values mostly.
# - one that is in the free table and need to be moved to the active table.
#
# Entry:
#       netInfo - network detail record for this network.
#       typeInfo - type detail record for the type of network.
#       bldgInfo - information about the building to assign to.
#
# Exit:
#       The network has either been added to the database or moved from
#       the free table into the appropriate allocated table.
#

def assign_network(netInfo, typeInfo, bldgInfo):

    # Figure out the source (current) and  destination (active) tables.
    if ( "network_table" in netInfo ):
        try:
            currentTable = _get_table_info(netInfo['network_table'])
        except NetException as err:
            if ( err.name == "TableUnknown" ):
                currentTable = None
    else:
        currentTable = None
    activeTableName = network_types.get_type_tables(typeInfo)[0]
    activeTable = _get_table_info(activeTableName)

    # Set the building code.
    netInfo['bldg_code'] = bldgInfo['bldg_code']

    # Now add or move and update the network.
    if ( currentTable == None ):

        # Verify what we have.
        _check_network(netInfo, typeInfo, checkType="add")
        # Ensure the network is marked for the updater and time.
        netInfo = _set_assign_info(netInfo)
        # Assign the new network - ie. add directly to the active table.
        assignedNet = add_network(netInfo, typeInfo,
                addToTable=activeTableName)

    elif ( currentTable['table'] == activeTable['table'] ):

        # Network is active. Moving from one building to another?
        # This is problematic if the network type changed and the
        # table needs to change too; values that should get set won't!
        _check_network(netInfo, typeInfo, checkType="update")
        assignedNet = update_network(netInfo, typeInfo)

    else:

        # The network is in the free table, make sure everything is valid.
        _check_network(netInfo, typeInfo, checkType="assign")
        # Construct a where clause for the move.
        myWhere = "network = '" + str(netInfo['network']) + "'"
        # Move the record into the correct table. This must be done so
        # that update_network has all the fields for the type available.
        netInfo = _set_assign_info(netInfo)
        movedNet = db_access.move_table_rows(
                        currentTable, activeTable, myWhere)[0]

        # Moving the network takes an exact copy of what is in the
        # database and moves it from one table to another. The end
        # result is that some fields may now be present (or gone)
        # that were not present (or have appeared) before.
        # Make sure the fields we have in movedNet are replaced with
        # the values we have in netInfo.
        # in fields that need updating.
        for aKey in list(movedNet.keys()):
            if ( aKey in netInfo ):
                movedNet[aKey] = netInfo[aKey]

        # Make sure the correct table is in movedNet.
        movedNet['network_table'] = activeTableName

        # Finally, update the values based on the changes.
        netToAssign = _set_assign_info(movedNet)
        assignedNet = update_network(netToAssign, typeInfo)

    return(assignedNet)
## end assign_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

