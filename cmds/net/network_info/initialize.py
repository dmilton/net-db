#

#  5 Feb 2014   ISO networks must be initialized separately because the
#               network field is a string, not cidr.
# 17 Oct 2013   Add support for ISO networks. Create list of 'networks'
#               tables to allow new network types to be added without
#               having to update this initialization routine.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 13 Feb 2013   initialize all tables but network_zap which doesn't have
#               the correct permissions for netview to read the schema.
# 14 Nov 2012   Changed to prefix global table names with an _

##############################################################################
# initialize()
# Perform initialization process for networks database. 
# Entry: none. initializes internal variables for use.
# Exit: network database fields are now ready to use.

def initialize():
    """
    initialize the networks table access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _networkAssigns, _networkISO, _networkEndpoints, _networkZap
    global _networkTables
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Ensure our dependant tables are initialized.
    building_info.initialize()
    network_types.initialize()
    network_zones.initialize()

    # Table of building codes which can be used as link endpoints.
    _networkEndpoints.update(db_access.init_db_table(_networkEndpoints))

    # Parent table of all network to building assignments.
    _networkAssigns.update(db_access.init_db_table(_networkAssigns))

    # ISO network table is special because ISO networks aren't IP.
    _networkISO.update(db_access.init_db_table(_networkISO))

    # Table with all fields of network_assigns and children.
    # Cannot handle ISO networks.
    _networkZap.update(db_access.init_db_table(_networkZap))

    # initialize all of the network child tables.
    for aTable in _networkTables:
        aTable.update( db_access.init_db_table(aTable) )

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

