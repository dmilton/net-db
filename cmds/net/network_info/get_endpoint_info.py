#\L

# 21 Aug 2014   Update column names.
# 13 May 2013   initial creation.

############################################################################
# get_endpoint_info
# Get a list of endpoints in priority order to be used for link assignment.
# Entry:
#        epCode - the endpoint to get information for.
# Exit:
#        a list of endpoint codes in order of priority is returned.

def get_endpoint_info(epCode):

    global _networkEndpoints

    result = []

    myWhere = "ep_code = '" + str(epCode) + "'"
    myOrder = "core_priority"
    epList = db_access.get_table_rows(_networkEndpoints, myWhere, myOrder)

    if ( len(epList) == 0 ):
        raise NetException("EpNotFound",
                "The endpoint code " + epCode + " was not found.")

    return(epList)
## end get_endpoint_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

