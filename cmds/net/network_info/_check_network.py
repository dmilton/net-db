#\L

#  6 Jan 2015   Link networks in the free table could not be updated
#               unless ep_code was specified. Changed check for ep_code
#               so it is only made when the network is being assigned.
#  4 Sep 2014   ISO network updates were failing on a duplicate name.
#  2 Sep 2014   Move the check for dictionary values into a generic
#               routine that can check any value. Removed some almost
#               identical code which varied only in the dictionary key.
# 29 Aug 2014   Check for links is problematic. Need to ensure there is
#               an endpoint code when it is assigned but the network
#               originates from the free table where the endpoint code
#               doesn't exist. The assumption that there will always be
#               an endpoint code is therefore wrong. The type alone does
#               not permit updates while the link is free. The table
#               alone presents a similar problem. Changed the check done
#               logic to allow and add, followed by an update to perform
#               a different set of tests. This may duplicate some tests
#               but ensures completeness. Now have a new check type of
#               assign.
# 18 Aug 2014   Type table column name changes.
# 16 Jun 2014   More updates, missed column name changes.
#  4 Jun 2014   Update for new column names which identify table somewhat.
#  4 Mar 2014   For IPv6, the tables must be checked so a return after
#               checking for the prime network flag wasn't the right
#               way to skip the unique vlan flag test.
#  5 Feb 2014   When updating the network field, checking for containing
#               and contained by present problmes. Instead, check to see
#               if the count is 1 and that should be okay, we should be
#               updating that network anyway.
# 24 Jan 2014   Check to be sure an update where not changing the vlan value
#                does not cause failure.
# 16 Dec 2013   Tests fit into three categories: add, update, and split
#               and vary for bridge and iso networks. Split into sections
#               to account for the type and category differences.
#  7 Nov 2013   Add checks for bridge type networks where no address space
#               has been allocated.
#  5 Nov 2013   Correct an exception to indicate the external vs alien.
# 24 Oct 2013   Add handling of network_iso table. Add a new entry to
#               the network dictionary to indicate a check has been
#               performed. assign_network moves networks from the free
#               table to the 'assigned' table but needs to check before
#               calling update_network which does a check too. Add a check
#               for new networks to verify not null fields have values.
# 17 Oct 2013   Split net with type argument creates multiple networks
#               of the same type in the same building. Since this is
#               the desired effect (free network building code) a check
#               to see that this is unique within the building is invalid.
#               When inserting into the network_free table, checking for
#               values in extern or link tables should not be done.
# 16 Oct 2013   Rename external networks table.
#  4 Jul 2013   Schema changed and prime_net only exists as a field in
#               the network_bldgs table. Only perform those checks when
#               the field is present.
# 10 Jun 2013   Provide more information when ep_code is invalid.
#  2 May 2013   Add code to check for additional fields based on
#               the network type info.
# 22 Apr 2013   add code to check whether the new network is contained
#               within an already existing network. Update to use the
#               new name for a network type 'net_type' instead of name.
# 19 Apr 2013   a result is no longer required. the network_table key
#               inserted by _get_networks solves those issues.
# 18 Apr 2013   initial creation.

############################################################################
# _check_network
# Entry:
#        netToCheck - network dictionary to check.
#        typeInfo - the type information for this network.
#        checkType - add, update, assign, split.
#        netChanging - is the network field itself changing?
# Exit: The network has been checked and verified to ensure there
#        aren't any problems with what has been provided.
#        If any problems exist an exception is raised.
#        
def _check_network(netToCheck, typeInfo, checkType, netChanging=False):
    global _networkAssigns, _networkISO, _networkBuildings
    global _networkLinks, _networkExternals

    if ( "net_check_done" in netToCheck ):
        if ( netToCheck['net_check_done'] == checkType ):
            return

    netToCheck['net_check_done'] = checkType

    # Make sure all not null fields have values.
    activeTableName = network_types.get_type_tables(typeInfo)[0]
    activeTable = _get_table_info(activeTableName)
    if ( checkType in ["add", "update", "assign"] ):
        reqdCols = db_access.get_reqd_cols(activeTable)
        for aColumn in reqdCols:
            goodValue = True
            if ( aColumn in netToCheck ):
                if ( netToCheck[aColumn] == None ):
                    goodValue = False
                if ( type(netToCheck[aColumn]) is str ):
                    if ( netToCheck[aColumn] == '' ):
                        goodValue = False
            else:
                goodValue = False
            if ( goodValue == False ):
                raise ValueError("The network must contain a value for " \
                        + aColumn)

    if ( typeInfo['net_type'] in ["iso","bridge"] ):
        if ( typeInfo['net_type'] == "iso" ):
            # Table for ISO networks.
            vlanCheckTable = _networkISO
        else:
            # Note this is the parent of all network assignment
            # tables except ISO.
            vlanCheckTable = _networkAssigns

        if ( checkType == "add" ):
            # Add requires everything to be unique.
            _check_name(netToCheck['net_name'], _networkISO)
            _check_name(netToCheck['net_name'], _networkAssigns)
            _check_net_equal(netToCheck['network'], _networkISO)
            _check_vlan_unique(
                    netToCheck['network'], typeInfo['net_type'],
                    netToCheck['bldg_code'], typeInfo['base_vlan_tag'], 
                    vlanCheckTable)
            _check_prime(netToCheck)
        elif ( checkType == "update" ):
            if ( netChanging ):
                # The name already exists so a check will fail.
                # The network needs to be unique.
                _check_net_equal(netToCheck['network'], _networkISO)
                _check_name(netToCheck['network'], _networkAssigns)
            else:
                # The network already exists, just check name.
                _check_name(netToCheck['net_name'], _networkISO)
                _check_name(netToCheck['net_name'], _networkAssigns)
        return

    if ( checkType in ["add","split"] ):
        # is the network unique? Assigns are duplicates briefly since
        # they move from the 'free' table to the 'active' table.
        _check_net_equal(netToCheck['network'], _networkAssigns)

    if ( checkType in ["add","assign","update"] ):
        # For updates, make sure we don't prevent an update of the
        # mask length on a network.
        # Make sure this network is not inside an existing network.
        myWhere = "network >> '" + str(netToCheck['network']) + "'"
        netCount = db_access.get_row_count(_networkAssigns, myWhere)
        if ( (netCount > 0 and checkType == "add") or \
                (netCount > 1 and netChanging and 
                    checkType in ["assign","update"]) ):
            raise ValueError("A network containing " + \
                    str(netToCheck['network']) + " already exists.")

        # Make sure this network doesn't contain other networks.
        myWhere = "network << '" + str(netToCheck['network']) + "'"
        netCount = db_access.get_row_count(_networkAssigns, myWhere)
        if ( (netCount > 0 and checkType == "add") or \
                ( netCount > 1 and netChanging and 
                    checkType in ["assign","update"] ) ):
            raise ValueError("Networks contained by " + \
                str(netToCheck['network']) + " exist.")

    if ( netToCheck['network'].version == 6 ):
        _check_prime(netToCheck)
    else:
        # For IPv4 networks, Make sure the vlan is unique when appropriate.
        if ( typeInfo['base_vlan_tag'] < 0 and checkType != 'update' ):
            # This doesn't really cover splits but those are in the free table.
            _check_vlan_unique(netToCheck['network'],
                    typeInfo['net_type'], netToCheck['bldg_code'],
                    typeInfo['base_vlan_tag'], _networkBuildings)

    #
    # Based on the type and the table to insert into, make sure we have
    # all the information.
    #
    insertTableName = activeTableName
    if ( "network_table" in netToCheck ):
        if ( netToCheck['network_table'] != None ):
            insertTableName = netToCheck['network_table']

    if ( insertTableName == _networkExternals['table'] ):

        # Make sure the remote code is set for external networks.
        _check_key_present("remote_code", netToCheck)

    elif ( insertTableName == _networkLinks['table']
            or (netToCheck['net_type'] == "link" and checkType == "assign") ):

        # Make sure the ep code is set for link networks.
        _check_key_present("ep_code", netToCheck)

        # Is this a valid endpoint code?
        epWhere = "ep_code = '" + netToCheck['ep_code'] + "'"
        epCount = db_access.get_row_count(_networkEndpoints, epWhere)
        if ( epCount == 0 ):
            raise ValueError("The endpoint code '" + netToCheck['ep_code'] \
                    + "' is not valid.")

    return()
## end _check_network()

def _check_key_present(theKey, netToCheck):

    # Check the key is present and has a value.
    keyIsMissing = False
    if ( theKey not in netToCheck ):
        keyIsMissing = True
    elif ( netToCheck[theKey] == None ):
        keyIsMissing = True
    elif ( type(netToCheck[theKey]) is str ):
        if ( len(netToCheck[theKey]) == 0 ):
            keyIsMissing = True

    if ( keyIsMissing ):
        raise ValueError("Networks of type " + netToCheck['net_type'] +
            "require a value for " + theKey + ".")

## end _check_key_present()

def _check_name(nameToCheck, tableToCheck):
    # is the name unique?
    myWhere = "net_name = '" + nameToCheck + "'"
    nameCount = db_access.get_row_count(tableToCheck, myWhere)
    if ( nameCount != 0 ):
        raise ValueError("A network named " + nameToCheck + " already exists.")
## end _check_name()

def _check_net_equal(theNet, tableToCheck):
    # is the network unique?
    myWhere = "network = '" + str(theNet) + "'"
    netCount = db_access.get_row_count(tableToCheck, myWhere)
    if ( netCount != 0 ):
        raise ValueError("The network " + str(theNet) + " already exists.")
## end _check_net_equal()

# Check that the vlan (on network type) is unique in the building.
def _check_vlan_unique(theNet, netType, theBldg, theVlan, tableToCheck):
        myWhere = "bldg_code = '" + theBldg + "'"
        if ( netType != "iso" ):
            myWhere += " AND vlan_tag = " + str(abs(theVlan))
        if ( netType not in ["iso","bridge"] ):
            myWhere += "AND family(network) = " + str(theNet.version)
            message = "An IPv" + str(theNet.version)
        else:
            message = "A"

        # Extract the existing record if it exists.
        dbNetInfo = db_access.get_table_rows(tableToCheck, myWhere)
        if ( len(dbNetInfo) != 0 ):
            # The network already exists in the db so update allowed.
            if ( dbNetInfo[0]['network'] == theNet ):
                # The network isn't changing, neither is the vlan_tag
                # so the entry will remain unique.
                return
            else:
                if ( netType != "iso" ):
                    message += " network with the vlan tag " + str(abs(theVlan))
                else:
                    message += "n ISO network"
                message += " already exists in the building."
                raise ValueError(message)

## end _check_vlan_unique()

def _check_prime(netToCheck):

    global _networkBuildings
    if ( "prime_net" in netToCheck ):
        if ( netToCheck['prime_net'] ):
            # Only IPv4 networks of type 'user' can be prime.
            if ( netToCheck['net_type'] != "user" or
                    netToCheck['network'].version == 6 ):
                raise ValueError("A network of type " + \
                    netToCheck['net_type'] + " cannot be the prime network.")

            # The prime network must be unique within the building.
            myWhere = "bldg_code = '" + str(netToCheck['bldg_code']) + \
                "' AND  prime_net = 'T'"
            netCount = db_access.get_row_count(_networkBuildings, myWhere)
            if ( netCount != 0 ):
                raise ValueError(\
                    "There can only be one prime network in a building.")

## end _check_prime()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
