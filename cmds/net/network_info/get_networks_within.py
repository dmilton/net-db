#

# 20 May 2015   Fix cidrContains vs cidrContainedBy.
# 22 Jan 2014   ISO networks now reside in a table, don't generate them.
# 25 Apr 2013   Allow matches that are equal as well as being contained
#               within or containing the network provided.
# 23 Apr 2013   Add str() wrapper for the network since we may receive
#               an ipaddr type rather than a string.
#  5 Mar 2013   update documentation, remove networks view.
#               Update to use new database schema.
#               Add optional argument 'reverse' to permit seraching
#               for a network which contains cidrNetwork instead of
#               searching for networks contained by cidrNetwork.
#  4 Feb 2013   Initial writing

############################################################################
# get_networks_within
#
# Input: cidrNetwork - cidr value to find all networks contained within.
# Output: list of network dictionaries for all networks in the building.
# Exception: NoNetworksDefined
#

def get_networks_within(cidrNetwork, reverse=False):
    """
    Return the active networks for a specific building code.
    """
    global _networkBuildings, _networkAliens, _networkLinks, _networkFree

    # restrict our request and order by vlan id and network.
    if ( reverse == False ):
        findKey = "cidrContainedBy"
    else:
        findKey = "cidrContains"

    sortOrder = "network"

    result = _get_networks(findKey, str(cidrNetwork), sortOrder, withFree=True)

    # Raise an exception if there isn't anything in our result.
    if ( len(result) == 0 ):
        raise NetException("NetNotFound", \
            "the network or address " + str(cidrNetwork) + " was not found")
    ## if ( len(result) == 0 ):

    return(result)
## end get_networks_within()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

