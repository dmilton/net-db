#\L

# 15 Sep 2015   initial creation.

############################################################################
# Network Address class.
# Combines python ipaddress class with isoaddress class and adds an
# unnumbered address class.

class base_ipaddress(metaclass=ipaddress):
    pass

class base_isoaddress(metaclass=isoaddress):
    pass

class meta_address(ipaddress, isoaddress):
    pass

class NetworkAddress(ipaddress, isoaddress):
    """
    Create a new network class based on ipaddress, isoaddress, and unnumbered.
    """

    _network_kind = ["IP", "ISO", "unnumbered"]

    def __init__(self, address):
        if ( kind in _network_kind ):
            self.__kind = kind
            self.__address = address
        else:
            raise ValueError("Invalid network kind")

    def type(self):
        return(self.__kind)

    def address(self):
        if ( self.__type == "unnumbered" ):
            return("")
        else:
            return self.__address

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

