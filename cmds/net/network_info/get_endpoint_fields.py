#\L

# 17 Oct 2013   initial creation.

############################################################################
# get_endpoint_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to building rooms.
# Entry:    none
# Exit:
#       a dictionary of keys with value None that correspond to all the
#       fields for a network endpoint.

def get_endpoint_fields():

    global _networkEndpoints

    result = {}

    # Get the list of fields/input values for this table.
    tableInfo = db_access.get_table_keys(_networkEndpoints)

    # Copy the table information so we can modify it.
    for aKey in list(tableInfo.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = tableInfo[aKey]

    return(result)
## end get_endpoint_fields()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

