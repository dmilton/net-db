#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 28 Apr 2014    initial creation.

############################################################################
# calc_v6_links
# Create as many links as are appropriate for the building.
#
# Entry:
#       bldgInfo - the information about the building.
#       epCode - if provided, only one link is computed for this endpoint.
#       zoneInfo - the zone information for user address spaces.
#       linkType - type information for a link network.
#       doNotUpdate - do not update the zone to show links have been used.
# Exit:
#       return a list of link networks ready to add to the database.

def calc_v6_links(bldgInfo, epCode='',
        zoneInfo={}, linkType={}, doNotUpdate=False):

    result = []

    if ( 'net_type' not in linkType ):
        # Get the type information for a link network.
        linkType = network_types.get_type_info('link')[0]

    if ( 'net_zone' not in zoneInfo ):
        # Get the zone information for link network (or default)
        zoneInfo = network_zones.get_user_zone_info(bldgInfo['net_zone'], 6)

    # Get the endpoints to use for this zone.
    if ( epCode > '' ):
        epList = [epCode]
    else:
        epList = get_endpoints(bldgInfo['net_zone'])

    # Get a link address for each endpoint.
    linkList = network_zones.get_next_link(zoneInfo, len(epList), doNotUpdate)

    for linkCtr in list(range(len(epList))):
        # Create the link network.
        newLink = new_template(linkType)
        newLink['network'] = linkList[linkCtr]
        newLink['bldg_code'] = bldgInfo['bldg_code']
        newLink['ep_code'] = epList[linkCtr]
        # These are all L3 links so the vlan tag is really only for
        # display ordering purposes. Use the endpoint to calculate so
        # should vlan's be used in the core, uniqueness is assured.
        newLink['vlan_tag'] = get_next_vlan( epList[linkCtr], 
                zoneInfo['next_link'] + abs(linkType['vlan_tag']) + linkCtr, 6 )
        # Should verify that this is a valid vlan tag id.)
        # Now that everything is set, figure out the name.
        newLink['net_name'] = name_network(newLink, linkType)
        result.append(newLink)

    return(result)
## end def calc_v6_links():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

