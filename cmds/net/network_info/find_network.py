#\L

# 26 Jan 2015   Change error message to say assign-user, not assign-net.
#               Raise a NetException when a network cannot be found rather
#               than a ValueError so a CannotFind error can be a warning
#               rather than a failure.
# 25 Aug 2014   Set the network table to be that of the free networks table.
#               ensure the vlan_tag is always set and merge the loop to
#               assign names into the loop setting the rest of the values.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Oct 2013   Change error generation to use ValueError for a network
#               that could not be found in the free table. Update to check
#               link networks for a vlan_tag value of 0. If so then use
#               the vlan_tag value from the network type and increment by
#               the number of links required.
# 28 Apr 2013   initial creation.

############################################################################
# find_network
# Given the type of network and a count, locate and return a list of
# networks of the appropriate type.
# Entry:
#       type_info - name of the type of network.
#       bldg_code - building code to use for the network.
#       net_zone - the network zone to find addresses from.
# Exit: A list of networks is returned. If there are not enough networks
#       of the specified type available/free in the database then an
#       exception is returned.

def find_network(type_info, bldg_code, net_zone):

    global _networkFree

    result = []

    # Figure out how many networks to get.
    if ( type_info['assign_method'] == 'next' ):
        net_count = 1
    elif ( type_info['assign_method'] == 'nextn' ):
        if ( type_info['net_type'] == 'link' ):
            ep_codes = get_endpoints(net_zone)
            net_count = len(ep_codes)
        else:
            raise NetException("NoCount", "Don't know how many networks" + \
                "of type '" + type_info['net_type'] + "' to allocate.")
    elif ( type_info['assign_method'] == 'best' ):
        raise NetException("WrongClass", "Networks of type '" + \
                type_info['net_type'] + "' must be assigned manually " + \
                "using assign-user.")
    else:
        raise NetException("WrongClass",
                "The network assignment method '" 
                + type_info['assign_method'] + "' is not found.")

    # Extract free network(s) from the database.
    my_where = "net_type = '" + type_info['net_type'] + "'"
    my_where += " AND net_zone = " + str(net_zone)
    my_order = "network"
    db_result = db_access.get_table_rows(
                    _networkFree, my_where, my_order, net_count)

    # If we didn't get the correct count, exit.
    if ( len(db_result) != net_count ):
        raise NetException("CannotFind", "Could not find " + str(net_count)
                + " free '" + type_info['net_type'] + "' networks(s) in zone "
                + str(net_zone) + ".")

    # copy the db_result into the appropriate type template.
    vlan_index = 0
    for a_net in db_result:
        # create an blank template
        net_copy = new_template(type_info)
        for a_key in list(net_copy.keys()):
            # copy all the common values across.
            if ( a_key in a_net ):
                net_copy[a_key] = a_net[a_key]
        # set the building code.
        net_copy['bldg_code'] = bldg_code
        # Say what table the network is in now.
        net_copy['network_table'] = _networkFree['table']
        # carry forward the network zone.
        net_copy['net_zone'] = a_net['net_zone']
        # Set the vlan tag.
        net_copy['vlan_tag'] = abs(type_info['base_vlan_tag']) + vlan_index
        # Set the endpoint code if this is a link.
        if ( type_info['net_type'] == 'link' ):
            net_copy['ep_code'] = ep_codes[vlan_index]
        # Name the network
        net_copy['net_name'] = name_network(net_copy, type_info)
        # Increment our vlan index so the tags are unique in the building.
        vlan_index += 1

        # add the network to the result list.
        result.append( net_copy )

    return(result)
## end find_network()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

