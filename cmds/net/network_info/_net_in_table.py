#\L

# 25 Aug 2014   Revisit the 'network table' vs 'current table' logic.
#               'network table' is always set when a network is read from
#               the database so we know which table it came from. In the
#               case of a new network (should not be in the db) the
#               network_table should be "None" as set by new_template.
# 20 Aug 2014   Need to check for 'current table' so a new network (which is
#               not in any table) gets the correct information. Moved the
#               loop out into _get_table_info so it can be used elsewhere.
#  5 Mar 2014   Add support for the iso network table.
# 24 Oct 2013   Use the _networkTables list so new tables are searched.
#               Improve error checking regarding cases where a network
#               is not currently in a table or the table a network
#               belongs in cannot be determined.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 18 Apr 2013   initial creation.

############################################################################
# _net_in_table
# Given a network, return the table information for that network.
# Entry:
#       netToCheck - the network to find.
# Exit:
#       the network has been located or an exception raised.
#

def _net_in_table(netToCheck):

    # Start with where the network thinks it is.
    if ( "network_table" not in netToCheck ):
        netTableName = None
    else:
        netTableName = netToCheck['network_table']

    result = _get_table_info(netTableName)

    return(result)
## end _net_in_table()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

