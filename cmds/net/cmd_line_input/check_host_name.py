#

# 28 Nov 2013   Change to obtain an fqdn and then the hostinfo which is
#               returned as a result.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
#  5 Aug 2010   Correct documentation in check_host_name.
# 29 Jul 2010   initial creation.

############################################################################
# check_host_name(theName)
# Verify the value resolves to a valid DNS host name.

def check_host_name(theName):
    try:
        fqdn = getfqdn(theName)
    except:
        raise ValueError("invalid host name " + theName)

    try:
        result = gethostbyaddr(fqdn)
    except:
        raise ValueError("invalid host name " + theName)

    return(result)
## end check_host_name()

# vim: syntax=python ts=4 sw=4 showmatch et :

