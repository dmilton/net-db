#

# 15 Aug 2014   Add better diagnostics when an invalid input keyword has
#               been used on the command line.
# 11 Aug 2014   Add udt_type processing so handling of ARRAY of X can be
#               managed using a comma separated list.
#  1 Apr 2013   Allow a key value of 0x0 to represent an empty/null value.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

##############################################################################
# process_input_keys()
#
# Entry:
#       keyValueList - the list of keyword/value pairs from the command line.
#       inputKeyInfo - list of valid keywords and the field type.
#

def process_input_keys(keyValueList, inputKeyInfo):

    # Start with an empty result.
    result = {}

    # Process the keyword/value pairs.
    argPtr = 0
    totalArgs = len(keyValueList)
    for argPtr in range(0, totalArgs, 2):
        # The keyword provided on the command line.
        inKeyName = keyValueList[argPtr]
        # The value for the keyword provided on the command line.
        keyValue = keyValueList[argPtr+1]
        # translate hyphen to underscore in the keyword name from command line.
        keyName = inKeyName.translate(str.maketrans('-','_'))
        if ( keyName in inputKeyInfo ):
            valueType = inputKeyInfo[keyName]['type']
            valueTypeName = inputKeyInfo[keyName]['udt_name']
        else:
            raise ValueError("invalid input keyword '" + inKeyName + "'")
        try:
            # result should use db field names (underscores)
            result[keyName] = \
                    check_input_info(keyValue, valueType, valueTypeName)
        except ValueError as why:
            if ( 'unknown type' in str(why) ):
                print("invalid value for input keyword '" + inKeyName + "'")
            raise
        ## end try
    ## end for argPtr in range(1, len(keyValueList[1:]), 2):

    if ( argPtr + 2 < totalArgs ):
        print(("Fatal error: missing value for keyword '" + \
            keyValueList[argPtr+2] + "'."))
        raise CmdException("incomplete command line input.")
    ## end if ( argPtr + 2 <= totalArgs ):
    
    return(result)
## end process_input_keys()

# vim: syntax=python ts=4 sw=4 showmatch et :

