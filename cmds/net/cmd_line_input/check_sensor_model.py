#

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 30 Aug 2010   Added A5205 model.
# 29 Jul 2010   initial creation.

############################################################################
# check_sensor_model(theModel)
# Verify the value looks like a sensor model.

def check_sensor_model(theModel):

    result = theModel
    if ( (theModel in ('A5010', 'A5020', 'A5205')) != True ):
        raise ValueError('invalid sensor model: ' + theModel)
    ## end sensor model check.

    return(result)
## end check_sensor_model()

# vim: syntax=python ts=4 sw=4 showmatch et :

