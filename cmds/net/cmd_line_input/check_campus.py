#

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# check_campus(theCampus)
# Verify the campus input matches either a string or number value.

def check_campus(checkCampus):

    theCampus = str(checkCampus)
    if ( theCampus in ('1', 'fortgarry', 'fort-garry', 'fg') ):
        result = 1
    elif ( theCampus in ('2', 'bannatyne', 'bannatyne', 'bc') ):
        result = 2
    elif ( theCampus in ('4', 'wpgedctr', 'wec') ):
        result = 4
    elif ( theCampus in ('5', 'extendededucationdowntown', 'eed') ):
        result = 5
    elif ( theCampus in ('6', 'smartpark', 'smart-park') ):
        result = 6
    elif ( theCampus in ('7', 'glenlea') ):
        result = 7
    else:
        raise ValueError('invalid value')

    return(result)
## end check_campus()

# vim: syntax=python ts=4 sw=4 showmatch et :

