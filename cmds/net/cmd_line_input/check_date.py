#

#  3 Sep 2014   Add now as a valid input and use the current time.
# 24 Jan 2014   Allow full timestamp values with microseconds.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 21 Mar 2013   Rewrite to use datetime class.
# 18 Mar 2013   reformat using tabs.
#  4 Aug 2010   Correct check_date so it converts to integers.
#               Improve checking so it verifies the day of month
#               properly even for leap years.
# 20 Jul 2010   Add new input type bldgCode.

############################################################################
# check_date(theDate)
# Verify the value input looks like a date of the form yyyy-mm-dd

def check_date(inputDate):

    # Check to see if we want a current time.
    if ( "now" in inputDate.lower() ):
        inputDate = str(datetime.now())

    # Check to see if we have one of these: 2010-04-15 14:30:40.742226
    try:
        (theDate, fracSecs) = inputDate.split('.')
        microsec = int(fracSecs)
    except ValueError:
        # Nope, have a regular date.
        theDate = inputDate
        microsec = 0

    # Now start with most complex case. yyyy-mm-dd HH:MM:SS
    try:
        result = datetime.strptime(theDate, '%Y-%m-%d %H:%M:%S')
        result.replace(microsecond=microsec)
        done = True
    except ValueError:
        done = False

    if ( not done ):
        # Next up, drop the seconds. yyyy-mm-dd HH:MM
        try:
            result = datetime.strptime(theDate, '%Y-%m-%d %H:%M')
            done = True
        except ValueError:
            done = False

    if ( not done ):
        # Now try just the date. yyyy-mm-dd
        try:
            result = datetime.strptime(theDate, '%Y-%m-%d')
            done = True
        except ValueError:
            done = False
            year = 0; month = 0; day = 0

    if ( not done ):
        # How about a time? hh:mm:ss
        try:
            result = datetime.strptime(theDate, '%H:%M:%S')
            done = True
        except ValueError:
            done = False

    if ( not done ):
        # How about a time? hh:mm
        try:
            result = datetime.strptime(theDate, '%H:%M')
            done = True
        except ValueError:
            done = False


    return(result)
## end check_date()

# vim: syntax=python ts=4 sw=4 showmatch et :

