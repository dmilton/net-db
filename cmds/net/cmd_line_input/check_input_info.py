#

#  4 Sep 2014   Allow inet and cidr to check for an ISO address too.
# 11 Aug 2014   New argument required so ARRAYs can be processed.
# 24 Apr 2014   Remove old google ipaddress library code.
#  7 Mar 2014   Add use of python 3.3 ipaddress library.
# 24 Jan 2014   Add better date type handling.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  1 Apr 2013   allow 0x0 to represent an empty string.
# 30 Mar 2013   update character processing.
# 18 Mar 2013   reformat using tabs.
# 20 Jul 2010   Add new input type bldgCode.
# 13 Jul 2010   Initial creation.

##############################################################################
# check_input_info()
# Entry:
#       theValue - the input value from the command line.
#       dataType - the data_type describing what the value should be.
#       dataTypeName - the udt_name for this column.

# udt_names which are NOT currently handled are:
#   real    -   float4      x   -   int2vector
#   double precision    -   float8
#   bytea   -   bytea       x   -   _text
#   abstime -   abstime     x   _   _float4
#   name    -   name        x   -   _name
#   xid     -   xid         x   -   _regtype    _anytype
#   interval    -   x       
#   timestamp with timezone -   timestampz
##################################################
#   bool, int2, _char, _text, int4, int8
#   smallint, integer, bigint
#   character varying   -   varchar
#   text    -   text        
#   cidr    -   cidr
##################################################
#   These may cause a problem:
#   character   -   bpchar
def check_input_info(theValue, dataType, dataTypeName):

    # Process the key value based on the identified type.
    if ( "int" in dataType ):
        result = int(theValue)
    elif ( "char" in dataType ):
        if ( theValue == "0x0" ):
            result = ""
        else:
            result = theValue
    elif ( "bool" in dataType ):
        result = check_boolean(theValue)
    elif ( dataType == "date" ):
        result = check_date(theValue)
    elif ( "timestamp" in dataType ):
        result = check_date(theValue)
    elif ( dataType == "ARRAY" ):
        result = check_array(theValue, dataTypeName)
    elif ( dataType == "hostName" ):
        result = check_host_name(theValue)
    elif ( dataType == "macaddr" ):
        result = check_mac_addr(theValue)
    elif ( dataType == "inet" ):
        try:
            result = check_iso_addr(theValue)
        except ValueError:
            result = ipaddress.ip_address(theValue)
    elif ( dataType == "cidr" ):
        try:
            result = check_iso_addr(theValue)
        except ValueError:
            result = ipaddress.ip_network(theValue)
    elif ( dataType == "sensorModel" ):
        result = check_sensor_model(theValue)
    elif ( dataType == "bldgCode" ):
        result = check_bldg_code(theValue)
    elif ( dataType == "campus" ):
        result = check_campus(theValue)
    else:
        # We don't know this type.
        raise ValueError("unknown type " + dataType)
    ## end if ( dataType['type'] == 'x' ):

    return(result)
## end check_input_info():

# vim: syntax=python ts=4 sw=4 showmatch et :

