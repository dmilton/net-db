#\L

# 18 Apr 2013   initial creation.

############################################################################
# check_name
def check_name(theName):
    # Names must start with a letter and consist of 
    # letters, numbers, periods hyphens and underscores.
    alphaLower = "abcdefghijklmnopqrstuvwxyz"
    alphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphaChars = alphaLower + alphaUpper
    allChars = alphaChars + "12345567890._-"

    if ( len(theName) == 0 ):
        raise ValueError('invalid name')

    if ( theName[0] not in alphaChars ):
        raise ValueError('invalid name')

    for aChar in theName[1:]:
        if ( aChar not in allChars ):
            raise ValueError('invalid name')

    return()
## end check_name()

# vim: syntax=python ts=4 sw=4 showmatch et :

