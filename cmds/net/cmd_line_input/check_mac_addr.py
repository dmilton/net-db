#

# 18 Feb 2013   Python3 uses different lower() syntax.
# 28 Nov 2013   Change checks to allow forms where leading zeros are not
#               in the list (like Solaris) and still parse correctly.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# check_mac_addr(theMacAddr)
# Verify the MAC address received is of the correct format.

def check_mac_addr(theMacAddr):
    hexChars = '0123456789abcdef'

    # get the list of all of the 'invalid characters' by stripping
    # the good ones from the MAC address.
    theMacAddr = theMacAddr.lower()
    badChars = theMacAddr.strip(hexChars + ':.')
    if ( len(badChars) != 0 ):
        raise ValueError("invalid MAC address " + theMacAddr)

    macForm1 = theMacAddr.split('.')
    macForm2 = theMacAddr.split(':')
    if ( len(macForm1) == 3 ):
        # handle the 0123.4567.89ab form
        macString = ''
        for theBits in macForm1:
            macString += theBits.rjust(4,'0')
    elif ( len(macForm2) == 6 ):
        # handle the 01:23:45:67:89:ab form(s)
        macString = ''
        for aByte in macForm2:
            macString += aByte.rjust(2,'0')
    else:
        raise ValueError("invalid MAC address " + theMacAddr)

    if ( len(macString) != 12 ):
        raise ValueError('invalid MAC')

    # We now have a string which is all hex characters and is of
    # the correct length. Translate this into the form 00:00:00:00:00:00.
    result = macString[0:2] + ':' + macString[2:4] + ':' +\
             macString[4:6] + ':' + macString[6:8] + ':' +\
             macString[8:10] + ':' + macString[10:12]

    return(result)
## end check_mac_addr()

# vim: syntax=python ts=4 sw=4 showmatch et :

