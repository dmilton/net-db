#\L

#  4 Sep 2014   initial creation.

############################################################################
# check_iso_addr
# Entry: isoAddr - an ISO address.
# Exit: returns true if this appears to be an ISO address
# 

def check_iso_addr(isoAddr):

    # Check that an address is of the form:
    #   49.0001.0000.0000.002a.00
    isoAddrChars = "0123456789abcdef."

    # get the list of all of the 'invalid characters' by stripping
    # the valid ones from the address.
    result = isoAddr.lower()
    badChars = result.strip(isoAddrChars)
    if ( len(badChars) > 0 or len(isoAddr) != 20 or not (
        ( isoAddr[:3] == "49." and isoAddr[-3:] == ".00" ) and
            ( len(isoAddr.split(".")) == 5 )) ):
        raise ValueError("invalid ISO address " + isoAddr)

    return(result)
## end check_iso_addr()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

