#

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# check_bldg_code(theCode)
# Verify the value looks like a building code. No verification is
# done to ensure it is valid, just that it looks like a building code.

def check_bldg_code(theCode):

    result = theCode
    if ( len(result) < 2 or theCode.isalnum() == False ):
        raise ValueError('invalid building code ' + theCode)
    ## end building code check.

    return(result)
## end check_bldg_code()

# vim: syntax=python ts=4 sw=4 showmatch et :

