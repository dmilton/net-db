#\L

# 11 Aug 2014   initial creation.

############################################################################
# check_array
# Entry: inputList - a comma separated list of 'things'
# Exit: a list of 'things'
# Exceptions:

def check_array(inputList, listItemType):
    result = []

    # Produce a list of items ot type listItemType
    itemsInList = inputList.split(',')
    for anItem in itemsInList:
        result.append(check_input_info(anItem, listItemType, listItemType))

    return(result)
## end check_array()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

