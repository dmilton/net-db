#

#  5 Feb 2014   Correct lower call for python3 syntax.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 18 Aug 2010   Allow input of 1 or 0 values.
#               zero or one rather than F and T.
# 13 Jul 2010   Initial creation.

############################################################################
# check_boolean(theBoolean)
# Verify the value input represents a boolean.

def check_boolean(theBoolean):

    theBoolean = str(theBoolean).lower()
    if ( theBoolean in ('1', 'true', 'yes', 't', 'y') ):
        result = 'T'
    elif ( theBoolean in ('0', 'false', 'no', 'f', 'n') ):
        result = 'F'
    else:
        raise ValueError('invalid boolean value' + theBoolean)
    ## end else if ( theBoolean in xxxx ):

    return(result)
## end check_boolean()
# vim: syntax=python ts=4 sw=4 showmatch et :

