#

# 22 Jan 2014   format not working properly because division now returns
#               a real rather than an integer. Force to be an integer.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 27 Mar 2013   Make output display a bit nicer.
# 25 Mar 2013   Initial creation.

##############################################################################
# list_input_keys(theTable)
# Entry: theTable - dictionary of information about a db table.
# Accept the key information and list all keys and the description.
# This allows display of the keys specific to this command.

def list_input_keys(fieldInfo, flagFieldList=[]):

    print("Command input keys and their types.")

    # get our field names and sort them.
    fieldNames = list(fieldInfo.keys())
    fieldNames.sort()

    # Loop through to find the maximum length field name.
    maxLen = 0
    for aKey in fieldNames:
        if ( len(aKey) > maxLen ):
            maxLen = len(aKey)
    # Everything below is indented 4 spaces, assume tabs are 8 spaces.
    maxLen += 4
    # calculate position of first tab
    firstTab = int( ( ( maxLen / 8 ) + 1) * 8 )
    format = "    %-" + str(firstTab) + "s"

    # Now loop through and display the field names and their types.
    for aKey in fieldNames:
        # translate underscores to hyphens for easier input.
        inKey = aKey.translate(str.maketrans('_','-'))
        # we have type, size, default, update, and pkey to check.
        aField = fieldInfo[aKey]
        if ( 'update' in aField ):
            msg = format % inKey
            if ( aField['type'] == 'character varying' ):
                fieldType = 'char'
            else:
                fieldType = aField['type']
            if ( aField['size'] == None ):
                msg += fieldType
            else:
                msg += fieldType + "(" + str(aField['size']) + ")"
            if ( aField['default'] != None ):
                msg += " default=" + aField['default'].split(':')[0]
            if ( aField['pkey'] == True ):
                msg += " KEY"
            print(msg)

## end list_input_keys()

# vim: syntax=python ts=4 sw=4 showmatch et :

