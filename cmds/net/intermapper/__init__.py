#
gVersion=''

from net.network_error import NetException

###############################################################################
# Interfaces table
#
_interface = {
	'database': 'intermapper',
	'role': 'intermapper',
	'table': 'interface',
	'index': 'interface_id'
}

from . import get_if_info

# vim: syntax=python ts=4 sw=4 showmatch et :

