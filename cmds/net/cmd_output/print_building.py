#\L

# 25 Mar 2015   Make printing of ipv6 address allocation optional.
# 26 Jan 2015   Remove core_bldg since it isn't referenced anywhere.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 10 Jun 2014   Changed colum name in table from 'name' to 'bldg_name' so
#               added code to handle either. Can remove once the production
#               database has been changed.
# 26 Mar 2014   Change output slightly so the zone is consistient amongst
#               the various commands.
# 23 Jan 2014   VTP Domain could be empty or None. Check for both conditions
#               and don't print if either condition is true.
#               Compute IPv6 allocation address for the building zone and
#               display that as well.
# 22 Jan 2014   ospf_area could be None, check for that instead of >= 0.
# 25 Sep 2013   Change order of output for more reasonable groupings.
# 11 Apr 2013   initial creation.

############################################################################
# print_building
#
# Entry:
#       bldg_info - dictionary of building information.
#       bldg_alloc - base IPv6 network for the building.
#       dump_flag - print(all fields of the record.
# Exit: the building information has been printed to the terminal.

def print_building(bldg_info, bldg_alloc, dump_flag=False):

    if ( dump_flag ):

        # dump all fields.
        key_list = list(bldg_info.keys())
        key_list.sort()
        # dump all fields in the record.
        print(("\nBuilding Code: " + bldg_info['bldg_code']))
        for a_key in key_list:
            if ( a_key != 'bldg_code' and bldg_info[a_key] != None ):
                print((a_key + " = " + str(bldg_info[a_key])))
        ## end for a_key in key_list:
    
    else:

        # Display the basic information for the building.
        print( "Building" + "(" + bldg_info['bldg_code'] + "): " + \
            bldg_info['bldg_name'] )
        if ( bldg_info['bldg_address'] != None ):
            print( "Building Address: " + bldg_info['bldg_address'] )
        if ( bldg_info['bldg_router'] > '' ):
            print( "Building Router: " + bldg_info['bldg_router'] )
        if ( bldg_info['ospf_area'] != None ):
            print( "OSPF Area: " + str(bldg_info['ospf_area']) )
        if ( bldg_info['vtp_domain'] != None ):
            if ( bldg_info['vtp_domain'] > '' ):
                print( "VTP Domain: " + bldg_info['vtp_domain'] )

        # Web and map links for the building.
        if ( bldg_info['web_dir'] != None ):
            print("Web Directory: " + bldg_info['web_dir'] +
                    ", Visibility: " + str(bldg_info['web_visible']) )

        # Deal with campus info.
        if ( bldg_info['campus_id'] > 0 and 'campus_name' in bldg_info ):
            print(("Network Zone: " + str(bldg_info['net_zone']) + \
                ", " + bldg_info['campus_name'] + " Campus" ))
        else:
            print(("Network Zone: " + str(bldg_info['net_zone']) + \
                ", Campus ID: " + str(bldg_info['campus_id']) ))

        # IPv6 Information.
        if ( bldg_info['ipv6_netid'] > 0 ):
            print(( "IPv6 Network ID: " + \
                str(bldg_info['ipv6_netid']) + \
                ", Next Network: " + str(bldg_info['ipv6_nextnet']) ))
            if ( len(str(bldg_alloc)) > 0 ):
                print("IPv6 Allocation Start: " + str(bldg_alloc))

## end def print_building():

# vim: syntax=python ts=4 sw=4 showmatch et :

