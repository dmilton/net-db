#\L

# 1 Apr 2013        Added flag and code to dump an entire record.
# 15 Nov 2102        initial creation.

############################################################################
# print_licenses
#
# Entry:
#        licenseList - list of licenses to print.
#        dumpFlag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_licenses(licenseList, dumpFlag=False):

    # If we only have one license, print the entrie thing.
    if ( len(licenseList) == 1 ):
        dumpFlag = True

    if ( len(licenseList) > 0 ):

        if ( dumpFlag == False ):

            # Convert our list of afl license dictionaries into a table.
            keys = ["serial_nbr", "member_id", "auth_code"]
            aflTable = []
            for aLicense in licenseList:
                if ( aLicense['auth_active'] == True ):
                    aflTable += aLicense

            aflHead = ("Serial Number", "Member ID", "Auth Code")
            aflTitle = "Advanced Feature Set Licenses:"
            print_table(aflHead, aflTable)

        else:

            keyList = list(licenseList[0].keys())
            keyList.sort()
            for aLicense in licenseList:
                # dump all fields in the record.
                print("\nAuthorization Code: " + aLicense['auth_code'])
                for aKey in keyList:
                    if ( aKey != 'auth_code' ):
                        print(aKey + " = " + str(aLicense[aKey]))

## end def print_licenses(bldgCode):


# vim: syntax=python ts=4 sw=4 showmatch et :

