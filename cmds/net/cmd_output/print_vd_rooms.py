#\L

# 12 Dec 2014   initial creation.

############################################################################
# print_vd_rooms
# Entry: vd_room_list - The voice data room list (as returned by get_vd_rooms)
# Exit: the list of voice data rooms has been printed to the terminal.
# Exceptions:

def print_vd_rooms(vd_room_list):

    if ( len(vd_room_list) > 0 ):
        print("Voice Data Rooms:")
        for a_room in vd_room_list:
            room_info = '[id=' + str(a_room['room_id'])
            if a_room['mdr'] == True:
                room_info += ",MDR"
            room_info += "]"
            print(("    " + a_room['room'] + room_info))
    else:
        print("No voice/data rooms defined.")

## end def print_vd_rooms()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

