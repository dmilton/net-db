#

# 23 Sep 2013   Add check to test that dictList is not zero length
#               and therefore not try to process the empty list.
# 25 Apr 2013   Virtually all lists now contain dictionaries so added
#               some unwrapping code here so it doesn't have to be
#               duplicated in every script that uses print_table.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# print_table
# Given a table and header print the table for the user.
# Entry:
#       title - string to print as a title for the table
#       header - tuple of column names
#       keyList - list of dictionary keys matching colums for header
#       dictList - list of dictioraries to print in a table.

def print_table(title, header, keyList, dictList):

    # Start by turning the list of dictionaries into a list of lists.
    tableRows = []
    if ( len(dictList) > 0 ):
        for aRow in dictList:
            rowList = []
            for aKey in keyList:
                if ( aRow[ aKey ] == None ):
                    rowList.append( '' )
                else:
                    rowList.append( str(aRow[ aKey ]) )
            tableRows.append(rowList)

    # determine the maximum length of each field.
    fieldCount = len(header)
    # start with the length of the header field.
    fieldSize = []
    for i in range(fieldCount):
        fieldSize.extend([ len(header[i]) ])

    # now update our size with that of the field itself.
    tableSize = len(tableRows)
    for i in range(tableSize):
        for j in range(fieldCount):
            if ( tableRows[i][j] != None):
                fieldSize[j] = max(fieldSize[j], len(str(tableRows[i][j])))

    # create a separator and output format line:
    sepLine = "+"
    formatLine = "|"
    for j in range(fieldCount):
        formatLine = formatLine + " %-" + str(fieldSize[j]) + "s |"
        sepLine = sepLine + "-"
        for i in range(fieldSize[j]):
            sepLine = sepLine + "-"
        sepLine = sepLine + "-|"
    tableSeparator = sepLine[:len(sepLine)-1] + "+"

    # now print out the table.
    print(title)
    print(tableSeparator)
    print((formatLine % header))
    print(tableSeparator)

    for aRecord in tableRows:
        thisRecord = ()
        for i in range(len(aRecord)):
            # note comma at end of next line. This is required to make aField
            # into a Tuple. That permits concatenation of Tuples in the next
            # statement.
            aField = str(aRecord[i]),
            thisRecord = thisRecord + aField
        ## end for i in range(len(anEntry)):
        print((formatLine % thisRecord))
    ## end for anEntry in table:
    print(sepLine)

## end def print_table():

# vim: syntax=python ts=4 sw=4 showmatch et :

