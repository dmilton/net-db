#

# 23 Mar 2015   Change building code handling to allow up to 8 characters.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 18 Aug 2010   Change output format routine so booleans print as
#               zero or one rather than F and T.
# 16 Aug 2010   Add new routine to format output.

##############################################################################
# output_format()
# Entry: the_value - the value to be formatted.
#         the_type - the variable type.

def output_format(the_value, the_type):

    # Process the key value based on the identified type.
    if ( the_type == 'bldgCode' ):
        # building codes can be up to 8 characters
        the_len = len(the_value)
        if ( the_len > 8 ):
            the_len = 8
        result = the_value[0:the_len]
    elif ( the_type == 'boolean' ):
        if ( the_value == True ):
            result = '1'
        else:
            result = '0'
    elif ( the_type == 'campus' ):
        result = str(the_value)
    elif ( the_type == 'date' ):
        result = the_value
    elif ( the_type == 'hostName' ):
        result = check_host_name(the_value)
    elif ( the_type == 'integer' ):
        result = str(the_value)
    elif ( the_type == 'macaddr' ):
        result = check_mac_addr(the_value)
    elif ( the_type == 'sensorModel' ):
        result = check_sensor_model(the_value)
    elif ( the_type == 'string' ):
        result = the_value
    else:
        # We don't know this type.
        raise ValueError('unknown type')
    ## end if ( theKeyInfo['type'] == 'x' ):

    return(result)
## end output_format():

# vim: syntax=python ts=4 sw=4 showmatch et :

