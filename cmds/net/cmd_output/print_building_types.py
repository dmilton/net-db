#\L

# 16 Apr 2015   initial creation.

############################################################################
# print_building_types
# Entry: bldg_type_list - the list of building type(s) to print.
# Exit: result
# Exceptions:

def print_building_types(bldg_type_list):

    bldg_list_len = len(bldg_type_list)
    if ( bldg_list_len == 0 ):
        print("No building types are defined.")
    elif ( bldg_list_len == 1 ):
        print("Building Type: " + bldg_type_list[0]['bldg_type'])
        print("Description: " + bldg_type_list[0]['bldg_type_description'])
    else:
        bldg_type_title = "Building Types"
        bldg_type_header = ("Building Type", "Description")
        bldg_type_keys = ["bldg_type", "bldg_type_description"]
        print_table(bldg_type_title, bldg_type_header,
                        bldg_type_keys, bldg_type_list)

    return
## end print_building_types()

# vim: syntax=python ts=4 sw=4 showmatch et :

