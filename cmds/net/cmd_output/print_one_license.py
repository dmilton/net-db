#\L

# 23 Mar 2015   initial creation.

############################################################################
# print_one_license
# Entry: aLicense - the license to print.
# Exit: the details of the license have been printed.
# Exceptions:

def print_one_license( aLicense ):

    if ( aLicense == None ):
        print("Unable to find a license with that auth code.")
    else:
        print("Authorization Code Information:")
        print("Auth Code: " + aLicense['auth_code'])
        print("RTU Serial: " + aLicense['rtu_serial'])
        if ( aLicense['router'] != None ):
            print("Router: " + aLicense['router'])
            print("Serial Number: " + str(aLicense['serial_nbr']))
            print("Member ID: " + str(aLicense['member_id']) )
            print("MAC Address: " + str(aLicense['lan_mac']))
        if ( aLicense['auth_active'] == True ):
            print("\nLicense has been activated with key:")
            print(str(aLicense['license_key']))
        else:
            print("License has not been activated yet.")

## end print_one_license()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

