#\L

# 22 Sep 2014   initial creation.

############################################################################
# print_one_type
#
# Entry:
#       theType - a network type dictionary to print.
#       showBrief - only show the main fields.
# Exit: the type has been printed to the terminal.

def print_one_type(theType, showBrief=False):

    # create a sorted list of the fields:
    keyList = list(theType.keys())
    keyList.sort()

    # Print the type information to the terminal:
    print("\nNetwork Type: " + theType['net_type'])
    print("Required Buildings: ["+ ', '.join(theType['bldg_type_list']) + "]")

    vlanNbr = str(abs(theType['base_vlan_tag']))
    if ( theType['base_vlan_tag'] > 0 ):
        vlanLine = "Starting VLAN ID: " + vlanNbr
    elif ( theType['base_vlan_tag'] < 0 ):
        vlanLine = "Unique VLAN ID: " + vlanNbr
    else:
        vlanLine = "Manual VLAN ID assignment."
    print(vlanLine)

    for aKey in keyList:
        if ( not showBrief  and
                aKey not in ["net_type", "base_vlan_tag",
                    "type_descr", "bldg_type_list"]):
            print(aKey + " = " + str(theType[aKey]))
        elif ( aKey == "type_descr" and len(theType['type_descr']) > 0 ):
            theDescr = "Description: " + theType['type_descr']
            print( textwrap.fill(textwrap.dedent(theDescr), 72) )

## end print_one_type()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

