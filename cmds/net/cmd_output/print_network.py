#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 20 Feb 2014   Change wording of table output since 'found' is not exactly
#               correct when adding a network.
# 19 Feb 2014   Some entries may not have an assign_by field (None) so
#               check for this and use the generic netmon as eh assigner.
# 30 Jan 2014   Correct documentation.
# 24 Jan 2014   Add display of the network name and new conditional
#               fields (network_table).
# 17 Dec 2013   Some networks cannot be prime. Don't try and disply
#               that information when it doesn't exist.
# 18 Oct 2013   initial creation.

############################################################################
# print_network
#
# Entry:
#        netInfo - dictionary of network information.
# Exit: the building information has been printed to the terminal.

def print_network(netInfo):

    # Display the basic information for the building.
    print("Network: " + str(netInfo['network']) + " vlan ID " + \
            str(netInfo['vlan_tag']) )
    print("Network Name: " + netInfo['net_name'])
    print("Building Code: " + netInfo['bldg_code'] )

    if ( "ep_code" in netInfo ):
        print("Network endpoint code: " + netInfo['ep_code'])

    if ( "remote_code" in netInfo ):
        print("Network remote building: " + netInfo['remote_code'])

    primeState = ""
    if ( "prime_net" in netInfo and netInfo['net_type'] == "user" ):
        if ( netInfo['prime_net'] ):
            primeState = " - prime net in building."
        else:
            primeState = " - non-prime."
    print("Network Type: " + netInfo['net_type'] + primeState)

    if ( 'assign_by' in netInfo ):
        print( "Assigned: " + str(netInfo['assign_date']) \
            + " by " + netInfo['assign_by'] )
    else:
        print( ("Assigned: " + str(netInfo['assign_date']) \
            + " by netmon" ) )

    if ( 'net_zone' in netInfo ):
        print("Network was made free on " + str(netInfo['free_date'])
                + ", net zone " + str(netInfo['net_zone']))

    if ( 'network_table' in netInfo ):
        print("The network is in the " + netInfo['network_table'] + " table.")

## end def print_network():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

