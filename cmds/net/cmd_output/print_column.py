#\L

#  2 May 2013		initial creation.

############################################################################
# print_column
#
# Entry:
#		columnList - list of network types to print.
# Exit: the list has been printed to the terminal.

def print_column(columnList):

	if ( len(columnList) > 0 ):
		# Convert our list of afl license dictionaries into a table.
		typeKeys = ["table_name","table_column","descr"]
		typeHead = ("Table Name", "Table Column", "Description")
		typeTitle = "Currently defined table column descriptions:"
		for aCol in columnList:
			print(("Table: " + aCol['table_name'] + \
					"\t Column: " + aCol['table_column'] + \
					"\nDesc: " + aCol['column_descr'] + "\n"))
	## end if ( len(columnList) > 0 ):

## end def print_column():

# vim: syntax=python ts=4 sw=4 showmatch et :

