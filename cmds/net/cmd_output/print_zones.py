#\L

# 11 Apr 2015   Add check for a zone description with no address ranges
#               and print an appropriate message.
# 21 Jun 2013   update for schema changes/field name changes.
# 13 May 2013   added net_type_list column to output.
# 25 Apr 2013   initial creation.

############################################################################
# print_zones
#
# Entry:
#        zone_list - list of network zones to print.
#        dump_flag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_zones(zone_list, dump_flag=False):

    # If we only have one zone, print the entrie thing.
    if ( len(zone_list) == 1 ):
        dump_flag = True

    if ( len(zone_list) > 0 ):
        if ( dump_flag == False ):
            # Split up by the zone ID.
            zones_by_id = {}
            for a_net_block in zone_list:
                if ( a_net_block['net_zone'] not in zones_by_id ):
                    zones_by_id[a_net_block['net_zone']] = []
                zones_by_id[a_net_block['net_zone']].append(a_net_block)

            # Table variables for all zone address blocks.
            zone_keys = ["network_range", "next_loop", "next_link",
                    "next_bldg", "max_net", "net_type_list"]
            zone_head = ("Network", "Loopback", "Link",
                    "Building", "Max Nets", "Types")
            
            # Print the individual network ranges of the zone.
            for a_net_zone_id in list(zones_by_id.keys()):
                zone_info_list = zones_by_id[a_net_zone_id]
                zone_title = "Network Zone: " + str(a_net_zone_id)
                zone_title += ", " + zone_info_list[0]['zone_descr']
                if ( "network_range" not in zone_info_list ):
                    print(zone_title)
                    print("No address ranges in this zone.\n")
                else:
                    print_table(zone_title, zone_head,
                            zone_keys, zone_info_list)
        else:
            keyList = list(zone_list[0].keys())
            keyList.sort()
            for aZone in zone_list:
                # dump all fields in the record.
                print("\nNetwork Zone: " + str(aZone['net_zone']))
                print("\nRange Description: " + str(aZone['range_descr']))
                for aKey in keyList:
                    if ( aKey not in ["net_zone","range_descr"] ):
                        print((aKey + " = " + str(aZone[aKey])))

## end def print_zones():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

