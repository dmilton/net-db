#\L

# 22 Sep 2014   Add formatted output of the type description. Simplify
#               decision logic regarding type_list as a dictionary or
#               a list of dictionaries.
# 21 Aug 2014   Update fields to print, remove family and tables.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 21 Jan 2014   Update to allow for prefix_size not being present and
#               to display net_family.
# 21 Jun 2013   Allow input of a dictionary or a list.
# 25 Apr 2012   initial creation.

############################################################################
# print_types
#
# Entry:
#       type_list - list of network types to print. If a dict then only
#                one type is expected and dump is implied.
#       dump_flag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_types(type_list, dump_flag=False):

    # If we only have one type, print the entrie thing.
    if ( type(type_list) is dict ):
        print_one_type(type_list, False)
    else:
        # This is a list of either zero or more than one type.
        if ( len(type_list) > 0 ):
            if ( dump_flag == False ):
                # Convert our list of typeInfo dictionaries into a table.
                if ( "type_required" in type_list[0] ):
                    reqd = "type_required"
                elif ( "bldg_required" in type_list[0] ):
                    reqd = "bldg_required"
                elif ( "bldg_type_list" in type_list[0] ):
                    reqd = "bldg_type_list"
                type_keys = ["net_type","assign_priority","base_vlan_tag",
                        "name_extension",reqd,"prefix_size" ]
                type_head = ("Type Name", "Pri", "Vlan", 
                        "Extension", "Required?", "Prefix")
                type_title = "Currently defined network types:"
                print_table(type_title, type_head, type_keys, type_list)
            else:
                key_list = list(type_list[0].keys())
                key_list.sort()
                for a_type in type_list:
                    print_one_type(a_type, False)

## end def print_types():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

