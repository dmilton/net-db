#\L

# 11 Apr 2014   Add output of the new field campus code.
# 13 Aug 2014   Change to detect input of a single campus_info dictionary
#               and just display a single entry.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 14 May 2013   initial creation.

############################################################################
# print_campus
#
# Entry:
#        campus_info    dictionary of campus information or a list of
#                       campus_info dictionaries.
#
# Exit: the campus information has been printed to the terminal.
#

def print_campus(campus_info):

    if ( type(campus_info) == dict ):
        print("Campus ID: " + str(campus_info['campus_id'])
                + " (" + campus_info['campus_code'] + ")")
        print("Campus Location: " + str(campus_info['campus_location']))
    else:
        # Only three fields in a campus record...
        campusTitle = "Found the following campus locations:"
        campusKeys = ["campus_id", "campus_code", "campus_location"]
        campusHead = ("Campus ID", "Campus Code", "Campus Location")
        print_table(campusTitle, campusHead, campusKeys, campus_info)

## end def print_campus(bldgCode):

# vim: syntax=python ts=4 sw=4 showmatch et :

