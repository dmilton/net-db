# Networking database modules.

# 16 Apr 2014   gacrux is now the 'production' development environment.
# 15 Apr 2014   More port changes to match new dev environment.
# 15 Apr 2014   Drop old development database, now running 9.4 for dev.
#  2 Apr 2014   Add new development database entry for the 9.4 upgrade.
#  9 Dec 2013   netdisco doesn't have a development db, make it match
#               the production database information.
#  2 Mar 2013   Remove passwords from the file.
# 13 Nov 2012   Fix minor problems with backwards compatibility for
#               dbAccess vs dbAccess. Added new key 'server' to avoid
#               collission with 'host'
#  7 Nov 2012   Added new variables to support new database routines.
# 19 Apr 2012   Changed the server names to use aliases instead.
# 19 Feb 2011   Updated the InterMapper database info to reflect
#               the move to postgres 8.4.
#  7 Feb 2011   Added another database instance and an array of
#               all server information which can be referenced by
#               a server id number. This allows addition of another
#               database instance without having to modify the
#               SetDbServer routine.
# 10 Mar 2010   Changed netlib to reference /opt2/local/lib/network.
# 12 Nov 2009   Changed netlib to reference /opt2 rather tha /usr
# 27 Oct 2009   Changed the networking database so production
#               and development servers are reversed so I can
#               easily test the 'not quite yet' production instance.
# 19 Aug 2009   Changed production and development servers for
#               the intermapper database over to mizar instances.
# 23 Jun 2009   Added variables for development postgres server
#               and added the port number to dbHost/devDbHost.

# Should the database be enabled?
# Set this variable to false when the database is down.
dbEnabled = True

# Production database server
dbProdHost = 'pgprod.cc.umanitoba.ca'
dbProdNetDbPort = 5447
dbProdInterMapperPort = 5444
dbProdNetdiscoPort = 5446
dbProdBcanPort = 5441

# Development database server
#dbDevHost = 'pgdevl.cc.umanitoba.ca'
dbDevHost = 'pgdevl.cc.umanitoba.ca'
dbDevNetDbPort = 5447
dbDevInterMapperPort = 5444
dbDevNetdiscoPort = 5446

# test database server.
dbDevHost2 = 'pgdevl.cc.umanitoba.ca'
dbDevNetDbPort2 = 5450
dbDevInterMapperPort2 = 5444
dbDevNetdiscoPort2 = 5446

# List of all databases, tables, and their user info.
dbServerInfo = {}

# Production database.
dbServerInfo[0] = {}
dbServerInfo[0]['networking'] = \
        {'server': dbProdHost, 'port': dbProdNetDbPort,
            'user':'netmon', 'look':'netview'}
dbServerInfo[0]['wireless'] = \
        {'server': dbProdHost, 'port': dbProdNetDbPort,
            'user':'netmon', 'look':'netview'}
dbServerInfo[0]['intermapper'] = \
        {'server': dbProdHost, 'port': dbProdInterMapperPort,
            'user':'netview', 'look':'netview'}
dbServerInfo[0]['netdisco'] = \
        {'server': dbProdHost, 'port': dbProdNetdiscoPort,
            'user':'netview', 'look':'netview'}

# The development server database information.
dbServerInfo[1] = {}
dbServerInfo[1]['networking'] = \
        {'server': dbDevHost, 'port': dbDevNetDbPort, 
                'user':'netmon', 'look':'netview'}
dbServerInfo[1]['wireless'] = \
        {'server': dbDevHost, 'port': dbDevNetDbPort, 
                'user':'netmon', 'look':'netview'}
# No development db for intermapper, only have read access regardless.
dbServerInfo[1]['intermapper'] = \
        {'server': dbProdHost, 'port': dbProdInterMapperPort,
                'user':'netview', 'look':'netview'}
# No development db for netdisco, only have read access regardless.
dbServerInfo[1]['netdisco'] = \
        {'server': dbProdHost, 'port': dbProdNetdiscoPort,
                'user':'netview', 'look':'netview'}

# Next server, may not be active.
dbServerInfo[2] = {}
dbServerInfo[2]['networking'] = \
        {'server': dbDevHost2, 'port': dbDevNetDbPort2,
            'user':'netmon', 'look':'netview'}
dbServerInfo[2]['wireless'] = \
        {'server': dbDevHost2, 'port': dbDevNetDbPort2,
            'user':'netmon', 'look':'netview'}
dbServerInfo[2]['intermapper'] = \
        {'server': dbDevHost2, 'port': dbDevInterMapperPort2,
            'user':'imdatabase', 'look':'netview'}
dbServerInfo[2]['netdisco'] = \
        {'server': dbDevHost2, 'port': dbDevNetdiscoPort2,
            'user':'netview', 'look':'netview'}

# The database server we are using...
# By default this would be the production server.
dbServer = {}

# The default user is netview. This user is used by default for all
# read only access to the database. For the 'netview' account to work a
#    GRANT SELECT, REFERENCES ON -table-or-view- TO netview
# needs to be issued for the appropriate tables.
# If a password needs to be changed use:
#    ALTER ROLE netmon WITH PASSWORD 'abcdefg'

dbPasswords = []

#

# 16 Apr 2014   initial creation

gSmtpServer = "smtp.cc.umanitoba.ca"

gBugFrom = "netmon@cc.umanitoba.ca"

gBugTo = "milton@cc.umanitoba.ca"

# SNMP variables:

# Index is the campus zone id.
snmpCommunity = {};
snmpCommunity[1] = ('public', 'private');

#from . import debug
#import debug
#from . import icmplib
#from . import cmd_line_io
#from . import db_access
#from . import db_schema
#from . import db_columns
#from . import user_defaults
#from . import network_error
#from . import network_types
#from . import network_zones
#from . import buildings
#from . import networks
#from . import network_snmp
#from . import router_afl
#from . import network_log
#from . import netdisco
#from . import intermapper
# from net import sensors
# from net import wireless
# from net import consoles
# from net import network_config

# vim: syntax=python ts=4 sw=4 showmatch et :

