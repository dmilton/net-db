#\L

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 02 Mar 1013   initial creation.

############################################################################
# read_pg_pass
def read_pg_pass():

    global _dbPasswords

    # Initialize our password variable.
    _dbPasswords = []

    # Find the .pgpass file in the user home directory.
    pgPass = os.environ['HOME'] + '/.pgpass'

    # Read the file and make a dictionary of it.
    try:
        f = open(pgPass, 'rb')
        reader = csv.DictReader(f, delimiter=':', quoting=csv.QUOTE_NONE)
        for row in reader:
            _dbPasswords.append(row)
    except:
        print(("Unable to open " + pgPass + " file. Cannot continue.."))
        raise

    return
## end read_pg_pass()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

