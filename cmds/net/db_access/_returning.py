#\L

# 20 Sep 2013   initial creation.

############################################################################
# _returning
# Create a returning clause which fetches all of the fields of the
# record that was just inserted/updated.

def _returning(tableFieldInfo):

    result = ' RETURNING ' + ','.join(list(tableFieldInfo.keys()))

    return(result)
## end _returning()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

