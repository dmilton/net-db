#

# 20 Sep 2013   Move the code that converts the db result from a list of lists
#               into a list of dictionaries into a separate routine so it
#               can be used to convert the INSERT/UPDATE queries RETURNING.
#  3 May 2013   Inconsistiencies with the order of items returned by .keys()
#               caused minor changes here and in table initialization.
# 29 Mar 2013   Tables with date fields are converted by psycopg if
#               a format is not used on the select. No need to convert
#               to a datetime object.
# 21 Feb 2013   When None is the value of a cidr field (ie a vlan that
#               is not routed) conversion using ipaddr cannot occur.
# 11 Feb 2013   Ipaddr conversion must occur after the row is turned
#               into a dictionary.
# 30 Jan 2013   add ipaddr type conversion for cidr table values.
#  6 Nov 2012   update to allow a sorting order.
#  1 Nov 2012   initial creation.

############################################################################
# get_table_rows
# Get a row (or rows) from theTable which match the key value keyValue.
# Entry:    theTable - dictionary describing the table.
#            whereClause - clause to use to restrict the query to some rows.
#            sortOrder - order by clause to sort the resulting rows.
# The result is a list containing one dictionary of values for each row
# returned from the database. Each dictionary has values with keys that
# match the names of the columns in the table.

def get_table_rows(theTable, whereClause, sortOrder = '', limit = 0):

    global _dbServer

    # Start with an empty list.
    result = []

    #
    # build the SQL query.
    #
    tableKeys = list(theTable['tableFields'].keys())
    myQuery = "SELECT " + ','.join(tableKeys) + " FROM " + theTable['table']
    # Add a where clause.
    if ( whereClause > '' ):
        myQuery += " WHERE " + whereClause

    # Add the order by clause.
    if ( sortOrder > '' ):
        myQuery += " ORDER BY " + sortOrder

    # Add the limit clause.
    if ( limit > 0 ):
        myQuery += " LIMIT " + str(limit)

    # Terminate the query statement.
    myQuery += ";\n"

    #
    # Get the rows from the table.
    #
    cur = _dbServer[theTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[theTable['database']]['dbConn'].commit()
    cur.close()

    # Now process the result, converting the lists into dictionaries.
    result = _convert_result(theTable, dbResult)
    
    return(result)
## end get_table_rows()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

