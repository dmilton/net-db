#

# 22 Apr 2014   Add code to extract the version of the database server.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  2 Mar 2013   Remove dbPass. Use .pgapass instead.
#  7 Nov 2012   extend the dbServer value so multiple databases could
#               be opened during the same session.
#  2 Nov 2012   Initial writing.

############################################################################
# init_db_server(serverName, serverPort, dbName, dbUser)
#
# Entry:
#        serverName - the name of the server to establish a connection to.
#        serverPort - the TCP port number the postgres server is running on.
#        dbName - the name of the database to open.
#        dbUser - the name of the user to use for connections.
#
# Set the server to use.

def init_db_server(serverName, serverPort, dbName, dbUser):

    global _dbServer, _dbPasswords

    # Extract the password from _dbPasswords (.pgpass)
    if ( len(_dbPasswords) > 0 ):
        for pwEntry in _dbPasswords:
            if ( (pwEntry['username'] == dbUser) \
                    and (pwEntry['database'] == dbName) \
                    and (pwEntry['hostname'] == serverName) ):
                if ( pwEntry['port'] == '*' or pwEntry == serverPort ):
                    dbPass = pwEntry['password']
    else:
        # try an empty password. this shouldn't work!
        dbPass = ''

    # The design assumption here is that the tables in the database
    # all have select and/or update capability by the same user. This
    # allows for a single connection to be created now and then individual
    # cursor's for each query.
    if ( (dbName in _dbServer) == False ):
        # We haven't seen this database before.
        _dbServer[dbName] = {}
        _dbServer[dbName]['serverName'] = serverName
        _dbServer[dbName]['serverPort'] = serverPort
        _dbServer[dbName]['dbUser'] = dbUser
        _dbServer[dbName]['dbConn'] = \
            psycopg2.connect(host=serverName, port=serverPort, \
                database=dbName, user=dbUser, password=dbPass)

        # now that the database connection is open, get the version.
        cur = _dbServer[dbName]['dbConn'].cursor()
        cur.execute("SELECT version();")
        dbResult = cur.fetchall()
        cur.close()
        _dbServer[dbName]['dbVer'] = dbResult[0][0]
    
## end init_db_server()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

