#\L

# 24 Oct 2013   initial creation.

############################################################################
# get_reqd_cols
# Return a list of table columns that are "NOT NULL" values.

def get_reqd_cols(theTable):

    # field Info:
    notNullFields = []
    fieldInfo = theTable['tableFields']
    for aField in list(fieldInfo.keys()):
        if ( fieldInfo[aField]['notNull'] ):
            notNullFields.append(aField)

    return(notNullFields)
## end get_reqd_cols()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

