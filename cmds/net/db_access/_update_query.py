#

# 12 Aug 2014   Update to pass udt_name value to _quote_field so array's
#               can be formatted correctly.
# 20 Sep 2013   Update to add a returning clause.
#  4 Apr 2013   Argument to _common_keys has been renamed to indexRmForce.
# 25 Feb 2013   add initialization of tableFields if it does not exist.
#               This prevents errors when a table has restricted rights
#               for some users but not others.
# 18 Dec 2012   boolean, numeric, mac and cidr values do not have sizes.
# 18 Dec 2012   initial creation.

##############################################################################
# _update_query(theTable, rowData, whereClause)
# The record to update must exist in the database.
# Entry:
#        theTable - dictionary of information describing the table.
#        newInfo - a dictionary containing the information to be updated
#        whereClause - the comparison string to uniquely identify the row or
#                rows in the table to update.
# Exit:
#        an SQL query to update fields in the table is returned.

def _update_query(theTable, rowData, whereClause):

    # Add fields to update that are present in rowData.
    if ( ('tableFields' in theTable) == False ):
        theTable.update(init_db_table(theTable))

    tableFieldInfo = theTable['tableFields']
    indexField = theTable['index']

    commonKeys = _common_keys(tableFieldInfo, indexField, rowData)
    nothingToUpdate = ( len(commonKeys) == 0 )

    # Construct our UPDATE statement.
    updateQuery = "UPDATE " + theTable['table'] + " SET "

    first = True
    for aKey in commonKeys:
        aFieldInfo = tableFieldInfo[aKey]
        # if this is the second and subsequent field to update...
        if ( first == False ):
            updateQuery += ", "
        ## end if ( first == False )
        updateQuery += aKey + " = "
        updateQuery += _quote_field(rowData[aKey],
                aFieldInfo['type'], aFieldInfo['udt_name'])
        # boolean, numeric, mac and cidr values do not have sizes.
        if ( aFieldInfo['size'] != None and 
                len(rowData[aKey]) > aFieldInfo['size'] ):
            err = aKey + ", " + len(rowData[aKey]) 
            err += " > " + str(aFieldInfo['size'])
            raise DbException("DbFieldTooLong", err)
        ## end if ( aFieldInfo['size'] < size(newInfo[aKey] ):
        first = False
    ## end for aKey in theTable['selectKeys']:

    if ( nothingToUpdate == True ):
        raise DbException ("NothingToUpdate", \
                "No fields were provided to update")
    else: 
        if ( whereClause != '' ):
            updateQuery += " WHERE " + whereClause
        ## end if ( whereClause != '' ):
    ## end else if ( nothingUpdated == True ):

    updateQuery += _returning(tableFieldInfo)

    return(updateQuery)
## end _update_query():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

