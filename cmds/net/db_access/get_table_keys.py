#\L

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 30 Oct 2012   initial creation.

############################################################################
# get_table_keys
# Build a dictionary of keys and types for use to help parsing and
# verification in cmd_line_input.
# Entry: tableInfo - the table information dictionary.
#    3)    tableFields - a dictionary of information, one entry for every
#        field in the table itself. This is used for i/o formatting to
#        ensure that columns that require quoting are actually quoted.

def get_table_keys(tableInfo):

    global _dbServer

    result = tableInfo['tableFields']

    return(result)
## end get_table_keys()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

