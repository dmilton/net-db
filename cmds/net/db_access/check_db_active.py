############################################################################
# check_db_active()

def check_db_active():
    global _dbEnabled

    if ( _dbEnabled == False ):
        print("The networking database is currently shut down.")
        print("Please try again later.")
        sys.exit(0)
    ## end if ( _dbEnabled == False ):

## end check_db_active():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

