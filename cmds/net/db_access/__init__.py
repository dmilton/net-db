#
gVersion=''

# 20 May 2014   Remove google ipaddr library support.
#  7 Mar 2014   Add use of python3.3 ipaddress library.
# 18 Oct 2013   Remove debug import, no longer required.
# 28 Feb 2013   Add import for datetime class.
# 30 Jan 2013   Add import for ipaddr.
# 21 Jan 2013   Nothing to import from networkingConfig any more.
# 13 Nov 2012   Only import netLib from networkingConfig.
#  7 Nov 2012   Expand/change the definition for dbServer so a more
#               granular access to tables could be defined.
#  5 Nov 2012   Update to reflect new functionality where the table
#               structure is taken directly from the schema rather
#               than from a user defined dictionary.
#  1 Sep 2010   Add information about having compound and sequence
#               key types defined for a table.
#               Add WhereClause to provide generic building of the
#               where clause of an SQL query.
# 31 Aug 2010   Need more of networking config. Replace the single
#               value import with the entire module. Add global
#               variable dbServer.

import csv
import os
from datetime import datetime

# Python 3.3 ipaddress library.
import ipaddress

# postgres database library.
import psycopg2

# Get the user name.
from getpass import getuser

# Tables require various details to be accessed. Here are the dictionary
# layouts which are required to deal with the tables.

# tableInfo = {
#   'database'      the name of the database the table resides in.
#   'table'         the name of the table.
#   'role'          the userid/role which has read/write access.
#   'index'         the field name of the index. If compound then there
#                   are multiple fields in the table which uniquely
#                   identify a single record in the table.
# * 'tableFields'   a dictionary of information, one entry for every field
#                   in the table itself. This is used for formatting to ensure
#                   values in a select, update, or insert are quoted when
#                   necessary. Checks against length are also made.
# }
# * --> field is added by db_access.init_db_table call.
#
# Once a table has been iniitalized the tableFields dictionary contains
# one key/tag for each column in the table. Each dictionary contains two
# values which are taken from a query against the DB schema itself:
# SELECT column_name,data_type,character_maximum_length,column_default
#     FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'theTable'
# The keys are 'type', 'size', and 'default'.

############################################################################
# Module global variables:

# What host/port is to be used for database access?
# A dictionary with one key for each database that is initialized.
# Each entry in the dictionary is another dictionary containing
# the following:
#   theServer  FQDN:TCP-PORT
#   For each table in the database the following entry must exist:
#   role        the user name/role for access to this table.
#   password    the password for the above role.

_dbServer = { } 

_dbEnabled = True

_dbPasswords = []

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

from . import DbException
from . import _common_keys
from . import _convert_result
from . import _insert_query
from . import _log_change
from . import _quote_field
from . import _returning
from . import _update_query
from . import change_table_rows
from . import check_db_active
from . import close_db_conn
from . import copy_table_rows
from . import db_error_handler
from . import delete_table_rows
from . import get_db_version
from . import get_reqd_cols
from . import get_row_count
from . import get_table_keys
from . import get_table_rows
from . import init_db_server
from . import init_db_table
from . import make_list
from . import move_table_rows
from . import new_table_row
from . import read_pg_pass
from . import set_table_row

