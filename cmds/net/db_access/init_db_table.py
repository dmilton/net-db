#\L

# 11 Aug 2014   Add udt_name to the information for each column so
#               the type for values of an array can be determined.
#               For clarity, change the column information extract
#               to use the column names from the schema rather than
#               renaming them. Did not change the name in the dictionary
#               as this will potentially cause problems elsewhere.
# 24 Oct 2013   Add is_nullable to the information for each column.
# 29 Mar 2013   Views do not have index information so failure on
#               an attempt is incorrect behaviour.
#               Date fields being converted to datetime objects
#               so sql format needs to be removed.
#  6 Mar 2013   Add check for inet fields and build a list for conversion.
# 27 Feb 2013   Add check for date fields and build a list for conversion.
# 30 Jan 2013   Add check for cidr fields and build a list so these
#               values can be converted into ipaddr objects.
# 30 Oct 2012   initial creation.

############################################################################
# init_db_table
# Perform Initialization of the table structure information given the
# database and table name.
# Entry: tableInfo - the table information dictionary.
# Exit: The tableInfo dictionary has new entries which are for the
#        database routines:
#    1)    tableFields - a dictionary of information, one entry for every
#        field in the table itself. This is used for i/o formatting to
#        ensure that columns that require quoting are actually quoted.
#    2)    cidrFields - list of fields which are of type cidr.
#    3)    inetFields - list of fields which are of type inet.
#    4)    indexFields - primary key

def init_db_table(tableInfo):

    global _dbServer

    # extract the table name from tableInfo.
    dbName = tableInfo['database']
    tableName = tableInfo['table']
    result = {}

    # Get the information regarding this table from the database.
    colNames = "column_name,data_type,udt_name,character_maximum_length,"
    colNames += "column_default,is_updatable,is_nullable"
    myQuery = "SELECT " + colNames + " FROM INFORMATION_SCHEMA.COLUMNS "
    myQuery += "WHERE table_name = '" + tableName + "';"
    
    cur = _dbServer[dbName]['dbConn'].cursor()
    cur.execute(myQuery)
    tableFieldInfo = cur.fetchall()
    cur.close()

    # If we get no response, exit with an error.
    if len(tableFieldInfo) == 0:
        raise DbException ("NoSchemaAccess", \
                dbName + "/" + tableName + '\n' + \
                str(_dbServer[dbName]['dbConn']) )
    ## end if len(tableFieldInfo) == 0:

    # The table information is similar to:
    # +-------------+--------------------------------+----------------------+
    # | column_name |          data_type             | character_max_length |
    # +-------------+--------------------------------+----------------------+
    # | a-char-var     | character varying           |                   32 |
    # | an-int-var     | integer                     |                      |
    # | active         | boolean                     |                      |
    # | lan_mac        | macaddr                     |                      |
    # | network        | cidr                        |                      |
    # | removal_date   | date                        |                      |
    # | create_date    | timestamp without time zone |                      |
    # | assign_date    | timestamp with time zone    |                      |

    # Now collect the index information.
    colNames = "i.relname as index_name,"
    colNames += "array_to_string(array_agg(a.attname), ', ') as column_names"
    myQuery = "SELECT " + colNames + " "
    myQuery += "FROM pg_class t, pg_class i, pg_index ix, pg_attribute a "
    myQuery += "WHERE t.oid = ix.indrelid AND i.oid = ix.indexrelid "
    myQuery += "AND a.attrelid = t.oid AND a.attnum = ANY(ix.indkey) "
    myQuery += "AND t.relkind = 'r' AND t.relname = '" + tableName + "' "
    myQuery += "GROUP BY t.relname, i.relname "
    myQuery += "ORDER BY t.relname, i.relname;"

    cur = _dbServer[dbName]['dbConn'].cursor()
    cur.execute(myQuery)
    tableIndexInfo = cur.fetchall()
    cur.close()

    # The index query result looks like this:
    # +----------------------+--------------+
    # |      index_name      | column_names |
    # +----------------------+--------------+
    # | network_assigns_bldg | bldg_code    |
    # | network_assigns_pkey | name         |
    # +----------------------+--------------+

    # The index query can also look like this:
    # +------------------+--------------------------+
    # |    index_name    |       column_names       |
    # +------------------+--------------------------+
    # | column_info_pkey | table_column, table_name |
    # +------------------+--------------------------+

    # If we get no response we cannot determine the primary key.
    if len(tableIndexInfo) == 0:
        primaryKey = ""
    else:
        # We only care about the primary key at the moment.
        for anIndex in tableIndexInfo:
            (indexName, colNames) = anIndex
            if "_pkey" in indexName:
                primaryKey = colNames

    # Initialize our field information.
    cidrFields = []        # These need conversion manually on each select.
    inetFields = []
    tableFields = {}    # details of the table itself
    first = True
    for aField in tableFieldInfo:
        (column_name, data_type, udt_name, character_maximum_length,
                column_default, is_updatable, is_nullable) = aField

        # Create the field info dictionary.
        fieldInfo = {}
        fieldInfo['type'] = data_type
        # Field prefixed with an _ if the type is an array.
        fieldInfo['udt_name'] = udt_name
        fieldInfo['size'] = character_maximum_length
        fieldInfo['default'] = column_default
        fieldInfo['update'] = ( is_updatable == "YES" )
        fieldInfo['notNull'] = ( is_nullable == "NO" )
        if ( column_name in primaryKey ):
            fieldInfo['pkey'] = True
            fieldInfo['update'] = False
        else:
            fieldInfo['pkey'] = False
        if ( data_type == 'cidr' ):
            cidrFields.append(column_name)
        elif ( data_type == 'inet' ):
            inetFields.append(column_name)
        ## end elif.
        tableFields[column_name] = fieldInfo

    ## end for aField in tableFieldInfo:

    # Create a dictionary to add to tableInfo.
    result['tableFields'] = tableFields
    result['cidrFields'] = cidrFields
    result['inetFields'] = inetFields
    result['indexFields'] = primaryKey

    return(result)
## end init_db_table()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

