#

# 23 May 2013   for x in y: will skip items in y if you delete
#               an item from y inside the loop. A copy of y is
#               required in order to allow field deletion.
# 14 May 2013   Change loop through the common keys so index out of range
#               errors cannot occur when a key gets deleted from the list.
#  4 May 2013   Update to handle multi-field keys properly.
#               Removed the indexRmForce argument as it doesn't make
#               sense. The only time an Index should not be part of
#               a query is when it is an insert with a sequence.
# 28 Feb 2013   initial creation.

##############################################################################
# _common_keys(tableFieldInfo, indexField, columnData)
# Determine the set of keys which are common to both the table columns
# and the data to be inserted or updated into the table.
# will have the values of rowData.
#
# Entry:
#        tableFieldInfo - dictionary describing the columns of the table.
#        indexField - name of the field which is the primary table index.
#        columnData - a dictionary containing the values to be inserted/updated.
#
# Exit:
#        Returns a list of field names which are common to the table itself,
#        have valid values in columnData, and the index value will be excluded
#        if it is a sequence.

def _common_keys(tableFieldInfo, indexField, columnData):

    # Find the keys in common between the table and the new column data.
    commonKeys = list( set(tableFieldInfo.keys()) & set(columnData.keys()) )

    # Assume the index does not need to be removed.
    excludeIndex = False
    indexFieldList = indexField.split(',')
    if ( len(indexFieldList) == 1 ):
        # Sequence index fields should be excluded from a typical insert.
        if ( tableFieldInfo[indexField]['default'] != None ):
            if ( "nextval" in tableFieldInfo[indexField]['default'] ):
                # This is a sequence, exclude the field from the Insert.
                excludeIndex = True

    # Copy common keys to the result
    result = list(commonKeys)
    for aKey in commonKeys:
        if ( aKey == indexField and excludeIndex == True ):
            # Remove the index key from the set.
            del result[result.index(aKey)]
        elif ( columnData[aKey] == None ):
            # Remove columnData keys that have no value.
            del result[result.index(aKey)]

    return(result)
## end _common_keys():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

