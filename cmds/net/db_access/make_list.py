#\

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 17 Jan 2013   Check all values in aTableRow which appear in keyList
#               so a 'None' is set to an empty string.
#  6 Nov 2012   Initial creation.

##############################################################################
# make_list(aTableRow, keyList)
# Entry:
#        aTableRow - dictionary of a single row's data.
#        keyList - list of fields desired in result.
# Exit: a list of the values for keys in keyList taken from aTableRow.

def make_list(aTableRow, keyList):

    result = []
    for i in range(len(keyList)):
        if ( aTableRow[ keyList[i] ] == None ):
            result.append( '' )
        else:
            result.append( aTableRow[ keyList[i] ] )
        ## end else if ( aTableRow[ keyList[i] ] == None ):
    ## end for i in range(len(keyList))

    return(result)
## end make_list()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

