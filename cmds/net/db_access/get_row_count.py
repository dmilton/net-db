#\L

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Dec 2012   initial creation.

############################################################################
# get_row_count
# Get the count of rows which match the where clause
# Entry:    theTable - dictionary describing the table.
#            whereClause - clause to use to restrict the query to some rows.
# The result is a list containing one dictionary of values for each row
# returned from the database. Each dictionary has values with keys that
# match the names of the columns in the table.

def get_row_count(theTable, whereClause=''):

    global _dbServer

    # assume zero rows.
    result = 0

    #
    # build the SQL query.
    #
    myQuery = "SELECT count(*) FROM %s" % (theTable['table'],)
    # Add a where clause.
    if ( whereClause > '' ):
        myQuery += " WHERE " + whereClause
    ## end if ( whereClause > '' ):

    # Terminate the query statement.
    myQuery += ";\n"

    # Run the query against the database.
    cur = _dbServer[theTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[theTable['database']]['dbConn'].commit()
    cur.close()

    # Query results are always a list of tuples.
    result = int(dbResult[0][0])

    return(result)
## end get_row_count()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

