#\L

#  9 Sep 2014   Add result of the number of rows deleted.
# 24 Apr 2014   Add a log of the query.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  4 May 2013   Remove limit as it is not applicable to delete.
# 25 Feb 2013   Change the query creation to simplfy reading.
#               Removed the fetchall as there's nothing to fetch when
#               a row is deleted from a table.
#  6 Nov 2012   initial creation.

############################################################################
# delete_table_rows
# Delete a row (or rows) from the table. Returns count of the number of
# rows deleted.
#
# Entry:theTable - dictionary describing the table.
#        whereClause - where clause to uniquely identify the row(s) to delete.
# Exit:    the rows specified have been deleted.

def delete_table_rows(theTable, whereClause):

    global _dbServer

    myQuery = "DELETE FROM " + theTable['table']
    myQuery += " WHERE " + whereClause + ";"

    cur = _dbServer[theTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    result = cur.rowcount;
    _dbServer[theTable['database']]['dbConn'].commit()
    cur.close()

    try:
        _log_change(myQuery)
    except:
        pass

    return(result)
## end delete_table_rows()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

