#

# 29 Mar 2013   Added new error messages.
# 11 Jul 2011   initial creation.

############################################################################
# Database Error handler.
# Use exit code values of 100 through 199

def db_error_handler(theError):
    result = 0

    if ( theError.name == "DbNoConnect" ):
        print("The database server may be down.")
        print("The error from the database was:")
        result = 100
    elif ( theError.name == "InvalidParameter" ):
        print("Invalid data type in parameters, list required.")
        result = 101
    elif ( theError.name == "NoSchemaAccess" ):
        print("The table permissions are incorrect, cannot read schema.")
        result = 102
    elif ( theError.name == "NoIndexAccess" ):
        print("The table has no index information.")
        result = 103
    elif ( theError.name == "DbFieldTooLong" ):
        print("The field was longer that the table column can hold.")
        result = 104
    elif ( theError.name == "NothingToUpdate" ):
        print("The update request did not contain anything to update.")
        result = 105
    else:
        print("An unknown database error error has occurred...")
        result = 199
    ## end if ( theError.name = X ):
    print( (str(theError.detail)) )

    return(result)
## end db_error_handler():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

