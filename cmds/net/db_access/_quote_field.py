#

# 12 Aug 2014   Update to use udt_name value to _quote_field so array's
#               can be formatted correctly. Now a recursive routine since
#               this calls itself for each item in an array.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  1 Sep 2010   Add check for compound index types and also
#               add SERIAL types.

##############################################################################
# _quote_field(field_data, data_type)
# The assumption made is that any field that is not numeric in
# nature requires quotes.
#
# Entry:
#       field_data - the field to quote if necessary.
#       data_type - the type (data_type) of field so we know to quote or not.
#       data_type_name - the udt_name field from the database table info.
# Exit:
#        The result is field_data as a string. If the field itself is
#        a string in the table then the result will contain quotes.

def _quote_field(field_data, data_type, data_type_name):

    is_number = ( data_type == 'SERIAL' or 'real' in data_type or
            'int' in data_type_name or 'float' in data_type_name )
    # assume the data needs quotes.
    quote_char = "'"
    if ( data_type == 'ARRAY' ):
        # Array needs '{ nbr, nbr }' as result for numeric and
        # '{ "str", "str" }' as result for all others.
        if ( len(field_data) == 0 ):
            # was passed a list with zero elements.
            result = 'NULL'
        else:
            result = "'{ " + _quote_it(field_data[0], '"', is_number)
            for a_field_item in field_data[1:]:
                result += ', ' + _quote_it(a_field_item, '"', is_number)
            result += " }'"

    else:

        result = _quote_it(field_data, "'", is_number)

    return(result)
## end _quote_field()

# This was split out since it needed to be in
# two places in the code.
def _quote_it(the_data, the_quote, is_number):
    if ( is_number ):
        result = str(the_data)
    else: 
        result = the_quote + str(the_data) + the_quote
    return(result)
## end def _quote_it():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

