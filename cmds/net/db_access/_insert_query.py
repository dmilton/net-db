#

# 12 Aug 2014   Update to pass udt_name value to _quote_field so array's
#               can be formatted correctly.
# 28 Feb 2013   filter out any keys which are not common between the
#               columns in the table and the newRow to be added.
# 25 Feb 2013   add initialization of tableFields if it does not exist.
#               This prevents errors when a table has restricted rights
#               for some users but not others.
# 21 Feb 2013   initial creation.

##############################################################################
# _insert_query(theTable, newInfo)
# Generate an insert query for the table described by theTable which
# will have the values of rowData.
#
# Entry:
#        theTable - dictionary describing the table.
#        rowData - a dictionary containing the information to be inserted
#        returning - list of fields to return after insert completes.
#
# Exit: returns the SQL query to perform an insert into the table.

def _insert_query(theTable, newRow, returning=""):

    # extract the basics of the table. tableFields, index, database
    # cidrFields, inetFields, role, table.
    if ( ('tableFields' in theTable) == False ):
        theTable.update(init_db_table(theTable))
        print(("Missing table initialization for table: " + theTable['table']))

    tableFieldInfo = theTable['tableFields']
    indexField = theTable['index']
    # Figure out the field names that need to be updated.
    commonKeys = _common_keys(tableFieldInfo, indexField, newRow)

    # Start with the insert.
    insertQuery = "INSERT INTO " + theTable['table'] + "\n"
    # Add the list of fields.
    insertQuery += " (" + ', '.join(commonKeys) + ") \n"

    # Construct our list of field values.
    valueList = " VALUES\n("
    first = True
    for aKey in commonKeys:
        fieldInfo = tableFieldInfo[aKey]
        # if this is the second and subsequent field to update...
        if ( first == True ):
            first = False
        else:
            valueList += ", "
        ## end else if ( first == True ):
        valueList += _quote_field(newRow[aKey],
                fieldInfo['type'], fieldInfo['udt_name'])
    ## end for aKey in tableInfo.keys()
    valueList = valueList + ") \n"

    # Add our list of values to our insert query.
    insertQuery += valueList

    insertQuery += _returning(tableFieldInfo)

    # Terminate the SQL statement.
    insertQuery += ";\n"

    return(insertQuery)
## end _insert_query():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

