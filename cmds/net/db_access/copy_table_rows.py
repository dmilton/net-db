#\L

# 24 Apr 2014   Add a log of the query.
# 23 Sep 2013   Add code to use the returning clause of insert
#               so a second select is no longer required.
# 18 Apr 2013   initial creation.

############################################################################
# copy_table_rows
# Copy a record from one table to another. The default behaviour is to
# move only one record based on the where clause provided.
#
# Entry:
#        srcTable - dictionary describing the source table.
#        destTable - dictionary describing the destination table.
#        whereClause - where clause describing source rows.
#        sortOrder - the order rows are to be selected from srcTable.
#        limit - restrict source rows to this number.
# Exit:    The row has been copied from srcTable to destTable.
#        The response returned is the values from the destTable.

def copy_table_rows(srcTable, destTable, whereClause, sortOrder='', limit=1):
    result = []
    
    # insert into network_links 
    #        ( name,vlan_tag,network,bldg_code,prime_net,assign_date,assign_by,
    #        net_type)
    #    (select name,vlan_tag,network,bldg_code,prime_net,assign_date,
    #        assign_by,net_type 
    #        from network_free
    #        where net_type = 'link'
    #        order by network
    #        limit 2
    #    ) RETURNING 
    #        name,vlan_tag,network,bldg_code,prime_net,
    #        assign_date,assign_by,net_type;

    # extract the basics of the table. tableFields, index, database
    # cidrFields, role, table.
    if ( ('tableFields' in srcTable) == False ):
        srcTable.update(init_db_table(srcTable))
        print(("Missing table initialization for table: " + srcTable['table']))
    if ( ('tableFields' in destTable) == False ):
        destTable.update(init_db_table(destTable))
        print(("Missing table initialization for table: " + destTable['table']))

    # Figure out the field names that need to be updated.
    # set(a) & set(b) returns common items between two lists a and b.
    # list(x) returns a list from set x.
    commonFields = ', '.join( list( set( srcTable['tableFields'].keys() ) \
            & set( destTable['tableFields'].keys() ) ) )

    # Start with the insert.
    myQuery = "INSERT INTO " + destTable['table'] + " \n"
    # Add the list of fields.
    myQuery += " (" + commonFields + ") \n"
    # Now construct the SELECT inside parenthesis.
    myQuery += "(SELECT " + commonFields + " \n"
    myQuery += "FROM " + srcTable['table'] + " \n"
    myQuery += "WHERE " + whereClause + " \n"
    if ( sortOrder > '' ):
        myQuery += "ORDER BY " + sortOrder + " \n"
    if ( limit > 0 ):
        myQuery += "LIMIT " + str(limit)
    myQuery += ") RETURNING " + \
        ','.join(list(destTable['tableFields'].keys())) + ";"

    # Process the query we generated, result is new row list in destTable.
    # This duplicates the rows from srcTable into destTable.
    cur = _dbServer[destTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[destTable['database']]['dbConn'].commit()
    cur.close()

    result = _convert_result(destTable, dbResult)

    try:
        _log_change(myQuery)
    except:
        pass

    return(result)
## end copy_table_rows()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

