#\L

# 12 Aug 2014   Update to pass udt_name value to _quote_field so array's
#               can be formatted correctly.
# 21 May 2014   initial creation.

############################################################################
# change_table_rows
# Change the value of all rows matching one value with another.
# Entry:
#       the_table   dictionary describing the table.
#       column_name name of column to change the value for.
#       old_value   the old value to use for the update.
#       new_value   the new value to use for the update.
#       cascade     cascade the change to all effected tables.
# Exit:
#       all rows which have the old value in the specified column
#       now have the new value.

def change_table_rows(the_table, column_name, old_value, new_value, cascade):
    global _dbServer, _changeLog

    # Create an update query.
    updateColumn = {}
    updateColumn[column_name] = new_value

    # Update query
    myQuery = "UPDATE " + the_table['table'] + " SET " + column_name
    myQuery += " = " + _quote_field(new_value,
            the_table['tableFields'][column_name]['type'],
            the_table['tableFields'][column_name]['udt_name'])
    # where clause:
    myQuery += " WHERE " + column_name + " = " + _quote_field(old_value,
            the_table['tableFields'][column_name]['type'],
            the_table['tableFields'][column_name]['udt_name'])
    myQuery += ";"

    # Process the query we generated.
    cur = _dbServer[the_table['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[the_table['database']]['dbConn'].commit()
    cur.close()

    try:
        _log_change(myQuery)
    except:
        pass

    return(dbResult)
## end change_table_rows()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

