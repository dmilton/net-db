#\L

# 13 May 2014   Add call to close the change log should it be open.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 12 Mar 2013   Add check for key incase close called on alredy closed db.
#  8 Nov 2012   initial creation.

############################################################################
# close_db_conn
def close_db_conn(dbName):

    global _dbServer

    if ( 'dbConn' in _dbServer[dbName] ):
        _dbServer[dbName]['dbConn'].close()
        del _dbServer[dbName]['dbConn']

    try:
        _close_change_log()
    except:
        pass

    return
## end close_db_conn()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

