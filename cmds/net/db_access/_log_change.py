#

# 13 May 2014   Build the query directly so the normal table initialization
#               is not required. This allows a connection to the db to be
#               established using the 'netlog' user.
# 24 Apr 2014   initial writing.

###############################################################################
# Change log table.
#
_changeLog = {
    'database': 'networking',
    'role': 'netlog',
    'table': 'change_log',
    'index': 'change_who,change_when'
}

############################################################################
# _log_change
# Add the change log to the database.
# Entry: theChange - the change that was made.
# Exit: the change has been logged.

def _log_change(theChange):
    """
    theChange = "made a change to the database"
    # 
    _log_change(theChange)
    """

    global _dbServer, _changeLog

    if ( 'dbConn' not in _changeLog ):
        _open_change_log()

    # Make the query to update the change table.
    myQuery = "INSERT INTO " + _changeLog['table'] + "\n"
    myQuery += " (change_who,change_what) VALUES\n("
    myQuery += "'" + getuser() + "', %s);\n"

    # Insert the change into the table.
    curs = _changeLog['dbConn'].cursor()
    curs.execute(myQuery, (theChange,))
    dbResult = cur.fetchall()
    _changeLog['dbConn'].commit()
    curs.close()

    return
## end def _log_change():

############################################################################
# _open_change_log
# Create a connection to the networking database for the change log.
# Entry: none.
# Exit: a connection has been established.

def _open_change_log():
    global _dbServer, _changeLog

    if ( 'dbConn' not in _changeLog ):
        # Open a separate connection to the database for the netlog user.
        _changeLog['serverName'] = _dbServer['networking']['serverName']
        _changeLog['serverPort'] = _dbServer['networking']['serverPort']
        _changeLog['dbUser'] = 'netlog'
        _changeLog['dbConn'] = psycopg2.connect(
                host=serverName, port=serverPort, database='networking',
                user='netlog', password='qr3fyd4z')

## end def _open_change_log():

############################################################################
# _close_change_log
# Close a connection to the networking database for the change log.
# Entry: none.
# Exit: any existing connection for the change log is closed.

def _close_change_log():
    global _dbServer, _changeLog

    if ( 'dbConn' in _changeLog ):
        _changeLog['dbConn'].close()
        del _changeLog['dbConn']

## end def _close_change_log():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

