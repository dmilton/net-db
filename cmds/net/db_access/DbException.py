#
# 15 Nov 2012   moved to the error handler where it belongs.

############################################################################
# Database Exception class.

class DbException(Exception):
    """Class for all exceptions raised by the database."""

    def __init__(self, name, detail):
        self.name = name
        self.detail = detail
        return
    ## end def __init__(self)

    def __str__(self):
        return repr(self.detail)
    ## end def __str__(self):

## end class DbException():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

