#\L
#  5 Nov 2012   initial creation.

############################################################################
# new_table_row
# Create a dictionary containing empty values for a row in the specified table.

def new_table_row(theTable):

    # Start with an empty dictionary.
    result = {}

    # Loop through the valid keys of the table and assign each one None.
    for aRow in list(theTable['tableFields'].keys()):
        result[aRow] = None

    return(result)
## end new_table_row()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

