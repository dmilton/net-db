#\L
# 20 May 2014   Remove google ipaddr library support.
#  7 Mar 2013   Start using python 3.3 ipaddress library.
# 20 Sep 2013   initial creation.

############################################################################
# _convert_result
# Take the result from the database and convert to a dictionary. Also
# convert the postgres inet and cidr data types to ipaddr types.

def _convert_result(theTable, dbResult):

    result = []

    # Now process the result, converting lists into dictionaries.
    cidrFields = theTable['cidrFields']
    inetFields = theTable['inetFields']
    for aRow in dbResult:
        # Make the row values list into a dictionary.
        dictRow = dict(list(zip(list(theTable['tableFields'].keys()), aRow)))
        # Check and convert any cidr fields.
        if ( len(cidrFields) > 0 ):
            for aField in cidrFields:
                # If we have a value for the cidr field, convert it.
                if ( dictRow[aField] != None ):
                    dictRow[aField] = ipaddress.ip_network(dictRow[aField])
        # Check and convert any inet fields.
        if ( len(inetFields) > 0 ):
            for aField in inetFields:
                # If we have a value for the cidr field, convert it.
                if ( dictRow[aField] != None ):
                    dictRow[aField] = ipaddress.ip_address(dictRow[aField])
        result.append( dictRow )
    ## end for aRow in dbResult:

    return(result)
## end _convert_result()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

