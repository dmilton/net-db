#\L

# 18 Apr 2013   initial creation.

############################################################################
# move_table_rows
# Move rows from one table to another.
# Entry:
#       srcTable - dictionary describing the source table.
#       destTable - dictionary describing the destination table.
#       whereClause - where clause describing source rows.
#       sortOrder - the order rows are to be selected from srcTable.
#       limit - restrict source rows to this number.
# Exit: The row has been copied from srcTable to destTable.
#       The response returned is the values from the destTable.

def move_table_rows(srcTable, destTable, whereClause, sortOrder='', limit=1):

    # Copy the rows.
    result = copy_table_rows(srcTable, destTable, whereClause, sortOrder, limit)

    # Noww delete them from srcTable
    delete_table_rows(srcTable, whereClause)

    return(result)
## end move_table_rows()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

