#\L

# 24 Apr 2014   Add a log of the query.
# 20 Sep 2013   Return a result by adding a RETURNING clause to the
#               INSERT/UPDATE query.
#  8 Apr 2013   No longer return a result. Since whereClause is optional
#               there is no way to reliably determine what was just
#               inserted or updated. This task must be the responsibility
#               of the calling routines.
# 25 Feb 2013   Make whereClause optional and test for an empty string
#               so an insert of a new value in a table with SERIAL
#               indexes will work cleanly.
#  5 Nov 2012   initial creation.

############################################################################
# set_table_row
# Set the values in a table row. Will perform an INSERT or UPDATE as
# appropriate. The result is the contents of the row after the update.
# Entry:theTable - dictionary describing the table.
#        rowData - dictionary of column values for a single row.
#        whereClause - uniquely identify a row in the table to update.
# Exit: the data has been inserted or updated in the database. It is up
#        to the calling routines to fetch the updated data if this is required.

def set_table_row(theTable, rowData, whereClause=''):
    global _dbServer, _changeLog

    # Start by attempting to get the row to see if it already exists.
    rowCount = 0
    if ( whereClause != '' ):
        rowCount = get_row_count(theTable, whereClause)
    ## end if ( whereClause != '' ):

    # Did we get a result?
    if ( rowCount == 0 ):
        # Row does not exist so this is an INSERT.
        myQuery = _insert_query(theTable, rowData)
    else:
        # Row exists so this is an UPDATE.
        try:
            myQuery = _update_query(theTable, rowData, whereClause)
        except:
            # If we get here there's nothing to do as the only failure
            # result from _update_query is there are no field to update.
            myQuery = ''
    ## end else if ( len(dbResult) == 0 ):
    
    if (myQuery > ''):
        # Process the query we generated, result is newly inserted/updated
        # table values.
        cur = _dbServer[theTable['database']]['dbConn'].cursor()
        cur.execute(myQuery)
        dbResult = cur.fetchall()
        _dbServer[theTable['database']]['dbConn'].commit()
        cur.close()
    ## end if (myQuery > ''):

    result = _convert_result(theTable, dbResult)

    try:
        _log_change(myQuery)
    except:
        pass

    return(result)
## end set_table_row()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

