#\L

# 16 Apr 2014   initial creation.

############################################################################
# get_db_version
# Entry: dbName - name of the database.
# Exit: result - the output of SELECT version();
# Exceptions:

def get_db_version(dbName):
    global _dbServer

    if ( dbName in _dbServer ):
        if ( 'dbVer' in _dbServer[dbName] ):
            result = _dbServer[dbName]['dbVer']
        else:
            raise ValueError("The database " + dbName + 
                " is not initialized.")
    else:
        raise ValueError("The database " + dbName + " is unknown.")

    return(result)
## end get_db_version()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

