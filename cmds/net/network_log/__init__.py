#
gVersion=''

from textwrap import wrap
from getpass import getuser

from net.network_error import NetException

# All entry types must be in this list of the log command will bail. This
# will prevent getting log entry types into the database which we don't
# know how to format for output.
logEntryTypes = [ 'netlog', 'unixlog', 'phonelog' ]

# Size of our output 'window'
pgWidth = 76
pgLines = 24

###############################################################################
# Network Types table.
#
_networkLog = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'network_log',
    'index': 'who, logtime'
}

# All entry types must be in this list of the log command will bail. This
# will prevent getting log entry types into the database which we don't
# know how to format for output.
logEntryTypes = [ 'up', 'down', 'config', 'netlog', 'unixlog', 'phonelog' ]

from . import initialize
from . import find_who

# vim: syntax=python ts=4 sw=4 showmatch et :

