#
############################################################################
# find_who()
#   figure out who is running this command.
# 

def find_who():
    """
    Determine the user name of the person running this command.
    """

    # Get the username from the environment...
    try:
        result = getuser()
    except:
        print("Something is wrong with your account.")
        raise
    ## end try

    return(result)
## end find_who()

# vim: syntax=python ts=4 sw=4 showmatch et :

