#

# 20 Feb 2013    Initial writing.

##############################################################################
# initialize()
# Perform initialization process for network log table.
# Entry: none. initializes internal variables for use.
# Exit: network log fields are now ready to use.

def initialize():
    """
    initialize the network log table access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _networkLog
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Table for the various types on network address allocations.
    _networkLog.update(db_access.init_db_table(_networkLog))

    _tables_initialized = True

## end initialize()


# vim: syntax=python ts=4 sw=4 showmatch et :

