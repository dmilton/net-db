############################################################################
# get_debug()                                                               #

def get_debug():
    """ Return the current debug level for the debugging output.
    """
    global _gDebug
    return(_gDebug)
##end get_debug

# vim: syntax=python ts=4 sw=4 showmatch et :

