
############################################################################
# debug_msg(level, message)

def debug_msg(debugMsgLevel, debugMessage):
    """Produce output for debugging purposes.
        The debug level is the level of debugging output to generate.
        As long as the the set debug level is greater than the level
        provided as argument, output is generated.
    """
    global _gDebug

    if ( _gDebug >=  debugMsgLevel ):
        print(debugMessage)

##end debug_msg

# vim: syntax=python ts=4 sw=4 showmatch et :

