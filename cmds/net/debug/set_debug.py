
#
############################################################################
# set_debug(level)
# Change the debul level to 'level'
# Entry:
#       level - the new level to set debug to.
# Exit:
#       the debug level has been set. If not-zero a message is displayed.

def set_debug(level):
    """ Set the debug level for the debugging output.
    """
    global _gDebug

    _gDebug = int(level)

    print( "debug level set to: " + str(_gDebug) )

##end set_debug

# vim: syntax=python ts=4 sw=4 showmatch et :

