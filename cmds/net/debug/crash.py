#

# 24 Apr 2014   Expand entire command line as part of the message.
# 16 Apr 2014   Change version to use a dictionary containing more
#               details about the environment.
# 3 Jan 2012    Changed from/to code to use variables.

##############################################################################
# crash...
def crash(cmdLine, traceInfo, cmdVersion=""):
    global BUGFROM, BUGTO, SMTPSERVER

    # Find the name of the server we are running on.
    kernel, hostname, release, version, hardware = uname()
    try:
        host, domain = hostname.split('.', 1)
    except ValueError:
        # solaris doesn't return a host with domain name in the hostname field.
        host = hostname
    ## end try
    
    # Get the name of the command.
    cmdName = basename(cmdLine[0])

    # Get the user name.
    cmdUser = getuser()

    # Change the argument list into a space separated string.
    first  = True
    cmdArgs = ""
    for anArg in cmdLine[1:]:
        if first:
            cmdArgs = anArg
            first = False
        else:
            cmdArgs = cmdArgs + ' ' + anArg

    # Make a reasonable email message subject line.
    subject = cmdUser + '@' + host + '% ' + cmdName + ' traceback'

    # Compose our email message.
    global BUGFROM, BUGTO
    message = "From: " + BUGFROM + "\n"
    message += "To: " + BUGTO + "\n"
    message += "Subject: " + subject + "\n\n"

    # spit out the entire command line.
    message += ' '.join(cmdLine) + "\n"
    if ( type(cmdVersion) == dict ):
        for aKey in list(cmdVersion.keys()):
            message += aKey + ":" + cmdVersion[aKey] + "\n"
    else:
        message += cmdVersion + "\n"
        message += "Hardware Info:" + kernel + "\n" + hostname + "\n" \
                    + release + "\n" + version + "\n" + hardware

    message += "\n"
    message += text(traceInfo)

    # translate bugTo into a list.
    recipientList = []
    for aRecipient in BUGTO.split(','):
        recipientList += [aRecipient]

    # Now send using SMTP.
    server = smtplib.SMTP(SMTPSERVER)
    server.sendmail(BUGFROM, recipientList, message)
    server.quit()

    # Print something for the user.
    print("\nA fatal error has occurred and the action(s) you requested cannot")
    print("be performed. The crash has been recorded or further investigation.")

## end crash()

# vim: syntax=python ts=4 sw=4 showmatch et :

