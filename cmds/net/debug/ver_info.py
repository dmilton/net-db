#\L

# dd mmm yyyy   initial creation.

############################################################################
# ver_info
# Entry:
#       cmdName - name of the command.
#       modVer - version of the module.
#       wrapVer - version of the command template wrapper.
# Exit: a dictionary of values regarding the current environment.
# Exceptions:

def ver_info(cmdName, modVer, wrapVer, schemaVer, dbVerInfo):
    result = {}

    # Collect as much version information as we can.
    pUname = platform.uname()
    result['os_info'] = "Host: " + pUname.node + "\n"
    result['os_info'] += "OS: " + pUname.system + " " + pUname.release+ "\n"
    result['os_info'] += "Hardware: " + pUname.machine
    pBuild = platform.python_build()
    result['py_info'] = "Python version: " + pBuild[0] + " " + pBuild[1]
    if ( cmdName > '' and modVer > '' and wrapVer > '' ):
        result['cmd_info'] = cmdName + ": module " + modVer
        result['cmd_info'] += ", main " + wrapVer
    else:
        result['cmd_info'] = "command version unavailable."
    result['schema_version'] = "Schema: " + str(schemaVer['major_version']) \
            + "." + str(schemaVer['minor_version']) \
            + " - " + str(schemaVer['reference_date'])
    if ( type(dbVerInfo) == dict ):
        for dbName in list(dbVerInfo.keys()):
            result[dbName] = dbName + ": " + dbVerInfo[dbName]

    return(result)
## end ver_info()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

