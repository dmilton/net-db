#
gVersion=''

from net.network_error import NetException
from net import db_access

# Flag to track if we have been initialized.
_tables_initialized = False

###############################################################################
# Building Types table:
#

_building_types = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'building_types',
    'index': 'bldg_type_id'
}

from . import initialize
from . import get_building_types
from . import get_building_type_fields
from . import new_building_type
from . import add_building_type
from . import update_building_type
from . import delete_building_type

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

