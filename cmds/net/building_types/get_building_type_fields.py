#\L

# 24 Mar 2014   initial creation.

############################################################################
# get_building_type_fields
# Entry: none
# Exit: returns the list of fields in the building_types table
# Exceptions:

def get_building_type_fields():

    global _building_types

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_building_types)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can
        # accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_building_type_fields()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

