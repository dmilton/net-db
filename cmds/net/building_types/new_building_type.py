#\L

# 18 Feb 2015   initial creation.

############################################################################
# new_bldg_type
# Create a new empty template for a building.
def new_bldg_type():

    global _building_types

    # Ask the DB what fields are in a building_type record.
    result = db_access.new_table_row(_building_types)

    return(result)
## end new_bldg_type()

# vim: syntax=python ts=4 sw=4 showmatch et :

