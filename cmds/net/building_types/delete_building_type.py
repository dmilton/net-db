#

#  9 Sep 2014   Add exception for a delete that deleted zero rows.
# 18 Sep 2013   Initial creation

##############################################################################
# delete_bldg_type(bldg_type_info)
# Delete the building type record.
# Entry:
#       bldg_type_info - dictionary of values from the building type
# Exit:
#       a new building record has been added to the database.
# Exceptions:
#       A UnknownCampusID NetException is raised if no rows are deleted.

def delete_bldg_type(bldg_type_info):

    global _building_types

    try:
        _check_building_type(bldg_type_info)
    except network_error.NetException as net_err:
        if (net_err.name == "BldgTypeExists" ):
            # this is as it should be, we have something to delete.
            pass
    else:
        raise network_error.NetException("BldgTypeNotFound",
                "The building type '" + bldg_type_info['bldg_type']
                + "' was not found.")

    # Build a where clause to identify this building.
    my_where = "bldg_type = '" + bldg_type_info['bldg_type'] + "'"

    # Delete the record.
    row_count = db_access.delete_table_rows(_building_types, my_where)

    return
## end delete_bldg_type():

# vim: syntax=python ts=4 sw=4 showmatch et :

