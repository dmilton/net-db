#\L

# 16 Apr 2015   initial creation.

############################################################################
# _check_building_type
# Check that the building type does not exist and that a current type
# does not share the same description.
# Entry:    bldg_type_info - building type to verify.
# Exit:     If the building type is unique, return True.

def _check_building_type(bldg_type_info):
    global _building_types

    # Check that we have a unique description.
    my_where = "lower(bldg_type_description) = lower('" + \
            bldg_type_info['bldg_type_description'] + "')"
    dup_descr = db_access.get_table_rows(_building_types, my_where)
    if ( len(dup_descr) > 0 ):
        raise NetException("BldgTypeDescrExists",
                "The building type '" + dup_descr['bldg_type'] +
                "' already has that description.")

    # Check that we have a unique bldg type id if provided
    if ( 'bldg_type_id' in bldg_type_info ):
        my_where = "bldg_type_id = '" + bldg_type_info['bldg_type_id'] + "'"
        id_count = db_access.get_row_count(_building_types, my_where)
        if ( id_count > 0 ):
            raise NetException("BldgTypeIDExists", "The building type ID " 
                    + bldg_type_info['bldg_type_id'] + " already exists.")

    # Check that we have a unique name.
    my_where = "lower(bldg_type) = lower('" + bldg_type_info['bldg_type'] + "')"
    name_count = db_access.get_row_count(_building_types, my_where)
    if ( name_count > 0 ):
        raise NetException("BldgTypeExists",
                "A building type named '" + bldg_type_info['bldg_type'] + \
                "' already exists.")

    return
## end _check_bldg_type():

# vim: syntax=python ts=4 sw=4 showmatch et :

