#

# 18 Feb 2015   Initial creation

##############################################################################
# update_bldg_type(bldg_type_info)
# Update a building type.
# Entry:   bldg_type_info - dictionary of values from the building type.
# Exit:    the building type record has been updated.

def update_bldg_type(bldg_type_info):

    global _building_types

    # Check to see if the type is unique.
    try:
        _check_bldg_type(bldg_type_info)
    except network_error.NetException as net_err:
        if (net_err.name == "BldgTypeExists" ):
            # this is as it should be, we have something to update.
            pass
    else:
        raise network_error.NetException("BldgTypeNotFound",
                "The building type '" + bldg_type_info['bldg_type']
                + "' was not found.")

    # Update the record in the database.
    my_where = "bldg_type = '" + bldg_type_info['bldg_type'] + "'"
    result = db_access.set_table_row(_building_types, 
                                bldg_type_info, my_where)[0]

    return(result)
## end update_bldg_type():

# vim: syntax=python ts=4 sw=4 showmatch et :

