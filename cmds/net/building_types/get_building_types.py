#\L

# 18 Feb 2015   initial creation.

############################################################################
# get_building_types
# Return a list of defined building types.
# Entry:    bldg_type_name - optional, if present, restrict to just this type.
# Exit:     A list of defined building types is returned.

def get_building_types(bldg_type_name=""):
    global _building_types

    if ( bldg_type_name != "" ):
        my_where = "bldg_type = '" + bldg_type_name + "'"
    else:
        my_where = ""
    # Get the list of defined building types
    result = db_access.get_table_rows(_building_types, my_where)

    if ( len(result) == 0 ):
        raise ValueError("The building type: '" + bldg_type_name
                + "' does not exist.")

    return(result)
## end get_building_types()

# vim: syntax=python ts=4 sw=4 showmatch et :

