#

# 18 Feb 2015   Initial creation

##############################################################################
# add_building_type(bldg_type_info)
# Add a new building to the building_detail table.
# Entry:    bldg_type_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_building_type(bldg_type_info):

    global _building_types

    # Verify this building type is unique.
    _check_building_type(bldg_type_info)

    # Create a new record.
    new_bldg_type = db_access.new_table_row(_building_types)

    # Copy matching values from bldg_type_info into our new record.
    # This eliminates any extraneous vaules in bldg_type_info.
    for a_key in list(new_bldg_type.keys()):
        if ( a_key in bldg_type_info ):
            new_bldg_type[a_key] = bldg_type_info[a_key]
        else:
            del new_bldg_type[a_key]

    # Add the record into the database.
    result = db_access.set_table_row(_building_types, new_bldg_type)[0]

    return(result)
## end add_building_type():

# vim: syntax=python ts=4 sw=4 showmatch et :

