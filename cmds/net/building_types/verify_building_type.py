#\L

# 18 Feb 2015   initial creation.

############################################################################
# verify_building_type
# Verify that the building type exists.
# Entry:    bldg_type_name - building type to verify.
# Exit:     If the building type is valid, return True.

def verify_building_type(bldg_type_name):
    global _building_types

    # Get a row count for building types with the specified name.
    my_where = "lower(bldg_type) = lower('" + bldg_type_name + "')"
    row_count = db_access.get_row_count(_building_types, my_where)

    return(row_count == 1)
## end verify_building_type()

# vim: syntax=python ts=4 sw=4 showmatch et :

