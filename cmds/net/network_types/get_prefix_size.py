#\L

#  5 Mar 2014        initial creation.

############################################################################
# get_prefix_size
# Given a network type return the prefix size for that type.

def get_prefix_size(the_type_info, ip_ver):

    # start with an invalid result.
    result = 0

    # Start by using the prefix_size if it is available:
    if ( 'prefix_size' in the_type_info ):
        if ( type(the_type_info['prefix_size']) is list ):
            # We have a list.
            if ( ip_ver == 4 and len(the_type_info['prefix_size']) > 0 ):
                # Element 0 is for IPv4
                result = the_type_info['prefix_size'][0]
            elif ( ip_ver == 6 and len(the_type_info['prefix_size']) > 1 ):
                # if it exists, element 1 is for IPv6
                result = the_type_info['prefix_size'][1]
        else:
            # We don't have a list so this is just for IPv4
            if ( ip_ver == 4 ):
                result = the_type_info['prefix_size']

    # We didn't get a value from prefix_size so pick some defaults
    # based on the type.
    if ( result == 0 and ip_ver == 4 ):
        if ( the_type_info['net_type'] == 'loop' ):
            result = 32
        elif ( the_type_info['net_type'] == 'link' ):
            result = 30
        elif ( the_type_info['net_type'] == 'camera' ):
            result = 24
        elif ( the_type_info['net_type'] == 'prot' ):
            result = 28
        else:
            result = 26

    if ( result == 0 and ip_ver == 6 ):
        if ( the_type_info['net_type'] == 'loop' ):
            result = 128
        elif ( the_type_info['net_type'] == 'link' ):
            result = 112
        else:
            result = 64

    if ( result == 0 ):
        raise ValueError("Unable to determine prefix for '" +\
                the_type_info['net_type'] + "' networks.")

    return(result)
## end get_prefix_size()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

