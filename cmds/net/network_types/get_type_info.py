#

#  9 Sep 2014   Change exception for consistiency.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
#  3 Jun 2014   Improve error message when the requested type is not found.
# 11 Sep 2013   Change default for ip_ver to be 0 (ignored in query)
#               since most types have type specific names. This corrects
#               for type 'iso' which is not IP and therefore has no version.
# 11 Jul 2013   add ip_ver argument and default to using IPv4.
#               this change meant sorting by version no longer makes sense.
# 25 Apr 2013   initial creation from networks library

############################################################################
# get_type_info
# Build a list of network type dictionaries.
# The list determines which network types can allocated. The list is
# ordered based on their allocation priority.
#
# Input:  the_type_name - the value to find a type for.
# Output: The type information.
# Exception: If the_type_name is specified and that type is not found in the db
#             then 'InvalidNetType' is returned.
# 
def get_type_info(the_type_name='', ip_ver=0):
    global _network_types

    # Generate a where clause.
    have_where = False
    my_where = ""
    if ( ip_ver != 0 ):
        my_where = "net_family = ipv" + str(ip_ver)
        have_where = True
    if ( the_type_name > "" ):
        if ( have_where == True ):
            my_where += " AND "
        my_where += "net_type = '" + the_type_name + "'"

    # Order table by priority of allocation to building and the ip version.
    sort_order = "assign_priority"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_network_types, my_where, sort_order)

    if ( len(result) == 0 ):
        raise NetException("NetTypeNotFound", 
            "The network type '" + the_type_name + "' is not defined.")

    return(result)
## end def get_type_info():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

