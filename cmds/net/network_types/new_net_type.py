#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_net_type
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_net_type():
    global _network_types

    result = db_access.new_table_row(_network_types)

    return(result)
## end new_net_type()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

