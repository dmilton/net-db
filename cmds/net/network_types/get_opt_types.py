#

# 11 Aug 2013   Numerous columns renamed for uniqueness and better
#               description of how they are used.
#  5 Mar 2014    Change query to exclude only vlans that are optional and
#                have a non-zero vlan tag value. This allows easy addition
#                of 'user' type networks with private address allocations
#                without the requirement of the vlan being unique in that
#                building code.
# 16 Oct 2013    initial creation

############################################################################
# get_opt_types
# Build a list of network type dictionaries for use on campus.
# The list contains only network types which are unique within the
# building and are optionally assigned.
#
# Input:  none.
# Output: The list of type information.
# 
def get_opt_types():
    global _network_types

    # Generate a where clause.
    my_where = "type_required = 'f' AND base_vlan_tag != 0"

    # Order table by priority of allocation to building
    sort_order = "assign_priority"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_network_types, my_where, sort_order)

    if ( len(result) == 0 ):
        # No codes found.
        raise NetException("NetTypeNotFound", "no types found.")

    return(result)
## end def get_opt_types():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

