#\L

# 25 Apr 2013        initial creation.

############################################################################
# get_net_type_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_net_type_fields():
    global _network_types

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_network_types)

    # Copy the table information so we can modify it.
    for aKey in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = table_info[aKey]

    return(result)
## end get_net_type_fields()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

