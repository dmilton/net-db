#
gVersion=''

from net.network_error import NetException
from net import db_access
from net.cmd_line_io import print_table

# Flag to track if we have been initialized.
_tables_initialized = False

###############################################################################
# Network Types table.
#
_network_types = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'network_types',
    'index': 'net_type'
}

from . import add_net_type
from . import delete_net_type
from . import get_net_type_fields
from . import get_opt_types
from . import get_type_tables
from . import get_prefix_size
from . import get_type_detail
from . import get_type_info
from . import initialize
from . import new_net_type
from . import print_types
from . import update_net_type

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

