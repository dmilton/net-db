#\L

# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 29 Apr 2014   initial creation.

############################################################################
# get_type_detail
# Build a dictionary of information regarding types defined.
# Entry:
#       ip_version - which version of ip? 4 or 6? or 0=generic.
#       use_ISO - should ISO network be included?
# Exit:
#       The result contains a dictionary of information:
#           required_type_names - A dictionary with each entry containing
#               a list of the required type names for each building type.
#           multi_type_names - set of types which can have multiple in bldg.
#           uniq_type_names - set of types which need to be unique in a bldg.
#           family_ip - network types with family ip
#           family_ipv4 - network types with family ipv4
#           family_ipv6 - network types with family ipv6
#           family_other - network types with non-ip family
#           type_info_by_name - dictionary of all types, key is type name.
# Exceptions:
#       

def get_type_detail(ip_version, use_ISO=False):
    result = {}

    # Key for ip family column in network types.
    if ( ip_version == 0 ):
        ip_family = ['ip','ipv4','ipv6']
    else:
        ip_family = ['ip', 'ipv' + str(ip_version)]
        if ( ip_version == 6 ):
            use_ISO = True

    # Key for required_types; comes from building_types table.
    bldg_type_list = building_types.get_building_types()

    # Get the list of all network types we can allocate, default is
    # to have them sorted by their assignment priority.
    net_type_list = get_type_info()

    # Loop through to create the detailed information.
    types_by_priority = []
    required_types = set()
    multi_types = set()
    uniq_types = set()
    v4family = set()
    v6family = set()
    v46family = set()
    other_family = set()
    all_type_info = {}
    for one_type in net_type_list:
        this_type_name = one_type['net_type']
        this_type_family = one_type['net_family']
        all_type_info[this_type_name] = one_type
        types_by_priority.append(this_type_name)
        for a_bldg_type in bldg_type_list:
            if ( this_type_family in ip_family ):
                if ( a_bldg_type in one_type['bldg_required'] ):
                    required_types[a_bldg_type].add(this_type_name)
            if ( one_type['net_type'] == 'iso' and use_ISO ):
                required_types[a_bldg_type].add(this_type_name)

        # Can there be more than one of this type in a building?
        if ( one_type['base_vlan_tag'] >= 0 ):
            multi_types.add(this_type_name)
        else:
            uniq_types.add(this_type_name)

        # Build family specific lists
        if ( this_type_family == 'ip' ):
            v46family.add(this_type_name)
        elif ( this_type_family == 'ipv4' ):
            v4family.add(this_type_name)
        elif ( this_type_family == 'ipv6' ):
            v6family.add(this_type_name)
        else:
            other_family.add(this_type_name)

    result['types_by_priority'] = types_by_priority
    result['required_type_names'] = {}
    for a_bldg_type in required_types.keys():
        result['required_type_names'][a_bldg_type] = \
                frozenset(required_types[a_bldg_type])
    result['multi_type_names'] = frozenset(multi_types)
    result['uniq_type_names'] = frozenset(uniq_types)
    result['family_ip'] = frozenset(v46family)
    result['family_ipv4'] = frozenset(v4family)
    result['family_ipv6'] = frozenset(v6family)
    result['family_other'] = frozenset(other_family)
    result['type_info_by_name'] = all_type_info

    return(result)
## end get_type_detail()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

