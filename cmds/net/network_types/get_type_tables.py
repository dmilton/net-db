#\L

# 18 Aug 2014        initial creation.

############################################################################
# get_type_tables
# Given a network type, determine the assigned and free tables
# the network should be placed into.

def get_type_tables(the_type_info):

    active_table = None
    free_table = None
    if ( 'net_table_list' in the_type_info ):
        if ( len(the_type_info['net_table_list']) == 2 ):
            (active_table,free_table) = the_type_info['net_table_list']
        elif ( len(the_type_info['net_table_list']) == 1 ):
            active_table = the_type_info['net_table_list'][0]
    elif ( 'rel_table' in the_type_info ):
        (active_table,free_table) = the_type_info['rel_table'].split(',')
    else:
        raise ValueError("Unable to determine which tables '" +\
                the_type_info['net_type'] + "' networks belong in.")

    return((active_table,free_table))
## end get_type_tables()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

