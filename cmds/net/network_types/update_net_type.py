#\L

# 20 Sep 2013        set_table_row now returns the updated/inserted
#                    values from the database. get_table_rows call not needed.
# 18 Apr 2013        initial creation.

############################################################################
# update_net_type
# Accept a net_type_info dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_net_type(net_type_info):
    global _network_types

    # Make sure what we got is valid.
    try:
        _check_net_type(net_type_info)
    except NetException as net_err:
        if ( net_err.name == "NetTypeExists" ):
            # this is as expected, it should exist.
            pass
    else:
        raise NetException("NetTypeNotFound",
                "The network type '" + net_type_info['net_type'] +
                "' was not found.")

    # Update an existing network with new values.
    my_where = "net_type = '" + net_type_info['net_type'] + "'"
    curr_info = db_access.get_table_rows(_network_types, my_where)[0]

    for a_key in list(curr_info.keys()):
        if ( a_key in net_type_info ):
            # remove information that has not changed.
            if ( net_type_info[a_key] == curr_info[a_key] 
                    and a_key != 'net_type' ):
                del net_type_info[a_key]

    if ( len(net_type_info) > 1 ):
        # If there is something to update, update it.
        result = db_access.set_table_row(_network_types,
                net_type_info, my_where)
    else:
        result = curr_info

    return(result)
## end update_net_type()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

