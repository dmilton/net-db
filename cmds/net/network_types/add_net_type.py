#

# 16 Apr 2015   The type_required value is gone and bldg_type_list replaced
#               it as a list of building types which identify the building
#               types where this network type is required.
# 11 Aug 2013   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
#               Change to use get_row_count rather than attempting
#               to select the record directly.
# 21 Jun 2013   result should be a dict, not a list. This provides
#               consistient results between Add and Update.
# 25 Apr 2013   initial writing.

############################################################################
# add_net_type
# Add the new type to the database.
# Entry: new_net_type - the dictionary of the type to add to the database.

def add_net_type(new_net_type):
    """
    # Start with an empty template
    new_net_type = NewTypeTemplate()
    # The only mandatory field
    new_net_type['net_type'] = 'user'
    # The remainder can be deleted for default or set as required.
    # What vlan should networks of this type start numbering at?
    new_net_type['base_vlan_tag'] = 40    # default is 0
    # How should new networks be determined?
    new_net_type['assign_method'] = 'manual'
    # What order should this network be assigned to a building in?
    new_net_type['assign_priority'] = 0
    # What building types should this network be automatically assigned to.
    new_net_type['bldg_type_list'] = ["office", "dc"]
    # What extension should be used to identify type in the name?
    new_net_type['name_extension'] = ''
    # What tables should be used for this type of network?
    # one or two entries, first is assigned table, second is free table.
    new_net_type['net_table_list'] = ['network_bldgs', 'network_free']
    # what family does this network belong to?
    # ip, ipv4, ipv6, iso, bri (bridged)
    new_net_type['net_family'] = 'ip'
    # prefix size - list - for ip, needs two entries ipv4 and ipv6
    new_net_type['prefix_size'] = [ 30, 112 ]
    addedType = add_net_type(new_net_type)
    """
    global _network_types

    # Make sure what we got is valid.
    _check_net_type(new_net_type)

    # Everything checked out so insert the new network into the table.
    result = db_access.set_table_row(_network_types, new_net_type)

    return(result)
## end def add_net_type():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

