#\L

#  5 Mar 2014        initial creation.

############################################################################
# get_bldg_required
# Given a particular network type and building type determine if a 
# if a type is required there.

def get_bldg_required(net_type_info, bldg_type):

    # start with an empty result.
    result = {}

    if ( bldg_type in net_type_info['bldg_required'] ):
        result = True
    else:
        result = False

    return(result)
## end get_bldg_required()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

