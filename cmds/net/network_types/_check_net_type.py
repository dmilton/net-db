#\L

# 16 Apr 2015   initial creation.

############################################################################
# _check_net_type
# Entry: net_type_info - The network type information to check.
# Exit: result
# Exceptions:

def _check_net_type(net_type_info):
    global _network_types

    # Check that the assignment method is valid.
    if ( 'assign_method' in net_type_info ):
        if ( net_type_info['assign_method'] not in ["manual", "next", "nextn",
                        "calc", "private", "range", "index"] ):
            raise NetException("InvalidAssignMethod",
                "The assignment method '" + net_type_info['assign_method']
                + "' is invalid.")

    # Check that the prefix size is valid.
    if ( 'prefix_size' in net_type_info ):
        prefix_size = net_type_info['prefix_size']
        if ( len(prefix_size) == 0 ):
            pass
        elif ( len(prefix_size) == 1 ):
            if ( prefix_size[0] > 32 or prefix_size[0] < 8 ):
                raise NetException("InvalidPrefix",
                        "The network prefix " + str(prefix_size[0])
                        + " is not valid for IPv4 networks.")
        elif ( len(prefix_size) == 2 ):
            if ( prefix_size[1] > 128 or prefix_size[1] < 32 ):
                raise NetException("InvalidPrefix",
                        "The network prefix " + str(prefix_size[1])
                        + " is not valid for IPv6 networks.")

    # Check the building types. Cannot have a constraint which checks
    # the individual elements of an array as a foreign key so we must
    # check each type in the list individually.
    if ( 'bldg_type_list' in net_type_info ):
        for a_bldg_type_name in net_type_info['bldg_type_list']:
            if ( not verify_building_type(a_bldg_type_name) ):
                raise NetException("InvalidBldgType",
                        "The building type '" + a_bldg_type_name +
                        "' is not defined.")

    # Check that the name is unique.
    my_where = "lower(net_type) = lower('" + net_type_info['net_type'] + "')"
    name_count = db_access.get_row_count(_network_types, my_where)
    if ( name_count > 0 ):
        raise NetException("NetTypeExists",
                "A network type named '" + net_type_info['net_type'] +
                "' already exists.")

    return
## end _check_net_type()

# vim: syntax=python ts=4 sw=4 showmatch et :

