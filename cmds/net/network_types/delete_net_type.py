#

#  9 Sep 2014   Add exception for deleting zero rows.
# 25 Apr 2013   initial creation.

############################################################################
# delete_net_type
# Given a type dictionary, delete the type from the database.
# Entry:
#        the_net_type - the type dictionary to delete.
# Exit: the type has been deleted from the database.

# 4 Feb 2012    initial creation

def delete_net_type(the_net_type):
    global _network_types

    my_where = "net_type = '" + the_net_type['net_type'] + "'"
    rows = db_access.delete_table_rows(_network_types, my_where)
    if ( rows == 0 ):
        raise NetException("NetTypeNotFound",
                "The network type " + the_net_type['net_type'] + " not found.")

    return()
## end def delete_net_type():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

