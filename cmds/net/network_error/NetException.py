#

# 3 Jan 2013        Remove duplicated error handler which is called
#                    by most recovery routines to output a reasonable
#                    message to the end user. The routine was being
#                    duplicated as it resides in a separate file.
# 2 Sep 2010        Initial creation based on original code written
#                    in the network.py module.

#^L
############################################################################
# Networking Error class.

class NetException(Exception):
    """Class for all exceptions raised in this module."""

    def __init__(self, name, detail):
        self.name = name
        self.detail = detail
        return
    ## end def __init__(self):

    def __str__(self):
        return repr(self.detail)
    ## end def __str__(self):

## end class NetException():

# vim: syntax=python ts=4 sw=4 showmatch et :

