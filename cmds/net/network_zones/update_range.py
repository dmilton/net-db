#\L

#  6 Nov 2014   Field name change from network to network_range.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 18 Apr 2013   initial creation.

############################################################################
# update_range
def update_range(range_info):
    global _zone_ranges

    # Update an existing network with new values.
    my_where = "network_range = '" + str(range_info['network_range']) + "'"
    result = db_access.set_table_row(_zone_ranges, range_info, my_where)

    return(result)
## end update_range()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

