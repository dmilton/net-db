#\L

# 21 Aug 2014   Rewrite to use new ipaddress functionality and allow
#               for both IPv4 and IPv6 results.
#  2 Jun 2014   Change function call update_zone to update_zone_net.
# 28 Apr 2014   Add link_count to allow getting multiple links if desired.
#               Add noUpdate flag to allow getting information without
#               actually updating the zone to show a link has been used.
# 25 Sep 2013   initial creation.

############################################################################
# get_next_link
# Compute a list of addresses to use for network links.
#
# Entry:
#       zone_info - the information on the zone.
#       prefix_size - from typeInfo - the list.
#       start_addr - address to start computing addresses from.
#       link_count - the number of link addresses to return.
#       update_zone - should the zone be updated to show link_count
#                   links have been used?
#
# Exit:
#       A list of addresses which can be used for network links has
#       been determined and is returned.

def get_next_link(rangeInfo, prefix_size,
        start_addr=ipaddress.ip_address('::'), link_count=1, update_zone=True):

    if ( start_addr == ipaddress.ip_address('::') ):
        start_addr = rangeInfo['network_range'].network_address

    # Get the next link index to use:
    address_offset = rangeInfo['next_link']
    
    # How many addresses are in a link network?
    addr_mult = ipaddress.ip_network(str(start_addr) + "/" +
            str(prefix_size)).num_addresses

    # Create link_count link addresses.
    result = []
    for line_ctr in list(range(link_count)):
        one_addr = ipaddress.ip_network( 
            str(start_addr + address_offset * addr_mult) + "/" +
            str(prefix_size) )
    
    # Update the zone to show a link has been used.
    if ( update_zone ):
        zone_info['next_link'] += link_count
        update_zone_net(zone_info)

    return(result)
## end def get_next_link():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

