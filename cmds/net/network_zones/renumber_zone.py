#\L

# 28 May 2014   initial creation.

############################################################################
# renumber_zone

def renumber_zone(zone_info, new_net_zone):
    global _zone_descriptions, _zone_ranges

    # Update the net_zone field.
    # The network_free table and network_zones table are both
    # using a foreign key based on the zone_descriptions net_zone value,
    # The update here will trigger a cascade update of the other tables.
    result = db_access.change_table_rows(_zone_descriptions,
            "net_zone", zone_info['net_zone'], new_net_zone, False)

    return(result)
## end renumber_zone_desc()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

