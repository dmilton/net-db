#
gVersion=''

import ipaddress

from net.network_error import NetException
from net import db_access
from net import network_types

# Flag to track if we have been initialized.
_tables_initialized = False

###############################################################################
# Network Description Table
# This table contains a description of the zones defined and what/where
# they should be used.

_zoneDescriptions = {
    'database': 'networking',
    'table': 'zone_descriptions',
    'role': 'netmon',
    'index': 'net_zone'
}

###############################################################################
# Network Zones Table
# This table contains large network blocks that are assigned to specific
# zones of the campus.

_zoneRanges = {
    'database': 'networking',
    'table': 'zone_ranges',
    'role': 'netmon',
    'index': 'network_range'
}

from . import initialize

from . import get_zone_info
from . import find_range
from . import get_user_range

from . import new_zone
from . import get_zone_desc
from . import get_zone_desc_fields
from . import add_zone
from . import update_zone
from . import delete_zone
from . import renumber_zone

from . import new_range
from . import get_range_fields
from . import add_range
from . import delete_range
from . import update_range

from . import get_next_bldg
from . import get_next_link
from . import get_next_loop
from . import net_id_2_bldg_net
from . import next_bldg

from . import print_zones

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

