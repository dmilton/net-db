#\L

# 21 Aug 2014   initial creation.

############################################################################
# calc_range_net
#
# Determine a network using the 'late mask' method. This is a method where
# the routing mask which can be applied to a building is a fixed determination
# only when the entire address space has been exhausted.
# 
# Entry: 
#       base_net - the base address space to compute an address for.
#       prefix_len - the size of network to split base_net into.
#       bldg_index - the index of the building.
#       net_index - the index of the subnet within the building.
#
# Exit:
#
#
def calc_range_net(base_net, prefix_len, bldg_index, net_index):
    
    if ( base_net.version == 6 ):
        # prefixlen for IPv6 is always /64.
        bits_of_net = prefix_len - base_net.prefixlen
        if ( bits_of_net % 4 != 0 ):
            raise ValueError(str(base_net) +
                 " is not masked on a nibble boundary.")
        # Convert the building index into binary:    bin(ipNetId)
        # drop the 0b prefixed on the binary:       [2:]
        # zero pad on the left to a length of bits_of_net:   .zfill(bits_of_net)
        # reverse the order of the bits:            [::-1]
        # convert the reversed binary number to an integer: int(n)
        # add index of next building network:        + ipNextNet
        # convert the result to hex:                hex( i )
        # drop the 0x prefixed on the hex:          [2:]
        hex_net = hex(int(bin(bldg_index)[2:].zfill(bits_of_net)[::-1],2) \
            + net_index)[2:]
        # Convert to a long hex string:
        addr_blocks = base_net.network_address.exploded.split(':')
        base_hex = ""
        for a_block in addr_blocks:
            base_hex += a_block
        # Take the assigned prefix portion and add our network portion
        base_alloc = base_hex[0:int(base_net.prefixlen/4)] + hex_net
        # Fill up to max with zero.
        net_as_hex = base_alloc + '0'.zfill(32-len(base_alloc))
        # Insert colons to make it look like an IPv6 address again.
        start_index = 0
        ip_addr_str = net_as_hex[0:4]
        for index in list(range(4,32,4)):
            ip_addr_str = ip_addr_str + ":" + net_as_hex[index:index+4]
        # Add the CIDR prefix
        ip_addr_str += "/" + str(prefix_len)
        # Make it back into an ipaddress object.
        result = ipaddress.ip_network(ip_addr_str)
    else:
        bits_of_net = prefix_len - base_net.prefixlen
        if ( bldg_index > (bits_of_net ** 2) ):
            raise ValueError(str(base_net) + " is exhausted.")

        # Convert our base_net into binary:
        addr_bytes = str(base_net.network_address).split('.')
        bin_net = ""
        for a_byte in addr_bytes:
            bin_net = bin_net + bin(int(a_byte))[2:].zfill(8)
        base_bits = bin_net[0:base_net.prefixlen]
        # Convert the building index into binary:           bin(bldg_index)
        # drop the 0b prefixed on the binary:               [2:]
        # zero pad on the left to a length of bits_of_net:   .zfill(bits_of_net)
        # reverse the order of the bits:                    [::-1]
        # convert the reversed binary number to an integer: int(n)
        # add index of next network in the building:        + ipNextNet
        net_bits = \
            int(bin(bldg_index)[2:].zfill(bits_of_net)[::-1],2) + net_index
        # Form our new subnet in binary:
        net_addr = base_bits + bin(net_bits)[2:]
        net_addr += '0'.zfill(32-len(network_bits))
        # Now make it look like an IP network
        net_addr_str = str(ipaddress.ip_address(int(net_addr,2))) + \
                "/" + str(prefix_len)
        result = ipaddress.ip_network(net_addr_str)

    return(result)
## end calc_range_net()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

