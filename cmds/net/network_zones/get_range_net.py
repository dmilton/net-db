#\L

# 11 Feb 2015   Changed the result from a network record to an
#               ipaddress.ip_network class.
# 26 Jan 2015   Improve error diagnostics.
#  6 Nov 2014   initial creation

############################################################################
# get_range_net
# Given building and type information, compute a range net based on
# the address range for the type.
#
# Entry:
#       bldg_info - the building to get a network for.
#       type_info - the type of network to create
#       ip_ver - the IP version to create a network for
#       net_index - the index of the network to create.
# Exit:
#       A network has been computed and and returned as result.
#    

def get_range_net(bldg_info, type_info, ip_ver, net_index):

    # Get the address range to use for this type and zone.
    zone_range = get_range_info(bldg_info['net_zone'], 
            type_info['net_type'], ip_ver)

    if ( len(zone_range) == 0 ):
        raise NetException("NoRange", "No IPv" + str(ip_ver) 
            + " address range was found for type '" + type_info['net_type'] 
            + "' in zone " + str(bldg_info['net_zone']) + ".")
    elif ( len(zone_range) > 1 ):
        raise NetException("TooManyRanges", "More than one IPv" + str(ip_ver)
            + "address range was found for type '" + type_info['net_type']  
            + "' in zone " + str(bldg_info['net_zone']) + ".") 
    else:
        zone_range = zone_range[0]

    result = calc_range_net(zone_range['network_range'],
            #network_types.get_prefix_size(type_info, ip_ver),
            get_prefix_size(type_info, ip_ver),
            bldg_info['ipv6_netid'], net_index)

    return(result)
## end get_range_net()

# vim: syntax=python ts=4 sw=4 showmatch et :

