#\L

# 15 May 2014   initial creation.

############################################################################
# update_zone_desc
def update_zone_desc(zone_info):
    global _zone_descriptions

    # Update an existing network with new values.
    my_where = "net_zone = " + str(zone_info['net_zone'])
    db_result = db_access.set_table_row(_zone_descriptions, zone_info, my_where)

    return(db_result[0])
## end update_zone_desc()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

