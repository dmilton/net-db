#\L

# 26 Sep 2014   Networks have become network_ranges to separte the
#               zone descriptions from the address spaces.
# 10 Jul 2013   update to return the entire zone record instead
#               of just the zone id.
# 14 Feb 2013   initial creation.

############################################################################
# find_range
# Entry: the_network - the network to determine the zone for.

def find_range(the_network):
    global _zone_ranges

    my_where = "network_range >>= '" + str(the_network) + "'"
    result = db_access.get_table_rows(_zone_ranges, my_where)
    
    return(result)
## end find_range()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

