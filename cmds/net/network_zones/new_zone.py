#\L

# 15 May 2014   initial creation.

############################################################################
# new_zone
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        zone description entry in the database.

def new_zone():
    global _zone_descriptions

    result = db_access.new_table_row(_zone_descriptions)

    return(result)
## end new_zone()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

