#\L

# 11 Feb 2015   Changed the result from a network record to an
#               ipaddress.ip_network class.
#               Changed name of variable update_range to update_addr_range
#               to remove conflict with function of the same name.
#  5 Nov 2014   complete rewrite to use calc_indexed_net.

############################################################################
# get_indexed_net
# Given building and type information, compute an indexed net based on
# an address range for the type.
#
# Entry:
#       bldg_info - the building to get a network for.
#       type_info - the type of network to create
#       ip_ver - the IP version to create a network for
#       update_addr_range - should the range index be updated ?
# Exit:
#       A network has been computed and and returned as result.
#    

def get_indexed_net(bldg_info, type_info, ip_ver, update_addr_range):

    # Get the address range to use for this type and zone.
    zone_range = get_range_info(bldg_info['net_zone'], 
            type_info['net_type'], ip_ver)

    if ( len(zone_range) == 0 ):
        raise NetException("NoRange", "No IPv" + str(ip_ver) 
                + " address range was found for type '" + type_info['net_type']
                + "' in zone " + str(bldg_info['net_zone']) + ".")
    elif ( len(zone_range) > 1 ):
        raise NetException("TooManyRanges", "More than one IPv" + str(ip_ver) 
            + "address range was found for type '" + type_info['net_type'] 
            + "' in zone " + str(bldg_info['net_zone']) + ".")
    else:
        zone_range = zone_range[0]
        zone_range['next_bldg']

    if ( update_addr_range ):
        zone_range['next_bldg'] += 1
        update_range(zone_range)

    result = calc_indexed_net(
            zone_range['network_range'],
            #network_types.get_prefix_size(type_info, ip_ver),
            get_prefix_size(type_info, ip_ver),
            zone_range['next_bldg'] )

    return(result)
## end get_indexed_net()

# vim: syntax=python ts=4 sw=4 showmatch et :

