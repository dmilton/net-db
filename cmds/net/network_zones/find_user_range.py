#\L

# 22 Aug 2014   column name in the range has changed.
# 15 Nov 2013   initial creation.

############################################################################
# find_user_range
# Get the network zone/address space from which user addresses are
# to be taken from.

def find_user_range(zone_id, ip_version):
    global _zone_ranges

    # Order is always the same.
    my_order = "network_range"

    # 
    my_where = "family(network_range) = " + str(ip_version)
    my_where += " AND (net_type_list = '' OR net_type_list LIKE '%user%')"
    my_where1 = my_where + " AND net_zone = " + str(zone_id)
    my_where2 = my_where + " AND net_zone = 0"

    # Start by looking for a zone specific address space.
    db_result = db_access.get_table_rows(_zone_ranges, my_where1, my_order)

    # If we didn't find one, look for one in zone 0, the default zone.
    if ( len(db_result) == 0 ):
        db_result = db_access.get_table_rows(_zone_ranges, my_where2, my_order)

    # If we still don't have a result, return an error.
    if ( len(db_result) == 0 ):
        raise ValueError("unable to find zone address space for the request.")
    else:
        result = db_result[0]

    return(result)
## end find_user_range()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

