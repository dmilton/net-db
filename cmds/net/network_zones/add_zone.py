#

# 18 Sep 2014   db routine returns a list but this function should
#               only return a singe description so dereference it on return.
#  2 May 2014   initial writing.

############################################################################
# add_zone
# Add the new zone to the database.
# Entry: the_description - description of the zone to add.
# Exit: A new entry has been added to the database.

def add_zone(the_description):
    """
    # net_zone is a sequence so only the description is required.
    the_description = "default zone when nothing specific is available."
    add_zone(the_description)
    """

    global _zone_descriptions

    # Create a zone the db routines.
    new_zone = {}
    new_zone['zone_descr'] = the_description

    # Insert the new network into the table.
    try:
        db_result = db_access.set_table_row(_zone_descriptions, new_zone)
    except:
        raise NetException("DuplicateZone", \
            "this zone already exists in the database.")

    return(db_result[0])
## end def add_zone():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

