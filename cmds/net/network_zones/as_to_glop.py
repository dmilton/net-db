#\L

# 05 Apr 2016   initial creation.

############################################################################
# as_to_glop
# Entry: asn - Autonomous System Number.
#       v6_prefix - IPv6 prefix to use for multicast address calculation.
#       scop - IPv6 multicast address scope - site, org, global.
# Exit: returns touple (ipv4 glop net, ipv6 glop net)
# Exceptions: Cannot be used for 32 bit ASN :-(

def as_to_glop(asn, v6_prefix, scop):

    # ASN's need to follow these rules:
    # 0: reserved.
    # 1-64.495: public AS numbers.
    # 64.496 – 64.511 – reserved to use in documentation.
    # 64.512 – 65.534 – private AS numbers.
    # 65.535 – reserved.
    if ( asn >= 64496 and asn <= 64511 ):
        raise ValueError("ASN value is reserved for documentation.")
    elif ( asn >= 64512 and asn <= 65534 ):
        raise ValueError("Private ASN numbers not valid for GLOP calculation.")
    elif ( asn == 0 or asn == 65535 ):
        raise ValueError("ASN values 0 and 65535 cannot be used.")
    elif ( asn > 65535 ):
        raise ValueError("GLOP range cannot be calculated for 32 bit ASNs")

    # Entire IPv4 GLOP range is 233/8, ASN specific part is 233.ASN.ASN,0/24
    # where the ASN part is the ASN converted to HEX and then fit into
    # the two octects of the address.
    asn_part = hex(asn)[2:]
    if ( len(asn_part) < 4 ):
        for i in range(0, 4 - len(asn_part)):
            asn_part = '0' + asn_part
    glop_v4_int = int( hex(233) + asn_part + '00', 16)
    glop_v4 = ipaddress.ip_network(glop_v4_int).supernet(8)

    # RFC 4291 is the start and info is updated by
    # 5952, 6052, 7136, 7346, 7371.
    # For IPv6, the range FF3x::/32 is defined for SSM services [SSM-IPV6].
    # +--------+----+----+----+----+--------+----------------+----------+
    # |   8    |  4 |  4 |  4 |  4 |    8   |       64       |    32    |
    # +--------+----+----+----+----+--------+----------------+----------+
    # |11111111|ff1 |scop|ff2 |rsvd|  plen  | network prefix | group ID |
    # +--------+----+----+----+----+--------+----------------+----------+
    #
    #                             +-+-+-+-+
    # ff1 is a set of 4 flags:    |X|Y|P|T|
    #                             +-+-+-+-+
    # X and Y may each be set to 0 or 1.  Note that X is for future
    # assignment, while a meaning is associated with Y in RFC 3956.
    #
    # - P = 0 indicates a multicast address that is not assigned
    #   based on the network prefix. This indicates a multicast
    #   address as defined in RFC 4291.
    # - P = 1 indicates a multicast address that is assigned based
    #   on the network prefix.
    # - If P = 1, T MUST be set to 1, otherwise the setting of the T
    #   bit is defined in Section 2.7 of RFC 4291.
    #
    #                                                +-+-+-+-+
    # ff2 (flag field 2) is a set of 4 flags:        |r|r|r|r|
    #                                                +-+-+-+-+
    # where "rrrr" are for future assignment as additional flag bits.
    # r bits MUST each be sent as zero and MUST be ignored on receipt.
    # Flag bits denote both ff1 and ff2.
    #
    # If the flag bits in ff1 are set to 0011, these settings create an
    # SSM range of ff3x::/32 (where 'x' is any valid scope value).  The
    # source address field in the IPv6 header identifies the owner of
    # the multicast address.  ff3x::/32 is not the only allowed SSM
    # prefix range.  For example, if the most significant flag bit in
    # ff1 is set, then we would get the SSM range ffbx::/32.
    #
    # scop is a 4-bit multicast scope value used to limit the scope of
    # the multicast group.  The values are:
    #   0  reserved
    #   1  node-local scope
    #   2  link-local scope
    #   3  (unassigned)
    #   4  (unassigned)
    #   5  site-local scope
    #   6  (unassigned)
    #   7  (unassigned)
    #   8  organization-local scope
    #   9  (unassigned)
    #   A  (unassigned)
    #   B  (unassigned)
    #   C  (unassigned)
    #   D  (unassigned)
    #   E  global scope
    #   F  reserved
    #
    # reserved should be zero.
    #
    # The unicast prefix-based IPv6 multicast address format supports
    # Source-specific multicast addresses, as defined by [SSM ARCH].  To
    # accomplish this, a node MUST:
    #      o  Set P = 1.
    #      o  Set plen = 0.
    #      o  Set network prefix = 0.
    # These settings create an SSM range of FF3x::/32 (where 'x' is any
    # valid scope value). The source address field in the IPv6 header
    # identifies the owner of the multicast address.
    #
    # group ID identifies the multicast group, either permanent or
    # transient, within the given scope.
    #
    # Given the above, it is likely that most will be part of:
    # FF35:0000:prefix::/96 site-local
    # FF38:0000:prefix::/96 organization-local
    # FF3E:0000:prefix::/96 global.

    # if you have 2001:db8:1234:5678::/64 then your prefix is:
    # FF3x:0030:2001:db8:1234:5678::/96
    # See RFC3306 / RFC3956 / RFC 4607 for the details.
    try:
        have_v6 = v6_prefix.version == 6
        if ( have_v6 ):
            if ( scop == 'site' ):
                v6_group = 'ff35:0:' + str(v6_prefix)[:-3] + '/96'
            elif ( scop == 'org' ):
                v6_group = 'ff38:0:' + str(v6_prefix)[:-3] + '/96'
            elif ( scop == 'global' ):
                v6_group = 'ff3e:0:' + str(v6_prefix)[:-3] + '/96'
            glop_v6 = ipaddress.ip_network(v6_group)
            result = (glop_v4,glop_v6)
        else:
            result = (glop_v4,)
    except:
        result = (glop_v4,)

    return(result)
## end as_to_glop()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

