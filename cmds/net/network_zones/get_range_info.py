#\L

# 14 Aug 2014   Improve documentation.
# 15 May 2014   Add description field to the output.
# 24 Oct 2013   When zone_id = 0, the id was not being added to the query.
#               This resulted in all zones being returned rather than just
#               the default assignment zone.
#               Renamed the where creation to start with underscore.
# 10 Jul 2013   initial creation.

############################################################################
# get_range_info
#
# Entry:
#       zone_id - the ID of the zone to fetch network ranges for.
#       type_name - optional, when specified find network range for
#               the specified type.
#       ip_version - when specified find the network range of the
#               specified ip version - 4 or 6.
# Exit:
#       the result is an address range which can be used to assign
#       networks of the requested type from.

def get_range_info(zone_id, type_name = '', ip_version=0):
    global _zone_descriptions, _zone_ranges

    result = []

    # Order is always the same; IPv6 first.
    my_order = "net_zone, family(network_range) desc, network_range"

    # Start with the most specific query.
    my_where = _build_range_where(zone_id, type_name, ip_version)
    db_result = db_access.get_table_rows(_zone_ranges, my_where, my_order)

    if ( len(db_result) == 0 ):
        # the result is empty, is there an 'any type' entry?
        my_where = _build_range_where(zone_id, '', ip_version)
        db_result = db_access.get_table_rows(_zone_ranges, my_where, my_order)

    if ( len(db_result) == 0 ):
        # the result is still empty, use the default zone 0.
        my_where = _build_range_where(0, '', ip_version)
        db_result = db_access.get_table_rows(_zone_ranges, my_where, my_order)

    # If we still don't have a result, return an error.
    if ( len(db_result) == 0 ):
        raise ValueError("unable to address range for the request.")

    # We have at least one address range. Add the description to each one.
    for aZoneNet in db_result:
        my_where = 'net_zone = ' + str(aZoneNet['net_zone'])
        try:
            zoneDesc = db_access.get_table_rows(_zone_descriptions, my_where)[0]
            aZoneNet['zone_descr'] = zoneDesc['zone_descr']
        except:
            raise ValueError("The database zone descriptions are corrupt.")
        result.append(aZoneNet)

    return(result)
## end get_range_info()

############################################################################
# _build_range_where
def _build_range_where(zone_id, type_name, ip_version):

    # Build a where clause that may contain any
    # of three choices - net_zone, ip_version, or net_type.
    if ( zone_id >= 0 ):
        zone_where = "net_zone = " + str(zone_id)
    if ( ip_version in [4, 6] ):
        ver_where = "family(network_range) = " + str(ip_version)
    if ( type_name > '' ):
        type_where = "net_type_list LIKE '%" + type_name + "%'"

    if ( zone_id >= 0 ):
        # have zone
        if ( ip_version in [4, 6] ):
            if ( type_name > '' ):
                # have all three.
                my_where = zone_where + " AND " + ver_where + " AND " + type_where
            else:
                # have zone and ip version
                my_where = zone_where + " AND " + ver_where
        elif ( type_name > '' ):
            # have zone and net type, not version.
            my_where = zone_where + " AND " + type_where
        else:
            # have only zone.
            my_where = zone_where
    else:
        # do not have zone.
        if ( ip_version in [4, 6] ):
            if ( type_name > '' ):
                # have version and type.
                my_where = ver_where + " AND " + type_where
            else:
                # have only ip version
                my_where = ver_where
        elif ( type_name > '' ):
            # no zone, no version, have net type
            my_where = type_where
        else:
            # no zone, no version, no net type.
            my_where = ''

    return my_where
## end _build_range_where()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

