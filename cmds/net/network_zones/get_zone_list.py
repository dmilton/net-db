#\L

# 26 Sep 2014   initial creation.

############################################################################
# get_zone_list
#
# Entry:
#       zone_id - the ID of the zone to fetch description for.
# Exit:
#       the result is an address range which can be used to assign
#       networks of the requested type from.

def get_zone_list(zone_id):
    global _zone_descriptions

    # Order is always the same.
    my_order = "net_zone"
    if ( zone_id > 0 ):
        my_where = "net_zone = " + str(zone_id)
    else:
        my_where = ""

    # Get the descriptions.
    result = db_access.get_table_rows(_zone_descriptions, my_where, my_order)

    # If we still don't have a result, return an error.
    if ( len(result) == 0 ):
        raise NetException("NoZone", "No zones found.")

    return(result)
## end get_zone_list()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

