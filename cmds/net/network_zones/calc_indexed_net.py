#\L

#  5 Nov 2014   Renamed to calc_indexed_net.
# 25 Mar 2013   Fix recursive calls so the appropriate subnet is
#               generated. Results were incorrect until I started using
#               the binary representation of the index to determine the
#               direction to branch down the tree.
# 12 Mar 2013   Convert from an interative loop to a recursive call which
#               This means the worst case is to compute a /128 address
#               from a /64 would be 63 deep as opposed to 2 ** 64 loops.
# 22 Oct 2013   initial creation.

############################################################################
# calc_indexed_net
# Given a base network, a prefix length, and an index, find the subnet
# which is at the specified index when base network is split into subnets
# of size prefix length.
# Entry:
#    base_net - the base network
#    prefix_len - the size of networks to generate
#    net_index - the index of the network to find.
# Exit:
#    The net_index network has been computed and is returned as a result.

def calc_indexed_net(base_net, prefix_len, net_index):

    # Check that we have subnets to generate:
    if ( base_net.prefix_len == prefix_len ):
        raise ValueError("cannot subnet " + str(base_net) + \
                " into multiple subnets of /" + str(prefix_len))

    # Determine how many bits are added to the network portion
    # of the address. ie. change in prefix length.
    bitsOfNet = prefix_len - base_net.prefix_len

    # Determine if the net_index is possible in the size of bitsOfNet
    if ( (2 ** bitsOfNet) < net_index ):
        raise ValueError("there are fewer than " + str(net_index) + \
                " networks of size /" + str(prefix_len) + " in " + str(base_net))

    # Turn the index into a binary value of bitsOfNet bits:
    bin_index = bin(net_index)[2:].zfill(bitsOfNet)

    # Now that we have everything checked out, figure out the network.
    result = _calc_indexed_net(base_net, prefix_len, bin_index)

    return(result)
## end calc_indexed_net()

# Recurse down the tree to find the answer rather than looping through all
# the subnets.

def _calc_indexed_net(base_net, prefix_len, bin_index):

    # Compute all of the networks - gives us a 21 digit number of networks
    # for computing a /128 address from a /64 block!
    # ipaddress.ip_network('2001:db8::/112').num_addresses = 65536
    # netList = list(base_net.subnets(prefix_len_diff=bitsOfNet))
    # Using python 3.3 ipaddress library this could be:
    #   prefix_diff = prefix_len - base_net.prefix_len
    #   list(base_net.subnets(prefix_len_diff=prefix_diff))[subnet_index]
    # The above can take seconds to run for IPv6.

    lower_half, upper_half = list(base_net.subnets())

    # Are we on the last step?
    if ( lower_half.prefix_len == prefix_len ):
        # Yes. Which half do we want?
        if ( bin_index[0] == '0' ):
            result = lower_half
        else:
            result = upper_half
    else:
        # Which half do we need to recurse down?

        if ( bin_index[0] == '0' ):
            result = _calc_indexed_net(lower_half, prefix_len, bin_index[1:])
        else:
            result = _calc_indexed_net(upper_half, prefix_len, bin_index[1:])

    return(result)
## end _calc_indexed_net()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

