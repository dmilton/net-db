#\L

# 19 Sep 2014   initial creation.

############################################################################
# get_zone_detail
# Entry: net_zone - The network zone to return details for.
# Exit: the zone description and any defined ranges are returned.
# Exceptions:

def get_zone_detail(net_zone):
    global _zone_descriptions, _zone_ranges

    result = []

    if ( net_zone >= 0 ):
        # return specified zone information:
        my_where = "net_zone = " + str(net_zone)
    else:
        # return all zones.
        my_where = ""
    my_order = "net_zone"

    zone_desc_list = db_access.get_table_rows(
            _zone_descriptions, my_where, my_order)

    for a_zone_desc in zone_desc_list:
        zone_detail = { 'net_zone': a_zone_desc['net_zone'] }
        zone_detail = { 'zone_descr': a_zone_desc['zone_descr'] }
        zone_detail = { 'ranges': [] }
        my_where = "net_zone = " + str(a_zone_desc['net_zone'])
        zone_range_list = db_access.get_table_rows(
                _zone_ranges, my_where, my_order)
        for a_range in zone_range_list:
            zone_detail['ranges'].append(a_range)
        result.append(zone_detail)

    return(result)
## end get_zone_detail()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

