#\L

# 14 Aug 2014   No longer returns a list since with the zone description
#               table there should only ever be one match.
# 20 May 2013   initial creation.

############################################################################
# get_zone_desc
# Entry: the_zone_id - the zone id to get the description for.

def get_zone_desc(the_zone_id):
    global _zone_descriptions

    my_where = "net_zone = " + str(the_zone_id)
    zoneInfo = db_access.get_table_rows(_zone_descriptions, my_where)
    if ( len(zoneInfo) == 0 ):
        raise NetException("NoZone",
            "the zone id " + str(the_zone_id) + " has not been defined yet.")
    
    return(zoneInfo[0])
## end get_zone_desc()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

