#

# 20 May 2014   Check for presence of the provided net_zone in the
#               zone descriptions table.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 25 Apr 2013   initial writing.

############################################################################
# add_range
# Add the new zone to the database.
# Entry: new_zone - the dictionary of the zone to add to the database.

def add_range(new_zone):
    """
    # Start with an empty template
    new_zone = new_template()
    # Update the fields you want to change.
    new_zone['network_range'] = ipaddr.IPNetwork("192.0.2.0/24")
    # optionally set the network zone. the default is zero.
    new_zone['net_zone'] = 777
    # Subnet 0 is where all links and loopback addresses are assigned from.
    # The whole thing is broken into /112 chunks where the '0' chunk is for
    # loopback addresses.
    new_zone['next_loop'] = 1
    # link addresses are allocated starting at 2 just to be even.
    new_zone['next_link'] = 2
    # Under normal circumstances, these fields should start with the defaults.
    del new_zone['next_bldg']
    del new_zone['next_link']
    del new_zone['next_loop']
    del new_zone['max_net']
    # 
    add_range(new_zone)
    """
    global _zone_descriptions, _zone_ranges

    # If we have a net_zone, make sure it is valid.
    if ( 'net_zone' in new_zone ):
        my_where = 'net_zone = ' + str(new_zone['net_zone'])
        result = db_access.get_row_count(_zone_descriptions, my_where)
        if ( result == 0 ):
            raise NetException("NoZone",
                "the zone id " + str(new_zone['net_zone']) + 
                " has not been defined yet.")

    # Insert the new network into the table.
    try:
        result = db_access.set_table_row(_zone_ranges, new_zone)
    except:
        raise NetException("DuplicateZone", \
            "this range already exists in the database.")

    return(result)
## end def add_range():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

