#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_range
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        zone entry in the database.

def new_range():
    global _zone_ranges

    result = db_access.new_table_row(_zone_ranges)

    return(result)
## end new_range()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

