#\L

# 25 Apr 2013        initial creation.

############################################################################
# get_range_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_range_fields():
    global _zone_ranges

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_zone_ranges)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_range_fields()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

