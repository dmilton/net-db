#\L

#  2 Jun 2014   Change function call update_zone to update_zone_net.
# 23 Oct 2013    Result is now just the netid since the calculation done
#                by net_id_2_bldg_net is broken for IPv4 and would be duplication
#                of networks routines get_net_index and get_indexed_net.
# 17 Oct 2013    Add the option to not update the zone.
# 13 Sep 2013    initial creation.

############################################################################
# get_next_bldg
# Determine the next network block to assign to a building. The default
# behaviour is to update the zone to show that a block has been assigned.
# To prevent the update from happening but return what would happen instead,
# set the no_update value to True.
# Entry:
#    range_info - the address range record to get information from.
#    no_update - do not update the range, just return information.
# Exit:
#    the id/index to use for the next building is returned. As a side
#    effect the id stored in the database is incremented.

def get_next_bldg(range_info, no_update=False):

    # Get the next building id.
    try:
        bldg_id = range_info['next_bldg']
    except:
        raise ValueError("Zone does not have user address space assigned.")

    # Extract the id of the next building to assign.
    result = range_info['next_bldg']

    if ( no_update == False ):
        # Update the zone record.
        range_info['next_bldg'] += 1
        update_zone_net(range_info)

    return(result)
## end get_next_bldg()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

