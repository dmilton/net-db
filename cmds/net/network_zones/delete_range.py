#

# 25 Apr 2013        initial creation.

############################################################################
# delete_range
# Given a zone dictionary, delete the zone from the database.
# Entry:
#        the_range - the zone dictionary to delete.
# Exit: the zone has been deleted from the database.

# 4 Feb 2012    initial creation

def delete_range(the_range):
    global _zone_ranges

    myWhere = "network_range = '" + str(the_range['network_range']) + "'"
    db_access.delete_table_rows(_zone_ranges, myWhere)

    return()
## end def delete_range():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

