#\L

# 26 Apr 2013        Simplify, taking the building zone to get the record
#                    for that zone. Using the network and building, calculate
#                    the base address space to use for a new building.
# 3 May 2012        initial creation.

############################################################################
# next_bldg
#
# Calculates the base IPv6 network to assign to a building.
# The assumption is that all base prefixes must have an even number of
# bits in the network portion based on nibble boundaries.
#
# The bits for the building are allocated counting up from the left of
# the network portion of the allocation. The bits for the network within
# the building are counting up from the right of the network portion.
#
def next_bldg(bldg_zone):

    # Get the allocation block for this zone.
    zone_info_list = get_zones_by_id(bldg_zone, 6)

    # Loop through the list to get the 'user' space.
    for a_zone in zone_info_list:
        if ( a_zone['net_type_list'] == '' ):
            # empty type list means good for anything.
            zone_info = a_zone
        elif ( 'user' in a_zone['net_type_list'] ):
            # type list explicitly for 'user' space.
            zone_info = a_zone
            break

    # Get the next building id.
    try:
        ipv6BldgId = zone_info['next_bldg']
    except:
        raise ValueError("Zone does not have IPv6 user address space assigned.")

    # Increment the building value.
    zone_info['next_bldg'] += 1

    # Get the network for the building.
    zoneNet = net_id_2_bldg_net(ipv6BldgId, 0, zone_info['network'])

    result = {'ipv6_netid': ipv6BldgId, 'ipv6_netbase': zoneNet}

    return(result)
## end next_bldg()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

