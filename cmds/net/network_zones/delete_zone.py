#

# 15 May 2014   initial creation.

############################################################################
# delete_zone
# Given a zone dictionary, delete the zone from the database.
# This will remove the zone description, all address spaces in that zone.
# Ideally, all buildings within the zone should be deleted as well but
# this introduces a circular dependency between buildings and zones.
#
# Entry:
#       net_zone - the zone id to delete.
# Exit: the zone description and all address spaces in that zone have
#       been deleted from the database.

def delete_zone(net_zone):
    global _zone_descriptions, _zone_ranges

    # Where clause for the net zone id.
    my_whene = "net_zone = '" + str(net_zone) + "'"

    # Delete for the zone address spaces.
    db_access.delete_table_rows(_zone_ranges, my_whene)

    # Delete for the zone description.
    db_access.delete_table_rows(_zone_descriptions, my_whene)

    return
## end def delete_zone():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

