#

# 28 Aug 2014   Table name changed and now have additional table
#               so added check for the name change.
# 25 Apr 2013   initial creation

##############################################################################
# initialize()
# Perform initialization process for network_zones table.
# Entry: none. initializes internal variables for use.
# Exit: network database fields are now ready to use.

def initialize():
    """
    initialize the network zone access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _zone_descriptions, _zone_ranges
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Table of zone descriptions.
    _zone_descriptions.update(db_access.init_db_table(_zone_descriptions))

    # Table of network address blocks assigned to specific zones.
    _zone_ranges.update(db_access.init_db_table(_zone_ranges))

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

