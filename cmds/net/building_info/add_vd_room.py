#

# 28 Nov 2013   Add call to _check_vd_room to verify what we are given.
# 11 Oct 2013   Initial creation

##############################################################################
# add_vd_room(room_info)
# Add a new building to the building_detail table.
# Entry:    room_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_vd_room(room_info):

    global _voice_data_rooms

    # Create a new record.
    new_room = db_access.new_table_row(_voice_data_rooms)

    # Copy matching values from room_info into our new record.
    # This eliminates any extraneous vaules in room_info.
    for a_key in list(new_room.keys()):
        if ( a_key in room_info ):
            new_room[a_key] = room_info[a_key]
        else:
            del new_room[a_key]

    _check_vd_room(new_room)

    # Add the record into the database.
    result = db_access.set_table_row(_voice_data_rooms, new_room, '')[0]

    return(result)
## end add_vd_room():

# vim: syntax=python ts=4 sw=4 showmatch et :

