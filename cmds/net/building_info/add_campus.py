#

# 27 Nov 2013   Add checks for duplicate id.
# 20 Sep 2013   set_table_row now returns a result after the insert or
#               update query has completed. The get_table_rows call is no
#               longer necessary.
# 18 Sep 2013   Initial creation

##############################################################################
# add_campus(campus_info)
# Add a new building to the building_detail table.
# Entry:    campus_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_campus(campus_info):

    global _campus_locations

    # Create a new record.
    new_campus = db_access.new_table_row(_campus_locations)

    # Copy matching values from campus_info into our new record.
    # This eliminates any extraneous vaules in campus_info.
    for a_key in list(new_campus.keys()):
        if ( a_key in campus_info ):
            new_campus[a_key] = campus_info[a_key]
        else:
            del new_campus[a_key]
        ## end else if ( a_key in campus_info ):
    ## end for a_key in new_campus.keys():

    # Check what we have - name (and id if provided) must be unique.
    _check_campus(new_campus)

    # Build a where clause to identify this building.
    my_where = ""

    # Add the record into the database.
    result = db_access.set_table_row(_campus_locations, new_campus, my_where)[0]

    return(result)
## end add_campus():

# vim: syntax=python ts=4 sw=4 showmatch et :

