#\L

# 23 Aug 2014   Default to only showing campus names with an id > 0
#               to hide the two internal ones. Add flag to permit
#               display of the default too.
#               list doesn't display extraneous information.
# 17 Jun 2014   Check for campus_id > 0 rather than != 0 so we can
#               pass a negative value as well as zero.
# 14 May 2013   initial creation.

############################################################################
# get_campus_info
# Given a campus_id, extract information from the database about that campus.
# Entry:
#       campus_id   the campus id to obtain information on.
#       show_any    if true, show any campus location, including internal ones.
#       
# Exit: A list of campus records is returned.

def get_campus_info(campus_id, show_any):

    global _campus_locations

    # Default campus is id 0, only show that when specifically requested.
    if ( show_any or campus_id >= 0 ):
        my_where = "campus_id = " + str(campus_id)
    else:
        my_where = "campus_id > 0"
    my_order = 'campus_id'
    result = db_access.get_table_rows(_campus_locations, my_where, my_order)

    return(result)
## end get_campus_info()

# vim: syntax=python ts=4 sw=4 showmatch et :

