#

# 16 Apr 2015   Initial creation

##############################################################################
# _check_building(campus_info)
# Verify that the campus info is unique.
# Entry:    campus_info - dictionary of values from the building.
# Exit:     the location (and id if provided) is verified to be unique.
#    if the values are not unique an exception is raised.

def _check_building(building_info):

    global _campus_locations, _building_detail

    # Check that we have a unique campus id if provided
    if ( 'campus_id' in building_info ):
        my_where = "campus_id = '" + campus_info['campus_id'] + "'"
        id_count = db_access.get_row_count(_campus_locations, my_where)
        if ( id_count == 0 ):
            raise NetException("InvalidCampusId",
                    "The campus ID " + building_info['campus_id'] + \
                    " does not exist.")

    # Check that the building type is valid.
    if ( 'bldg_type' in building_info ):
        valid = building_types.verify_building_type(building_info['bldg_type'])
        if ( not valid ):
            raise NetException("InvalidBldgType",
                    "The building type '" + building_info['bldg_type'] +
                    "' does not exist.")

    # Check that the network zone is valid.
    zone_desc = network_zones.get_zone_descr(building_info['net_zone'])

    # Check that we have a unique building code.
    my_where = "bldg_code = '" + building_info['bldg_code'] + "'"
    bldg_code_count = db_access.get_row_count(_building_detail, my_where)
    if ( bldg_code_count > 0 ):
        raise NetException("BldgCodeExists",
                "The building code '" + building_info['bldg_code'] + \
                "' already exists.")

    return
## end _check_building():

# vim: syntax=python ts=4 sw=4 showmatch et :

