#

# 19 Nov 2014   Change the addition of building_campus table info so that
#               all fields are added.
# 15 Aug 2014   Expand the error detail to make diagnostics to the user
#               easier to understand.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 21 Jun 2013   Update to use two table queries so the buildings view
#               is not required for the code to work.
#  7 Jun 2012   Add new fields.
# 20 Dec 2010   changed to use db_access module for query generation
#               and making the dictionary result.
#               initial creation, copied from network.py

############################################################################
# get_building_info
#
# Input: bldg_code - the building code to get information about.
# Output: dictionary of information regarding the specific building.
# Exception: UnknownBuildingCode.
#
def get_building_info(bldg_code):
    """
    Check the database for a building code.
    try:
        bldg_info = get_building_info(bldg_code)
    except:
        print("unknown building code")
    """

    global _building_detail, _campus_locations

    try:
        # Get the building_detail table entry.
        my_where = "bldg_code = '" + bldg_code + "'"
        result = db_access.get_table_rows(_building_detail, my_where)[0]
        # Add the corresponding building_campus table entry.
        my_where = "campus_id = " + str(result['campus_id'])
        campus_info = db_access.get_table_rows(_campus_locations, my_where)[0]
        for a_key in list(campus_info.keys()):
            result[a_key] = campus_info[a_key]
    except TypeError:
        # or dbResult does not have a length - it is an integer.
        raise NetException("UnknownBuildingCode", 
                "Invalid building code '" + bldg_code + "'")
    except IndexError:
        # we get here from the dbResult[0] since the result is not an array.
        raise NetException("UnknownBuildingCode", 
                "Invalid building code '" + bldg_code + "'")
    except Exception:
        # something unexpected happened, raise it so we can debug the problem.
        raise
    ## end try

    return(result)
## end get_building_info()

# vim: syntax=python ts=4 sw=4 showmatch et :

