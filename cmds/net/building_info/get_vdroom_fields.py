#\L

# 21 Mar 2013   define result.
# 18 Mar 2013   initial creation.

############################################################################
# get_vdroom_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to building rooms.

def get_vdroom_fields():

    global _voice_data_rooms

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_voice_data_rooms)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_vdroom_fields()

# vim: syntax=python ts=4 sw=4 showmatch et :

