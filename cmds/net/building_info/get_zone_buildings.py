#

# 20 May 2014   initial creation

############################################################################
# get_zone_buildings
#
# Input: net_zone - the network zone to get a list of buildings for.
# Output: list of building info dictionaries.
#
def get_zone_buildings(net_zone):
    """
    Get the list of building using net_zone address spaces.
    bldg_list = building_info.get_zone_buildings(net_zone)
    """

    global _building_detail

    my_where = "net_zone = " + str(net_zone)

    result = db_access.get_table_rows(_building_detail, my_where)

    return(result)
## end get_zone_buildings()

# vim: syntax=python ts=4 sw=4 showmatch et :

