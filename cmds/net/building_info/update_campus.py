#

# 25 Sep 2014   Check the campus exists and raise an exception if not.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 27 Nov 2013   Add check to ensure the location is unique.
# 18 Sep 2013   Initial creation

##############################################################################
# update_campus(campus_info)
# Add a new building to the building_detail table.
# Entry:    campus_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def update_campus(campus_info):

    global _campus_locations

    # Build a where clause to identify this building.
    my_where = "campus_id = " + str(campus_info['campus_id'])

    # Check the name is unique
    name_check = {'campus_name':campus_info['campus_name']}
    _check_campus(name_check)

    # Make sure the record exists:
    row_count = db_access.get_row_count(_campus_locations, my_where)

    if ( row_count == 0 ):
        raise NetException("not found",
                "The campus ID " + str(campus_info['campus_id']) +
                " was not found in the database.")

    # Update the record in the database.
    result = db_access.set_table_row(
            _campus_locations, campus_info, my_where)[0]

    return(result)
## end update_campus():

# vim: syntax=python ts=4 sw=4 showmatch et :

