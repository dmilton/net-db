#\L

# 14 Mar 2013   initial creation.

############################################################################
# new_building
# Create a new empty template for a building.
def new_building():

    global _building_detail

    # Do something here.
    result = db_access.new_table_row(_building_detail)

    return(result)
## end new_building()

# vim: syntax=python ts=4 sw=4 showmatch et :

