#

#  9 Sep 2014   Add exception for a delete that deleted zero rows.
# 18 Sep 2013   Initial creation

##############################################################################
# delete_campus(campus_info)
# Delete the campus record. All dependencies must be resolved BEFORE calling.
# Entry:
#       campus_info - dictionary of values from the building.
# Exit:
#       a new building record has been added to the database.
# Exceptions:
#       A UnknownCampusID NetException is raised if no rows are deleted.

def delete_campus(campus_info):

    global _campus_locations

    # Build a where clause to identify this building.
    my_where = "campus_id = " + str(campus_info['campus_id'])

    # Delete the record, cascade to get other rows dependant on this one.
    rows = db_access.delete_table_rows(_campus_locations, my_where)
    if ( rows == 0 ):
        raise NetException("UnknownCampusID", 
            "Campus with ID " + str(campus_info['campus_id']) + " not found.")

    return
## end delete_campus():

# vim: syntax=python ts=4 sw=4 showmatch et :

