#

# 11 Aug 2013   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 27 Nov 2013   Initial creation

##############################################################################
# _check_campus(campus_info)
# Verify that the campus info is unique.
# Entry:    campus_info - dictionary of values from the building.
# Exit:     the location (and id if provided) is verified to be unique.
#    if the values are not unique an exception is raised.

def _check_campus(campus_info):

    global _campus_locations

    # Check that we have a unique name
    my_where = "lower(campus_name) = lower('" 
    my_where += campus_info['campus_name'] + "')"
    name_count = db_access.get_row_count(_campus_locations, my_where)
    if ( name_count > 0 ):
        raise NetException("duplicate",
                "A campus named '" + campus_info['campus_name'] + \
                "' already exists.")

    # Check that we have a unique campus id if provided
    if ( 'campus_id' in campus_info ):
        my_where = "campus_id = '" + campus_info['campus_id'] + "'"
        id_count = db_access.get_row_count(_campus_locations, my_where)
        if ( id_count > 0 ):
            raise NetException("duplicate",
                    "The campus ID " + campus_info['campus_id'] + \
                    " already exists.")

    return
## end _check_campus():

# vim: syntax=python ts=4 sw=4 showmatch et :

