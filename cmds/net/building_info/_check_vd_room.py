#

# 28 Nov 2013   Initial creation

##############################################################################
# _check_vd_room(room_info)
# Verify that the campus info is unique.
# Entry:    room_info - dictionary of values from the building.
# Exit:    the location (and id if provided) is verified to be unique.
#    if the values are not unique an exception is raised.

def _check_vd_room(room_info):

    global _voice_data_rooms

    # Check that we have a unique name
    my_where = "lower(room) = lower('" + room_info['room'] + "')"
    my_where += " AND bldg_code = '" + room_info['bldg_code'] + "'"
    room_count = db_access.get_row_count(_voice_data_rooms, my_where)
    if ( room_count > 0 ):
        raise ValueError("Voice/Data Room " + room_info['room'] + \
            " already exists" + " in building " + room_info['bldg_code'])

    return
## end _check_vd_room():

# vim: syntax=python ts=4 sw=4 showmatch et :

