#

# 31 May 2012   initial creation.

##############################################################################
# change_zones(theInfo)
# Update a building in the building_detail table.
# Entry:    theInfo building_detail dictionary
# Exit:        the table row as updated is returned.

def change_zones(old_zone, new_zone):

    global _building_detail

    # Get the current record.
    my_where = "net_zone = " + str(old_zone)
    nbr_bldgs_in_zone = db_access.get_row_count(_building_detail, my_where)
    if ( nbr_bldgs_in_zone == 0 ):
        raise NetException("NoZone",
                "No buildings were found in zone " + str(old_zone))

    # We found some buildings to update.
    update_row_count = db_access.change_table_rows(
            _building_detail, 'net_zone', old_zone, new_zone, False)

    if ( update_row_count != nbr_bldgs_in_zone ):
        raise NetException("UpdateFailure",
                "for some reason " + str(update_row_count) +
                " rows were updated but " + str(nbr_bldgs_in_zone) +
                " were originally found.")

    return(update_row_count)
## end change_zones():

# vim: syntax=python ts=4 sw=4 showmatch et :

