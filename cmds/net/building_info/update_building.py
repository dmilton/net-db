#

# 23 Sep 2013   set_table_row now returns a list (using RETURNING clause)
#               so dereference list to a single item.
# 15 Apr 2013   get_table_rows returns a list but this function always
#               return a single building entry.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
# 15 Mar 2013   Improve checks to eliminate keys that have no value.
# 26 Feb 2013   Add checks to eliminate keys in the detail record
#               that aren't in teh bldg_info record we received.
#               Remove LOWER so case sensitive checks can be made.
#  6 Nov 2012   Rewrite to use dictionary based db_access routines.
# 31 May 2012   initial creation.

##############################################################################
# update_building(theInfo)
# Update a building in the building_detail table.
# Entry:    theInfo building_detail dictionary
# Exit:        the table row as updated is returned.

def update_building(bldg_info):

    global _building_detail

    # Get the current record.
    my_where = "bldg_code = '" + bldg_info['bldg_code'] + "'"
    curr_bldg_list = db_access.get_table_rows(_building_detail, my_where)
    if ( len(curr_bldg_list) == 0 ):
        raise NetException("UnknownBuildingCode", bldg_info['bldg_code'])
    else:
        curr_bldg = curr_bldg_list[0]

    # Create a new empty record.
    update_info = db_access.new_table_row(_building_detail)

    # Copy matching values from bldg_info into our new record.
    # This eliminates extraneous vaules in bldg_info.
    for a_key in list(update_info.keys()):
        key_exists = a_key in bldg_info
        if ( key_exists == True ):
            key_not_none = ( bldg_info[a_key] != None )
            key_not_dup = (bldg_info[a_key] != curr_bldg[a_key])
        if ( key_exists and key_not_none and key_not_dup ):
            update_info[a_key] = bldg_info[a_key]
        else:
            del update_info[a_key]

    # Update the record in the database.
    result = db_access.set_table_row(_building_detail, update_info, my_where)[0]

    return(result)
## end update_building():

# vim: syntax=python ts=4 sw=4 showmatch et :

