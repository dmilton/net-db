#

#  9 Sep 2014   Add exception for deleting zero rows.
# 11 Oct 2013   Initial creation

##############################################################################
# delete_vd_room(room_info)
# Delete the room record. All dependencies must be resolved BEFORE calling.
# Entry:    room_info - dictionary containing the room_id to delete.
# Exit:    the room has been removed from the database.

def delete_vd_room(room_info):

    global _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "room_id = " + str(room_info['room_id'])

    # Delete the record.
    rows = db_access.delete_table_rows(_voice_data_rooms, my_where)
    if ( rows == 0 ):
        raise NetException("UnknownRoomID",
            "Unable to delete room: " + str(room_info))

    return
## end delete_vd_room():

# vim: syntax=python ts=4 sw=4 showmatch et :

