#

# 11 Oct 2013   Initial creation

##############################################################################
# update_vd_room(room_info)
# Add a new building to the building_detail table.
# Entry:    room_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def update_vd_room(room_info):

    global _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "room_id = " + str(room_info['room_id'])

    # Update the record in the database.
    result = db_access.set_table_row(_voice_data_rooms, room_info, my_where)[0]

    return(result)
## end update_vd_room():

# vim: syntax=python ts=4 sw=4 showmatch et :

