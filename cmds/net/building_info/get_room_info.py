#\L

#  6 Dec 2013   initial creation.

############################################################################
# get_room_info
# Given a room id, return the name of the room.
# Entry:
#    room_id - the id of the room.
# Exit:
#    if the room id is in the database the record is returned.

def get_room_info(room_id):
    global _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "room_id = " + str(room_id)

    # Update the record in the database.
    db_result = db_access.get_table_rows(_voice_data_rooms, my_where)

    if ( len(db_result) == 1 ):
        result = db_result[0]
    else:
        raise ValueError("A room with id " + str(room_id) + \
                " could not be found.")

    return(result)
## end get_room_info()

# vim: syntax=python ts=4 sw=4 showmatch et :

