#\L

# 17 Apr 2013   initial creation.

############################################################################
# find_router_buildings
# Entry: the_router - which router to search buildings for.
# Exit: A search of all buildings is made, looking for the specified router.
#        All building which use that router are returned.

def find_router_buildings(the_router):
    global _building_detail

    my_where = "router = '" + the_router + "'"
    result = db_access.get_table_rows(_building_detail, my_where)

    if ( len(result) == 0 ):
        raise NetException("UnknownBuilding", \
            "could not find " + the_router + " listed in any building." )

    return(result)
## end find_router_buildings()

# vim: syntax=python ts=4 sw=4 showmatch et :

