#

# 23 Sep 2013   set_table_rows now returns a list (via RETURNING clause)
#               but the result here should be a single item.
# 15 Apr 2013   get_table_rows returs a list but this function always
#               returns a single building entry.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
# 26 Feb 2013   add check to ensure bldg_info has all of the keys
#               that are in a building_detail record and delete
#               the keys in the new record that aren't in bldg_info.
#               Remove the lower case compare so that a case sensitive
#               building code comparison can take place.
#  6 Nov 2012   Rewrite to use db_access.
# 19 Oct 2012   The PgQuery call was missing db_access, changed
#               indentation to use tabs exclusively.
# 10 Dec 2010   Changed to use db_access library for query generation.
#               Initial creation, copied from network_rw.py

##############################################################################
# add_building(bldg_info)
# Add a new building to the building_detail table.
# Entry:    bldg_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_building(bldg_info):

    global _building_detail

    # Create a new record.
    new_bldg = db_access.new_table_row(_building_detail)

    # Copy matching values from bldg_info into our new record.
    # This eliminates any extraneous vaules in bldg_info.
    for a_key in list(new_bldg.keys()):
        if ( a_key in bldg_info ):
            new_bldg[a_key] = bldg_info[a_key]
        else:
            del new_bldg[a_key]

    # Build a where clause to identify this building.
    my_where = "bldg_code = \'" + new_bldg['bldg_code'] + "\'"

    # Add the record into the database.
    result = db_access.set_table_row(_building_detail, new_bldg, my_where)[0]

    return(result)
## end add_building():

# vim: syntax=python ts=4 sw=4 showmatch et :

