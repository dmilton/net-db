#
gVersion=''

# Libraries used in this module.
from net import db_access
from net.network_error import NetException
from net import network_zones
from net.cmd_line_io import print_table

# Flag to track if we have been initialized.
_tablesinitialized = False

#\
###############################################################################
# Building Campus table.
#
_campus_locations = {
	'database': 'networking',
	'role': 'netmon',
	'table': 'campus_locations',
	'index': 'campus_id'
};

###############################################################################
# Building Detail table.
#

_building_detail = {
	'database': 'networking',
	'role': 'netmon',
	'table': 'building_detail',
	'index': 'bldg_code'
};

###############################################################################
# Building Voice/Data Rooms Table
#

_voice_data_rooms = {
	'database': 'networking',
	'table': 'voice_data_rooms',
	'role': 'netmon',
	'index': 'room_id'
};

from . import _check_campus
from . import _check_vd_room
from . import add_building
from . import add_campus
from . import add_vd_room
from . import change_zones
from . import delete_building
from . import delete_campus
from . import delete_vd_room
from . import find_building_code
from . import find_router_buildings
from . import get_building_fields
from . import get_building_info
from . import get_campus_buildings
from . import get_campus_fields
from . import get_campus_info
from . import get_room_info
from . import get_vd_rooms
from . import get_vdroom_fields
from . import get_zone_buildings
from . import initialize
from . import new_building
from . import new_campus
from . import print_building
from . import print_campus
from . import update_building
from . import update_campus
from . import update_vd_room

# vim: syntax=python ts=4 sw=4 showmatch et :

