#

# 17 Jun 2014   Add check for campus_id > 0 so we can get a list of
#               all defined buildings.
# 15 May 2013   initial creation

############################################################################
# get_campus_buildings
#
# Input: campus_id - the campus-id to get a list of buildings for.
# Output: list of building info dictionaries.
#
def get_campus_buildings(campus_id):
    """
    Get the list of building on the specified campus.
    Using -1 for the campus_id will get a list of all buildings.
    bldg_list = building_info.get_campus_buildings(campus_id)
    """

    global _building_detail

    if ( campus_id > 0 ):
        my_where = "campus_id = " + str(campus_id)
    else:
        my_where = ""

    my_order = "campus_id, bldg_code"

    result = db_access.get_table_rows(_building_detail, my_where, my_order)

    return(result)
## end get_campus_buildings()

# vim: syntax=python ts=4 sw=4 showmatch et :

