#

#  7 Jun 2012   Update to use generic db_access routines.
# 31 May 2012   initial creation.

############################################################################
# get_vd_rooms
#
# Input: bldg_code - building to obtain list of V/D rooms for.
# Output: list of V/D rooms in the building.
#
def get_vd_rooms(bldg_code):
    """
    Get the list of voice data rooms in the building.
    vd_room_list = get_vd_rooms(bldg_code)
    """

    global _voice_data_rooms

    my_where = "bldg_code = '" + bldg_code + "'"

    # Now construct the full select query for this building code.
    db_result = db_access.get_table_rows(_voice_data_rooms, my_where);

    return db_result
## end get_vd_rooms()

# vim: syntax=python ts=4 sw=4 showmatch et :

