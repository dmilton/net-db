#

#  9 Sep 2014   Add exception for deleting zero buildings.
# 18 Sep 2013   initial creation

##############################################################################
# delete_building(bldg_info)
# Delete the building record. All dependancies must be removed before calling
# this routine. That means any allocated networks must be deleted or returned
# to the free pool.
# Entry:    bldg_info - dictionary of values from the building.
# Exit:    all rooms associated with this building, and the building row itself
#    have been removed from the database.

def delete_building(bldg_info):

    global _building_detail, _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "bldg_code = \'" + bldg_info['bldg_code'] + "\'"

    # Delete the voice/data rooms in the building.
    ignore = db_access.delete_table_rows(_voice_data_rooms, my_where)

    # Delete the building row.
    rows = db_access.delete_table_rows(_building_detail, my_where)
    if ( rows == 0 ):
        raise NetException("UnknownBuildingCode", 
                "Building with code" + bldg_info['bldg_code'] + "not found.")

    return
## end delete_building():

# vim: syntax=python ts=4 sw=4 showmatch et :

