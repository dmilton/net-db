#

# 13 Nov 2012   Change to use internal variables (_ prefix) for the
#               tables. Add note that the database itself must be
#               initialized.
#  5 Nov 2012   initial writing

##############################################################################
# initialize()
# Perform initialization process for building information tables. 
# Note that the parent routine must have initialized the networking database
# in order for this routine to work.
# Entry: none. initializes internal variables for use.
# Exit: building information tables are now ready to use.

def initialize():
    """
    initialize the building table access field variables.
    """

    # Our view and associated tables.
    global _campus_locations, _building_detail, _voice_data_rooms
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Note that the parent routine must have initialized the
    # database (networking) in order for this routine to work.
    # initialize the fieldList (for SELECT) and keyList (query dictionary)

    _campus_locations.update(db_access.init_db_table(_campus_locations))

    _building_detail.update(db_access.init_db_table(_building_detail))

    _voice_data_rooms.update(db_access.init_db_table(_voice_data_rooms))

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 showmatch et :

