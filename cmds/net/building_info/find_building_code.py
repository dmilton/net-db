#

# 10 Sep 2014   Sort output by building code.
# 18 Jun 2014   add check of both bldg_name and bldg_code for the string
#               to catch instances of where a building code is used
#               with --find. Remove refenece to the building view and
#               use the building detail table like everything else.
#  4 Jun 2014   change building_detail column name to bldg_name.
#  6 Nov 2012   rewrite to use new db_access routines that return dictionaries.
# 19 Oct 2012   missed the extraction of the field name from the
#               table field dictionary which caused errors.
#               PgQuery was missing the db_access. prefix.
# 31 May 2012   Reformat using tabs instead of spaces and tabs.
# 21 Feb 2012   initial creation.

############################################################################
# find_building_code
#
# Entry: name - the name of the building to search for.
# Exit: A list of building info dictionaries.
# Exception: UnknownBuilding.
#
def find_building_code(bldg_name):
    """
    Check the database for buildings that match the given name. 
    try:
        bldg_info = find_building_code(bldg_name)
    except:
        print("unknown building name")
    """

    global _building_detail

    # build the appropriate were clause to find this building.
    my_where = "LOWER(bldg_name) LIKE LOWER(\'%" + bldg_name + "%\')"
    my_where += " OR LOWER(bldg_code) LIKE LOWER(\'%" + bldg_name + "%\')"

    # Now grab the appropriate row from the table.
    db_result = db_access.get_table_rows(_building_detail, my_where)

    if ( len(db_result) == 0 ):
        raise NetException("UnknownBuilding", bldg_name)

    # Sort the list by building code.
    result = sorted( db_result , key=lambda elem: elem['bldg_code'] )

    return(result)
## end find_building_code()

# vim: syntax=python ts=4 sw=4 showmatch et :

