#

# 21 Jan 2013   Initial creation from find-ip-info.

############################################################################
# find_node_entries:
# Given search criteria (lowMac, highMac, and active) find entries in the
# netdisco node table for that mac or range of mac addresses. 
# Input:
#        lowMacAddr - the lower MAC address to find the switch ports for.
#        highMacAddr - the upper MAC address to find the switch ports for.
#        activeOnly - only find the active switch port.
# Output: list of dictionaries describing the switch ports.

def find_node_entries(lowMacAddr, highMacAddr, activeOnly):

    # Node table contains the switch port information gleaned from the
    # MAC address forwarding tables.
    global _node

    # Construct our where clause.
    if ( lowMacAddr == highMacAddr ):
        # only looking for one MAC address, only find one switch port.
        myWhere = "mac = '" + lowMacAddr + "'"
        myOrder = 'time_last'
    else:
        myWhere = "mac >= '" + lowMacAddr + "'"
        myWhere += " and mac <= '" + highMacAddr + "'"
        myOrder = 'time_last, active'
    ## end else if ( lowMacAddr == highMacAddr ):

    if ( activeOnly == True ):
        myWhere += " and active = 'T'"
    ## end if ( activeOnly == True ):

    # Get the results of our query.
    result = db_access.get_table_rows(_node, myWhere, myOrder)

    return result
## end def find_node_entries():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

