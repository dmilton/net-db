#
gVersion=''

from socket import gethostbyaddr

from net.network_error import NetException
from net import db_access

###############################################################################
# Network Devices Table (switches and routers)
# This table identifies switch and router device information.

_device = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'device',
    'index': 'ip'
}

###############################################################################
# Table of switch ports by ip of switch.
# This table maps out all of the available switch ports.

_devicePort = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'device_port',
    'index': 'port, ip'
}

###############################################################################
# Network Node IP Table
# This table maps out all of the IP to MAC address (ARP/NDP) relationships.

_nodeIp = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'node_ip',
    'index': 'ip, mac'
}

###############################################################################
# Network Node Table (mac address forwarding table leaves)
# This table maps a MAC address to a switch port name.

_node = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'node',
    'index': 'mac, switch, port'
}

from . import find_arp_entries
from . import find_node_entries
from . import find_port_info
from . import find_room_ports
from . import get_snmp_comm
from . import initialize
from . import print_arp_entry
from . import print_switch_ports

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

