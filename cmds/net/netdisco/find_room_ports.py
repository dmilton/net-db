#\L

# 11 Jun 2014   Make search use lower case to force a case insensitive
#               search of the table.
# 15 Apr 2013   devIpOrNet is an IPNetwork object, convert to string
#               for the sql where clause.
# 29 Jan 2013   initial creation.

############################################################################
# find_room_ports
# Entry:
#        devIpOrNet - the CIDR representation of the management network
#                    for the building in question. This could also be the
#                    loopback address of a router.
#        roomAddr - the communications outlet or room number.
# Exit:        portList - a list of ports that match the roomAddr criteria.
#

def find_room_ports(devIpOrNet, roomAddr):

    global _devicePort

    myWhere = "ip <<= '" + str(devIpOrNet) + "' AND " + \
            "LOWER(name) LIKE LOWER('%" + roomAddr + "%')"
    result = db_access.get_table_rows(_devicePort, myWhere)

    return(result)
## end find_room_ports()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

