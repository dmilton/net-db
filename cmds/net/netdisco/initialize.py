#

# 24 Jan 2013   Add nodeIP table initialization.
# 10 Jan 2013   Initial Creation

##############################################################################
# initialize()
# Perform initialization process for netdisco database. 
# Entry: none. initializes internal variables for use.
# Exit: network database fields are now ready to use.

def initialize():
    """
    initialize the networks table access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _device, _devicePort, _node, _nodeIp
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Table of device information for network switches/routers.
    _device.update(db_access.init_db_table(_device))
    # Table of switch port information by device/switch.
    _devicePort.update(db_access.init_db_table(_devicePort))
    # Table of MAC addresses by associated switch port.
    _node.update(db_access.init_db_table(_node))
    # Table of ARP entries for nodes connected to the network.
    _nodeIp.update(db_access.init_db_table(_nodeIp))

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

