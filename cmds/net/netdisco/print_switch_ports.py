#

# 18 Mar 2014   Change single line output format.
# 11 Mar 2013   remove explicit references to netdisco module since
#               this is now part of that module. Change so the MAC
#               address is displayed but only the first time it appears.
#               Juniper switches report MAC addresses on sub interface .0
#               so we need to chop that off to get descriptions.
# 11 Mar 2013   initial creation from find-ip-info

############################################################################
# print_switch_ports
# Given a list of switch ports, print them out for the user.
# Entry:    portList - list of ports.
#            briefFlag - single (brief) or multi-line (not brief) output.

def print_switch_ports(portList, briefFlag):

    lastMac = ""
    thisMac = ""
    for aPort in portList:
        # prepare check against previous MAC so see if we need to display it.
        lastMac = thisMac
        thisMac = aPort['mac']
        # switchName =  dns lookup.
        try:
            switchName = socket.gethostbyaddr(
                    str(aPort['switch']))[0].split('.')[0]
        except socket.herror as reason:
            switchName = str(aPort['switch']) + " (no reverse DNS!)"
        # Juniper reports interface as unit .0 so we need to chop it off.
        aPort['port'] = aPort['port'].split('.')[0]
        try:
            portDetail = find_port_info(aPort['switch'], aPort['port'])
        except ValueError as reason:
            print(reason)
            portDetail = {}
            portDetail['name'] = 'N/A'
        ## end try:
        message = ""
        # display info for user.
        if ( thisMac != lastMac ):
            if ( briefFlag == True ):
                message = aPort['mac']
                if ( len(portList) == 1 ):
                    message += "\t"
                else:
                    message += "\n"
            else:
                message = "The MAC address " + aPort['mac'] + "\n"
            ## end else if ( briefFlag == True ):
        ## end if ( thisMac != lastMac ):
        if ( briefFlag == True ):
            message += portDetail['name'] + "\t"
            message += switchName + " " + aPort['port'] + "\t"
            message += " currently " + portDetail['up']
        else:
            message += "was seen connected to " + switchName + " "
            message += "interface " + aPort['port'] + "\n"
            message += "desc " + portDetail['name'] + " "
            if ( 'up' in portDetail ):
                message += "state is " + portDetail['up']
        ## end else if ( briefFlag == True ):
        print(message)
    ## end for aPort in portList:

## end def print_switch_ports():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

