#

#  7 Mar 2013   Initial creation.

############################################################################
# find_port_info:
# Given a switch ip and port name, get the rest of the details for the port.
# Input:
#        switchIp - ip address of the switch.
#        portName - name of the interface.
# Output: dictionary describing the switch port.

def find_port_info(switchIp, portName):

    # Node table contains the switch port information gleaned from the
    # MAC address forwarding tables.
    global _devicePort

    myWhere = "ip = '" + str(switchIp) + "'"
    myWhere += " and port = '" + portName + "'"
    result = db_access.get_table_rows(_devicePort, myWhere, '', 1)
    if ( len(result) == 1 ):
        result = result[0]
    else:
        raise ValueError("switch: " + str(switchIp) + \
                ", port: " + portName + " not found")

    return result
## end def find_port_info():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

