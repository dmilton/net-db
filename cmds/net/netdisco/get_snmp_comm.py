#\L

#  3 Apr 2013   Update logic to create an exception when the
#               community string cannot be found.
# 24 Jan 2013   initial creation.

############################################################################
# get_snmp_comm
def get_snmp_comm(deviceName):
    global _device

    # Do something here.
    myWhere = "name LIKE '" + deviceName + "%'"
    dbResult = db_access.get_table_rows(_device, myWhere)

    if ( len(dbResult) >= 1 ):
        result = dbResult[0]['snmp_comm']
    else:
        raise NetException("NoSnmpCommunity", \
                "Could not determine snmp community for device: " + deviceName)

    return(result)
## end get_snmp_comm()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

