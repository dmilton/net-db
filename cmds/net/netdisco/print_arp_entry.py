#\L

# 18 Mar 2014    Change brief output to be a single tab delimited line.
#  1 May 2013    initial creation.

############################################################################
# print_arp_entry
# Print an arp entry for the user.
# Entry:    oneArpEntry - dictionary of values for the arp entry.
#            briefFlag - single line (brief) or multi-line (not brief) output.

def print_arp_entry(oneArpEntry, briefFlag):
    if ( oneArpEntry['active'] == True ):
        arpState = 'active'
    else:
        arpState = 'inactive'
    if ( briefFlag == True ):
        print( str(oneArpEntry['ip']) + '\t' + oneArpEntry['mac'] + '\t' + \
            str(oneArpEntry['time_last']) +'\t' + arpState )
    else:
        print("The IP Address " + str(oneArpEntry['ip']))
        print("   was recorded from MAC address " + oneArpEntry['mac'])
        print("   and last seen " + str(oneArpEntry['time_last']))
        print("   The last known state was " + arpState)

## end def print_arp_entry():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

