#

# 18 Mar 2014   Add flag to get only the active entry.
#  6 Mar 2013   allow specification of mac and/or ip address.
# 21 Jan 2013   Initial rewrite from find-ip-info using db_access.

############################################################################
# find_arp_entries:

def find_arp_entries(faMacAddr, faIpAddr, activeOnly=False):

    global _nodeIp

    # Create the where clause for the arp entries we want to find.
    if ( str(faIpAddr) != "" and str(faMacAddr) != "" ):
        myWhere = "ip = '" + str(faIpAddr) + "' AND " + \
            "mac = '" + faMacAddr + "'"
    elif ( str(faIpAddr) != "" and str(faMacAddr) == "" ):
        myWhere = "ip = '" + str(faIpAddr) + "'"
    elif ( str(faIpAddr) == "" and str(faMacAddr) != "" ):
        myWhere = "mac = '" + faMacAddr + "'"
    else:
        raise ValueError('no ip or mac addresses to find')

    if ( activeOnly ):
        myWhere += " AND active = 'T'"

    myOrder = "time_last, active"

    # Now get the information from the database.
    result = db_access.get_table_rows(_nodeIp, myWhere, myOrder)

    return result
## end find_arp_entries()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

