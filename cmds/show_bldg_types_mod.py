#
# Version information for this command.
#

g_version='dd mmm yyyy - 1.0.0'

#  6 Apr 2015   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:

    import building_types

except ImportError as msg:

    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)

except Exception as exc:

    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print( """
usage: %s --help | --version
usage: %s [building-type]
    --help              display this help message
    --version           show version information and exit.
    building-type       optionally restrict output to only this type.
    """ % ( gCommand, gCommand ) )
    return(exit_code)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"] # [ "netdisco", "intermapper"]
    result['dbUserClass'] = "look"  # user

    # this command wants add, update, delete, and keys actions?
    result['setArgs'] = False
    # Default action.
    result['action'] = "list"

    # Command line options:
    # Cannot use a/add, u/update, d/delete, and k/keys if setArgs is true.
    # If you don't want a coresponding short option then use ""
    result['optionList'] = [ ]

    # What are the required positional arguments?
    result['requiredArgs'] = []
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmd_line_info - all command line flags and options processed thus far.
#    arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmd_line_info, arg_list):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    if ( len(arg_list) > 0 ):
        result['bldg_type'] = arg_list[0]
        result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    try:
        building_types.initialize()
    except db_access.DbException as db_error:
        print("Unable to initialize database table. Contact your DBA.")
        sys.exit(1)

    if ( 'bldg_type' in cmd_line_info ):
        bldg_type_list = building_types.get_building_types(
                cmd_line_info['bldg_type'])
    else:
        bldg_type_list = building_types.get_building_types()

    cmd_output.print_building_types(bldg_type_list)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 showmatch et :

