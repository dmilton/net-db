# Version information for this command.

g_version='09 Jan 2015 - 3.0.9'

# Split a network into a number of smaller subnets.

#  9 Jan 2014   Was not setting ep_code when type is link.
#  9 Dec 2014   Add --in-place instead of --force for allocated networks
#               and then maintain the original name for the first network
#               and auto-increment the vlan tag for subsequent ones.
#  1 Jun 2014   Provide consistiency of arguments by changing --type,
#               --zone, and --vlan to --net-type, --net-zone, and --vlan-tag
#               respectively. This matches the database more closely and
#               the arguments used for most of the set-xxx commands.
#  2 May 2014   Missed conversion of GetZoneById to get_zone_by_id so
#               splits which set the zone generate an error.
# 20 Mar 2014   Correct ipaddress library changes so both old google code
#               and new python 3.3 code will work.
# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
#  4 Nov 2013   Fix Usage, prefixes are 8 through 32.
# 12 Jul 2013   Found problem with assignment of vlan tag when the
#               type info has a negative value.
#  9 May 2013   Update for library name changes. Use cidr value for
#               the network to split rather than a vlan name.
#  8 Apr 2013   add_network returns a result so we need to handle it here.
#               delete_network has no return a result so don't look for one.
# 28 Feb 2013   network types schema changed, no longer use id/typeid.
# 14 Feb 2013   Rewrite code to use new database access routines and new
#               table schema. 
# 23 Feb 2012   Add transaction to bracket operations and provide
#               some error recovery. Convert from old monolithic
#               code to the modular command line processing.
#               Altered so the vlan_tag is preserved from the original
#               to all of the newly created networks.
# 21 Feb 2012   Changed to modular code and updated so the vlan
#               column is vlan_tag.
# 12 Nov 2009   Changed import path to /opt2/local/etc.
# 10 Sep 2009   Use new add network routine and do a check for
#               the failure of an add.
#               Changed the CIDR values for ipv4 to fit our class
#               B networks (min 16) and allow for host based (32).
# 18 Aug 2009   Script failing because -O flag missing.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to handle exceptions.
# 29 Jun 2009   Add code to handle development database server.
# 22 Dec 2008   Fixed bug introduced by GetNextNetwork change.
#               Add code to set the network type.
# 15 Sep 2008   initial translation from bash.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries
try:
    import building_info
    import network_info
    import network_types
    import network_zones
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--net-type=x] [--vlan-tag=a] [--net-zone=z] network cidr
usage: %s --in-place network cidr
    --help          display this help message
    --version       show version information and exit.
    --in-place      new split networks are assigned to current building.
    --net-type=x    set the type of network when creating new subnets.
    --vlan-tag=a    set the vlan tag of network when creating new subnets.
    --net-zone=z    set the zone of network when creating new subnets.
    network         the network (CIDR format) to split.
    cidr            new CIDR value for split networks (8 - 32 inclusive).
    """ % ( gCommand, gCommand, gCommand ) )
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = "split"

    result['optionList'] = [ ("t:", "net-type="),
            ("v:", "vlan-tag="), ("z:", "net-zone="), ("i", "in-place") ]

    result['requiredArgs'] = ["network", "prefixlen"]
    result['minArgs'] = 2

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0
    # We have two positional arguments.
    if ( len(argList) > 1 ):
        result['network'] = ipaddress.ip_network(argList[0])
        result['prefixlen'] = int(argList[1])
        result['argsUsed'] = 2

    # If there is a problem with an argument:
    # raise CommandError, ("kind of error")

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise CommandError, ("invalid option combination")

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize the network tables here.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()
    network_zones.initialize()

    # Drop into debug when requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Verify we have a valid CIDR value.
    newPrefixLen = cmdLineInfo['prefixlen']
    if ( newPrefixLen < 8 or newPrefixLen > 32 ):
        message = "Invalid new CIDR %d" % ( newPrefixLen )
        message += "The new CIDR must be between 8 and 32."
        raise CommandError(message)

    # Verify that the command arguments all work together.
    if ( cmdLineInfo['--in-place'] and 
        ( cmdLineInfo['--net-type'] != None or
          cmdLineInfo['--vlan-tag'] != None or
          cmdLineInfo['--net-zone'] != None ) ):
        raise CommandError("cannot combine --in-place with any other option")

    # Get the network we need to split.
    netList = network_info.get_networks_within(cmdLineInfo['network'])

    if ( len(netList) == 0 ):
        raise CommandError("Could not find network " \
                + str(cmdLineInfo['network']) + " in the database.")
    elif ( len(netList) > 1 ):
        raise CommandError("The network range " + str(cmdLineInfo['network'])
                + " contains more than one network.")

    netToSplit = netList[0]

    if ( "net_zone" not in netToSplit ):
        # network is currently assigned. Take the zone from the building.
        bldgInfo = building_info.get_building_info(netToSplit['bldg_code'])
        netToSplit['net_zone'] = bldgInfo['net_zone']
        if ( not cmdLineInfo['--in-place'] ):
            raise CommandError("Network assigned to a building.")
            print("****    This network is allocated to a building!    ****")
            print("If you wish to split an assigned building either use the")
            print("--in-place option or free the network first.")

    if ( cmdLineInfo['--net-zone'] != None ):
        # This verifies the user supplied zone id is valid.
        zoneInfo = network_zones.find_net_zone(netToSplit['network'])
        # Set the zone in the network to split.
        netToSplit['net_zone'] = cmdLineInfo['--net-zone']

    # Is the existing CIDR already smaller than the one requested?
    if ( netToSplit['network'].prefixlen >= newPrefixLen ):
        message = "The network you are attempting to split is:"
        message += "   %s" % ( str(netToSplit['network']) ) + "\n"
        if ( newPrefixLen <= 32 ):
            message += "This network already has a mask smaller than or "
            message += "equal to the one specified." + "\n"
            message += "The new CIDR must be > " + \
                str(netToSplit['network'].prefixlen) + " and <= 32.\n"
        else:
            message += "The network cannot be split any further.\n"
        ## end else if ( newPrefixLen <= 32 ):
        raise CommandError(message)

    # Get the type information
    if ( cmdLineInfo['--net-type'] != None ):
        # New type/type changing.
        typeInfo = network_types.get_type_info(cmdLineInfo['--net-type'])[0]
    else:
        # Existing type, split networks will be the same.
        typeInfo = network_types.get_type_info(netToSplit['net_type'])[0]

    # Everything checks out so far, display what we found.
    print("Located network:")
    print( "  network " + str(netToSplit['network']) 
            + ", network zone " + str(netToSplit['net_zone']))
    typeLine = "  type " + netToSplit['net_type']
    if ( cmdLineInfo['--net-type'] != None ):
        typeLine += " (changing to " + typeInfo['net_type'] + ")"
    print(typeLine)
    vlanLine = "  building code " + netToSplit['bldg_code'] + \
        ", name " + netToSplit['net_name'] + \
        ", vlan " + str(netToSplit['vlan_tag'])
    if ( cmdLineInfo['--vlan-tag'] != None ):
        vlanLine += " (changing to " + cmdLineInfo['--vlan-tag'] + ")"
    print(vlanLine)
    if ( cmdLineInfo['--in-place'] ):
        print("The updated networks will remain assigned to the building:")
        print("  " + bldgInfo['bldg_name'] + " (" + bldgInfo['bldg_code'] + ")")

    # Determine the new list of networks.
    # How many bits are we using for subnets?

    # now generate the subnets.
    newNetList = list(netToSplit['network'].subnets(new_prefix=newPrefixLen))

    # Create a template network. This will be used for each entry in
    # newNetList to keep everything in sync. The netToSplit values are
    # already set correctly so start with them.
    newNetwork = network_info.new_network(typeInfo)

    # Now set things as requested or needed for a free network.
    if ( cmdLineInfo['--in-place'] ):
        newNetwork['bldg_code'] = netToSplit['bldg_code']
        newNetwork['assign_by'] = netToSplit['assign_by']
    else:
        newNetwork['bldg_code'] = '00'      # invalid/unassigned building code.
        newNetwork['prime_net'] = False      # never a prime network when free.
        newNetwork['assign_by'] = ""        # nobody has assigned it.
        newNetwork['net_zone'] = netToSplit['net_zone']
        # Set unassigned ep_code and remote code. This avoids errors in
        # add network when the type is link or extern.
        if ( "ep_code" in newNetwork ):
            newNetwork['ep_code'] = "00"
        if ( "remote_code" in newNetwork ):
            newNetwork['remote_code'] = "00"

    # set the vlan tag as appropriate.
    if ( cmdLineInfo['--vlan-tag'] != None ):
        # Explicitly specified vlan tag
        vlan_tag = cmdLineInfo['--vlan-tag']
    elif ( cmdLineInfo['--in-place'] ):
        # the split is in place, start with the current vlan tag.
        vlan_tag = netToSplit['vlan_tag']
    else:
        # Use the default vlan tag based on the type.
        vlan_tag = abs(typeInfo['base_vlan_tag'])

    # Which table should the split networks be inserted into?
    if ( cmdLineInfo['--in-place'] ):
        # Use the same table as the current network.
        addTable = netToSplit['network_table']
    else:
        addTable = network_types.get_type_tables(typeInfo)[1]
        if ( addTable == None ):
            raise CommandError("Networks of type " + typeInfo['net_type'] +
                        " are not added to the free table.")

    # Add the new networks into the database.
    print("Creating new networks:")
    for aNet in newNetList:
        # This changeds for each network
        newNetwork['network'] = aNet
        # Don't change the name of the first network if --in-place
        if ( cmdLineInfo['--in-place'] and vlan_tag == netToSplit['vlan_tag'] ):
            newNetwork['net_name'] = netToSplit['net_name']
            maskUpdateOnly = True
        else:
            newNetwork['net_name'] = network_info.name_network(
                newNetwork, typeInfo, bogon=True)
            maskUpdateOnly = False
        # Set the vlan tag to use.
        newNetwork['vlan_tag'] = vlan_tag

        # Okay, all constructed, add/update the database.
        try:
            if ( maskUpdateOnly ):
                addNet = network_info.set_prefix_len(netToSplit, newPrefixLen)
            else:
                addNet = network_info.add_network(
                        newNetwork, typeInfo, True, addTable)
        except:
            print("An attempt to add the network has failed.")
            raise

        # Show what was done.
        if ( "net_zone" in addNet ):
            print( "  network " + str(addNet['network']) 
                + ", network zone " + str(addNet['net_zone']) )
        else:
            print( "  network " + str(addNet['network']) )
        print("  building code " + addNet['bldg_code'] + ", name " + \
            addNet['net_name'] + ", vlan " + str(addNet['vlan_tag']) + "\n" )

        if ( cmdLineInfo['--in-place'] ):
            # Increment the vlan tag for in-place so we don't get duplicates.
            vlan_tag += 1

    # Now delete the original network we split into parts.
    if ( cmdLineInfo['--in-place'] ):
        # List the updated networks for the building.
        network_info.list_networks(netToSplit['bldg_code'])
    else:
        # Remove the original network now that new ones added.
        # This does not need to be done when in-place is set because
        # the first network is updated rather than 'duplicated'.
        print("Split complete, removing original network.")
        network_info.delete_network(netToSplit, typeInfo['net_family'])


## end main()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
