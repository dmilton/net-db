# Version information for this command.

g_version='25 Sep 2014 - 3.0.2'

# 13 May 2014   Qualify the NetException handling.
# 24 Mar 2014   Remove call to check_name since it was preventing
#               the 2nd type from being used.
# 25 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version | --keys]
usage: %s net-type [key1 value1]...
usage: %s --update net-type key key-value [key2 value2]...
usage: %s --delete net-type
usage: %s --keys
    --help          display this help message
    --version       show version information and exit.
    --keys          list input keys for updating all fields in the table.
    --update        update keys associated with net-type x.
    --delete        delete the type from the database. This will
                    fail if there are networks of this type defined.
    net-type        the network type to be added/updated/deleted.
    """ % ( gCommand, gCommand, gCommand, gCommand, gCommand )))
    sys.exit(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # Assume the default is to display/list information.
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"  # read/update access.

    # This is a set command
    result['setArgs'] = True        # Implement add, delete, keys, update.
    result['action'] = "add"        # default action

    result['optionList'] = []

    # Add requires the new network type, all others can default.
    result['addRequired'] = ["net_type"]
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires the network type and a key/key-value pair to update.
    result['updateRequired'] = ["net_type"]
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete requires the network type.
    result['delRequired'] = ["net_type"]
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # initialize our type table so we know what input fields are valid.
    network_types.initialize()

    # Get the list of valid input keys
    result['inputKeys'] = network_types.get_net_type_fields()

    if ( cmdInfo['action'] in ["add", "update", "delete"] ):
        result['net_type'] = argList[0]
        result['argsUsed'] = 1
    else:
        result['argsUsed'] = 0

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    try:
        dbTypeInfo = network_types.get_type_info(cmdLineInfo['net_type'])
        if ( cmdLineInfo['action'] == "add" ):
            print("Unable to add type " + cmdLineInfo['net_type'])
            print("Type already exists.")
            sys.exit(1)
    except NetException as err:
        if ( err.name == "NetTypeNotFound" ):
            if ( cmdLineInfo['action'] in ["update", "delete"] ):
                print("Unable to " + cmdLineInfo['action'] + 
                        " type.")
                print(err.detail)
                sys.exit(2)
    except:
        raise

    if ( cmdLineInfo['action'] in ["add", "update"] ):

        # create a record from cmdLineInfo to use for add/update.
        theType = network_types.new_net_type()
        for aKey in list(theType.keys()):
            aKey2 = aKey.translate(str.maketrans('_','-'))
            if ( aKey in cmdLineInfo ):
                theType[aKey] = cmdLineInfo[aKey]
            elif ( aKey2 in cmdLineInfo ):
                theType[aKey] = cmdLineInfo[aKey2]
            else:
                del theType[aKey]

        if ( cmdLineInfo['action'] == 'add' ):
            dbRecord = network_types.add_net_type(theType)
            print("The type has been added:")
        else:
            dbRecord = network_types.update_net_type(theType)
            print("The type has been updated:")
        # print the license we have.
        cmd_output.print_types(dbRecord, True)

    elif ( cmdLineInfo['action'] == 'delete' ):

        toDelete = {}
        toDelete['net_type'] = cmdLineInfo['net_type']
        network_types.delete_net_type(toDelete)
        print("The type has been deleted.")

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
