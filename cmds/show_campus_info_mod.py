# Version information for this command.

g_version='25 Sep 2014 - 3.0.2'

# 13 May 2014   Cleanup usage display, expand examples.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 14 May 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:

	import building_info

except ImportError as msg:

    print("The NetTrackDB utilities are not properly installed.")
    print(msg)
    sys.exit(-1)

except Exception as exc:

    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [campus-id]
usage: %s --buildings campus-id
    --help          display this help message
    --version       show version information and exit.
    --buildings     a brief listing of buildings on the specified campus.
    campus-id       the campus id to provide information on.
                    if no campus-id is provided, list all campus locations.
    
    """ % ( gCommand, gCommand, gCommand ))
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"

    result['setArgs'] = False
    result['action'] = 'list'

    result['optionList'] = [ ("b", "buildings") ]
    result['requiredArgs'] = []
    result['minArgs'] = 0

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # There aren't any mandatory arguments so if we have one it is
    # the campus id.
    if ( len(argList) == 1 ):
        result['campus_id'] = int(argList[0])
        result['argsUsed'] = 1

    # If there is a problem with an argument:
    # raise ValueError, ("kind of value error")

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    if ( ('campus_id' in theOptions) == False ):
        if ( theOptions['--buildings'] == True ):
            raise ValueError("--buildings requires a campus-id arg.")

    return()
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize the building and campus tables.
    building_info.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # List all campus locations?
    if ( 'campus_id' in cmdLineInfo ):
        campusList = \
                building_info.get_campus_info(cmdLineInfo['campus_id'], False)
    else:
        campusList = building_info.get_campus_info(0, True)

    if ( len(campusList) == 0 ):
        raise CommandError("There is no campus defined for that id.")

    if ( cmdLineInfo['--buildings'] == False ):
        cmd_output.print_campus(campusList)
    else:
        # produce a list of all buildings for the campus.
        buildingList = building_info.get_campus_buildings(cmdLineInfo['campus_id'])
        # Sort the list by building code.
        buildingList = sorted( buildingList, key=lambda
                elem: "%s" % (elem['bldg_code'].lower(),) )
        # Now print them out.
        title = "Buildings for the campus location: " + \
                        campusList[0]['campus_location']
        keys = ['bldg_code', 'bldg_name', 'net_zone']
        header = ("Code", "Name", "Net Zone")
        cmd_output.print_table(title, header, keys, buildingList)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
