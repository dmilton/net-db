#
# Version information for this command.
#

g_version='01 Oct 2014 - 1.0.1'

# 15 Aug 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_types
    import network_zones
    import network_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print( """
usage: %s --help | --version
usage: %s [--info] bldg-code core-links...
    --help          display this help message
    --version       show version information and exit.
    --info          display assignments without performing them.
    --endpoint      This router is where buildings terminate.
    bldg-code       building code for new core router.
    core-links...   core building codes to create links to.
    """ % (gCommand, gCommand) )
    sys.exit(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # For a set-xyz command there are four main actions available:
    # keys, add, update, and delete. The default action is 'add'.
    # For any other type of command the default action is 'show'
    # and the command line flags and positional arguments list is empty.

    # Optional argument to change a positional argument type.
    # This allows the primary key value to be changed by command line flags.
    # This is also how additonal flags are assigned with non set-xyz commands.
    # Cannot use k, u, or d for set commands as these map to
    # keys, update, and delete respectively.
    result['longOptions'] = ['info','endpoint']
    result['shortOptions'] = 'ie'

    # Show/Not table set-xyz commands:
    result['minArgs'] = 2

    # table set-xyz commands:
    # The add requires two arguments or the entry is pretty much useless.
    # list of table columns that are mandatory.
    #result['addRequired'] = ['bldg_code', 'room']
    #result['addMinArgs'] = len(result['addRequired'])
    # Update requires the table column that is the primary key and
    # one key/value pair to update.
    #result['updateRequired'] = ['room_id']
    #result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete uses the room_id field.
    #result['delRequired'] = ['room_id']
    #result['delMinArgs'] = len(result['delRequired'])

    # Assume all modifiers are inactive. Note the above should not have
    # the double hyhpen while the value below requires it. If the flag
    # above is on the command line the value below will be inverted.
    for aMod in result['longOptions']:
        # If the option has '=' in it, the value is not boolean.
        cmdLineFlag = '--' + aMod
        if ( cmdLineFlag[-1] == '=' ):
            cmdLineFlag = cmdLineFlag[:-1]
            result[cmdLineFlag] = None
        else:
            result[cmdLineFlag] = False

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdInfo, argList):

    result = {}

    # count arguments, depending upon task, arguments are different.
    argCount = len(argList)

    # For a set command:
    if ( cmdInfo['action'] in ['update', 'delete'] ):
        # Delete requires one argument - the room_id.
        result['room_id'] = int(argList[0])
        result['argsUsed'] = 1
    elif ( cmdInfo['action'] == 'add' ):
        # Add requires bldg_code and room
        result['bldg_code'] = argList[0]
        result['room'] = argList[1]
        result['argsUsed'] = 2
    else:
        # Grab the core building code
        result['core_bldg_code'] = argList[0]
        result['argsUsed'] = 1
        # and the list of links to create.
        result['core_links'] = []
        for argNbr in list(range(1,len(argList))):
            result['core_links'].append(argList[argNbr])
            result['argsUsed'] += 1

    return(result)
## end process_positional()

#^L
##############################################################################
# ProcessOption
# This routine must process the command specific options one at a time
# and return any dictionary values which Main will use for processing.

def process_option(option, argument):

    result = {}

    # This is only required to handle options which have
    # arguments: ie --name='name-to-set'
    if option in ("-i", "--info"):
        result['--info'] = True
    elif option in ("-e", "--endpoint"):
        result['--endpoint'] = True
    else:
        raise ValueError("unhandled option:" + str(option))
    ## end if

    return(result)
## end process_option()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError("invalid option combination description")

    return
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Now handle what the user gave us on the command line.
    # Verify we have valid information:
    # Must have valid building codes _and_ the link codes must be
    # valid endpoints.
    zones = []
    try:
        newBldgInfo = building_info.get_building_info( 
                                cmdLineInfo['core_bldg_code'] )
    except NetException as err:
        if ( err.name == 'UnknownBuildingCode' ):
            print("Fatal Error: " + err.detail)
            return
    except:
        # Something unexpected happened, raise it for debugging.
        raise
    zones.append(newBldgInfo['net_zone'])

    epPriority = {}
    # Now verify the links...
    for aCode in cmdLineInfo['core_links']:
        # First, make sure the building code is valid.
        try:
            bldgInfo = building_info.get_building_info(aCode)
        except NetException as err:
            if ( err.name == 'UnknownBuildingCode' ):
                print("Fatal Error: " + err.detail)
                return
        except:
            # Something unexpected happened, raise it for debugging.
            raise

        # Now verify this is an endpoint.
        try:
            epInfo = network_info.get_endpoint_info(aCode)
        except NetException as err:
            if ( err.name == 'EpNotFound' ):
                print("Fatal Error: " + err.detail)
                return
        except:
            # Something unexpected happened, raise it for debugging.
            raise
        epPriority[aCode] = epInfo['core_priority']

    # If we get this far, the core building is valid as are the
    # links that are requested.
    # Get the networks already assigned to the building.
    try:
        netList = network_info.get_networks( cmdLineInfo['core_bldg_code'] )
    except NetException as err:
        if ( err.name == 'NoNetworksDefined' ):
            # This is expected and valid.
            pass
        else:
            raise
    except:
        raise

    # A core router requires a loopback addresses and links
    # to the specified other core routers.

    # Get the zone address range information for the new router
    zoneInfo = network_zones.get_range_info(newBldgInfo['net_zone'])

    # Check the networks we already have:
    loopNets = []
    linkNets = []
    haveLinksTo = []
    otherNets = []
    isoNet = {}
    for aNet in netList:
        if ( aNet['net_type'] == 'loop' ):
            loopNets.append(aNet)
        elif ( aNet['net_type'] == 'link' ):
            linkNets.append(aNet)
            haveLinksTo.append(aNet['bldg_code'])
            haveLinksTo.append(aNet['ep_code'])
        elif ( aNet['net_type'] == 'iso' ):
            isoNet = aNet
        else:
            otherNets.append(aNet)

    # Set flag whether updates to zones should be made or not.
    noUpdate = cmdLineInfo['action'] == 'show'
    noUpdate = True

    # Now get networks if necessary:
    loopTypeInfo = network_types.get_type_info('loop')
    if ( len(loopNets) == 0 ):
        aLoop4 = network_info.new_network(loopTypeInfo)
        loopZone4 = network_zones.get_range_info(
                newBldgInfo['net_zone'], 'loop', 4)
        aLoop4['network'] = network_zones.get_next_loop(loopZone4, 1, noUpdate)
        aLoop6 = network_info.new_network(loopTypeInfo)
        loopZone6 = network_zones.get_range_info(
                newBldgInfo['net_zone'], 'loop', 6)
        aLoop6['network'] = network_zones.get_next_loop(loopZone6, 1, noUpdate)
        loopNets = [aLoop4, aLoop6]
    elif ( len(loopNets) == 1 ):
        # already have a loopback assigned, only assign v6 if
        # it doesn't exist already.
        if ( loopNets[0]['network'].version == 4 ):
            aLoop6 = network_info.new_network(loopTypeInfo)
            loopZone6 = network_zones.get_range_info(
                        newBldgInfo['net_zone'], 'loop', 6)
            aLoop6['network'] = network_zones.get_next_loop(
                        loopZone6, 1, noUpdate)
        loopNets.append(aLoop6)

    # Now that we have loopback addresses, determine the ISO address.
    isoTypeInfo = network_types.get_type_info('iso')
    if ( isoNet == {} ):
        isoNet = network_info.calc_iso_network(loopNets, isoTypeInfo)

    # Get the links...
    linkTypeInfo = network_types.get_type_info('link')
    linkZone4 = network_zones.get_range_info(
                        newBldgInfo['net_zone'], 'link', 4)
    linkZone6 = network_zones.get_range_info(
                        newBldgInfo['net_zone'], 'link', 6)
    for anEp in cmdLineInfo['core_links']:
        if ( anEp in haveLinksTo ):
            print("A link to " + anEp + " already exists.")
        else:
            aLink = network_info.new_network(linkTypeInfo)
            aLink['network'] = network_zones.get_next_link(
                        linkZone4, 1, noUpdate)
            linkNets.append(aLink)
            aLink = network_info.new_network(linkTypeInfo)
            aLink['network'] = network_zones.get_next_link(
                        linkZone6, 1, noUpdate)
            linkNets.append(aLink)

    allNets = loopNets + [isoNet] + linkNets + otherNets
    tblTitle = "Networks for " + newBldgInfo['bldg_name']
    tblHead = ("Name", "VLAN", "Network", "Type")
    tblKeyList = ['network_name', 'vlan_tag', 'network', 'net_type']
    cmd_output.print_table(tblTitle, tblHead, tblKeyList, allNets)

    if ( cmdLineInfo['action'] == 'show' ):
        # 
        print("showing info")
    elif ( cmdLineInfo['action'] == 'add' ):
        #
        print("adding info")

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

