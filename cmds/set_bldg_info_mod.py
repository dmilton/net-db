#
# Version information for this command.

g_version='01 Oct 2014 - 3.0.4'

# This command allows updating of a building information and can
# add a new building to the networking database.

# 20 Aug 2014   More column name changes and update usage to reflect the
#               change from 'name' to 'bldg-name'.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 13 May 2014   Qualify the NetException handling.
# 26 Mar 2014   Clean up usage formatting.
#  4 Nov 2013   Rewrite using new template where add is default.
#  3 Jun 2013   Improve command line error messages.
#  8 Apr 2013   add_building and update_building returns a result
#               containing the newly updated/inserted table row.
# 14 Mar 2013   Start update for new db routines.
# 16 Aug 2010   Add VerifyOptions function.
# 13 May 2010   Added another 'code' for extended ed downtown.
#               Corrected problems with setting ss_day,week,
#               month, and usage values in the database.
#  8 Apr 2010   Converted to new modular command structure.
#               Added new keys for statseeker data extraction.
# 12 Nov 2009   Changed import path to /opt2/local/etc.
# 13 Aug 2009   Add new field pp_code.
# 24 Jul 2009   Added new field map_code.
#  8 Jul 2009   Write code to delete a building code.
#  6 Jul 2009   Added OSPF area
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to handle exceptions.
# 29 Jun 2009   add code to handle development database server.
# 26 Sep 2008   initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
	import building_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version | --keys]
usage: %s bldg-code bldg-name 'Building Name' [key key-value]...
usage: %s --update bldg-code key key-value [key key-value]...
usage: %s --delete bldg-code
    --help          display this help message
    --version       show version information and exit.
    --keys          list valid input keys.
    --update        Update an existing building code.
    --delete        Delete the building code from the table.
                    This looks for any resources allocated to the building
                    and releases or removes them.
    bldg-code       the two letter building code to be used.
    key value       key/value pairs which can be used on the command line.
                    Use the --keys option to display the complete set.

    With the exception of delete, at least one (key value) pair is required.
    When adding a new building, the (bldg-name 'Building Name')
    pair is mandatory.
    """ % ( gCommand, gCommand, gCommand, gCommand )))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = True
    result['action'] = "add"

    result['optionList'] = []

    # Add requires three arguments - bldg-code name bldg-name
    result['addMinArgs'] = 3
    result['addRequired'] = ["bldg_code", "bldg_name"]
    # Update requires three arguments - bldg-code and one key/value pair.
    result['updateMinArgs'] = 3
    result['updateRequired'] = ["bldg_code"]
    # Delete only requires the building code.
    result['delMinArgs'] = 1
    result['delRequired'] = ["bldg_code"]

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#            the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # initialize our building tables so we have the valid input fields.
    building_info.initialize()

    # Get the list of valid input keys
    result['inputKeys'] = building_info.get_building_fields()

    # count arguments in addition to our required building code.
    argCount = len(argList)

    # If the keys flag is requested then we are mostly done.
    if ( cmdInfo['action'] in ["add","update","delete"] ):
        # our only positional argument is the building code.
        result['bldg_code'] = argList[0]
        result['argsUsed'] = 1
    else:
        result['argsUsed'] = 0

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # turn our cmdLineInfo into a buildingInfo dictionary
    # and an action to perform.

    if ( cmdLineInfo['action'] in ["add", "update"] ):
        # create a new record from cmdLineInfo to use for add/update.
        buildingInfo = building_info.new_building()
        for aKey in list(buildingInfo.keys()):
            aKey2 = aKey.translate(str.maketrans('_','-'))
            if ( aKey in cmdLineInfo ):
                buildingInfo[aKey] = cmdLineInfo[aKey]
            elif ( aKey2 in cmdLineInfo ):
                buildingInfo[aKey] = cmdLineInfo[aKey2]
            else:
                del buildingInfo[aKey]
        ## end for aKey in buildingInfo.keys():

        try:
            if ( cmdLineInfo['action'] == "add" ):
                bldgInfo = building_info.add_building(buildingInfo)
                dumpFlag = False
                print("The building has been added:")
            else:
                bldgInfo = building_info.update_building(buildingInfo)
                dumpFlag = True
            # print the building information we have.
            cmd_output.print_building(bldgInfo, "", dumpFlag)
        except NetException as err:
            if ( err.name == "duplicate" ):
                print("This building code already exists.")
                print((err.detail))
            elif ( err.name == "not found" ):
                print("Unable to find this building code.")
                print((err.detail))
            else:
                raise
        except:
            raise
    elif ( cmdLineInfo['action'] == "delete" ):
        try:
            bldgInfo = building_info.get_building_info(cmdLineInfo['bldg_code'])
        except NetException as err:
            if ( err.name == "not found" ):
                print("Unable to find this building code.")
                print((err.detail))
                sys.exit(1)
            else:
                raise
        except:
            raise
        ## end try:
        cmd_output.print_building(bldgInfo, "", dumpFlag=True)
        building_info.delete_building(bldgInfo)
        print("Building delete complete.")
        print("Voice/Data rooms, and wireless installations have been deleted.")
        print("Any allocated networks have been removed and/or returned to")
        print("the free pool.")

## end main()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

