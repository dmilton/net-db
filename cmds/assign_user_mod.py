# Version information for this command.

g_version='06 Jan 2015 - 3.0.7'

# Assign a free network to a specific building.

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 13 May 2014   Qualify the NetException handling.
# 26 Mar 2014   Cleanup formatting of the usage output.
# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
#  5 Feb 2014   Add diagnostic information about assignment of IPv6 user
#               networks. Change network name computation to use the
#               building name as a starting place and then run it through
#               the networks library utility to refine it.
# 22 Jan 2014   Set default name, vlan tag, and prime_net when the requested
#               network will be the first/only network currently asssigned
#               to the building.
#  7 Nov 2013   Add a listing of networks assigned to the building after
#               the assignment is complete.
# 19 Sep 2013   Remove 'None' values from the vlan being assigned.
# 13 Sep 2013   Vlan tag command line option not working, setting
#               wrong dictionary item in commandOptions.
# 10 Jun 2013   Allow assignment of link networks.
# 14 May 2013   Fix assign_date so the default of now() is used.
# 14 May 2013   Update to allow assignment of loopback networks.
# 18 Apr 2013   Rewrite for new library module networks.
#  7 Dec 2010   The name checking code was passing a duplicate name.
#               Must assign the network to the building before
#               we can mark it as the prime network. Otherwise we
#               don't know what other networks to clear the flag for.
# 16 Nov 2010   Found bug in SetVlanNumber where sometimes the
#               value in newNumber is a string, sometimes an integer.
#               When it was an integer the routine crashed when
#               building the query. Added type conversion to fix.
# 16 Aug 2010   Add VerifyOptions function.
# 25 May 2010   New field in network_detail - ospf_area. Update
#               the code to allow setting this value on assignment.
#               If the area is not specified, the default is to
#               use the area of the building.
# 20 May 2010   When the network being assigned is a link, the
#               record in network_allocations which is updated
#               is not necessarily the correct one. Need to
#               ensure the record is the 'building' end that
#               gets altered.
#  4 Mar 2010   Changed error from NoName to DuplicateName and
#               raised the exception so it is caught and handled
#               properly rather than causing a failure.
#  8 Feb 2010   Removed a stray quote character from the assign
#               networks query statement.
# 26 Jan 2010   Added code to set assign_date and assign_by fields
#               in the network_allocations table. This is a move
#               from the network_detail table since a 'network' can
#               be allocated to multiple buildings we need to track
#               when it was allocated. This allows a vlan which is
#               not routed to have a date since the 0.0.0.0/0 network
#               is allocated in potentially hundreds of locations.
# 30 Nov 2009   Add --vlan=N flag to allow changing the vlan number.
#               Add --prime=T/F to set the prime_net flag.
#               Add --type=X to set the type of network.
#               Change --rename to --name.
#               Complete rewrite of basic command structure to fit
#               modular approach.
# 12 Nov 2009   Changed import path to /opt2/local/etc.
#  8 Oct 2009   Allow periods in a vlan name.
# 18 Aug 2009   Script failing because -O flag missing.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to handle exceptions.
# 29 Jun 2009   add code to handle development database server.
# 19 Jun 2009   remove the code which prevented names with upper
#               case values since case senitive    names are allowed.
# 27 Jan 2009   added code to update the assign_by and assign_date
#               fields in the database.
# 17 Sep 2008   initial writing.

#
############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import network_types
    import building_info
    import network_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#\
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [options] network bldg-code
    --help          display this help message
    --version       show version information and exit.
    --name=y        rename the network to y (cannot contain spaces).
    --prime         Make this the prime network. This will reset the prime
                    flag for all other networks in the building.
                    This will force the type to 'user'.
    --type=X,Y      Assign a network of type X (extern|rez|loop|link) using
                    remote code Y. Cannot be used with --prime.
    --vlan=N        Set the vlan ID number to N, default based on type.
    network         CIDR network address to assign.
    bldg-code       Building code to assign network to.
    """ % ( gCommand )))
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return the default options set for this command.

def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"  # read only. read/update access = "user"

    # Assume the default is to assign a network.
    result['setArgs'] = False
    result['action'] = "assign"

    # Command line options:
    result['optionList'] = [ ("n:", "name="), ("p", "prime"),
            ("t:", "type="), ("v:", "vlan=") ]

    # Must have the network and the building code.
    result['requiredArgs'] = ["network", "bldg_code"]
    result['minArgs'] = 2

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the list positional command line arguments.

def process_positional(cmdInfo, argList):

    result = {}

    # If we don't have the correct number of arguments, return failure.
    if ( len(argList) != 2 ):
        raise CommandError("Missing command argument(s)")

    # We have two positional arguments.
    result['network'] = ipaddress.ip_network(argList[0])
    result['bldg_code'] = argList[1]
    result['argsUsed'] = 2

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):
 
    # If the prime flag is set, the type must be user.
    if ( theOptions['--prime'] and theOptions['--type'] != None ):
        if ( theOptions['--type'] != 'user' ):
            raise CommandError("network flagged as prime but type not user.")

    if ( theOptions['--name'] != None ):
        cmd_line_input.check_name(theOptions['--name'])

    if ( theOptions['--type'] != None ):
        try:
            typeName, epCode = argument.split(',')
        except ValueError:
            raise CommandError("invalid argument to --type:" + 
                    str(theOptions['--type']))
        theOptions['net_type'] = typeName
        theOptions['ep_code'] = epCode
    else:
        theOptions['net_type'] = "user"

    if ( theOptions['--vlan'] != None ):
        theOptions['--vlan'] = int(theOptions['--vlan'])

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize our table structures.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Verify the building code.
    bldgInfo = building_info.get_building_info(cmdLineInfo['bldg_code'])

    # Verify the network exists.
    netList = networks.get_networks_within(cmdLineInfo['network'])
    netInfo = netList[0]

    # Verify the type is one we can deal with here.
    if (cmdLineInfo['net_type'] not in
            ["user", "rez", "extern", "loop", "link"]):
        raise ValueError("invalid network type for assign.")

    # Get the type information.
    typeInfoList = network_types.get_type_info(cmdLineInfo['net_type'])
    if ( len(typeInfoList) != 1 ):
        raise ValueError("Unknown network Type")
    else:
        typeInfo = typeInfoList[0]

    # Create an appropriate empty template.
    netToAssign = network_info.new_network(typeInfo)

    # Copy what we have from the database.
    for aKey in list(netToAssign.keys()):
        if ( aKey in netInfo ):
            netToAssign[aKey] = netInfo[aKey]

    # Create a temporary name, this may be updated below 
    netToAssign['net_name'] = network_info.name_network(netToAssign,
            typeInfo, basename=bldgInfo['bldg_code'])

    # If this is the only network in the building, mark it as prime.
    try:
        assignedNets = network_info.get_networks(bldgInfo['bldg_code'])
    except NetException as netErr:
        if ( netErr.name == "NoNetworksDefined" ):
            # This is okay, the network must be type user, IPv4, and prime.
            if ( cmdLineInfo['net_type'] != 'user' ):
                print("The first IPv4 network assigned to a building must be")
                print("a network for end users.")
                raise ValueError("invalid network type for end users.")
            if ( netToAssign['network'].version != 4 ):
                print("This command can only be used to assign IPv4 networks.")
                print("Pure IPv6 buildings can be created using assign-nets.")
                raise ValueError("Network not IPv4")
            # Default to making this network the prime network.
            netToAssign['prime_net'] = True
            # Start with the building name for network name.
            netToAssign['net_name'] = bldgInfo['bldg_name']
            # Fix the name to fit our standards.
            netToAssign['net_name'] = network_info.name_network(
                    netToAssign, typeInfo)
        else:
            # Something happened which likely needs fixing.
            raise

    # Set the default vlan tag for this network.
    if ( typeInfo['base_vlan_tag'] > 0 ):
        netToAssign['vlan_tag'] = \
            network_info.get_next_vlan(bldgInfo['bldg_code'],
                    typeInfo['base_vlan_tag'])
    else:
        netToAssign['vlan_tag'] = abs(typeInfo['base_vlan_tag'])

    # Update with what we got from the command line.
    if ( cmdLineInfo['--name'] != None ):
        netToAssign['net_name'] = cmdLineInfo['--name']
    if ( cmdLineInfo['--vlan'] != None ):
        netToAssign['vlan_tag'] = cmdLineInfo['--vlan']
    netToAssign['prime_net'] = cmdLineInfo['--prime']

    # For links and extern networks, deal with the ep_code/remote_code value.
    if ( typeInfo['net_type'] == 'link' ):
        netToAssign['ep_code'] = cmdLineInfo['ep_code']
    elif ( typeInfo['net_type'] == 'extern' ):
        netToAssign['remote_code'] = cmdLineInfo['remote_code']

    # Assign the network.
    theNet = network_info.assign_network(netToAssign, typeInfo, bldgInfo)

    print("The network has been assigned.")
    network_info.list_networks(cmdLineInfo['bldg_code'])

## end main()

##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

