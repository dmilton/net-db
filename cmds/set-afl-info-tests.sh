#!/bin/sh
echo "adding a record"
./set-afl-info --debug=-1 --dbserv=1 \
	auth-code-value rtu-serial-value afl-model-type

echo "adding a duplicate record"
./set-afl-info --debug=-1 --dbserv=1 \
	auth-code-value rtu-serial-value auth-code-model

echo "updating a record which does not exist"
./set-afl-info --debug=-1 --dbserv=1 \
	--update no-auth-code-value \
	router router-name \
	member-id 0

echo "updating all fields of a record"
./set-afl-info --debug=-1 --dbserv=1 \
	--update auth-code-value \
	rtu-serial new-rtu-serial-value \
	afl-model new-auth-code-model \
	router router-name \
	member-id 0 \
	switch-model model-value \
	active true \
	lan-mac 01:23:45:67:89:ab \
	replaced-by old-router \
	removal-date 2013-01-01 \
	removal-reason 'testing update' \
	rma-number rma-number-value \
	rma-switch rma-switch-value \
	license-key 'string to represent a license key, should we read a file?'

echo "deleting a record"
./set-afl-info --debug=-1 --dbserv=1 \
	--delete auth-code-value

echo "deleting a record that does not exist"
./set-afl-info --debug=-1 --dbserv=1 \
	--delete auth-code-value

