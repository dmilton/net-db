# Version information for this command.

g_version="31 Aug 2010 - 3.0.0"

# 13 May 2014   Qualify the NetException handling.
# 23 Sep 2013   Add code to conditionally debug after init done.
#  7 Jun 2013   start conversion for new sensor library and tab format.
#  1 Sep 2010   Change field list to remove no-maint and on-maint
#               and replace them with maint-state.
# 18 Aug 2010   Reinstate --building option and start creating
#               reports for output. Extend the first command line
#               positional argument to a sensor grouping.
# 16 Aug 2010   Remove --building option and change to --report
#               and add --sort options.
#  5 Aug 2010   Add --csid, --mac-addr, and --history flags.
# 29 Jul 2010   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries
try:
	import sensor_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# Global variables. Report types...
noGroupReports = ("name", "serial-nbr", "csid", \
        "campus", "campusFG", "campusBC")
groupReports = ('bldg-code', 'serial-key', 'po', 'active', 'maint-state')
allReports = noGroupReports + groupReports

############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] | [--version] 
usage: %s [[--csid] | [--mac-addr]] [[--history] | [--dump]] sensor-id
usage: %s --report=X [--sort=Y] [--building=Z] [--dump] [sensor-group]
    --help          display this help message
    --version       show version information and exit.
    --dump          list sensors information in CSV output.
    --history       list the history for the sensor where it was/is installed.
    --csid          the sensor-id is the csid number.
    --mac-addr      the sensor-id is a MAC address.
    --building=Z    restrict output to just this building.
    --report=X      generate report of sensors. The reports are building,
                    campusFg, campusBc, serial-key, serial-nbr, po, csid,
                    active, and maint-state.
    --sort=Y        sort report by Y where Y is the field to sort by.
    sensor-id       A value to identify the sensor. This can be a host name,
                    serial number, csid, or mac address.
    sensor-group    In report generation mode this can be campus,
                    serial-key, or maintenance state. Not all reports
                    make sense with a grouping; unexpected results or errors
                    may be produced with some groupings.
    """ % ( gCommand, gCommand, gCommand )))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# command_options()
# This routine returns a dictionary containing two lists.
# The lists are used to build the strings required for command line
# processing by the main wrapper.

def command_options():

    result = {}
    result['short'] = "b:cdhmr:s:"
    result['long'] = ["csid", "dump", "history", "mac-addr", \
            "building=", "report=", "sort="]
    result['minArgs'] = 0

    return(result)
## end command_options()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # Assume the default is to display/list information.
    result['action'] = 'list'
    result['lookup'] = 'host' # csid or mac
    result['csidLookup'] = False
    result['macLookup'] = False
    result['history'] = False
    result['dump'] = False

    return(result)
## end default_options()

#
##############################################################################
# ProcessOption
# This routine must process the command specific options one at a time
# and return any dictionary values which Main will use for processing.

def process_option(option, argument):

    result = {}

    if option in ("-b", "--building"):
        result['bldg-code'] = argument
    elif option in ("-c", "--csid"):
        result['lookup'] = 'csid'
    elif option in ("-d", "--dump"):
        result['dump'] = True
    elif option in ("-h", "--history"):
        result['history'] = True
    elif option in ("-m", "--mac-addr"):
        result['lookup'] = 'mac'
    elif option in ("-r", "--report"):
        result['action'] = 'report'
        result['report'] = argument
    elif option in ("-s", "--sort"):
        result['sort'] = argument
    else:
        raise CmdException("unhandled option:" + str(option))
    ## end if

    return(result)
## end process_option()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    if ( theOptions['dump'] == True and theOptions['history'] == True ):
            raise CmdException(("dump cannot be used with history"))
    ## end if ( theOptions['dump'] == True and theOptions['history'] == True ):

    if ( theOptions['action'] == 'report' ):

        fatal = theOptions['lookup'] in [ 'mac', 'csid' ] or theOptions['history']
        if ( fatal ):
            raise CmdException("report can only be used with building=, sort=, and dump")
        ## end if ( fatal ):

        # Check for a known report type.
        global noGroupReports, groupReports, allReports
        if ( ( theOptions['report'] in allReports ) == False ):
            raise CmdException("unknown report type " + theOptions['report'])
        else:
            hasGroup = 'report-group' in theOptions
            needsGroup = theOptions['report'] in groupReports
            if ( needsGroup == True and hasGroup == False ):
                raise CmdException("The " + theOptions['report'] + \
                        " report requires a sensor-group argument.")
            elif ( needsGroup == False and hasGroup == True ):
                raise CmdException("The " + theOptions['report'] + \
                        " report does not require an argument.")
            ## end if ( needsGroup == XX and hasGroup == YY ):
        ## end else if ( ( theOptions['report'] in allReports ) == False ):

    ## end if ( theOptions['action'] == 'report' ):

## end verify_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    if ( cmdInfo['action'] == 'list' ):

        # We only have one positional argument. Try and do a host
        # lookup on the name. If it passes we have a host name,
        # otherwise we have a serial number or csid.
        result['sensor-ident'] = argList[0]

    elif ( cmdInfo['action'] == 'report' ):

        if ( len(argList) == 1 ):
            cmdInfo['report-group'] = argList[0]
        elif ( len(argList) > 1 ):
            raise CmdException("invalid command arguments for report: " + \
                    str(argList))
        ## end if ( len(argList) != 0 ):

    ## end else if ( cmdInfo['action'] == 'XXXX' )

    # If there is a problem with an argument:
    # raise CmdException, ("kind of error")

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(commandInfo):

    # Setup access to the sensor view and tables.
    sensor_info.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Now do the work.
    if ( commandInfo['action'] == 'list' ):

        if ( ('bldg-code' in commandInfo) == True ):
            print("Find all sensors for this building.")
        else:
            try:

                aSensor = {}
                if ( ('serial-nbr' in commandInfo) == True ):
                    aSensor = sensor_info.find_sensor(commandInfo['serial-nbr'])
                elif ( ('host-name' in commandInfo) == True ):
                    aSensor = sensor_info.find_sensorByName(commandInfo['host-name'])
                elif ( ('csid' in commandInfo) == True ):
                    aSensor = sensor_info.find_sensorByCsid(commandInfo['csid'])
                elif ( ('mac-addr' in commandInfo) == True ):
                    aSensor = sensor_info.find_sensorByMac(commandInfo['mac-addr'])
                ## end elif ( commandInfo.has_key('XXX') == True ):

            except NetException as info:
                if ( info.name == "UnknownSensorSerialNbr" ):
                    print("Could not find the requested serial number.")
                elif ( info.name == "UnknownSensorName" ):
                    print("Could not find the requested sensor name.")
                elif ( info.name == "UnknownSensorCSID" ):
                    print("Could not find the requested CSID.")
                elif ( info.name == "UnknownSensorMac" ):
                    print("Could not find the requested MAC address.")
                else:
                    raise
                ## end elif ( info.name == "XXX" ):
            except:
                raise
            ## end try

            if ( len(aSensor) > 0 ):
                if ( commandInfo['history'] == True ):
                    sensor_info.find_sensorHistory(aSensor)
                else:
                    sensor_info.print_sensor(aSensor)
                ## end else if ( commandInfo['history'] == True ):
            ## end if ( len(theSensor) > 0 ):

        ## end else if ( commandInfo.has_key('bldg-code') == True ):

    elif ( commandInfo['action'] == 'report' ):

        if ( ('bldg-code' in commandInfo) == True ):
            bldgCode = commandInfo['bldg-code']
        else:
            bldgCode = ''
        ## end else if ( commandInfo.has_key('bldg-code') == True ):

        global groupReports, noGroupReports
        if ( commandInfo['report'] in noGroupReports ):
            ReportByHost(bldgCode, commandInfo['report'], commandInfo['dump'])
        elif ( commandInfo['report'] in groupReports ):
            ReportByGroup(commandInfo['report'], commandInfo['report-group'],
                    commandInfo['dump'])
        else:
            print("That report has not been implemented yet.")
        ## end elif ( commandInfo['report'] == 'xxx' ):

    ## end elif ( commandInfo['action'] == 'xxx' ):

## end main()

#
##############################################################################
# Local function definitions...

##############################################################################
# find_sensorHistory(theSensor)

def find_sensorHistory(theSensor):

    sensorList = []
    # Start with the current sensor.
    sensorList.append(theSensor)
    nextSensor = theSensor

    # First, move forward in time to our currently installed sensor.
    while ( ('replaced-by' in nextSensor) == True ):
        nextSensor = find_sensor(nextSensor['replaced-by'])
        sensorList.append(nextSensor)
    ## end while ( nextSensor.has_key('replaced-by') == True ):
    sensorList.reverse()

    # Now move backward in time to the oldest sensor.
    nextSensor = theSensor
    findNext = True
    while ( findNext == True ):
        try:
            nextSensor = find_sensorByReplaced(nextSensor['serial-nbr'])
            sensorList.append(nextSensor)
        except:
            findNext = False
        ## end try
    ## end while ( findNext == True ):

    sensorFields = ['name', 'lan-mac', 'wlan-mac', 'serial-nbr',
        'serial-key', 'csid', 'po', 'model', 
        'active', 'maint-state']
    # We have the complete list. Print them out.
    for aSensor in sensorList:
        SensorReport(aSensor, sensorFields, False, False)
        print("")
    ## end for aSensor in sensorList:

    return()
## end find_sensorHistory()

##############################################################################
# SensorReport(theSensor, sensorFields, dumpFormat, printHeader)

def SensorReport(aSensor, sensorFields, dumpFormat, printHeader):

    if ( dumpFormat == True ):
        sensor_info.dump_sensor(aSensor, sensorFields, printHeader)
    else:
        print_sensor(aSensor)
        print("")
    ## end else if ( dumpFormat == True ):

## end SensorReport()

##############################################################################
# ReportByHost(theBuilding)

def ReportByHost(theBuilding, theOrder, dumpFormat):

    global sensorView

    sensorList = []
    if ( theBuilding > '' ):
        sensorWhere = sensorView['bldg-code']['dbField'] + " = " + \
            db_access.QuoteField(theBuilding, sensorView['bldg-code']['type'])
    else:
        sensorWhere = ''
    ## end if ( theBuilding > '' ):

    if ( theOrder == 'serial-nbr' ):
        sensorOrder = sensorView['serial-nbr']['dbField']
    elif ( theOrder == 'csid' ):
        sensorOrder = sensorView['csid']['dbField']
    else:
        sensorOrder = sensorView['bldg-code']['dbField'] + ", " + \
              sensorView['name']['dbField']
    ## end if ( theOrder == 'xxx' ):

    sensorQuery = db_access.SelectQuery('sensors', sensorView, 
        sensorWhere, sensorOrder)

    dbResult = db_access.PgQuery(sensorQuery, 'networking')

    sensorList = db_access.MakeDictList(dbResult, sensorView['sensors'])

    # We have the complete list. Print them out.
    sensorFields = ['name', 'lan-mac', 'wlan-mac', 'serial-nbr',
                'serial-key', 'csid', 'po', 'model', 
                'active', 'maint-state', 'replaced-by', 'rma-sensor']

    printHeader = True
    for aSensor in sensorList:
        SensorReport(aSensor, sensorFields, dumpFormat, printHeader)
        printHeader = False
    ## end for aSensor in sensorList:

    return()
## end ReportByHost()

##############################################################################
# ReportByGroup(theBuilding, theOrder, dumpFormat)

def ReportByGroup(theReport, theGroup, printFormat):

    global sensorView

    sensorList = []

    # Limit the report to just the sensors matching the specified criteria.
    # This restricts the name of a report to matching names of the fields
    # in the table. You need to expand this somehow.
    keyInfo = sensorView[theReport]
    whereValue = db_access.QuoteField(\
        check_input_info(theReport, theGroup, keyInfo), \
        keyInfo['type'])
    sensorWhere = keyInfo['dbField'] + " = " + whereValue

    # Sort the results by the appropriate field(s).
    sensorOrder = sensorView['name']['dbField']
    if ( theReport == 'serial-key' ):
        dumpFields = ['name', 'lan-mac', 'wlan-mac', 'serial-nbr',
                'csid', 'po', 'model', 'active',
                'maint-state', 'replaced-by', 'rma-sensor']
    elif ( theReport == 'po' ):
        dumpFields = ['name', 'lan-mac', 'wlan-mac', 'serial-nbr',
                'serial-key', 'csid', 'model', 'active', 
                'maint-state', 'replaced-by', 'rma-sensor']
    elif ( theReport == 'active' ):
        dumpFields = ['name', 'lan-mac', 'wlan-mac', 'serial-nbr',
                'serial-key', 'csid', 'po', 'model', 
                'maint-state', 'replaced-by', 'rma-sensor']
    elif ( theReport == 'maint-state' ):
        sensorOrder = sensorView['campusid']['dbField'] + "," + \
            sensorView['serial-key']['dbField']
        dumpFields = ['name', 'lan-mac', 'wlan-mac', 'serial-nbr',
                'serial-key', 'csid', 'po', 'model',
                'maint-state', 'replaced-by', 'rma-sensor']
    ## end if ( theReport > 'xxxx' ):

    sensorQuery = db_access.SelectQuery('sensors', sensorView, 
        sensorWhere, sensorOrder)

    dbResult = db_access.PgQuery(sensorQuery, 'networking')

    sensorList = db_access.MakeDictList(dbResult, sensorView['sensors'])

    # We have the complete list. Print them out.
    printHeader = True
    for aSensor in sensorList:

        SensorReport(aSensor, dumpFields, printFormat, printHeader)
        printHeader = False

    ## end for aSensor in sensorList:

    return()
## end ReportByGroup()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

