# Version information for this command.

g_version='26 Sep 2014 - 3.0.5'

# 29 Oct 2013   Change zone-id option to be net-zone to match the db
#               column name.
# 22 Oct 2013   Add option to dump all information by zone id.
# 26 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries
try:
    import network_zones
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s [net-cidr]
usage: %s --net-zone net-zone-id
    --help          display this help message
    --version       show version information and exit.
    --brief         show zone id and descriptions only.
    --net-zone      the argument is the net-zone number.
    net-cidr        the address range (CIDR) to display information about.
    net-zone-id     the net zone id to display information about.
    """ % ( gCommand, gCommand, gCommand)))
    return(exit_code)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = False
    # Default action.
    result['action'] = "list"

    result['optionList'] = [ ("b", "brief"), ("z", "net-zone") ]

    result['requiredArgs'] = []
    result['minArgs'] = 0

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmd_line_info - any command line flags and default options thus far.
# arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmd_line_info, arg_list):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # We have one optional positional argument.
    if ( len(arg_list) > 1 ):
        raise ValueError("too many command line arguments")
    elif ( len(arg_list) == 1 ):
        if ( cmd_line_info['--net-zone'] ):
            result['net_zone'] = int(arg_list[0])
        else:
            result['network_range'] = ipaddress.ip_network(arg_list[0])

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # initialize any tables here.
    network_zones.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Obtain our list of zones.

    # If we have no arguments then list all zone descriptions and ranges.
    if ( "network_range" not in cmd_line_info and "net_zone" not in
            cmd_line_info):
        zone_list = network_zones.get_zone_list(-1)

    # If we have a network range then restrict to just that range.
    if ( "network_range" in cmd_line_info ):
        zone_list = network_zones.find_range(cmd_line_info['network_range'])

    if ( "net_zone" in cmd_line_info ):
        zone_id = cmd_line_info['net_zone']
        if ( cmd_line_info['--brief'] ):
            zone_list = network_zones.get_zone_list(zone_id)
        else:
            zone_list = network_zones.get_range_info(zone_id)

    if ( len(zone_list) > 0 ):
        if ( cmd_line_info['--brief'] ):
            tbl_title = "Network Zones"
            tbl_head = ("Zone ID", "Zone Description")
            tbl_keys = ["net_zone", "zone_descr"]
            cmd_output.print_table(tbl_title, tbl_head, tbl_keys, zone_list)
        else:
            cmd_output.print_zones(zone_list)
    else:
        print("Could not find any zone information.")

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
