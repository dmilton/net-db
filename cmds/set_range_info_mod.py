# Version information for this command.

g_version='25 Sep 2014 - 3.0.3'

# 17 Apr 2015   network field was renamed to network_range.
# 24 Sep 2014   Rewrite to use new command line processing.
# 16 May 2014   Rewrite to use set command template.
# 13 May 2014   Qualify the NetException handling.
# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 25 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import network_zones
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version | --keys]
usage: %s zone-cidr [key1 value1]...
usage: %s --update zone-cidr key key-value [key2 value2]...
usage: %s --delete zone-cidr
    --help          display this help message
    --version       show version information and exit.
    --keys          list input keys for updating all fields in the table.
    --update        update keys associated with zone zone-cidr.
    --delete        delete the cidr value from the zone table.
    zone-cidr       the zone cidr value.
    """ % ( gCommand, gCommand, gCommand, gCommand )))
    sys.exit(exit_code)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What database and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options:
    result['optionList'] = []

    # There are four actions available - keys, add, update, and delete.
    # The add requires two arguments or the entry is pretty much useless.
    # list of table columns that are mandatory.
    result['addRequired'] = ['network_range']
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires the table column that is the primary key and
    # one key/value pair to update.
    result['updateRequired'] = ['network_range']
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete uses the room_id field.
    result['delRequired'] = ['network_range']
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmd_line_info, cmd_line_args):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize tables and set input keys.
    network_zones.initialize()
    result['inputKeys'] = network_zones.get_range_fields()

    # All operations require a minimum argument of the cidr
    # address range to add, update, or delete.
    if ( cmd_line_info['action'] in ['add','update','delete'] ):
        result['network_range'] = ipaddress.ip_network(cmd_line_args[0])
        result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # Turn cmd_line_info into a router_afl record dictionary
    # and an action to perform.
    action = cmd_line_info['action']

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    if ( action == 'delete' ):
        try:
            to_delete = {}
            to_delete['network_range'] = cmd_line_info['network_range']
            network_zones.delete_range(to_delete)
            print("The zone range " + str(to_delete['network_range'])
                            + " has been deleted.")
        except NetException as err:
            if ( err.name == 'not found' ):
                print("Unable to delete range as it was not in the database.")
                print((err.detail))
            else:
                raise
    elif ( action in ["add", "update"] ):
        # starting with a db record, which is all underscores,
        # translate underscores into hyphens so either can be used.

        # create a record from cmd_line_info to use for add/update.
        the_zone = network_zones.new_range()
        for aKey in list(the_zone.keys()):
            aKey2 = aKey.translate(str.maketrans('_','-'))
            if ( aKey in cmd_line_info ):
                the_zone[aKey] = cmd_line_info[aKey]
            elif ( aKey2 in cmd_line_info ):
                the_zone[aKey] = cmd_line_info[aKey2]
            else:
                del the_zone[aKey]

        try:
            if ( cmd_line_info['action'] == 'add' ):
                db_record = network_zones.add_range(the_zone)
                print("The zone has been added:")
            else:
                db_record = network_zones.update_range(the_zone)
                print("The zone has been updated:")
            # print the license we have.
            cmd_output.print_zones(db_record, True)
        except NetException as err:
            if ( err.name == "duplicate" ):
                print((err.detail))
            elif ( err.name == "not found" ):
                print((err.detail))
            else:
                raise
        except:
            raise

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 showmatch et :

