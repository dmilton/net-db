#
# Version information for this command.
#
g_version='24 Sep 2014 - 1.0.3'

# 20 May 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import network_zones
    import network_info
    import building_info
except ImportError as theErr:
    print("The NetTrackDB modules are not installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version | --keys
usage: %s 'zone-description'
usage: %s --update net-zone-id 'new zone-description'
usage: %s --renumber=new-net-zone-id net-zone-id
usage: %s --delete [--recurse] net-zone-id
    --help                      display this help message
    --version                   show version information and exit.
    --update                    update the zone description.
    --renumber=new-net-zone     change net-zone to new-net-zone
    --delete                    remove record from the table.
    --recurse                   only valid with delete, removes networks too.
    net-zone-id                 zone id to create/update/delete.
    zone-description            text string with the purpose of the zone.
    """ % (gCommand, gCommand, gCommand, gCommand, gCommand) )
    sys.exit(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # This command wants add, update, delete, and keys actions.
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options. renumber is an additional action.
    result['optionList'] = [ ("r", "recurse"), ("n:","renumber=") ]

    # The add requires the descriptio; net_zone is a sequence.
    # list of table columns that are mandatory.
    result['addRequired'] = ["zone_description"]
    result['addMinArgs'] = len(result['addRequired'])
    # Delete requires the zone id.
    result['delRequired'] = ["net_zone"]
    result['delMinArgs'] = len(result['delRequired'])
    # Update requires the table column that is the primary key and
    # the new description.
    result['updateRequired'] = ["net_zone", "zone_description"]
    result['updateMinArgs'] = len(result['updateRequired'])
    
    return(result)
## end default_options()

##############################################################################
# process_option
# This routine must process the command specific options one at a time
# and may return anything in default_options such a change in action.

def process_option(option, argument):

    result = {}

    # Handle options that change the action.
    if ( option == "--renumber" ):
        result['action'] = "renumber"
        result['requiredArgs'] = ["net_zone"]
        result['minArgs'] = 1
    else:
        # Fail here, caller handles these options.
        raise NameError(option + " is not defined here.")

    return(result)
## end process_option()


#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdInfo, argList):

    result = {}

    # initialize our building tables so we have the valid input fields.
    network_zones.initialize()

    # Get the list of inupt keys for the table
    result['inputKeys'] = network_zones.get_zone_desc_fields()

    argCount = len(argList)
    result['argsUsed'] = 0

    if ( cmdInfo['action'] == "renumber" and argCount == 1 ):
        result['net_zone'] = int(argList[0])
        result['argsUsed'] = 1
    elif ( cmdInfo['action'] == "add" and argCount == 1 ):
        result['zone_description'] = argList[0]
        result['argsUsed'] = 1
    elif ( cmdInfo['action'] == "update" and argCount == 2 ):
        # Add requires the zone id and new description.
        result['net_zone'] = int(argList[0])
        result['zone_description'] = argList[1]
        result['argsUsed'] = 2
    if ( cmdInfo['action'] == "delete" and argCount == 1 ):
        # Delete requires one argument - the room_id.
        result['net_zone'] = int(argList[0])

    return(result)
## end process_positional()

##############################################################################
# verify_options
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.
# It is not required for a command that implements add, delete, keys, update.
# Entry:
#       commandLineInfo - the processed command line as generated by
#       process_command_line, default_options and process_positional.
# Exit:
#       none.
# Exceptions:
#       raise a ValueError exception if there is something wrong with
#       the values of the data provided on the command line. Raise a
#       CommandError if there is something wrong with the combination
#       of options/arguments provided.
#
def verify_options(cmdLineInfo):

    # Check theOptions to see if there are some which are options
    # which are mutually exclusive. Verify the values received are
    # within bounds. Set default values based on the command line
    # received so far.
    # raise ValueError("invalid input for x")
    # raise CommandError("option X cannot be used with option Y")
        
    if ( cmdLineInfo['action'] == "renumber" and cmdLineInfo['--recurse'] ):
        raise CommandError("renumber cannot be combined with recurse")
    elif ( cmdLineInfo['action'] != "delete" and cmdLineInfo['--recurse'] ):
        raise CommandError("recurse is only valid with delete")

    return(cmdLineInfo)
## end verify_options()

#
##############################################################################
# Main

def main(commandInfo, userDefaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Try and get the description.
    # This always fails for an add since we don't know the ID until
    # it has been added to the table.
    noZone = True
    if ( commandInfo['action'] in ["update", "delete"] ):
        try:
            zoneDescr = network_zones.get_zone_desc(commandInfo['net_zone'])
            noZone = False
        except NetException as theErr:
            if ( theErr.name != "NoZone" ):
                raise
        except:
            raise

    # Now handle what the user gave us on the command line.
    if ( commandInfo['action'] == "add" ):

        if ( noZone ):
            # zone being added is not a duplicate so add it.
            newZone = network_zones.add_zone(commandInfo['zone_description'])
        else:
            raise ValueError("The zone '" + str(commandInfo['zone_description'])
                    + "' already exists")
        print("A new zone has been created:")
        print("Network Zone: " + str(newZone['net_zone']) + ", " +
                    newZone['zone_descr'])

    elif ( commandInfo['action'] == "update" ):

        if ( noZone ):

            raise ValueError("The zone " + str(commandInfo['net_zone'])
                    + " does not exist")

        else:

            theZone = {}
            theZone['net_zone'] = commandInfo['net_zone']
            theZone['zone_descr'] = commandInfo['zone_description']
            updatedZone = network_zones.update_zone_desc(theZone)
            print("Zone Updated.")
            print("Network Zone " + str(updatedZone['net_zone']) +
                    ", " + updatedZone['zone_descr'])

    elif ( commandInfo['action'] == "renumber" ):

        # The net_zone field is used as a foreign key with update cascade
        # so updating the net_zone should update all of the dependant
        # foreign keys. This makes renumber pretty trivial.
        raise CommandError("renumber not yet implemented")

    elif ( commandInfo['action'] == "delete" ):

        # delete the zone.
        if ( noZone ):
            raise ValueError("The zone does not exist")

        # The net_zone field is used as a foreign key with delete
        # set default in buildings and free networks so removing the
        # net_zone should set the buildings and networks to a default
        # 'unknown value' to be fixed later.

        # delete the zone should delete any free networks.
        if ( commandInfo['--recurse'] ):
            print("Recursive delete, removing assigned networks...")
            # Need type info for releasing/deleting networks.
            typeDetail = network_types.get_type_detail(0)
            typeInfoByName = typeDetail['type_info_by_name']
            # Remove all buildings and networks
            zoneBuildings = building_info.get_zone_buildings(
                    commandInfo['net_zone'])
            for aBuilding in zoneBuildings:
                # delete all of the allocated networks
                bldgNets = network_info.get_networks(aBuilding['bldg_code'])
                for aNet in bldgNets:
                    netFamily = typeInfoByName[aNet['net_type']]['net_family']
                    network_info.delete_network(aNet, netFamily)
                    print("removed " + aNet['network'] + " from " +
                            aNet['bldg_code'])
                # Now remove the building itself.
                print("you may want to remove building:")
                print(aBuilding['bldg_name'] + 
                        "(" + aBuilding['bldg_code'] + ")")
                print("set-bldg-info --delete " + aBuilding['bldg_code'])
        # Remove address spaces in the zone
        addrRanges = network_zones.get_range_info(commandInfo['net_zone'])
        for aRange in addrRanges:
            # get_range_info could return zone 0 if this zone has no
            # address ranges defined.
            if ( aRange['net_zone'] == commandInfo['net_zone'] ):
                print("Removing range: " + str(aRange['network_range']) )
                network_zones.delete_range(aRange)

        # finally, remove the zone itself.
        network_zones.delete_zone(commandInfo['net_zone'])
        print("Removed zone " + str(commandInfo['net_zone']) )

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

