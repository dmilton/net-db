#
# Version information for this command.

g_version='16 Jan 2015 - 3.0.11'

# This script accepts one argument - an IP address.
# From the IP address it will determine the most likely IP network for
# that IP address.

# 19 Mar 2015   Add --whois option to allow lookup of Who IS data.
# 16 Jan 2015   Add output of vlan name.
# 16 Sep 2014   Change for new template.
# 30 Apr 2014   Expand information gathering to include the network
#               description, network type, and type description.
# 24 Apr 2014   When there are multiple ARP entries for an IP but none
#               of them are active, things would fail. Now default to
#               using the first one, then search to replace it with one
#               that is currently active.
# 25 Mar 2014   Fix issues with an ARP entry that exists but because of
#               broken Cisco SNMP MIB, the mac address forwarding table
#               is unavailable. Use unknown when the port cannot be
#               determined.
# 24 Mar 2014   fix printing information for invalid networks. Add the
#               building code to the output after the building name.
# 19 Mar 2014   Add --file argument which allows processing of a file
#               of IP addresses. Major rewrite of mainline.
#               Rename --dump-net to --subnet
# 23 Sep 2013   Add code to conditionally debug after init done.
# 23 Apr 2013   Have more complete error checking when a free network
#               is returned as a result.
# 11 Mar 2013   Complete rewrite with netdisco module. Changed db
#               table initialization from tuple to list. Change calls
#               to find MAC addresses based on rewrite of find-mac-info
#               so the same call can be used for both.
# 22 Jan 2013   Rewrite as version 3 using db_access. Completely
#               new structure for all database access.
# 21 Jan 2013   Update to use modules in the site-local directory
#               rather than in the old netLib directory.
# 27 Sep 2011   The 'size' value is no longer available from postgres
#               as a 128 bit integer is needed to handle ipv6 addresses.
#               Changed code to make a call to a GetNetSize routine
#               which only works for ipv4 addresses.
# 16 Aug 2010   Add VerifyOptions function.
#  9 Mar 2010   Add a new --dump-net option which loops through
#               all of the addresses in a subnet.
#               Added --brief potion to show easily parsable output.
#  4 Mar 2010   Convert to using standardized command module and
#               added better user diagnostics.
# 12 Nov 2009   Changed import directory to /opt2/local/etc.
#  2 Nov 2009   Moved to /opt2/local/... and updated the include
#               of the global configuration file to reflect this.
#  4 Aug 2009   Fixed problems with the development server code.
# 14 Jul 2009   More changes to exception handling so debug output
#               generates tracebacks to the screen but normal execution
#               captures the traceback and mails them to me.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Added code to handle development database server.
#               Removed SanityCheckIP since it is no longer used.
# 24 Jun 2009   Added ability to do DNS lookup.
# 22 Jun 2009   Changed help to say IP rather than MAC address.
#               Was returning the first port returned by the query
#               but this is not always correct. When a device has
#               been on many ports, the order of the entries in the
#               database may not reflect the current state. Ordering
#               the result by last seen time and taking the most
#               recent one gives the desired result.
# 22 Jan 2009   Do a little sanity checking on the IP address.
# 20 Jan 2009   Fixed command line argument processing so the
#               short flags are not used for debug and version.
#  9 Jan 2009   Added output option to display the switch port.
# 17 Dec 2008   Added more checks for switch port information since
#               when a building is upgraded, the original port may
#               no longer exist. When the switch is aged out, the
#               the MAC address records don't point to the right place.
# 12 Dec 2008   Added a check to test for a valid switch port
#               result since sometimes we have an ARP entry but
#               do not have a switch port location for the MAC address.
#  5 Dec 2008   Added code to lookup in the networking database
#               for the IP subnet and building information.
#               Added section to query node table to obtain the
#               switch ports the active MAC address has been
#               recorded on. Also added queries to the device_port
#               table to get the interface description.
# 18 Nov 2008   When no information on an IP is available, the
#               command issues tracebacks. Fixed to give a meaningful
#               message that no information exists.
#  7 Jul 2008   Rewrote to use the netdisco database information
#               instead of the network info/node info database.
# 20 Nov 2007   Changed location of db commands scripts.
# 25 Oct 2007   Fixed more bad table variable references.
# 24 Oct 2007   Fixed some bad table references and moved some
#               of the macros into the global macro script.
# 23 Oct 2007   Changed database table references to use variables
#               so table and database names can change easily.
# 18 Oct 2007   Created a limited output option. Changed many of
#               the variable names so they remained local to the
#               routine OR were named as global variables.
#               Changed database queries to use inet_ntoa for
#               IP address information.
# 24 Aug 2007   When there are multiple entries in the database
#               the order is by addition so the most current entry
#               could be in the middle of the list. Sort the list
#               by last link up to get the most current entry first.
# 23 Aug 2007   Changed database view fields, updated query.
#               Wrote routine to extract the ARP table information
#               in the database when location entries do not exist.
# 16 Jan 2007   Update script to check the ARP table information
#               in the database in case we have ARP entries but
#               do not have physical location.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress
from socket import gethostbyaddr

# External Libraries
try:
    # Module with other dependancies:
    from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_types
    import network_info
    import netdisco
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] [--version] 
usage: %s [--port] [--subnet] ip-address
usage: %s [--port] --file file-path
usage: %s --whois ip-address
usage: %s --whois --file file-path
    --help      Display this help message.
    --version   Show version information and exit.
    --whois     Display tech and abuse contact for ip-address.
    --brief     Generate single line output. This is the default when
                --file and --subnet are specified.
    --port      Show the switch port with description.
    --file      Process all addresses in file-path, one IP address per line.
    --subnet    Output summary information for all addresses in a network.
                This option is not valid for IPv6 subnets.
    ip-address  An IPv4 or IPv6 address. When --subnet is specified
                this can be any IPv4 address within the desired subnet.
    """ % ( gCommand, gCommand, gCommand, gCommand, gCommand )))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking", "netdisco"]
    result['dbUserClass'] = "look"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = 'single-ip'

    result['optionList'] = []
    result['optionList'].append( ("b", "brief") )
    result['optionList'].append( ("s", "subnet") )
    result['optionList'].append( ("f", "file") )
    result['optionList'].append( ("p", "port") )
    result['optionList'].append( ("w", "whois") )

    result['requiredArgs'] = ["user_ip_file"]
    result['minArgs'] = 1

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0
    # We only have one positional argument.
    if ( cmdInfo['--file'] ):
        # Our positional argument is a file name.
        result['user_ip_file'] = argList[0]
        result['argsUsed'] = 1
    elif ( cmdInfo['--subnet'] ):
        result['user_ip_file'] = ipaddress.ip_interface(argList[0])
        result['argsUsed'] = 1
    else: # address lookup or whois lookup.
        # Our positional argument is an IP address.
        result['user_ip_file'] = ipaddress.ip_address(argList[0])
        result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(cmdLineInfo):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.

    if ( cmdLineInfo['--subnet'] and
            ( cmdLineInfo['--file'] or cmdLineInfo['--whois'] ) ):
        raise CommandError("--subnet cannot be used with --file or --whois.")

    if ( cmdLineInfo['--subnet'] ):
        if ( cmdLineInfo['user_ip_file'].version == 4 and
                cmdLineInfo['user_ip_file'].network.prefixlen < 22 ):
            raise CommandError("max of /22 mask size for IPv4")
        # Cannot (do not want to) process an entire IPv6 network.
        if ( cmdLineInfo['user_ip_file'].version == 6 and
                cmdLineInfo['user_ip_file'].network.prefixlen < 120 ):
            raise CommandError("max of /120 mask size for IPv6")
        # subnet output is always brief
        cmdLineInfo['action'] = "subnet"
        cmdLineInfo['--brief'] = True
    elif ( cmdLineInfo['--file'] ):
        # file output is always brief
        cmdLineInfo['action'] = "read-file"
        cmdLineInfo['--brief'] = True

    return(cmdLineInfo)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize any tables here.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()
    netdisco.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    if ( cmdLineInfo['action'] == 'single-ip' ):

        if ( cmdLineInfo['--whois'] ):
            whois_info = get_whois_info(cmdLineInfo['user_ip_file'])
            print_whois_info(cmdLineInfo['user_ip_file'], whois_info)
        else:
            # Get the information from the networking database.
            ipInfo = get_networking_info(cmdLineInfo['user_ip_file'])
            # Get the information from the netdisco database.
            ipInfo.update( get_netdisco_info(ipInfo['ip']) )
            # Generate some output.
            print_net_info(ipInfo)
            print_ip_info(ipInfo, cmdLineInfo['--brief'], cmdLineInfo['--port'])

    elif ( cmdLineInfo['action'] == 'subnet' ):

        # Get the information from the networking database.
        netInfo = get_networking_info(cmdLineInfo['user_ip_file'])
        print_net_info(netInfo)

        # Did we get a subnet on the command line?
        if ( cmdLineInfo['user_ip_file'].network.prefixlen == 32 or
                cmdLineInfo['user_ip_file'].network.prefixlen == 128 ):
            # no subnet, just a single IP, use the mask from the database.
            theNetwork = netInfo['network']
        else:
            # Got a subnet, use the mask from the command line.
            theNetwork = cmdLineInfo['user_ip_file'].network

        for oneIp in list(theNetwork.hosts()):
            # Display the IP/Arp Info
            ipInfo = {}
            ipInfo.update(netInfo)
            ipInfo['ip'] = oneIp
            ipInfo.update( get_netdisco_info(ipInfo['ip']) )
            print_ip_info(ipInfo, cmdLineInfo['--brief'], cmdLineInfo['--port'])

    elif ( cmdLineInfo['action'] == 'read-file' ):

        # Read the file, insert into global below by IP so we eliminate
        # duplicates as we go along by virtue of the dictionary.
        global gIpInfoRecords
        whoisInfoRecords = {}

        # Open the file.
        try:
            inFile = open(cmdLineInfo['user_ip_file'], 'r')
        except FileNotFoundError as msg:
            print(msg)
            sys.exit(-2)
        # Read lines one at a time, counting as we go.
        inputLines = 0
        for aLine in inFile:
            inputLines += 1
            try:
                if ( len(aLine[:-1]) >= 7 ):
                    thisIP = ipaddress.ip_address(aLine[:-1])
                    if ( thisIP not in gIpInfoRecords ):
                        if ( cmdLineInfo['--whois'] ):
                            gotWhois = False
                            for aNet in whoisInfoRecords.keys():
                                if ( thisIP in aNet ):
                                    gotWhois = True
                                    whois_info = whoisInfoRecords[aNet]
                            if ( gotWhois == False ):
                                whois_info = get_whois_info(thisIP)
                                whois_cidr = \
                                    ipaddress.ip_network(whois_info['asn_cidr'])
                                whoisInfoRecords[whois_cidr] = whois_info
                            gIpInfoRecords[thisIP] = whois_info
                        else:
                            gIpInfoRecords[thisIP] = get_networking_info(thisIP)
            except ValueError as msg:
                print(msg)

        print("There were " + str(inputLines) + " lines of input with " + \
                str(len(gIpInfoRecords)) + " unique IP addresses.\n")

        sortedIPs = sorted(gIpInfoRecords.keys())
        currentSubnet = ipaddress.ip_network('0.0.0.0/0')
        # Process each IP
        for theIP in sortedIPs:
            ipInfo = gIpInfoRecords[theIP]

            if ( cmdLineInfo['--whois'] ):
                print_whois_info(theIP, ipInfo)
            else:
                # If the IP is not valid then we are done for this one.
                if ( 'free_date' in ipInfo ):
                    print( (str(theIP) + " is not valid on campus."))
                    continue

                # First time we see a subnet, print summary info about it.
                if ( currentSubnet != ipInfo['network'] ):
                    print_net_info(ipInfo)
                    currentSubnet = ipInfo['network']

                # Get the netdisco information.
                ipInfo.update( get_netdisco_info(ipInfo['ip']) )
                print_ip_info(ipInfo, 
                        cmdLineInfo['--brief'], cmdLineInfo['--port'])
            print("---")

## end main()

#
##############################################################################
# Local function definitions...

def print_net_info(ipInfo):

    if ( 'free_date' in ipInfo ):
        print( "\nThe IP address " + str(ipInfo['ip']) + \
            " is not part of any active network.")
        raise ValueError("This address is not currently valid on campus.")

    print( "\nThe IP address " + str(ipInfo['ip']) + \
        " is part of the network " + str(ipInfo['network']) + "." )
    print("The network named '" + ipInfo['net_name'] + 
            "' is type " + ipInfo['net_type'] + ".")
    routerIP = ipInfo['network'].network_address + 1
    print( "The router for this network is " + str(routerIP) +\
        " and this network has been" )
    print( "allocated to " + ipInfo['bldg_name'] + \
            " (" + ipInfo['bldg_code'] + ").\n" )

## end def print_net_info()

def print_ip_info(ipInfo, brief, showPort):

    # If the network is invalid, say so.
    if ( 'free_date' in ipInfo ):
        print( str(ipInfo['ip']) + " is not valid on campus." )
        return

    # If there is not ARP entry, it's never been seen online.
    if ( 'mac' not in ipInfo ):
        print( str(ipInfo['ip']) + " has not been seen online." )
        return

    # shorten the host name for the switch.
    shortName = ipInfo['switch_name'].split('.')[0]

    if ( brief ):
        # Single line summary output.
        if ( showPort ):
            msg = str(ipInfo['ip']) + "\t" + \
                ipInfo['mac'] + "\t" + ipInfo['comm_outlet'] + "\t" + \
                shortName + "\t" + ipInfo['port'] + "\t" + \
                str(ipInfo['last_seen']) + "\t" + ipInfo['bldg_name']
        else:
            msg = str(ipInfo['ip']) + "\t" + \
                ipInfo['mac'] + "\t" + ipInfo['comm_outlet'] + "\t" + \
                str(ipInfo['last_seen']) + "\t" + ipInfo['bldg_name']
    else:
        # multi-line verbose output.
        msg = "The IP Address " + str(ipInfo['ip']) + "\n" +\
            "    was recorded from MAC address " + ipInfo['mac'] + "\n" +\
            "    and last seen " + str(ipInfo['last_seen']) + ".\n" +\
            "The MAC address " + ipInfo['mac'] + " was seen connected to \n    "
        if ( showPort ):
            msg += "switch " + shortName + " port " + ipInfo['port']
        msg += " outlet " + ipInfo['comm_outlet']

    print(msg)
## end def print_ip_info(ipInfo):

# Save building, type, and ip information.
gBuildingInfoRecords = {}
gTypeInfoRecords = {}
gIpInfoRecords = {}

# Given an IP address, find the network and building from the
# networking database.
def get_networking_info(anIp):
    global gBuildingInfoRecords, gIpInfoRecords, gTypeInfoRecords

    result = {}

    if ( 'type_info_by_name' not in gTypeInfoRecords ):
        gTypeInfoRecords = network_types.get_type_detail(0,True)

    if ( anIp not in gIpInfoRecords ):
        result['ip'] = anIp
        try:
            netList = network_info.get_networks_within(anIp, reverse=True)
        except:
            # Couldn't find the network in the database.
            result['free_date'] = "-infinity"
        else:
            # found a network.
            result['network'] = netList[0]['network']
            result['net_name'] = netList[0]['net_name']
            netType = netList[0]['net_type']
            result['net_type'] = netType
            typeInfo = gTypeInfoRecords['type_info_by_name'][netType]
            if ( 'free_date' in netList[0] ):
                # Network is not allocated to a building.
                result['free_date'] = netList[0]['free_date']
            else:
                # Network is assigned to a building, get
                result['bldg_code'] = netList[0]['bldg_code']
                # Only grab building info if we don't have it already.
                if ( result['bldg_code'] not in gBuildingInfoRecords ):
                    gBuildingInfoRecords[result['bldg_code']] = \
                        building_info.get_building_info(result['bldg_code'])
                result['bldg_name'] = \
                        gBuildingInfoRecords[result['bldg_code']]['bldg_name']
                gIpInfoRecords[anIp] = result
    else:
        result = gIpInfoRecords[anIp]

    return(result)
## end def get_networking_info()

# Given an IP address find the ARP table entry, switch forwarding table
# entry for the associated MAC (Node entry), and then look at the switch
# port itself to get the interface description/alias.
def get_netdisco_info(theIP):
    result = {}

    # If the IP is not valid then we are done for this one.
    if ( 'free_date' in result ):
        return(result)
    else: 
        # Get the active ARP entry for this IP.
        arpEntries = netdisco.find_arp_entries("", theIP)
        if ( len(arpEntries) == 0 ):
            return(result)
        elif ( len(arpEntries) >= 1 ):
            # Start with the first one, then look for the active one.
            recentArp = arpEntries[0]
            # got more than one, pick active one
            for anArp in arpEntries:
                if ( anArp['active'] ):
                    recentArp = anArp

    # Extract the MAC address and last time seen.
    result['mac'] = recentArp['mac']
    result['last_seen'] = recentArp['time_last']

    # Get all ports this MAC has been seen on.
    portList = netdisco.find_node_entries(result['mac'], result['mac'], False)
    # Only use the last port found which is the most recent one.
    if ( len(portList) > 0 ):
        thePort = portList[-1]
        result['switch'] = thePort['switch']
        result['switch_name'] = gethostbyaddr(str(result['switch']))[0]
        # Juniper ports have a unit interface. Remove that if present.
        if ( '.' in portList[-1]['port'] ):
            result['port'] = thePort['port'].split('.')[0]
        else:
            result['port'] = thePort['port']

        # Now get the details we want on that specific port.
        portInfo = netdisco.find_port_info(result['switch'], result['port'])
        result['comm_outlet'] = portInfo['name']
    else:
        result['switch'] = '0.0.0.0/0'
        result['switch_name'] = 'unknown'
        result['comm_outlet'] = 'unknown'
        result['port'] = 'unknown'

    return(result)
## end def get_netdisco_info()

# Given an IP address find the WHOIS information for that address.
def get_whois_info(theIP):
    # Get the Whois information for the IP address.
    whois_obj = IPWhois(str(theIP))
    whois_info = whois_obj.lookup()
    return(whois_info)
## end def get_whois_info()

def print_whois_info(ip_address, whois_info):
    print("IP Address: " + str(ip_address))
    print("Org: " + whois_info['nets'][0]['description'])
    print("Net range: " + whois_info['nets'][0]['range'])
    print("Tech: " + whois_info['nets'][0]['tech_emails'])
    print("Abuse: " + whois_info['nets'][0]['abuse_emails'])
## end def print_whois_info()
# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
