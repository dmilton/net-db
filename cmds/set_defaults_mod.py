#
# Version information for this command.
#
g_version='26 Sep 2014 - 1.0.1'

#  9 Apr 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import user_defaults
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#\
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s [--help | --version | --keys | --show | --delete]
usage: %s user-name key key-value [key key-value]...
usage: %s key key-value [key key-value]...
    --help              display this help message
    --version           show version information and exit.
    --keys              display the update keys for the command.
    --update            instead of changing, show current settings.
    --delete            remove defaults values for a user.
    table-key-value     key/value pair (see --keys output).
    """ % (gCommand, gCommand))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    result['optionList'] = [ ("u:", "--user=") ]

    # There are four actions available - keys, add, update, and delete.
    # Given that the table only contains one row, a key value doesn't
    # make sense so there are no required values, only that one key
    # and key-value pair be provided.
    # Add requires at least one key/value pair to update.
    result['addRequired'] = []
    result['addMinArgs'] = len(result['addRequired']) + 2
    # Delete uses the room_id field.
    result['delRequired'] = []
    result['delMinArgs'] = len(result['delRequired'])
    # Update requires at least one key/value pair to update.
    result['updateRequired'] = []
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    
    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdInfo, argList):

    result = {}

    # initialize our building tables so we have the valid input fields.
    defaults.initialize()

    # Get the list of inupt keys for the table
    result['inputKeys'] = defaults.get_defaults_fields()

    # Only delete has an optional positional argument.
    result['argsUsed'] = 0

    if ( cmdInfo['action'] in ["delete"] ):
        if ( len(argList) == 1 ):
            result['user_name'] = argList[0][:32]
            result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # Global default information.
    global _defaultInfo

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Start by figuring out which user is to be modified.
    if ( cmdLineInfo['--user'] != None ):
        userName = cmdLineInfo['--user']
    else:
        from getpass import getuser
        userName = getuser()

    # Get the defaults for all users.
    defaultInfo = user_defaults.get_defaults(userName)
    if ( defaultInfo['user_name'] != userName ):
        globalDefault = True
        defaultInfo['user_name'] = userName
    else:
        globalDefault = False

    # Update anything in defaultInfo that was provided on the command line.
    for aKey in list(cmdLineInfo.keys()):
        if ( aKey in defaultInfo and aKey != "user_name" ):
            defaultInfo[aKey] = cmdLineInfo[aKey]

    # Now handle what the user gave us on the command line.
    if ( cmdLineInfo['action'] == "add" ):
        if ( globalDefault ):
            user_defaults.add_defaults(defaultInfo)
        else:
            raise CommandError(
                    "Cannot add user. User information already exists.")
    elif ( cmdLineInfo['action'] == "update" ):
        if ( globalDefault ):
            raise CommandError(
                    "Cannot update user. Existing user information not found.")
        else:
            user_defaults.update_defaults(defaultInfo)
    elif ( cmdLineInfo['action'] == "delete" ):
        if ( globalDefault ):
            raise CommandError(
                    "Cannot delete user. Existing user information not found.")
        else:
            user_defaults.delete_defaults(defaultInfo)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

