# Version information for this command. Updated automatically on install
# by set-version command.

g_version='28 Apr 2015 - 1.0.11'

# Allocate networks from the database to a specific building.

# 28 Apr 2015   Change --type to --net-type for consistiency across suite.
#               Multiple --net-type arguments not working, only the last one
#               is recognized. Change to a single arg with comma separated
#               list of network types as argument.
# 21 Aug 2014   Update to use new networks module to get the list of
#               networks to assign.
# 11 Aug 2014   Invalid building code exception handling was not working
#               properly. Fixed error message.
#  4 Jun 2014   Update for new column names which identify table somewhat.
#  2 Jun 2014   Back out of move of get_v4/get_v6_networks to networks
#               library. The rewrite in the networks module isn't ready
#               for prime time and these mostly work.
# 24 Apr 2014   Fix bug where user informational message is not set after
#               assigning a network.
#  9 Apr 2014   The list of types already allocated was not being constructed
#               correctly as some types did not get added. 
# 24 Mar 2014   Cleanup Usage display and format similar to other commands.
# 10 Mar 2014   Remove unused import of the Google ip address class.
#  5 Mar 2014   Change the way the --type command line option works so
#               that becomes the only type which is added.
#               the standard/default networks.
#  5 Mar 2014   Fix 'info' command line argument to it actually does
#               what it's supposed to. Fix test for the prefix size==list.
#  5 Mar 2014   In filtering through the command line arguments to build
#               the list of 'needed' types, some types were misseg because
#               the search was for 'net_type' and not for the the type name.
# 29 Dec 2013   Split the ipv4/ipv6 network calculation and then allocate
#               all the networks once calculated.
# 26 Nov 2013   add_network returns a result so make it explicit that
#               we are ignoring that result here.
# 29 Oct 2013   Add --v6 flag to assign IPv6 networks.
# 15 Oct 2013   Add new type camera for NVR/DVR and digital camera use.
#               Remove assign as a command line flag as this is the
#               default behaviour. Rewrite much of the underlying network
#               calculation routines to use ip address class and subnets
#               rather than playing games with the string version of
#               the networks. This yeilds ip version agnostic code.
#  4 Jul 2013   Add debugger code to isolate problems triggered by
#               a db schema change.
#  4 May 2013   Start conversion for new schema and db routines.
# 21 Jul 2011   With the move of the VoIP network and growth of
#               wireless/sensors, etc. expanded the size of the
#               management network and the sunray networks.
# 18 Jan 2011   The VoIP network isn't big enough any more. Changed
#               code to allocate from a new private class B using
#               the same scheme as for the misc network.
# 7 Dec 2010    Seems the classroom allocation isn't working because
#               the network_types value got changed to clsrm. Fixed.
# 16 Aug 2010   Add VerifyOptions function.
# 13 May 2010   Conversion to modular mainline. Fixed problems caused
#               by allocating the misc network as a secondary on the
#               main user network.
#  3 Dec 2009   If a primary network is not assigned the command
#               exits with the error of: UnboundLocalError: local
#               variable 'userNetwork' referenced before assignment.
# 10 Sep 2009   Complete rewrite of the allocation scheme. Now use
#               the network_types table to determine what networks
#               need to be allocated and compare that table to the
#               networks currently allocated in the building.
#               Added separate routine to calculate the private
#               networks rather than doing this in main.
# 18 Aug 2009   Added new routine to handle allocation of networks
#               in a more generic way. Uses the network_types table
#               to assign vlan id and name postfix.
# 17 Aug 2009   Made sunray network optional, added code to handle
#               the various allocation dates, etc for the private
#               networks being added. Add code to allocate a loopback
#               address to the building.
# 29 Jul 2009   Moved crash routine into debug module. Updated call.
#               Synchronized form with new template.
#               Added loopback and sunray as command line options.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to mail stack trace/bug output.
# 29 Jun 2009   Add code for development database server.
#  5 Jun 2009   Add code to set the assign_by and assign_date
#               fields of the network_allocations table.
# 10 Dec 2008   Add variables and handlers for Bannatyne Campus.
# 25 Nov 2008   The GetNextNet call needs to use the mask of
#               and network of the 'previous' network but in the
#               case of the VoIP network I used the VoIP mask
#               which is incorrect. Need to add the cidr values
#               for both the starting network and the next network
#               in the chain to GetNextNetwork in order to obtain
#               the correct results.
# 23 Sep 2008   Fix problems with allocation of classroom subnet.
#               Some variable names did not get changed appropriately
#               when duplicated code was turned into a subroutine.
# 18 Aug 2008   Fix for new GetNetworks result format.
# 10 Jul 2008   initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
    import network_zones
    from network_log import find_who
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    # Additional types need to match the net_type
    # field in the network_types table.
    print(("""
usage: %s --help | --version
usage: %s [--info] [--net-type=X] [-v6] bldg-code
    --help        display this help message
    --version     show version information and exit.
    --info        show what would happend but do not allocate networks.
    --v6          add IPv6 networks instead of IPv4.
<<<<<<< HEAD
    --net-type=X  add only the net type X to the building.
=======
    --net-type=X  add net type X (or comma separated list of types).
>>>>>>> develop
    --list-types  list optional types which can be added.
    """ % ( gCommand, gCommand ) ))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = False
    # Default action.
    result['action'] = "assign"

    # Command line options:
    result['optionList'] = [ ("i", "info"), ("t:", "net-type="),
            ("l", "list-types"), ("4", "v4"), ("6", "v6") ]

    # Required arguments (--list-types doesn't require any)
    result['requiredArgs'] = ["bldg_code"]
    result['minArgs'] = 1

    return(result)
## end default_options()

##############################################################################
# process_option
# This routine must process the command specific options one at a time
# and may return anything in default_options such a change in action.

def process_option(option, argument):

    result = {}

    # Handle options that change the action.
    if ( option == "--list-types" ):
        result['action'] = 'list-types'
        result['requiredArgs'] = []
        result['minArgs'] = 0
    else:
        # Fail here, caller handles these options.
        raise NameError(option + " is not defined")

    return(result)
## end process_option()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0

    # We only have one positional argument.
    if ( cmdInfo['action'] == "list-types" ):
        if ( len(argList) != 0 ):
            raise CommandError("extra command line arguments for --list-types.")
    else:
        result['bldg_code'] = argList[0]
        result['argsUsed'] = 1

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise CommandError("invalid option combination")

    if ( theOptions['--v6'] and theOptions['--v4'] ):
        raise CommandError("--v4 and --v6 are mutually exclusive")

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize the table handlers.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()
    network_zones.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # display the optional types if requested.
    if ( cmdLineInfo['action'] == 'list-types' ):
        list_types()
        sys.exit(0)

    # Verify the building code.
    try:
        bldgInfo = building_info.get_building_info(cmdLineInfo['bldg_code'])
    except NetException as err:
        if ( err.name == "UnknownBuildingCode" ):
            print(("Unable to find building code: " + err.detail))
            sys.exit(2)
        ## end if ( err.name == "UnknownBuildingCode" )
    except Exception:
        raise

    # Was a specific type requested?
    if ( cmdLineInfo['--net-type'] != None ):
        requestedTypes = cmdLineInfo['--net-type'].split(',')
    else:
        requestedTypes = []

    # If requesting IPv6 then ISO is required too.
    if ( cmdLineInfo['--v6'] ):
        requestedTypes.append("iso")
        ipVer = 6
    else:
        ipVer = 4

    print("Allocating/Checking networks for:")
    print("    " + bldgInfo['bldg_name'] + " on " +
            bldgInfo['campus_name'] + " campus.")

    # Get the basic required information.
    # This raises an exception if no user address space assigned.
    buildingNetDetail = network_info.get_bldg_net_detail(bldgInfo['bldg_code'],
            requestedTypes, ipVer)

    # List the primary network and what has already been assigned.
    userNetwork = buildingNetDetail['prime_net']
    if ( ipVer == 4 and "prime_net" in buildingNetDetail ):
        print("Primary IPv4 User Network:")
        if ( userNetwork == None ):
            print("    Primary network not assigned!")
        else:
            print(("    vlan " + str(userNetwork['vlan_tag']) + ", name " + \
                userNetwork['net_name'] + " " + str(userNetwork['network']) ))

    # List any remaining networks already allocated.
    if ( len(buildingNetDetail['assigned_networks']) == 1 ):
        print("No other networks are currently assigned.")
    else:
        print("Found these additional networks assigned:")
        # Would be nice to sort the networks but ISO is a string, not a
        # ipaddress type; the elem['network'].version presents problems.
        #foundNets = sorted( buildingNetDetail['assigned_networks'] , key=lambda
        #    elem: "%04d %d %s" % (elem['vlan_tag'], elem['network'].version,
        #        elem['network']) )

        for aNet in buildingNetDetail['assigned_networks']:
            if ( aNet['net_type'] == 'user' ):
                if ( aNet['prime_net'] != True ):
                    print(("    vlan " + str(aNet['vlan_tag']) + ", name " + \
                        aNet['net_name'] +" " + str(aNet['network']) ))
            else:
                print(("    vlan " + str(aNet['vlan_tag']) + ", name " + \
                    aNet['net_name'] +" " + str(aNet['network']) ))

    # Get the list of networks to assign.
    if ( ipVer == 6 ):
        netsToAllocate = network_info.get_v6_networks(
                bldgInfo, buildingNetDetail, cmdLineInfo['--info'] == False)
    else:
        netsToAllocate = network_info.get_v4_networks(
                bldgInfo, buildingNetDetail, cmdLineInfo['--info'] == False)

    # Is there anything to assign?
    if ( len(netsToAllocate) > 0 ):
        if ( cmdLineInfo['--info'] ):
            print("Would assign network(s): ")
        else:
            print("Assigning network(s): ")
            updateUser = find_who()
    else:
        print("No additional network(s) to assign.")

    # If we have at least one network to assign, assign it.
    netTypeInfoList = buildingNetDetail['net_type_detail']['type_info_by_name']
    for aNet in netsToAllocate:
        aNet['bldg_code'] = bldgInfo['bldg_code']
        if ( cmdLineInfo['--info'] ):
            addedNet = aNet
        else:
            typeInfo = netTypeInfoList[aNet['net_type']]
            addedNet = network_info.assign_network(aNet, typeInfo, bldgInfo)

        print(("    vlan " + str(addedNet['vlan_tag']) + ", name " + \
            addedNet['net_name'] + ", type " + addedNet['net_type'] + \
            ", " + str(addedNet['network']) ))

    print("Network assignment run complete.")

## end main()

def list_types():

    theTypes = network_types.get_opt_types()
    if ( len(theTypes) > 0 ):
        print("Optional types which can be added are:")
        typeNames = []
        for aType in theTypes:
            typeNames.append(aType['net_type'])
        for s in range(0, len(typeNames), 4):
            if ( s + 4 > len(typeNames) ):
                e = len(typeNames)
            else:
                e = s + 4
            print(("\t" + '\t'.join(typeNames[s:e])))

## end list_types()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

