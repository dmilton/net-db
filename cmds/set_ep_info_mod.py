#
# Version information for this command.
#
g_version='25 Sep 2014 - 3.0.2'

# 11 Oct 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version | --keys]
usage: %s ep-code core-priority
usage: %s --update ep-code core-priority
usage: %s --delete ep-code
    --help              display this help message
    --version           show version information and exit.
    --update            update ep-code core-priority or net-zone.
    --delete            remove the endpoint ep-code.
    ep-code             the building code to make a core endpoint.
    core-priority       the value to set the endpoint priority to.
    """ % (gCommand, gCommand, gCommand, gCommand)))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options:
    result['optionList'] = []

    # There are four actions available - keys, add, update, and delete.
    # The add requires two arguments or the entry is pretty much useless.
    result['addRequired'] = ['ep_code', 'core_priority']
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires room id and one key/value pair to update.
    result['updateRequired'] = ['ep_code', 'core_priority']
    result['updateMinArgs'] = len(result['updateRequired'])
    # Delete uses the room_id field.
    result['delRequired'] = ['ep_code']
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdInfo, argList):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize tables and set input keys.
    network_info.initialize()
    result['inputKeys'] = network_info.get_endpoint_fields()

    if ( cmdInfo['action'] in ["add", "update"] ):
        # Add requires bldg_code and room
        result['ep_code'] = argList[0][:8]
        result['core_priority'] = int(argList[1])
        result['argsUsed'] += 2
    if ( cmdInfo['action'] == "delete" ):
        # Delete requires one argument - the ep-code.
        result['ep_code'] = argList[0][:8]
        result['argsUsed'] += 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Try and get the building code. If it doesn't exist then
    # we received an invalid building code on the command line.
    bldgInfo = building_info.get_building_info(cmdLineInfo['ep_code'])

    # Build the endpoint from the building and command line info.
    theEndpoint = network_info.new_endpoint()
    theEndpoint['ep_code'] = cmdLineInfo['ep_code']
    if ( "core_priority" in cmdLineInfo ):
        # If provided, set the core priority.
        theEndpoint['core_priority'] = cmdLineInfo['core_priority']
    # the endpoint zone always matches the building zone.
    theEndpoint['net_zone'] = bldgInfo['net_zone']

    # Check to see if this endpoint already exists.
    try:
        dbEndpoint = network_info.get_endpoint_info(cmdLineInfo['ep_code'])
        if ( cmdLineInfo['action'] == "add" ):
            print("Unable to add:")
            print("The endpoint " + cmdLineInfo['ep_code'] +
                    " already exists.")
            sys.exit(-1)
    except NetException as err:
        if ( err.name == "EpNotFound" and 
                cmdLineInfo['action'] in ["update", "delete"] ):
            print("Unable to " + cmdLineInfo['action'] + ":")
            print(err.detail)
            sys.exit(-2)
    except:
        # Not sure what went wrong, raise so we can debug it.
        raise

    # Now perform the requested action.
    if ( cmdLineInfo['action'] == "add" ):
        epInfo = network_info.add_endpoint(theEndpoint)
        print("The endpoint has been added:")
    elif ( cmdLineInfo['action'] == "update" ):
        # update a row in the db.
        epInfo = network_info.update_endpoint(theEndpoint)
        print("The endpoint has been updated:")
    elif ( cmdLineInfo['action'] == "delete" ):
        # delete a row from the db.
        network_info.delete_endpoint(cmdLineInfo['ep_code'])
        print("The endpoint " + cmdLineInfo['ep_code'] + " has been deleted.")

    try:
        print("Endpoint Code: " + epInfo['ep_code'])
        print("Network Zone: " + str(epInfo['net_zone']))
        print("Core Priority: " + str(epInfo['core_priority']))
    except:
        # The above causes an error when an endpoint is deleted.
        pass

## end main()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

