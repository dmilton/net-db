#
# Version information for this command.
#
g_version='01 Oct 2014 - 3.0.2'

#  4 Jun 2014   Update for new column names which identify table somewhat.
#  7 Jun 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--link=remote-code] bldg-code vlan-tag vlan-name
    --help          display this help message
    --version       show version information and exit.
    --link=x        vlan is linked to building x.
    bldg-code       the building code to add a vlan to.
    vlan-tag        the vlan tag to use for the vlan.
    vlan-name       the name to use for the vlan.
    """ % ( gCommand, gCommand ))
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = False
    # Default action.
    result['action'] = "addVlan"

    # Command line options:
    result['optionList'] = [ ("l:", "link=") ]

    # Required arguments:
    result['requiredArgs'] = ["bldg_code", "vlan_tag", "net_name"]
    result['minArgs'] = len(result['requiredArgs'])

    # Remote code is required because vlans go in the network_remote
    # table. By default they aren't connected to another building but
    # in some cases could be. We default to the "00" building.
    result['remote_code'] = '00'

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We have three positional arguments.
    result['bldg_code'] = argList[0]
    result['vlan_tag'] = int(argList[1])
    result['net_name'] = argList[2]
    result['argsUsed'] = 3

    # If there is a problem with an argument:
    # raise ValueError, ("kind of value error")

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Nothing to do here.
    
    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize building and network tables.
    building_info.initialize()
    network_types.initialize()
    network_info.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Verify the building code is valid.
    bldgInfo = building_info.get_building_info(cmdLineInfo['bldg_code'])

    # VLAN's are of type "bridge"
    typeInfo = network_types.get_type_info("bridge")[0]

    # Get a new vlan template:
    vlanInfo = network_info.new_network(typeInfo)

    # Check the remote  code if it isn't 00.
    if ( cmdLineInfo['--link'] != None ):
        # This will fail if the remote code is invalid.
        rmtBldgInfo = building_info.get_building_info(cmdLineInfo['--link'])
        vlanInfo['remote_code'] = rmtBldgInfo['bldg_code']
    else:
        vlanInfo['remote_code'] = "00"


    # Set the values of our new vlan.
    vlanInfo['net_type'] = typeInfo['net_type']
    vlanInfo['net_name'] = cmdLineInfo['net_name']
    vlanInfo['vlan_tag'] = cmdLineInfo['vlan_tag']
    vlanInfo['bldg_code'] = cmdLineInfo['bldg_code']

    # Now add the VLAN.
    result = network_info.add_vlan(vlanInfo, typeInfo)

    # Let the user know everything is done.
    print("The VLAN has been added to the database.")

## end main()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

