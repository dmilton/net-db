# Version information for this command.

g_version='17 Sep 2014 - 3.0.4'

# 23 Jun 2014   If a router MAC address has not been added to a database
#               entry then it will be None. Rather than test for None,
#               simply bracketed the variable with str() to convert.
#  6 Jun 2014   Comparison of a string to 'None' in any way other than
#               == or != does not work in python3 (ie no > or <)
#  2 May 2013   add --auth flag so a search can be made by auth code.
# 12 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import router_afl
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] [--version] router
    --help        display this help message
    --version        show version information and exit.
    --auth        argument is an auth code rather than a router name.
    router        the host name of the router.
    """ % ( gCommand ) ))
    sys.exit(exit_code)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    # default action.
    result['action'] = 'list'

    result['optionList'] = [ ("a", "auth" ), ("s", "serial") ]

    result['requiredArgs'] = ["auth_identifier"]
    result['minArgs'] = 1

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmd_line_info - any command line flags and default options thus far.
# arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmd_line_info, arg_list):

    result = {}

    # We only have one positional argument.
    result['auth_identifier'] = arg_list[0]
    result['argsUsed'] = 1

    # If there is a problem with an argument:
    # raise ValueError, ("kind of value error")

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(the_options):

    # Check tags in the_options to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError, ("invalid option combination")
    if ( the_options['--auth'] and the_options['--serial'] ):
        raise CommandError("--auth and --serial are mutually exclusive.")

    return(the_options)
## end verify_options()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # initialize the networking AFL table.
    router_afl.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    if ( cmd_line_info['--auth'] ):
        # find license by auth code.
        aLicense = router_afl.find_afl_info(
                cmd_line_info['auth_identifier'], "auth_code")
        cmd_output.print_one_license(aLicense)
    elif ( cmd_line_info['--serial'] ):
        # Find license by serial number.
        aLicense = router_afl.find_afl_info(
                cmd_line_info['auth_identifier'], "serial_nbr")
        cmd_output.print_one_license(aLicense)
    else:
        # Find all the licenses for this router.
        router = cmd_line_info['auth_identifier']
        licenseList = router_afl.get_afl_info(router)
        if ( len(licenseList) == 0 ):
            print("Authorization Codes and Licensese have not been assigned to")
            print("this router. Use ")
            print(("\tassign-afl " + router))
            print("to assign auth codes and once the licenses have been")
            print("created, activate them using")
            print("\tactivate-afl LICENSE-KEY-FILE")
        else:
            afl_title = "Virtual chassis components of " + router
            afl_head = ("VC","Model","Serial","MAC")
            afl_keys = ["member_id","switch_model","serial_nbr","lan_mac"]
            cmd_output.print_table(afl_title, afl_head, afl_keys, licenseList)

            notActivated = False
            for aLicense in licenseList:
                if ( aLicense['license_key'] == None ):
                    notActivated = True

            if ( notActivated == True ):
                print("An Auth Code has been assigned but not activated.")
                print("Run assign-afl to obtain the Auth Codes and follow")
                print("the instructions found there.")

            print("\nLicense Keys:")
            for aLicense in licenseList:
                if ( aLicense['license_key'] != None ):
                    if ( len(aLicense['license_key']) > 0 ):
                        print((aLicense['license_key']))

        ## end if ( len(licenseList) == 0 ):

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
