# Version information for this command.

g_version='02 May 2013 - 1.0.0'

# 13 May 2014   Qualify the NetException handling.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 25 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import db_columns
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--update] column-name description
usage: %s --delete column-name
    --help              display this help message
    --version           show version information and exit.
    --update            update description for the table column. (default)
    --delete            delete the description associated with the table column.
    table-name          the name of the table
    column-name         the name of the column
    description         description for the specified column.
    """ % ( gCommand, gCommand, gCommand ))
    sys.exit(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # Assume the default is to display/list information.
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"  # read/update access.

    result['setArgs'] = True
    result['action'] = "add"

    result['optionList'] = []

    result['addRequired'] = ["column_name", "column_descr"]
    result['addMinArgs'] = len(result['addRequired'])

    result['updateRequired'] = ["column_name", "column_descr"]
    result['updateMinArgs'] = len(result['updateRequired'])

    result['delRequired'] = ["column_name"]
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0
    if ( len(argList) >= 1 ):
        cmdInfo['column_name'] = argList[0]
        result['argsUsed'] += 1

    # Add and update also require the column description.
    if ( cmdInfo['action'] in ["add", "update"] and len(argList) == 2 ):
        cmdInfo['column_descr'] = argList[1]
        result['argsUsed'] += 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize the table.
    db_columns.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    if ( cmdLineInfo['action'] == 'delete' ):
        try:
            toDelete = {}
            toDelete['column_name'] = cmdLineInfo['column_name']
            db_columns.delete_column(toDelete)
            print("The column description has been deleted.")
        except NetException as err:
            if ( err.name == 'not found' ):
                print("The column description was not in the database.")
                print((err.detail))
            else:
                raise
    elif ( action in ["add", "update"] ):
        # create a record from cmdLineInfo to use for add/update.
        theDescr = db_columns.new_db_column()
        theDescr['column_name'] = cmdLineInfo['column_name']
        theDescr['column_descr'] = cmdLineInfo['column_descr']

        try:
            if ( cmdLineInfo['action'] == "add" ):
                dbRecord = db_columns.add_column(theDescr)
                print("The column has been added:")
            else:
                dbRecord = db_columns.update_column(theDescr)
                print("The column has been updated:")
            # print the license we have.
            cmd_output.print_column(dbRecord)
        except NetException as err:
            if ( err.name == "duplicate" ):
                print((err.detail))
            elif ( err.name == "not found" ):
                print((err.detail))
            else:
                raise
        except:
            raise

    ## end elif ( action in ["add", "update"] ):

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
