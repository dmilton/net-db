# Version information for this command.

g_version='24 Sep 2014 - 3.0.4'

# 24 Mar 2014   Remove call to check_name since it was preventing
#               the 2nd type from being used.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 26 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries
try:
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s [--detail] [net-type]
    --help          display this help message
    --version       show version information and exit.
    --detail        show type information with usage description.
    net-type        the name of the type to display information about.
    """ % ( gCommand, gCommand )))
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Set values for getopt - comman line options.
    result['optionList'] = [ ("d", "detail") ]

    # Minimum number of arguments.
    result['minArgs'] = 0
    result['requiredArgs'] = []

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = 'list'

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0
    # We have one optional positional argument.
    if ( len(argList) >= 1 ):
        result['net_type'] = argList[0]
        result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError, ("invalid option combination")

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize any tables here.
    network_types.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Obtain our list of types.
    theType = ""
    if ( "net_type" in cmdLineInfo ):
        theType = cmdLineInfo['net_type']
    typeList = network_types.get_type_info(theType)

    if ( not cmdLineInfo['--detail'] ):
        cmd_output.print_types(typeList)
    else:
        showBrief = len(typeList) > 1
        for aNetType in typeList:
            cmd_output.print_one_type(aNetType, showBrief)

    return
## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
