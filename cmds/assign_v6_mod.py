# Version information for this command.

g_version='19 Sep 2013 - 1.0.0'

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 14 Apr 2014   Most of the original functionality has been integrated into
#               assign-nets. This command now allows assignment of IPv6
#               address spaces to VLAN entries which already have IPv4.
# 18 Jun 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
    import network_zones
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#\
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] | [--version]
usage: %s [--info] bldg-code vlan-tag
    --help          display this help message
    --version       show version information and exit.
    --info          show what would take place, don't actually do anything.
    bldg-code       the building code to add a network to.
    vlan-tag        vlan tag to add an IPv6 network to.
    """ % ( gCommand, gCommand )))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # Assume the default is to assign networks.
    result['setArgs'] = False
    result['action'] = 'assign'

    result['optionList'] = [ ("i", "info") ]

    result['requiredArgs'] = ["bldg_code", "vlan_tag"]
    result['minArgs'] = 2

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    if ( len(argList) == cmdInfo['minArgs'] ):
        result['bldg_code'] = argList[0]
        result['vlan_tag'] = int(argList[1])
        result['argsUsed'] = 2
    else:
        result['argsUsed'] = 0

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(commandInfo, userDefaults):

    # initialize any tables here.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()
    network_zones.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Verify we have a valid building code.
    bldgInfo = building_info.get_building_info(commandInfo['bldg_code'])

    # Get the information for the zone.
    zoneInfoList = network_zones.get_range_info(
            bldgInfo['net_zone'], ipVersion=6)

    # Loop through the list to get the 'user' space.
    for aZone in zoneInfoList:
        if ( aZone['net_type_list'] == '' ):
            # empty type list means good for anything.
            zoneInfo = aZone
        elif ( 'user' in aZone['net_type_list'] ):
            # type list defined explicitly for 'user' space.
            zoneInfo = aZone
            break

    # Do what we are here for.
    if ( commandInfo['action'] == 'add-loop' ):

        # Create the new loopback address.
        newLoop = network_info.calc_v6_loop(bldgInfo, zoneInfo)

        print(("Added " + newLoop['net_type'] + " network: " \
                + newLoop['net_name'] + ", " + str(newLoop['network']) \
                + ", vlan " + str(newLoop['vlan_tag'])))

    elif ( commandInfo['action'] == 'add-link' ):

        # Verify the endpoint code and get the info.
        epInfo = network_info.get_endpoint_info(commandInfo['ep_code'])

        # Create the new link.
        newLink = network_info.add_v6_link(bldgInfo['net_zone'],
                bldgInfo['bldg_code'], epInfo)

        print(("Added " + newLink['net_type'] + " network: " \
                + newLink['net_name'] + ", " + str(newLink['network']) \
                + ", vlan " + str(newLink['vlan_tag'])))

    elif ( commandInfo['action'] == 'assign' ):

        # Do we have a base address assigned to the building?
        if ( bldgInfo['ipv6_netid'] < 0 ):
            # No, get the next one for this zone.
            bldgZoneId = network_zones.get_next_bldg(zoneInfo)
            bldgInfo['ipv6_netid'] = bldgZoneId
            bldgInfo['ipv6_nextnet'] = 0
            building_info.update_building(bldgInfo)

            # Add the router loopback address.
            network_info.add_v6_loop(bldgInfo['ipv6_netid'], bldgInfo['bldg_code'])

            # Add links from the router into the core.
            epList = network_info.get_endpoints(bldgInfo['net_zone'])
            for anEp in epList:
                network_info.add_v6_link(bldgInfo['net_zone'],
                        bldgInfo['bldg_code'], anEp)

        # Build a list of IPv6 types defined.
        typeInfoList = network_types.get_type_info(ipVersion=6)
        typeList = []
        for aType in typeInfoList:
            if ( aType['version'] == 6 ):
                typeList.append(aType['net_type'])

        # Get our list of networks assigned to the building.
        rawNetList = network_info.get_networks(bldgInfo['bldg_code'])

        # Remove any network that's not IPv6.
        netInfoList = []
        for aNet in rawNetList:
            if ( aNet['net_type'] != 'iso' and aNet['network'] != None ):
                if ( aNet['network'].version == 6 ):
                    netInfoList.append(aNet)

        # Get the list of types we already have.
        haveTypes = []
        for aNet in netInfoList:
            haveTypes.append(aNet['net_type'])

        # Now subtract "what we have" from "what we want" to
        # determine the list of types we still need.
        needTypes = list ( set(typeList) - set(haveTypes) )

        # Show what we found already.
        for aNet in netInfoList:
            print(("Found " + aNet['net_type'] + " network: " \
                + aNet['net_name'] + ", " + str(aNet['network']) \
                + ", vlan " + str(aNet['vlan_tag'])))

        # Add what is required and missing.
        newNetList = []
        for aType in needTypes:
            if ( aType == 'link-v6' ):
                # shouldn't need to here!
                print("Adding a link - shouldn't this already be done?")
            elif ( aType == 'loop-v6' ):
                # bla bla bla too
                print("Adding a loopback address should be done already!")
            elif ( aType == 'user-v6' ):
                print("need a user network")
                userNet = network_info.new_network(aType)
                userNet['net_name'] = bldgInfo['bldg_code'] + '-user' + \
                        aType['extension']
                userNet['net_type'] = aType['net_type']
                userNet['network'] = network_info.calc_range_net(
                        zoneInfo['network_range'], 64,
                        bldgInfo['ip_netid'], bldgInfo['ipv6_nextnet'])
                if ( ('vlan_tag' in commandInfo) == True ):
                    vlanStart = commandInfo['vlan_tag']
                else:
                    vlanStart = aType['vlan_tag']
                userNet['vlan_tag'] = \
                    network_info.get_next_vlan(bldgInfo['bldg_code'], vlanStart, 6)
                if ( userNet['vlan_tag'] != vlanStart ):
                    raise ValueError("The vlan tag " + str(vlanStart) + \
                            " already has an IPv6 network assigned.")

        # Now add a network if the vlan tag was requested.

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

