# Version information for this command.

g_version='09 Apr 2014 - 1.0.2'

# 13 May 2014   Qualify the NetException handling.
# 20 Feb 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
from socket import gethostbyaddr, getaddrinfo

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)


#\
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] [--version] bldg-code
    --help         display this help message
    --version      show version information and exit.
    bldg-code      the building code to check DNS entries for.
    On MacOS use 'sudo dscacheutil -flushcache' to flush the DNS cache.
    """ % ( gCommand ) ))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# command_options()
# This routine returns a dictionary containing two lists.
# The lists are used to build the strings required for command line
# processing by the main wrapper.

def command_options():

    result = {}
    result['short'] = ""
    result['long'] = []
    result['minArgs'] = 1

    return(result)
## end command_options()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['action'] = 'list'

    return(result)
## end default_options()

#
##############################################################################
# ProcessOption
# This routine must process the command specific options one at a time
# and return any dictionary values which Main will use for processing.

def process_option(option, argument):

    raise ValueError("unhandled option:" + str(option))

    return(result)
## end process_option()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    result['bldg_code'] = argList[0]

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError("invalid option combination")

    return()
## end verify_options()

#
##############################################################################
# Main

def main(commandInfo):

    # initialize any tables here.
    try:
        building_info.initialize()
        network_info.initialize()
    except db_access.DbException as err:
        db_access.db_error_handler(err)

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Make sure the building code is valid:
    try:
        bldgInfo = building_info.get_building_info(commandInfo['bldg_code'])
    except NetException as err:
        if ( err.name == "UnknownBuildingCode" ):
            print("Unable to find the building code: " +
                    commandInfo['bldg_code'] )
            sys.exit(2)
        else:
            raise
    except:
        raise

    # Get the list of networks in the building.
    try:
        netList = network_info.get_networks(commandInfo['bldg_code'])
    except NetException as err:
        if ( err.name == "NoNetworksDefined" ):
            print(err.detail)
            sys.exit(3)
        else:
            raise
    except:
        raise

    # Loop through every network and perform DNS lookups on each address.
    for aNetwork in netList:
        if ( aNetwork['net_type'] not in ['iso','bridge'] ):
            ipList = []
            #print("checking network: " + str(aNetwork['network']) )
            prefixLength = aNetwork['network'].prefixlen
            try:
                theNetworkIP = aNetwork['network'].network_address
            except AttributeError:
                theNetworkIP = aNetwork['network'].ip
            # Figure out what address (or addresses) to check.
            if ( prefixLength in ( 32, 128 )):
                # For a /32 or /128 network, check only that address.
                ipList.append(theNetworkIP)
            elif ( prefixLength in ( 30, 112 )):
                # For a /30 or /112 network, check the first two addresses.
                ipList.append(theNetworkIP + 1)
                ipList.append(theNetworkIP + 2)
            else:
                # For all other networks, just the vlan interface for now.
                ipList.append(theNetworkIP + 1)
            # Now do the checking
            for anIP in ipList:
                checkResult = LookupHostInfo(anIP)
                # Now print something:
                if ( checkResult['match'] ):
                    msg = checkResult['hostname'] + " has address "
                    msg += checkResult['ip']
                elif ( checkResult['rev-check'] == False ):
                    msg = "### "
                    msg += "IP " + checkResult['ip'] + " not found."
                elif ( checkResult['fwd-check'] == False ):
                    msg = "### "
                    msg += checkResult['ip'] + " has name "
                    msg += checkResult['hostname']
                    msg += "\nbut " + checkResult['hostname'] + " not found."
                print(msg)

## end main()

#
##############################################################################
# Local function definitions...

##############################################################################
# VerifyHostInfo
# Given an ip address, determine the host name and all associated
# IP addresses for the original IP received.
#
# Entry: ipaddress.ip_address(x)
#
# Exit: dictionary containing the following:
#   rev-lookup: result of gethostbyaddr
#   fwd-lookup: result of getaddrinfo
#   hostname: string    fqdn lookup of IP address.
#   match: boolean      forward and reverse fqdn's match
#   rev-check:          reverse lookup succeeded
#   fwd-check:          forward lookup succeeded
#

def LookupHostInfo(theIPAddress):

    #
    result = {}

    ipToCheck = str(theIPAddress)
    result['ip'] = ipToCheck

    # Begin by attempting to resolve the IP address to a host name.
    revLookupGood = True
    try:
        result['rev-lookup'] = gethostbyaddr(ipToCheck)
        result['hostname'] = result['rev-lookup'][0]
    except:
        revLookupGood = False

    # Now try and resolve the host name into an IP address (or list).
    fwdLookupGood = True
    if ( revLookupGood ):
        try:
            result['fwd-lookup'] = getaddrinfo(result['hostname'],None)
        except:
            fwdLookupGood = False
    else:
        fwdLookupGood = False

    # Now compare the results:
    # revLookup:    ('qab1.net.umanitoba.ca',
    #                   ['76.40.179.130.in-addr.arpa'], ['130.179.40.76'])
    # fwdLookup:     [ (30, 2, 17, '', ('2620:0:bd0::2a', 0, 0, 0)),
    #                   (30, 1, 6, '', ('2620:0:bd0::2a', 0, 0, 0)),
    #                   (2, 2, 17, '', ('130.179.40.76', 0)),
    #                   (2, 1, 6, '', ('130.179.40.76', 0))]
    # Pure IPv4 addresses:
    # revLookup:    ('qab1-voip.net.umanitoba.ca',
    #                   ['1.1.30.172.in-addr.arpa'], ['172.30.1.1'])
    # fwdLookup:    [ (2, 2, 17, '', ('172.30.1.1', 0)),
    #                   (2, 1, 6, '', ('172.30.1.1', 0))]
    lookupMatch = False
    if ( revLookupGood and fwdLookupGood ):
        for oneTuple in result['fwd-lookup']:
            anIP = oneTuple[4][0]
            if ( anIP == ipToCheck ):
                lookupMatch = True

    result['match'] = lookupMatch
    result['rev-check'] = revLookupGood
    result['fwd-check'] = fwdLookupGood

    return(result)
## end def LookupHostInfo():

# vi: syntax=python ts=4 sw=4 tw=78 showmatch et :
