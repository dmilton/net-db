# Version information for this command.

g_version="dd mmm yyyy - 1.0.0"

# 20 Jun 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    # No NetTrackDB libraries required in this routine.
    pass
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s domain-name
    --help          display this help message
    --version       show version information and exit.
    --file          argument is a file containing a list of domains.
    domain-name     the domain name to retrieve NS records for.
    """ % ( gCommand, gCommand ) ))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# command_options()
# This routine returns a dictionary containing two lists.
# The lists are used to build the strings required for command line
# processing by the main wrapper.

def command_options():

    result = {}
    result['short'] = "f"
    result['long'] = ["file"]
    result['minArgs'] = 1

    return(result)
## end command_options()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['action'] = 'lookup'
    result['file'] = False

    return(result)
## end default_options()

#
##############################################################################
# ProcessOption
# This routine must process the command specific options one at a time
# and return any dictionary values which Main will use for processing.

def process_option(option, argument):

    result = {}

    if option in ("-f", "--file"):
        result['file'] = True
    else:
        raise ValueError("unhandled option:" + str(option))
    ## end if

    return(result)
## end process_option()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    if ( cmdInfo['file'] ):
        result['input-file'] = argList[0]
    else:
        result['domain-name'] = argList[0]

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError("invalid option combination description")

    return
## end verify_options()

#
##############################################################################
# Main

def main(commandInfo):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Create a list of domain names to check.
    if ( "domain-name" in commandInfo ):

        nsInfo = get_ns_records(commandInfo['domain-name'])
        print(nsInfo)
        
    else:
        # Open the file.
        try:
            inFile = open(commandInfo['input-file'], 'r')
        except FileNotFoundError as msg:
            print(msg)
            sys.exit(-2)
        # Read lines one at a time, checking each as we go.
        inputLines = 0
        for aLine in inFile:
            domainName = aLine.strip()
            nsInfo = get_ns_records(domainName)
            print(nsInfo)

## end main()

#
##############################################################################
# Local function definitions...

def get_ns_records(domainName):
    if ( domainName > '' ):
        question = DNS.DnsRequest(name=domainName, qtype="NS")
        answer = question.req()
        result = answer.show()
    else:
        result = ""

    return(result)
## end def get_ns_records()


# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
