# Version information for this command.

g_version='dd mmm yyyy - 1.0.0'

#  6 Dec 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import console_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#\
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version | --keys]
usage: %s [--flag] digi-mac-addr room-id-nbr
usage: %s --update digi-mac-addr key key-value
usage: %s --delete digi-mac-addr
    --help              display this help message
    --version           show version information and exit.
    --keys              display the update keys for the command.
    --update            update record in the table.
    --delete            remove record from the table.
    --code              use two letter digi device code instead of MAC.
    digi-mac-addr       MAC address of the digi to update/delete.
    room-id-nbr         id of the room digi is installed in (bldg-info output)
    """ % (gCommand, gCommand, gCommand, gCommand)))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options:
    result['longOptions'] = ['code']
    result['shortOptions'] = ""

    # There are four actions available - keys, add, update, and delete.
    # Add requires the digi MAC and room id digi is installed in.
    result['addRequired'] = ['digi_mac', 'room_id']
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires digi MAC and one key/value pair to update.
    result['updateRequired'] = ['digi_mac']
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete uses the room_id field.
    result['delRequired'] = ['digi_mac']
    result['delMinArgs'] = len(result['delRequired'])
    # Assume the modifier is not active. Note the above should not have
    # the double hyhpen while the value below should have. If the flag
    # above is on the command line the value below will be 'not'ed.
    for aMod in result['longOptions']:
        # If the option has '=' in it, the value is not boolean.
        cmdLineFlag = '--' + aMod
        if ( cmdLineFlag[-1] == "=" ):
            cmdLineFlag = cmdLineFlag[:-1]
            result[cmdLineFlag] = None
        else:
            result[cmdLineFlag] = False

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdInfo, argList):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize tables and set input keys.
    console_info.initialize()
    result['inputKeys'] = console_info.GetDigiFields()

    # count arguments, depending upon task, arguments are different.
    argCount = len(argList)

    if ( cmdInfo['action'] in ['update', 'delete'] ):
        # Delete requires one argument - the digi_mac.
        result['digi_mac'] = argList[0]
        result['argsUsed'] += 1
    elif ( cmdInfo['action'] == 'add' ):
        # Add requires digi_mac and room_id
        result['digi_mac'] = argList[0]
        result['room_id'] = int(argList[1])
        result['argsUsed'] += 2

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(commandInfo, userDefaults):

    # we use the buildings tables here too, initialize them.
    building_info.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Create a digi dictionary with our command arguments.
    digiInfo = console_info.new_digi()
    for aKey in list(digiInfo.keys()):
        if ( aKey in cmdLineInfo ):
            digiInfo[aKey] = cmdLineInfo[aKey]
        else:
            del digiInfo[aKey]

    # Now handle what the user gave us on the command line.
    if ( cmdLineInfo['action'] == 'add' ):
        # Try and get the building room. If it doesn't exist then
        # we received an invalid room id on the command line.
        roomInfo = building_info.get_room_info(digiInfo['room_id'])
        # Check that we have digi_code, digi_ip, and server_id.
        # Compute the values if necessary.
        if ( ('digi_code' in digiInfo) == False ):
            digiInfo.update(console_info.GetNextDigiInfo())
        if ( ('server_id' in digiInfo) == False ):
            digiInfo['server_id'] = 1
        elif ( digiInfo['server_id'] == None ):
            digiInfo['server_id'] = 1
        # We have a valid room, try and add the digi.
        digiInfo = console_info.AddDigi(digiInfo)[0]
        portList = console_info.AddPorts(digiInfo)
    elif ( cmdLineInfo['action'] == 'update' ):
        # update a row in the db.
        digiInfo = console_info.UpdateDigi(digiInfo)
    elif ( cmdLineInfo['action'] == 'delete' ):
        # delete a row from the db.
        console_info.DeleteDigi(digiInfo)

    if ( cmdLineInfo['action'] in ("add", "update") ):
        print("The digi has been added/updated:")
        console_info.PrintDigi(digiInfo)
        if ( cmdLineInfo['action'] == 'add' ):
            console_info.PrintDigiPorts(portList)
    else:
        print("The digi (and ports) have been removed from the database.")

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

