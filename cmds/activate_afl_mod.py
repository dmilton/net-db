# Version information for this command.

g_version='01 Oct 2014 - 3.0.5'

#  6 Jun 2014   From python2 to 3 conversion, missed translate call.
#               Rewrite section to find serial numbers and authorizations
#               from the license key file.
# 23 Sep 2013   Add code to conditionally debug after init done.
#  5 Apr 2013   Change arguments to just the license file name since
#               everything required is in the file.
# 12 Mar 2013   Update for new command template functionality.
# 18 Dec 2012   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB libraries:
try:
    import router_afl
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s license-key-file
    --help              display this help message
    --version           show version information and exit.
    license-key-file    name of the key file Juniper generated.
    """ % ( gCommand, gCommand ) )
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = False
    # Default action.
    result['action'] = "activate"

    # Command line options:
    result['optionList'] = []

    # required arguments and minimum cli argument count:
    result['requiredArgs'] = ['license_key']
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We have two positional arguments.
    result['license_key'] = argList[0]
    result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize the AFL tables:
    router_afl.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Take the file name and extract the serial number.
    try:
        keyFile = ReadKeyFile(cmdLineInfo['license_key'])
    except IOError:
        print(("Unable to open file: " + cmdLineInfo['license_key']))
        sys.exit(1)
    ## end try


    # grab the serial number and authorization code.
    keyFileLines = keyFile.split('\n')
    for aLine in keyFileLines:
        # grab what we want.
        if ( "Serial" in aLine ):
            cmdLineInfo['serial_nbr'] = aLine.split(':')[1].strip()
        elif ( "Authorization" in aLine ):
            authCode = aLine.split(':')[1].strip()

    if ( ('serial_nbr' in cmdLineInfo) == False ):
        print("Unable to extract a serial number from the key file.")
        print("Is this a Juniper license file?")
        sys.exit(3)
    ## end if ( cmdLineInfo.has_key('serial_nbr') == False ):

    aflInfo = router_afl.find_afl_info(cmdLineInfo['serial_nbr'])
    if ( aflInfo == None ):
        print("That serial number does not have this auth code assigned.")
        print("This may indicate database corruption. Please report this")
        print("so any issues may be resolved.")
        sys.exit(2)
    else:
        # mark the auth code as activated.
        aflInfo['auth_active'] = True
        # read the license file.
        aflInfo['license_key'] = keyFile
        dbAflRecord = router_afl.update_license(aflInfo)[0]
        # Print instructions for user.
        print("The database has been updated to record the license key.")
        print(("please login to the router " + aflInfo['router']))
        print("and run the request system license command:")
        print("> request system license add terminal")
        print((dbAflRecord['license_key']))
    ## end else if ( aflInfo == None ):

## end main()

#
##############################################################################
# Local function definitions...

def ReadKeyFile(keyPath):
    """
    Read the file in its entirety and assign content to the result.
    """

    result = ''

    fp = open(keyPath,"r")
    while 1:
        line = fp.readline()
        if not line: break
        result += line
    ## end while 1:

    return result
## end ReadKeyFile():

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

