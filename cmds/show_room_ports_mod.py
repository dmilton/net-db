# Version information for this command.

g_version='06 Jan 2015 - 3.0.4'

#  6 Jan 2014   After conversion to new command line processing, check
#               that there are enough args and ignore any extras.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 24 Apr 2013   Hybrid building/desktop routers would not work because
#               the management network is empty. Needed the loopback
#               address for the router instead. Changed the networks
#               routine to get_mgmt_info which retrieves both the management
#               and loopback information for the building.
# 15 Apr 2013   New template initialization setup.
# 13 Mar 2013   First take at conversion to new database routines.
# 31 May 2012   Updated to use vlan_tag instead of number for db table.
#  6 Feb 2011   Crashed when no result found in database. Fixed to
#               provide a useful diagnostic message.
# 16 Aug 2010   Add VerifyOptions function.
# 21 May 2009   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import socket

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries
try:
    import building_info
    import network_info
    import netdisco
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s bldg-code room-number
    --help              display this help message
    --version           show version information and exit.
    bldg-code           the building code to find communications outlets for.
    room-nbr            the room number in the building to find ports for.
    """ % ( gCommand, gCommand )))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking", "netdisco"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # this command wants add, update, delete, and keys actions?
    result['setArgs'] = False
    # Default action.
    result['action'] = "find"

    result['optionList'] = []

    result['requiredArgs'] = ["bldg_code", "room_nbr"]
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):
    result = {}

    result['argsUsed'] = 0

    # We have two positional arguments.
    if ( len(argList) >= 2 ):
        result['bldg_code'] = argList[0]
        result['room_nbr'] = argList[1]
        result['argsUsed'] = 2

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise CmdException, ("invalid option combination")

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(commandInfo, userDefaults):

    # initialize tables.
    building_info.initialize()
    network_info.initialize()
    netdisco.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Lookup basic information about the building.
    try:
        bldgInfo = building_info.get_building_info(commandInfo['bldg_code'])
        print(("Building: " + bldgInfo['bldg_name']))
        print(("Campus: " + bldgInfo['campus_name']))
    except:
        raise
    ## end try

    # Find the management network for the building and router loopback
    # address for the building.
    mgmtInfo = network_info.get_mgmt_info(commandInfo['bldg_code'])

    # Find the ports within the building with matching description.
    commOutletList = []
    for aNet in mgmtInfo:
        commOutletList += \
            netdisco.find_room_ports(aNet['network'], commandInfo['room_nbr'])

    nbrCommOutlets = len(commOutletList)
    if ( nbrCommOutlets == 0 ):
        print("No switch port descriptions matched the value provided.")
    else:
        for anOutlet in commOutletList:
            # Convert the switch IP to a switch name.
            try:
                fqdnSwitch = socket.gethostbyaddr(str(anOutlet['ip']))[0]
                switchName = fqdnSwitch.split('.')[0]
            except socket.herror:
                # DNS lookup failed, use IP.
                switchName = str(anOutlet['ip'])
            except:
                raise
            anOutlet['ip'] = switchName
    
        # Now print our outlet list.
        roomTitle = ""
        roomHead = ("Switch", "Port", "Description", "State")
        roomKeys = ["ip", "port", "name", "up"]
        cmd_output.print_table(roomTitle, roomHead, roomKeys, commOutletList)
    ## end else if ( nbrCommOutlets == 0 ):

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

