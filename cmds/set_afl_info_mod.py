# Version information for this command.

g_version='17 Sep 2014 - 3.0.3'

# 16 Jun 2014   Update for new column that identifies the type of
#               license (afl_model) that the auth code applies to.
#               Rewrite to use the set-cmd-template.
# 13 May 2014   Qualify the NetException handling.
# 23 Sep 2013   Add code to conditionally debug after init done.
#  5 Apr 2013   Change arguments to just the license file name since
# 11 Apr 2013   Add check for length of authorization code to reject
#               codes which are likely incorrect.
# 05 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import router_afl
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version | --keys]
usage: %s auth-code rtu-serial afl-model [key value]...
usage: %s --update auth-code key key-value [key2 value2]...
usage: %s --delete auth-code
    --help          display this help message
    --version       show version information and exit.
    --keys          list input keys for updating all fields in the table.
    --update        update keys associated with auth-code x.
    --delete        remove the auth code from the table.
    auth-code       the authorization code for license.
    rtu-serial      the RTU Serial Number for this auth code.
    afl-model       the 'model' of the AFL auth code
    Default action is to add a new auth code. Duplicates will generate
    an error without doing anything.
    """ % ( gCommand, gCommand, gCommand, gCommand )))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # Assume the default is to display/list information.
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    result['setArgs'] = True
    result['action'] = "add"
    result['optionList'] = []

    # For the four actions, keys, add, update, and delete:
    # To add a new row to the table we require the auth_code, rtu_serial,
    # and the afl_model information.
    result['addRequired'] = ['auth_code', 'rtu_serial', 'afl_model']
    result['addMinArgs'] = len(result['addRequired'])
    # To update a now in the table we require the auth_code and the
    # values to update.
    result['updateRequired'] = ['auth_code']
    result['updateMinArgs'] = len(result['updateRequired']) + 2
    # Delete requires the auth_code itself.
    result['delRequired'] = ['auth_code']
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}


    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize our afl table so we know what input fields are valid.
    router_afl.initialize()
    result['inputKeys'] = router_afl.get_fields()

    if ( cmdInfo['action'] in ['update', 'delete'] ):

        # Delete requires one argument - the auth_code.
        # The first argument is always the auth code.
        result['auth_code'] = argList[0]
        result['argsUsed'] = 1

    elif ( cmdInfo['action'] == 'add' ):

        # Add requires auth_code, rtu_serial, and afl_model.
        result['auth_code'] = argList[0]
        if ( len(result['auth_code']) != 19 ):
            raise ValueError("invalid authorization code.")
        result['rtu_serial'] = argList[1]
        result['afl_model'] = argList[2]
        result['argsUsed'] = 3

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # Turn cmdLineInfo into a router_afl record dictionary
    # and an action to perform.
    action = cmdLineInfo['action']

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Now handle what the user gave us on the command line.
    if ( action in ["add", "update"] ):
        # starting with a db record, which is all underscores,
        # translate underscores into hyphens so either can be used.

        # create a record from cmdLineInfo to use for add/update.
        newAfl = router_afl.new_license()
        for aKey in list(newAfl.keys()):
            aKey2 = aKey.translate(str.maketrans('_','-'))
            if ( aKey in cmdLineInfo ):
                newAfl[aKey] = cmdLineInfo[aKey]
            elif ( aKey2 in cmdLineInfo ):
                newAfl[aKey] = cmdLineInfo[aKey2]
            else:
                del newAfl[aKey]
        ## end for aKey in newAfl.keys():

        try:
            if ( cmdLineInfo['action'] == 'add' ):
                dbRecord = router_afl.add_license(newAfl)
                dumpFlag = False
                print("The license has been added:")
            else:
                dbRecord = router_afl.update_license(newAfl)
                print("The license has been updated:")
                dumpFlag = True
            # print the license we have.
            cmd_output.print_licenses(dbRecord, dumpFlag)
        except NetException as err:
            if ( err.name == "duplicate" ):
                print("This AFL auth code already exists.")
                print((err.detail))
            elif ( err.name == "not found" ):
                print("Unable to find auth_code for update.")
                print((err.detail))
            else:
                raise
        except:
            raise
    elif ( action == 'delete' ):
        try:
            toDelete = {}
            toDelete['auth_code'] = cmdLineInfo['auth_code']
            router_afl.delete_license(toDelete)
            print("The license has been deleted.")
        except NetException as err:
            if ( err.name == 'license not found' ):
                print("Unable to delete license as it was not in the database.")
                print((err.detail))
            else:
                raise

    ## end elif ( action in ["add", "update"] ):

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

