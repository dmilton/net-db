# Version information for this command.

g_version='12 Dec 2014 - 3.0.11'

# show-bldg-info script. Returns information from the database regarding
# the building code provided as argument.

# 12 Dec 2014   Moved print function for voice/data rooms to the buildings
#               module so it can be used by add-digi which checks rooms.
# 18 Jun 2014   Update for new building name column name. Broke the --name
#               flag by changing name to bldg_name for the action.
# 13 May 2014   Qualify the NetException handling.
# 13 Feb 2014   Start using space formatting rather than tabs and changed
#               default to expand tabs. This conforms more closely to the
#               python standard. Now to change away from camel case for
#               function calls.
# 23 Jan 2014   Move printing of IPv6 base network to print_buildings
#               in the buildings module.
#  4 Dec 2013   Add output of the room id when listing the v/d rooms.
#  7 Nov 2013   Move list_networks into networks module so it can be
#               used by other commands.
# 25 Sep 2013   Add code to display a buildings ipv6 allocation
#               and next network id when one has been assigned.
#               Remove paranthesis around userid and switch order
#               in assigned field to make the field shorter.
# 21 Jun 2013   Changed schema for ipv6 variables and now need the
#               network_types table for generating the ISO network.
# 14 Jun 2013   Changed schema so prime_net is only on the network_bldgs
#               table. This caused issues in the display code because
#               it used to be on all tables.
# 12 Jun 2013   Update usage to be easier to read. Fix so the two letter
#               building code is no longer mentioned since up to 8 are
#               now allowed. Correct grammer issues in the help section.
#  1 May 2013   Make afl display list only when there are licenses assigned.
# 19 Apr 2013   Invalid building codes cause failure, correctly handle
#               exception returned by building library.
# 28 Mar 2013   Dates are now datetime objects.
# 28 Mar 2013   Table initialization errors were not referencing the
#               db_access module. The network type field is renamed to
#               net_type.
#  6 Mar 2013   Now using .pgpass so remove password from db init call.
# 27 Feb 2013   remove reference to typeid as that field no longer exists.
#               remove code to convert typeid to a type name as the name is
#               now used directly.
# 21 Feb 2013   rewrite to use new database schema and network2 library.
# 13 Nov 2012   Start using buildings, db_access.
# 12 Oct 2012   Add AFL license information for this building.
# 31 Jul 2012   Add ISO address when an IPv6 address is defined for
#               the loopback interface.
# 31 May 2012   Add output for base IPv6 network and only display
#               the core building flag if it is true. Reformat to
#               use only tabs instead of tabs and spaces combined.
# 22 Feb 2012   Start using the buildings module for retrieving
#               information about the building itself.
# 21 Feb 2012   Add new command line flag to permit finding the
#               building information by the name of the building.
# 16 Aug 2010   Add verify_options function.
# 13 Jul 2010   Added user name to assigned output column and
#               the building code itself in parenthesis after the name
#               in the building output line.
# 11 Mar 2010   Added a command line flag to just output the name
#               of a building.
# 27 Jan 2010   Changed to produce correct output when a network
#               is null in the table - ie. a vlan without an
#               associated IP address range.
# 30 Nov 2009   Added flag to output to indicate when a network
#               is the primary building network.
# 12 Nov 2009   Changed import path to /opt2/local/etc.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to handle exceptions.
# 26 Jun 2009   Add --devel hidden option.
# 18 Jun 2009   Change description field to be the allocated date.
#  5 Jun 2009   Add ospf area to building details output.
#  2 Dec 2008   Fixed tracebacks which occur when there are no
#               networks assigned to a building.
# 15 Sep 2008   Removed reference to db_access since it is not
#               required here. Changed building info check to
#               handle exceptions.
# 22 Aug 2008   Changed to use new GetNetworks format of having
#               a list of network dictionary entries.
#  8 Aug 2008   fixed problem with empty webdir generating spurious
#               output on the Web directory line.
#               fix problem where MDC not correctly identified.
#               change module library from networking to network.
#  7 Aug 2008   fixed problem where a building with no v/d rooms
#               defined generated tracebacks on the debug message.
# 10 Jul 2008   initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import network_info
    import building_info
    import router_afl
    import network_types
    import network_zones
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help | --version
usage: %s --find name-fragment
usage: %s [--name] [--net-zone] building-code
    --help          display this help message
    --version       show version information and exit.
    --name          output only the full building name.
    --nets          output only the assigned networks.
    --net-zone      output only the zone id for this building.
    --find          search by a fragment of the building name.
    name-fragment   a fragment of the building name to search for.
    building-code   the building code.
    """ % ( gCommand, gCommand, gCommand ) ))
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.

def default_options():
    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"  # read only access.

    # command implements add, update, delete, keys?
    result['setArgs'] = False
    # Default action.
    result['action'] = "list"

    result['optionList'] = []
    result['optionList'].append( ("f", "find") )
    result['optionList'].append( ("N", "name") )
    result['optionList'].append( ("z", "net-zone") )
    result['optionList'].append( ("n", "nets") )

    # Minimum arguments:
    result['requiredArgs'] = ["bldg_search_value"]
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

##############################################################################
# process_positional()
# This routine must process the list positional command line arguments.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    result['bldg_search_value'] = argList[0]
    result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    try:
        # initialize the building table handlers.
        building_info.initialize()
        # initialize the network table handlers.
        network_info.initialize()
        # initialize the network AFL table handlers.
        router_afl.initialize()
        # initialize the network types table.
        network_types.initialize()
    except db_access.DbException as err:
        db_access.db_error_handler(err)
        sys.exit(0)
    ## end try
    
    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    if ( cmdLineInfo['--find'] ):
        find_buildings(cmdLineInfo['bldg_search_value'])
    else:
        try:
            bldgInfo = building_info.get_building_info(
                            cmdLineInfo['bldg_search_value'])
        except NetException as err:
            if ( err.name == "UnknownBuildingCode" ):
                print(("Unable to find building code: " 
                    + cmdLineInfo['bldg_search_value']))
                sys.exit(2)
            else:
                raise
            ## end else if ( err.name == "UnknownBuildingCode" )
        except:
            raise
        ## end try
        if ( cmdLineInfo['--nets'] ):
            network_info.list_networks(cmdLineInfo['bldg_search_value'])
        elif ( cmdLineInfo['--name'] ):
            print(bldgInfo['bldg_name'])
        elif ( cmdLineInfo['--net-zone'] ):
            print(str(bldgInfo['net_zone']))
        else:
            list_bldg_info(bldgInfo)

## end main()

##############################################################################
# Insert any local function definitions here.

def list_bldg_info(bldg_info):
    # If the building has IPv6 defined, get the range.
    if ( bldg_info['ipv6_netid'] > 0 ):
        zone_info = network_zones.get_range_info(
                bldg_info['net_zone'], 'user', 6)[0]
        bldg_alloc = network_zones.calc_range_net(
                zone_info['network_range'], 64, bldg_info['ipv6_netid'], 0)
    else:
        bldg_alloc = ""
    # List the basic building information.
    cmd_output.print_building(bldg_info, bldg_alloc)

    # List the voice data rooms in the building.
    vd_rooms = building_info.get_vd_rooms(bldg_info['bldg_code'])
    cmd_output.print_vd_rooms(vd_rooms)

    # List any AFL licenses assigned to the building router.
    if ( bldg_info['bldg_router'] > '' ):
        list_afl_licenses(bldg_info['bldg_router'])

    try:
        network_info.list_networks(bldg_info['bldg_code'])
    except network_error.NetException as err:
        banner = "############################################################"
        if ( err.name == "NoNetworksDefined" ):
            print(banner)
            print(("# " + err.detail))
            if ( bldg_info['bldg_router'] > '' ):
                allBldgs = building_info.find_router_buildings(
                    bldg_info['bldg_router'])
                for aBldg in allBldgs:
                    if ( aBldg['bldg_code'] != bldg_info['bldg_code'] ):
                        print("# The same router was found for building code "
                            + aBldg['bldg_code'])
                        print(banner)
                        foundNets = network_info.list_networks(
                                aBldg['bldg_code'])
            else:
                print(banner)
        else:
            raise
    ## end try:

## end def list_bldg_info(bldg_info):

############################

def list_afl_licenses(router):
    if ( router > '' ):
        # Get the Advanced Feature License (AFL) information.
        afl_info_list = router_afl.get_afl_info(router)
        if ( len(afl_info_list) > 0 ):
            # List any active AFL codes as such.
            for an_afl in afl_info_list:
                if ( an_afl['auth_active'] == True ):
                    an_afl['auth_code'] = 'active'
            afl_title = "Advanced Feature Set Licenses assigned to " + router
            afl_head = ("Serial Number", "Member ID", "Auth Code")
            afl_keys = ("serial_nbr", "member_id", "auth_code")
            cmd_output.print_table(afl_title, afl_head, afl_keys, afl_info_list)

## end def list_afl_licenses(bldgCode):

############################

def find_buildings(name_fragment):
    # Get a list of buildings which have nameFragment in them.
    try:
        bldg_list = building_info.find_building_code(name_fragment);
    except network_error.NetException as err:
        if ( err.name == "UnknownBuildingCode" ):
            print("Unable to find a building named: " + name_fragment)
            sys.exit(2);
        else:
            raise
        ## end if ( err.name == "UnknownBuildingCode" ):
    except:
        raise;
    ## end try

    bldg_title = "Building(s) found:"
    bldg_head = ("Bldg Code", "Name")
    bldg_keys = ("bldg_code", "bldg_name")
    cmd_output.print_table(bldg_title, bldg_head, bldg_keys, bldg_list)
## end def find_buildings()

# vim: syntax=python ts=4 sw=4 showmatch et :

