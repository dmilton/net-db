#
# Version information for this command.
#
g_version='30 Sep 2014 - 1.0.9'

# 17 Jun 2014   Update for changed column - name to net_name.
#  4 Jun 2014   Update usage and allow setting the type to '' so all
#               types can be shown.
# 26 Mar 2014   Fix usage to reflect recent changes to the code and the
#               documentation in the man page.
# 30 Jan 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
	import network_info
	import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--net-type=X] [--net-zone=Y] [cidr-net-size]
    --help          display this help message
    --version       show version information and exit.
    --net-type=X    the network type to list (default is user). See
                    show-net-types for the types defined. Using a type
                    of '' will show all types.
    --net-zone=Y    restrict output to networks from zone Y.
    cidr-net-size   the size of network to restrict to (default is all).
    """ % ( gCommand, gCommand ) )
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = 'list'

    # Command line options:
    result['optionList'] = [ ("t:", "net-type="), ("z:", "net-zone=") ]

    # Required positional arguments:
    result['requiredArgs'] = []
    result['minArgs'] = 0

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0
    # First argument is the size of network.
    if ( len(argList) > 0 ):
        result['cidr_size'] = int(argList[0])

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    if ( theOptions['--net-type'] == None ):
        theOptions['--net-type'] = "user"

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize any tables here.
    networks.initialize()
    network_types.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Setup some default values.
    if ( "cidr_size" not in cmdLineInfo ):
        cidrSize = 32
    else:
        cidrSize = cmdLineInfo['cidr_size']

    netZone = userDefaults['net_zone']
    if ( cmdLineInfo['--net-zone'] != None ):
        netZone = int(cmdLineInfo['--net-zone'])
    if ( netZone == None ):
        netZone = -1

    if ( "--net-type" not in cmdLineInfo ):
        netType = "user"
    else:
        netType = cmdLineInfo['--net-type']

    # Make sure the type provided is valid.
    typeInfo = network_types.get_type_info(netType)

    # Get the list of free networks.
    freeNets = networks.get_free_networks(cidrSize, netType, netZone)

    # Print our table.
    netTitle = "Free Networks:"
    netHead = ("Name", "VLAN", "Network", "Type", "Zone")
    netKeys = ("net_name", "vlan_tag", "network", "net_type", "net_zone")
    cmd_line_io.print_table(netTitle, netHead, netKeys, freeNets)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
