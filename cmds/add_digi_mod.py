#
# Version information for this command.
#

g_version='dd mmm yyyy - 1.0.0'

# 19 Nov 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import subprocess

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB libraries:
try:
    import building_info
    import network_zones
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print( """
usage: %s --help | --version
usage: %s bldg-code room-nbr digi-count digi-type digi-mac
    --help              display this help message
    --version           show version information and exit.
    --info              show what changes would take place.
    bldg-code           building code the digi is being installed in.
    room-nbr            room number where the digi is installed.
    digi-count          count of this DIGI unit in this voice/data room.
    digi-type           model of digi (el16 or el32) being installed.
    digi-mac            mac address of digi being installed.
    """ % (gCommand, gCommand) )
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"] # [ "netdisco", "intermapper"]
    result['dbUserClass'] = "look"  # user

    # this command wants add, update, delete, and keys actions?
    result['setArgs'] = False
    # Default action.
    result['action'] = "add"

    # Command line options:
    result['optionList'] = [ ("i", "info") ]
    result['requiredArgs'] = ["bldg_code", "digi_room", "digi_count",
            "digi_type", "digi_mac"]
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

##############################################################################
# process_option
# This routine must process the command specific options one at a time
# and may return anything in default_options such a change in action.

def process_option(option, argument):

    result = {}

    # Handle options that change the action.
    if ( option == "--info" ):
        result['action'] = 'info'
    else:
        # Fail here, caller handles these options.
        raise NameError(option + " is not defined")

    return(result)
## end process_option()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmdLineInfo - all command line flags and options processed thus far.
#    argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmdLineInfo, argList):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    if ( len(argList) == 5 ):
        result['bldg_code'] = argList[0]
        result['digi_room'] = argList[1]
        result['digi_count'] = int(argList[2])
        result['digi_type'] = argList[3]
        result['digi_mac'] = argList[4]
        result['argsUsed'] = 5

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Constants for SMF service and properties:
    cons_smf_fmri = "svc:/site/conserver"
    cons_opt_server = "options/server"
    def_console_server = "/usr/local/sbin/conserver"
    cons_opt_config = "options/configuration_file"
    def_cons_config = "/usr/local/etc/conserver.cf"
    def_cons_log_dir = "/var/conserver/logs"

    dhcp_smf_fmri = "svc:/site/isc-dhcpd:default"
    dhcp_opt_server = "options/server"
    def_dhcp_server = "/usr/local/sbin/dhcpd"
    dhcp_opt_config = "options/configuration_file"
    def_dhcp_config = "/etc/dhcpd.conf"
    dhcp_config_dir = "/var/dhcpd"

    realport_admin_tool = "/opt/realport/drpadmin"
    realport_config_file = "/opt/realport/drp.conf"

    # Initialize the tables:
    building_info.initialize()
    network_zones.initialize()

    # Verify the building code.
    bldg_info = building_info.get_building_info(cmd_line_info['bldg_code'])

    # Verify the voice/data room.
    roomFound = False
    vd_room_list = building_info.get_vd_rooms(cmd_line_info['bldg_code'])
    for a_room in vd_room_list:
        if ( a_room['room'] == cmd_line_info['digi_room'] ):
            roomFound = True
            break

    if ( not roomFound ):
        print("The room " + cmd_line_info['digi_room'] + " was not found.")
        if ( len(vd_room_list) > 0 ):
            print("The following rooms are defined for this building:")
            cmd_output.print_vd_rooms(vd_room_list)
        print("If the room is correct, you can add it with the command:")
        addRoom = cmd_line_info['bldg_code'] + " " + cmd_line_info['digi_room']
        print("set-room-info " + addRoom)
        print("If this is the main voice/data room for the building then")
        print("you should add the --mdc argument:")
        print("set-room-info --mdc " + addRoom)
        raise ValueError("Invalid room " + cmd_line_info['digi_room'])

    # digi name:
    digi_name = cmd_line_info['bldg_code'] + "c"
    digi_name += cmd_line_info['digi_room'] + "-" 
    digi_name += str(cmd_line_info['digi_count'])

    # Verify the type of digi.
    if ( cmd_line_info['digi_type'] == "el16" ):
        digi_ports = 16
    elif ( cmd_line_info['digi_type'] == "el32" ):
        digi_ports = 32
    else:
        print("DIGI type must be one of 'el16' or 'el32'")
        raise ValueError("Unsupported DIGI type " + cmd_line_info['digi_type'])
    
    # verify the MAC address.
    cmd_line_input.check_mac_addr(cmd_line_info['digi_mac'])

    # Now get the information we need from SMF:
    conserver_server = get_smf_property(cons_smf_fmri,
            cons_opt_server, def_console_server)
    cons_config = get_smf_property(cons_smf_fmri,
            cons_opt_config, def_cons_config)
    cons_config_dir = os.path.dirname(cons_config)

    # Figure out where the DIGI configuration should go:
    digi_config_file = cons_config_dir + "/config/" + \
            cmd_line_info['bldg_code'] + ".cf"

    dhcp_server = get_smf_property(dhcp_smf_fmri,
            dhcp_opt_server, def_dhcp_server)
    dhcp_config = get_smf_property(dhcp_smf_fmri,
            dhcp_opt_config, def_dhcp_config)
    dhcp_config_file = dhcp_config_dir + "/cons-" + \
            cmd_line_info['digi_type'] + ".conf"

    realport_admin_tool = "/opt/realport/drpadmin"
    realport_config_file = "/opt/realport/drp.conf"

    # Figure out the digi code and index in the config file.
    digi_code, digi_nbr = get_next_digi(realport_config_file)

    # Create a DHCP configuration entry and restart the service.
    add_dhcp_host_entry(digi_name, cmd_line_info['digi_mac'],
            dhcp_config_file, dhcp_smf_fmri)

    # Tell the realport drivers about the new DIGI.
    # A side effect of this is to create serial device nodes.
    add_realport_config(realport_admin_tool, digi_name, digi_ports, digi_code)

    # Create a conserver configuration entry and restart the service.
    add_console_server_config(cons_config, digi_config_file, cons_log_dir,
            digi_ports,
            cmd_line_info['bldg_code'], digi_name, digi_code)
    #add_console_server_config(cons_config, digi_config_file, console_log_dir,
    #    campus, bldg_name, bldg_code, digi_ports, digi_name, digi_code):

    # setup09:~> drpadmin
    # Please select an option (a)dd (d)elete (s)how (r)eset (q)uit : a
    # Enter the IP address or network name of the unit: test.cons.umanitoba.ca
    # Enter the number of ports: 16
    # Enter the tty device ID (only 2 chars allowed) : zz
    # Would you like this RealPort session to be encrypted?
    # NOTE: Not all RealPort products support encrypted RealPort sessions.
    # Please check your RealPort product's firmware release notes
    # or product literature before selecting "always".
    # If in doubt, select "never".
    # (always/never) : (never): never
    # The following device will be configured,
    # 262     test.cons.umanitoba.ca  16      zz      secure (never)
    # Is this correct (y to add or x to abort) ? y
    # Please select an option (a)dd (d)elete (s)how (r)eset (q)uit : q

    print("append the following to: " + realport_config_file)
    print(str(digi_nbr) + "\t" +
            cmd_line_info['digi_name'] + ".cons.umanitoba.ca\t" + 
            str(digi_ports) + "\t" + digi_code + "\t771\tnever\t1027")

## end main()

#
##############################################################################
# Local function definitions...

def check_sudo():
    # Call sudo with the -v option to see if the user is allowed.
    print("Cannot check sudo yet")
    return
## end check_sudo():

def get_next_digi(realport_config_file):

    letters = "abcdefghijklmnopqrstuvwxyz"

    # exec this: cut -f4 /opt/realport/drp.conf | sort | tail -1
    cmd = "cut -f4 " + realport_config_file + " | sort | tail -1",
    last_digi_code = subprocess.Popen(cmd, shell=True,
            universal_newlines=True, stdout=subprocess.PIPE).stdout.read()

    cmd = "cut -f1 " + realport_config_file + " | tail -1",
    last_digi_nbr = subprocess.Popen(cmd, shell=True,
            universal_newlines=True, stdout=subprocess.PIPE).stdout.read()

    first = last_digi_code[0]
    second = last_digi_code[1]

    if ( last_digi_code[1] == "z" ):
        first = letters[letters.index(last_digi_code[0]) + 1]
        second = "a"
    else:
        second = letters[letters.index(last_digi_code[1]) + 1]

    digi_code = first + second
    digi_nbr = int(last_digi_nbr) + 1

    return(digi_code, digi_nbr)
## end get_next_digi():

def get_smf_property(smf_fmri, property_name, default_value):
    cmd = "/usr/bin/svcprop -p " + property_name + " " + smf_fmri
    value = subprocess.Popen(cmd, shell=True,
            universal_newlines=True, stdout=subprocess.PIPE).stdout.read()

    if ( value == "" or value == '""' ):
        result = default_value
    else:
        result = value

    # Try to convert result to an integer.
    try:
        x = int(result)
        result = x
    except ValueError:
        # The result is a string.
        pass

    return(result)
## end get_smf_property()

def add_dhcp_host_entry(digi_name, digi_mac, dhcp_config_file, dhcp_service):

    print("Adding DHCP server configuration:")
    hostEntry = "host " + digi_name + " {\n"
    hostEntry += "\thardware ethernet " + digi_mac + ";\n"
    hostEntry += "\toption host-name \"" + digi_name + "\";\n"
    hostEntry += "\tddns-hostname \"" + digi_name + "\";\n"
    hostEntry += "}\n"
    print(hostEntry)

    f = open(dhcp_config_file, "w")
    f.seek(0,2) # go to the end of the file.
    f.write(hostEntry)
    f.close()

    restart_cmd = "sudo /usr/sbin/svcadm refresh " + dhcp_service
    print("restarting the dhcp service:")
    print("  " + restart_cmd)
    ignore = subprocess.Popen(restart_cmd, shell=True,
            universal_newlines=True, stdout=subprocess.PIPE).stdout.read()

## end add_dhcp_host_entry

def add_console_server_config(digi_config_file, console_log_dir,
        campus, bldg_name, bldg_code, digi_ports, digi_name, digi_code):

    print("updating the console server config: " + digi_config_file)
    config_add = ""
    # Check to see if the console server config file exists.
    if ( os.path.isfile(digi_config_file) == False ):
        print("This is a new building, setting defaults")
        master_server = "master_1"
        log_names = console_log_dir + "/" + bldg_code + "/&.console"
        # master_server = "master_1b"
        # Create the defaults clause at the start of the file.
        config_add = 37 * "# " + "#\n"
        config_add += "# Consoles for " + bldg_name + "\n"
        config_add += 37 * "# " + "#\n#\n"
        # Default for switches/routers
        config_add += "default " + bldg_code + " {\n" 
        config_add += "\tinclude " + master_server + ";\n"
        config_add += "\trw " + campus + "NetAdmin;\n"
        config_add += "\tinclude serial;\n"
        config_add += "\tlogfile " + log_names + ";\n"
        config_add += "}\n"
        # Default for access points.
        config_add += "default " + bldg_code + "-ap {\n" 
        config_add += "\tinclude " + master_server + ";\n"
        config_add += "\trw " + campus + "WirelessAdmin;\n"
        config_add += "\tinclude serial;\n"
        config_add += "\tlogfile " + log_names + ";\n"
        config_add += "}\n"
        # Default for wireless sensors.
        config_add += "default " + bldg_code + "-s {\n" 
        config_add += "\tinclude " + master_server + ";\n"
        config_add += "\trw " + campus + "SensorAdmin;\n"
        config_add += "\tinclude serialSensor;\n"
        config_add += "\tlogfile " + log_names + ";\n"
        config_add += "}\n"

    # Identify the DIGI itself.
    config_add += 37 * "# " + "#\n"
    config_add += "# Etherlite " + digi_ports + " - " + digi_name
    config_add += " serial code '" + digi_code + "'\n"
    config_add += 37 * "# " + "#\n#\n"

    # Create template entries for the ports themselves.
    for port_nbr in list(range(2,digi_ports)):
        config_add += "# Port " + str(port_nbr) + " on EtherLite '" 
        config_add += digi_name + " (" + digi_code + ")\n" 
        config_add += "#console port" + str(port_nbr).zfill(2) + " {\n"
        config_add += "#\tinclude " + bldg_code + ";\n"
        config_add += "#\tdevice /dev/dty/" + digi_code 
        config_add += str(port_nbr).zfill(3) + "s;\n#}\n"

    # Create or append to the file.
    f.open(digi_config_file, "w")
    f.seek(0,2)
    f.write(config_add)
    f.close()

## end add_console_server_config

def add_realport_config(realport_admin_tool, digi_name, digi_ports, digi_code):
    import pexpect

    # Start an expect session to the drpadmin tool:
    drpadmin = pexpect.spawn("sudo " + realport_admin_tool)

    # milton@jed:~> drpadmin
    # Please select an option (a)dd (d)elete (s)how (r)eset (q)uit : a
    drpadmin.expect("Please .*: ")
    drpadmin.sendline("a")

    # Enter the IP address or network name of the unit:
    drpadmin.expect("Enter the .* network name .*: ")
    drpadmin.sendline(digi_name + ".cons.umanitoba.ca")

    # Enter the number of ports:
    drpadmin.expect("Enter .* ports: ")
    drpadmin.sendline(str(digi_ports))

    # Enter the tty device ID (only 2 chars allowed) :
    drpadmin.expect("Enter the tty device .*: ")
    drpadmin.sendline(digi_code[:2])

    # Would you like this RealPort session to be encrypted?
    # NOTE: Not all RealPort products support encrypted RealPort sessions.
    # Please check your RealPort product's firmware release notes
    # or product literature before selecting "always".
    # If in doubt, select "never".
    # (always/never) : (never):
    drpadmin.expect("(always/never) : (never): ")
    drpadmin.sendline("never")

    # The following device will be configured,
    # 262 test.cons.umanitoba.ca  16  zz  secure (never)
    # Is this correct (y to add or x to abort) ?
    drpadmin.expect("Is this correct .* ")
    drpadmin.sendline("y")

    # Please select an option (a)dd (d)elete (s)how (r)eset (q)uit :
    drpadmin.expect("Please .*: ")
    drpadmin.sendline("q")

## end add_realport_config

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

