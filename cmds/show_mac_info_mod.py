# Version information for this command.

g_version='12 Dec 2014 - 3.0.3'

# This script accepts one argument - a MAC address.
# From the MAC address it will try and determine where on campus that
# MAC address has been located, what IP addresses that have been assigned
# and any network login sessions associated with that MAC address.

# 22 Oct 2015   If a MAC address is not in the database the command
#               simply exits quietly. Change this to produce a message
#               indicating that the MAC address was not found.
# 26 Nov 2013   Missing return statement in VerifyOptions.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 11 Mar 2013   Rewrite for new database routines and moved netdisco
#               database specific operations into netdisco module.
#               Add brief flag.
# 22 Nov 2010   check_mac_addr moved from network_input to cmd_line_input.
# 16 Aug 2010   Add VerifyOptions function.
#               Add --partial flag to allow looking for a partial
#               MAC address.
# 29 Jul 2010   Used network_input module to allow input of both
#               forms of MAC address and do a bit of checking
#               on the MAC address provided.
# 5 May 2010    Converted to modular command. Fixed bug in the
#               host function when an IP doesn't resolve to anything.
# 12 Nov 2009   Changed import directory to /opt2/local/etc.
#  6 Oct 2009   With development/production server implementation
#               the dbServer variable was not being set correctly.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to send bug output to me.
# 20 Jan 2009   Changed command line processing so debug, version,
#               and help do not use short command line flags.
# 29 Oct 2008   Fixed database queries for new PgQuery.
# 10 Jun 2008   Removed debug, host and used standard include
#               modules instead.
# 12 Mar 2008   put into production.
# 10 Mar 2008   initial writing for netdisco database.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import netdisco
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] [--version] | [--partial] mac-address
    --help        display this help message
    --version        show version information and exit.
    --brief        show single line output
    --partial        search for a partial mac address.
    mac-address        the MAC address of the device to find information on.
    """ % ( gCommand )))
    sys.exit(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking", "netdisco"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information verbosely.
    result['setArgs'] = False
    # default action.
    result['action'] = 'list'

    result['optionList'] = [ ("b", "brief"), ("p", "partial") ]

    result['requiredArgs'] = ["mac-address"]
    result['minArgs'] = 1

    return(result)
## end default_options()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise CommandError, ("invalid option combination")
    return(theOptions)
## end verify_options()

##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}
    result['argsUsed'] = 0

    # We only have one positional argument, the mac address.
    theMacAddr = argList[0]
    if ( cmdInfo['--partial'] == True ):
        hexChars = '0123456789abcdef'
        theMacAddr = theMacAddr.lower()
        macAddrBytes = theMacAddr.split(':')
        lowerMacAddr = macAddrBytes[0]
        for i in range(1,len(macAddrBytes)):
            lowerMacAddr += ":" + macAddrBytes[i]
        upperMacAddr = lowerMacAddr
        for i in range(len(macAddrBytes), 6):
            lowerMacAddr += ":00"
            upperMacAddr += ":ff"
        result['mac-address'] = cmd_line_input.check_mac_addr(lowerMacAddr)
        result['high-mac'] = cmd_line_input.check_mac_addr(upperMacAddr)
    else:
        result['mac-address'] = cmd_line_input.check_mac_addr(theMacAddr)
    ## end if ( argList['partial'] == True ):

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(command_info, user_defaults):

    # initialize any tables here.
    netdisco.initialize()

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    brief_flag = command_info['--brief']
    # Look for switch port locations...
    if ( command_info['--partial'] == False ):
        mac_address = command_info['mac-address']
        # Get the list of ports this MAC address has been seen on.
        port_list = netdisco.find_node_entries(mac_address, mac_address, brief_flag)
        if ( len(port_list) == 0 ):
            print("The MAC address " + mac_address +
                " was not found in the database.")
        else:
            netdisco.print_switch_ports(port_list, False)
            # Now figure out what IP addresses the MAC has used.
            arp_entries = netdisco.find_arp_entries(mac_address, "")
            for an_arp_entry in arp_entries:
                netdisco.print_arp_entry(an_arp_entry, brief_flag)
    else:
        # Find only active ports for a range of MAC addresses.
        port_list = netdisco.find_node_entries(command_info['mac-address'], \
                command_info['high-mac'], True)
        netdisco.print_switch_ports(port_list, brief_flag)
    ## end if ( command_info['partial'] == False ):

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
