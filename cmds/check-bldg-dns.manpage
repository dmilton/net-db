.\"
.\"	check-bldg-dns.l,v 1.0 2014-02-20 09:27:26 milton
.\"
.TH check-bldg-dns "February 20, 2014" "1.0" "check building dns"
.UC 6
.SH NAME
check-bldg-dns \- check building DNS.
.SH SYNOPSIS
.B check-bldg-dns --help
.P
.B check-bldg-dns --version
.P
.B check-bldg-dns bldg-code
.P

.SH DESCRIPTION
This command will extract the list of networks for a building and then
perform checks on all of the networks to ensure the router interfaces
and some of the special networks have appropriately defined host names.

.PP
The only argument is
.I bldg-code
which is the building code to check the DNS entries for.

.SH OPTIONS
.TP 8 8
.BI --help
Display a summary of help/usage information.
.TP 8 8
.BI --version
List the revision information of this command.
.TP 8 8

.PP
.SH EXAMPLES
.B check-bldg-dns qa
.nf
qab1.net.umanitoba.ca has address 130.179.40.76
qab1.net.umanitoba.ca has address 2620:0:bd0::2a
qab1-mgmt.net.umanitoba.ca has address 192.168.1.1
qab1-user.cc.umanitoba.ca has address 130.179.1.1
qab1-user-2nd.net.umanitoba.ca has address 172.31.1.1
qab1-v42.cc.umanitoba.ca has address 130.179.3.1
qab1-v42-2nd.net.umanitoba.ca has address 172.31.3.1
qab1-v42.cc.umanitoba.ca has address 2620:0:bd0:c000::1
qab1-sray.net.umanitoba.ca has address 192.168.1.129
qab1-voip.net.umanitoba.ca has address 172.30.1.1
qab1-prot.net.umanitoba.ca has address 130.179.191.177
enrouter-qab1.net.umanitoba.ca has address 130.179.41.49
qab1-enrouter.net.umanitoba.ca has address 130.179.41.50
enrouter-qab1.net.umanitoba.ca has address 2620:0:bd0::2a:1
qab1-enrouter.net.umanitoba.ca has address 2620:0:bd0::2a:2
mhrouter-qab1.net.umanitoba.ca has address 130.179.41.53
qab1-mhrouter.net.umanitoba.ca has address 130.179.41.54
mhrouter-qab1.net.umanitoba.ca has address 2620:0:bd0::2b:1
.PP
.SH DIAGNOSTIC OUTPUT
.PP
If an invalid building code is provided, an error is generated and
the command exits. For any network which doesn't have valid router
interface DNS entries the output will indicate what was found as
well as any differences between what the forward and reverse lookup
as. All checks begin with the reverse entry so if there
are no reverse entries, nothing can be verified.

