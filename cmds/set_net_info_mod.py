# Version information for this command.

g_version='09 Jan 2015 - 3.0.16'

#  9 Jan 2015   Update of fields from the allocated table were not being
#               included when an existing network is being assigned.
#  6 Jan 2015   Updates of fields that were part of the free table would
#               be eliminated because the allocated table was being used
#               as the template for the network. Need a merge of both.
# 15 Dec 2014   process_positional was generating a misleading exception
#               when an --assign argument was prodived. Fixed the logic
#               since --assign is valid for both add and --update.
#  4 Sep 2014   Similar to free-net and show-net-info, make the default
#               argument list for add to start with the CIDR network
#               value. Change the normal network behavious to also use
#               the CIDR network rather than the network name since in
#               many instances the name is changing. Wasn't always
#               working properly for ISO networks, fixed.
#  4 Sep 2014   When adding to the database a search through the
#               database is not required - the network shouldn't exist.
#               Still need an empty dictionary so allow the same loop
#               to take information from the db (for add, empty, for
#               update, db values) and from the command line and merge
#               them together.
# 29 Aug 2014   Add option to assign a network which potentially involves
#               changing the table. Needed to create a new structure of
#               the correct type so all fields were present and could be
#               verified before the update was made.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 12 May 2014   Fix issues with adding ISO networks and correct problem
#               when updating a network using it's name.
# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
# 20 Feb 2014   When adding networks, the search_key was set but the
#               corresponding value was not being set when processing the
#               command line. This caused an error claiming 'network' was
#               not being provided. The other issue is that a network can
#               be added to the free table or to an already allocated table
#               but when an allocated table is used, the building code may
#               or may not be present. Force this so it is required.
#  5 Feb 2014   When changing the network itself, problems occur in the
#               update since the network is used as a search/update key.
#               Check for this condition so update knows to use the name
#               field as the search/update key.
# 24 Jan 2014   The --cidr argument wasn't working as the caller checked
#               for 'name', regardless. Changed the required value to be
#               the same for all options (includes iso) and then handle
#               the different value appropriately here.
# 19 Dec 2013   When updating a network net_type_info was not extracted from
#               the db unless the type was being changed. This caused
#               problems because the required fields could not be verified.
# 26 Nov 2013   add_network, update_network, and assign_network return lists
#               but print_network expects a dictionary. 
# 23 Oct 2013   Usage doesn't list the update and delete options.
# 18 Oct 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s [--help | --version | --keys]
usage: %s net-cidr net-name net-type bldg-code [key key-value]...
usage: %s --update [--assign] net-cidr key key-value...
usage: %s --update [--assign] --by-name net-name key key-value...
usage: %s --delete net-cidr
    --help          display this help message
    --version       show version information and exit.
    --keys          display the add/update keys for the command.
    --by-name       for updates, the first argument is the network name.
    --update        network specified is to be updated.
    --delete        network specified is to be deleted.
    --assign        should the network be assigned or left potentially free
    net-cidr        network (CIDR form) to add/update/delete.
    net-type        type of network (see type-info)
    net-name        name of the network to add/update/delete.
    bldg-code       building code this network should be placed into.
    key key-value   key and key value pairs (see --keys)
    """ % (gCommand, gCommand, gCommand, gCommand, gCommand))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # command implements add, update, delete, keys?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Allow changing the positional argument type to name instead of network.
    # Actually assign the network while adding/updating.
    result['optionList'] = [ ("b", "by-name"), ("", "assign") ]

    # There are four actions available - keys, add, update, and delete.
    # list of table columns that are mandatory.
    result['addRequired'] = ["network", "net_name", "net_type", "bldg_code"]
    result['addMinArgs'] = len(result['addRequired'])
    # Delete uses the name (or with --cidr) the network field.
    result['delRequired'] = ["net_or_name"]
    result['delMinArgs'] = len(result['delRequired'])
    # Update requires name or --cidr network and one key/value pair to update.
    result['updateRequired'] = ["net_or_name"]
    result['updateMinArgs'] = len(result['updateRequired']) + 2

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmd_line_info - all command line flags and options processed thus far.
#    arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmd_line_info, arg_list):

    # Check that our command line flags make sense.
    if ( cmd_line_info['action'] == "add" ):
        if ( cmd_line_info['--by-name'] ):
            raise ValueError("--by-name not valid for add operation")
    elif ( cmd_line_info['action'] == "delete" ):
        if ( cmd_line_info['--assign'] ):
            raise ValueError("--assign not valid for delete operation")

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize tables and set input keys.
    network_info.initialize()
    result['inputKeys'] = network_info.get_network_fields()

    if ( cmd_line_info['action'] == "add" ):
        result['net_name'] = arg_list[1]
        result['net_type'] = arg_list[2]
        result['bldg_code'] = arg_list[3]
        if ( result['net_type'] == 'iso' ):
            result['network'] = cmd_line_input.check_iso_addr(arg_list[0])
        else:
            result['network'] = ipaddress.ip_network(arg_list[0])
        result['argsUsed'] = 4
    elif ( cmd_line_info['action'] in ["update","delete"] ):
        netArg = arg_list[0]
        if ( cmd_line_info['--by-name'] ):
            result['net_or_name'] = arg_list[0]
            result['requiredArgs'] = ['net_name']
            result['updateRequired'] = ['net_name']
            result['minArgs'] = 3
        else:
            try:
                result['net_or_name'] = ipaddress.ip_network(arg_list[0])
            except ValueError:
                # Doesn't look like an IP address, try ISO.
                result['net_or_name'] = cmd_line_input.check_iso_addr(arg_list[0])
                result['net_type'] = "iso"
        result['argsUsed'] = 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Update and delete need to get the network from the database.
    if ( cmd_line_info['action'] in ["update","delete"] ):
        if ( cmd_line_info['--by-name'] ):
            search_key = "name"
        else:
            search_key = "cidrEqual"
            if ( "net_type" in cmd_line_info ):
                if ( cmd_line_info['net_type'] == "iso" ):
                    search_key = "isoEqual"
        search_value = cmd_line_info['net_or_name']
        # Try and find the network from the database.
        net_info = network_info.get_network_info(search_key, search_value)
        
        # Get our type information
        if ( 'net_type' in cmd_line_info ):
            # Type info provided (or this is an ISO network.
            net_type_info = network_types.get_type_info(
					cmd_line_info['net_type'])[0]
        else:
            # If not provided, get the type info from the network itself.
            net_type_info = network_types.get_type_info(net_info['net_type'])[0]

        # Are we changing the network itself?
        net_changing = False
        if ( 'network' in cmd_line_info ):
            if ( net_info['network'] != cmd_line_info['network'] ):
                net_changing = True
    else:
        # Add just needs the appropriate empty record to populate.
        net_info = {}
        net_type_info = network_types.get_type_info(
				cmd_line_info['net_type'])[0]

    # Add and update will change what is in net_info, delete doesn't
    # care as long as we found the network in the database.
    if ( cmd_line_info['action'] in ["add", "update"] ):
        # At this point we now have a network type. The destination
        # table and fields vary based on type, get a record containing
        # all the correct parts:
        net_to_update = network_info.new_network(net_type_info)

        # If we already have a network in the db then use the keys we
        # got from the database rather than those from the type. This
        # allows updates of fields in the 'free' table as well as in
        # an allocated network.
        if ( net_info == {} or cmd_line_info['--assign'] ):
            # Network is new, not in db or is being assigned.
            # Use keys from the active table.
            net_key_names = list(net_to_update.keys())
        else:
            # Network is in db and just being updated.
            # use keys from the table the network is in.
            net_key_names = list(net_info.keys())

        for a_key in net_key_names:
            # Copy whatever came from the database.
            if ( a_key in net_info ):
                net_to_update[a_key] = net_info[a_key]
            # Replate any database values with the command line input.
            if ( a_key in cmd_line_info ):
                net_to_update[a_key] = cmd_line_info[a_key]

    # Now that we've got a complete and updated net_info record, act on it.
    if ( cmd_line_info['action'] == "add" ):

        updated_net = network_info.add_network(net_to_update, net_type_info)
        print("The network has been successfully added:")

    elif ( cmd_line_info['action'] == "update" ):

        if ( cmd_line_info['--assign'] ):

            # Get the building info for the assignment:
            if ( "bldg_code" in cmd_line_info ):
                # From the command line.
                bldg_code = cmd_line_info['bldg_code']
            else:
                # Or the building code isn't changing.
                bldg_code = net_info['bldg_code']
            bldg_info = building_info.get_building_info(bldg_code)
            # Do the assignment:
            updated_net = network_info.assign_network(
                    net_to_update, net_type_info, bldg_info)
            print("The network has been successfully assigned:")

        else:   # not cmd_line_info['--assign']

            # To avoid issues with the network itself changing _and_ an
            # assignment request, do an update, then assign.
            updated_net = network_info.update_network(
                    net_to_update, net_type_info, net_changing)
            print("The network has been successfully updated:")

    elif ( cmd_line_info['action'] == "delete" ):

        # delete a row from the db.
        network_info.delete_network(net_info, net_type_info['net_family'])
        print("The network has been successfully deleted.")

    # For add and update, print the result of the action.
    if ( cmd_line_info['action'] in ('add','update') ):
        cmd_output.print_network(updated_net)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=72 showmatch et :
