#!/usr/bin/env python3

g_wrap_version='01 Oct 2014 - abcde'

# This is a generic command wrapper. It handles version, debug, help,
# and databasae devel command line arguments. It also handles --keys
# and accepts --update, --delete, and --add for set commands. Any
# command line arguments are processes and placed into a dictionary
# which the comand module itself must handle.
# Positional arguments are the responsibility of the command module.
# Key/key-value pairs are processed here.

# 14 Jun 2015   Add error checking for an incorrect db password.
# 10 Apr 2015   Convert variables to remove camel case convention.
#               Add error message when opening an non-existant database.
#  1 Oct 2014   Bump major version for merged template which supports
#               both show/regular commands and set commands.
# 18 sep 2014   When a set command has an option which changes the
#               action to something which is not part of the default
#               set, min_args was not being set.
# 17 Sep 2014   Move some work out of __main__ to make it easier to read.
#               Reflect code changes in documentation.
#  5 Sep 2014   Minor cleanup and documentation improvements.
# 21 Aug 2014   Debug env variable being set but with no value causes
#               a ValueError exception. Added try clause and default value.
# 11 Aug 2014   Continued development, for a non-set type command.
# 21 Apr 2014   Add checks for db version and create a new version
#               variable that is used for the --version output.
# 18 Mar 2014   Improve import exception handling so the missing module
#               is displayed.
#  6 Feb 2014   Add try block for imports to catch un-installed modules.
#  5 Feb 2014   When an argument provided is determined invalid (ValueError) 
#               after command line processing, exceptions need to be handled
#               and raised.
# 22 Jan 2014   Python3 conversion for maketrans.
# 18 Dec 2013   Update version display code.
# 29 Nov 2013   Add further debug code similar to set-cmd-template.
# 27 Nov 2013   Change argument --devel to --dbserv so environment and
#               command line arguments are consistient. Remove printing
#               of db server line when processing argument, duplicate.
#  5 Nov 2013   Add translation of module names with hyphens to underscores
#               so the modules can be imported and used elsewhere.
# 14 Jun 2013   Improve database error handling and messages.
#  1 May 2013   Add NET_DBSERV environment variable and do some cleanup
#               on the import os routines.
# 30 Apr 2013   Add handling of a NET_DEBUG environment variable so
#               I don't have to use --debug while testing.
# 23 Apr 2013   Make exit code increment more easily with each error.
#               Handle exceptions raised from main()
# 21 Mar 2013   Allow access to database when processing command line
#               arguments. Cannot do options/flags since they may change
#               which database is being accessed.
# 14 Mar 2013   Add more useful diagnostic when .pgpass file is
#               missing or incomplete.
# 12 Mar 2013   Update to permit user module to specify the databases
#               to initialize and have that done in this module.
#  6 Mar 2013   Print interpreter version in version information.
# 12 Feb 2013   Change debug handling so pdb can be used without
#               any output or email crash reports.
# 24 Jan 2013   Change order of priority for module loading so the current
#               directory is checked first. This allows testing before
#               installing the code.
# 21 Jan 2013   Change references to routines in db_access so they
#               are explicitly imported. Fix issues with the database
#               server id so we set the value in process_command_line.
# 16 Jan 2013   Remove sys path append code. Everything is now being
#               installed in the site-local directory.
# 13 Nov 2012   Save db_server_id for use in the command module. This
#               allows various database(s) to be opened as required
#               by the command itself. This needs cleanup
#               when everything is converted for db_access.
# 31 May 2012   Reformat to replace spaces with tabs.
#  2 Sep 2011   Add module to db exceptions.
# 11 Jul 2011   Trap error messages from the database and exit
#               cleanly rather than causing a traceback.
#  7 Feb 2011   Another database instance is running to permit
#               testing of the most recent postgres release. Changed
#               code to allow selection of this instance.
#  3 Sep 2010   Change import of cmd_line_input so routines must be
#               referenced directly. This avoids name collision.
# 31 Aug 2010   Change network_input module to cmd_line_input.
# 17 Aug 2010   Add SystemExit exception handling so any routine
#               can call the usage() function and exit cleanly.
# 16 Aug 2010   Add verify_options function so any options which
#               are mutually exclusive or conflicting can be
#               tested and identified. Moved the traceback handling
#               code into a separate routine so it can be called
#               from two places in __main__. This permits any bugs
#               in the command line processing routines to be shown
#               cleanly rather than just quietly exiting.
# 29 Jul 2010   Handle ValueError exceptions generated by the
#               network_input module.
# 20 Jul 2010   Remove generic command line key/key-value input
#               processor and move to network_input module.
# 16 Jun 2010   Add a generic command line key/key-value input
#               processor for the 'set' commands. Parts may also
#               be used to verify various types of input.
# 26 Mar 2010   Add a check for the correct minimum number of
#               command line arguments to process_command_line.
#  4 Mar 2010   Add exception handling for the command line processing
#               so decent diagnostics get sent back to the user.
#  1 Mar 2010   Add cmdInfo as argument to process_positional so
#               commands which have variable positonal arguments
#               based on option flags can do their work.
# 30 Nov 2009   Add better NetException functionality so an error in
#               normal processing does not cause an exception report
#               and no 'valid' error reporting to the user.
# 10 Sepr 2009  Create a function which looks for modules to load
#               into the global dictionary space. The module is
#               either in the current directory (checked first)
#               or in our global space.
# 14 Aug 2009   New code which abstracts the actual processing
#               in a command from the template. This should allow
#               for easier updates to the template and for the
#               command itself to be compiled rather than as source.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it doesn't fail on normal exits.
#  3 Jul 2009   Improve bug handling by capturing the stack trace
#               and emailing them to me rather than having them
#               show up for the user to see.
#  5 Jun 2009   Added code to handle hidden '--devel' flag which
#               redirects queries to development postgres
#               server on mizar. Made debug option hidden as well.
# 25 Aug 2008   Initial writing.

#
#############################################################################
# The command module itself must implement:
#
# default_options() - must return a dictionary of default behaviours which
#                     the function main() will process.
#       options['dbList'] = list of databases to open/initialize.
#       options['dbUserClass'] = user to access database with.
#       options['action'] = what is the default action to be performed.
#       options['setArgs'] = True -> implement --keys,--add,update,--delete.
#       options['optionList'] = [ ("f", "flag"), ("o:", "opt=") ]
#       When setArgs == False:
#       options['requiredArgs'] = ["a-reqd-arg"]
#       options['minArgs'] = minimum argument count.
#       When setArgs == True:
#       options['addRequired'] = list of keywords required for an add.
#       options['addMinArrgs'] = minimum arguments required for add.
#       options['updateRequired'] = keywords/values required for an update.
#       options['updateMinArgs'] = minimum arguments required for update.
#       options['delRequired'] = keywords/values required for a delete.
#       options['delMinArgs'] = minimum arguments required for delete.
#
# process_positional(cmdInfo, argList) - must process the positional command
#        line options. Raise an exception if the arguments are invalid.
#        Returns a dictionary which main() will process.
# main(commandInfo) - takes the processed command line options and arguments
#        in the form of a dictionary of actions and performs the
#        actions as appropriate.

#
############################################################################
# Global variable setup and library imports.

# Standard Libraries
import os
import sys
import platform
import getopt
import string
import distutils.sysconfig as sysconfig
import pdb

try:
    # error handler for the postgres library.
    from psycopg2 import OperationalError
except ImportError as msg:
    print("Database access requires the psycopg2 library to be installed.")
    print(msg)
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)
## end try

try:

    # Debugging - crash reporting.
    import debug
    # input command line processing routines.
    import cmd_line_input
    # output formatting routines.
    import cmd_output
    # network module exception handler.
    #from network_error import Timeout, NetException, net_error_handler
    import network_error
    # get the basic database server information
    from net_db_config import dbServerInfo
    global g_db_server_info
    g_db_server_info = dbServerInfo
    # database communications routines.
    import db_access
    # database schema version
    import db_schema
    # database table column information
    import db_columns
    # user defaults
    import user_defaults

except ImportError as msg:

    print("The NetDB utilities are not properly installed.")
    print(msg)
    sys.exit(-1)

except Exception as exc:

    fatal_error()
    sys.exit(-2)


global gCommand
gCommand = "template"

global g_ver_info
g_ver_info = {}

global g_user_defaults
g_user_defaults = {}

#
############################################################################
# Command Error class.

class CommandError(Exception):
    """Class for exceptions raised in this module."""

    def __init__(self, message):
        self.message = message
        return

    def __str__(self):
        return repr(self.message)

## end class CommandError():

#
##############################################################################
# process_command_line
#

def process_command_line(sys_argv_list, db_server_id):
    global g_ver_info, g_version, g_wrap_version

    result = {}

    # Drop into debugger, value 2 is trace command line processing.
    if ( debug.get_debug() == 2 ):
        pdb.set_trace()

    # Options that all commands implement...
    option_list = [ ("", "help"), ("", "version"),
            ("", "debug="), ("", "dbserv=") ]

    # Set our command specific options as defined by the command module.
    cmd_options = default_options()

    # All command line flags or options are unset or have no value.
    for an_opt in cmd_options['optionList']:
        # If the option has '=' in it, the value is not boolean.
        cmd_line_info_flag = '--' + an_opt[1]
        if ( cmd_line_info_flag[-1] == "=" ):
            cmd_line_info_flag = cmd_line_info_flag[:-1]
            result[cmd_line_info_flag] = None
        else:
            result[cmd_line_info_flag] = False

    # Add command specific options to our list.
    option_list += cmd_options['optionList']

    # Add the standart 'set' arguments...
    if ( cmd_options['setArgs'] ):
        option_list += [ ("a", "add"), ("d", "delete"),
            ("k", "keys"), ("u", "update") ]

    # Turn option_list into the respective short and long options for getopt.
    short_options = ""
    long_options = []
    for an_opt in option_list:
        if ( len(an_opt[0]) > 0 ):
            short_options += an_opt[0]
        long_options.append(an_opt[1])

    # Process any of the command line options.
    try:
        opt_list, arg_list = getopt.getopt(
                sys_argv_list, short_options, long_options)
    except getopt.GetoptError as err:
        # print help information and exit:
        print((str(err)))
        sys.exit(usage(1))

    # loop through the options that getopt handled.
    for option, argument in opt_list:
        if option == "--version":
            cmd_options['action'] = "version"
        elif option == "--help":
            cmd_options['action'] = "help"
        elif ( cmd_options['setArgs'] and option in ["-a", "--add"] ):
            cmd_options['action'] = "add"
        elif ( cmd_options['setArgs'] and option in ["-u", "--update"] ):
            cmd_options['action'] = "update"
        elif ( cmd_options['setArgs'] and option in ["-d", "--delete"] ):
            cmd_options['action'] = "delete"
        elif ( cmd_options['setArgs'] and option in ["-k", "--keys"] ):
            cmd_options['action'] = "keys"
        elif ( option == "--debug" ):
            # Setting on the CLI occurs after 1 and 2 have passed execution.
            # Will override NET_DEBUG environment and default.
            debug.set_debug(int(argument))
        elif ( option == "--dbserv" ):
            # Will override NET_DBSERV environment and default.
            db_server_id = int(argument)
        else:
            # Convert a short option (-f), to the long one (--flag)
            if ( option[1] != "-" ):
                for an_opt in option_list:
                    if ( len(an_opt[0]) > 0 ):
                        if ( option == ("-" + an_opt[0][0]) ):
                            if ( an_opt[0][-1] == ":" ):
                                option = "--" + an_opt[1][:-1]
                            else:
                                option = "--" + an_opt[1]
                            break
            try:
                # process_option when implemented, allows late changes
                # to the command options based on options as they arrive.
                cmd_options.update(process_option(option, argument))
            except NameError:
                # If application didn't implement process_option...
                if ( len(argument) > 0 ):
                    result[option] = argument
                else:
                    result[option] = not result[option]

    # Now that al command line processing is complete, set the action.
    result['action'] = cmd_options['action']

    # There's now an action to perform so we can set minArgs and
    # requiredArgs as appropriate. In the case where result['setArgs']
    # is false, there's nothing to do: requiredArgs values should have
    # been set in default_options.
    if ( cmd_options['setArgs'] and result['action'] == "add" ):
        required_args = cmd_options['addRequired']
        min_args = cmd_options['addMinArgs']
    elif ( cmd_options['setArgs'] and result['action'] == "update" ):
        required_args = cmd_options['updateRequired']
        min_args = cmd_options['updateMinArgs']
    elif ( cmd_options['setArgs'] and result['action'] == "delete" ):
        required_args = cmd_options['delRequired']
        min_args = cmd_options['delMinArgs']
    elif ( result['action'] in ["keys", "version", "help"] ):
        required_args = []
        min_args = 0
    else:
        required_args = cmd_options['requiredArgs']
        min_args = cmd_options['minArgs']

    # Set the database list to open. This could get changed by the
    # command module if flags warrant a new db open or make one
    # no longer necessary.
    result['dbList'] = cmd_options['dbList']

    # Open the databases.
    if ( "late_db_open" not in cmd_options and result['action'] != "help" ):
        schema_ver, db_ver_info = open_databases(dbServerInfo, db_server_id,
                        cmd_options['dbUserClass'], cmd_options['dbList'])

    # Drop into the debugger for positional argument handling.
    if ( debug.get_debug() == 4 ):
        pdb.set_trace()

    # Process the positional command line arguments.
    if ( result['action'] not in ["help","version"] ):
        # If we don't have the correct number of arguments, return failure.
        if ( min_args > 0 and len(arg_list) < min_args ):
            raise CommandError("Missing command line argument(s)")

        # Process positional command line arguments implemented in the module.
        # This sets the input keys value for "setArgs" commands (--keys action)
        result.update(process_positional(result, arg_list))
        args_used = result['argsUsed']
        del result['argsUsed']

        if ( cmd_options['setArgs'] and result['action'] != "keys" ):
            # For set commands, process any input/key-value pairs.
            args_left = len(arg_list) - args_used
            if ( args_left > 0 ):
                if ( args_left % 2 == 1 ):
                    raise CommandError(
                            "invalid/missing command line arguments.")
                else:
                    # Process positional used an argument or two but we
                    # still have at least one key/key-value pair to process.
                    result.update( cmd_line_input.process_input_keys(
                            arg_list[args_used:], result['inputKeys']))

            # Check that the required arguments are present:
            for an_arg in required_args:
                if ( (an_arg in result) == False ):
                    raise CommandError(result['action'] + " requires " +
                            an_arg + " argument.")

            # Check if there are additional arguments.
            has_extra_arg = False
            for an_arg in result['inputKeys']:
                has_arg = an_arg in result
                not_reqd_arg = an_arg not in required_args
                if ( has_arg and not_reqd_arg ):
                    has_extra_arg = True

        # Drop into the debugger for argument verification.
        if ( debug.get_debug() == 5 ):
            pdb.set_trace()

        # Verify that everything we have been handed makes sense.
        try:
            # Optionally, the module can implement verify_options()
            result.update(verify_options(result))
        except NameError:
            # verify_options wasn't defined, just move on.
            pass
        except:
            # Something else went wrong, pass it up the chain.
            # should be a CommandError or ValueError.
            raise

    # If verify_options changed the db_list, we need to open
    # databases here.
    if ( result['action'] != "help" ):
        if ( "late_db_open" in cmd_options ):
            result['dbList'] = cmd_options['dbList']
            schema_ver, db_ver_info = open_databases(dbServerInfo,
                    db_server_id, cmd_options['dbUserClass'], result['dbList'])

        # Generate/complete our version information.
        g_ver_info = debug.ver_info(gCommand, g_version, g_wrap_version,
                                    schema_ver, db_ver_info)

    return(result)
## end process_command_line()

#
##############################################################################
# open_databases()
# Given the server information and list of databases, open connections
# to all the specified databases.
#

def open_databases(g_db_server_info, db_server_id, db_user_class, db_list):

    # Drop into debugger if requested.
    if ( debug.get_debug() == 3 ):
        pdb.set_trace()

    # Indicate if we are using the development database.
    if ( db_server_id != 0 ):
        print("development db " + str(db_server_id) + " selected.")

    # Loop through each database and open a connection.
    db_ver_info = {}
    db_server = g_db_server_info[db_server_id]
    for db_name in db_list:
        server = db_server[db_name]['server']
        port = db_server[db_name]['port']
        db_user = db_server[db_name][db_user_class]
        try:
            with network_error.Timeout(1):
                db_access.init_db_server(server, port, db_name, db_user)
                db_ver_info[db_name] = db_access.get_db_version(db_name)
        except network_error.Timeout.Timeout as err:
            print("The database server connection timed out!")
            sys.exit(-1)
        except OperationalError as err:
            the_error = str(err)
            # FATAL:  no pg_hba.conf entry for host "130.179.16.167", 
            #    user "netview", database "networking", SSL off
            if ( 'pg_hba.conf' in the_error ):
                print("Foo, charlatan! Are you authorized to be here?")
                print("This host does not have access to the database server.")
            elif ( 'sendauth' in the_error ):
                print("Foo, charlatan! Are you authorized to be here?")
                print("Your .pgpass may be missing an entry:")
                print("%s:%n:%s:%s:PASSWD" % (server, port, db_name, db_user))
                print(server + ":" + str(port) + ":" + db_name + ":"
                        + db_user + ":PASSWORD")
            elif ( 'Connection refused' in the_error ):
                print("The database is refusing connections!")
                print("Connection refused by " + server + ":" + str(port))
                print("for database " + db_user + "@" + db_name)
            elif ( "does not exist" in the_error ):
                print("Cannot continue, database(s) missing:")
                print("    " + the_error)
            else:
                print("An unknown database error occurred.")
                if ( debug.get_debug() != 0 ):
                    print("That's a new database error! Here's the detail:")
                    print("    " + the_error)
            sys.exit(-1)

    result = (db_schema.get_schema_version(), db_ver_info)

    return(result)
## end open_databases()

##############################################################################
# close_databases()
# Close any open database connections.
#

def close_databases(db_list):
    # Cleanup by closing our database connections.
    for db_name in db_list:
        try:
            with network_error.Timeout(1):
                db_access.close_db_conn(db_name)
        except network_error.Timeout.Timeout as err:
            print("The database server connection timed out!")
        except:
            raise
## end close_databases():

#
##############################################################################
# envir_setup()
# Get environment variables and set defaults.

def envir_setup():

    # Check to see if we are going to do some debugging.
    try:
        env_debug = int( os.getenv('NET_DEBUG', 0) )
    except ValueError:
        # Debug set but no value, use -1.
        env_debug = -1
    except:
        fatal_error()
        sys.exit(-2)

    if ( env_debug != 0 ):
        debug.set_debug(env_debug)

    # Set our default database server id. 0 = production, 1/2 = devel.
    try:
        db_server_id = int(os.getenv('NET_DBSERV', 0))
    except ValueError:
        # NET_DBSERV set but no value, use production (0)
        db_server_id = 0

    return(env_debug,db_server_id)
## end envir_setup():

##############################################################################
# get_load_module()
#
def get_load_module(cmd_name):
    global gCommand
    # Figure out where the command module is.
    gCommand = os.path.basename(cmd_name)
    full_cmd_path = os.path.dirname(cmd_name)
    # drop the .py extension if it exists, then add a _mod.py extension.
    mod_name_parts = gCommand.split('.')
    if ( len(mod_name_parts) > 1 ):
        mod_name = '.'.join(mod_name_parts[0:len(mod_name_parts)-1]) + '_mod.py'
    else:
        mod_name = mod_name_parts[0] + '_mod.py'

    # Figure out where the local python libraries are.
    py_lib_path = sysconfig.get_python_lib()

    # build list of potential module file names:
    mod_file_list = []
    mod_file_list.append(full_cmd_path + '/' + \
                    mod_name.translate(str.maketrans('-','_')))
    mod_file_list.append(py_lib_path + '/network/' + \
                    mod_name.translate(str.maketrans('-','_')))

        # use current path as default so testing is easier.
    for mod_file in mod_file_list:
        # loop through each file. quit after the first one found.
        try:
            f1 = os.stat(mod_file)
            mod_file_name = mod_file
            break
        except OSError:
            mod_file_name = ''

    if ( mod_file_name == '' ):
        print("Could not find command modules: ")
        print((str(mod_file_list)))
        sys.exit(errExitCode)

    return(mod_file_name)
## end get_load_module():

##############################################################################
# parse_cli()
#
def parse_cli(sys_argv_list, exit_code, db_server_id):
    try:
        cmd_line_info = process_command_line(sys_argv_list, db_server_id)
    except ImportError as msg:
        print("A library or routine in a library is likely missing.")
        print("ImportError: " + str(msg))
        sys.exit(exit_code)
    except CommandError as message:
        print(message)
        sys.exit(usage(exit_code+1))
    except ValueError as message:
        # Something is wrong with the arguments provided.
        print("ValueError: " + str(message))
        sys.exit(exit_code+2)
    except IndexError:
        sys.exit(usage(exit_code+3))
    except SystemExit:
        # Just quietly exit.
        raise
    except Exception as exc:
        fatal_error()
        sys.exit(exit_code+4)

    # increment by the three error values above.
    exit_code += 4

    return(exit_code, cmd_line_info)
## end parse_cli():


##############################################################################
# fatal_error()
#

def fatal_error():
    # Generate a traceback if debugging is enabled, otherwise
    # email the report and exit quietly.

    global g_ver_info

    if ( debug.get_debug() == 0 ):
        debug.crash(sys.argv[0:], sys.exc_info(), g_ver_info)
    else:
        raise

    return()
## end fatal_error()

#
##############################################################################
#
# Process the command line and then do the work.
#

if __name__ == "__main__":

    # Check that we have a reasonable version of python.
    if ( sys.version_info < (3,3) ):
        print("This command requires Python version 3.3 or newer.")
        sys.exit(exit_code)

    # Check our environment variables and set up any debugging.
    env_debug, db_server_id = envir_setup()

    # Start with an exit code of one.
    exit_code = 1

    # Drop into debugger, value 1 is trace looking for files.
    if ( debug.get_debug() == 1 ):
        pdb.set_trace()

    # Import the stuff of our command.
    mod_file_name = get_load_module(sys.argv[0])
    exec(compile(open(mod_file_name).read(), mod_file_name, 'exec'))
    exit_code += 1

    # Process the command line.
    exit_code, cmd_line_info = parse_cli(sys.argv[1:], exit_code, db_server_id)

    # Extract the db_list so we can clean up after main()
    db_list = cmd_line_info['dbList']
    # main() doesn't require this.
    del cmd_line_info['dbList']

    # Drop into debugger if requested.
    if ( debug.get_debug() == 6 ):
        pdb.set_trace()

    # Now we have a verified command line and action to perform.
    if ( cmd_line_info['action'] == 'version' ):

        # display our collected version information and exit.
        for a_key in list(g_ver_info.keys()):
            print(g_ver_info[a_key])
        print("")

    elif ( cmd_line_info['action'] == 'help' ):

        # Display the usage message and exit.
        sys.exit(usage(0))

    elif ( cmd_line_info['action'] == 'keys' ):

        cmd_line_input.list_input_keys(cmd_line_info['inputKeys'])

    else:

        # done with the inputKeys, remove it now.
        if ( "inputKeys" in cmd_line_info ):
            del cmd_line_info['inputKeys']

        # The networking database should be open now so load the defaults.
        user_defaults = user_defaults.get_defaults()

        # Do the real work.
        try:
            main(cmd_line_info, user_defaults)
        except SystemExit:
            # Exit quietly.
            raise
        except ImportError as msg:
            print("A library or routine in a library is likely missing.")
            print("ImportError: " + str(msg))
            sys.exit(exit_code+0)
        except ValueError as message:
            # Something is wrong with the arguments provided.
            print("ValueError: " + str(message))
            sys.exit(exit_code+1)
        except network_error.NetException as err:
            exit_code = network_error.net_error_handler(err)
            sys.exit(exit_code)
        except CommandError as message:
            print(message)
            sys.exit(exit_code+2)
        except db_access.DbException as err:
            exit_code = db_access.db_error_handler(err)
            sys.exit(exit_code)
        except Exception as exc:
            fatal_error()
            sys.exit(exit_code+3)

    # Cleanup by closing our database connections.
    close_databases(db_list)

## end if __name__ == "__main__":

# vim: syntax=python ts=4 sw=4 showmatch expandtab :
