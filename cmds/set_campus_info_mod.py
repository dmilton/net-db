# Version information for this command.

g_version='25 Sep 2014 - 3.0.1'

# 13 May 2014   Cleanup usage formatting.
# 25 Apr 2013   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
	import building_info
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help | --version ]
usage: %s [--add] "name of campus"
usage: %s --update campus-id "new name of campus"
usage: %s --delete campus-id
    --help          display this help message
    --version       show version information and exit.
    --keys          list valid input keys.
    --update        update the name of the campus.
    --delete        delete the campus.
    """ % ( gCommand, gCommand, gCommand, gCommand )))
    return(exit_code)
## end usage()

##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # Assume the default is to display/list information.
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"  # read/update access.

    # Set default action and options.
    result['setArgs'] = True
    result['action'] = 'add'

    result['optionList'] = []

    # The add requires two arguments or the entry is pretty much useless.
    # list of table columns that are mandatory.
    result['addRequired'] = ["campus_name"]
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires the table column that is the primary key and
    # one key/value pair to update.
    result['updateRequired'] = ["campus_id", "campus_name"]
    result['updateMinArgs'] = len(result['updateRequired'])
    # Delete uses the room_id field.
    result['delRequired'] = ["campus_id"]
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmd_line_info - any command line flags and default options thus far.
# arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmd_line_info, arg_list):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # initialize tables and set input keys.
    building_info.initialize()
    result['inputKeys'] = building_info.get_campus_fields()

    # Now process any remaining arguments.
    if ( cmd_line_info['action'] == "add" ):

        # All arguments on the command line are the campus name.
        result['campus_name'] = arg_list[0]
        result['argsUsed'] += 1
        for anArg in arg_list[1:]:
            result['campus_name'] += " " + anArg
            result['argsUsed'] += 1

    elif ( cmd_line_info['action'] == "update" ):

        result['campus_id'] = int(arg_list[0])
        result['campus_name'] = arg_list[1]
        result['argsUsed'] += 2
        for anArg in arg_list[2:]:
            result['campus_name'] += " " + anArg
            result['argsUsed'] += 1

    elif ( cmd_line_info['action'] == "delete" ):

        result['campus_id'] = int(arg_list[0])
        result['argsUsed'] += 1

    return(result)
## end process_positional()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(the_options):

    # Check tags in the_options to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    if ( the_options['action'] == "delete" ):
        if ( "campus_id" not in the_options ):
            raise CommandError("delete requires campus id argument.")
        ## end if ( the_options.has_key('auth_code') == False ):
    elif ( the_options['action'] == "add" ):
        # require auth_code and rtu_serial
        if ( "campus_name" not in the_options ):
            raise CommandError("add requires the location")
        ## end if ( the_options.has_key('auth_code') == False ...
    elif ( the_options['action'] == "update" ):
        # check that there is at least one option.
        if ("campus_name" in the_options and "campus_id" in the_options):
            has2ndArg = True
        else:
            raise CommandError("update requires campus id and location value.")

    return(the_options)
## end verify_options()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # In order to support the --keys tag, all tables are initialized
    # previous to our arriving here in the code. This doesn't really
    # make sense with this particular table since there's really only
    # one field but this allows that to change without too much rewriting
    # of the code here.

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Turn cmd_line_info into a router_afl record dictionary
    # and an action to perform.
    action = cmd_line_info['action']

    if ( action == 'delete' ):

        print("Delete is a big operation!")
        print("Obtain a list of all buildings on the specified campus.")
        print("For each building:")
        print("  obtain a list of all networks in the building.")
        print("  the 'loop' network is the name of the router")
        print("    The AFL license needs to me managed - hardware reassigned?")
        print("  delete/free all the networks")
        print("  obtain a list of all voice/data rooms in the building.")
        print("  delete all voice/data rooms in the building.")
        print("  delete the building.")
        print("Finally, delete the campus.")
        print("Only the last step is actually taking place.")
        theCampus = building_info.new_campus()
        theCampus['campus_id'] = cmd_line_info['campus_id']
        building_info.delete_campus(theCampus)
        print("The campus has been deleted.")

    elif ( action in ["add", "update"] ):

        # starting with a db record, which is all underscores,
        # translate underscores into hyphens so either can be used.

        # create a record from cmd_line_info to use for add/update.
        theCampus = building_info.new_campus()
        for aKey in list(theCampus.keys()):
            aKey2 = aKey.translate(str.maketrans('_','-'))
            if ( aKey in cmd_line_info ):
                theCampus[aKey] = cmd_line_info[aKey]
            elif ( aKey2 in cmd_line_info ):
                theCampus[aKey] = cmd_line_info[aKey2]
            else:
                del theCampus[aKey]

        try:
            if ( cmd_line_info['action'] == 'add' ):
                dbRecord = building_info.add_campus(theCampus)
                print("The campus has been added:")
            else:
                dbRecord = building_info.update_campus(theCampus)
                print("The campus has been updated:")
            cmd_output.print_campus(dbRecord)
        except NetException as err:
            if ( err.name == "duplicate" ):
                print((err.detail))
            elif ( err.name == "not found" ):
                print((err.detail))
            else:
                raise
        except:
            raise

    ## end elif ( action in ["add", "update"] ):

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

