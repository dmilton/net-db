#
# Version information for this command.

g_version='25 Mar 2015 - 3.0.11'

# 25 Mar 2015   Ping implementations between MacOS/Solaris/Linux vary so
#               rewrite ping here to handle the variations.
# 11 Jun 2014   pysnmp returns OID without the leading period so the check
#               for an OID with a leading period will fail.
#  6 Jun 2014   Upgrade to netsnmp broke string parsing. Rewrote the
#               snmp module to use pysnmp but to avoid errors here wrapper
#               routines for snmp_get/snmp_walk were needed to format the
#               results based on the kind of value being requested.
# 13 May 2014   Qualify the NetException handling.
#  8 May 2014   Missed a few string functions that changed syntax
#               from python 2 to python 3.
# 23 Sep 2013   Add code to conditionally debug after init done.
# 15 Apr 2013   The --snmp flag and --no-snmp both imply that netdisco
#               may not be available to obtain the snmp community from.
#               Change so the netdisco library is not initialized.
#  8 Apr 2013   update_license now returns a result containing the
#               values of the updated record.
#  5 Apr 2013   Add reverse host name lookup check. Add manual entry
#               for serial number and virtual chassis number.
#  2 Apr 2013   Add new flag --snmp so the snmp community string can
#               be added manually on the command line.
# 31 Mar 2013   Update for new router_afl routine names.
# 12 Mar 2013   Update for new command template functionality.
# 24 Jan 2013   Add code to get snmp community string from the netdisco
#               database. This should fix issues with the Bannatyne
#               campus using different community strings.
#  3 Jan 2012   Updated notes provided when an auth code is assigned to
#               reference the storage directory and activation command.
# 19 Dec 2012   Need to use router name rather than the building code
#               since things like westgrid-um, bcrouter, fgrouter, etc.
#               don't follow the same naming convention for building
#               routers.
#               Added some exception handling to deal with switches
#               which have old code, are offline, or are not Juniper.
# 15 Nov 2012   initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import router_afl
    import network_snmp
    import netdisco
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s --help |--version
usage: %s [--snmp=X|--no-snmp=y,z] router-name
    --help              display this help message
    --version           show version information and exit.
    --snmp=x            x = snmp community string to use.
    --no-snmp=y,z       y = serial number, z = virtual chassis id.
    router-name         the name of the router to assign auth keys for.
    """ % ( gCommand, gCommand )))
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking", "netdisco"]
    result['dbUserClass'] = "user"
    # the --snmp and --no-snmp both drop "netdisco" from dbList.
    result['late_db_open'] = True

    # Assume the default is to display/list information.
    result['setArgs'] = False
    # default action:
    result['action'] = "assign"

    # Command line options:
    result['optionList'] = [ ("s", "snmp="), ("n", "no-snmp=") ]

    # Required arguments:
    result['requiredArgs'] = ["router"]
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    # We only have one positional argument.
    result['router'] = argList[0]
    result['argsUsed'] = 1

    # raise CommandError, ("kind of error")

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(the_options):

    # Assume we are going to use the netdisco database to
    # determine the SNMP community.
    the_options['use_netdisco'] = True

    if ( the_options['--snmp'] != None ):
        # Manual specification of the SNMP community.
        the_options['snmp_community'] = the_options['--snmp']
        the_options['use_netdisco'] = False
        the_options['dbList'] = ["networking"]

    if ( the_options['--no-snmp'] != None ):
        # Router may not be online, manual specification
        # of serial number and virtual chassis id.
        sn, vc = the_options['--no-snmp'].split(',')
        the_options['serial_nbr'] = sn
        the_options['vc_number'] = int(vc)
        the_options['use_netdisco'] = False
        the_options['dbList'] = ["networking"]
        if ( int(vc) > 9 or int(vc) < 0 ):
            raise CommandError("invalid vc number " + str(int(vc)))

    # If there is a problem with an argument:
    # What information has been provided from the command line?
    hasSn = "serial_nbr" in the_options
    hasVc = "vc_number" in the_options

    if ( "snmp_community" in the_options ):
        if ( hasSn or hasVc ):
            raise CommandError(("--snmp and --no-snmp cannot be combined."))
    else:
        # we don't have snmpComm so if either of hasSn is true but not both
        # then we have incomplete information.
        if ( (hasSn or hasVc) and not (hasSn and hasVc) ):
            raise CommandError(
                "--no-snmp requires serial-nbr,vc-number as argument.")

    if ( the_options['action'] == "help" ):
        the_options['use_netdisco'] = False
        the_options['dbList'] = ["networking"]

    return(the_options)
## end verify_options()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # If debug is set, drop into the debugger.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # initialize the table structures we need.
    router_afl.initialize()

    # initialize the netdisco tables.
    if ( cmd_line_info['use_netdisco'] ):
        try:
            netdisco.initialize()
        except:
            # initialization of netdisco failed, don't use it.
            cmd_line_info['use_netdisco'] = False

    # Make sure we have a valid host name.
    try:
        import socket
        router_ip = socket.gethostbyname(cmd_line_info['router'])
    except socket.gaierror:
        print("Unknown Host " + cmd_line_info['router'])
        sys.exit(1)
    ## end try
    try:
        router_fqdn = socket.gethostbyaddr(router_ip)[0]
    except socket.herror:
        print("Hostname reverse lookup failed: " + router_ip)
        router_fqdn = cmd_line_info['router']
    ## end try

    # Check that the router is online
    online = ping(router_ip)

    if ( online == False ):
        print("Router not reachable/pingable.")
        sys.exit(2)

    # Figure out which community string to use.
    if ( cmd_line_info['--no-snmp'] == None ):
        if ( "snmp_community" in cmd_line_info ):
            snmp_comm = cmd_line_info['snmp_community']
        else:
            try:
                snmp_comm = netdisco.get_snmp_comm(cmd_line_info['router'])
            except NetException as why:
                print((why.detail))
                sys.exit(3)
    
    if ( cmd_line_info ['--no-snmp'] != None ):
        print("Skipping hardware check. Assumptions have been made that")
        print("the router is a Juniper EX4200 virtual chassis system")
        print("consisting of _24_ port switches and that the serial number")
        print("is valid. No hardware MAC address information will be")
        print("included when the license is assigned.")
        vc_id = cmd_line_info['vc_number']
        chassis_info = {}
        chassis_info['serial-nbrs'] = [cmd_line_info['serial_nbr']]
        chassis_info['model-nbrs'] = ["ex4200-24p"]
        chassis_info['mac-addresses'] = ['00:00:00:00:00:00']
    else:
        vc_id = -1
        # This is for Juniper hardware only so..
        sys_obj_id = network_snmp.get_sys_object_id(router_ip, snmp_comm)
        if ( not '1.3.6.1.4.1.2636.1.1.1.2.31' in sys_obj_id ):
            print("This command only assigns afl authorization codes to")
            print("Juniper ex3200 and ex4200 switches. This switch reports as:")
            print(sys_obj_id)
            sys.exit(5)

        # Verify a minimum software revision and exit if it is not current.
        firmware_ver = network_snmp.get_junos_version(router_ip, snmp_comm)
        major_ver = float(firmware_ver[0:4])
        minor_ver = float(firmware_ver[5:])
        if ( major_ver < 11.4 ):
            print("The firmware on this router is too old. You must upgrade to")
            print("at least 11.4 before you can assign an afl license.")
            print(("Current version is: " + firmware_ver))
            sys.exit(6)

        # poll the router to obtain the list of serial numbers and their
        # respective member id.
        chassis_info = network_snmp.get_junos_chassis_info(router_ip, snmp_comm)

    show_directions = False
    print("Virtual chassis components:")
    print("VC    Model        Serial        MAC        Auth Code")
    for i in list(range(len(chassis_info['serial-nbrs']))):
        #
        # Get the afl information for each switch in the chassis.
        #
        # Find license and fill in the blanks to assign them.
        switch_afl = router_afl.find_afl_info(chassis_info['serial-nbrs'][i])
        if ( switch_afl == None ):
            # Did not find one already assigned, get one.
            afl_list = router_afl.get_licenses(chassis_info['model-nbrs'][i], 1)
            if ( len(afl_list) == 1 ):
                switch_afl = afl_list[0]
            # Update all fields based on installed hardware.
            switch_afl['serial_nbr'] = chassis_info['serial-nbrs'][i]
            switch_afl['router'] = router_fqdn
            if ( vc_id >= 0 ):
                switch_afl['member_id'] = vc_id
            else:
                switch_afl['member_id'] = i
            switch_afl['lan_mac'] = chassis_info['mac-addresses'][i]
            switch_afl['switch_model'] = chassis_info['model-nbrs'][i]

        switch_member = str(switch_afl['member_id'])

        # List the assigned license, flagging those that have been configured
        # on the router and listing those that have not.
        if ( switch_afl['auth_active'] ):
            switch_member += ' *'
            afl_auth_code = 'active'
        else:
            afl_auth_code = switch_afl['auth_code']
            show_directions = True
            # Update any fields that were changed.
            updated_afl = router_afl.update_license(switch_afl)

        print(("%-4s%s\t%s\t%s\t%s" % 
            (switch_member, switch_afl['switch_model'], 
                switch_afl['serial_nbr'], switch_afl['lan_mac'],
                afl_auth_code)))

    print("\n")

    # do we need to provide directions to the user?
    if ( show_directions == True ):
        print("New licenses authorization codes have been assigned (or are")
        print("marked as not active) to at least one switch in the virtual")
        print("chassis. Please login to the Juniper License Management System")
        print("at:    www.juniper.net/generate_license")
        print("select the EX Series link and follow the instructions there.\n")
        print("/*************************************************************")
        print("**")
        print("** Download the key file for the license key through the")
        print("** Juniper system and store this key in:")
        print("**    /opt2/junos/keys/SWITCH_SERIAL_NBR.txt")
        print("** Using the serial number of the switch for the file name.")
        print("** Once received, use the command:")
        print("**    activate-afl /opt2/junos/keys/SWITCH_SERIAL_NBR.txt")
        print("** to mark the key as assigned and active.")
        print("** The output of activate-afl will instruct you on how to")
        print("** install the license key on the router.")
        print("**")
        print("*************************************************************/")
    ## end if ( show_directions == True ):

## end main()

#
##############################################################################
# Local function definitions...

def ping(hostname):
    result = False
    import os
    # There is a python implementation of ICMP echo request/reply
    # but it requires root to run. Unfortunately, using the ping
    # command is a cross platform pain; Solaris = /usr/sbin/ping,
    # MacOS/BSD = /sbin/ping, Linux = /bin/ping. And then there
    # are the myriad of options. All we want is a go/no-go!
    # This is handled here by a zero/non-zero exit code.
    #                           BSD     Solaris     Linux
    # Numeric Output (no DNS)   -n      -n          -n
    # Count of 1 icmp packets   -c 1                -c 1
    # exit after one success    -o
    # timeout after 1 second    -t 1    second arg  -W 1
    # Don't display ICMP errors -Q
    # be as quiet as possible   -q                  -q
    uname_info = os.uname()
    if ( uname_info.sysname in ["Darwin", "FreeBSD"] ):
        ping_cmd = "/sbin/ping -n -c 1 -o -q -Q " + hostname + " > /dev/null"
    elif ( uname_info.sysname == "SunOS" ):
        ping_cmd = "/usr/sbin/ping -n " + hostname + " 1 > /dev/null"
    elif ( uname_info.sysname == "Linux" ):
        ping_cmd = "/bin/ping -n -c 1 -W 1 -q " + hostname + " > /dev/null"
    else:
        # We don't know the OS so just exit with a fail.
        raise CmdException("ping not supported/configured on this OS.")

    # Send the appropriate platform specific ping command:
    response = os.system(ping_cmd)

    #and then check the response...
    if response == 0:
        result = True
    ## end if response == 0:

    return result
## end def ping()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

