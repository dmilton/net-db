# version information for this command.

g_version='01 Oct 2014 - 3.0.9'

# Free a network in the database.
#
# If the network is private it is removed from the database.
# If the network is public then it is removed from the network_bldg
# or network_link child table and inserted to the network_free table.

#  4 Sep 2014   Doesn't work with ISO networks, fixed.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 20 Mar 2014   Preserve the network zone when freeing it.
# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
# 23 Sep 2013   Add code to drop into debugger after init done.
# 23 Feb 2012   Rewrite for the new database structure and child tables.
#  5 Jan 2012   Change 'vlan' to 'vlan_tag'. This means in some
#               places the dictionary tag 'number' needed to change
#               to vlan_tag.
# 16 Aug 2010   Add VerifyOptions function.
# 25 Mar 2010   Fix Usage so the arguments accepted are listed.
#               Raise exception in ProcessArg.
#  8 Dec 2009   Convert to modular wrapper form.
# 27 Nov 2009   Changed AllocateNetwork some time ago to accept
#               an argument for the userid assigning a network.
#               Needed to add the argument here but in this case
#               we can use 'netmon' since we are freeing a network.
# 26 Nov 2009   When adding the bug collection code I neglected
#               to notice that a sys.exit(n) would also cause a
#               crash. Moved the internal exception handling off
#               to __main__ so that normal errors can be handled
#               with an error message to the user first and a
#               graceful exit rather than a 'crash'.
# 12 Nov 2009   Changed import path to /opt2/local/etc.
# 21 Oct 2009   Changed name of CampusFreeCodes to GetNetTypes.
# 20 Aug 2009   Changed code to match the new CampusFreeCodes routine.
# 18 Aug 2009   Script failing because -O flag missing.
#  6 Jul 2009   Exception handling code was failing on sys.exit(0)
#               Changed code so it didn't fail on normal exits.
#  3 Jul 2009   Add code to handle exceptions.
# 29 Jun 2009   add code to handle development database server.
# 24 Nov 2008   problems with unassigned variable when
#               looking up the network to release.
# 18 Sep 2008   initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--type=x] cidr-network
usage: %s [--type=x] --name network-name
    --help              display this help message
    --version           show version information and exit.
    --type=x            set the network type when releasing.
    --name              argument is the name of the network.
    cidr-network        network in CIDR format to free.
    """ % ( gCommand, gCommand, gCommand ))
    return(exitCode)
## end usage()

##############################################################################
# default_options()
# This routine must process the positional command line arguments.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = "release"

    # Command line options
    result['optionList'] = [ ("t:", "type="), ("n", "name") ]

    # Required arguments and minimum argument count.
    # can be either network or net_name when --name is used.
    result['requiredArgs'] = ["network_or_name"]
    result['minArgs'] = len(result['requiredArgs'])

    return(result)
## end default_options()

#
##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise CommandError("invalid option combination")
    if ( theOptions['--name'] ):
        theOptions['netSearchType'] = "name"
    else:
        theOptions['netSearchType'] = "cidrEqual"

    return(theOptions)
## end verify_options()

##############################################################################
# process_positional()
# This routine must process the list positional command line arguments.

def process_positional(cmdInfo, argList):

    result = {}

    # If we don't have the correct number of arguments, return failure.
    if ( len(argList) != 1 ):
        raise CommandError("Missing command argument")

    # The only positional argument is the CIDR value to change:
    if ( cmdInfo['--name'] ):
        result['net_name'] = argList[0]
    else:
        try:
            result['network'] = cmd_line_input.check_iso_addr(argList[0])
            result['netSearchType'] = "isoEqual"
        except ValueError:
            result['network'] = ipaddress.ip_network(argList[0])
    result['argsUsed'] = 1

    return(result)
## end process_positional()

#
############################################################################
# Main
def main(commandInfo, userDefaults):

    # initialize our tables.
    building_info.initialize()
    network_info.initialize()
    network_types.initialize()

    # Drop into the debugger if necessary.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Lookup the network.
    # An exception is raised here for not found or multiple networks
    # found so processing continues only if one match is found.
    if ( commandInfo['netSearchType'] in ["cidrEqual","isoEqual"] ):
        net_id = commandInfo['network']
    else:
        net_id = commandInfo['net_name']
    netInfo = network_info.get_network_info(commandInfo['netSearchType'], net_id)

    # Get the information on the building this is presently allocated to.
    bldgCode = netInfo['bldg_code']
    bldgInfo = building_info.get_building_info(bldgCode)

    print("The network is currently allocated to:")
    print("  " + bldgInfo['bldg_name'] )
    print("With the following details:")
    print("  network " + str(netInfo['network']))
    print("  bldg-code " + netInfo['bldg_code'] + ", name " \
            + netInfo['net_name'] + ", vlan " + str(netInfo['vlan_tag']) )

    # Figure out if we know what kind of network this is.
    # NetException(InvalidNetType) is raised if the type is not found.
    typeInfo = network_types.get_type_info(netInfo['net_type'])[0]
    print("  network type " + typeInfo['net_type'] )

    if ( 'net_type' in commandInfo ):
        # Type is changing, get the info for the update.
        newTypeInfo = network_types.get_type_info(commandInfo['net_type'])[0]
        print(("  changing type to " + newTypeInfo['net_type']))
    else:
        # Type not changing so use what already exists.
        newTypeInfo = typeInfo

    # Preserve the zone information from the building as we free it.
    netInfo['net_zone'] = bldgInfo['net_zone']

    # Now we can free the network.
    network_info.free_network(netInfo, newTypeInfo, bldgInfo['net_zone'])

    print(("The network " + str(netInfo['network']) + " is now avaialable."))

## end main()

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

