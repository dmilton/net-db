# Version information for this command.

g_version='26 Sep 2014 - 1.0.4'

# 10 Mar 2014   Begin using the python 3.3 ipaddress library.
# 24 Jan 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_info
    import network_info
    import network_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(msg)
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print(("""
usage: %s [--help] [--version] cidr-network
    --help              display this help message
    --version           show version information and exit.
    cidr-network        the network to display information for.
    """ % ( gCommand ) ))
    sys.exit(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = 'list'

    result['optionList'] = []

    result['requiredArgs'] = ["network"]
    result['minArgs'] = 1

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0

    # We only have one positional argument.
    if ( len(cmdInfo) >= 1 ):
        result['network'] = ipaddress.ip_network(argList[0])
        result['argsUsed'] = 1

    return(result)
## end process_positional()

##############################################################################
# VerifyOptions
# This routine must verify the options which are present and return
# an exception if the combination of options presented is invalid.

def verify_options(theOptions):

    # Check tags in theOptions to see if there are some which are
    # mutually exclusive. If so, return an appropriate exception.
    # raise ValueError("invalid option combination")

    return(theOptions)
## end verify_options()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize any tables here.
    network_info.initialize()
    building_info.initialize()
    network_types.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Get the information about the network.
    #netInfo = networks.get_network_info("cidrEqual", cmdLineInfo['network'])
    netList = networks.get_networks_within(cmdLineInfo['network'])
    #bldgInfo = buildings.get_building_info(netInfo['bldg_code'])
    #typeInfo = network_types.get_type_info(netInfo['net_type'])

    # Display what we got:
    if ( len(netList) == 1 ):
        networks.print_network(netList[0])
    else:
        # Print our table.
        netTitle = "Networks Found:"
        netHead = ("Bldg Code", "Name", "VLAN", "Network", "Type")
        netKeys = ("bldg_code", "net_name", "vlan_tag", "network", "net_type")
        from cmd_line_io import print_table
        print_table(netTitle, netHead, netKeys, netList)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 showmatch et :
