#
# Version information for this command.
#

g_version='dd mmm yyyy - 1.0.0'

# 14 Apr 2015   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    from ipwhois import IPWhois
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:
    import building_types
except ImportError as msg:
    print("The NetTrackDB utilities are not properly installed.")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exit_code)

def usage(exit_code):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print( """
usage: %s --help | --version
usage: %s bldg-type bldg-type-description
usage: %s --update bldg-type new-bldg-type-description
usage: %s --delete bldg-type
    --help              display this help message
    --version           show version information and exit.
    bldg-type           An up to 8 character short name for building type.
    bldg-type-description   A longer description as to the type of building.
    """ % (gCommand, gCommand, gCommand, gCommand) )
    return(exit_code)
## end usage()

##############################################################################
# default_options()
# This routine must return databases, database users, argument counts,
# and required argument lists for the add, update, and delete actions.

def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"] # [ "netdisco", "intermapper"]
    result['dbUserClass'] = "look"  # user

    # this command wants add, update, delete, and keys actions?
    result['setArgs'] = True
    # Default action.
    result['action'] = "add"

    # Command line options:
    # Cannot use a/add, u/update, d/delete, and k/keys if setArgs is true.
    # If you don't want a coresponding short option then use ""
    result['optionList'] = [ ]

    # For commands that do not implement the "add, update, delete, keys"
    # commands, what are the required positional arguments.
    result['requiredArgs'] = ["bldg_type", "bldg_type_description"]
    result['minArgs'] = len(result['requiredArgs'])

    # For commands that implement the "add, update, delete, keys"
    # commands, what are the required positional arguments for each.
    # The add requires two arguments or the entry is pretty much useless.
    # list of table columns that are mandatory.
    result['addRequired'] = ["bldg_type", "bldg_type_description"]
    result['addMinArgs'] = len(result['addRequired'])
    # Update requires the table column that is the primary key and
    # one key/value pair to update.
    result['updateRequired'] = ["bldg_type"]
    result['updateMinArgs'] = len(result['updateRequired']) + 1
    # Delete uses the room_id field.
    result['delRequired'] = ["bldg_type"]
    result['delMinArgs'] = len(result['delRequired'])

    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments that are
# not key/key-value pairs. This essentially means the key field(s) for the
# table being updated.
# Entry:
#    cmd_line_info - all command line flags and options processed thus far.
#    arg_list - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.
# Exit: the result of this command should be the positional, unnamed
#    arguments from the command line.

def process_positional(cmd_line_info, arg_list):

    result = {}

    # Assume we don't consume any command line arguments.
    result['argsUsed'] = 0

    # # #
    # For commands that implement add, update, delete, and keys:
    #

    # initialize tables and set input keys.
    building_types.initialize()
    result['inputKeys'] = building_types.get_building_type_fields()

    if ( cmd_line_info['action'] == "delete" ):
        # Delete requires one argument - the bldg_type
        result['bldg_type'] = arg_list[0]
        result['argsUsed'] = 1
    elif ( cmd_line_info['action'] in ["add", "update"] ):
        # Update requires bldg_type plus the new description.
        # Add requires reqd_pos_arg and whatever else is needed
        # to form a complete row in the table.
        result['bldg_type'] = arg_list[0]
        result['bldg_type_description'] = arg_list[1]
        result['argsUsed'] = 2

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmd_line_info, user_defaults):

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Error conditions have been eliminated, do the add/update/delete.
    if ( cmd_line_info['action'] == "add" ):
        the_bldg_type = building_types.add_building_type(cmd_line_info)
    elif ( cmd_line_info['action'] == "update" ):
        the_bldg_type = building_types.update_building_type(cmd_line_info)
    elif ( cmd_line_info['action'] == "delete" ):
        building_types.delete_building_type(cmd_line_info)

    if ( cmd_line_info['action'] in ["add","update"] ):
        cmd_output.print_building_type(the_bldg_type)
    else:
        print("The building type '" + the_bldg_type['bldg_type'] 
                + "' has been deleted.")

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

