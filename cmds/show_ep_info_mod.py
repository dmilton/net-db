#
# Version information for this command.
#
g_version='25 Sep 2014 - 1.0.5'

#  6 Feb 2014   Initial writing.

############################################################################
# import modules used here...

# The code which includes what you write in this module imports...
# Standard Python Libraries:
# os, sys. platform, getopt, string, and pdb.
# NetTrackDB libraries:
# debug, cmd_line_input, cmd_output network_error, 
# db_access, db_schema, db_columns, user_defaults

# Standard Libraries
#import ipaddress

# External Libraries
try:
    # Module with other dependancies:
    #from ipwhois import IPWhois
    pass
except ImportError as msg:
    print ("The ipwhois module requires dnspython3")
    print(gCommand + ": " + str(msg))
    sys.exit(-1)
except Exception as exc:
    fatal_error()
    sys.exit(-2)

# NetTrackDB Libraries:
try:

	import network_info

except ImportError as msg:

    print("The NetTrackDB utilities are not properly installed.")
    print(msg)
    sys.exit(-1)

except Exception as exc:

    fatal_error()
    sys.exit(-2)

#
############################################################################
# usage(exitCode)

def usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print("""
usage: %s --help | --version
usage: %s [--show-hidden] [zone-id]
    --help              display this help message
    --version           show version information and exit.
    --show-hidden       show all endpoints, not just ones for building links.
    zone-id             the id of the zone to provide endpoints for.
    """ % ( gCommand, gCommand ) )
    return(exitCode)
## end usage()

#
##############################################################################
# default_options()
# This routine must return any default options or actions that should
# be applied for command line options to override.
def default_options():

    result = {}

    # What databases and permissions are required?
    result['dbList'] = ["networking"]
    result['dbUserClass'] = "look"    # read only. read/update access = "user"

    # Assume the default is to display/list information.
    result['setArgs'] = False
    result['action'] = "list"

    # command line options:
    result['optionList'] = [ ("s", "show-hidden") ]

    # required arguments and count.
    result['requiredArgs'] = []
    result['minArgs'] = 0


    return(result)
## end default_options()

#
##############################################################################
# process_positional()
# This routine must process the positional command line arguments.
# cmdInfo - any command line flags and default options thus far.
# argList - the list of command line arguments not yet processed.
#           the count will be a minimum of minArgs as defined above.

def process_positional(cmdInfo, argList):

    result = {}

    result['argsUsed'] = 0
    if ( len(argList) >= 1 ):
        result['net_zone'] = int(argList[0])
        result['argsUsed'] += 1

    return(result)
## end process_positional()

#
##############################################################################
# Main

def main(cmdLineInfo, userDefaults):

    # initialize any tables here.
    network_info.initialize()

    # Drop into debugger if requested.
    if ( debug.get_debug() == -1 ):
        pdb.set_trace()

    # Establish a default for netZone
    if ( "net_zone" in cmdLineInfo ):
        netZone = cmdLineInfo['net_zone']
    else:
        netZone = -1

    # Get the list of endpoints.
    epList = network_info.get_endpoints(netZone, 
            showDetail=True, showHidden=cmdLineInfo['--show-hidden'])

    # display them.
    epTitle = "Network Core Endpoint/Termination Points"
    if ( netZone >= 0 ):
        epHead = ("Endpoint Code", "Priority")
        epKeys = ("ep_code", "core_priority")
    else:
        epHead = ("Network Zone", "Endpoint Code", "Priority")
        epKeys = ("net_zone", "ep_code", "core_priority")

    cmd_output.print_table(epTitle, epHead, epKeys, epList)

## end main()

#
##############################################################################
# Local function definitions...

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :
