#!/bin/bash

# Extract the version information from an existing git commit.
# The output can be used to insert into a file for a version variable.

PYTHON=python
tagBase=$1
fileName=$2
if [ -z ${3} ]; then
	ver_var=g_version
else
	ver_var=${3}
fi

Usage() {
	cmd=`basename $0`
	echo "Usage: $cmd base-name file-name [version-variable-name]"
	exit $1
}

if [ -z "$tagBase" ]; then
	Usage 1
fi

if [ -z "$fileName" ]; then
	Usage 2
fi

tagVersion="`git tag -l ${tagBase}'-*' | tail -1`"

if [ -z "$tagVersion" ]; then
	# echo "There doesn't seem to be a version for ${tagBase}"
	revisioncount=`git log --oneline | wc -l | awk '{print $1}'`
	new_ver_nbr=`git log --oneline | head -1 | cut -f1 '-d '`
	projectversion=`git describe --tags --long`
	cleanversion=${projectversion%%-*}
	tagVersion="$cleanversion.$new_ver_nbr build $revisioncount"
	fileDate="`date '+%d %b %Y'`"
else
	OLDIFS="$IFS"
	IFS=-
	set -- ${tagVersion}
	tagBaseName=$1
	tagDate=$2
	new_ver_nbr=$3
	IFS="$OLDIFS"

	tY=int\(\'`echo $tagDate | cut -c1-4`\'\)
	tM=int\(\'`echo $tagDate | cut -c5-6`\'\)
	tD=int\(\'`echo $tagDate | cut -c7-8`\'\)
	fileDate="`$PYTHON -c \"from datetime import date; d=date(${tY},${tM},${tD}) ; print(d.strftime('%d %b %Y'))\"`"

fi

SED1="s/^${ver_var}.*/${ver_var}=\'${fileDate} - ${new_ver_nbr}\'/"

if [ -r $fileName ]; then
	mv -f $fileName $fileName.old
	sed -e "$SED1" $fileName.old > $fileName
fi

