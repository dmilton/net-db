#!/bin/sh

ChangeDir=orig_files

# really simple tool to help make change from StudlyCaps function
# names to lower_case function names.

if [ ! -d $ChangeDir ]; then
	mkdir $ChangeDir
fi

for changeFile in cmd-template.py set-cmd-template.py *_mod.py; do
	if [ -f $changeFile -a $changeFile != new_fcn.py ]; then
		mv $changeFile $ChangeDir/$changeFile
		sed -f /opt2/src/net-db/libs/sed-changes-2.txt $ChangeDir/$changeFile\
		   	> $changeFile
	fi
done

