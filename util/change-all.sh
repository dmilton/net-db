#!/bin/sh

ChangeDir=new_files
RenameFcn=/opt2/src/net-db/function-rename.txt

StudlyCaps=$1
lower_case=$2

# really simple tool to help make change from StudlyCaps function
# names to lower_case function names.

if [ "X$StudlyCaps" = "X" ]; then
	echo "Usage: change-all orig new"
	#exit 1
fi

if [ "X$lower_case" = "X" ]; then
	echo "Usage: change-all orig new"
	#exit 1
fi

#CHANGE="s[$StudlyCaps[$lower_case[g"

if [ ! -d $ChangeDir ]; then
	mkdir $ChangeDir
fi

echo $CHANGE >> /opt2/src/net-db/function-rename.txt

for changeFile in LibModules *.py; do
	baseFile=`basename -s .py $changeFile`
	changeStr=`grep -- \\\[$baseFile $RenameFcn`
	newFile=`echo $changeStr | cut -d[ -f3`
	if [ -z "$newFile" ]; then
		newFile=$baseFile
	fi
	if [ -f $changeFile ]; then
		sed -f $RenameFcn $changeFile > $ChangeDir/$newFile.py
		echo git mv $changeFile $newFile.py
		grep '# vim:' $ChangeDir/$newFile.py > /dev/null
		if [ $? -ne 0 ]; then
			echo '\n# vim: syntax=python ts=4 sw=4 showmatch et :\n' >> \
				$ChangeDir/$newFile.py
		fi
	else
		echo changeFile: $changeFile
	fi
done

