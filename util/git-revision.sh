#!/bin/bash
revisioncount=`git log --oneline | wc -l | awk '{print $1}'`
git_revision=`git log --oneline | head -1 | cut -f1 '-d '`
projectversion=`git describe --tags --long`
cleanversion=${projectversion%%-*}

#echo "$projectversion-$revisioncount"
echo "$cleanversion.$git_revision build $revisioncount"

