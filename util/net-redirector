#!/bin/bash
# vim: ts=4 sw=4 et showmatch :

# Take some control over the path and what binary gets executed.
# Include only what is locally written and installed by packages here.
# Path crafted here to put the network utilities first, followed by
# the local install of postgresql, then os supplied and local supplied.
#	/usr/local/netdb/bin		- networking utilities.
#	/usr/local/pgsql/bin		- Postgres client binaries
#	/usr/bin:/usr/local/bin		- OS and locally built binaries.
#	/opt2/bin					- networking redirector path.
PATH="/usr/local/pgsql/bin:/usr/bin:/usr/local/bin:/usr/local/netdb/bin:"

OS="`uname -s`"
pgm=$(basename $0)

# Alter the path to allow overriding OS supplied versions with our own.
if [ $OS = "SunOS" ]; then
	# Change path for Solaris specific things.
	# So far, the above path covers everything.
	PATH="$PATH"
fi

if [ $OS = "Darwin" ]; then
	# Change path for MacOS specific things.
	# So far, the above path covers everything.
	PATH="/usr/local/netdb/bin:$PATH"
fi

case $pgm in
	bldg-info)
		pgm=show-bldg-info
		;;
	conserver|console)
		if [ -d /opt2/conserver -o -h /opt2/conserver ]; then
			export PATH="/opt2/conserver/bin:/opt2/conserver/sbin:$PATH"
		else
			echo $pgm: Command not found.
			exit 1
		fi
		;;
	dgipserv|drpadmin)
		if [ "$OS" = "SunOS" ]; then
			export PATH="/opt/realport/sparc64:/opt/realport"
		else
			echo "$pgm only available under Solaris"
			exit 0
		fi
		;;
	find-ip-info)
		echo "find-ip-info has been renamed to show-ip-info"
		pgm=show-ip-info
		;;
	find-mac-info)
		echo "find-mac-info has been renamed to show-mac-info"
		pgm=show-mac-info
		;;
	git)
		export PATH="/opt2/git/bin:$PATH"
		;;
	man|help|net-help)
		PAGES="/usr/share/man:/usr/local/share/man"
		PAGES="$PAGES:/usr/local/netdb/share/man"
		PAGES="$PAGES:/opt2/netdb/share/man:/opt2/conserver/share/man"
		PAGES="$PAGES:/opt2/python/share/man:/opt2/postgres/share/man"
		PAGES="$PAGES:/opt2/git/share/man"
		pgmArgs="-M $PAGES"
		if [ $pgm = "help" -o $pgm = "net-help" ]; then
			pgmArgs="$pgmArgs net-help"
		else
			pgmArgs="-M $PAGES $@"
		fi
		pgm="/usr/bin/man"
		;;
	pg_config|psql|psqldev|psqldev2)
		# Defaults, production server.
		export PGHOST=pgprod.cc.umanitoba.ca
		if [ "X" != "X$1" ]; then
			export PGDATABASE=$1
		else
			export PGDATABASE=networking
		fi
		# Make changes for development servers.
		if [ $pgm = "psqldev" -o $pgm = "psqldev2" ]; then
			# Argument 1 is potentially the database name:
			export PGHOST=pgdevl.cc.umanitoba.ca
			pgm=psql
		fi
		# Different databases have different users and ports:
		case $PGDATABASE in
			networking) 	export PGPORT=5447 ; export PGUSER=netmon ;;
			netdisco)		export PGPORT=5446 ; export PGUSER=netdisco ;;
			intermapper)	export PGPORT=5444 ; export PGUSER=imdatabase ;;
			bcan)			export PGPORT=5441 ; export PGUSER=netview ;;
		esac
		if [ $pgm = "psqldev2" ]; then
			# Different instance, new db version
			export PGPORT=5450
		fi
		;;
esac

# Check that we aren't in an infinite loop:
WHICH=`/usr/bin/which $pgm | grep -v /opt2/bin/$pgm`
if [ -z "$WHICH" ]; then
	echo $pgm: Command not found
	exit 1
fi
if [ -z "$pgmArgs" ]; then
	exec $pgm "$@"
else
	exec $pgm $pgmArgs
	echo exec $pgm $pgmArgs
fi

# vim: syntax=sh ts=4 sw=4 tw=0 noet :
