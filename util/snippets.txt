# To find a tag matching a pattern:
% git tag --list 'assign-afl*'
assign-afl-1.0.7

If you have imported some changes from another VCS and would like to
add tags for major releases of your work, it is useful to be able to
specify the date to embed inside of the tag object; such data in the
tag object affects, for example, the ordering of tags in the gitweb
interface.

To set the date used in future tag objects, set the environment
variable GIT_COMMITTER_DATE (see the later discussion of possible
values; the most common form is "YYYY-MM-DD HH:MM"). For example:
	$ GIT_COMMITTER_DATE="2006-10-02 10:31" git tag -s v1.0.1

# To delete a tag.
git tag -d assign-afl-1.0.7

# To update/replace a tag.
git tag 

