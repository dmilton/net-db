#!/bin/sh

cd /usr/local/src/net-db/cmds
# This command assumes that all commands are in the current directory.
PATH=./:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin

# List of tests at bottom of script.
# Shortcomings:
# Passing garbage/invalid values to the commands is not tested throroughly.

# Use test/local database
NET_DEBUG=-2
NET_DBSERV=2

# Test network type
TYPE_NAME='clsrm'
TYPE_BASE_VLAN='-280'
TYPE_EXT='-clsrm'
TYPE_REQD=False
TYPE_FAMILY='ip'
TYPE_ASSIGN_PRI='50'
TYPE_ASSIGN_METHOD=calc
TYPE_TABLE_LIST=network_bldgs,network_free
TYPE_TABLE_LIST2=network_bldgs,network_free
TYPE_TABLE_LIST3=network_iso,None
TYPE_IS_REQUIRED=f
BASE_VLAN_TAG='-666'
TYPE_DESCRIPTION='ISO used for IS-IS routing on loopback.'
TYPE_PREFIXES='64,64'
TYPE_NAME_BAD=invalidType

# This variable should go away.
TYPE_USE_PRIVATE=True

# Test zone
ZONE_NAME="netdb zone"
ZONE_ID=777
ZONE_ID_BAD=666
# The test campus.
CAMPUS_NAME="netdb test campus"
CAMPUS_BAD_ID=666
# Address space for the zone.
TEST_V4_MAIN=172.25.0.0/18
TEST_V4_MAIN_S1="--type=user --vlan=40 172.25.0.0/18 19"
TEST_V4_MAIN_S2="172.25.0.0/19 20"
TEST_V4_MAIN_S3="172.25.0.0/20 21"
TEST_V4_MAIN_S4="172.25.0.0/21 22"
TEST_V4_MAIN_S5="172.25.0.0/22 23"
TEST_V4_MAIN_S6="172.25.0.0/23 24"
# Link and loop networks.
TEST_V4_MAIN_S7="--type=link --vlan=401 172.25.0.0/24 25"
TEST_V4_MAIN_S8="172.25.0.0/25 26"
TEST_V4_MAIN_S9="--type=loop --vlan=1 172.25.0.0/26 27"
TEST_V4_MAIN_S9="172.25.0.0/27 32"
TEST_V4_MAIN_Sa="--type=link --vlan=401 172.25.0.64/26 30"
# protected networks.
TEST_V4_MAIN_Sb="--type=prot --vlan=280 172.25.4.0/22 24"
TEST_V4_MAIN_Sc="172.25.4.0/24 28"
TEST_V4_MAIN_Sd="--type=prot 172.25.4.0/22 24"
# user networks.
TEST_V4_MAIN_Se="--type=user --vlan=40 172.25.32.0/19 22"
TEST_V4_MAIN_Sf="172.25.32.0/22 24"
TEST_V4_MAIN_Sg="172.25.36.0/22 24"
# management and voip networks.
TEST_VV4_MGMT="172.25.64.0/18"
TEST_VV4_VOIP="172.25.128.0/18"
#
TEST_V6_MAIN=2001:DB8:0000::/48
TEST_V6_MGMT=2001:DB8:8000::/48
TEST_V6_VOIP=2001:DB8:4000::/48
#
TEST_EP1="netdb-core1"
TEST_EP2_NAME="Test Campus Core Router 1"
#
TEST_EP2="netdb-core2"
TEST_EP2_NAME="Test Campus Core Router 2"
#
TEST_BLDG="CmdTest-v4"
TEST_BLDG_NAME="IPv4 Test Building"
#
TEST_BLDG_6="CmdTest-v6"
TEST_BLDG_NAME_6="IPv6 Test Building"
#
TEST_BLDG_64="CmdTest-v64"
TEST_BLDG_NAME_64="IPv4 + IPv6 Test Building"
TEST_BLDG_64_ROOM_1="v6 MDC"
TEST_BLDG_64_ROOM_2="v6 VDR"
#

# Run command and conditionally based on the arguments.
# Need to check for successful command completion and also for
# command completion that failed. This way error checking gets verified.
# All commands set an exit value of 0 on success, non-zero on failure.
# $1 = the exit value of the command.
# $2 Exit when the 'failed' condition is detected. This means for a command
# where an error should be generated, $2 = 0. For a command where no error
# should be generated then $2 = 1.
# $3 = the line number where this function was called from.
# $4 = the error message to display.
run_and_check() {
	cmd_to_test="$1"
	normal_exit=$2
	check_line=$3
	error_msg=$4
	arg_ctr=$#

	# Run the command and capture the exit value.
	echo '##########################################################'
	echo $cmd_to_test
	echo Argument count $arg_ctr
	echo '##########################################################'
	$cmd_to_test
	exit_value=$?

	fail=0
	if [ $normal_exit -ne 0 -a $exit_value -eq 0 ]; then
		# the command should have failed but returned success.
		fail=1
		err_msg="The command on line $check_line did not generate an error."
	fi
	if [ $normal_exit -eq 0 -a $exit_value -ne 0 ]; then
		# the command should have succeeded but returned failure.
		fail=1
		err_msg="The command on line $check_line failed. Exit $exit_value"
	fi

	if [ $fail -eq 1 ]; then
		echo "########################################################"
		echo $err_msg
		[ "X$error_message" = "X" ] || echo $error_message
		echo "########################################################"
		exit $exit_value
	fi
}

step_check_exit() {
	if [ $1 -eq $2 ]; then
		exit 0
	fi
}

run_test() {
	RUN_STEP=$1
	RUN_CLEANUP=$2
	case $RUN_STEP in
		1)	[ $RUN_CLEANUP -eq 0 ] && test_bldg_type
			[ $RUN_CLEANUP -eq 0 ] || cleanup_bldg_type
			;;
		1)		# can run standalone.
			[ $RUN_CLEANUP -eq 0 ] && test_net_type
			[ $RUN_CLEANUP -eq 0 ] || cleanup_type
			;;
		2)		# can run standalone.
			[ $RUN_CLEANUP -eq 0 ] && test_campus
			[ $RUN_CLEANUP -eq 0 ] || cleanup_campus
			;;
		3)		# can run standalone.
			[ $RUN_CLEANUP -eq 0 ] && test_zone
			[ $RUN_CLEANUP -eq 0 ] || cleanup_zone
			;;
		4)		# can run standalone.
			[ $RUN_CLEANUP -eq 0 ] && test_user
			[ $RUN_CLEANUP -eq 0 ] || cleanup_user
			;;
		5)		# requires a campus and a zone.
			[ $RUN_CLEANUP -eq 0 ] && test_building
			[ $RUN_CLEANUP -eq 0 ] || cleanup_building
			;;
		6)		# requires a campus, zone, and core building.
			[ $RUN_CLEANUP -eq 0 ] && test_building
			[ $RUN_CLEANUP -eq 0 ] || cleanup_building
			;;
		7)		# requires a campus, zone, and building.
			[ $RUN_CLEANUP -eq 0 ] && test_user_building
			[ $RUN_CLEANUP -eq 0 ] || cleanup_user_building
			;;
		8)		# requies a camus, zone, and user building.
			[ $RUN_CLEANUP -eq 0 ] && test_building_room
			[ $RUN_CLEANUP -eq 0 ] || cleanup_building_room
			;;
		9)
			[ $RUN_CLEANUP -eq 0 ] && test_rooms
			[ $RUN_CLEANUP -eq 0 ] || cleanup_rooms
			;;
		# Step  6 = end user buildings creation.
		# Step  7 = rooms added to the building.
		# Step  8 = add networks to the database, pre-allocated ones.
		# Step  9 = split and merge networks.
		# Step 10 = assign user network to end user building.
		# Step 11 = assign all network types to end user building.
		# Step 12 = network show/set commands
	esac
}

test_help_ver() {
	bldg_type_help_ver
	net_type_help_ver
	campus_help_ver
	zone_help_ver
	bldg_help_ver
}

bldg_type_help_ver() {
	set -e
	show-bldg-types --version
	set-bldg-type --version
	show-bldg-types --help
	set-bldg-type --help
	set-bldg-type --keys
	set +e
}

test_bldg_type() {
	echo "########################################################"
	echo "# Testing building type commands."
	echo "########################################################"

	# Check for type already exists, description already exists
	# and id already exists.

	# This doesn't exist, it should generate an error.
	FULL_CMD="show-bldg-types CE"
	run_and_check "$FULL_CMD" 1 $LINENO 

	# Add a type that already exists, it should generate an error.
	FULL_CMD="set-bldg-type office 'end users present in office buildings'"
	run_and_check "$FULL_CMD" 1 $LINENO

	# Add some incorrect but new information.
	FULL_CMD="set-bldg-type PE 'Somebody Edge'"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# now fix the incorrect information.
	FULL_CMD="set-bldg-type --update PE 'Provider Edge'"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# display the newly added and corrected information.
	FULL_CMD="show-bldg-types PE"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Add a building type to test delete.
	FULL_CMD="set-bldg-type delete-me 'delete-me testing'"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Delete our target building type.
	FULL_CMD="set-bldg-type --delete delete-me"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Delete an invalid building type.
	FULL_CMD="set-bldg-type --delete invalid"
	run_and_check "$FULL_CMD" 1 $LINENO 

	# Now generate the entire list of building types.
	FULL_CMD="show-bldg-types"
	run_and_check "$FULL_CMD" 0 $LINENO 
}

test_net_type() {
	echo "########################################################"
	echo "# Testing network type commands."
	echo "########################################################"

	# Check for invalid assignment method, invalid prefix size,
	# invalid building types, duplicate network type.

	# Type classroom
	FULL_CMD="./set-net-type $TYPE_NAME base-vlan-tag $TYPE_BASE_VLAN \
		name-extension $TYPE_EXT type-required $TYPE_REQD \
		net-family $TYPE_FAMILY"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Update almost everything.
	FULL_CMD="./set-net-type --update $TYPE_NAME \
		assign-method $TYPE_ASSIGN_METHOD net_table_list $TYPE_TABLE_LIST \
		name-extension \"$TYPE_EXT\" assign-priority $TYPE_ASSIGN_PRI \
		type-required $TYPE_IS_REQUIRED base-vlan-tag $BASE_VLAN_TAG"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Update the prefixes
	FULL_CMD="./set-net-type --update $TYPE_NAME 
		prefix-size "$TYPE_PREFIXES""
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Update the description. The next test doesn't work properly. When
	# passed to run_and_check the description gets broken up into multiple
	# arguments causing the script to fail with:
	#	ValueError: invalid input keyword 'used'
	# Running the command manually from the command line works. It seems
	# to have something to do with the function call but I'm not sure what.
	FULL_CMD="./set-net-type --update $TYPE_NAME \
	   	type-descr \"$TYPE_DESCRIPTION\"" 
	#run_and_check "$FULL_CMD" 0 $LINENO 
	./set-net-type --update $TYPE_NAME type-descr "$TYPE_DESCRIPTION" 
	STATUS=$?
	if [ $STATUS -ne 0 ]; then
		echo $FUL_CMD
		echo Line near $LINENO failed with exit $STATUS
		exit $STATUS
	fi

	# Show what has been done above.
	FULL_CMD="./show-net-types $TYPE_NAME"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Now generate some errors - non-existant type.
	FULL_CMD="./set-net-type --update $TYPE_NAME_BAD assign_method calc \
		net_table_list $TYPE_TABLE_LIST2"
	run_and_check "$FULL_CMD" 1 $LINENO 

	# Change the network table list and assignment method.
	FULL_CMD="set-net-type $TYPE_NAME assign_method $TYPE_ASSIGN_METHOD\
	   	net_table_list $TYPE_TABLE_LIST3"
	run_and_check "$FULL_CMD" 1 $LINENO 

	# Cleanup after our testing.
	FULL_CMD="set-net-type --delete $TYPE_NAME"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Now try and delete something that doesn't exist:
	run_and_check "$FULL_CMD" 1 $LINENO 
	
	echo "########################################################"
}

net_type_help_ver() {
	set -e
	show-net-types --help
	show-net-types --version
	set-net-type --help
	set-net-type --version
	set-net-type --keys
	set +e
}

cleanup_type() {

	# Cleanup after our testing.
	FULL_CMD="set-net-type --delete $TYPE_NAME"
	run_and_check "$FULL_CMD" 0 $LINENO 

	# Now try and delete something that doesn't exist:
	run_and_check "$FULL_CMD" 0 $LINENO 

	echo "########################################################"
}

test_campus() {
	echo "########################################################"
	echo "# Testing network campus commands."
	echo "########################################################"

	# test duplicate campus (CaMeL cAsE), duplicate campus_id.

	create_test_campus
	echo The campus added has an id of $ADDED_CAMPUS_ID

	# Add the same campus again, should generate an error.
	FULL_CMD="./set-campus-info "$CAMPUS_NAME""
	run_and_check "$FULL_CMD" 1 $LINENO \
			"Adding a duplicate campus did not fail."

	echo Attempting to show a campus that does not exist.
	FULL_CMD="./show-campus-info $CAMPUS_BAD_ID"
	run_and_check "$FULL_CMD" 1 $LINENO \
			"Display of a non-existant campus did not fail."

	echo Display the campus added above.
	# now display what we have.
	FULL_CMD="./show-campus-info $ADDED_CAMPUS_ID"
	run_and_check "$FULL_CMD" 0 $LINENO \
			"showing a valid campus failed."

	echo "########################################################"
	echo "# First phase of campus test commands has passed."
	echo "########################################################"

}

campus_help_ver() {
	set -e
	./show-campus-info --version
	./show-campus-info --help
	./set-campus-info --version
	./set-campus-info --help
	./set-campus-info --keys
	set +e
}

get_test_campus_id() {
	# see if the campus is already defined:
	TEST_CAMUPS_ID=`./show-campus-info | grep -i "$CAMPUS_NAME" | cut -f2 '-d|'`
}

create_test_campus() {
	# Get the test campus id.
	get_test_campus_id

	if [ "X$TEST_CAMPUS_ID" = "X" ]; then
		# New campus.
		FULL_CMD="./set-campus-info \"$CAMPUS_NAME\""
		run_and_check "$FULL_CMD" 0 $LINENO 
	fi

	# Figure out the campus id so we can delete it later.
	get_test_campus_id

	# The above always returns an exit code of 1 so must test for
	# an empty string instead.
	if [ "X$TEST_CAMPUS_ID" = "X" ]; then
		echo Unable to retrieve the test campus.
		exit 1
	fi
}

cleanup_campus() {
	if [ "X$TEST_CAMPUS_ID" = "X" ]; then
		# Remove the campus created above:
		FULL_CMD="./set-campus-info --delete $TEST_CAMPUS_ID"
		run_and_check "$FULL_CMD" 0 $LINENO \
			"deleting a campus failed."
		echo "Test campus ID $TEST_CAMPUS_ID has been deleted"
		echo "########################################################"
	fi
}

test_zone() {
	echo "########################################################"
	echo "# Testing network zone commands."
	echo "########################################################"

	echo "# create a new network zone."
	create_test_zone

	# Renumber a zone that doesn't exist:
	FULL_CMD="./set-zone-info --renumber=$ZONE_ID $ZONE_ID_BAD"
	run_and_check "$FULL_CMD" 1 $LINENO \
		"renumbering a zone which does not exist succeeded."

	# Renumber a zone to one that already exists:
	FULL_CMD="./set-zone-info --renumber=$TEST_ZONE_ID 0"
	run_and_check "$FULL_CMD" 1 $LINENO \
		"renumbering a zone to one that exists succeeded."

	# Add a zone that exists
	FULL_CMD="./set-zone-info \"$ZONE_NAME\""
	run_and_check "$FULL_CMD" 1 $LINENO \
		"adding a zone that exists did not fail."

	# update a zone that does not exist.
	FULL_CMD="./set-zone-info --update $ZONE_ID_BAD \"$ZONE_NAME\""
	run_and_check "$FULL_CMD" 1 $LINENO \
		"updating an undefined zone succeeded."

	# update a zone that does exist
	FULL_CMD="./set-zone-info --update $ZONE_ID \"$ZONE_NAME\""
	run_and_check "$FULL_CMD" 0 $LINENO "updating an existing zone failed."

	# add a network range to a zone that does not exist
	FULL_CMD="./set-range-info $TEST_V4_MAIN net-zone $ZONE_ID_BAD"
	run_and_check "$FULL_CMD" 1 $LINENO \
		"adding an address range to an undefined zone succeeded."

	# add a network range for user networks.
	FULL_CMD="./set-range-info $TEST_V4_MAIN net-zone $TEST_ZONE_ID"
	run_and_check "$FULL_CMD" 0 $LINENO \
		"adding an address range to a zone failed."

	# updating an address range description.
	FULL_CMD="./set-range-info --update $TEST_V4_MAIN range-description \
		\"user and pre-allocated network types come from here.\""
	run_and_check "$FULL_CMD" 0 $LINENO \
		"adding an address range description failed."

	# Add a network range for device management.
	FULL_CMD="./set-range-info $TEST_V4_MGMT net-zone $TEST_ZONE_ID \
		net-type-list mgmt"
	run_and_check "$FULL_CMD" 0 $LINENO \
		"adding an address range failed."

	# Renumber the zone to our desired ID.
	FULL_CMD="./set-zone-info --renumber=$ZONE_ID $TEST_ZONE_ID"
	run_and_check "$FULL_CMD" 0 $LINENO \
		"renumbering a zone failed."

	# We should now be able to see that the management space above
	# has been renumbered along with the zone description.
	RENUMBERED_RANGE_ID=`./show-zone-info $TEST_V4_MGMT | \
		grep 'Network Zone' | cut -f2 -d:`
	if [ $RENUMBERED_RANGE_ID -ne $ZONE_ID ]; then
		echo "renumbering a zone did not renumber the address range."
		exit 1
	fi
	
	# Add address ranges for testing allocation commands:
	set -e
	./set-range-info $TEST_V4_VOIP net-zone $ZONE_ID net-type-list voip \
		range-descr "voip networks assigned from here"

	# IPv6 address spaces for the test zone
	./set-range-info $TEST_V6_MAIN net-zone $ZONE_ID range-descr \
		"user, loopback, link, and special types assigned from here"

	./set-range-info $TEST_V6_MGMT net-zone $ZONE_ID net-type-list mgmt

	./set-range-info --update $TEST_V6_MGMT \
		range-descr "management networks assigned from here"

	./set-range-info $TEST_V6_VOIP net-zone $ZONE_ID net-type-list voip

	./set-zone-info --update $TEST_V6_VOIP range-descr \
		"voip networks assigned from here"

	set +e

	# now list the zones.
	echo "# display our handiwork in the new zone."
	FULL_CMD="./show-zone-info --net-zone $ZONE_ID"
	run_and_check $FULL_CMD 0 $LINENO \
		"The show-zone-info command should have succeeded."

	# Now request an invalid zone.
	echo "# display an invalid zone."
	FULL_CMD="./show-zone-info --net-zone $ZONE_ID_BAD"
	run_and_check $FULL_CMD 0 $LINENO \
		"showing an invalid zone did not fail."

	echo "# delete an invalid zone."
	FULL_CMD="./set-zone-info --delete $ZONE_ID_BAD"
	run_and_check "$FULL_CMD" 1 $LINENO \
		"deleting an invalid zone succeeded."
}

zone_help_ver() {
	set -e
	./show-zone-info --version
	./show-zone-info --help
	./set-zone-info --version
	./set-zone-info --help
	./set-zone-info --keys
	./set-range-info --version
	./set-range-info --help
	./set-range-info --keys
	set +e
}

get_test_zone_id() {
	TEST_ZONE_ID=`./show-zone-info --brief | \
		grep -i "$ZONE_NAME" | cut -f2 '-d|'`
}

create_test_zone() {
	# Create a zone to test with if it doesn't already exist.
	get_test_zone_id
	if [ "X$TEST_ZONE_ID" = "X" ]; then
		FULL_CMD="./set-zone-info \"$ZONE_NAME\""
		run_and_check "$FULL_CMD" 0 $LINENO \
			"creating a zone failed."
	fi

	get_test_zone_id
	if [ "X$TEST_ZONE_ID" = "X" ]; then
		echo Unable to retrieve the test zone.
		exit 1
	fi
}

cleanup_zone() {
	FULL_CMD="./set-zone-info --delete --recurse $ZONE_ID"
	run_and_check "$FULL_CMD" 0 $LINENO \
		"recursize zone delete failed."
}

set_bldg_test() {
	echo "########################################################"
	echo "# Testing building management commands."
	echo "########################################################"
	FULL_CMD="./set-bldg-info TeSt name 'Test Building' net-zone 0"
	run_and_check "$FULL_CMD" 0 $LINENO "create test building failed"

	FULL_CMD="./set-bldg-info --update TeSt campus-id $ADDED_CAMPUS_ID \
		ospf-area 666 core-bldg F \
		ipv6-netid -1 ipv6-nextnet 0 map-code '' \
		net-zone -1 next-prot-vlan 280 next-user-vlan 40 \
		ospf-area -1 router norouter web-visible false"
	run_and_check "$FULL_CMD" 0 $LINENO "update test building failed"

	FULL_CMD="./set-bldg-info --delete TeSt"
	run_and_check "$FULL_CMD" 0 $LINENO "delete test building failed."
}

create_ep_bldgs() {
	echo "########################################################"
	echo "# Testing building management commands (for endpoints)"
	echo "########################################################"
	# create core endpoint building(s) for the test zone.
	# Building code for parking free networks.
	set-bldg-info 00 name 'default building' net-zone -1
	set-bldg-info --update 00 campus-id $ADDED_CAMPUS_ID ospf-area 666
	# This command needs to be rewritten to use the new set-cmd-template.
	set-bldg-info $TEST_EP1 net-zone $ZONE_ID name "$TEST_EP1_NAME"
	set-bldg-info --update $TEST_EP1 campus-id $CAMPUS_NAMEID \
		switch-ct 2 visible F
	set-bldg-info --update $TEST_EP1 router "$TEST_EP1-router"
	set-bldg-info $TEST_EP2 net-zone $ZONE_ID name "$TEST_EP2_NAME"
	set-bldg-info --update $TEST_EP2 campus-id $CAMPUS_NAMEID \
		switch-ct 2 visible F
	set-bldg-info --update $TEST_EP2 router "$TEST_EP2-router"
	show-bldg-info $TEST_EP1
	show-bldg-info $TEST_EP2
	show-bldg-info --name $TEST_EP2
	show-bldg-info --net-zone $TEST_EP1
	show-bldg-info --nets $TEST_EP1
	show-bldg-info --find netdb
	show-bldg-info --find $TEST_EP1
	show-bldg-info --find not-found

	step_check_exit $STEP_NBR 4
}

bldg_help_ver() {
	set -e
	./show-bldg-info --version
	./show-bldg-info --help
	./set-bldg-info --version
	./set-bldg-info --help
	./set-bldg-info --keys
	set +e
}


create_endpoints() {

	echo "########################################################"
	echo "# Testing network endpoint commands."
	echo "########################################################"
	# Now make these endpoints. Priority indicates which one to use first
	# when assigning links from buildings into the core.
	set-ep-info $TEST_EP1 priority 20
	set-ep-info --update $TEST_EP1 priority 10
	set-ep-info $TEST_EP2 priority 10
	set-ep-info --update $TEST_EP2 priority 20
	# display the endpoints for the zone.
	show-ep-info $ZONE_ID

	if [ $STEP_NBR -eq 5 ]; then
		exit 0
	fi
}

ep_help_ver() {
	set -e
	show-ep-info --version
	show-ep-info --help
	set-ep-info --version
	set-ep-info --help
	set-ep-info --keys
	set +e
}

test_office_bldg() {
	echo "########################################################"
	echo "# Testing building commands for office buildings."
	echo "########################################################"
	# create three buildings in our test campus and zone.
	set-bldg-info $TEST_BLDG net-zone $ZONE_ID name "$TEST_BLDG_NAME"
	set-bldg-info --update $TEST_BLDG campus-id $CAMPUS_NAMEID switch-ct 4
	set-bldg-info --update $TEST_BLDG router "$(TEST_BLDG)b1"
	show-bldg-info $TEST_BLDG
	#
	set-bldg-info $TEST_BLDG_4 net-zone $ZONE_ID name "$TEST_BLDG_NAME_4"
	set-bldg-info --update $TEST_BLDG_4 campus-id $CAMPUS_NAMEID switch-ct 4
	set-bldg-info --update $TEST_BLDG_4 router "$(TEST_BLDG_4)b1"
	show-bldg-info $TEST_BLDG_4
	#
	set-bldg-info $TEST_BLDG_6 net-zone $ZONE_ID name "$TEST_BLDG_NAME_6"
	set-bldg-info --update $TEST_BLDG_6 campus-id $CAMPUS_NAMEID switch-ct 4
	set-bldg-info --update $TEST_BLDG_6 router "$(TEST_BLDG_6)b1"
	show-bldg-info $TEST_BLDG_6

}

test_net_info() {
	echo "########################################################"
	echo "# Testing network information commands."
	echo "########################################################"
	# Add the main IPv4 network and break it up for type use.
	show-net-info --version
	show-net-info --help
	set-net-info --version
	set-net-info --help
	set-net-info --keys
	set-net-info bogon user $TEST_V4_MAIN 00 net-zone $ZONE_ID
	set-net-info --update bogon 
}

test_split_net() {
	echo "########################################################"
	echo "# split network tests."
	echo "########################################################"
	# Split the network for use.
	split-net --version
	split-net --help
	split-net $TEST_V4_MAIN_S1
	split-net $TEST_V4_MAIN_S2
	split-net $TEST_V4_MAIN_S3
	split-net $TEST_V4_MAIN_S4
	split-net $TEST_V4_MAIN_S5
	split-net $TEST_V4_MAIN_S6
	split-net $TEST_V4_MAIN_S7
	split-net $TEST_V4_MAIN_S8
	split-net $TEST_V4_MAIN_S9
	split-net $TEST_V4_MAIN_Sa
	split-net $TEST_V4_MAIN_Sb
	split-net $TEST_V4_MAIN_Sc
	split-net $TEST_V4_MAIN_Sd
	split-net $TEST_V4_MAIN_Se
	split-net $TEST_V4_MAIN_Sf
	split-net $TEST_V4_MAIN_Sg
}


# Allow step through the tests. Code will exit after completion of
# the appropriate step number. Steps will not execute unless their
# number has been identified so always run in order.
# Step  0 = run everything.
# Step  1 = building types
#			Check for type already exists, description already exists
#			and id already exists.
# Step  2 = network types
# Step  3 = campus
#			check for duplicates, duplicate campus_id,
# Step  4 = zone
# Step  5 = core buildings
# Step  6 = make core buildings into endpoints
#			check for endpoints that don't have buildings.
#			check for buildings that aren't type 'core'
#			or type 'dc'. check for duplicates.
# Step  7 = end user buildings creation.
#			check for duplicate bldg_code, invalid campus,
#			duplicate bldg_name, invalid bldg_type,
#			invalid net_zone
# Step  8 = rooms added to the building.
#			check for CaMeL case room number duplicate,
#			duplicate room code, invald bldg_code.
# Step  9 = add networks to the database, pre-allocated ones.
# Step 10 = split and merge networks.
# Step 11 = assign user network to end user building.
# Step 12 = assign all network types to end user building.
# Step 13 = network show/set commands

THIS_SCRIPT=$0
TEST_CODE=$1
# run_test RUN_STEP RUN_CLEANUP
test_help_ver
if [ "${TEST_CODE}X" = "X" ]; then
	STEP_NBR=1
	run_test 1 1
else
	run_test $TEST_CODE 1
fi

