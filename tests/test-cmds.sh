#!/bin/sh

cd /usr/local/src/net-db/cmds

# Run debug to prevent tracebacks from being mailed.
NET_DEBUG=-2
# Use local database
NET_DBSERV=2
export NET_DEBUG NET_DBSERV

# This command assumes that all commands are in the current directory.
PATH=.:/usr/local/bin:/bin:/usr/bin:/sbin:/usr/sbin

CMD_LIST="show-net-types set-net-type show-bldg-types set-bldg-type \
	show-zone-info set-zone-info set-range-info \
	show-campus-info set-campus-info \
	show-bldg-info set-bldg-info set-room-info \
	show-ep-info set-ep-info \
	show-net-info set-net-info \
	show-free-nets free-net split-net merge-nets \
	assign-user assign-nets assign-2nd assign-vlan \
	show-ip-info show-mac-info show-room-ports \
	"
# set-afl-info show-afl-info assign-afl activate-afl \
# check-bldg-dns check-zone-ns chk-nets
# show-digi-info set-digi-info
# set-user-defaults
# set-column-info
# show-sensor-info


test_set_afl_info() {
	set-afl-info --help
	set-afl-info --version
	set-afl-info --keys
	# set-afl-info gavu-ipit-JuNt-npJe RTU0000000739671 EX-24-AFL
}

test_show_afl_info() {
	show-afl-info --help
	show-afl-info --version
}

test_assign_afl() {
	assign-afl --help
	assign-afl --version
}

test_activate_afl() {
	activate-afl --help
	activate-afl --version
}

test_set_bldg_type() {
	set-bldg-type --help
	set-bldg-type --version
	set-bldg-type --keys
	set-bldg-type PE
	set-bldg-type "Provider Edge"
	set-bldg-type PE "Edge"
	set-bldg-type --update PE "Provider Edge"
}

test_show_bldg_types() {
	show-bldg-types --help
	show-bldg-types --version
	show-bldg-types
	show-bldg-types core
	show-bldg-types CE
}

test_set_net_type() {
	set-net-type --help
	set-net-type --version
	set-net-type --keys
}

test_show_net_types() {
	show-net-types --help
	show-net-types --version
	show-net-types
	show-net-types loop
	show-net-types --detail 2nd
	show-net-types invalid
	show-net-types --detail invalid
}

test_set_zone_info() {
	set-zone-info --help
	set-zone-info --version
	set-zone-info --keys
}

test_set_range_info() {
	set-range-info --help
	set-range-info --version
	set-range-info --keys
}

test_show_zone_info() {
	show-zone-info --help
	show-zone-info --version
}

test_set_campus_info() {
	set-campus-info --help
	set-campus-info --version
	set-campus-info --keys
}

test_show_campus_info() {
	show-campus-info --help
	show-campus-info --version
}

test_set_bldg_info() {
	set-bldg-info --help
	set-bldg-info --version
	set-bldg-info --keys
}

test_set_room_info() {
	set-room-info --help
	set-room-info --version
	set-room-info --keys
}

test_show_bldg_info() {
	show-bldg-info --help
	show-bldg-info --version
}

test_set_ep_info() {
	set-ep-info --help
	set-ep-info --version
	set-ep-info --keys
}

test_show_ep_info() {
	show-ep-info --help
	show-ep-info --version
}

test_set_net_info() {
	set-net-info --help
	set-net-info --version
	set-net-info --keys
}

test_show_net_info() {
	show-net-info --help
	show-net-info --version
}

test_show_free_nets() {
	show-free-nets --help
	show-free-nets --version
}

test_free_net() {
	free-net --help
	free-net --version
}

test_split_net() {
	split-net --help
	split-net --version
}

test_merge_nets() {
	merge-nets --help
	merge-nets --version
}

test_assign_user() {
	assign-user --help
	assign-user --version
}

test_assign_nets() {
	assign-nets --help
	assign-nets --version
}

test_assign_2nd() {
	assign-2nd --help
	assign-2nd --version
}

test_assign_vlan() {
	assign-vlan --help
	assign-vlan --version
}

test_show_ip_info() {
	show-ip-info --help
	# show-ip-info --version
}

test_show_mac_info() {
	show-mac-info --help
	# show-mac-info --version
}

test_show_room_ports() {
	show-room-ports --help
	# show-room-ports --version
}

# Simple loop to run the tests.
set -e
for aCommand in $CMD_LIST; do
	macro_name=`echo test_$aCommand | tr '-' '_'`
	echo
	echo Testing $aCommand
	echo
	$macro_name
done

