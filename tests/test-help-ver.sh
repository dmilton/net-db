#!/bin/sh
# vim: syntax=sh ts=4 sw=4 showmatch et :

# Assume all commands and modules are in the TEST_BIN directory.
TEST_BIN=/usr/local/src/net-db/cmds
PATH=${TEST_BIN}:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin

# Netdisco db extract commands.
CMD_NETDISCO_INFO="show-ip-info show-mac-info show-room-ports"

# List of commands that show tables from the db.
CMD_LIST_INFO="show-afl-info show-bldg-info show-campus-info \
	show-ep-info show-net-info show-net-types show-bldg-types \
    show-zone-info show-free-nets"

# List of commands that update tables from the db.
CMD_LIST_SET="set-afl-info set-bldg-info set-room-info set-campus-info \
	set-ep-info set-net-info set-net-type set-bldg-type \
    set-zone-info set-range-info"

# list of commands that assign or manipulate db information.
CMD_LIST_UPDATE="assign-afl activate-afl free-net split-net merge-nets \
	assign-user assign-nets assign-2nd assign-vlan"

# check-bldg-dns check-zone-ns chk-nets
# show-digi-info set-digi-info
# set-defaults
# set-column-info
# show-sensor-info

# Run debug to prevent tracebacks from being mailed.
NET_DEBUG=-2
export NET_DEBUG
# Use local database
NET_DBSERV=2
export NET_DBSERV

cd $TEST_BIN

CMD_LIST_BASIC="$CMD_LIST_INFO $CMD_LIST_UPDATE"
for aCommand in $CMD_LIST_BASIC; do
    # Print a header for this command check.
	echo "\n--\tchecking $aCommand\n--\n"
    # check that the man page is installed.
	man -w $aCommand
    # Now run the command for version and help, exiting for any error.
	set -e
	$aCommand --version
	echo "--"
	$aCommand --help
	set +e
	# sleep 1
done

for aCommand in $CMD_LIST_SET; do
    # Print a header for this command check:
	echo "\n--\t$aCommand"
    # check that the man page in installed.
	man -w $aCommand
    # Now run the command, exiting immediately on any error.
	set -e
	$aCommand --version
	echo "--"
	$aCommand --help
	echo "--"
	$aCommand --keys
	set +e
	# sleep 1
done

