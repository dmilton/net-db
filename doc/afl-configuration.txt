Subject:	HOWTO: get/set AFL licenses on Juniper EX-series routers
Date:	Mon, 23 Jun 2014 14:57:41 -0500
From:	Daryl Fonseca-Holt <Daryl.Fonseca-Holt@umanitoba.ca>
To:	Daryl Fonseca-Holt <Daryl.Fonseca-Holt@umanitoba.ca>

Obtain Authorization Code <au> and RTU Serial Number <rtu> and Model 
Number <mod#>from the Purchase Order.

Add to the Network database via oberon login:
    set-afl-info <au> <rtu> <mod#>

    Ex: set-afl-info e8Mu-itNr-rene-rrri RTU0000000943628 EX-24-AFL

Repeat above as needed for each RTU in the order.

Assign the licenses(s) to a router via oberon login

    assign-afl <router>

    Ex: assign-afl s2b1

Record the output from assign-afl or keep it on-screen for copy-and-paste.

Login to Juniper support website as mentioned by the output of assign-afl


Goto Juniper License Management System: 
http://www.juniper.net/generate_license.

From the pulldown next to the 'GO' button select 'Ex-series Products' 
then click 'GO' button.

Using the output from assign-afl above copy-and-paste the Device Serial 
Number and Authorization Code.

At the next web page confirm the accuracy of your entries by clicking 
the 'GENERATE LICENSE' button.

At the next page choose Download / Email to obtain the text of the license.

At the next page select your preferred message of obtaining a copy of 
the license text and click  the 'OK' button. Please note if you use 
Firefox, the download filename may have a blank in front of it. I delete 
that before saving.

Repeat the above six steps until all licenses need have been downloaded.

Put a copy of each license text in /opt2/junos/keys/<router serial 
number>.txt for a backup.
    scp -p BR0213507772.txt oberon:/opt2/junos/keys

Open a terminal to the router the licenses are for as you will copy and 
paste the output in the next step.

Use activate-afl /opt2/junos/keys/<router serial number>.txt from oberon 
login. Repeat for each license for the router.

Copy the license below the > request system license add termina and the 
blank lines into your paste buffer

On the router, at the CLI, enter `request system license add terminal'

Paste the license text and then use <Ctrl-D>. You should get the message:
  JUNOS525867: successfully added
  add license complete (no errors)

Repeat the system request for each license obtained. The End.

If you get the error message:

  invalid signature: cannot validate /etc/db/certs/FeatureLicense-v4.pem
  terminal:7 error: JUNOS520440: subject issuer mismatch:
  /C=US/ST=CA/L=Sunnyvale/O=Juniper Networks/OU=Juniper
  CA/CN=PackageCA/emailAddress=ca@juniper.net
  add license complete (1 errors)

then JunOS is less than version 12.3 and you must follow the convoluted 
procedure below to clean up the message just made and add the v4 
Feature  License (from


> start shell
% su -
   (password is standard for a building router)
 % rm /var/db/certs/FeatureLicense-v4.pem
   (don't worry, it's empty, this is the clean up)
 % cli
> op url http://www.juniper.net/support/files/scripts/update-cert.slax 
key sha1 1348c976215cd2e944e931e7f858913af626e013
   (as one line with a space between sha1 and the keystring
> exit
    (leave root CLI)
% exit
    (leave root shell)
% exit
    (leave your shell)
>

Now start over at the `request system license add terminal' above. The 
'op url' has added the v4 Feature License.

-- 
 --
 Daryl Fonseca-Holt, Network Architect, University of Manitoba, 204.480.1079



