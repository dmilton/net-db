Building types are used to describe the various "parts" of a network
and help identify what kinds of networks should be assigned to a given
building. There are three pre-defined building types:

+----------|---------------------------------------------------------------+
| Building |                                                               +
|   Type   | Description                                                   |
+----------|---------------------------------------------------------------+
| office   | Buildings where workstations are installed.                   |
| core     | Core router/buidings. No workstations or servers, only links. |
| dc       | Data Centre router/buildings. Systems are typically servers   |
+----------|---------------------------------------------------------------|

If you require additional building types, you will need to make changes to
various tables in order to make them effective.

To begin with, you need to decide on a name and and that to the building
types table using the set-bldg-type command.

Once that is complete you need to determine what network types should be
assigned by default when a new building of this type is created. Update
those network types using the set-type-info command and setting the
bldg-type-list value to include your new building type.

Now, whenever you assign networks to a building using assign-nets, the
identified network types will be assigned to your buildings.

