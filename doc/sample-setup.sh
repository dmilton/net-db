#!/bin/sh

echo "########################################################"
echo "# Creating building types for MPLS"
set-bldg-type PE "provider edge"
set-bldg-type P "provider core"
set-bldg-type CE "customer edge"
show-bldg-types

echo "########################################################"
echo "# Creating basic network types."
set-net-type --add user vlan-tag 40 class best required true \
	private false extension '' priority 1 version 6 \
	rel_table "network_bldgs,None" \
	descr "End user workstation equipment"
set-net-type --add loop vlan-tag 1 class next required true \
	private false extension 'b1' priority 2 version 6 \
	rel_table "network_bldgs,None" \
	descr "Router loopback interfaces"
set-net-type --add link vlan-tag 0 class next2 required true \
	private false extension '-link' priority 3 version 6 \
	rel_table "network_links,None" \
	descr "router links to the core - level 2 routers."
set-net-type --add mgmt vlan-tag 2 class private required true \
	private true extension '-mgmt' priority 4 version 6 \
	rel_table "network_bldgs,None" \
	descr "network infrastructure, switches, access points, sensors."
set-net-type --add voip vlan-tag 260 class private required true \
	private true extension '-voip' priority 5 version 6 \
	rel_table "network_bldgs,None" \
	descr "Telecommunications equipment - IP phones."
set-net-type --add 2nd vlan-tag 40 class private required true \
	private true extension '-2nd' priority 6 version 6 \
	rel_table "network_bldgs,None" \
	descr "End user lab/test/printer/embedded system equipment"
set-net-type --add sunray vlan-tag 256 class private required false \
	private true extension '-sunray' priority 7 version 6 \
	rel_table "network_bldgs,None" \
	descr "Thin client terminals."
set-net-type --add prot vlan-tag 280 class next required false \
	private true extension '-prot' priority 8 version 6 \
	rel_table "network_bldgs,None" \
	descr "Protected access to Internet but no access from Internet."
set-net-type --add login vlan-tag 270 class next required false \
	private true extension '-login' priority 9 version 6 \
	rel_table "network_bldgs,None" \
	descr "Protected access to Internet but no access from Internet."

echo "########################################################"
echo "# Adding zone 1 - v6 address space."
set-zone-info --add 1 network 2001:DB8::/48 link 0 loopback 1 building 1 \
	descr "Primary user address space for zone 1"
set-zone-info --add 1 network 2001:db8:8000::/48 link 0 loopback 1 building 1 \
	descr "Address space used for secondary connections (no Internet)" \
	net-type-list "voip,2nd,sunray,mgmt"


echo "########################################################"
echo "# Adding a building."
set-bldg-info --add bogus bldg-zone 55 campusid 1 core-bldg F \
	name "bogus building for testing scripts" \
	ospf-area 1 router bgrouter visible false webdir /network/bogus

echo "########################################################"
echo "# Display the new building with minimial attributes"
bldg-info bogus

echo "########################################################"
echo "# Add the main voice data room/wiring closet."
set-wiring-closet --add bogus "Rm 003c" True
echo "# Add a second voice data room/wiring closet."
set-wiring-closet --add bogus "Rm 102" False

echo "########################################################"
echo "# Display the new building with voice data rooms defined."
bldg-info bogus

echo "########################################################"
echo "# Searching for our bogus building."
bldg-info --find bogus

echo "########################################################"
echo "# Display the building name and zone"
bldg-info --name bogus
bldg-info --zone bogus

# Add a bogus network.
echo "########################################################"
echo "# Add two new network ranges."
# 192.0.2.0/24 (TEST-NET-1)
# 198.51.100.0/24 (TEST-NET-2)
# 203.0.113.0/24 (TEST-NET-3)

./set-net-info --add=free 192.0.2.0/24 name TEST-NET-v4 bldg_code 00 \
	net_type user zone 55
./set-net-info --add=free 2001:DB8::/32 name TEST-NET-v6 bldg_code 00 \
	net_type user zone 55

echo "########################################################"
echo "# Creating a pure IPv6 building."
echo "# Assigning the base v6 address for the building."
./alloc-nets-v6 --base bogus 

activate-afl@
afl-info@
assign-afl@
assign-net@
bldg-info@
find-ip-info@
find-mac-info@
find-room-ports@
merge-nets@
set-afl-info@
set-afl-info-tests.sh*
