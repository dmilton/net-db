from django.db import models

class AutonomousSystem(models.Model):
    #CREATE TABLE "autonomous_systems" (
    #   asn             INTEGER DEFAULT 66635 PRIMARY KEY,
    #   as_name         VARCHAR(32),
    #   org_id          varchar(16) NOT NULL UNIQUE,
    #   org_name        varchar(64) NOT NULL UNIQUE,
    #);
    #GRANT SELECT,REFERENCES on autonomous_systems to netview;
    #INSERT INTO autonomous_systems VALUES (0,'default campus');
    asn = models.IntegerField(default=65535,primary_key=True)
    as_name = models.CharField(max_length=32)
    org_id = models.CharField(max_length=16)
    org_name = models.CharField(max_length=64)

