from django.db import models

class ZoneDescription(models.Model):
    #CREATE TABLE zone_descriptions (
    #   net_zone        SERIAL PRIMARY KEY,
    #   asn             INTEGER REFERENCES autonomous_systems(asn)
    #                   ON UPDATE CASCADE ON DELETE RESTRICT,
    #   zone_descr      VARCHAR(64)
    #);
    net_zone = models.AutoField(primary_key=True)
    asn = models.ForeignKey(AutonommousSystem,on_delete=models.CASCADE)
    zone_descr = models.CharField(max_length=64)

