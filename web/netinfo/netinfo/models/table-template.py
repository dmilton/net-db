from django.db import models

class TableName(models.Model):
    #CREATE TABLE table_name (
    #   custom_id       SERIAL PRIMARY KEY,
    #   ext_field       INTEGER REFERENCES autonomous_systems(asn)
    #                   ON UPDATE CASCADE ON DELETE RESTRICT,
    #   char_field      VARCHAR(64)
    #);
    custom_id = models.AutoField(primary_key=True)
    ext_field = models.ForeignKey(AutonommousSystem,on_delete=models.CASCADE)
    char_field = models.CharField(max_length=64)
    int_field = models.IntegerField(default=-1)
    # Auto set date field to now.
    date_field = models.DateField(auto_now=True)
    # Auto set date field - only at creation.
    date_field = models.DateField(auto_now_add=True)
    date_time = models.DateTimeField(auto_now_add=True)




