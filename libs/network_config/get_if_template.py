#
############################################################################
# get_if_template
def get_if_template(configType):

    query = "SELECT config FROM switch_if_templates WHERE name = '%s'" \
        % (configType)
    queryResult = PgQuery(query, dbName="networking")
    try:
        result = queryResult[0][0]
    except TypeError:
        raise NetException("InvalidConfigType", configType )
    except Exception:
        raise
    ## end try

    return(result)
## end def get_if_template():

# vim: syntax=python ts=4 sw=4 showmatch et :

