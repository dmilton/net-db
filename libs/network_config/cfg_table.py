#\
# 30 Oct 2011    Initial creation.

###############################################################################
# Network AFL License table
#
_switchIfTemplates = {
    'database': 'networking',            # name of the database
    'role': 'netmon',                    # role for database access
    'table': 'switch_if_templates',    # name of the table
    'index': 'name',                    # name of index column
}


# vim: syntax=python ts=4 sw=4 showmatch et :

