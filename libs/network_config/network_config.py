#
g_version='20 Jan 2014 - 2.0.0'

from network_error import NetException
import db_access



#\
# 30 Oct 2011    Initial creation.

###############################################################################
# Network AFL License table
#
_switchIfTemplates = {
    'database': 'networking',            # name of the database
    'role': 'netmon',                    # role for database access
    'table': 'switch_if_templates',    # name of the table
    'index': 'name',                    # name of index column
}



#
############################################################################
# get_if_template
def get_if_template(configType):

    query = "SELECT config FROM switch_if_templates WHERE name = '%s'" \
        % (configType)
    queryResult = PgQuery(query, dbName="networking")
    try:
        result = queryResult[0][0]
    except TypeError:
        raise NetException("InvalidConfigType", configType )
    except Exception:
        raise
    ## end try

    return(result)
## end def get_if_template():


#

############################################################################
# list_if_types
def list_if_types():

    global ifTemplateTable, ifTemplateFields, ifTemplateKeys

    # Query the database to get the current list of available templates.
    query = "SELECT " + ifTemplateFields + " FROM " + ifTemplateTable
    ifTypeTable = PgQuery(query, dbName="networking")

    # Now print out the table.
    ifTypeHead = ( "Name", "Description" )
    print_table(ifTypeHead, ifTypeTable)

## end def list_if_types():



# vim: syntax=python ts=4 sw=4 showmatch et :
