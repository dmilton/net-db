#

############################################################################
# list_if_types
def list_if_types():

    global ifTemplateTable, ifTemplateFields, ifTemplateKeys

    # Query the database to get the current list of available templates.
    query = "SELECT " + ifTemplateFields + " FROM " + ifTemplateTable
    ifTypeTable = PgQuery(query, dbName="networking")

    # Now print out the table.
    ifTypeHead = ( "Name", "Description" )
    print_table(ifTypeHead, ifTypeTable)

## end def list_if_types():


# vim: syntax=python ts=4 sw=4 showmatch et :

