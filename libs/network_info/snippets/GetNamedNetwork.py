#
# 13 Feb 2013	initial creation.

############################################################################
# GetNamedNetwork
#
# Find a network in the database with the specified name.
# Entry: netName - the name of the network to get information for.
# Exit:
#		The result is a list of network dictionaries if any match
#		the provided name. If no matches are found a zero length list
#		is returned.
#
def GetNamedNetwork(netName):
	# Search by the name of the network.
	myWhere = "name = '" + netName + "'"

	sortOrder = ""

	result = _GetNetworks("name", netName, sortOrder, bldgZone=0, \
			genISO=False, withFree=True)

	return result
## end def GetNamedNetwork():

