#

# 13 Jun 2013		initial creation.

############################################################################
# DeleteVlan
# Given a vlanInfo dictionary, delete the vlan from the database.
# Entry:
#		vlanInfo - the vlan dictionary to delete.
# Exit: the vlan has been deleted from the database.

# 4 Feb 2012	initial creation

def DeleteVlan(vlanInfo):
	global _networkZap

	# insert into network_free
	#  (name,vlan_tag,network,bldg_code,primenet,assign_date,assign_by,net_type)
	# select
	#   name,vlan_tag,network,bldg_code,primenet,assign_date,assign_by,net_type
	# from network_bldgs where network = '130.179.92.0/22';
	#
	# delete from network_bldgs where network = '130.179.92.0/22';
	#
	# update network_free
	#  set name = '130.179.92-bogon',	zone = 1,
	#		vlan_tag=0,					bldg_code='00',
	#		primenet='F'
	#  where network = '130.179.92.0/22';

	# Make a copy of the network in the network_zap table.
	vlanInfo['zap_date'] = 'now()'
	vlanInfo['zap_by'] = network_log.FindWho()
	dbAccess.SetTableRow(_networkZap, vlanInfo)
	
	delTable = NetInTable(vlanInfo)

	myWhere = "bldg_code = '" + vlanInfo['bldg_code'] + "'"
	myWhere += " AND vlan_tag = " + str(vlanInfo['vlan_tag'])
	dbAccess.DeleteTableRows(delTable, myWhere)

	return
## end def DeleteVlan():

