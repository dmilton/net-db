#\L
# 29 September 2010		initial creation.

############################################################################
# CalcIPv4Networks
# Given a base to start from, calculate the networks which are to be used
# for Management, Sunray, and VoIP networks.
# ENTRY:
#	baseNet - the base network to start from as a string.
# EXIT:
#	returns a dictionary containing three entries:
#	management, sunray, and voip which are the three IPv4 subnets
#	to be used for the building.

def CalcIPv4Networks(baseNet):
    debug.debug(1, "CalcIPv4Networks: baseNet=" + str(baseNet));

    # Take the base network and find out what the cidr mask is.
    theNet = ipaddr.IPv4Network(baseNet);
    prefixLen = baseIP.prefixlen;
    if ( theNet.prefixlen >= 22 and theNet.prefixlen <= 26 ):
	mgmtPrefixLen = theNet.prefixlen + 2;
    elif ( theNet.prefixlen < 22 and theNet.prefixlen >= 16 ):
	mgmtPrefixLen = 24;
    else:
	raise ValueError('%s does not appear to be a valid address range' %
		baseNet);
    ## end if ( prefixLen >= 22 and prefixLen <= 26 )
    srayPrefixLen = mgmtPrefixLen;
    voipPrefixLen = mgmtPrefixLen - 1;

    # Now that we know the prefix length to use for the networks,
    # calculate them based on our baseIP.

    debug.debug(1, "CalcIPv4Networks: exit");
    return();
## end CalcIPv4Networks()

