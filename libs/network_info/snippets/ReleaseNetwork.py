############################################################################
# ReleaseNetwork
# Given a network record, return it to the free pool appropriate for that
# particular type. If a private network is supplied, delete it.

# 13 January 2012	initial writing, cloned from AllocateNetwork2

def ReleaseNetwork(netInfo):
    """
    Take a network and return it to the free pool.
    Use:
    try:
	netInfo = FindNetByVlan(bldgCode, vlan_tag)
    except NetException, err:
	if ( err.name == "NoNetwork" ):
	    die - network not in database.
	else:
	    raise
	## end if ( err.name = "NoNetwork" ):
    except:
	raise
    ## end try
    ReleaseNetwork(netInfo)
    """
    debug.debug(1, "ReleaseNetwork: netInfo=" + str(netInfo))

    global networkDetail, networkVlans, networkAllocations, networksView

    # Figure out what kind of network this is.
    netType = netInfo['type']
    netTypeInfo = network.GetNetTypes(bldgInfo['campusid'], netType)

    # What needs to happen?
    if ( netTypeInfo['free_action'] == 'delete' ):
	# This is a network we can delete if it is no longer required.
        DeleteNetwork(netInfo['bldg_code'], netInfo['network'])
    elif ( netTypeInfo['free_action'] == 'release' ):
	# Free the network by making the network the appropriate free code.
        freeCode = typeInfo['freecode']
        # Figure out what to name the network.
        theNet = netInfo['network'].split('/')[0]
        netInfo['name'] = NameNetwork(netInfo['name'], theNet, netType)
        # Update remaining properties of the network to reflect free state.
        netInfo['searchable'] = False
        netInfo['bogon'] = True
        netInfo['description'] = ''
        netInfo['assign_by'] = network_log.FindWho()

	# Generate our query...
	myQuery = dbAccess.UpdateQuery('network_detail', networkDetail,

    ## end elif ( typeInfo['free_action'] == 'release' ):

    # Find the network_vlans record from the networks view.
    myQuery = "SELECT vlanid FROM networks" + \
	    " WHERE network = '%s' AND bldg_code = '%s'" + \
	    " ORDER by assign_date DESC LIMIT 1"
    myQuery = myQuery % ( theNet, netInfo['bldg_code'] )
    result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

    try:
	vlanid = int(result[0][0])
    except TypeError:
	# we don't have an array so result is scalar (query failed)
	# this means the vlan cannot be found.
	vlanid = 0
    except:
	raise
    ## end try

    if ( vlanid == 0 ):
	raise NetException, ("NoVlanRecord", oldBldgCode + " ~ " + str(theNet))
    ## end if ( vlanid == 0 ):

    # If a duplicate name exists on the new name, fail.
    # Eliminate the existing record by vlan id to ensure we don't fail
    # when the vlan name is not being changed.
    myQuery = "SELECT count(name) FROM network_vlans" + \
	    " WHERE name = '%s'" % ( netInfo['name'] )
    result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")
    nameCount = int(result[0][0])
    debug.debug(2, "ReleaseNetwork: nameCount=" + str(nameCount))
    if ( nameCount != 0 ):
	raise NetException, ( "NameExists", newName + " ~ " + str(vlanid) )
    ## end if ( nameCount != 0 ):

    # Update the name and description.
    myQuery = "UPDATE network_vlans" + \
	    " SET name = '%s', description = '%s'" \
	    " WHERE id = %s" % \
	    ( netInfo['name'], netInfo['description'], vlanid )
    result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

    # Update the searchable and bogon flags.
    myQuery = "UPDATE network_detail" + \
	    " SET searchable = '%s', bogon = 'F'" \
	    " WHERE network = '%s'" % \
	    ( str(netInfo['searchable']), str(netInfo['bogon']), theNet )
    result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

    # Update the building code, assign_date, and assign_by fields.
    myQuery = "UPDATE network_allocations" + \
	    " SET assign_date = now(), assign_by = '%s', bldg_code = '%s'" \
	    " WHERE bldg_code = '%s' AND vlanid = %s" % \
	    ( netInfo['assign_by'], newBldgCode, netInfo['bldg_code'], vlanid )
    result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

    debug.debug(1, "ReleaseNetwork: result=" + str(result))
    return result
## end def ReleaseNetwork():
