#\L
# 26 Sept 2010		initial creation.

############################################################################
# NextSubnet
# Calculate the next subnet up based on the provided mask.
# Entry:
#	baseNetwork - the network to start with. This is the cidr string
#		representation received from the database.
#	family - is this an IPv4 (4) or IPv6 (6) network?
# Exit:
#	result - the cidr string representation of the network adjacent
#		to the one provided as an argument.
#

import ipaddr;

#def NextSubnet(baseNetwork, family):
def NextSubnet(baseNetwork):
    #debug.debug(1, "NextSubnet: baseNetwork=" + str(baseNetwork));
    #debug.debug(1, "NextSubnet: family=" + str(family));

    # Use the ipaddr module to create an ipaddr entity.
    #if ( family == 4 ):
    #	ip1 = ipaddr.IPv4Network(baseNetwork);
    #else:
    #	ip1 = ipaddr.IPv6Network(baseNetwork);
    #debug(2, "NextSubnet: ip1=" + str(ip1));
    ip1 = ipaddr.IPNetwork(baseNetwork);

    # Make our network one 'bit' bigger.
    ip2 = ip1.supernet(1);
    #debug(2, "NextSubnet: ip2=" + str(ip2));

    # Determine the two subnets in the supernet created above and
    # the second one is the result we want.
    ip3 = ip2.subnet()[1];
    #debug(2, "NextSubnet: ip3=" + str(ip3));

    #debug.debug(1, "NextSubnet: exit");
    return(str(ip3));
## end NextSubnet()

