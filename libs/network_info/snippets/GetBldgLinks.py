#\L

# 13 September 2013		initial creation.

############################################################################
# GetBldgLinks
#
# Build a list of link networks for use in a building.
#
def GetBldgLinks(bldgCode, bldgZone):

	# Get the information on the zone.
	zoneInfo = network_zones.GetZoneInfo(bldgZone, 'link-v6', 6)[0]

	# Get the type information for links.
	typeInfo = network_types.GetTypeInfo('link-v6')

	# Get the core endpoints for the zone.
	epList = GetEndpoints(bldgZone)

	result = []
	for anEp in epList:
		newLink = NewTemplate(typeInfo)
		newLink['network'] = \
			GetNextIPv6Link(zoneInfo['network'],zoneInfo['next_link'])
		zoneInfo['next_link'] += 1
		newLink['bldg_code'] = bldgCode
		newLink['ep_code'] = anEp['ep_code']
		newLink['name'] = anEp['ep_code'] + '-' + bldgCode + '-' \
				+ typeInfo['extension']
		newLink['net_type'] = typeInfo['net_type']
		newLink['network_table'] = typeInfo['rel_table'].split(',')[0]
		result.append(newLink)

	# We used some links so update the zone.
	network_zones.UpdateZone(zoneInfo)

	return(result)
## end GetBldgLinks()

