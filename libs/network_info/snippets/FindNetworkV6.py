#

# 13 Jul 2013		add loopback and user address space computations.
# 6 Feb 2013		rewrite for new database schema.

############################################################################
# FindNetworkV6
# Given the basic information about a building, calculate the next available
# IPv6 network address space for use in the building.
# Links and loopback addresses are calculated based on the zone information.
#
# Entry:
#	typeInfo	- type information for network to 'find'
#	bldgInfo - the building info detail record.
#	netCount - optional count of networks to return. By default 1 network
#		is returned unless this is a link network request where the number
#		of links returned will match the core routers for the zone.
#

def FindNetworkV6(typeInfo, bldgInfo, netCount=0):

	result = []

	netType = typeInfo['net_type']
	netZone = bldgInfo['net_zone']

	if ( bldgInfo['ipv6_netbase'] == None ):
		zoneInfo = network_zones.GetZoneInfo(netZone, netType, 6)

	for netIndex in range(netCount):
		if ( typeInfo['net_type'] == 'link-v6' ):
			epInfoList = network_ends.GetEndpoints(netZone)
			linkCount = length(epInfo)
			typeInfo = network_types.GetTypeInfo('link-v6')
			zoneInfo = network_zones.GetZones(netZone, 6)
			nextLink = zoneInfo['link']
			zoneInfo['link'] += epInfo
			network_zones.UpdateZone(zoneInfo)
			for anEp in epInfoList:
				aLink = NewTemplate(typeInfo)
				aLink['name'] = anEp['ep_code'].lower() + "-" \
					+ str(nextLink) + typeInfo['extension']
				aLink['ep_code'] = anEp['ep_code']
				aLink['net_type'] = 'link-v6'
				aLink['network'] = ipaddr.IPNetwork(
						str(zoneInfo['network'].network) + ":" \
						+ hex(nextLink)[2:].zfill(4) + ":0/112" )
				result += aLink
				nextLink += 1
		elif ( typeInfo['net_type'] == 'link-v6' ):

	return(result)
## end def FindNetworkV6():

