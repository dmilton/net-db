#\L

# 3 May 2012		initial creation.

############################################################################
# CalcIPv6Nets
#
# Calculates the next IPv6 network to assign to the specified building.
# The bits for the building are allocated counting up from the left of
# the network portion of the allocation. The bits for the network within
# the building are counting up from the right of the network portion.
#
def CalcIPv6Nets(bldgCode):

	# Figure out the 'building' portion of the address.
	bldgInfo = GetBuildingInfo(bldgCode)
	buildingV6 = bldgInfo['ipv6_netbase']

	# Which zone do we allocate addresses from?
	bldgZone = bldgInfo['net_zone']

	# Figure out the network within building portion of the address.
	v6increment = bldgInfo['ipv6_nextnet'] + 1

	# update the allocation information.
	updateAllocation = false
	if ( v6zoneInfo['max_net'] < v6increment ):
		v6zoneInfo['max_net'] = v6increment
		updateAllocation = true
	## end if ( v6zoneInfo['max-net'] < v6increment ):

	# Get the allocation block for this zone.
	v6zoneInfo = GetZoneAllocation(bldgZone)

	# No IPv6 addresses have been assigned to this building.
	if ( buildingV6 == 0 ):
		# update the building information.
		bldgInfo['ipv6_netbase'] = v6zoneInfo['building']
		bldgInfo['ipv6_nextnet'] = v6increment
		# update the allocation information.
		v6zoneInfo['building'] = v6zoneInfo['building'] + 1
		updateAllocation = true
		buildingV6 = bldgInfo['ipv6_netbase']
	## end if ( buildingV6 == 0 ):

	# Build the network portion of the IPv6 address.
	# start by chopping off the cidr information (:/48, :/40, ...).
	result = v6zoneInfo['network'][:-4]

	# number of bits available for network.
	netBits = 64 - int(v6zoneInfo['network'][-2:])

	# convert the building index into binary.
	# For python 2.6 and later do:
	if ( 0 ):
		# result of bin has 0b prefixed on the number.
		bldgBin = bin(buildingV6)[2:]
	else:
		bldgBin = IntToBin(buildingV6)
	## end else if ( 0 ):

	# Zero pad on the left to the number of 'netBits'.
	zerosToAdd = netBits - len(bldgBin)
	for i in range(0, zerosToAdd):
		bldgBin = '0' + bldgBin
	## end for i in range(0, zerosToAdd):

	# Reverse the order, convert to an integer, add network number.
	bldgNetwork = BinToInt(bldgBin[::-1]) + v6increment

	# Now convert the whole thing to hex.
	bldgHex = "%x" % (bldgNetwork)
	result = result + bldgHex + '::/64'

	return()
## end CalcIPv6Nets()

def IntToBin(n):
	digs = []
	s = ''
	if n<0:
		s = '-'
		n = -n
	while True:
		digs.append(str(n % 2))
		n /= 2
		if not n: break
	if s: digs.append(s)
	digs.reverse()
	return ''.join(digs)
## end ToBin()

def BinToInt(binstr):
	decnum = 0
	for i in binstr:
		decnum = decnum * 2 + int(i)
	return(decnum)
## end BinToInt()

