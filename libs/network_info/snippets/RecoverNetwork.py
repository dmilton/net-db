############################################################################
# RecoverNetwork
# Given a building code and network address, restore a previously 
# deleted network.
def RecoverNetwork(bldgCode, network):
    debug.debug(1, "RecoverNetwork: bldgCode=" + str(bldgCode))
    debug.debug(1, "RecoverNetwork: network=" + str(network))

    # Get the saved information from the network_zap table. 
    rawQuery = "SELECT " + zapFields + \
	    " FROM network_zap" + \
	    " WHERE bldg_code = '%s' AND network = '%s'"
    myQuery = rawQuery % (bldgCode, network)
    result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

    if ( len(result) == 1 ):
	zapRecord = network.MakeNetDictionary(result[0])
	if ( zapRecord['network'] == 'NULL' ):
	    AddVlan(zapRecord)
	else:
	    AddNetwork(zapRecord)
	## end else if ( zapRecord['network'] == 'NULL' ):
	
	# Now remove the just recovered zap record.
	myQuery = "DELETE FROM network_zap WHERE id = %s" \
		% (zapRecord['zap_id'])
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")
    ## end if ( len(result) == 1 ):

    debug.debug(1, "RecoverNetwork: exit")
## end def RecoverNetwork():

