# ed: ts=4 sw=4
############################################################################
# AllocateNetwork2
# Given all the details, change the allocations for a network to assign
# an existing database entry to a specific building.

# 16 July 2012		problem with update forcing bogon to false when
#					the argument needs to be passed. This allows a
#					network to be set as bogon.
# 4 January 2012	Changed queries to use raw tables rather than
#					the view.
# 4 February 2010	AllocateNetwork2 needed to use 'netmon' to select
#					fields from the network_allocations table.

def AllocateNetwork2(netInfo, newBldgCode):
	"""
	Allocate a network to a specific building.
	Use:
	try:
		netInfo = FindNetByVlan(bldgCode, vlan_tag)
	except NetException, err:
		if ( err.name == "NoNetwork" ):
			die - network not in database.
		else:
			raise
		## end if ( err.name = "NoNetwork" ):
	except:
		raise
	## end try
	AllocateNetwork2(netInfo, newBldgCode)
	"""
	debug.debug(1, "AllocateNetwork2: netInfo=" + str(netInfo))
	debug.debug(1, "AllocateNetwork2: newBldgCode=" + str(newBldgCode))

	# Make sure this allocation will not produce a duplicate.
	# If a duplicate building allocation exists then fail.
	theNet = netInfo['network']
	myQuery = "SELECT count(network) FROM network_allocations" + \
			" WHERE network = '%s' AND bldg_code = '%s'" % \
			( theNet, newBldgCode )
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")
	allocCount = int(result[0][0])
	if ( allocCount != 0 ):
		raise NetException, ( "AllocationExists", newBldgCode + "<>" + theNet )
	## end if ( allocCount != 0 ):

	# Find the network_vlans record from the networks view.
	myQuery = "SELECT vlanid FROM networks" + \
			" WHERE network = '%s' AND bldg_code = '%s'" + \
			" ORDER by assign_date DESC LIMIT 1"
	myQuery = myQuery % ( theNet, netInfo['bldg_code'] )
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

	try:
		vlanid = int(result[0][0])
	except TypeError:
		# we don't have an array so result is scalar (query failed)
		# this means the vlan cannot be found.
		vlanid = 0
	except:
		raise
	## end try

	if ( vlanid == 0 ):
		raise NetException, ("NoVlanRecord", oldBldgCode + " ~ " + str(theNet))
	## end if ( vlanid == 0 ):

	# If a duplicate name exists on the new name, fail.
	# Eliminate the existing record by vlan id to ensure we don't fail
	# when the vlan name is not being changed.
	myQuery = "SELECT count(name) FROM network_vlans" + \
			" WHERE name = '%s'" % ( netInfo['name'] )
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")
	nameCount = int(result[0][0])
	debug.debug(2, "AllocateNetwork2: nameCount=" + str(nameCount))
	if ( nameCount != 0 ):
		raise NetException, ( "NameExists", newName + " ~ " + str(vlanid) )
	## end if ( nameCount != 0 ):

	# Update the name and description.
	myQuery = "UPDATE network_vlans" + \
			" SET name = '%s', description = '%s'" \
			" WHERE id = %s" % \
			( netInfo['name'], netInfo['description'], vlanid )
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

	# Update the searchable and bogon flags.
	myQuery = "UPDATE network_detail" + \
			" SET searchable = '%s', bogon = '%s'" \
			" WHERE network = '%s'" % \
			( str(netInfo['searchable']), str(netInfo['bogon']), theNet )
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

	# Update the building code, assign_date, and assign_by fields.
	myQuery = "UPDATE network_allocations" + \
			" SET assign_date = now(), assign_by = '%s', bldg_code = '%s'" \
			" WHERE bldg_code = '%s' AND vlanid = %s" % \
			( netInfo['assign_by'], newBldgCode, netInfo['bldg_code'], vlanid )
	result = dbAccess.PgQuery(myQuery, dbName="networking", dbUser="netmon")

	debug.debug(1, "AllocateNetwork2: result=" + str(result))
	return result
## end def AllocateNetwork2():
