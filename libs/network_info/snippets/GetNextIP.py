#

# 10 Apr 2013		Remove ipaddr class import, this is globally done.
# 22 Jan 2013		Initial creation. Needs to be rewritten using an IP
#					address class rather than direct calls to the database.

############################################################################
# GetNextIP
#
# Input: startingIP - the IP address we are starting from.
# Output: the next IP address is computed and returned.
#

def GetNextIP(startingIP):

	result = str(ipaddr.IPAddress(startingIP) + 1)

	return result
## end def GetNextIP():

