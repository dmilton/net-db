#
############################################################################
# GetRouterAddress
def GetRouterAddress(network):
    debug.debug(1, "GetRouterAddress: network=" + str(network))

    myQuery = "SELECT host('" + network + "'::cidr + 1) AS router"
    result = PgQuery(myQuery, dbName="networking")
    result = result[0][0]

    debug.debug(1, "GetRouterAddress: result=" + str(result))
    return result
## end def GetRouterAddress():

