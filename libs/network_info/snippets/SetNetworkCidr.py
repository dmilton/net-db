#
#
# 23 Feb 2012	Change code to update network_detail instead of networks
#		as the on update clause of the networks view has been removed.

############################################################################
# SetNetworkCidr
# Given a network and new CIDR, update the database to reflect the new CIDR.
# Returns the new network record.
def SetNetworkCidr(network, newCidr):
    debug.debug(1, "SetNetworkCidr: network=" + str(network))
    debug.debug(1, "SetNetworkCidr: newCidr=" + str(newCidr))

    # Verify the new cidr value is consistient.
    # Get the current network and cidr values.
    (theNet, theCidr) = network.split('/')

    # If the new cidr is greater than the existing one we are 
    # splitting the existing network into pieces. If the new cidr
    # is less than the existing one we are merging networks together.
    if ( newCidr < theCidr ):
	# The new cidr is less than the old cidr so we are merging
	# networks together. Check that we are not creating a new
	# network which encompases any existing ones.
	myQuery = "SELECT count(*) FROM networks" + \
		" WHERE network <<= \'%s/%s\'"
    ## end if ( newCidr < theCidr ):

    # Update the database record...
    myQuery = "UPDATE network_detail" + \
	    " SET network = set_masklen(\'%s\', %s)" + \
	    " WHERE network = \'%s\'"
    result = dbAccess.PgQuery(myQuery % ( network, str(newCidr), network), \
	       dbName="networking", dbUser="netmon")

    # Find the new database record...
    result = FindNetByCidr(theNet)
    
    debug.debug(1, "SetNetworkCidr: result=%s" % (str(result)))
    return result
## end def SetNetworkCidr()

