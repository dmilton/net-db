#
############################################################################
# FindNetBy
#
# Find a network in the database based upon the key information provided.
# Entry:
#	keyName - the key field name as found in networksView.
#	keyValue - the value to compare against.
#	operator - how to compare the values.
# Exit:
#	The result is a list of network dictionaries that match as described.
#	If no matches are found a zero length list is returned.
#
def FindNetBy(keyName, keyValue, operator='='):
    debug.debug(1, "FindNetBy: keyName=" + str(keyName))
    debug.debug(1, "FindNetBy: keyValue=" + str(keyValue))
    debug.debug(1, "FindNetBy: operator=" + str(operator))

    # Searches are done via the networks view which references the
    # tables network_detail, network_vlans, network_types, network_assigns.
    global networksView;

    # If we actually get a CIDR network, just grab the IP portion.
    inetIP = netCidr.split('/')[0]

    # Now make our query using the IP
    myQuery = MakeNetQuery('ByIp', (inetIP,), 0, '', 0)

    # Lookup in the database.
    aNetwork = PgQuery(myQuery, dbName="networking")
    try:
	result = MakeNetworkDictionary(aNetwork[0])
	# For the moment, only use the first response.
	#if ( len(aNetwork) > 1 ):
	#    bldgCode = result['bldg_code']
	#    vlanNbr = result['vlan']
	#    raise NetException, ("MultipleNetworks", \
	#	"bldgCode %s, vlanNbr %s" % (bldgCode,vlanNbr))
	## end if ( len(aNetwork) > 1)
    except IndexError:
     	raise NetException, ("NoNetwork", "%s" % (netCidr))
    except TypeError:
     	raise NetException, ("NoNetwork", "%s" % (netCidr))
    except:
	raise
    ## end try

    debug.debug(1, "FindNetByCidr: result=" + str(result))
    return result
## end def FindNetByCidr():

#
############################################################################
# FindNetByCode
#
# Input: freeCode - the 'free' code to find networks within.
#        netCount - the number of networks to find.
# Output: a network dictionary of the specified type and count is returned.
# Exception: NoNetwork
#

def FindNetByCode(freeCode, netCount):
    debug.debug(1, "FindNetByCode: freeCode=" + str(freeCode))

    result = []
    myQuery = MakeNetQuery('ByCode', (freeCode,), 4, 'network', netCount)

    foundNets = PgQuery(myQuery, dbName="networking")
    try:
	i = 0
	for aNet in foundNets:
	    result.extend( [ MakeNetworkDictionary(aNet) ] )
    except TypeError:
     	raise NetException, ("NoCode", "%s" % (freeCode))
    except:
	raise
    ## end try

    debug.debug(1, "FindNetByCode: result=" + str(result))
    return result
## end def FindNetByCode():

#
############################################################################
# FindNetByName
def FindNetByName(netName):
    debug.debug(1, "FindNetByName: netName=" + str(netName))

    myQuery = MakeNetQuery('ByName', (netName,), 4, '', 1)

    aNetwork = PgQuery(myQuery, dbName="networking")
    try:
	result = MakeNetworkDictionary(aNetwork[0])
    except IndexError:
     	raise NetException, ("NoName", "%s" % (netName))
    except TypeError:
     	raise NetException, ("NoName", "%s" % (netName))
    except:
	raise
    ## end try

    debug.debug(1, "FindNetByName: result=" + str(result))
    return result
## end def FindNetByName():

#
############################################################################
# FindNetByType
#
# Input: bldgCode - the building code to search for the specified network type.
#        theType - the type to search for.
# Output: a network dictionary of the resulting specified type.
# Exception: NoNetwork
#

def FindNetByType(bldgCode, theType):
    debug.debug(1, "FindNetByType: bldgCode=" + str(bldgCode))
    debug.debug(1, "FindNetByType: theType=" + str(theType))

    # a network vlan tag is always unique to the building.
    myQuery = MakeNetQuery('ByCodeType', (bldgCode,theType), 0, '', 1)

    aNetwork = PgQuery(myQuery, dbName="networking")
    try:
	result = MakeNetworkDictionary(aNetwork[0])
    except IndexError:
     	raise NetException, ("NoType", \
		"bldgCode %s, theType %s" % (bldgCode,theType))
    except TypeError:
     	raise NetException, ("NoType", \
		"bldgCode %s, theType %s" % (bldgCode,theType))
    except:
	raise
    ## end try

    debug.debug(1, "FindNetByType: result=" + str(result))
    return result
## end def FindNetByType():

#
############################################################################
# FindNetByVlan
#
# Input: bldgCode - the building code to find the specified vlan in.
#        vlanTag - the id of the vlan to find in the building
# Output: a network dictionary for the vlan in the building.
# Exception: NoNetwork
#

def FindNetByVlan(bldgCode, vlanTag):
    debug.debug(1, "FindNetByVlan: bldgCode=" + str(bldgCode))
    debug.debug(1, "FindNetByVlan: vlanTag=" + str(vlanTag))

    # a network vlan id is always unique to the building.
    myQuery = MakeNetQuery('ByCodeVlan', (bldgCode,vlanTag), 0, '', 1)

    aNetwork = PgQuery(myQuery, dbName="networking")
    try:
	result = MakeNetworkDictionary(aNetwork[0])
    except IndexError:
     	raise NetException, ("NoVlan", \
		"bldgCode %s, vlanTag %s" % (bldgCode,vlanTag))
    except TypeError:
     	raise NetException, ("NoVlan", \
		"bldgCode %s, vlanTag %s" % (bldgCode,vlanTag))
    except:
	raise
    ## end try

    debug.debug(1, "FindNetByVlan: result=" + str(result))
    return result
## end def FindNetByVlan():

