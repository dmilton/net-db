#\L

# 20 Jun 2013		initial creation.

############################################################################
# CalcIPv6Link
#
# Links are allocated as /112's in subnet zero of the zone allocation.
#
# Entry:
#	v6allocation - the base address space assigned to the zone. Must be a /48.
#	linkID - the ID or index of the next link address in the zone.
# Exit:
#	the linkID and v6allocation have been used to calculate a /112
#	network address space to use for links.
#
def CalcIPv6Link(v6Allocation, linkID):

	# convert the link index into hex. bin result has 0b prefixed on the number.
	linkHex = hex(linkID)[2:]

	# Now take the allocation and add the link address.
	result = ipaddr.IPNetwork(str(v6allocation.ip) + linkHex + ':0/112')

	return(result)
## end CalcIPv6Link()

