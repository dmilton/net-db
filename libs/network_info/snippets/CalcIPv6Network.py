#\L

# 20 Jun 2013		initial creation.

############################################################################
# CalcIPv6Network
#
# Networks are allocated counting from the leftmost bits for the building
# and from the right for the subnet within the building.
#
# Entry:
#	v6allocation - the base address space assigned to the zone. Must be a /48.
#	buildingID - the ID or index of the building to convert into an address space.
#	networkID - the ID or index of the next network in the building.
# Exit:
#	the buildingID and v6allocation have been used to calculate a base /64
#	network address space to assign to the building.
#
def CalcIPv6Network(v6allocation, buildingID, networkID=0):

	# number of bits available for network.
	# This should always be 16.
	netBits = 64 - v6allocation.prefixlen

	# convert the building index into binary.
	# result of bin has 0b prefixed on the number.
	bldgBinary = bin(buildingID)[2:]

	# Zero pad on the left to the number of 'netBits'.
	zerosToAdd = netBits - len(bldgBinary)
	for i in range(0, zerosToAdd):
		bldgBinary = '0' + bldgBinary
	## end for i in range(0, zerosToAdd):

	# Reverse the order, convert to an integer, then to hex.
	#						      b   [::-1]			reverse order of bits.
	#				   int(       rb        , 2)		convert binary to int.
	#										     + 
	#             hex(            i              )		convert int to hex
	#                                             [2:]	drop the '0x'
	bldgNetwork = hex( int( bldgBinary[::-1], 2) )[2:]

	# Now take the allocation and add the subnet/network address.
	result = ipaddr.IPNetwork(str(v6allocation.ip)[:-1] + bldgNetwork + '::/64')

	return(result)
## end CalcIPv6Network()

