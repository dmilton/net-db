#\L

# 20 Jun 2013		initial creation.

############################################################################
# CalcIPv6Loop
#
# Networks are allocated counting from the leftmost bits for the building
# and from the right for the subnet within the building.
#
# Entry:
#	v6allocation - the base address space assigned to the zone. Must be a /48.
#	loopbackID - the ID or index of the next loopback address in the zone.
# Exit:
#	the buildingID and v6allocation have been used to calculate a base /64
#	network address space to assign to the building.
#
def CalcIPv6Loop(v6Allocation, loopbackID):

	# convert the loopback index into hex.
	#    hex returns 0b prefixed on the number.
	loopHex = hex(loopbackID)[2:]

	# Take the allocation address space and add the loopback address.
	result = ipaddr.IPNetwork(str(v6allocation.ip) + loopHex + '/128')

	return(result)
## end CalcIPv6Loop()

