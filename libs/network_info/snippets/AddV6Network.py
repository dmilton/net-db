#\L

# 24 Sep 2013	move subnet algorithm to separate routine
#				to permit use elsewhere.
# 11 Jul 2013	initial creation.

############################################################################
# AddV6Network
#
# Add an IPv6 link between building bldgCode and epCode to the database.
#
# Entry:
#	typeInfo - the type of network to add.
#	netName - the name to use for the network (will add type extension)
#	bldgInfo - the building to add the network to.
#
# Exit:
#	the new network address has been determined, and inserted into
#	the appropriate database table. The result is the network as
#	stored in the database.
#	
def AddV6Network(typeInfo, netName, bldgInfo):

	# Get the type information for a user network.
	typeInfo = network_types.GetTypeInfo('user-v6')[0]

	# Get the zone information for link network (or default)
	zoneInfo = network_zones.GetZoneInfo(bldgInfo['net_zone'], typeInfo, 6)[0]

	# Create the network.
	newNet = NewTemplate(typeInfo)
	newNet['name'] = netName + typeInfo['extension']

	# 64 bits of network but zone allocations are intended to a /48
	# so there should always be 16 bits of network to the zone.
	netBits = 64 - zoneInfo['network'].prefixlen

	newNet['network'] = \
		network_zones.NetId2BldgNet(netBits, bldgInfo['ipv6_netid'],\
			bldgInfo['ipv6_nextnet'],
			zoneInfo['network'])

	# Start numbering user networks at 40.
	newNet['vlan_tag'] = GetNextVlan(bldgInfo['bldg_code'], 40)
	newNet['bldg_code'] = bldgInfo['bldg_code']
	newNet['net_type'] = typeInfo['net_type']

	# Add the network to the database.
	netInfo = AddNetwork(newNet, typeInfo)

	# Update the zone if the max net has increased.
	if ( zoneInfo['max_net'] < bldgNextNet ):
		zoneInfo['max_net'] = bldgNextNet
		network_zones.UpdateZone(zoneInfo)

	return(netInfo)
## end def AddV6Network():

