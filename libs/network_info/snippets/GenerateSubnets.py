#\L
# 13 Feb 2013		initial creation.

############################################################################
# GenerateSubnets
# Given an existing subnet and new prefix length, generate all of the subnets.
# Input: ipNet - the network to split into new subnets.
#		prefixLen - the new subnet prefix length to use.
# Output: a list of ip networks of size prefixLen which encompass the
#		original range of ipNet.

def GenerateSubnets(ipNet, prefixLen):
	result = []
	# Figure out what we are starting with...
	baseIP = ipaddr.IPAddress(str(ipNet).split('/')[0])
	baseSize = ipNet.numhosts
	newSize = ipaddr.IPNetwork(str(baseIP) + '/' + str(prefixLen)).numhosts

	# Now loop through to create our new subnets...
	newNetCount = pow(2, ( prefixLen - ipNet.prefixlen ) )
	aSubnet = int(baseIP)
	for i in range(newNetCount):
		#netStr = str(ipaddr.IPAddress(aSubnet)) + '/' + str(prefixLen)
		#newNet = ipaddr.IPNetwork(netStr)
		newNet = ipaddr.IPNetwork( \
				str(ipaddr.IPAddress(aSubnet)) + '/' + str(prefixLen) )
		result.append(newNet)
		aSubnet += newSize
	## end for i in range(newNetCount):

	return(result)
## end GenerateSubnets()

