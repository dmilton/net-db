#\L
# 26 Sept 2010		initial creation.

############################################################################
# CalcNextNetwork
# Calculate the next subnet up based on the provided mask.
# Entry:
#	baseNetwork - the network to start with. This is the cidr string
#		representation received from the database.
#	maskLen - the size of network to calculate.
# Exit:
#	result - the cidr string representation of the network adjacent
#		to the one provided as an argument.
#

import ipaddr

def CalcNextNetwork(baseNetwork):
	debug.debug(1, "CalcNextNetwork: baseNetwork=" + str(baseNetwork))

	# the ipaddr module makes this almost trivial.
	ip1 = ipaddr.IPNetwork(baseNetwork)

	# Now make the network bigger by 1 bit.
	ip2 = ip1.supernet(1)

	# Now get the list of subnets that are 1 bit smaller.
	ip3 = ip2.subnet(1)

	# Take the second subnet as our result.
	result = ip3[1]

	debug.debug(1, "CalcNextNetwork: result=" str(result))
	return(str(ip3))
## end CalcNextNetwork()

