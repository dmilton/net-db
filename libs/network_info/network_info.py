#
# Network database table handlers.
#
#
g_version=''

# 29 Apr 2014   Add string and cmd_line_io imports.
# 19 Oct 2013   Add datetime for nicer time stamp output.
# 10 Apr 2013   Add network_log import
#  5 Feb 2013   Add Google IP address (ipaddr) class import.

import datetime
import ipaddress
from string import ascii_letters, digits
from getpass import getuser

import db_access
from network_error import NetException
import network_zones
import network_types
import building_info

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

#  4 Jun 2014   Update for new column names which identify table somewhat.
#  5 Feb 2014   ISO table cannot be part of _networkTables because the network
#               field is a string, not cidr.
# 17 Oct 2013   Add support for network_iso and build a new _networkTables
#               list which contains all of the tables to search when looking
#               for networks in a building.
# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 26 Apr 2013   Remove type and zone tables as these have been
#               moved off into their own libraries.
# 28 Feb 2013   Update the index fields for various tables.
#  7 Feb 2013   Drop the network view and add the network zap table.
# 15 Nov 2012   Add the network_zones table. Correct typo in the view
#               definition.
#  6 Nov 2012   Update to use new dictionary based db access routines
# 29 Oct 2012   Update so the 'table' key only exists in the tableDetail
#               portion of the table. Then specifically call it tableDetail
#               everywhere else. Now the name has a meaning and is not
#               being used differently in different places.
#  3 May 2012   Update network-assigns table for new structure. Remove
#               the networkDetail and networkVlans tables as they are now
#               integrated into the network assigns table and/or children.
#  2 Mar 2012   Add network-free table as another child to network-assigns.
# 24 Feb 2012   Add the network-assigns table and children. Update to
#               reflect the current table definitions.
# 12 Sep 2011   Change the 'number' field to 'vlan_tag' to
#               more accurately    describe what the field means.
# 27 Sep 2010   Add the component table information to the
#               networks view for more generic db_access use.
# 17 Sep 2010   Remove the 'type' field from the table definitions.
# 31 Aug 2010   sensor_location table 'sensor' field needs to permit
#               changes, 'name' needs to deny changes as it is the
#               key field.
# 18 Aug 2010   Initial creation.

###############################################################################
# Network Endpoint Table
# This table contains building codes that are valid endpoints.
# The priority is used to identify prescendence when a link has an endpoint
# at both ends - highest priority gets assigned to bldg_code.
global _networkEndpoints
_networkEndpoints = {
    'database': 'networking',
    'table': 'network_endpoints',
    'role': 'netmon',
    'index': 'ep_code'
}

###############################################################################
# ISO networks table
# This table holds a record of any building that has an ISO network assigned.
# The purpose of this is to track and allow use of IS-IS.
global _networkISO
_networkISO = {
    'database': 'networking',
    'table': 'network_iso',
    'role': 'netmon',
    'index': 'net_name'
}

###############################################################################
# Network Assignment Table
# This is a parent table and should be considered read only.
global _networkAssigns
_networkAssigns = {
    'database': 'networking',
    'table': 'network_assigns',
    'role': 'netview',
    'index': 'net_name'
}

###############################################################################
# Network Table List
# This is a list of all the network table information where the networks
# for various uses can be stored.
global _networkTables
_networkTables = []
# As network tables are defined below, append them to this list.

###############################################################################
# Network Building Assignment Table
# This is a child table to the network_assigns table and is the table
# which contains all end user/building network address spaces.
#
global _networkBuildings
_networkBuildings = {
    'database': 'networking',
    'table': 'network_bldgs',
    'role': 'netmon',
    'index': 'net_name'
}

_networkTables.append(_networkBuildings)


###############################################################################
# Network Links Assignment Table
# This is a child table to the network_assigns table and is the table
# which contains all links used between network devices.
#
global _networkLinks
_networkLinks = {
    'database': 'networking',
    'table': 'network_links',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkLinks)

###############################################################################
# Network Externals table
# This is a child table to the network_assigns table and is for networks
# that are assigned to external groups. We route them but have no real
# control over the address space itself. Rez is an example, as are the
# networks in GlenLea, Straw Bale, etc.
global _networkExternals
_networkExternals = {
    'database': 'networking',
    'table': 'network_extern',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkExternals)

###############################################################################
# Free Network Table
# This is a child table to the network_assigns table and is the table
# which contains all networks that are currently unallocated.
#
global _networkFree
_networkFree = {
    'database': 'networking',
    'table': 'network_free',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkFree)

###############################################################################
# Free Zap Table
# This table holds a record of any network which has been deleted from
# one of the above active tables. This is an audit trail table.
#
global _networkZap
_networkZap = {
    'database': 'networking',
    'table': 'network_zap',
    'role': 'netmon',
    'index': 'id'
}


#\L

# 15 Sep 2015   initial creation.

############################################################################
# ISO Address class.

"""A minimal implementation of ISO host/network addreses.

This library is suitable for accepting and verifying ISO formatted
addresses when used to implement an IS-IS routing infrastructure.

"""

class isoaddress():
    """ISO network addresses"""
    # default area should be 49     49 = private.
    # 49.0001.00a0.c96b.c490.00     hex values.
    # ^^                            AFI (one byte)
    #    ^^^^                       area (0 to 12 bytes)
    #         ^^^^^^^^^^^^^^        station id (6 bytes)
    #                        ^^     selector
    __afi = 0
    __area = 0
    __station_id = 0
    __sel = 0

    def __init__(self, iso_addr):
        # AFI (Authority and Format Indicator) 0x49 is private AFI.
        self.__afi = int(iso_addr[:2],16)
        # Remove the AFI and Selector from the address to get the
        # bytes representing the area and station identifiers.
        area_station = iso_addr[3:-3].split('.')
        # Area is a variable length of 0 to 12 bytes.
        if ( len(area_station) < 3 ):
            raise ValueError("Station Identifier is less than 6 bytes.")
        elif ( len(area_station) > 9 ):
            raise ValueError("Area Identifier is greater than 12 bytes.")
        elif ( len(area_station) == 3 ):
            self.__area = None
            self.__station_id = int(''.join(area_station),16)
        else:
            self.__area = int(''.join(area_station[:len(area_station)-3]),16)
            # Station ID is always 6 bytes, each hex word separated by periods.
            self.__station_id = int(''.join(area_station[-3:]),16)
        if ( self.__station_id == 0 ):
            raise ValueError("Station Identifier cannot be 0")
        # Selector is the last byte/two nibbles.
        self.__sel__ = int(iso_addr[-2:],16)

    def iso_address(self):
        # AFI is always one byte.
        afi = hex(self.__afi)[2:].zfill(2)
        # Area can be ZERO up to 12 bytes! typically 2.
        if ( self.__area <= 255 ):   # 1 byte
            area = area.zfill(hex(self.__area)[2:])
        else:
            area = __add_periods(hex(self.__area)[2:])
        # Station is always 6 bytes.
        station = __add_periods(hex(self.__station_id)[2:].zfill(12))
        # Selector is always 1 byte.
        selector = hex(self.__sel)[2:].zfill(2)
        # Put them all together...
        return(afi + "." + area + "." + station + "." + selector)

    def str(self):
        return(iso_address(self))

    def get_mixed_type_key(self):
        """Return a key suitable for sorting between addresses.
        """
        if isinstance(self, isoaddress):
            return(self.iso_address_exploded())

    def iso_address_exploded(self):
        # Produces the full length address suitable for sorting.
        # AFI is always one byte.
        afi = hex(self.__afi)[2:].zfill(2)
        # Area is maximum 12 bytes so make it so.
        area = hex(self.__area)[2:].zfill(24)
        # Station is always 6 bytes.
        station = __add_periods(hex(self.__station_id)[2:].zfill(12))
        # Selector is always 1 byte.
        selector = hex(self.__sel)[2:].zfill(2)
        # Put them all together...
        return(afi + "." + area + "." + station + "." + selector)

    def compare_networks(self,other):
        left = get_mixed_type_key(self)
        right = get_mixed_type_key(other)
        if ( left == right ):
            result = 0
        elif ( left < right ):
            result = -1
        else:
            result = 1
        return(result)

    def is_private(self):
        if ( self.__afi == int("49",16) ):
            return(true)
        else:
            return(false)
        
    def version(self):
        return(1)

    def area(self):
        return(self.__area)

    def __add_periods(iso_addr_string):
        # Must have an even number of nibbles.
        if ( len(iso_addr_string) % 2 == 1 ):
            addr_string = "0" + iso_addr_string
        else:
            addr_string = iso_addr_string
        # Form string from right to left, inserting periods as we go.
        result = addr_string[-4:]
        for i in list(range(len(addr_string),4,-4)):
            ni = i * -1
            if ( i % 4 == 2 ):
                nie = ni + 2
            else:
                nie = ni + 4
            result = addr_string[ni:nie] + "." + result
        return(result)


#\L

# 15 Sep 2015   initial creation.

############################################################################
# Network Address class.
# Combines python ipaddress class with isoaddress class and adds an
# unnumbered address class.

class base_ipaddress(metaclass=ipaddress):
    pass

class base_isoaddress(metaclass=isoaddress):
    pass

class meta_address(ipaddress, isoaddress):
    pass

class NetworkAddress(ipaddress, isoaddress):
    """
    Create a new network class based on ipaddress, isoaddress, and unnumbered.
    """

    _network_kind = ["IP", "ISO", "unnumbered"]

    def __init__(self, address):
        if ( kind in _network_kind ):
            self.__kind = kind
            self.__address = address
        else:
            raise ValueError("Invalid network kind")

    def type(self):
        return(self.__kind)

    def address(self):
        if ( self.__type == "unnumbered" ):
            return("")
        else:
            return self.__address


#

#  5 Feb 2014   ISO networks must be initialized separately because the
#               network field is a string, not cidr.
# 17 Oct 2013   Add support for ISO networks. Create list of 'networks'
#               tables to allow new network types to be added without
#               having to update this initialization routine.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 13 Feb 2013   initialize all tables but network_zap which doesn't have
#               the correct permissions for netview to read the schema.
# 14 Nov 2012   Changed to prefix global table names with an _

##############################################################################
# initialize()
# Perform initialization process for networks database. 
# Entry: none. initializes internal variables for use.
# Exit: network database fields are now ready to use.

def initialize():
    """
    initialize the networks table access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _networkAssigns, _networkISO, _networkEndpoints, _networkZap
    global _networkTables
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Ensure our dependant tables are initialized.
    building_info.initialize()
    network_types.initialize()
    network_zones.initialize()

    # Table of building codes which can be used as link endpoints.
    _networkEndpoints.update(db_access.init_db_table(_networkEndpoints))

    # Parent table of all network to building assignments.
    _networkAssigns.update(db_access.init_db_table(_networkAssigns))

    # ISO network table is special because ISO networks aren't IP.
    _networkISO.update(db_access.init_db_table(_networkISO))

    # Table with all fields of network_assigns and children.
    # Cannot handle ISO networks.
    _networkZap.update(db_access.init_db_table(_networkZap))

    # initialize all of the network child tables.
    for aTable in _networkTables:
        aTable.update( db_access.init_db_table(aTable) )

    _tables_initialized = True

## end initialize()


#\L

# 25 Aug 2014   New networks should always be marked as being in
#               network_table 'None'. Removed logic to set the table here
#               as it was confusing other routines.
#  5 Mar 2014   Add check for ISO network table. Add more usage comments.
# 17 Oct 2013   Change to use the _networkTables list so additional types
#               of networks (and tables) do not need updates here.
# 15 Oct 2013   If the type is provided, set that value in the dictionary.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 21 Jun 2013   Allow a string type name in addition to a type dictionary.
#  9 May 2013   When a type is not passed, the free table is returned
#               as the default but that was not being set in the result.
#  2 May 2013   Need the network_table field added to a template so
#               we are assured it will be there for add/update/assign.
# 30 Apr 2013   Change name to be consistient with other libraries.
# 21 Apr 2103   update to add network type. This way the record produced
#               contains all fields for that particular type.
# 14 Feb 2013   initial creation.

############################################################################
# new_network
# Create a new network template based on the network type information
# provided.
#
# Entry:
#   netTypeInfo - typeInfo dictionary of type to create.
# Exit:
#   a dictionary containing the fields necessary for creation of a new
#   network entry in the database.
#
#   NOTE: This routine assumes the network is going to be a new network
#   that does not currently exist in any table of the database.
#

def new_network(netTypeInfo):

    # Figure out which table this type belongs in when active.
    netTableName = network_types.get_type_tables(netTypeInfo)[0]

    # Convert the table name into a table structure.
    netTableInfo = _get_table_info(netTableName)

    # Now figure out what fields this table contains.
    result = db_access.new_table_row(netTableInfo)

    # Set the type
    result['net_type'] = netTypeInfo['net_type']

    # New networks are not in any table. Note that when making
    # a copy of a network structure the network_table value must
    # also be copied.
    result['network_table'] = None

    return(result)
## end new_network()


#

# 20 May 2015   Change sort so sorting by IP address actually works.
# 22 Jan 2014   ISO networks now in their own table, remove the argument
#               to generate an ISO network.
# 17 Oct 2013   Add a sort to the end since the multi-table approach
#               may result in a table not sorted by vlan_tag and network.
# 25 Apr 2013   Extract common code and move to _get_networks.
# 19 Apr 2013   Add the source table to the dictionary for each network.
# 17 Apr 2013   Add code to generate the ISO network when an IPv6
#               loopback address exists.
# 13 Feb 2013   Rewrite to use the new table structure.
# 15 Nov 2012   Rather than using the parent table (assigns) use the
#               child tables so we get all the information.
#  6 Nov 2012   Rewrite to use db_access routines that return dictionaries.
# 29 Oct 2012   Initial writing for v2 table format.

############################################################################
# GetNetworks
#
# Input:
#        theCode - the building code to get the list of networks for.
# Output: list of network dictionaries for all networks in the building.
# Exception: NoNetworksDefined
#

def get_networks(theCode):
    """
    Return the active networks for a specific building code.
    """

    sortOrder = "vlan_tag, network"
    result = _get_networks("bldgCode", theCode, sortOrder, withFree=False)

    # Raise an exception if there isn't anything in our result.
    if ( len(result) == 0 ):
        raise NetException("NoNetworksDefined", \
                "No networks were found allocated to building code " + theCode)
    ## if ( len(result) == 0 ):

    # Sort the list by vlan tag and network since the multi-table approach
    # may give results which are not ordered by vlan_tag and network.
    result = sorted( result , key=lambda elem: "%04d" % elem['vlan_tag'] )
    # The sort fails when a "bridge" network type is encountered.
    #result = sorted( result ,
    #        key=lambda item: ipaddress.get_mixed_type_key(item['network']) )

    return(result)
## end get_networks()


#

#  5 Nov 2014   Check the CIDR value provided and raise an exception
#               for illegal or unsupported values.
# 25 Aug 2014   Change setting of the network table based on the
#               table structure rather than a constant. This way the
#               table name in the database could change without also
#               requiring changes here.
# 30 Jan 2014   Initial writing.

############################################################################
# get_free_networks
#
# Input:
#        cidrSize - cidr minimum mask size.
#        netType - network type.
#        netZone - the zone to seach.
# Output: list of network dictionaries for all networks matching
#        the criteria provided.
#

def get_free_networks(cidrSize, netType, netZone):
    """
    Return free networks of a given size (or larger) and/or type.
    """
    # table being searched.
    global _networkFree

    # If cidrSize in invalid, raise exception
    if ( cidrSize > 32 or cidrSize < 16 ):
        raise VauleError("A CIDR mask value of " + str(cidrSize) + \
                " is invalid. CIDR must be >= 16 and <= 32")

    # Order list by network.
    sortOrder = "network"

    # Build our search.
    myWhere = ''
    if ( cidrSize == 0 ):
        cidrSize = 16

    if ( cidrSize >= 16 and cidrSize <= 32 ):
        myWhere = "masklen(network) >= 16 and masklen(network) <= " + \
                str(cidrSize)

    if ( netType > '' ):
        myWhere += " and net_type = \'" + netType + "\'"

    if ( netZone >= 0 ):
        myWhere += " and net_zone = " + str(netZone)

    # Get our list of user and building centric networks.
    freeNets = db_access.get_table_rows(_networkFree, myWhere, sortOrder)

    result = []
    # If we have an IPv6 loopback address, add an ISO network.
    for aNet in freeNets:
        aNet['network_table'] = _networkFree['table']
        result.append( aNet )

    return(result)
## end get_free_networks()


#\L

# 25 Jan 2015   Stop testing for no user network present and let other
#               routines handle the exception if necessary.
#  9 Jan 2015   When assigning specific networks, prime_net was not
#               being set.
#  4 Nov 2014   When a network type is specifically requested, that
#               type can have more than one in a building, and the building
#               already has one of those types - the type got eliminated.
#               Changed so that requested types are always returned as a
#               type to assign.
# 21 Aug 2014   initial creation.

############################################################################
# get_bldg_net_detail
# Get the details of the networks currently assigned to a building.
#
# Entry:
#       bldgCode - the building code to examine assignments for.
#       requestedTypes - set of network types desired in addition to required.
#       ipVersion - 4 or 6 - look for IPv4 or IPv6 network types.
#
# Exit:
#       A dictionary of information is returned about the building:
#       assigned_networks - list of networks currently assigned.
#       assigned_types - set of network types currently assigned.
#       required_type_names - set of network types which are required.
#       multi_type_names - set of types which can have multiple in bldg.
#       type_info_by_name - dictionary of all types, key is type name.
#       prime_net - primary user network (40 for v6, flagged for v4)
#       loop_nets - list of link networks for this ipVersion
#       link_nets - list of link networks for this ipVersion
#
# Exceptions:
#       For user buildings and IPv4, if no primary user address space assigned.

def get_bldg_net_detail(bldgCode, requestedTypes, ipVersion):

    result = {}
    # Produce a list of network types we already have.
    # Get the list of network types we can allocate.
    netTypeDetail = network_types.get_type_detail(ipVersion)
    ipFamily = 'family_ipv' + str(ipVersion)

    # Get the existing list of networks for this building.
    bldgNets = get_networks(bldgCode)

    # Start with the required types and those which were explicitly
    # requested by the caller.
    typesToAssign = set(netTypeDetail['required_type_names'])

    # loop through the networks already assigned, figuring out what
    # type and family it belongs in so we can remove it from the types
    # to assign. This will eliminate 'requestedTypes' when that type is
    # already assigned but there can be multiple of that type assigned
    # in a building.
    assignedTypes = set()
    loopNets = []
    linkNets = []
    for aNetwork in bldgNets:

        netTypeName = aNetwork['net_type']
        netTypeInfo = netTypeDetail['type_info_by_name'][netTypeName]
        netFamily = netTypeInfo['net_family']

        if ( netTypeName in netTypeDetail['family_other'] ):
            # Non IP types should always be recorded but in most cases
            # these should not be _required_ types.
            assignedTypes.add(netTypeName)
            if ( netTypeName in netTypeDetail['uniq_type_names'] ):
                # These networks are always unique in the building.
                typesToAssign.discard(netTypeName)

        elif ( netTypeName in netTypeDetail['family_ip']
                or netTypeName in netTypeDetail[ipFamily] ):
            # We have an IP family, check that we have the right version.
            if ( aNetwork['network'].version ==  ipVersion ):
                # we have a match for a unique type, remove from the list.
                typesToAssign.discard(netTypeName)

            # These can be version specific (ISO/bridge has no .version attr)
            thisNetVer = aNetwork['network'].version

            # Only capture networks of the IP version of address we
            # are assigning: ie - need IPv6 loopback and link for IPv6 user.
            if ( thisNetVer == ipVersion ):
                assignedTypes.add(netTypeName)

            if ( netTypeName == 'loop' ):
                loopNets.append(aNetwork)
            elif ( netTypeName == 'link' ):
                linkNets.append(aNetwork)
            elif ( netTypeName == 'user' ):
                # capture the primary network.
                if (( ipVersion == 4 and aNetwork['prime_net'] ) or
                    ( ipVersion == 6 and aNetwork['vlan_tag'] == 40 ) ):
                    primeNet = aNetwork
        ## end else if ( thisNetFamily not in ['ip', 'ipv4', 'ipv6'] ):
    ## end for aNetwork in bldgNets:

    # Raise an exception if we don't have a prime network.
    try:
        result['prime_net'] = primeNet
    except:
        result['prime_net'] = None

    result['assigned_networks'] = bldgNets
    result['assigned_types'] = assignedTypes
    # Ensure the requested types are part of the list.
    result['types_to_assign'] = typesToAssign.union(requestedTypes)
    result['net_type_detail'] = netTypeDetail
    result['loop_nets'] = loopNets
    result['link_nets'] = linkNets

    return(result)
## end get_bldg_net_detail()


#

# 20 May 2015   Fix cidrContains vs cidrContainedBy.
# 22 Jan 2014   ISO networks now reside in a table, don't generate them.
# 25 Apr 2013   Allow matches that are equal as well as being contained
#               within or containing the network provided.
# 23 Apr 2013   Add str() wrapper for the network since we may receive
#               an ipaddr type rather than a string.
#  5 Mar 2013   update documentation, remove networks view.
#               Update to use new database schema.
#               Add optional argument 'reverse' to permit seraching
#               for a network which contains cidrNetwork instead of
#               searching for networks contained by cidrNetwork.
#  4 Feb 2013   Initial writing

############################################################################
# get_networks_within
#
# Input: cidrNetwork - cidr value to find all networks contained within.
# Output: list of network dictionaries for all networks in the building.
# Exception: NoNetworksDefined
#

def get_networks_within(cidrNetwork, reverse=False):
    """
    Return the active networks for a specific building code.
    """
    global _networkBuildings, _networkAliens, _networkLinks, _networkFree

    # restrict our request and order by vlan id and network.
    if ( reverse == False ):
        findKey = "cidrContainedBy"
    else:
        findKey = "cidrContains"

    sortOrder = "network"

    result = _get_networks(findKey, str(cidrNetwork), sortOrder, withFree=True)

    # Raise an exception if there isn't anything in our result.
    if ( len(result) == 0 ):
        raise NetException("NetNotFound", \
            "the network or address " + str(cidrNetwork) + " was not found")
    ## if ( len(result) == 0 ):

    return(result)
## end get_networks_within()


#\L

# 17 Jun 2014   Raise an exception if more than one match is found.
# 24 Jan 2014   Get information from the free table too.
# 18 Oct 2013   initial creation.

############################################################################
# get_network_info
# Look in the network_xxx tables to find the network specified.
# Entry:
#    searchKey - what value are we searching for? cidrEqual? isoEqual? name?
#    searchValue - the value to search for.
# Exit: The network_xxx tables have been searched, looking for a network
#    that matches searchValue. If one is found it is returned.

def get_network_info(searchKey, searchValue):

    myOrder = "network"
    netList = _get_networks(searchKey, searchValue, myOrder, withFree=True)

    netsFound = len(netList)
    if ( netsFound == 0 ):
        raise ValueError("Network not found in the database")
    elif ( netsFound > 1 ):
        raise ValueError("Multiple networks found in the database.")
    else:
        result = netList[0]

    return(result)
## end get_network_info()


#

# 20 May 2015   Logic was reversed for finding networks contained within
#               and contained by. Add sort functionality so results are
#               returned in network address order.
# 10 Sep 2014   In the case of a exception, the search value may not
#               always be a string so force conversion.
#  4 Sep 2014   ISO search causes SQL syntax error when compared against
#               cidr or inet. Exclude those checks when network type is ISO.
# 25 Aug 2014   Update to use the table column from the table record
#               rather than the hard coded value for network_table.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 22 Jan 2014   ISO networks are in a table, they shouldn't be generated.
#               removed the option to have an ISO network generated.
# 19 Dec 2013   ISO search is different from other networks since
#               a match by network of <<= or >>= doesn't make sense.
# 17 Dec 2013   ISO networks don't have a net_type field, add it so
#               networks of that type can be properly identified.
# 17 Oct 2013   Add support for network_iso table.
# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 14 Jun 2013   Searching for name where clause was broken. Fixed.
#  7 Jun 2013   A bridged vlan doesn't have an address space so a
#               check needs to be made to avoid issues.
# 25 Apr 2013   Initial writing.

############################################################################
# _get_networks
#
# Input:
#        findKey - What to use to search for networks.
#        findValue - the value to seach for
#        sortOrder - the order to return networks in.
#        withFree - include networks from the free table?
# Output: list of network dictionaries for all networks matching
#        the criteria provided.
# NOTE: networks are grouped by the order clause so within the building
#        table they will be ordered but if more than one table has matches
#        then they could appear 'out of sequence.'
#

def _get_networks(findKey, findValue, sortOrder, withFree=False):
    """
    Return the active networks for a specific building code.
    """
    # tables being searched.
    global _networkBuildings, _networkExternals, _networkLinks
    global _networkISO, _networkFree

    myWhere = ""
    linkWhere = ""
    isoWhere = ""
    externWhere = ""
    # Build our search.
    if ( findKey == "bldgCode" ):
        myWhere = "bldg_code = \'%s\'" % (findValue)
        linkWhere = myWhere + " OR ep_code = \'%s\'" % (findValue)
        externWhere = myWhere + " OR remote_code = \'%s\'" % (findValue)
        isoWhere = myWhere
    else:
        if ( findKey == "cidrEqual" ):
            myWhere = "network = '" + str(findValue) + "'"
        elif ( findKey == "cidrContains" ):
            # myWhere = "network <<= '" + str(findValue) + "'"
            myWhere = "network >>= '" + str(findValue) + "'"
        elif ( findKey == "cidrContainedBy" ):
            # myWhere = "network >>= '" + str(findValue) + "'"
            myWhere = "network <<= '" + str(findValue) + "'"
        elif ( findKey == "name" ):
            myWhere = "net_name = '" + findValue + "'"
            isoWhere = myWhere
        elif ( findKey == "isoEqual" ):
            isoWhere = "network = '" + str(findValue) + "'"
        else:
            raise NetException("KeyUknown", "Invalid search criteria " \
                    + findKey + ":" + str(findValue))
        if ( not findKey == "isoEqual" ):
            # Only the building code search requires different clauses
            # for links and external networks.
            externWhere = myWhere
            linkWhere = myWhere

    # Start with an empty list.
    result = []

    # Get our list of user and building centric networks.
    if ( myWhere > "" ):
        bldgNets = db_access.get_table_rows(
                _networkBuildings, myWhere, sortOrder)
        for aNet in bldgNets:
            aNet['network_table'] = _networkBuildings['table']
            result.append( aNet )

    # Get our ISO network if it exists.
    if ( isoWhere > '' ):
        isoNets = db_access.get_table_rows(_networkISO, isoWhere)
    else:
        isoNets = []
    if ( len(isoNets) > 0 ):
        for aNet in isoNets:
            # Add fields not in the database as they are constants.
            aNet['net_type'] = 'iso'
            aNet['vlan_tag'] = 1
            aNet['network_table'] = _networkISO['table']
            result.append(aNet)

    # Add networks which are routed by this router but are not in this building.
    if ( externWhere > "" ):
        externalNets = db_access.get_table_rows(
                _networkExternals, externWhere, sortOrder)
        for aNet in externalNets:
            aNet['network_table'] = _networkExternals['table']
            result.append( aNet )

    # Add links to the core.
    if ( linkWhere > "" ):
        linkNets = db_access.get_table_rows(_networkLinks, linkWhere, sortOrder)
        for aNet in linkNets:
            aNet['network_table'] = _networkLinks['table']
            result.append( aNet )

    # Add any networks which are in the free table.
    if ( myWhere > "" and withFree ):
        freeNets = db_access.get_table_rows(_networkFree, myWhere, sortOrder)
        for aNet in freeNets:
            aNet['network_table'] = _networkFree['table']
            result.append( aNet )

    # Sort the list by network since the multi-table approach
    # will give results unsorted results.
    result = sorted( result , key=lambda elem: "%04d" % elem['vlan_tag'] )
    # result = sorted( result , key=lambda elem: "%s" % (elem['network']) )
    # The sort fails when a "bridge" network type is encountered. 
    # The network field value may be "None"???
    #result = sorted( result , 
    #        key=lambda item: ipaddress.get_mixed_type_key(item['network']) )

    return(result)
## end _get_networks()


#\L

# 18 Aug 2014   Type info vlan tag column renamed to base_vlan_tag.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 13 May 2013   vlan tags in the network_types table can now be
#               negative to represent network types that are unique
#               to a building. Make sure the absolute value in used
#               when assigning the vlan tag to a network.
# 25 Apr 2013   initial creation.

############################################################################
# set_default_info
# Given a network record, set all values to their normal default as if
# this were a free network.
# Entry:
#        netInfo - the network to set to default values.
#        typeInfo - the type information to use for default values.
# Exit: all values required for a free network have been set to defaults.
def set_default_info(netInfo, typeInfo):

    # Give the default network-bogon name.
    netInfo['net_name'] = name_network(netInfo, typeInfo, bogon=True)

    # Only one IPv4 network per building is prime so assume False.
    # No concept of prime for IPv6.
    netInfo['prime_net'] = False
    # Assign the default catch-all building code for free networks.
    netInfo['bldg_code'] = "00"
    # Set the network type
    netInfo['net_type'] = typeInfo['net_type']
    # set the default vlan_tag for this network type.
    netInfo['vlan_tag'] = abs(typeInfo['base_vlan_tag'])

    # Clear the assign by since this one has not been assigned.
    netInfo['assign_by'] = ""
    netInfo['assign_date'] = "-infinity"

    return(netInfo)
## end set_default_info()


#\L

# 24 Apr 2013   Make response a list which includes both the vlan 2
#               subnet and the router loopback address. This permits
#               finding hybrid building/desktop routers in netdisco.
# 19 Apr 2013   Add the source table to the response.
# 15 Apr 2013   correct typo, new db schema uses network_assigns.
# 26 Feb 2013   consistient naming for address version vs family.
# 29 Jan 2013   initial creation.

############################################################################
# get_mgmt_info
# Entry:
#       bldgCode - the building code to get the management network for.
#       version - find IPv4 or IPv6 network?
# Exit:
#       a query is made against the database and the network address space
#       which is being used for the specified building is returned.

def get_mgmt_info(bldgCode, version=4):
    global _networkBuildings

    myWhere = "bldg_code = '" + bldgCode + \
            "' AND ( vlan_tag = 1 OR vlan_tag = 2 ) AND family(network) = " + \
            str(version)
    try:
        result = db_access.get_table_rows(_networkBuildings, myWhere)
        for aNet in result:
            aNet['network_table'] = 'network_bldgs'
        if ( len(result) == 0 ):
            raise NetException("NoMgmtInfo", \
                "Could not find management information about this building.")
    except:
        raise
    ## end try

    return(result)
## end get_mgmt_info()


#\L

# 26 Jan 2015   Change error message to say assign-user, not assign-net.
#               Raise a NetException when a network cannot be found rather
#               than a ValueError so a CannotFind error can be a warning
#               rather than a failure.
# 25 Aug 2014   Set the network table to be that of the free networks table.
#               ensure the vlan_tag is always set and merge the loop to
#               assign names into the loop setting the rest of the values.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Oct 2013   Change error generation to use ValueError for a network
#               that could not be found in the free table. Update to check
#               link networks for a vlan_tag value of 0. If so then use
#               the vlan_tag value from the network type and increment by
#               the number of links required.
# 28 Apr 2013   initial creation.

############################################################################
# find_network
# Given the type of network and a count, locate and return a list of
# networks of the appropriate type.
# Entry:
#       type_info - name of the type of network.
#       bldg_code - building code to use for the network.
#       net_zone - the network zone to find addresses from.
# Exit: A list of networks is returned. If there are not enough networks
#       of the specified type available/free in the database then an
#       exception is returned.

def find_network(type_info, bldg_code, net_zone):

    global _networkFree

    result = []

    # Figure out how many networks to get.
    if ( type_info['assign_method'] == 'next' ):
        net_count = 1
    elif ( type_info['assign_method'] == 'nextn' ):
        if ( type_info['net_type'] == 'link' ):
            ep_codes = get_endpoints(net_zone)
            net_count = len(ep_codes)
        else:
            raise NetException("NoCount", "Don't know how many networks" + \
                "of type '" + type_info['net_type'] + "' to allocate.")
    elif ( type_info['assign_method'] == 'best' ):
        raise NetException("WrongClass", "Networks of type '" + \
                type_info['net_type'] + "' must be assigned manually " + \
                "using assign-user.")
    else:
        raise NetException("WrongClass",
                "The network assignment method '" 
                + type_info['assign_method'] + "' is not found.")

    # Extract free network(s) from the database.
    my_where = "net_type = '" + type_info['net_type'] + "'"
    my_where += " AND net_zone = " + str(net_zone)
    my_order = "network"
    db_result = db_access.get_table_rows(
                    _networkFree, my_where, my_order, net_count)

    # If we didn't get the correct count, exit.
    if ( len(db_result) != net_count ):
        raise NetException("CannotFind", "Could not find " + str(net_count)
                + " free '" + type_info['net_type'] + "' networks(s) in zone "
                + str(net_zone) + ".")

    # copy the db_result into the appropriate type template.
    vlan_index = 0
    for a_net in db_result:
        # create an blank template
        net_copy = new_template(type_info)
        for a_key in list(net_copy.keys()):
            # copy all the common values across.
            if ( a_key in a_net ):
                net_copy[a_key] = a_net[a_key]
        # set the building code.
        net_copy['bldg_code'] = bldg_code
        # Say what table the network is in now.
        net_copy['network_table'] = _networkFree['table']
        # carry forward the network zone.
        net_copy['net_zone'] = a_net['net_zone']
        # Set the vlan tag.
        net_copy['vlan_tag'] = abs(type_info['base_vlan_tag']) + vlan_index
        # Set the endpoint code if this is a link.
        if ( type_info['net_type'] == 'link' ):
            net_copy['ep_code'] = ep_codes[vlan_index]
        # Name the network
        net_copy['net_name'] = name_network(net_copy, type_info)
        # Increment our vlan index so the tags are unique in the building.
        vlan_index += 1

        # add the network to the result list.
        result.append( net_copy )

    return(result)
## end find_network()


#\L

# 20 Mar 2015   Merge together common elements of range and indexed nets.
# 26 Jan 2015   The main user network could be None so check before
#               calling to calculate networks based upon that address space.
#  4 Nov 2014   Arguments for calc_range_net changed to permit more
#               complete network record results.
# 20 Aug 2014   Rewrite to use the network type assign_method field
#               exclusively for computing new networks or finding
#               existing ones to assign.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Apr 2014   initial creation.

############################################################################
# get_v4_networks
# Entry:
#
#       bldg_info - dictionary of information about the building to
#               assign networks to.
#       bldg_net_detail - dictionary of information about networks in the
#               building. Output of get_bldg_net_detail.
#       update_range - should a range assignment actually update the range
#               to indicate a network has been taken? This allows one to
#               see what would happen without actually allocating the net.
#
# Exit: A list of networks matching the needTypesList will be returned. In
#       the case of link, multiple networks could be returned depending on
#       how the network_endpoints for the zone is constructed; one for every
#       endpoint in that zone will be returned. In the case of ISO, a result
#       could be based on an IPv4 or IPv6 loopback address depending on what
#       already exists in the buiding and what was requested in this call. 
# Exceptions:
#       If a specific type of network could not be found in the database
#       then an exception is raised.


def get_v4_networks(bldg_info, bldg_net_detail, update_range):

    result = []
    net_type_detail = bldg_net_detail['net_type_detail']
    types_to_assign = bldg_net_detail['types_to_assign']
    loop_nets = bldg_net_detail['loop_nets']
    main_user_net = bldg_net_detail['prime_net']

    if ( main_user_net == None ):
        raise NetException("CannotComputeNet",
                "The primary user network must be assigned manually.")

    # At this point we have a set of types to assign (types_to_assign), the
    # main user network (main_user_net), the loopback (loop_nets), and the
    # links (linkNets). This should all have been figured out in
    # get_bldg_net_detail.
    # Go through the type list in assign_priority order and determine
    # those networks which are needed.
    for type_name in net_type_detail['types_by_priority']:
        if ( type_name in types_to_assign ):
            type_info = net_type_detail['type_info_by_name'][type_name]
            if ( type_info['assign_method'] == "manual" ):
                raise NetException("CannotComputeNet", "networks of type " 
                        + type_name + " must be assigned manually." )
            elif ( type_info['assign_method'] in ["next", "nextn"] ):
                try:
                    nets_to_add = find_network(type_info,
                            bldg_info['bldg_code'], bldg_info['net_zone'])
                    result += nets_to_add
                except NetException as the_err:
                    if ( the_err.name == "CannotFind" ):
                        print(the_err.detail)
                    else:
                        raise

                # Record loopback interface networks so ISO can be assigned.
                if ( type_name == "loop" ):
                    loop_nets += nets_to_add
            elif ( type_info['assign_method'] == "calc" ):
                if ( type_name == "iso" ):
                    result.append(calc_iso_network(loop_nets, type_info))
                else:
                    if ( main_user_net != None ):
                        result.append(
                            calc_network(main_user_net, type_info, bldg_info) )
            elif ( type_info['assign_method'] in ["range","index"] ):
                new_net = new_template(type_info)
                new_net['bldg_code'] = bldg_info['bldg_code']
                new_net['vlan_tag'] = type_info['base_vlan_tag']
                if ( type_info['assign_method'] == "range" ):
                    new_net['network'] = network_zones.get_range_net(
                                bldg_info, type_info, 4, update_range)
                elif ( type_info['assign_method'] == "index" ):
                    new_net['network'] = network_zones.get_indexed_net(
                            bldg_info, type_info, 4, update_range) 
                new_net['net_name'] = name_network(new_net, type_info)
                result.append( new_net )
            else:
                raise NetException("UnknownAssignMethod",
                    "Unknown assignment method " + type_info['assign_method'])

    return(result)
## end get_v4_networks()


#\L

#  6 Nov 2041   Change call to net_id_2_bldg_net to use calc_range_net.
# 22 Aug 2014   Complete rewrite based on get_v4_networks and the info
#               gathered in get_bldg_net_detail.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Apr 2014   initial creation.

############################################################################
# get_v6_networks
# Entry:
#       bldgInfo - dictionary of information about the building to
#               assign networks to.
#       bldgNetDetail - dictionary of information about networks in the
#               building. Output of get_bldg_net_detail.
#       infoOnly - flag to indicate if information should be updated.
#               When allocating networks, values in the building
#               and/or address range may also need updates to reflect
#               the assignment.
#
# Exit:
#       If this is the first time call for a building then bldgInfo will
#       have an invalid ipv6_netid value; one must be computed and 
#       ipv6_nextnet set to zero. The zone address range must also be
#       updated to indicate a building has been 'consumed'.
#       When infoOnly is true, the next available network id will be
#       determined but not consumed (ie range not updated). Similarly,
#       the building info changes will not be saved either.
#
# Exceptions:

def get_v6_networks(bldgInfo, bldgNetDetail, infoOnly):

    # Start with an empty list of networks to assign/allocate.
    netsToAllocate = []

    # Get all the address spaces for this zone.
    zoneAddrList = network_zones.get_range_info(
                    bldgInfo['net_zone'], "", 6)

    # If we only get one response then all types come from the same block.
    if ( len(zoneAddrList) == 1 ):
        userZoneInfo = zoneAddrList[0]
    else:
        # Where any required user networks will come from.
        for anAddressZone in zoneAddrList:
            if ( "user" in anAddressZone['net_type_list'] ):
                userZoneInfo = network_zones.get_user_zone_info(
                                bldgInfo['net_zone'], ipVersion=6)

    # do we have a base address assigned to the building?
    if ( bldgInfo['ipv6_netid'] < 0 ):
        # no, get the next free one for the building's zone.
        bldgInfo['ipv6_netid'] = network_zones.get_next_bldg(
                userZoneInfo, infoOnly)
        bldgInfo['ipv6_nextnet'] = 0

        # Update the building to show the allocation of a base address.
        if ( infoOnly == False ):
            buildings.update_building(bldgInfo)

    netTypeInfoList = bldgNetDetail['net_type_detail']['type_info_by_name']
    
    needISO = False
    for typeToAdd in bldgNetDetail['types_to_assign']:
        if ( typeToAdd.lower() == "iso" ):
            needISO = True
        elif ( typeToAdd.lower() == "loop" ):
            newLoop = calc_v6_loop(bldgInfo, userZoneInfo,
                            netTypeInfoList['loop'], infoOnly)
            netsToAllocate.append(newLoop)
            loopNetList.append(newLoop)
        elif ( typeToAdd.lower() == "link" ):
            newLinks = calc_v6_links(bldgInfo, epCode='',
                        zoneInfo=userZoneInfo,
                        linkType=netTypeInfoList['link'],
                        doNotUpdate=infoOnly)
            for aNet in newLinks:
                netsToAllocate.append(aNet)
        else:
            typeInfo = netTypeInfoList[typeToAdd]
            # calculate a network to assign.
            if ( len(zoneAddrList) == 1 ):
                zoneRange = zoneAddrList[0]['network_range']
            else:
                # multiple address spaces for this zone, figure out
                # which one to use for this network type.
                zoneFound = False
                anyType = {}
                for anAddressZone in zoneAddrList:
                    if ( typeToAdd in anAddressZone['net_type_list'] ):
                        zoneRange = anAddressZone['network_range']
                        zoneFound = True
                        break
                    elif ( anAddressZone['net_type_list'] == "" ):
                        # Check for a zone that can be used for any type.
                        anyTypeNetwork = anAddressZone['network_range']
                if ( not zoneFound ):
                    # If we didn't find a specific zone,
                    # use the 'any type' default
                    zoneRange = anyTypeNetwork

            # Get a new network entry.
            netToAdd = new_template(typeInfo)

            # Populate the network entry.
            netToAdd['network'] = network_zones.calc_range_net(
                    zoneRange, 64, bldgInfo['ipv6_netid'],
                    bldgInfo['ipv6_nextnet'])
            netToAdd['bldg_code'] = bldgInfo['bldg_code']
            netToAdd['vlan_tag'] = get_next_vlan(
                                    bldgInfo['bldg_code'],
                                    typeInfo['base_vlan_tag'], 6)
            netToAdd['net_name'] = name_network(netToAdd, typeInfo)
            # Do not set the table, let assign_network or assign_network
            # figure out which table to insert the network into.
            netToAdd['network_table'] = None

            netsToAllocate.append(netToAdd)
        ## end else if ( typeToAdd.lower() == "iso" ):

        # If this is a loop network, make sure we record it so
        # generation of an ISO network will use it.

    ## end for typeToAdd in needTypesList:

    # If we need an ISO network, generate that too.
    if ( needISO ):
        isoNetwork = calc_iso_network(loopNetList, netTypeInfoList['iso'])
        netsToAllocate.append(isoNetwork)

    return(netsToAllocate)
## end get_v6_networks()


#\L

#  4 Sep 2014   Shorten length of ISO network to just the network zone
#               and enough room for an IPv4 address in hex.
# 25 Aug 2014   Cleanup the ISO address generation part and remove
#               the network table section - this will get added later
#               before inserting it into the database - or after the
#               insertion has completed successfully. Allow for future
#               use of multiple areas, perhaps based on the building zone.
# 21 Aug 2014   Check that a valid loopback address has been provided.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 12 May 2014   Add more details on the format of an ISO address and
#               extend the system id portion to the full 6 bytes.
# 28 Apr 2014   Add optional isoType argument to save the lookup when
#               the caller has already done this.
#  5 Mar 2014   Remove dependancy on IPv6 and use v6 or v4.
#               Compute using IPv6 when available.
# 23 Oct 2013   Change to use name_network to generate the name.
# 17 Oct 2013   ISO networks are now in their own table and new_template is
#               aware of this and sets values appropriately. This routine
#               now returns a network dictionary that could be added to
#               the ISO networks table.
# 11 Sep 2013   Use type info for name extension.
# 19 Apr 2013   Add network_table entry to response record.
# 18 Apr 2013   use zfill string routine instead of loop.
# 17 Apr 2013   initial creation.

############################################################################
# calc_iso_network
# Entry: 
#       loopNetList - the list of networks assigned as type 'loop'
#       isoType - type info for 'iso' from network_types.
# Exit:
#       returns an ISO network structure. This will be computed on one of
#       two values. If the loopNetList contains an IPv6 address then the
#       ISO network will be the based on the host portion of that address.
#       If the loopNetList contains an IPv4 address then the ISO address
#       will be based on the full IPv4 address. Preference will always be
#       given to the IPv6 address.
#
def calc_iso_network(loopNetList, isoType={}):
    
    # Which network are we using for the basis of the ISO address?
    baseNet = None
    for aNet in loopNetList:
        if ( aNet['network'].version == 6 and aNet['net_type'] == 'loop' ):
            baseNet = aNet
            break
        elif ( aNet['network'].version == 4 and aNet['net_type'] == 'loop'
                and baseNet == None ):
            baseNet = aNet

    # Test that we have a list.
    if ( baseNet == None ):
        raise NetException("NoLoopback", "ISO network addresses are computed"
                + " based on the loopback address but none provided.")

    # If we didn't receive the type info, get it now.
    if ( 'net_type' not in isoType ):
        isoType = network_types.get_type_info('iso')[0]

    # Get a new ISO network template.
    result = new_template(isoType)

    # Assign all common values.
    result['vlan_tag'] = abs(isoType['base_vlan_tag'])
    result['bldg_code'] = baseNet['bldg_code']
    result['net_name'] = name_network(result, isoType, baseNet['net_name'])

    # 49.0001.0000.0000.00
    # isoNet = "49."                # AFI
    # isoNet += "0001."             # area id. (take from zone)
    # isoNet += "0000.0000"         # system id.
    areaId = 1
    isoNetStart = "49." + str(areaId).zfill(4) + "."
    if ( baseNet['network'].version == 6 ):
        # Extract the last two bytes of the IPv6 address.
        netBit = str(baseNet['network'].network_address).split(":")[-1]
        isoAddrPart = "0000." + netBit.zfill(4)
    else:
        # Use the entire IPv4 address represented in HEX.
        netAsHex = hex(int(baseNet['network'].network_address))[2:]
        isoAddrPart = netAsHex[0:4] + '.' + netAsHex[4:8]
    result['network'] = isoNetStart + isoAddrPart + ".00"

    return(result)
## end calc_iso_network()


#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 28 Apr 2014   initial creation.

############################################################################
# calc_v6_loop
# Create a loopback interface network ready to add to the database.
#
# Entry:
#       bldgInfo - the information for this building.
#       zoneInfo - the user address space zone information for the building.
#       loopType - network type information for 'loop'
#       doNotUpdate - flag to indicate if information should be updated.
#
# Exit:
#       a new loopback interface address and assocatied network has
#       been generated and is returned as a result.
#    
def calc_v6_loop(bldgInfo, zoneInfo={}, loopType={}, doNotUpdate=False):

    if ( 'net_type' not in loopType ):
        # Get the type information for a link network.
        loopType = network_types.get_type_info('loop')[0]

    if ( 'net_zone' not in zoneInfo ):
        # Get the zone information for link network (or default)
        zoneInfo = network_zones.get_user_zone_info(bldgInfo['net_zone'], 6)

    newLoop = new_template(loopType)
    newLoop['network'] = network_zones.get_next_loop(zoneInfo, doNotUpdate)
    newLoop['bldg_code'] = bldgInfo['bldg_code']
    newLoop['net_type'] = loopType['net_type']
    newLoop['vlan_tag'] = abs(loopType['vlan_tag'])
    newLoop['prime_net'] = False
    newLoop['net_name'] = name_network(newLoop, loopType)

    return(newLoop)
## end def calc_v6_loop():


#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 28 Apr 2014    initial creation.

############################################################################
# calc_v6_links
# Create as many links as are appropriate for the building.
#
# Entry:
#       bldgInfo - the information about the building.
#       epCode - if provided, only one link is computed for this endpoint.
#       zoneInfo - the zone information for user address spaces.
#       linkType - type information for a link network.
#       doNotUpdate - do not update the zone to show links have been used.
# Exit:
#       return a list of link networks ready to add to the database.

def calc_v6_links(bldgInfo, epCode='',
        zoneInfo={}, linkType={}, doNotUpdate=False):

    result = []

    if ( 'net_type' not in linkType ):
        # Get the type information for a link network.
        linkType = network_types.get_type_info('link')[0]

    if ( 'net_zone' not in zoneInfo ):
        # Get the zone information for link network (or default)
        zoneInfo = network_zones.get_user_zone_info(bldgInfo['net_zone'], 6)

    # Get the endpoints to use for this zone.
    if ( epCode > '' ):
        epList = [epCode]
    else:
        epList = get_endpoints(bldgInfo['net_zone'])

    # Get a link address for each endpoint.
    linkList = network_zones.get_next_link(zoneInfo, len(epList), doNotUpdate)

    for linkCtr in list(range(len(epList))):
        # Create the link network.
        newLink = new_template(linkType)
        newLink['network'] = linkList[linkCtr]
        newLink['bldg_code'] = bldgInfo['bldg_code']
        newLink['ep_code'] = epList[linkCtr]
        # These are all L3 links so the vlan tag is really only for
        # display ordering purposes. Use the endpoint to calculate so
        # should vlan's be used in the core, uniqueness is assured.
        newLink['vlan_tag'] = get_next_vlan( epList[linkCtr], 
                zoneInfo['next_link'] + abs(linkType['vlan_tag']) + linkCtr, 6 )
        # Should verify that this is a valid vlan tag id.)
        # Now that everything is set, figure out the name.
        newLink['net_name'] = name_network(newLink, linkType)
        result.append(newLink)

    return(result)
## end def calc_v6_links():


#

# This whole routine needs to be rewritten. Too many dependencies
# on the network type and variations based on the type. Not enough
# done to ensure the name is unique.

# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 19 Feb 2014   For 2nd networks, sometimes basename is set, sometimes not.
# 18 Feb 2014   Rather than checking for 'prime_net', check the type and
#               force all non-user networks to be lower case.
#               Make sure -v4/-v6 is appended last (2nd networks are wrong)
#  5 Feb 2014   Missing str call for ip version extension.
#  4 Feb 2014   Change name scheme for prime user network. Strip a few
#               characters from the name like quotes, periods, etc.
# 27 Jan 2014   Bogon network name missing str call.
# 23 Oct 2013   Add rules for bogon networks.
# 21 Oct 2013   for 2nd type, accept basename arg.
# 19 Oct 2013   names now contain the type and IP version.
#  1 May 2013   fold names to lower case.

#############################################################################
# name_network
# figure out what to use for the name of a network.
# Entry:
#    netInfo - dictionary of network information.
#    typeInfo - the type info for the network.
#    basename - if provided, use this as the base (for 2nd type networks)
#    bogon - if provided, the name is 'ip-bogon'
# Exit:
#    The name has been determined and is returned as a result.

def name_network(netInfo, typeInfo, basename='', bogon=False):

    # How the name is constructed:
    # var-var2-var3
    # ['bldg_code', "-", "net_type", "-", "2nd"]
    # nameParts = []

    result = ""

    # Figure out the name.
    if ( bogon ):
        try:
            netName = str(netInfo['network'].network_address) + "-bogon"
        except AttributeError:
            netName = str(netInfo['network'].ip) + "-bogon"
        # Now translate periods into hyphens.
        netName = netName.translate(str.maketrans('.','-'))

    elif ( typeInfo['net_type'] == 'loop' ):
        netName = netInfo['bldg_code'] + typeInfo['name_extension']
    elif ( typeInfo['net_type'] == 'iso' ):
        netName = basename.split('-')[0] + typeInfo['name_extension']
    elif ( typeInfo['net_type'] == 'link' ):
        netName = netInfo['ep_code'] + "-" + netInfo['bldg_code'] + "b-link"
    elif ( typeInfo['net_type'] == 'user' ):
        if ( netInfo['prime_net'] == True ):
            netName = netInfo['net_name'].rsplit('-',1)[0]
        else:
            netName = netInfo['bldg_code'] + "-user"
    elif ( typeInfo['net_type'] == '2nd' ):
        if ( netInfo['net_name'] == None ):
            nameBase = basename.rsplit('-',1)[0]
        else:
            nameBase = netInfo['net_name'].rsplit('-',1)[0]
        if ( nameBase == '' ):
            nameBase = netInfo['bldg_code']
        netName = nameBase + typeInfo['name_extension']

    else:
        netName = netInfo['bldg_code'] + typeInfo['name_extension']

    # Add -v4/-v6 on the end. iso causes an error because the network value
    # is a string, not an ipaddr object.
    if ( typeInfo['net_type'] != 'iso' ):
        netName += "-v" + str(netInfo['network'].version)

    # Remove any invalid characters.
    for aChar in list(netName):
        if ( aChar in ascii_letters or aChar in digits or aChar == '-' ):
            result += aChar

    # With the exception of user networks in the building,
    # all network names shoud be lower case.
    if ( typeInfo['net_type'] not in ['user', '2nd'] ):
        result = result.lower()

    return(result)
## end def name_network()


#\L

#  6 Jan 2015   Link networks in the free table could not be updated
#               unless ep_code was specified. Changed check for ep_code
#               so it is only made when the network is being assigned.
#  4 Sep 2014   ISO network updates were failing on a duplicate name.
#  2 Sep 2014   Move the check for dictionary values into a generic
#               routine that can check any value. Removed some almost
#               identical code which varied only in the dictionary key.
# 29 Aug 2014   Check for links is problematic. Need to ensure there is
#               an endpoint code when it is assigned but the network
#               originates from the free table where the endpoint code
#               doesn't exist. The assumption that there will always be
#               an endpoint code is therefore wrong. The type alone does
#               not permit updates while the link is free. The table
#               alone presents a similar problem. Changed the check done
#               logic to allow and add, followed by an update to perform
#               a different set of tests. This may duplicate some tests
#               but ensures completeness. Now have a new check type of
#               assign.
# 18 Aug 2014   Type table column name changes.
# 16 Jun 2014   More updates, missed column name changes.
#  4 Jun 2014   Update for new column names which identify table somewhat.
#  4 Mar 2014   For IPv6, the tables must be checked so a return after
#               checking for the prime network flag wasn't the right
#               way to skip the unique vlan flag test.
#  5 Feb 2014   When updating the network field, checking for containing
#               and contained by present problmes. Instead, check to see
#               if the count is 1 and that should be okay, we should be
#               updating that network anyway.
# 24 Jan 2014   Check to be sure an update where not changing the vlan value
#                does not cause failure.
# 16 Dec 2013   Tests fit into three categories: add, update, and split
#               and vary for bridge and iso networks. Split into sections
#               to account for the type and category differences.
#  7 Nov 2013   Add checks for bridge type networks where no address space
#               has been allocated.
#  5 Nov 2013   Correct an exception to indicate the external vs alien.
# 24 Oct 2013   Add handling of network_iso table. Add a new entry to
#               the network dictionary to indicate a check has been
#               performed. assign_network moves networks from the free
#               table to the 'assigned' table but needs to check before
#               calling update_network which does a check too. Add a check
#               for new networks to verify not null fields have values.
# 17 Oct 2013   Split net with type argument creates multiple networks
#               of the same type in the same building. Since this is
#               the desired effect (free network building code) a check
#               to see that this is unique within the building is invalid.
#               When inserting into the network_free table, checking for
#               values in extern or link tables should not be done.
# 16 Oct 2013   Rename external networks table.
#  4 Jul 2013   Schema changed and prime_net only exists as a field in
#               the network_bldgs table. Only perform those checks when
#               the field is present.
# 10 Jun 2013   Provide more information when ep_code is invalid.
#  2 May 2013   Add code to check for additional fields based on
#               the network type info.
# 22 Apr 2013   add code to check whether the new network is contained
#               within an already existing network. Update to use the
#               new name for a network type 'net_type' instead of name.
# 19 Apr 2013   a result is no longer required. the network_table key
#               inserted by _get_networks solves those issues.
# 18 Apr 2013   initial creation.

############################################################################
# _check_network
# Entry:
#        netToCheck - network dictionary to check.
#        typeInfo - the type information for this network.
#        checkType - add, update, assign, split.
#        netChanging - is the network field itself changing?
# Exit: The network has been checked and verified to ensure there
#        aren't any problems with what has been provided.
#        If any problems exist an exception is raised.
#        
def _check_network(netToCheck, typeInfo, checkType, netChanging=False):
    global _networkAssigns, _networkISO, _networkBuildings
    global _networkLinks, _networkExternals

    if ( "net_check_done" in netToCheck ):
        if ( netToCheck['net_check_done'] == checkType ):
            return

    netToCheck['net_check_done'] = checkType

    # Make sure all not null fields have values.
    activeTableName = network_types.get_type_tables(typeInfo)[0]
    activeTable = _get_table_info(activeTableName)
    if ( checkType in ["add", "update", "assign"] ):
        reqdCols = db_access.get_reqd_cols(activeTable)
        for aColumn in reqdCols:
            goodValue = True
            if ( aColumn in netToCheck ):
                if ( netToCheck[aColumn] == None ):
                    goodValue = False
                if ( type(netToCheck[aColumn]) is str ):
                    if ( netToCheck[aColumn] == '' ):
                        goodValue = False
            else:
                goodValue = False
            if ( goodValue == False ):
                raise ValueError("The network must contain a value for " \
                        + aColumn)

    if ( typeInfo['net_type'] in ["iso","bridge"] ):
        if ( typeInfo['net_type'] == "iso" ):
            # Table for ISO networks.
            vlanCheckTable = _networkISO
        else:
            # Note this is the parent of all network assignment
            # tables except ISO.
            vlanCheckTable = _networkAssigns

        if ( checkType == "add" ):
            # Add requires everything to be unique.
            _check_name(netToCheck['net_name'], _networkISO)
            _check_name(netToCheck['net_name'], _networkAssigns)
            _check_net_equal(netToCheck['network'], _networkISO)
            _check_vlan_unique(
                    netToCheck['network'], typeInfo['net_type'],
                    netToCheck['bldg_code'], typeInfo['base_vlan_tag'], 
                    vlanCheckTable)
            _check_prime(netToCheck)
        elif ( checkType == "update" ):
            if ( netChanging ):
                # The name already exists so a check will fail.
                # The network needs to be unique.
                _check_net_equal(netToCheck['network'], _networkISO)
                _check_name(netToCheck['network'], _networkAssigns)
            else:
                # The network already exists, just check name.
                _check_name(netToCheck['net_name'], _networkISO)
                _check_name(netToCheck['net_name'], _networkAssigns)
        return

    if ( checkType in ["add","split"] ):
        # is the network unique? Assigns are duplicates briefly since
        # they move from the 'free' table to the 'active' table.
        _check_net_equal(netToCheck['network'], _networkAssigns)

    if ( checkType in ["add","assign","update"] ):
        # For updates, make sure we don't prevent an update of the
        # mask length on a network.
        # Make sure this network is not inside an existing network.
        myWhere = "network >> '" + str(netToCheck['network']) + "'"
        netCount = db_access.get_row_count(_networkAssigns, myWhere)
        if ( (netCount > 0 and checkType == "add") or \
                (netCount > 1 and netChanging and 
                    checkType in ["assign","update"]) ):
            raise ValueError("A network containing " + \
                    str(netToCheck['network']) + " already exists.")

        # Make sure this network doesn't contain other networks.
        myWhere = "network << '" + str(netToCheck['network']) + "'"
        netCount = db_access.get_row_count(_networkAssigns, myWhere)
        if ( (netCount > 0 and checkType == "add") or \
                ( netCount > 1 and netChanging and 
                    checkType in ["assign","update"] ) ):
            raise ValueError("Networks contained by " + \
                str(netToCheck['network']) + " exist.")

    if ( netToCheck['network'].version == 6 ):
        _check_prime(netToCheck)
    else:
        # For IPv4 networks, Make sure the vlan is unique when appropriate.
        if ( typeInfo['base_vlan_tag'] < 0 and checkType != 'update' ):
            # This doesn't really cover splits but those are in the free table.
            _check_vlan_unique(netToCheck['network'],
                    typeInfo['net_type'], netToCheck['bldg_code'],
                    typeInfo['base_vlan_tag'], _networkBuildings)

    #
    # Based on the type and the table to insert into, make sure we have
    # all the information.
    #
    insertTableName = activeTableName
    if ( "network_table" in netToCheck ):
        if ( netToCheck['network_table'] != None ):
            insertTableName = netToCheck['network_table']

    if ( insertTableName == _networkExternals['table'] ):

        # Make sure the remote code is set for external networks.
        _check_key_present("remote_code", netToCheck)

    elif ( insertTableName == _networkLinks['table']
            or (netToCheck['net_type'] == "link" and checkType == "assign") ):

        # Make sure the ep code is set for link networks.
        _check_key_present("ep_code", netToCheck)

        # Is this a valid endpoint code?
        epWhere = "ep_code = '" + netToCheck['ep_code'] + "'"
        epCount = db_access.get_row_count(_networkEndpoints, epWhere)
        if ( epCount == 0 ):
            raise ValueError("The endpoint code '" + netToCheck['ep_code'] \
                    + "' is not valid.")

    return()
## end _check_network()

def _check_key_present(theKey, netToCheck):

    # Check the key is present and has a value.
    keyIsMissing = False
    if ( theKey not in netToCheck ):
        keyIsMissing = True
    elif ( netToCheck[theKey] == None ):
        keyIsMissing = True
    elif ( type(netToCheck[theKey]) is str ):
        if ( len(netToCheck[theKey]) == 0 ):
            keyIsMissing = True

    if ( keyIsMissing ):
        raise ValueError("Networks of type " + netToCheck['net_type'] +
            "require a value for " + theKey + ".")

## end _check_key_present()

def _check_name(nameToCheck, tableToCheck):
    # is the name unique?
    myWhere = "net_name = '" + nameToCheck + "'"
    nameCount = db_access.get_row_count(tableToCheck, myWhere)
    if ( nameCount != 0 ):
        raise ValueError("A network named " + nameToCheck + " already exists.")
## end _check_name()

def _check_net_equal(theNet, tableToCheck):
    # is the network unique?
    myWhere = "network = '" + str(theNet) + "'"
    netCount = db_access.get_row_count(tableToCheck, myWhere)
    if ( netCount != 0 ):
        raise ValueError("The network " + str(theNet) + " already exists.")
## end _check_net_equal()

# Check that the vlan (on network type) is unique in the building.
def _check_vlan_unique(theNet, netType, theBldg, theVlan, tableToCheck):
        myWhere = "bldg_code = '" + theBldg + "'"
        if ( netType != "iso" ):
            myWhere += " AND vlan_tag = " + str(abs(theVlan))
        if ( netType not in ["iso","bridge"] ):
            myWhere += "AND family(network) = " + str(theNet.version)
            message = "An IPv" + str(theNet.version)
        else:
            message = "A"

        # Extract the existing record if it exists.
        dbNetInfo = db_access.get_table_rows(tableToCheck, myWhere)
        if ( len(dbNetInfo) != 0 ):
            # The network already exists in the db so update allowed.
            if ( dbNetInfo[0]['network'] == theNet ):
                # The network isn't changing, neither is the vlan_tag
                # so the entry will remain unique.
                return
            else:
                if ( netType != "iso" ):
                    message += " network with the vlan tag " + str(abs(theVlan))
                else:
                    message += "n ISO network"
                message += " already exists in the building."
                raise ValueError(message)

## end _check_vlan_unique()

def _check_prime(netToCheck):

    global _networkBuildings
    if ( "prime_net" in netToCheck ):
        if ( netToCheck['prime_net'] ):
            # Only IPv4 networks of type 'user' can be prime.
            if ( netToCheck['net_type'] != "user" or
                    netToCheck['network'].version == 6 ):
                raise ValueError("A network of type " + \
                    netToCheck['net_type'] + " cannot be the prime network.")

            # The prime network must be unique within the building.
            myWhere = "bldg_code = '" + str(netToCheck['bldg_code']) + \
                "' AND  prime_net = 'T'"
            netCount = db_access.get_row_count(_networkBuildings, myWhere)
            if ( netCount != 0 ):
                raise ValueError(\
                    "There can only be one prime network in a building.")

## end _check_prime()

#\L

# 12 Sep 2014   Change logic for hidden endpoints so hidden endpoints
#               can be extracted for just one network zone.
# 21 Aug 2014   Update column names.
# 28 May 2014   Restrict endpoint list to those with a priority greater
#               than zero so using 0 or -1 can exclude an endpoint from
#               the link assignment process. This permits an ep_code or
#               bldg_code to be in a given zone and have core network
#               attributes but not aggregator attributes.
#  6 Feb 2014   Add detail flag to return list of endpoint records rather
#               than the list of endpoint codes used for establishing links.
# 13 May 2013   initial creation.

############################################################################
# get_endpoints
# Get a list of endpoints in priority order to be used for link assignment.
# Entry:
#        netZone - the zone to get endpoints for.
# Exit:
#        a list of endpoint codes in order of priority is returned.

def get_endpoints(netZone, showDetail=False, showHidden=False):

    global _networkEndpoints

    # If the netZone is < 0 then return all visible endpoints.
    if ( netZone < 0 and not showHidden ):
        # show endpoints for all zones, visible endpoints.
        myWhere =  "core_priority > 0"
    elif ( netZone < 0 and showHidden ):
        # show endpoints for all zones, include hidden endpoints.
        myWhere = ""
    elif ( netZone >= 0 and showHidden ):
        # show endpoints for specific zone, visible endpoints.
        myWhere = "net_zone = " + str(netZone)
    elif ( netZone >= 0 and not showHidden ):
        # show endpoints for specific zone, include hidden endpoints.
        myWhere = "net_zone = " + str(netZone) + " and core_priority > 0"

    # Sort by zone and priority.
    myOrder = "net_zone, core_priority"

    epList = db_access.get_table_rows(_networkEndpoints, myWhere, myOrder)

    # If showDetail is true then return the full endpoint record rather
    # than just the endpoint codes themselves.
    if ( showDetail ):
        result = epList
    else:
        result = []
        for anEndpoint in epList:
            result.append(anEndpoint['ep_code'])

    return(result)
## end get_endpoints()


#\L

# 21 Aug 2014   Update column names.
# 13 May 2013   initial creation.

############################################################################
# get_endpoint_info
# Get a list of endpoints in priority order to be used for link assignment.
# Entry:
#        epCode - the endpoint to get information for.
# Exit:
#        a list of endpoint codes in order of priority is returned.

def get_endpoint_info(epCode):

    global _networkEndpoints

    result = []

    myWhere = "ep_code = '" + str(epCode) + "'"
    myOrder = "core_priority"
    epList = db_access.get_table_rows(_networkEndpoints, myWhere, myOrder)

    if ( len(epList) == 0 ):
        raise NetException("EpNotFound",
                "The endpoint code " + epCode + " was not found.")

    return(epList)
## end get_endpoint_info()


#\L

# 14 Apr 2014   Add check for vlan tag being None and generate error.
# 26 Sep 2013   Add ipVer argument to allow selection by version.
#               Expand range to allow for 64 user vlans in the building.
#  2 May 2013   initial creation.

############################################################################
# get_next_vlan
# Find the next available vlan in a building.
#
# Entry:
#        bldgCode - the building code to search within.
#        vlanTag - the starting vlan tag to search from.
#        ipVer - restrict search to IPv4 or IPv6
# Exit:
#        the database has been searched and the next available free vlan id
#        in the building has been determined and returned.

def get_next_vlan(bldgCode, vlanTag, ipVer=4):
    global _networkAssigns

    if ( vlanTag == None or vlanTag == 0 ):
        # we should never get called with this value
        raise ValueError("the vlan tag cannot be None or zero.")
    elif ( vlanTag < 0 ):
        # this should be unique in the building so return the absolute value
        result = abs(vlanTag)
    else:
        # This has a positive starting value. Search the currently allocated
        # vlans (in the building) starting with this value and use the next
        # available one.
        myWhere = "bldg_code = '" + bldgCode + "'"
        myWhere += " AND family(network) = " + str(ipVer)

        for i in range(vlanTag, vlanTag + 64):
            vlanWhere = myWhere + " AND vlan_tag = " + str(i)
            netCount = db_access.get_row_count(_networkAssigns, vlanWhere)
            if ( netCount == 0 ):
                result = i
                break

    return(result)
## end get_next_vlan()


#\L

# 25 Aug 2014   Revisit the 'network table' vs 'current table' logic.
#               'network table' is always set when a network is read from
#               the database so we know which table it came from. In the
#               case of a new network (should not be in the db) the
#               network_table should be "None" as set by new_template.
# 20 Aug 2014   Need to check for 'current table' so a new network (which is
#               not in any table) gets the correct information. Moved the
#               loop out into _get_table_info so it can be used elsewhere.
#  5 Mar 2014   Add support for the iso network table.
# 24 Oct 2013   Use the _networkTables list so new tables are searched.
#               Improve error checking regarding cases where a network
#               is not currently in a table or the table a network
#               belongs in cannot be determined.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 18 Apr 2013   initial creation.

############################################################################
# _net_in_table
# Given a network, return the table information for that network.
# Entry:
#       netToCheck - the network to find.
# Exit:
#       the network has been located or an exception raised.
#

def _net_in_table(netToCheck):

    # Start with where the network thinks it is.
    if ( "network_table" not in netToCheck ):
        netTableName = None
    else:
        netTableName = netToCheck['network_table']

    result = _get_table_info(netTableName)

    return(result)
## end _net_in_table()


#\L

# 25 Aug 2014   initial creation.

############################################################################
# _get_table_info
# Given a table name, return the full table information dictionary.
# Entry:
#       netTableName - the name of the table to return details on.
# Exit:
#       If the table name is valid, the table info is returned.
#

def _get_table_info(netTableName):
    global _networkTables, _networkFree, _networkISO

    if ( netTableName == None ):
        result = None
    elif ( netTableName.lower() == _networkFree['table'].lower() ):
        result = _networkFree
    elif ( netTableName.lower() == _networkISO['table'].lower() ):
        result = _networkISO
    else:
        # From the table name find the table info.
        result = None
        for aNetTable in _networkTables:
            if ( netTableName == aNetTable['table'] ):
                result = aNetTable
                break

    if ( result == None ):
        raise NetException( "TableUnknown", \
                "Could not find table information for '" 
                + str(netTableName) + "'" )

    return(result)
## end _get_table_info()


#\L

# 20 Dec 2013   loop through all of the different network tables so
#               any possible input key for all network types can be
#               used on the command line.
# 17 Oct 2013   initial creation.

############################################################################
# get_network_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to networks in the network_free table.
# Entry:    none.
# Exit:
#       a dictionary containing all the fields of a network as if it were
#       to be inserted into any table in the networking database. This is
#       somewhat misleading because each of the network_xxx tables has
#       unique fields specific to the kinds of networks kept there. Not
#       all fields are appropirate for all networks. Ie the free_date is
#       not appropriate for a network in active use.

def get_network_fields():

    global _networkTables

    result = {}

    for aTable in _networkTables:
        # Get the list of fields/input values for this table.
        tableInfo = db_access.get_table_keys(aTable)

        # Copy the table information so we can modify it.
        for aKey in list(tableInfo.keys()):
            # translate _ to - so we can accept input with a hyphen.
            key2 = aKey.translate(str.maketrans('-','_'))
            result[key2] = tableInfo[aKey]

    return(result)
## end get_network_fields()


#\L

# 28 Aug 2014   initial creation.

############################################################################
# _set_assign_info
# Entry:
#        netToSet - network dictionary to set info in.
#
# Exit:
#       the network has been adjusted and the user and timestamp
#       fields have been set to force them to default values.
#        
def _set_assign_info(netToSet):

    # Regardless of what happens, always force the user field to
    # be that of the current user and default the assign date to now.
    if ( "assign_by" in netToSet ):
        netToSet['assign_by'] = getuser()
    if ( "assign_date" in netToSet ):
        netToSet['assign_date'] = "now()"
    if ( "free_date" in netToSet ):
        netToSet['free_date'] = "now()"

    return(netToSet)
## end _set_assign_info()

#\L

#  2 Sep 2014   initial creation.

############################################################################
# _set_iso_info
# If this is an ISO network then set the appropriate table and other
# fixed values which are expected in a network info dictionary.
#
# Entry:
#        netToSet - network dictionary to set info in.
#
# Exit:
#       The vlan_tag and table have been set.
#       fields have been set to force them to default values.
#        
def _set_iso_info(netToSet, typeInfo):
    global _networkISO

    result = {}
    # Copy our input network.
    for aKey in list(netToSet.keys()):
        result[aKey] = netToSet[aKey]

    if ( typeInfo['net_type'] == "iso" ):
        # Add constants not stored in the database but required for display.
        result['net_type'] = typeInfo['net_type']
        result['vlan_tag'] = 1
        result['network_table'] = _networkISO['table']

    return(result)
## end _set_iso_info()


#\L

#  2 Sep 2014   Fix issues where a network is being updated and assigned
#               at the same time. Moving tables didn't always get the
#               end result it was supposed to.
# 20 Aug 2014   Change the current table logic to avoid issues where the
#               network does not exist in the database.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network.
#  4 Jul 2013   The assign date was not been correctly updated when
#               a network was assigned. Used now() as the value.
# 10 Jun 2013   Must move the network to the destination table before
#               doing the update or not all fields get set. The current
#               table (network_table) field was not getting updated when
#               the record was moved from one table to another.
#  7 Jun 2013   assign_date may not exist in all netInfo dictionaries so
#               check before attempting to delete it.
# 14 May 2013   Change logic so assignment of a private network, which
#               is an Add gets handled properly.
# 22 Apr 2013   initial creation.

############################################################################
# assign_network
# Assign a network to a specific building.
# This routine can accept two basic classes of network to assign:
# - one that does not exist in the database - computed network values mostly.
# - one that is in the free table and need to be moved to the active table.
#
# Entry:
#       netInfo - network detail record for this network.
#       typeInfo - type detail record for the type of network.
#       bldgInfo - information about the building to assign to.
#
# Exit:
#       The network has either been added to the database or moved from
#       the free table into the appropriate allocated table.
#

def assign_network(netInfo, typeInfo, bldgInfo):

    # Figure out the source (current) and  destination (active) tables.
    if ( "network_table" in netInfo ):
        try:
            currentTable = _get_table_info(netInfo['network_table'])
        except NetException as err:
            if ( err.name == "TableUnknown" ):
                currentTable = None
    else:
        currentTable = None
    activeTableName = network_types.get_type_tables(typeInfo)[0]
    activeTable = _get_table_info(activeTableName)

    # Set the building code.
    netInfo['bldg_code'] = bldgInfo['bldg_code']

    # Now add or move and update the network.
    if ( currentTable == None ):

        # Verify what we have.
        _check_network(netInfo, typeInfo, checkType="add")
        # Ensure the network is marked for the updater and time.
        netInfo = _set_assign_info(netInfo)
        # Assign the new network - ie. add directly to the active table.
        assignedNet = add_network(netInfo, typeInfo,
                addToTable=activeTableName)

    elif ( currentTable['table'] == activeTable['table'] ):

        # Network is active. Moving from one building to another?
        # This is problematic if the network type changed and the
        # table needs to change too; values that should get set won't!
        _check_network(netInfo, typeInfo, checkType="update")
        assignedNet = update_network(netInfo, typeInfo)

    else:

        # The network is in the free table, make sure everything is valid.
        _check_network(netInfo, typeInfo, checkType="assign")
        # Construct a where clause for the move.
        myWhere = "network = '" + str(netInfo['network']) + "'"
        # Move the record into the correct table. This must be done so
        # that update_network has all the fields for the type available.
        netInfo = _set_assign_info(netInfo)
        movedNet = db_access.move_table_rows(
                        currentTable, activeTable, myWhere)[0]

        # Moving the network takes an exact copy of what is in the
        # database and moves it from one table to another. The end
        # result is that some fields may now be present (or gone)
        # that were not present (or have appeared) before.
        # Make sure the fields we have in movedNet are replaced with
        # the values we have in netInfo.
        # in fields that need updating.
        for aKey in list(movedNet.keys()):
            if ( aKey in netInfo ):
                movedNet[aKey] = netInfo[aKey]

        # Make sure the correct table is in movedNet.
        movedNet['network_table'] = activeTableName

        # Finally, update the values based on the changes.
        netToAssign = _set_assign_info(movedNet)
        assignedNet = update_network(netToAssign, typeInfo)

    return(assignedNet)
## end assign_network()


#

# 25 Aug 2014   Change the table logic entirely. Use the network_types
#               call and base the decision entirely on that. New networks
#               should not exist in any table, proceed on that basis.
# 20 Aug 2014   When adding a network and the table is unknown or not
#               specified, the table chosen was the active rather than
#               the free one.
# 19 Feb 2014   Some networks don't have the assign date field so deleting
#               it causes errors, check and only delete if it exists.
#  5 Feb 2014   Networks were not having their assign date timestap updated.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network.
# 24 Oct 2013   More thorough error checking and generation on the table
#               information.
# 18 Oct 2013   Update documentation.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
# 26 Feb 2013   remove typeid references, now using the name.
# 19 Feb 2013   initial writing.

############################################################################
# add_network
# Add the network specified to the database. Unless otherwise specified
# all networks are added to their appropriate 'active' table.
# Entry:
#    netToAdd - the dictionary of the network to add to the database.
#    typeInfo - the network type information.
#    ignoreDup - flag to indicate if the network is inside an alreday
#               existing network - this happens when splitting networks.
#    addToTable - the table to add the network to, should not be set
#                 by routines outside of the network module.
# Exit: The information provided is merged and verified and if valid
#       the network will be added to the database.

def add_network(netToAdd, typeInfo, ignoreDup=False, addToTable=""):
    """
    Add the specified network to the networking database.
    netToAdd = new_template()
    netToAdd['network'] = "10.0.0.0/8"
    # required for adding assigned links.
    netToAdd['ep_code'] = "code from network_endpoints"
    # required for adding external networks.
    netToAdd['remote_code'] = "00"
    # addToTable needs to be one of the valid network tables
    addedNet = add_network(netToAdd, typeInfo)
    """

    # Check the network, adding a network is always a new network
    # but when splitting a network into subnets we need to avoid
    # checking for 'is contained within' which are 'duplicates'
    if ( ignoreDup ):
        _check_network(netToAdd, typeInfo, checkType='split')
    else:
        _check_network(netToAdd, typeInfo, checkType='add')

    if ( addToTable == "" ):
        # Get the table that an assigned network of this type
        # should be inserted into.
        assignedTableName = network_types.get_type_tables(typeInfo)[0]
    else:
        assignedTableName = addToTable

    # Convert the name into a table info structure.
    theTable = _get_table_info(assignedTableName)
    
    # Insert the new network into the table.
    netToAdd = _set_assign_info(netToAdd)
    addedNet = db_access.set_table_row(theTable, netToAdd)[0]

    # set the table the network came from.
    addedNet['network_table'] = assignedTableName
    result = _set_iso_info(addedNet, typeInfo)

    return(result)
## end def add_network():


#\L

# 28 Apr 2015   Trap duplicate key constraint errors and handle cleanly.
#  4 Jun 2014   Update for new column names which identify table somewhat.
#  5 Feb 2014   When changing the network (or mask) the where clause must
#               be based on the network name. Similar issues exist when
#               updating the name, must use the network. Both must be unique
#               so there isn't an issue in that regard but I don't like
#               the idea of changing the carpet you are standing on.
# 19 Dec 2013   The changed _check_network syntax contained a reference
#               to newNet rather than netInfo.
# 17 Dec 2013   Correct for ISO networks not having a type or vlan tag
#               stored in the database table.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network in set-net-info.
# 24 Oct 2013   For consistency with add_network, perform a _check_network
#               here before doing anything.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 10 Jun 2013   Drop _check_network call to avoid duplication. Assumption is
#               this routine will be called with valid information.
# 13 May 2013   Check for a current table of 'None' which is what happens
#               when assigning a private network.
#  2 May 2013   This should not return a list, only one record should
#               ever be updated by this command.
# 18 Apr 2013   initial creation.

############################################################################
# update_network
# Update a network in the database.
# Entry:
#       netInfo - the network to update
#       typeInfo - the network type information for the updated network.
#       netChanging - is the actual network value changing?
# Exit:
#       the network has been updated.

def update_network(netInfo, typeInfo, netChanging=False):

    # Check for the table.
    try:
        theTable = _net_in_table(netInfo)
    except NetException as errInfo:
        if ( errInfo.name == "TableUnknown" ):
            raise ValueError("the network " + netInfo['network'] + \
                " specifies an invalid table:" + netInfo['network_table'])

    # Make sure everything is valid.
    _check_network(netInfo, typeInfo, \
            checkType="update", netChanging=netChanging)

    # Update the network with new values.
    if ( netChanging ):
        # The network itself is changing so use name as the key.
        myWhere = "net_name = '" + netInfo['net_name'] + "'"
    else:
        myWhere = "network = '" + str(netInfo['network']) + "'"

    try:
        updatedNet = db_access.set_table_row(theTable, netInfo, myWhere)[0]
    except psycopg2.IntegrityError as dbError:
        if ( "duplicate key" in dbError.args[1] ):
            raise ValueError(dbError.args)

    # result is directly from the table so preserve that information
    # before returning to the caller.
    updatedNet['network_table'] = theTable['table']
    result = _set_iso_info(updatedNet, typeInfo)

    return(result)
## end update_network()


#\L

# 10 Dec 2014   Code originally written for merging/supernetting of 
#               multiple networks into one but there are instances where
#               the opposite is desired. Modify to permit either case.
#               Since result always consisted of a list of one network,
#               change to return the single network rather than a list.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 24 Apr 2013   initial creation.

############################################################################
# set_prefix_len
# Update the CIDR mask length of a network.
# Entry:
#        networkInfo - the network as currently found in the database.
#        newPrefix - the prefix to apply to the network.
# Exit: The network prefix has been changed to match newPrefix.

def set_prefix_len(networkInfo, newPrefix):
    # Figure out which table this network is in.
    theTable = _net_in_table(networkInfo)

    # Figure out what the new network will look like
    currentPrefix = networkInfo['network'].prefixlen
    if ( newPrefix > currentPrefix ):
        # the network is getting smaller. In this instance, always use
        # the first network in the subnet list.
        newNetwork = list( networkInfo['network'].subnets(
                            new_prefix=newPrefix) )[0]
    else:
        # The network is getting bigger. This means the base network
        # address could be different. ie 192.168.1.128/25 changing
        # mask to 23 would be 192.168.0.0/23.
        newNetwork = networkInfo['network'].supernet(new_prefix=newPrefix)

    # This is what actually needs to happen to set the mask in postgres.
    # set_masklen('192.168.1.0/24'::cidr, 16)
    networkInfo['network'] = newNetwork

    # Names must be unique and since we are updating the network itself
    # (the primary key) we need to use the name instead.
    myWhere = "net_name = '" + str(networkInfo['net_name']) + "'"

    # Update an existing network with new values.
    result = db_access.set_table_row(theTable, networkInfo, myWhere)[0]

    # set the table entry befor returning.
    result['network_table'] = theTable['table']

    return(result)
## end set_prefix_len()


#

# 17 Sep 2014   When deleting a vlan (set-net-info --delete) the
#               where clause must be constructed using net_name.
# 20 Aug 2014   Test for network type was wrong; was testing family
#               rather than the type. Add the netFamily argument so
#               a copy of ip family networks can be made in the zap table.
# 24 Apr 2014   When deleting iso networks the insert into the zap table
#               causes problems. Only insert ip family in the zap table.
# 10 Apr 2013   Remove network_log import, this is done globally.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
#  4 Feb 2012   initial creation

############################################################################
# delete_network
# Given a network dictionary, delete the network from the database.
# Entry:
#       theNetwork - the network dictionary to delete.
#       netFamily - ip, ipv4, or ipv6 - the network type family.
# Exit: the network has been deleted from the database.


def delete_network(theNetwork, netFamily):
    global _networkZap

    # copy all fields of the network into the network_zap table
    # so we could restore it if required. Note that we should
    # preserve the assign by, assing date, and free date values
    # should they exist in theNetwork.
    # At the moment, only IP networks can be restored; there's no
    # facility to restore an ISO network.
    if ( netFamily in ['ip','ipv4','ipv6'] ):
        # Make a copy of the network in the network_zap table.
        if ( "zap_date" in theNetwork ):
            del theNetwork['zap_date']
        theNetwork['zap_by'] = getuser()
        db_access.set_table_row(_networkZap, theNetwork)
    
    # If this raises an exception we have serious problems!
    tableInfo = _net_in_table(theNetwork)

    if ( theNetwork['network'] == None ):
        # deleting a vlan
        myWhere = "net_name = '" + theNetwork['net_name'] + "'"
    else:
        myWhere = "network = '" + str(theNetwork['network']) + "'"
    db_access.delete_table_rows(tableInfo, myWhere)

    return
## end def delete_network():


#\L

# 21 May 2014   initial creation.

############################################################################
# change_bldgs
# Update all networks in a building to be a new building.
# Both the old and new building code must already be defined. If the
# building code is an endpoint, those entries will be updated as well.
# Entry:
#       old_bldg_code - the building to change from.
#       new_bldg_code - the building to change to.
# Exit:
#       all networks assigned to old_bldg_code are now assigned
#       to new_bldg_code.

def change_bldgs(old_bldg_code, new_bldg_code):
    # The tables we will change.
    global _networkTables
    global _networkISO, _networkEndpoints, _networkLinks, _networkExtern

    updated_networks = 0

    # Start with the allocation tables.
    for a_net_table in _networkTables:
        updated_networks += db_access.change_table_rows(
                a_net_table, 'bldg_code', old_bldg_code, new_bldg_code, True)

    # Update any ISO networks.
    updated_networks += db_access.change_table_rows(
            _networkISO, 'bldg_code', old_bldg_code, new_bldg_code, True)

    # Now get links, endpoints, and links.
    myWhere = "ep_code = '" + old_bldg_code + "'"
    oldEp = get_endpoint_info(old_bldg_code)
    myWhere = "ep_code = '" + new_bldg_code + "'"
    new_ep_count = db_access.get_row_count(_networkEndpoints, myWhere)
    if ( new_ep_count == 0 ):
        # must have an endpoint to move links to.
        newEp = {}
        for aKey in oldEp.keys():
            newEp[aKey] = oldEp[aKey]
        newEp['ep_code'] = new_bldg_code
    # Need to ensure the new_bldg_code exists as an endpoint.
    updated_networks += db_access.change_table_rows(
            _networkEndpoints, 'ep_code', old_bldg_code, new_bldg_code, True)
    updated_networks += db_access.change_table_rows(
            _networkLinks, 'ep_code', old_bldg_code, new_bldg_code, True)
    updated_networks += db_access.change_table_rows(
            _networkExtern, 'remote_code', old_bldg_code, new_bldg_code, True)

    return(updated_networks)
## end change_bldgs()


#\L

# 25 Aug 2014   When freeing a private address space, use the python
#               library to determine if the address space is private
#               rather than using the type info flag.
# 20 Aug 2014   When merging networks, the base network is likely already
#               free. Change exception to use NetException so it is easier
#               to handle the expected and normal condition.
# 24 Oct 2013   Use free table from typeInfo rather than hard coded constant.
# 22 May 2013   typeInfo now an explicit argument and an update
#               is performed before moving tables.
# 25 Apr 2013   Update for new table schema.
# 01 May 2012   initial creation.

############################################################################
# free_network
# Return a network to the free pool/table. If the network is one based on
# a calculation then the network is simply deleted frmo the database.
# Entry:
#       netInfo - the network that is to be released.
#       typeInfo - the type information to use for the free network.
#       netZoneID - the zone the network belongs to.
# Exit:
#       the network has been removed from whatever active table it was
#       originally in and placed into the free table if appropriate.
#
def free_network(netInfo, typeInfo, netZoneID):
    global _networkFree

    # Only do something if the current table is not the free table.
    freeTableName = network_types.get_type_tables(typeInfo)[1]

    if ( netInfo['network_table'] == freeTableName ):
        # Trap this so when this is the expected condition it can be handled.
        raise NetException("NetIsFree", "The network "
                + str(netInfo['network']) + " is already free.")


    # Primary key is network so the where is identical whether we are
    # moving into the free table or deleting from the active table.
    myWhere = "network = '" + str(netInfo['network']) + "'"

    # Figure out which table the network is in now.
    srcTableInfo = _net_in_table(netInfo)

    if ( freeTableName in [None, ''] ):
        # No free table so just delete the network.
        db_access.delete_table_rows(srcTableInfo, myWhere)
    else:
        # Get the free table information.
        freeTableInfo = _get_table_info(freeTableName)
        # Move our network to the 'free' table.
        movedNet = db_access.move_table_rows(
                        srcTableInfo, freeTableInfo, myWhere)
        # Set defaults for a free network.
        movedNet = set_default_info(netInfo, typeInfo)
        movedNet['net_zone'] = netZoneID
        # Update the record to match our default values.
        db_access.set_table_row(freeTableInfo, movedNet, myWhere)

    return()
## end free_network()


#\L

# 23 Apr 2014   Mark this as a network that does not exist in the database
#               so add_network or assign_network work properly.
# 29 Dec 2013   no need for a list as a result, just return the
#               single computed network.
# 22 Oct 2013   initial creation.

############################################################################
# calc_network
# Given a starting network, determine what network to use for the specified
# type in the provided zone.
# Entry:
#    netInfo - the network (dict) used as a starting point.
#    typeInfo - the type of (dict) network to compute.
#    bldgInfo - the building info (dict) for the network.
# Exit:
#    The result is a network which is based on netInfo.

def calc_network(netInfo, typeInfo, bldgInfo):

    # Start with an empty template.
    result = new_template(typeInfo)
    result['net_type'] = typeInfo['net_type']
    if ( typeInfo['base_vlan_tag'] != 0 ):
        result['vlan_tag'] = abs(typeInfo['base_vlan_tag'])
    else:
        result['vlan_tag'] = netInfo['vlan_tag']
    result['net_zone'] = bldgInfo['net_zone']
    result['bldg_code'] = bldgInfo['bldg_code']

    # Get the zone information for our starting network.
    userRangeInfo = network_zones.find_user_range(
            bldgInfo['net_zone'], netInfo['network'].version )

    # Figure out what index in the range the starting network is.
    netIndex = get_net_index(
            userRangeInfo['network_range'], netInfo['network'])

    # Get the type specific zone information
    rangeInfoList = network_zones.get_range_info(
            bldgInfo['net_zone'], typeInfo['net_type'],
            netInfo['network'].version )

    # get_range_info returns a list but we expect only one.
    if ( len(rangeInfoList) > 1 ):
        raise NetException("TooManyRanges", "More than one IPv"
                + str(netInfo['network'].version)
                + " address range matches for the zone "
                + bldgInfo['net_zone'] + " and type " + typeInfo['net_type'] )
    else:
        rangeInfo = rangeInfoList[0]

    if ( rangeInfo['net_type_list'] > "" ):
        types_for_zone = rangeInfo['net_type_list'].split(',')

    # Find the same index in the type specific zone.
    newNetAddr = network_zones.calc_indexed_net(
            rangeInfo['network_range'], netInfo['network'].prefixlen, netIndex)

    # If we have multiple types in this zone then split evenly amongst
    # each type. This means the number of types in a zone should be a
    # power of 2 for optimal assignment utilization.
    if ( len(types_for_zone) <= 1 ):
        result['network'] = newNetAddr
    else:
        # How many bits do we need.
        numberTypes = len(types_for_zone)
        for i in range(1, numberTypes+1):
            if ( 2 ** i >= numberTypes ):
                maskSize = i
                break
        # Determine the subnets.
        subnets = list(newNetAddr.subnets(prefixlen_diff=maskSize))

        # Which one do we need?
        for i in range(numberTypes):
            if ( typeInfo['net_type'] == types_for_zone[i] ):
                result['network'] = subnets[i]
                break

    # Set an appropriate name.
    result['net_name'] = name_network(result,
            typeInfo, netInfo['net_name'])

    return(result)
## end calc_network()


#\L

# 26 Mar 2014   The contained within comparision changed with the python3
#               ipaddress class. Fixed the comparisons. Rewrote to use
#               recursion to calculate the depth so a restriction of 16
#               bits isn't required any more.
# 22 Oct 2013   initial creation.

############################################################################
# get_net_index
# Given a base network and a target network, determine how many subnets
# of target.prefexlen size exist between the start of base network and
# the target network.
#
# Entry:
#    baseNet - the base network.
#    targetNet - the target network. Must be in baseNet.
# Exit:
#    The index of the subnet is determined and returned as the result.

def get_net_index(baseNet, targetNet):

    # Check that baseNet contains targetNet
    if ( not baseNet.overlaps(targetNet) ):
        raise ValueError(str(baseNet) + " does not contain " + str(targetNet))

    # Start with 0 to prime the recursive routine.
    result = _get_net_index(baseNet, targetNet, '0')

    return(result)
## end get_net_index()

# Recurse down the tree to find the index rather than looping through
# all of the subnets.

def _get_net_index(baseNet, targetNet, binIndex):

    # A really, really slow way:
    # result = 0
    # for aNet in baseNet.subnet(bitsOfNet):
    #     if ( aNet == targetNet ):
    #         break
    #     result += 1

    try:
        lowerHalf, upperHalf = list(baseNet.subnets())
    except AttributeError:
        lowerHalf, upperHalf = baseNet.subnet(1)

    # Are we on the last step?
    if ( lowerHalf.prefixlen == targetNet.prefixlen ):
        # Yes. Which half is the correct one?
        if ( lowerHalf == targetNet ):
            result = int(binIndex + '0', 2)
        else:
            result = int(binIndex + '1', 2)
    else:
        # Build the index on the way down, one bit at a time.
        if ( lowerHalf.overlaps(targetNet) ):
            binIndex += '0'
            result = _get_net_index(lowerHalf, targetNet, binIndex)
        else:
            binIndex += '1'
            result = _get_net_index(upperHalf, targetNet, binIndex)

    return(result)
## end _get_net_index()


#

# 25 Aug 2014   Similar to add_network, add_vlan is always the addition
#               of something new. Use the network_types call to figure out
#               where the vlan should be added.
#  4 Jun 2014   Update for new column names which identify table somewhat.
# 19 Dec 2013   _check_network call was referenceing netToADd rather than
#               using vlanInfo.
# 16 Dec 2013   Interface to _check_network changed due to issues caused
#               by some checks when updating a network.
#  7 Nov 2013   Checks to find the correct insert table fail because the
#               type (bridge) is not mapping to the appropriate table.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 13 Jun 2013   Initial writing

############################################################################
# add_vlan
# Add a vlan to a building.
# Entry:
#       vlanInfo - the information about the vlan itself.
#       typeInfo - the network type information (usually 'extern')
# Exit:
#       The specified vlan has been added to the appropriate table.

def add_vlan(vlanInfo, typeInfo):
    """
    #Add the specified vlan to the networking database.
    typeInfo = network_types.get_type_info('bridge')
    vlanInfo = networks.new_template(typeInfo)
    vlanInfo['net_name'] = "test-vlan"
    vlanInfo['vlan_tag'] = 289
    vlanInfo['bldg_code'] = 'ci'
    vlanInfo['remote_code'] = 'qa'
    # or vlanInfo['remote_code'] = '00'    # parking space.
    # Now add the vlan.
    networks.add_vlan(vlanInfo, typeInfo)
    """

    # Make sure everything we have is valid.
    _check_network(vlanInfo, typeInfo, checkType='add')

    # Get the table that an assigned vlan should be inserted into.
    assignedTableName = network_types.get_type_tables(typeInfo)[0]

    # Convert the name into a table info structure.
    theTable = _get_table_info(assignedTableName)

    # Insert the VLAN into the appropriate table.
    vlanInfo = _set_assign_info(vlanInfo)
    result = db_access.set_table_row(theTable, vlanInfo)[0]

    # Set the table the vlan was added to.
    result['network_table'] = assignedTableName

    return(result)
## end def add_vlan():


#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 23 Sep 2013   set_table_row returns a list, need to set the network_table
#               field in each network dictionary in the list.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 13 Jun 2013   initial creation.

############################################################################
# update_vlan
# Update a vlan in the network table.
# Entry:
#       vlanInfo - the information to update.
#       typeInfo - the type information to use for the updated vlan.
# Exit:
#       the vlan has been updated.

def update_vlan(vlanInfo, typeInfo):

    try:
        theTable = _net_in_table(vlanInfo)
    except NetException as errInfo:
        if ( errInfo.name == "TableUnknown" ):
            raise ValueError("the vlan " + vlanInfo['net_name'] + \
                " specifies an invalid update table:" + activeTable)

    # Update the vlan with new values.
    myWhere = "bldg_code = '" + vlanInfo['bldg_code'] + "'"
    myWhere += " AND vlan_tag = " + str(vlanInfo['vlan_tag'])
    result = db_access.set_table_row(theTable, vlanInfo, myWhere)

    # Result is a list, fix the network_table for each one.
    for aNet in result:
        aNet['network_table'] = theTable['table']

    return(result)
## end update_vlan()


#\L

# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Add search of the external networks table to allow addition
#               of secondary addresses on external networks.
# 22 May 2013   initial creation.

############################################################################
# get_vlan_info
#       Given a building code and vlan tag, find the network in the database
#       that matches the building code and vlan tag.
#       Functionally this is to allow addition of a secondary address space
#       to a network. This should find both the primary and any secondary
#       address spaces associated with that vlan.
# Entry:
#       bldgCode - the building code to get information about.
#       vlanTag - the vlan tag to look for.
# Exit:
#       Only two tables network_bldgs and network_extern are
#       searched. network_free, network_link, and network_iso are not.

def get_vlan_info(bldgCode, vlanTag):
    global _networkBuildings, _networkExternals

    # Only search the network buildings table.
    bldgWhere = "bldg_code = '" + bldgCode + "'"
    rmtWhere = " OR remote_code = '" + bldgCode + "'"
    vlanWhere = " AND vlan_tag = " + str(vlanTag)
    myWhere = bldgWhere + vlanWhere
    extWhere = "(" + bldgWhere + rmtWhere + ")" + vlanWhere

    myOrder = "network"

    # Check the buildings table
    bldgNets = db_access.get_table_rows(_networkBuildings, myWhere, myOrder)
    for aNet in bldgNets:
        aNet['network_table'] = _networkBuildings['table']
    # Check the external networks table
    extNets = db_access.get_table_rows(_networkExternals, extWhere, myOrder)
    for aNet in extNets:
        aNet['network_table'] = _networkExternals['table']

    result = bldgNets + extNets

    return(result)
## end get_vlan_info()


#\L

# 17 Oct 2013   initial creation.

############################################################################
# get_endpoint_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to building rooms.
# Entry:    none
# Exit:
#       a dictionary of keys with value None that correspond to all the
#       fields for a network endpoint.

def get_endpoint_fields():

    global _networkEndpoints

    result = {}

    # Get the list of fields/input values for this table.
    tableInfo = db_access.get_table_keys(_networkEndpoints)

    # Copy the table information so we can modify it.
    for aKey in list(tableInfo.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = tableInfo[aKey]

    return(result)
## end get_endpoint_fields()


#\L

# 17 Oct 2013    initial creation.

############################################################################
# new_endpoint
# Entry:    none
# Exit:
#    a dictionary containing the fields necessary for creation of a new
#    network endpoint in the database.

def new_endpoint():
    global _networkEndpoints

    result = db_access.new_table_row(_networkEndpoints)

    return(result)
## end new_endpoint()


#

# 21 Aug 2014   Update column names.
# 16 Jun 2014   endpoint priority can now be zero so an endpoint can
#               be defined but excluded from the link assignment process.
# 17 Oct 2013   Initial writing

############################################################################
# add_endpoint
# Add a new endpoint to a zone.
# Entry:
#       epInfo - endpoint information dictionary.
# Exit:
#       a new endpoint corresponding to epInfo has been added.

def add_endpoint(epInfo):
    """
    # Add the specified endpoint to the networking database.
    epInfo = networks.new_endpoint()
    epInfo['ep_code'] = 'ep1'
    epInfo['core_priority'] = -1
    epInfo['net_zone'] = 999
    networks.add_endpoint(epInfo)
    """

    global _networkEndpoints

    # Make sure the name is not a duplicate.
    myWhere = "ep_code = '" + epInfo['ep_code'] + "'"
    epCount = db_access.get_row_count(_networkEndpoints, myWhere)
    if ( epCount != 0 ):
        raise NetException( "EndpointExists", epInfo['ep_code'] )

    # Default the priority if it isn't already set.
    if ( epInfo['core_priority'] == None ):
        epInfo['core_priority'] = -1

    # Endpoints within a zone must all have unique priorities if they
    # are greater than zero. If one exists with the same value as provided,
    # change it to force a difference.
    if ( epInfo['core_priority'] > 0 ):
        myWhere = "net_zone = " + str(epInfo['net_zone'])
        myWhere += "AND core_priority = " + str(epInfo['core_priority'])
        epCount = db_access.get_row_count(_networkEndpoints, myWhere)
        if ( epCount != 0 ):
            # Have matching endpoint priority. Make it something different.
            epInfo['core_priority'] += 10

    result = db_access.set_table_row(_networkEndpoints, epInfo)[0]

    return(result)
## end def add_endpoint():


#\L

# 17 Oct 2013   initial creation.

############################################################################
# update_endpoint
# Update an existing endpoint.
# Entry:
#       epInfo - the endpoint to update.
# Exit:
#       the endpoint has been updated.

def update_endpoint(epInfo):
    global _networkEndpoints

    myWhere = "ep_code = '" + epInfo['ep_code'] + "'"
    result = db_access.set_table_row(_networkEndpoints, epInfo, myWhere)[0]

    return(result)
## end update_endpoint()


#

# 17 Oct 2013   initial creation.

############################################################################
# delete_endpoint
# Given an endpoint code, delete the endpoint from the database.
# Entry:
#        epCode - the endpoint to delete.
# Exit: the endpoint has been deleted from the database.

# 4 Feb 2012    initial creation

def delete_endpoint(epCode):
    global _networkEndpoints

    myWhere = "ep_code = '" + epCode + "'"
    db_access.delete_table_rows(_networkEndpoints, myWhere)

    return()
## end def delete_endpoint():


#\L

# 10 Jun 2014   Kludge around the net_name/name column rename until
#               the change reaches production.
#  7 Nov 2013   initial creation.

############################################################################
# list_networks
# Entry:
#    bldgCode - the building code to list networks for.
# Exit:
#    the list of networks allocated to the building has been printed.

def list_networks(bldgCode):
    # Get our list of networks in the building.
    netList = get_networks(bldgCode)

    # Change any values we want for output formatting.
    for aNetwork in netList:
        # Mark primnet so it shows on output.
        if ( 'prime_net' in aNetwork ):
            if ( aNetwork['prime_net'] ):
                aNetwork['vlan_tag'] = str(aNetwork['vlan_tag']) + '*'
        theDate = str(aNetwork['assign_date'])[0:10]
        try:
            theUser = aNetwork['assign_by'].strip()
        except:
            theUser = 'netmon'
        aNetwork['assign_date'] = theUser + ":" + theDate

    # Print our table.
    netTitle = "Networks Allocated:"
    netHead = ("Name", "VLAN", "Network", "Type", "Assigned")
    netKeys = ("net_name", "vlan_tag", "network", "net_type", "assign_date")
    from cmd_output import print_table
    print_table(netTitle, netHead, netKeys, netList)

    return()
## end list_networks():


# vim: syntax=python ts=4 sw=4 showmatch et :
