#
# Network database table handlers.
#
#
g_version=''

# 29 Apr 2014   Add string and cmd_line_io imports.
# 19 Oct 2013   Add datetime for nicer time stamp output.
# 10 Apr 2013   Add network_log import
#  5 Feb 2013   Add Google IP address (ipaddr) class import.

import datetime
import ipaddress
from string import ascii_letters, digits
from getpass import getuser

import db_access
from network_error import NetException
import network_zones
import network_types
import building_info

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

#  4 Jun 2014   Update for new column names which identify table somewhat.
#  5 Feb 2014   ISO table cannot be part of _networkTables because the network
#               field is a string, not cidr.
# 17 Oct 2013   Add support for network_iso and build a new _networkTables
#               list which contains all of the tables to search when looking
#               for networks in a building.
# 16 Oct 2013   Rename external networks table.
#  7 Oct 2013   Changed name of aliens table to externals which describes
#               what they are a little better; networks that are handled
#               externally.
# 26 Apr 2013   Remove type and zone tables as these have been
#               moved off into their own libraries.
# 28 Feb 2013   Update the index fields for various tables.
#  7 Feb 2013   Drop the network view and add the network zap table.
# 15 Nov 2012   Add the network_zones table. Correct typo in the view
#               definition.
#  6 Nov 2012   Update to use new dictionary based db access routines
# 29 Oct 2012   Update so the 'table' key only exists in the tableDetail
#               portion of the table. Then specifically call it tableDetail
#               everywhere else. Now the name has a meaning and is not
#               being used differently in different places.
#  3 May 2012   Update network-assigns table for new structure. Remove
#               the networkDetail and networkVlans tables as they are now
#               integrated into the network assigns table and/or children.
#  2 Mar 2012   Add network-free table as another child to network-assigns.
# 24 Feb 2012   Add the network-assigns table and children. Update to
#               reflect the current table definitions.
# 12 Sep 2011   Change the 'number' field to 'vlan_tag' to
#               more accurately    describe what the field means.
# 27 Sep 2010   Add the component table information to the
#               networks view for more generic db_access use.
# 17 Sep 2010   Remove the 'type' field from the table definitions.
# 31 Aug 2010   sensor_location table 'sensor' field needs to permit
#               changes, 'name' needs to deny changes as it is the
#               key field.
# 18 Aug 2010   Initial creation.

###############################################################################
# Network Endpoint Table
# This table contains building codes that are valid endpoints.
# The priority is used to identify prescendence when a link has an endpoint
# at both ends - highest priority gets assigned to bldg_code.
global _networkEndpoints
_networkEndpoints = {
    'database': 'networking',
    'table': 'network_endpoints',
    'role': 'netmon',
    'index': 'ep_code'
}

###############################################################################
# ISO networks table
# This table holds a record of any building that has an ISO network assigned.
# The purpose of this is to track and allow use of IS-IS.
global _networkISO
_networkISO = {
    'database': 'networking',
    'table': 'network_iso',
    'role': 'netmon',
    'index': 'net_name'
}

###############################################################################
# Network Assignment Table
# This is a parent table and should be considered read only.
global _networkAssigns
_networkAssigns = {
    'database': 'networking',
    'table': 'network_assigns',
    'role': 'netview',
    'index': 'net_name'
}

###############################################################################
# Network Table List
# This is a list of all the network table information where the networks
# for various uses can be stored.
global _networkTables
_networkTables = []
# As network tables are defined below, append them to this list.

###############################################################################
# Network Building Assignment Table
# This is a child table to the network_assigns table and is the table
# which contains all end user/building network address spaces.
#
global _networkBuildings
_networkBuildings = {
    'database': 'networking',
    'table': 'network_bldgs',
    'role': 'netmon',
    'index': 'net_name'
}

_networkTables.append(_networkBuildings)


###############################################################################
# Network Links Assignment Table
# This is a child table to the network_assigns table and is the table
# which contains all links used between network devices.
#
global _networkLinks
_networkLinks = {
    'database': 'networking',
    'table': 'network_links',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkLinks)

###############################################################################
# Network Externals table
# This is a child table to the network_assigns table and is for networks
# that are assigned to external groups. We route them but have no real
# control over the address space itself. Rez is an example, as are the
# networks in GlenLea, Straw Bale, etc.
global _networkExternals
_networkExternals = {
    'database': 'networking',
    'table': 'network_extern',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkExternals)

###############################################################################
# Free Network Table
# This is a child table to the network_assigns table and is the table
# which contains all networks that are currently unallocated.
#
global _networkFree
_networkFree = {
    'database': 'networking',
    'table': 'network_free',
    'role': 'netmon',
    'index': 'network'
}

_networkTables.append(_networkFree)

###############################################################################
# Free Zap Table
# This table holds a record of any network which has been deleted from
# one of the above active tables. This is an audit trail table.
#
global _networkZap
_networkZap = {
    'database': 'networking',
    'table': 'network_zap',
    'role': 'netmon',
    'index': 'id'
}

# vim: syntax=python ts=4 sw=4 showmatch et :

