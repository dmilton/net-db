#
g_version='13 Apr 2015 - aca4442'

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network AFL License table
#
global _routerAFL
_routerAFL = {
    'database': 'networking',   # name of the database
    'role': 'netmon',           # role for database access
    'table': 'router_afl',      # name of the table
    'index': 'auth_code',       # name of index column
    'indexType': 'string',      # data type of index column
}

global _aflModels
_aflModels = {
    'database': 'networking',    # name of the database
    'role': 'netmon',            # role for database access
    'table': 'afl_models',       # name of the table
    'index': 'auth_code',        # name of index column
    'indexType': 'string',       # data type of index column
}


#

# 6 Nov 2012        initial creation

##############################################################################
# initialize()
# Perform initialization process for network afl information table. 
# Entry: none. initializes internal variables for use.
# Exit: network afl table is now ready to use.

def initialize():
    """
    initialize the network afl table access field variables.
    """

    # The table of AFL license assignments.
    global _routerAFL
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _routerAFL.update(db_access.init_db_table(_routerAFL))

    _tables_initialized = True

## end initialize()



#

# 12 Apr 2013        Change argument to use the router name rather than
#                    a building code. There is no guarantee that a building
#                    router name is always the building code followed by b1.
# 12 Oct 2012        Initial writing.

############################################################################
# get_afl_info
#
# Input: router - the building code to get the list of licenses for.
# Output: list of license dictionaries for all switches in the router.
# Exception: NoAflAssigned
#

def get_afl_info(router):
    """
    Return the AFL license info for a specific building code.
    """

    global _routerAFL

    myWhere = "router = '" + router + "'"
    myOrder = "member_id"
    result = db_access.get_table_rows(_routerAFL, myWhere, myOrder)

    return(result)
## end get_afl_info()



#

# 13 Dec 2012        get_table_rows returs a list but the result is supposed
#                    to be a dicitonary. Extract dictionary from the list and
#                    return that as a result.
# 13 Dec 2012        Initial writing.

############################################################################
# find_afl_info
#
# Input: serial number - the switch serial number the licenses for.
# Output: dictionary for the license assigned to the switch.
#         if no license is assigned, None is returned.
#
#

def find_afl_info(searchValue, searchField='serial_nbr'):
    """
    Return the AFL license info for the specified serial number or
    authorization code.
    """

    global _routerAFL

    if ( searchField == "serial_nbr" ):
        myWhere = "serial_nbr = '" + searchValue + "'"
    elif ( searchField == "auth_code" ):
        myWhere = "auth_code = '" + searchValue + "'"
    else:
        raise ValueError("unknown search field: " + searchField)

    result = db_access.get_table_rows(_routerAFL, myWhere)

    # result is always a list from get_table_rows so...
    if ( len(result) == 0 ):
        result = None
    else:
        result = result[0]
    ## end if ( len(result) == 0 ):

    return(result)
## end find_afl_info()



#

# 16 Jun 2014   Now have multiple kinds of licenses to handle so not only
#               must a count be used, the correct kind of license must be
#               returned to the caller.
# 11 Apr 2013   update exception to make it consistient amongst modules.
# 18 Dec 2012   print count properly, needed string conversion.
#               passed wrong argument for sort order to get_table_rows.
#               query returned empty result because switch_model is not
#               an empty string '', seem to be null.
# 13 Dec 2012   sort auth codes by rtu_serial for assignment.
# 15 Nov 2012   initial writing.

############################################################################
# get_licenses
# Find liceneses in the database that have not been assigned yet.
#
# Input:
#       model - the type of key required.
#       count - how many licenses are required?
#
# Output: list of 'count' license dictionaries which have not been assigned.
# Exception: NoAflAvailable
#

def get_licenses(model, count):
    """
    Return a list of count licenses for a switch type model.
    """

    global _routerAFL

    myWhere = "member_id = -1 AND serial_nbr = ''"
    myWhere += " AND LOWER(afl_model) = LOWER('" + license_model(model) + "')"
    result = db_access.get_table_rows(_routerAFL, myWhere, \
                                sortOrder='rtu_serial', limit=count)

    # Verify that we have a list of licenses.
    aflCount = len(result)
    if ( aflCount != count ):
        raise NetException("not found", "insufficient free licenses, " + \
                str(count) + " needed, " + str(aflCount) + " available.")

    return(result)
## end get_licenses()


#\L

# 16 Jun 2014   initial creation.

############################################################################
# license_model
# Entry:
#       model - switch model number
# Exit:
#       the model number has been converted into the type of authorization
#       code (AFL Part Number) required for the type of switch.
# Exceptions:
#       if the model provided is unrecognized an exception is raised.

def license_model(switch_model):
    global _aflModels

    # Here is what should really happen:
    #myWhere = "switch_model = '" + switch_model + "'"
    #aflModelInfo = db_access.get_table_rows(_aflModels, myWhere)
    #if ( len(aflModelInfo) == 0 ):
    #    raise ValueError("Unknown switch model type: " + switch_model)
    #else:
    #    result = aflModelInfo['afl_model']

    # This is a quick fix to handle switch model types in AFL licenses.
    # What is really required is a new table to map the switch model
    # type as returned via SNMP to the AFL model number type as
    # documented here:
    # http://www.juniper.net/techpubs/en_US/junos12.3/topics/concept/
    #           ex-series-software-licenses-overview.html
    # The table would then be referenced here making future updates
    # to this code for new model types unnecessary.
    if ( switch_model.upper() in ['EX3200-24P', 'EX3200-24T', 'EX4200-24F',
                'EX4200-24P', 'EX4200-24PX', 'EX4200-24T'] ):
        result = 'EX-24-AFL'
    elif ( switch_model.upper() in ['EX3200-48P', 'EX3200-48T', 'EX4200-48F',
                'EX4200-48P', 'EX4200-48PX', 'EX4200-48T',
                'EX4500-40F-BF', 'EX4500-40F-BF-C',
                'EX4500-40F-FB', 'EX4500-40F-FB-C'] ):
        result = 'EX-48-AFL'
    elif ( switch_model.upper() == 'EX4550' ):
        result = 'EX4550-AFL'
    elif ( switch_model.upper() == 'EX6210' ):
        result = 'EX6210-AFL'
    elif ( switch_model.upper() == 'EX8208' ):
        result = 'EX8208-AFL'
    elif ( switch_model.upper() == 'EX8216' ):
        result = 'EX8216-AFL'
    elif ( switch_model.upper() == 'EX-XRE200' ):
        result = 'EX-XRE200-AFL'
    elif ( switch_model.upper() == 'EX9204' ):
        result = 'EX9204-AFL'
    elif ( switch_model.upper() == 'EX9208' ):
        result = 'EX9208-AFL'
    elif ( switch_model.upper() == 'EX9214' ):
        result = 'EX9214-AFL'
    else:
        raise ValueError("Unknown switch model type: " + switch_model)

    return(result)
## end license_model()


#\L

# 26 Mar 2013        initial creation.

############################################################################
# get_fields
def get_fields():

    global _routerAFL

    result = db_access.get_table_keys(_routerAFL)

    return(result)
## end get_fields()



#\L
# dd mmm yyyy                initial creation.

############################################################################
# new_license
def new_license():

    global _routerAFL

    result = db_access.new_table_row(_routerAFL)

    return(result)
## end new_license()



#\L

# 20 Sep 2013        set_table_row now returns the updated/inserted
#                    values from the database. get_table_rows call not needed.
# 11 Apr 2013        update exception to make it consistient amongst modules.
#  8 Apr 2013        db_access.set_table_row no longer returns a result so
#                    a call to get_table_rows is required.
# 27 Mar 2013        initial creation.

############################################################################
# add_license
# Add an AFL license to the database.
# Entry: theLicense - the license record to add to the database.
# Exit: the database has been updated to reflect the license provided.
# 
def add_license(theLicense):
    """
    Update the fields of an AFL license row.
    """

    global _routerAFL

    # Grab the license from the database.
    myWhere = "auth_code = '%s'" % (theLicense['auth_code'],)

    licenseInDb = db_access.get_row_count(_routerAFL, myWhere)

    if ( licenseInDb > 0 ):
        raise NetException("duplicate", \
                "auth_code " + theLicense['auth_code'])

    # Pass this to the database routines to do the update.
    result = db_access.set_table_row(_routerAFL, theLicense, myWhere)

    return(result)
## end add_license()



#\L

# 20 Sep 2013        set_table_row now returns the updated/inserted
#                    values from the database. get_table_rows call not needed.
# 11 Apr 2013       update exception to make it consistient amongst modules.
#  8 Apr 2013        db_access.set_table_row no longer returns a result so
#                    a call to get_table_rows is required.
#  1 Apr 2013        Result from get_table_rows could be a list with zero
#                    entries, cannot index [0]. Need index [0] once a
#                    result is verified.
# 31 Mar 2013        Change routine name to update_license and return a
#                    result containing the updated record. Return an
#                    exception when the record does not exist.
# 18 Dec 2012        get_table_rows returs a list and in this instance that
#                    list should always have only one entry.
# 10 Dec 2012        initial creation.

############################################################################
# update_license
# Update an AFL license to reflect the changes that have been applied to
# the dictionary. 
# Entry: theLicense - the license record from the database.
# Exit: the database has been updated to reflect the license provided.
# 
def update_license(theLicense):
    """
    Update the fields of an AFL license row.
    """

    global _routerAFL

    # Grab the license from the database.
    myWhere = "auth_code = '%s'" % (theLicense['auth_code'],)

    licenseInDb = db_access.get_table_rows(_routerAFL, myWhere)

    if ( len(licenseInDb) != 1 ):
        raise NetException("not found", \
                "auth code " + theLicense['auth_code'])

    # turn the list returned by get_table_rows into a singe entry.
    licenseInDb = licenseInDb[0]

    # Create an empty row to populate with updated data.
    updatedLicense = db_access.new_table_row(_routerAFL)

    # Compare the license we got to the one from the database
    # and build a third one which only has the key (auth-code)
    # and the updated fields present.
    for aKey in list(licenseInDb.keys()):
        if ( (aKey in theLicense) == True ):
            if ( licenseInDb[aKey] != theLicense[aKey] ):
                updatedLicense[aKey] = theLicense[aKey]
            else:
                del updatedLicense[aKey]
            ## end if ( licenseInDb[aKey] != theLicense[aKey] ):
        ## end if ( theLicense.has_key(aKey) == True ):
    ## end for aKey in licenseInDb.keys():
    updatedLicense['auth_code'] = theLicense['auth_code']

    # Pass this to the database routines to do the update.
    result = db_access.set_table_row(_routerAFL, updatedLicense, myWhere)

    return(result)
## end update_license()



#\L

# 11 Apr 2013       update exception to make it consistient amongst modules.
# 31 Mar 2013        initial creation.

############################################################################
# delete_license
# Remove an AFL license from the database.
# Entry: theLicense - the license record to remove from the database.
# Exit: the license has been removed from the database.
# 
def delete_license(theLicense):
    """
    Delete the AFL license row from the network_afl table..
    """

    global _routerAFL

    # Grab the license from the database.
    myWhere = "auth_code = '%s'" % (theLicense['auth_code'],)

    # Does the record exist?
    licenseInDb = db_access.get_row_count(_routerAFL, myWhere)

    if ( licenseInDb == 1 ):
        db_access.delete_table_rows(_routerAFL, myWhere)
    else:
        raise NetException("not found", \
                "auth_code " + theLicense['auth_code'] )

    return()
## end delete_license()



# vim: syntax=python ts=4 sw=4 showmatch et :
