#
g_version=''

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network AFL License table
#
global _routerAFL
_routerAFL = {
    'database': 'networking',   # name of the database
    'role': 'netmon',           # role for database access
    'table': 'router_afl',      # name of the table
    'index': 'auth_code',       # name of index column
    'indexType': 'string',      # data type of index column
}

global _aflModels
_aflModels = {
    'database': 'networking',    # name of the database
    'role': 'netmon',            # role for database access
    'table': 'afl_models',       # name of the table
    'index': 'auth_code',        # name of index column
    'indexType': 'string',       # data type of index column
}

# vim: syntax=python ts=4 sw=4 showmatch et :

