#\

###############################################################################
# Interfaces table
#
_interface = {
	'database': 'intermapper',
	'role': 'intermapper',
	'table': 'interface',
	'index': 'interface_id'
}

