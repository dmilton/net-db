#
g_version='20 Jan 2014 - 2.0.0'

from network_error import NetException

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False


#\

###############################################################################
# Interfaces table
#
_interface = {
	'database': 'intermapper',
	'role': 'intermapper',
	'table': 'interface',
	'index': 'interface_id'
}

#

############################################################################
# get_if_info
# Switch must be FQDN name of the switch to look up.
# ifName or ifIndex must be filled in.

def get_if_info(switch, ifName="", ifIndex=0):
	global imDbInterfaceTable, imDbInterfaceFields, imDbInterfaceKeys

	if ( ifName == "" and ifIndex == 0):
	 		raise NetException("noIfId", "switch=%s" % (switch))

	# Basics of the intermapper database query.
	q1 = "SELECT " + imDbInterfaceFields + " FROM " + imDbInterfaceTable
	q2=" WHERE ifname = '%s' AND device_id = "
	q3=" WHERE ifindex = %s AND device_id = "
	q4=" (SELECT device_id FROM device WHERE dnsname = '%s'" + \
			" AND server_id = 2 LIMIT 1)"
	q5=" LIMIT 1"

	# One of two queries, depending on whether we have ifName or ifIndex.
	if ( ifName > "" ):
		query = (q1 + q2 + q4 + q5) % (ifName, switch)
	else:
		query = (q1 + q3 + q4 + q5) % (str(ifIndex), switch)

	# Now query the database.
	info = PgQuery(query, dbName="intermapper")

	if ( len(info) > 0 ):
		# Turn the database result into a dictionary.
		result = dict(list(zip(imDbInterfaceKeys, info[0])))
	else:
	 		raise NetException("NoInterface", "ifName=%s,ifIndex=%s" 
				% ( ifName, str(ifIndex) ))
	## end else if ( len(info) > 0 ):

	debug.debug(1, "get_if_info: result=%s" % (str(result)))
	return result
## end def get_if_info():


# vim: syntax=python ts=4 sw=4 showmatch et :
