#
g_version='11 Jun 2014 - 3.0.2'

import string

from socket import gethostbyaddr
from os import popen

global pySnmpExists

# Write exception check around netsnmp and use comand line tools if
# the python netsnmp module is not installed.
try:
    from pysnmp.entity.rfc3413.oneliner import cmdgen
    pySnmpExists = True
except ImportError as err:
    print("The package works better when library pysnmp is installed.")
    print("See https://pypi.python.org/packages/source/p/pysnmp/")
    print(str(err))
    pySnmpExists = False
    ##raise

from network_error import NetException


#\L

#  3 Apr 2013        Rewrite to use netsnmp.
# 24 Jan 2013        Add community string as argument.
# 19 Dec 2012        initial creation.

############################################################################
# snmp_get
# Entry:
#       community       the snmp community to use
#       device          the fully qualified host name to query
#       oid             the SNMP OID to obtain.
# Exit:
#       The device has been queried and the value found at the specified
#       OID is returned as the result.

#http://pysnmp.sourceforge.net/examples/current/v3arch/oneliner/
#   manager/cmdgen/get-v2c.html
def snmp_get(community, device, oid):

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
            cmdgen.CommunityData(community),
            cmdgen.UdpTransportTarget((device, 161)),
            oid, lookupNames=False, lookupValues=True,
    )

    # Check for errors and print out results
    if errorIndication:
        raise NetException("SnmpGetFailed", errorIndication)
    else:
        if errorStatus:
            raise NetException("SnmpGetFailed", \
                "snmp get request failed for " + device + " oid: " + oid)
        else:
            result = varBinds[0][1].prettyPrint()

    return(result)
## end def snmp_get():

def snmp_get_old(community, device, oid):

    result = ''

    myCommand = "snmpget -v2c -c" + community + " -ObentU " \
                + device + " " + oid
    p = popen(myCommand,"r")
    while 1:
        line = p.readline()
        if not line: break
        result += line
    ## end while 1:

    if ( not result > '' ):
        raise NetException("SnmpGetFailed", \
                "snmp get request failed for " + device + " oid: " + oid)

    return(result)
## end def snmp_get_old()

def snmp_get_v6():
    # http://pysnmp.sourceforge.net/examples/current/v3arch/oneliner
    # /manager/cmdgen/get-v3-over-ipv6-with-mib-lookup.html
    #from pysnmp.entity.rfc3413.oneliner import cmdgen

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
        cmdgen.UsmUserData('usr-md5-des', 'authkey1', 'privkey1'),
        cmdgen.Udp6TransportTarget(('::1', 161)),
        cmdgen.MibVariable('1.3.6.1.2.1.1.1.0'),
        '1.3.6.1.2.1.1.2.0',
        '1.3.6.1.2.1.1.3.0'
    )

    # Check for errors and print out results
    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1] or '?'
                )
            )
        else:
            for name, val in varBinds:
                print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))

## end def snmp_get_v6():


#\L

#  6 Jun 2014   rewrite to use pysnmp; netsnmp api changed and the output
#               results were vastly different. hopefully pysnmp will be more
#               predictable since it is manually installed rather than part
#               of MacOS where a system update could break our calls.
# 24 Jan 2013   add community string as argument.
# 19 Dec 2012   initial creation.

############################################################################
# snmp_walk
# Entry:
#       community - the SNMP community to use for the query
#       device      - the FQDN of the device to query
#       oid         - the SNMP OID to query.
# Exit:
#       the device has been queried and the results are returned.

def snmp_walk(community, device, oid):
    # http://pysnmp.sourceforge.net/examples/
    #   current/v3arch/oneliner/manager/cmdgen/getnext-v2c.html
    # from pysnmp.entity.rfc3413.oneliner import cmdgen

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
        cmdgen.CommunityData(community),
        cmdgen.UdpTransportTarget((device, 161)),
        oid,
    )

    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            raise NetException("snmp-error", errorStatus.prettyPrint())
        else:
            result = []
            for varBindTableRow in varBindTable:
                mibEntry = {}
                mibEntry['oid'] = varBindTableRow[0][0].prettyPrint()
                mibEntry['value'] = varBindTableRow[0][1].prettyPrint()
                result.append(mibEntry)

    return(result)
## end def snmp_walk():

def snmp_walk_v6():
    # http://pysnmp.sourceforge.net/examples/current/
    # v3arch/oneliner/manager/cmdgen/getnext-v3-over-ipv6-with-mib-lookup.html
    from pysnmp.entity.rfc3413.oneliner import cmdgen

    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorIndex, varBindTable = cmdGen.nextCmd(
        cmdgen.UsmUserData('usr-md5-des', 'authkey1', 'privkey1'),
        cmdgen.Udp6TransportTarget(('::1', 161)),
        cmdgen.MibVariable('IF-MIB', 'ifEntry'),
        lookupNames=True, lookupValues=True
    )

    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            print('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBindTable[-1][int(errorIndex)-1] or '?'
                )
            )
        else:
            for varBindTableRow in varBindTable:
                for name, val in varBindTableRow:
                    print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
## end def snmp_walk_v6():


#\L
# 8 Apr 2013                initial creation.

############################################################################
# get_sys_object_id
def get_sys_object_id(theDevice, snmpComm):

    sysObjectID_OID = '.1.3.6.1.2.1.1.2.0'
    sysObjId = snmp_get(snmpComm, theDevice, sysObjectID_OID)
    result = sysObjId.strip('["]').strip(string.whitespace)

    return(result)
## end get_sys_object_id()



#\L
# 8 Apr 2013        initial creation.

############################################################################
# get_junos_version
def get_junos_version(theDevice, snmpComm):

    codeVerOid = '.1.3.6.1.4.1.2636.3.40.1.4.1.1.1.5.0'
    firmwareVer = snmp_get(snmpComm, theDevice, codeVerOid)
    # ASN format after 'prettyPrint' is "b'12.3R6.6'"
    result = firmwareVer[2:-1]

    return(result)
## end get_junos_version()


#\L
# dd mmm yyyy        initial creation.

############################################################################
# get_junos_chassis_info
def get_junos_chassis_info(theDevice, snmpComm):

    # The Chassis MIB variables we want are:
    # jnxVirtualChassisMemberTable.jnxVirtualChassisMemberEntry.xx
    # Where the xx is:
    #   1 = jnxVirtualChassisMemberId
    #   2 = jnxVirtualChassisMemberSerialnumber - table.
    #       1.3.6.1.4.1.2636.3.40.1.4.1.1.1.2
    #   4 = jnxVirtualChassisMemberMacAddBase - table.
    #       1.3.6.1.4.1.2636.3.40.1.4.1.1.1.4
    #   8 = model
    #       1.3.6.1.4.1.2636.3.40.1.4.1.1.1.8.[vc-id]
    jnxVcInfo = ".1.3.6.1.4.1.2636.3.40.1.4.1.1.1."
    vcDone = False
    serialOid = jnxVcInfo + "2"
    macOid = jnxVcInfo + "4"
    modelOid = jnxVcInfo + "8"

    # Get the serial numbers from the virtual chassis.
    serialNbrs = snmp_walk(snmpComm, theDevice, serialOid)
    # ASN strings are b"xyz" so drop the b' at the start and ' at the end.
    serialList = []
    for aSerial in serialNbrs:
        serialList.append( aSerial['value'][2:-1] )

    macAddrs = snmp_walk(snmpComm, theDevice, macOid)
    macList = []
    for oneMac in macAddrs:
        # ASN mac address is 0x format so drop the 0x
        aMac =  oneMac['value'][2:]
        # Now turn the hex nibbles into colon separated octects.
        macHex = aMac[0:2]
        for i in list( range(2, len(aMac), 2) ):
            macHex += ":" + aMac[i:i+2]
        macList.append(macHex)

    modelNbrs = snmp_walk(snmpComm, theDevice, modelOid)
    modelList = []
    # ASN strings are b'xyz' so drop the b' at the start and ' at the end.
    for oneModel in modelNbrs:
        modelList.append( oneModel['value'][2:-1] )

    result = {}
    result['serial-nbrs'] = serialList
    result['mac-addresses'] = macList
    result['model-nbrs'] = modelList

    return(result)
## end get_junos_chassis_info()



#\L

# 11 Jun 2014   Remove leading period since the pysnmp utilty does not
#               return the OIDs with them.
# 15 Apr 2013   initial creation.

############################################################################
# sys_obj_2_model
def sys_obj_2_model(oid):

    result = 'unknown'

    if ( oid == '1.3.6.1.4.1.9.1.220' ):
        result = 'WS-2924M-XL-EN'
    elif ( oid == '1.3.6.1.4.1.9.1.283' ):
        result = 'WS-C6509-E'
    elif ( oid == '1.3.6.1.4.1.9.1.287' ):
        result = 'WS-C3524-XL-EN-PWR'
    elif ( oid == '1.3.6.1.4.1.9.1.366' ):
        result = 'WS-C3550-24-EMI'
    elif ( oid == '1.3.6.1.4.1.9.1.400' ):
        result = 'WS-C6513'
    elif ( oid == '1.3.6.1.4.1.9.1.431' ):
        result = 'WS-C3550-12G'
    elif ( oid == '1.3.6.1.4.1.9.1.449' ):
        result = 'WS-C6503-E'
    elif ( oid == '1.3.6.1.4.1.9.1.516' ):
        result = 'WS-C3750-24PS-S'
        result = 'WS-C3750G-24T-S'
    elif ( oid == '1.3.6.1.4.1.9.1.563' ):
        result = 'WS-C3560-24PS-S'
    elif ( oid == '1.3.6.1.4.1.9.1.832' ):
        result = 'WS-C6509-V-E'
    elif ( oid == '1.3.6.1.4.1.3224.1.34' ):
        result = 'SSG5-v92-WLAN'
    elif ( oid == '1.3.6.1.4.1.1916.2.28' ):
        result = 'Summit48si'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.29' ):
        result = 'mx240'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.30' ):
        result = 'ex3200'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.31' ):
        result = 'ex4200'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.34' ):
        result = 'srx3600'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.36' ):
        result = 'srx210'
    elif ( oid == '1.3.6.1.4.1.2636.1.1.1.2.59' ):
        result = 'ex8208'

    return(result)
## end sys_obj_2_model()



# vim: syntax=python ts=4 sw=4 showmatch et :
