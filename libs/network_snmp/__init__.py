#
g_version=''

import string

from socket import gethostbyaddr
from os import popen

global pySnmpExists

# Write exception check around netsnmp and use comand line tools if
# the python netsnmp module is not installed.
try:
    from pysnmp.entity.rfc3413.oneliner import cmdgen
    pySnmpExists = True
except ImportError as err:
    print("The package works better when library pysnmp is installed.")
    print("See https://pypi.python.org/packages/source/p/pysnmp/")
    print(str(err))
    pySnmpExists = False
    ##raise

from network_error import NetException

# vim: syntax=python ts=4 sw=4 showmatch et :

