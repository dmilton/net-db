#
g_version=''

# Add stuff here.
import re
import textwrap

## End


#\L

# 16 Apr 2015   initial creation.

############################################################################
# print_building_types
# Entry: bldg_type_list - the list of building type(s) to print.
# Exit: result
# Exceptions:

def print_building_types(bldg_type_list):

    bldg_list_len = len(bldg_type_list)
    if ( bldg_list_len == 0 ):
        print("No building types are defined.")
    elif ( bldg_list_len == 1 ):
        print("Building Type: " + bldg_type_list[0]['bldg_type'])
        print("Description: " + bldg_type_list[0]['bldg_type_description'])
    else:
        bldg_type_title = "Building Types"
        bldg_type_header = ("Building Type", "Description")
        bldg_type_keys = ["bldg_type", "bldg_type_description"]
        print_table(bldg_type_title, bldg_type_header,
                        bldg_type_keys, bldg_type_list)

    return
## end print_building_types()


#\L

# 22 Sep 2014   Add formatted output of the type description. Simplify
#               decision logic regarding type_list as a dictionary or
#               a list of dictionaries.
# 21 Aug 2014   Update fields to print, remove family and tables.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 21 Jan 2014   Update to allow for prefix_size not being present and
#               to display net_family.
# 21 Jun 2013   Allow input of a dictionary or a list.
# 25 Apr 2012   initial creation.

############################################################################
# print_types
#
# Entry:
#       type_list - list of network types to print. If a dict then only
#                one type is expected and dump is implied.
#       dump_flag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_types(type_list, dump_flag=False):

    # If we only have one type, print the entrie thing.
    if ( type(type_list) is dict ):
        print_one_type(type_list, False)
    else:
        # This is a list of either zero or more than one type.
        if ( len(type_list) > 0 ):
            if ( dump_flag == False ):
                # Convert our list of typeInfo dictionaries into a table.
                if ( "type_required" in type_list[0] ):
                    reqd = "type_required"
                elif ( "bldg_required" in type_list[0] ):
                    reqd = "bldg_required"
                elif ( "bldg_type_list" in type_list[0] ):
                    reqd = "bldg_type_list"
                type_keys = ["net_type","assign_priority","base_vlan_tag",
                        "name_extension",reqd,"prefix_size" ]
                type_head = ("Type Name", "Pri", "Vlan", 
                        "Extension", "Required?", "Prefix")
                type_title = "Currently defined network types:"
                print_table(type_title, type_head, type_keys, type_list)
            else:
                key_list = list(type_list[0].keys())
                key_list.sort()
                for a_type in type_list:
                    print_one_type(a_type, False)

## end def print_types():


#\L

# 22 Sep 2014   initial creation.

############################################################################
# print_one_type
#
# Entry:
#       theType - a network type dictionary to print.
#       showBrief - only show the main fields.
# Exit: the type has been printed to the terminal.

def print_one_type(theType, showBrief=False):

    # create a sorted list of the fields:
    keyList = list(theType.keys())
    keyList.sort()

    # Print the type information to the terminal:
    print("\nNetwork Type: " + theType['net_type'])
    print("Required Buildings: ["+ ', '.join(theType['bldg_type_list']) + "]")

    vlanNbr = str(abs(theType['base_vlan_tag']))
    if ( theType['base_vlan_tag'] > 0 ):
        vlanLine = "Starting VLAN ID: " + vlanNbr
    elif ( theType['base_vlan_tag'] < 0 ):
        vlanLine = "Unique VLAN ID: " + vlanNbr
    else:
        vlanLine = "Manual VLAN ID assignment."
    print(vlanLine)

    for aKey in keyList:
        if ( not showBrief  and
                aKey not in ["net_type", "base_vlan_tag",
                    "type_descr", "bldg_type_list"]):
            print(aKey + " = " + str(theType[aKey]))
        elif ( aKey == "type_descr" and len(theType['type_descr']) > 0 ):
            theDescr = "Description: " + theType['type_descr']
            print( textwrap.fill(textwrap.dedent(theDescr), 72) )

## end print_one_type()


#\L

# 11 Apr 2015   Add check for a zone description with no address ranges
#               and print an appropriate message.
# 21 Jun 2013   update for schema changes/field name changes.
# 13 May 2013   added net_type_list column to output.
# 25 Apr 2013   initial creation.

############################################################################
# print_zones
#
# Entry:
#        zone_list - list of network zones to print.
#        dump_flag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_zones(zone_list, dump_flag=False):

    # If we only have one zone, print the entrie thing.
    if ( len(zone_list) == 1 ):
        dump_flag = True

    if ( len(zone_list) > 0 ):
        if ( dump_flag == False ):
            # Split up by the zone ID.
            zones_by_id = {}
            for a_net_block in zone_list:
                if ( a_net_block['net_zone'] not in zones_by_id ):
                    zones_by_id[a_net_block['net_zone']] = []
                zones_by_id[a_net_block['net_zone']].append(a_net_block)

            # Table variables for all zone address blocks.
            zone_keys = ["network_range", "next_loop", "next_link",
                    "next_bldg", "max_net", "net_type_list"]
            zone_head = ("Network", "Loopback", "Link",
                    "Building", "Max Nets", "Types")
            
            # Print the individual network ranges of the zone.
            for a_net_zone_id in list(zones_by_id.keys()):
                zone_info_list = zones_by_id[a_net_zone_id]
                zone_title = "Network Zone: " + str(a_net_zone_id)
                zone_title += ", " + zone_info_list[0]['zone_descr']
                if ( "network_range" not in zone_info_list ):
                    print(zone_title)
                    print("No address ranges in this zone.\n")
                else:
                    print_table(zone_title, zone_head,
                            zone_keys, zone_info_list)
        else:
            keyList = list(zone_list[0].keys())
            keyList.sort()
            for aZone in zone_list:
                # dump all fields in the record.
                print("\nNetwork Zone: " + str(aZone['net_zone']))
                print("\nRange Description: " + str(aZone['range_descr']))
                for aKey in keyList:
                    if ( aKey not in ["net_zone","range_descr"] ):
                        print((aKey + " = " + str(aZone[aKey])))

## end def print_zones():


#\L

# 11 Apr 2014   Add output of the new field campus code.
# 13 Aug 2014   Change to detect input of a single campus_info dictionary
#               and just display a single entry.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 14 May 2013   initial creation.

############################################################################
# print_campus
#
# Entry:
#        campus_info    dictionary of campus information or a list of
#                       campus_info dictionaries.
#
# Exit: the campus information has been printed to the terminal.
#

def print_campus(campus_info):

    if ( type(campus_info) == dict ):
        print("Campus ID: " + str(campus_info['campus_id'])
                + " (" + campus_info['campus_code'] + ")")
        print("Campus Location: " + str(campus_info['campus_location']))
    else:
        # Only three fields in a campus record...
        campusTitle = "Found the following campus locations:"
        campusKeys = ["campus_id", "campus_code", "campus_location"]
        campusHead = ("Campus ID", "Campus Code", "Campus Location")
        print_table(campusTitle, campusHead, campusKeys, campus_info)

## end def print_campus(bldgCode):


#\L

# 25 Mar 2015   Make printing of ipv6 address allocation optional.
# 26 Jan 2015   Remove core_bldg since it isn't referenced anywhere.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 10 Jun 2014   Changed colum name in table from 'name' to 'bldg_name' so
#               added code to handle either. Can remove once the production
#               database has been changed.
# 26 Mar 2014   Change output slightly so the zone is consistient amongst
#               the various commands.
# 23 Jan 2014   VTP Domain could be empty or None. Check for both conditions
#               and don't print if either condition is true.
#               Compute IPv6 allocation address for the building zone and
#               display that as well.
# 22 Jan 2014   ospf_area could be None, check for that instead of >= 0.
# 25 Sep 2013   Change order of output for more reasonable groupings.
# 11 Apr 2013   initial creation.

############################################################################
# print_building
#
# Entry:
#       bldg_info - dictionary of building information.
#       bldg_alloc - base IPv6 network for the building.
#       dump_flag - print(all fields of the record.
# Exit: the building information has been printed to the terminal.

def print_building(bldg_info, bldg_alloc, dump_flag=False):

    if ( dump_flag ):

        # dump all fields.
        key_list = list(bldg_info.keys())
        key_list.sort()
        # dump all fields in the record.
        print(("\nBuilding Code: " + bldg_info['bldg_code']))
        for a_key in key_list:
            if ( a_key != 'bldg_code' and bldg_info[a_key] != None ):
                print((a_key + " = " + str(bldg_info[a_key])))
        ## end for a_key in key_list:
    
    else:

        # Display the basic information for the building.
        print( "Building" + "(" + bldg_info['bldg_code'] + "): " + \
            bldg_info['bldg_name'] )
        if ( bldg_info['bldg_address'] != None ):
            print( "Building Address: " + bldg_info['bldg_address'] )
        if ( bldg_info['bldg_router'] > '' ):
            print( "Building Router: " + bldg_info['bldg_router'] )
        if ( bldg_info['ospf_area'] != None ):
            print( "OSPF Area: " + str(bldg_info['ospf_area']) )
        if ( bldg_info['vtp_domain'] != None ):
            if ( bldg_info['vtp_domain'] > '' ):
                print( "VTP Domain: " + bldg_info['vtp_domain'] )

        # Web and map links for the building.
        if ( bldg_info['web_dir'] != None ):
            print("Web Directory: " + bldg_info['web_dir'] +
                    ", Visibility: " + str(bldg_info['web_visible']) )

        # Deal with campus info.
        if ( bldg_info['campus_id'] > 0 and 'campus_name' in bldg_info ):
            print(("Network Zone: " + str(bldg_info['net_zone']) + \
                ", " + bldg_info['campus_name'] + " Campus" ))
        else:
            print(("Network Zone: " + str(bldg_info['net_zone']) + \
                ", Campus ID: " + str(bldg_info['campus_id']) ))

        # IPv6 Information.
        if ( bldg_info['ipv6_netid'] > 0 ):
            print(( "IPv6 Network ID: " + \
                str(bldg_info['ipv6_netid']) + \
                ", Next Network: " + str(bldg_info['ipv6_nextnet']) ))
            if ( len(str(bldg_alloc)) > 0 ):
                print("IPv6 Allocation Start: " + str(bldg_alloc))

## end def print_building():


#\L

# 12 Dec 2014   initial creation.

############################################################################
# print_vd_rooms
# Entry: vd_room_list - The voice data room list (as returned by get_vd_rooms)
# Exit: the list of voice data rooms has been printed to the terminal.
# Exceptions:

def print_vd_rooms(vd_room_list):

    if ( len(vd_room_list) > 0 ):
        print("Voice Data Rooms:")
        for a_room in vd_room_list:
            room_info = '[id=' + str(a_room['room_id'])
            if a_room['mdr'] == True:
                room_info += ",MDR"
            room_info += "]"
            print(("    " + a_room['room'] + room_info))
    else:
        print("No voice/data rooms defined.")

## end def print_vd_rooms()


#\L

#  4 Jun 2014   Update for new column names which identify table somewhat.
# 20 Feb 2014   Change wording of table output since 'found' is not exactly
#               correct when adding a network.
# 19 Feb 2014   Some entries may not have an assign_by field (None) so
#               check for this and use the generic netmon as eh assigner.
# 30 Jan 2014   Correct documentation.
# 24 Jan 2014   Add display of the network name and new conditional
#               fields (network_table).
# 17 Dec 2013   Some networks cannot be prime. Don't try and disply
#               that information when it doesn't exist.
# 18 Oct 2013   initial creation.

############################################################################
# print_network
#
# Entry:
#        netInfo - dictionary of network information.
# Exit: the building information has been printed to the terminal.

def print_network(netInfo):

    # Display the basic information for the building.
    print("Network: " + str(netInfo['network']) + " vlan ID " + \
            str(netInfo['vlan_tag']) )
    print("Network Name: " + netInfo['net_name'])
    print("Building Code: " + netInfo['bldg_code'] )

    if ( "ep_code" in netInfo ):
        print("Network endpoint code: " + netInfo['ep_code'])

    if ( "remote_code" in netInfo ):
        print("Network remote building: " + netInfo['remote_code'])

    primeState = ""
    if ( "prime_net" in netInfo and netInfo['net_type'] == "user" ):
        if ( netInfo['prime_net'] ):
            primeState = " - prime net in building."
        else:
            primeState = " - non-prime."
    print("Network Type: " + netInfo['net_type'] + primeState)

    if ( 'assign_by' in netInfo ):
        print( "Assigned: " + str(netInfo['assign_date']) \
            + " by " + netInfo['assign_by'] )
    else:
        print( ("Assigned: " + str(netInfo['assign_date']) \
            + " by netmon" ) )

    if ( 'net_zone' in netInfo ):
        print("Network was made free on " + str(netInfo['free_date'])
                + ", net zone " + str(netInfo['net_zone']))

    if ( 'network_table' in netInfo ):
        print("The network is in the " + netInfo['network_table'] + " table.")

## end def print_network():


#\L

# 1 Apr 2013        Added flag and code to dump an entire record.
# 15 Nov 2102        initial creation.

############################################################################
# print_licenses
#
# Entry:
#        licenseList - list of licenses to print.
#        dumpFlag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_licenses(licenseList, dumpFlag=False):

    # If we only have one license, print the entrie thing.
    if ( len(licenseList) == 1 ):
        dumpFlag = True

    if ( len(licenseList) > 0 ):

        if ( dumpFlag == False ):

            # Convert our list of afl license dictionaries into a table.
            keys = ["serial_nbr", "member_id", "auth_code"]
            aflTable = []
            for aLicense in licenseList:
                if ( aLicense['auth_active'] == True ):
                    aflTable += aLicense

            aflHead = ("Serial Number", "Member ID", "Auth Code")
            aflTitle = "Advanced Feature Set Licenses:"
            print_table(aflHead, aflTable)

        else:

            keyList = list(licenseList[0].keys())
            keyList.sort()
            for aLicense in licenseList:
                # dump all fields in the record.
                print("\nAuthorization Code: " + aLicense['auth_code'])
                for aKey in keyList:
                    if ( aKey != 'auth_code' ):
                        print(aKey + " = " + str(aLicense[aKey]))

## end def print_licenses(bldgCode):



#\L

# 23 Mar 2015   initial creation.

############################################################################
# print_one_license
# Entry: aLicense - the license to print.
# Exit: the details of the license have been printed.
# Exceptions:

def print_one_license( aLicense ):

    if ( aLicense == None ):
        print("Unable to find a license with that auth code.")
    else:
        print("Authorization Code Information:")
        print("Auth Code: " + aLicense['auth_code'])
        print("RTU Serial: " + aLicense['rtu_serial'])
        if ( aLicense['router'] != None ):
            print("Router: " + aLicense['router'])
            print("Serial Number: " + str(aLicense['serial_nbr']))
            print("Member ID: " + str(aLicense['member_id']) )
            print("MAC Address: " + str(aLicense['lan_mac']))
        if ( aLicense['auth_active'] == True ):
            print("\nLicense has been activated with key:")
            print(str(aLicense['license_key']))
        else:
            print("License has not been activated yet.")

## end print_one_license()


#\L

#  2 May 2013		initial creation.

############################################################################
# print_column
#
# Entry:
#		columnList - list of network types to print.
# Exit: the list has been printed to the terminal.

def print_column(columnList):

	if ( len(columnList) > 0 ):
		# Convert our list of afl license dictionaries into a table.
		typeKeys = ["table_name","table_column","descr"]
		typeHead = ("Table Name", "Table Column", "Description")
		typeTitle = "Currently defined table column descriptions:"
		for aCol in columnList:
			print(("Table: " + aCol['table_name'] + \
					"\t Column: " + aCol['table_column'] + \
					"\nDesc: " + aCol['column_descr'] + "\n"))
	## end if ( len(columnList) > 0 ):

## end def print_column():


#

# 23 Sep 2013   Add check to test that dictList is not zero length
#               and therefore not try to process the empty list.
# 25 Apr 2013   Virtually all lists now contain dictionaries so added
#               some unwrapping code here so it doesn't have to be
#               duplicated in every script that uses print_table.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# print_table
# Given a table and header print the table for the user.
# Entry:
#       title - string to print as a title for the table
#       header - tuple of column names
#       keyList - list of dictionary keys matching colums for header
#       dictList - list of dictioraries to print in a table.

def print_table(title, header, keyList, dictList):

    # Start by turning the list of dictionaries into a list of lists.
    tableRows = []
    if ( len(dictList) > 0 ):
        for aRow in dictList:
            rowList = []
            for aKey in keyList:
                if ( aRow[ aKey ] == None ):
                    rowList.append( '' )
                else:
                    rowList.append( str(aRow[ aKey ]) )
            tableRows.append(rowList)

    # determine the maximum length of each field.
    fieldCount = len(header)
    # start with the length of the header field.
    fieldSize = []
    for i in range(fieldCount):
        fieldSize.extend([ len(header[i]) ])

    # now update our size with that of the field itself.
    tableSize = len(tableRows)
    for i in range(tableSize):
        for j in range(fieldCount):
            if ( tableRows[i][j] != None):
                fieldSize[j] = max(fieldSize[j], len(str(tableRows[i][j])))

    # create a separator and output format line:
    sepLine = "+"
    formatLine = "|"
    for j in range(fieldCount):
        formatLine = formatLine + " %-" + str(fieldSize[j]) + "s |"
        sepLine = sepLine + "-"
        for i in range(fieldSize[j]):
            sepLine = sepLine + "-"
        sepLine = sepLine + "-|"
    tableSeparator = sepLine[:len(sepLine)-1] + "+"

    # now print out the table.
    print(title)
    print(tableSeparator)
    print((formatLine % header))
    print(tableSeparator)

    for aRecord in tableRows:
        thisRecord = ()
        for i in range(len(aRecord)):
            # note comma at end of next line. This is required to make aField
            # into a Tuple. That permits concatenation of Tuples in the next
            # statement.
            aField = str(aRecord[i]),
            thisRecord = thisRecord + aField
        ## end for i in range(len(anEntry)):
        print((formatLine % thisRecord))
    ## end for anEntry in table:
    print(sepLine)

## end def print_table():


#

# 23 Mar 2015   Change building code handling to allow up to 8 characters.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 18 Aug 2010   Change output format routine so booleans print as
#               zero or one rather than F and T.
# 16 Aug 2010   Add new routine to format output.

##############################################################################
# output_format()
# Entry: the_value - the value to be formatted.
#         the_type - the variable type.

def output_format(the_value, the_type):

    # Process the key value based on the identified type.
    if ( the_type == 'bldgCode' ):
        # building codes can be up to 8 characters
        the_len = len(the_value)
        if ( the_len > 8 ):
            the_len = 8
        result = the_value[0:the_len]
    elif ( the_type == 'boolean' ):
        if ( the_value == True ):
            result = '1'
        else:
            result = '0'
    elif ( the_type == 'campus' ):
        result = str(the_value)
    elif ( the_type == 'date' ):
        result = the_value
    elif ( the_type == 'hostName' ):
        result = check_host_name(the_value)
    elif ( the_type == 'integer' ):
        result = str(the_value)
    elif ( the_type == 'macaddr' ):
        result = check_mac_addr(the_value)
    elif ( the_type == 'sensorModel' ):
        result = check_sensor_model(the_value)
    elif ( the_type == 'string' ):
        result = the_value
    else:
        # We don't know this type.
        raise ValueError('unknown type')
    ## end if ( theKeyInfo['type'] == 'x' ):

    return(result)
## end output_format():


# vim: syntax=python ts=4 sw=4 showmatch et :
