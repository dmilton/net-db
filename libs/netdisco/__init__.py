#
g_version=''

import socket

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

