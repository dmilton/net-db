#
g_version=''

import socket

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False


#\

# 11 Mar 2013   Add brief table descriptions to help complete documentation.
# 22 Jan 2013   Add node_ip table.
# 10 Jan 2013   Initial creation.

###############################################################################
# Network Devices Table (switches and routers)
# This table identifies switch and router device information.

_device = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'device',
    'index': 'ip'
}

###############################################################################
# Table of switch ports by ip of switch.
# This table maps out all of the available switch ports.

_devicePort = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'device_port',
    'index': 'port, ip'
}

###############################################################################
# Network Node IP Table
# This table maps out all of the IP to MAC address (ARP/NDP) relationships.

_nodeIp = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'node_ip',
    'index': 'ip, mac'
}

###############################################################################
# Network Node Table (mac address forwarding table leaves)
# This table maps a MAC address to a switch port name.

_node = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'node',
    'index': 'mac, switch, port'
}


#

# 24 Jan 2013   Add nodeIP table initialization.
# 10 Jan 2013   Initial Creation

##############################################################################
# initialize()
# Perform initialization process for netdisco database. 
# Entry: none. initializes internal variables for use.
# Exit: network database fields are now ready to use.

def initialize():
    """
    initialize the networks table access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _device, _devicePort, _node, _nodeIp
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Table of device information for network switches/routers.
    _device.update(db_access.init_db_table(_device))
    # Table of switch port information by device/switch.
    _devicePort.update(db_access.init_db_table(_devicePort))
    # Table of MAC addresses by associated switch port.
    _node.update(db_access.init_db_table(_node))
    # Table of ARP entries for nodes connected to the network.
    _nodeIp.update(db_access.init_db_table(_nodeIp))

    _tables_initialized = True

## end initialize()


#\L

#  3 Apr 2013   Update logic to create an exception when the
#               community string cannot be found.
# 24 Jan 2013   initial creation.

############################################################################
# get_snmp_comm
def get_snmp_comm(deviceName):
    global _device

    # Do something here.
    myWhere = "name LIKE '" + deviceName + "%'"
    dbResult = db_access.get_table_rows(_device, myWhere)

    if ( len(dbResult) >= 1 ):
        result = dbResult[0]['snmp_comm']
    else:
        raise NetException("NoSnmpCommunity", \
                "Could not determine snmp community for device: " + deviceName)

    return(result)
## end get_snmp_comm()


#

# 18 Mar 2014   Add flag to get only the active entry.
#  6 Mar 2013   allow specification of mac and/or ip address.
# 21 Jan 2013   Initial rewrite from find-ip-info using db_access.

############################################################################
# find_arp_entries:

def find_arp_entries(faMacAddr, faIpAddr, activeOnly=False):

    global _nodeIp

    # Create the where clause for the arp entries we want to find.
    if ( str(faIpAddr) != "" and str(faMacAddr) != "" ):
        myWhere = "ip = '" + str(faIpAddr) + "' AND " + \
            "mac = '" + faMacAddr + "'"
    elif ( str(faIpAddr) != "" and str(faMacAddr) == "" ):
        myWhere = "ip = '" + str(faIpAddr) + "'"
    elif ( str(faIpAddr) == "" and str(faMacAddr) != "" ):
        myWhere = "mac = '" + faMacAddr + "'"
    else:
        raise ValueError('no ip or mac addresses to find')

    if ( activeOnly ):
        myWhere += " AND active = 'T'"

    myOrder = "time_last, active"

    # Now get the information from the database.
    result = db_access.get_table_rows(_nodeIp, myWhere, myOrder)

    return result
## end find_arp_entries()


#\L

# 18 Mar 2014    Change brief output to be a single tab delimited line.
#  1 May 2013    initial creation.

############################################################################
# print_arp_entry
# Print an arp entry for the user.
# Entry:    oneArpEntry - dictionary of values for the arp entry.
#            briefFlag - single line (brief) or multi-line (not brief) output.

def print_arp_entry(oneArpEntry, briefFlag):
    if ( oneArpEntry['active'] == True ):
        arpState = 'active'
    else:
        arpState = 'inactive'
    if ( briefFlag == True ):
        print( str(oneArpEntry['ip']) + '\t' + oneArpEntry['mac'] + '\t' + \
            str(oneArpEntry['time_last']) +'\t' + arpState )
    else:
        print("The IP Address " + str(oneArpEntry['ip']))
        print("   was recorded from MAC address " + oneArpEntry['mac'])
        print("   and last seen " + str(oneArpEntry['time_last']))
        print("   The last known state was " + arpState)

## end def print_arp_entry():


#

# 21 Jan 2013   Initial creation from find-ip-info.

############################################################################
# find_node_entries:
# Given search criteria (lowMac, highMac, and active) find entries in the
# netdisco node table for that mac or range of mac addresses. 
# Input:
#        lowMacAddr - the lower MAC address to find the switch ports for.
#        highMacAddr - the upper MAC address to find the switch ports for.
#        activeOnly - only find the active switch port.
# Output: list of dictionaries describing the switch ports.

def find_node_entries(lowMacAddr, highMacAddr, activeOnly):

    # Node table contains the switch port information gleaned from the
    # MAC address forwarding tables.
    global _node

    # Construct our where clause.
    if ( lowMacAddr == highMacAddr ):
        # only looking for one MAC address, only find one switch port.
        myWhere = "mac = '" + lowMacAddr + "'"
        myOrder = 'time_last'
    else:
        myWhere = "mac >= '" + lowMacAddr + "'"
        myWhere += " and mac <= '" + highMacAddr + "'"
        myOrder = 'time_last, active'
    ## end else if ( lowMacAddr == highMacAddr ):

    if ( activeOnly == True ):
        myWhere += " and active = 'T'"
    ## end if ( activeOnly == True ):

    # Get the results of our query.
    result = db_access.get_table_rows(_node, myWhere, myOrder)

    return result
## end def find_node_entries():


#

#  7 Mar 2013   Initial creation.

############################################################################
# find_port_info:
# Given a switch ip and port name, get the rest of the details for the port.
# Input:
#        switchIp - ip address of the switch.
#        portName - name of the interface.
# Output: dictionary describing the switch port.

def find_port_info(switchIp, portName):

    # Node table contains the switch port information gleaned from the
    # MAC address forwarding tables.
    global _devicePort

    myWhere = "ip = '" + str(switchIp) + "'"
    myWhere += " and port = '" + portName + "'"
    result = db_access.get_table_rows(_devicePort, myWhere, '', 1)
    if ( len(result) == 1 ):
        result = result[0]
    else:
        raise ValueError("switch: " + str(switchIp) + \
                ", port: " + portName + " not found")

    return result
## end def find_port_info():


#\L

# 11 Jun 2014   Make search use lower case to force a case insensitive
#               search of the table.
# 15 Apr 2013   devIpOrNet is an IPNetwork object, convert to string
#               for the sql where clause.
# 29 Jan 2013   initial creation.

############################################################################
# find_room_ports
# Entry:
#        devIpOrNet - the CIDR representation of the management network
#                    for the building in question. This could also be the
#                    loopback address of a router.
#        roomAddr - the communications outlet or room number.
# Exit:        portList - a list of ports that match the roomAddr criteria.
#

def find_room_ports(devIpOrNet, roomAddr):

    global _devicePort

    myWhere = "ip <<= '" + str(devIpOrNet) + "' AND " + \
            "LOWER(name) LIKE LOWER('%" + roomAddr + "%')"
    result = db_access.get_table_rows(_devicePort, myWhere)

    return(result)
## end find_room_ports()


#

# 18 Mar 2014   Change single line output format.
# 11 Mar 2013   remove explicit references to netdisco module since
#               this is now part of that module. Change so the MAC
#               address is displayed but only the first time it appears.
#               Juniper switches report MAC addresses on sub interface .0
#               so we need to chop that off to get descriptions.
# 11 Mar 2013   initial creation from find-ip-info

############################################################################
# print_switch_ports
# Given a list of switch ports, print them out for the user.
# Entry:    portList - list of ports.
#            briefFlag - single (brief) or multi-line (not brief) output.

def print_switch_ports(portList, briefFlag):

    lastMac = ""
    thisMac = ""
    for aPort in portList:
        # prepare check against previous MAC so see if we need to display it.
        lastMac = thisMac
        thisMac = aPort['mac']
        # switchName =  dns lookup.
        try:
            switchName = socket.gethostbyaddr(
                    str(aPort['switch']))[0].split('.')[0]
        except socket.herror as reason:
            switchName = str(aPort['switch']) + " (no reverse DNS!)"
        # Juniper reports interface as unit .0 so we need to chop it off.
        aPort['port'] = aPort['port'].split('.')[0]
        try:
            portDetail = find_port_info(aPort['switch'], aPort['port'])
        except ValueError as reason:
            print(reason)
            portDetail = {}
            portDetail['name'] = 'N/A'
        ## end try:
        message = ""
        # display info for user.
        if ( thisMac != lastMac ):
            if ( briefFlag == True ):
                message = aPort['mac']
                if ( len(portList) == 1 ):
                    message += "\t"
                else:
                    message += "\n"
            else:
                message = "The MAC address " + aPort['mac'] + "\n"
            ## end else if ( briefFlag == True ):
        ## end if ( thisMac != lastMac ):
        if ( briefFlag == True ):
            message += portDetail['name'] + "\t"
            message += switchName + " " + aPort['port'] + "\t"
            message += " currently " + portDetail['up']
        else:
            message += "was seen connected to " + switchName + " "
            message += "interface " + aPort['port'] + "\n"
            message += "desc " + portDetail['name'] + " "
            if ( 'up' in portDetail ):
                message += "state is " + portDetail['up']
        ## end else if ( briefFlag == True ):
        print(message)
    ## end for aPort in portList:

## end def print_switch_ports():


# vim: syntax=python ts=4 sw=4 showmatch et :
