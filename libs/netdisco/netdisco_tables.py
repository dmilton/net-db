#\

# 11 Mar 2013   Add brief table descriptions to help complete documentation.
# 22 Jan 2013   Add node_ip table.
# 10 Jan 2013   Initial creation.

###############################################################################
# Network Devices Table (switches and routers)
# This table identifies switch and router device information.

_device = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'device',
    'index': 'ip'
}

###############################################################################
# Table of switch ports by ip of switch.
# This table maps out all of the available switch ports.

_devicePort = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'device_port',
    'index': 'port, ip'
}

###############################################################################
# Network Node IP Table
# This table maps out all of the IP to MAC address (ARP/NDP) relationships.

_nodeIp = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'node_ip',
    'index': 'ip, mac'
}

###############################################################################
# Network Node Table (mac address forwarding table leaves)
# This table maps a MAC address to a switch port name.

_node = {
    'database': 'netdisco',
    'role': 'netview',
    'table': 'node',
    'index': 'mac, switch, port'
}

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

