#

g_version=''

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

#

# 3 Jan 2013        Remove duplicated error handler which is called
#                    by most recovery routines to output a reasonable
#                    message to the end user. The routine was being
#                    duplicated as it resides in a separate file.
# 2 Sep 2010        Initial creation based on original code written
#                    in the network.py module.

#^L
############################################################################
# Networking Error class.

class NetException(Exception):
    """Class for all exceptions raised in this module."""

    def __init__(self, name, detail):
        self.name = name
        self.detail = detail
        return
    ## end def __init__(self):

    def __str__(self):
        return repr(self.detail)
    ## end def __str__(self):

## end class NetException():


#
import signal
import time
 
#def test_request(arg=None):
#    """Your request."""
#    time.sleep(2)
#    return arg
 
class Timeout():
    """Timeout class using ALARM signal."""
    class Timeout(Exception):
        pass
 
    def __init__(self, sec):
        self.sec = sec
 
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)
 
    def __exit__(self, *args):
        signal.alarm(0)    # disable alarm
 
    def raise_timeout(self, *args):
        raise Timeout.Timeout()
 
#def main():
#    # Run block of code with timeouts
#    try:
#        with Timeout(3):
#            print(test_request("Request 1"))
#
#        with Timeout(1):
#            print(test_request("Request 2"))
#    except Timeout.Timeout:
#            print("Timeout")
# 
##############################################################################
# 
#if __name__ == "__main__":
#    main()
#
#

#  9 Sep 2014   Change network type exception to match new codes.
# 19 Jun 2013   Add message for invalid endpoint codes.
#  3 Jan 2013   Add new error for bldg-info failed search.
#  2 Sep 2010   Add handling of snmp timeout errors.

############################################################################
# Networking Error handler.

def net_error_handler(theError):
    result = 0

    if ( theError.name == "NoNetwork" ):
        print("This network is not defined or active at the U of M.")
        result = 1
    elif ( theError.name == "NoNetworksDefined" ):
        print("No IP networks have been allocated to this building.")
        result = 2
    elif ( theError.name == "UnknownFreeCode" ):
        print("I don't know how to allocate networks for this location.")
        result = 3
    elif ( theError.name == "UnknownBuildingCode" ):
        print(theError.detail)
        result = 4
    elif ( theError.name == "MultipleNetworks" ):
        print("Unknown or invalid building code.")
        result = 5
    elif ( theError.name == "NoVlan" ):
        bldgCode = theError.detail[0]
        vlanID = theError.detail[1]
        print("A vlan of ID " + str(vlanID) + " was not found for" + \
            "the building code '" + bldgCode + "'")
        result = 6
    elif ( theError.name == "NoType" ):
        bldgCode = theError.detail[0]
        theType = theError.detail[1]
        print("A vlan of type " + str(theType) + " was not found for" + \
            "the building code '" + bldgCode + "'")
        result = 7
    elif ( theError.name == "NoCode" ):
        print("A vlan of code " + str(theError.detail) + " was not found.")
        result = 8
    elif ( theError.name == "NoName" ):
        print("A network of with the name " + str(theError.detail) +  \
            " was not found.")
        result = 9
    elif ( theError.name == "NoUserNetwork" ):
        print("A user network has not been defined for this building.")
        print("A user data network must be defined before proceeding.")
        result = 10
    elif ( theError.name == "UnknownLocation" ):
        print("The location " + theError.detail + " is unknown so private")
        print("networks cannot currently be assigned.")
        result = 11
    elif ( theError.name == "NetTypeNotFound" ):
        print(theError.detail)
        result = 11
    elif ( theError.name == "NoUplinks" ):
        print("Could not find uplinks for building code: " + theError.detail)
        result = 12
    elif ( theError.name == "noIfId" ):
        print("No interface name or ifIndex was provided to find.")
        result = 13
    elif ( theError.name == "NoInterface" ):
        print("The interface with details: " + theError.detail + \
                "could not be found in the InterMapper database.")
        result = 14
    elif ( theError.name == "InvalidIP" ):
        print("The IP address " + theError.detail + \
                "does not appear to be valid.")
        result = 15
    elif ( theError.name == 'SnmpError' ):
        print("An SNMP error occurred: " + theError.detail)
        result = 16
    elif ( theError.name == "NoSuchInterface" ):
        print("The requested interface could not be found.")
        result = 17
    elif ( theError.name == "UnknownBuilding" ):
        print("A building with a name or part of the name could not be found.")
        result = 18
    elif ( theError.name == "NetworkExists" ):
        print("The network " + theError.detail + " already exists.")
        result = 19
    elif ( theError.name == "NameExists" ):
        print("The network named " + theError.detail + " already exists.")
        result = 20
    elif ( theError.name == "NetNotFound" ):
        print("That network was not found in the database.")
        result = 21
    elif ( theError.name == "EmptyNetwork" ):
        # this is a bug: dump something and raise the exception.
        print("The network passed was empty or zero length.")
        print("This should not happen!")
        result = 22
    elif ( theError.name == "EpNotFound" ):
        print("Endpoints must be in the core and used in priority order.")
        print("Try swapping the building code for the endpoint code.")
        result = 23
    else:
        print(str(theError))
        result = 24
    ## end if ( error.name = X ):

    return result
## end net_error_handler():


# vim: syntax=python ts=4 sw=4 showmatch et :
