#\

# 10 Apr 2013        Initial creation.

###############################################################################
# wireless locations table
#
_wirelessLocations = {}
_wirelessLocations = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'wireless_locations',
    'index': 'id'
}


# vim: syntax=python ts=4 sw=4 showmatch et :

