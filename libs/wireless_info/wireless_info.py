#
g_version='10 Apr 2015 - b259f84'

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False


#\

# 10 Apr 2013        Initial creation.

###############################################################################
# wireless locations table
#
_wirelessLocations = {}
_wirelessLocations = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'wireless_locations',
    'index': 'id'
}



#\L

# dd mmm yyyy   initial creation.

############################################################################
# new_fcn
# Entry: x - something
# Exit: result
# Exceptions:

def new_fcn(x):

    # Do something here.

    return
    #return(result)
## end new_fcn()


# vim: syntax=python ts=4 sw=4 showmatch et :
