#
g_version=''

import ipaddress

from network_error import NetException
import db_access

# need network_types.get_prefix_size
from network_types import get_prefix_size

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network Description Table
# This table contains a description of the zones defined and what/where
# they should be used.

_zone_descriptions = {
    'database': 'networking',
    'table': 'zone_descriptions',
    'role': 'netmon',
    'index': 'net_zone'
}

###############################################################################
# Network Zones Table
# This table contains large network blocks that are assigned to specific
# zones of the campus.

_zone_ranges = {
    'database': 'networking',
    'table': 'zone_ranges',
    'role': 'netmon',
    'index': 'network_range'
}


#

# 28 Aug 2014   Table name changed and now have additional table
#               so added check for the name change.
# 25 Apr 2013   initial creation

##############################################################################
# initialize()
# Perform initialization process for network_zones table.
# Entry: none. initializes internal variables for use.
# Exit: network database fields are now ready to use.

def initialize():
    """
    initialize the network zone access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _zone_descriptions, _zone_ranges
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Table of zone descriptions.
    _zone_descriptions.update(db_access.init_db_table(_zone_descriptions))

    # Table of network address blocks assigned to specific zones.
    _zone_ranges.update(db_access.init_db_table(_zone_ranges))

    _tables_initialized = True

## end initialize()


#\L

# 15 May 2014   initial creation.

############################################################################
# new_zone
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        zone description entry in the database.

def new_zone():
    global _zone_descriptions

    result = db_access.new_table_row(_zone_descriptions)

    return(result)
## end new_zone()


#

# 18 Sep 2014   db routine returns a list but this function should
#               only return a singe description so dereference it on return.
#  2 May 2014   initial writing.

############################################################################
# add_zone
# Add the new zone to the database.
# Entry: the_description - description of the zone to add.
# Exit: A new entry has been added to the database.

def add_zone(the_description):
    """
    # net_zone is a sequence so only the description is required.
    the_description = "default zone when nothing specific is available."
    add_zone(the_description)
    """

    global _zone_descriptions

    # Create a zone the db routines.
    new_zone = {}
    new_zone['zone_descr'] = the_description

    # Insert the new network into the table.
    try:
        db_result = db_access.set_table_row(_zone_descriptions, new_zone)
    except:
        raise NetException("DuplicateZone", \
            "this zone already exists in the database.")

    return(db_result[0])
## end def add_zone():


#\L

# 15 May 2014   initial creation.

############################################################################
# update_zone_desc
def update_zone_desc(zone_info):
    global _zone_descriptions

    # Update an existing network with new values.
    my_where = "net_zone = " + str(zone_info['net_zone'])
    db_result = db_access.set_table_row(_zone_descriptions, zone_info, my_where)

    return(db_result[0])
## end update_zone_desc()


#

# 15 May 2014   initial creation.

############################################################################
# delete_zone
# Given a zone dictionary, delete the zone from the database.
# This will remove the zone description, all address spaces in that zone.
# Ideally, all buildings within the zone should be deleted as well but
# this introduces a circular dependency between buildings and zones.
#
# Entry:
#       net_zone - the zone id to delete.
# Exit: the zone description and all address spaces in that zone have
#       been deleted from the database.

def delete_zone(net_zone):
    global _zone_descriptions, _zone_ranges

    # Where clause for the net zone id.
    my_whene = "net_zone = '" + str(net_zone) + "'"

    # Delete for the zone address spaces.
    db_access.delete_table_rows(_zone_ranges, my_whene)

    # Delete for the zone description.
    db_access.delete_table_rows(_zone_descriptions, my_whene)

    return
## end def delete_zone():


#\L

# 28 May 2014   initial creation.

############################################################################
# renumber_zone

def renumber_zone(zone_info, new_net_zone):
    global _zone_descriptions, _zone_ranges

    # Update the net_zone field.
    # The network_free table and network_zones table are both
    # using a foreign key based on the zone_descriptions net_zone value,
    # The update here will trigger a cascade update of the other tables.
    result = db_access.change_table_rows(_zone_descriptions,
            "net_zone", zone_info['net_zone'], new_net_zone, False)

    return(result)
## end renumber_zone_desc()


#\L

# 26 Sep 2014   initial creation.

############################################################################
# get_zone_list
#
# Entry:
#       zone_id - the ID of the zone to fetch description for.
# Exit:
#       the result is an address range which can be used to assign
#       networks of the requested type from.

def get_zone_list(zone_id):
    global _zone_descriptions

    # Order is always the same.
    my_order = "net_zone"
    if ( zone_id > 0 ):
        my_where = "net_zone = " + str(zone_id)
    else:
        my_where = ""

    # Get the descriptions.
    result = db_access.get_table_rows(_zone_descriptions, my_where, my_order)

    # If we still don't have a result, return an error.
    if ( len(result) == 0 ):
        raise NetException("NoZone", "No zones found.")

    return(result)
## end get_zone_list()


#\L

# 19 Sep 2014   initial creation.

############################################################################
# get_zone_detail
# Entry: net_zone - The network zone to return details for.
# Exit: the zone description and any defined ranges are returned.
# Exceptions:

def get_zone_detail(net_zone):
    global _zone_descriptions, _zone_ranges

    result = []

    if ( net_zone >= 0 ):
        # return specified zone information:
        my_where = "net_zone = " + str(net_zone)
    else:
        # return all zones.
        my_where = ""
    my_order = "net_zone"

    zone_desc_list = db_access.get_table_rows(
            _zone_descriptions, my_where, my_order)

    for a_zone_desc in zone_desc_list:
        zone_detail = { 'net_zone': a_zone_desc['net_zone'] }
        zone_detail = { 'zone_descr': a_zone_desc['zone_descr'] }
        zone_detail = { 'ranges': [] }
        my_where = "net_zone = " + str(a_zone_desc['net_zone'])
        zone_range_list = db_access.get_table_rows(
                _zone_ranges, my_where, my_order)
        for a_range in zone_range_list:
            zone_detail['ranges'].append(a_range)
        result.append(zone_detail)

    return(result)
## end get_zone_detail()


#\L

# 14 Aug 2014   Improve documentation.
# 15 May 2014   Add description field to the output.
# 24 Oct 2013   When zone_id = 0, the id was not being added to the query.
#               This resulted in all zones being returned rather than just
#               the default assignment zone.
#               Renamed the where creation to start with underscore.
# 10 Jul 2013   initial creation.

############################################################################
# get_range_info
#
# Entry:
#       zone_id - the ID of the zone to fetch network ranges for.
#       type_name - optional, when specified find network range for
#               the specified type.
#       ip_version - when specified find the network range of the
#               specified ip version - 4 or 6.
# Exit:
#       the result is an address range which can be used to assign
#       networks of the requested type from.

def get_range_info(zone_id, type_name = '', ip_version=0):
    global _zone_descriptions, _zone_ranges

    result = []

    # Order is always the same; IPv6 first.
    my_order = "net_zone, family(network_range) desc, network_range"

    # Start with the most specific query.
    my_where = _build_range_where(zone_id, type_name, ip_version)
    db_result = db_access.get_table_rows(_zone_ranges, my_where, my_order)

    if ( len(db_result) == 0 ):
        # the result is empty, is there an 'any type' entry?
        my_where = _build_range_where(zone_id, '', ip_version)
        db_result = db_access.get_table_rows(_zone_ranges, my_where, my_order)

    if ( len(db_result) == 0 ):
        # the result is still empty, use the default zone 0.
        my_where = _build_range_where(0, '', ip_version)
        db_result = db_access.get_table_rows(_zone_ranges, my_where, my_order)

    # If we still don't have a result, return an error.
    if ( len(db_result) == 0 ):
        raise ValueError("unable to address range for the request.")

    # We have at least one address range. Add the description to each one.
    for aZoneNet in db_result:
        my_where = 'net_zone = ' + str(aZoneNet['net_zone'])
        try:
            zoneDesc = db_access.get_table_rows(_zone_descriptions, my_where)[0]
            aZoneNet['zone_descr'] = zoneDesc['zone_descr']
        except:
            raise ValueError("The database zone descriptions are corrupt.")
        result.append(aZoneNet)

    return(result)
## end get_range_info()

############################################################################
# _build_range_where
def _build_range_where(zone_id, type_name, ip_version):

    # Build a where clause that may contain any
    # of three choices - net_zone, ip_version, or net_type.
    if ( zone_id >= 0 ):
        zone_where = "net_zone = " + str(zone_id)
    if ( ip_version in [4, 6] ):
        ver_where = "family(network_range) = " + str(ip_version)
    if ( type_name > '' ):
        type_where = "net_type_list LIKE '%" + type_name + "%'"

    if ( zone_id >= 0 ):
        # have zone
        if ( ip_version in [4, 6] ):
            if ( type_name > '' ):
                # have all three.
                my_where = zone_where + " AND " + ver_where + " AND " + type_where
            else:
                # have zone and ip version
                my_where = zone_where + " AND " + ver_where
        elif ( type_name > '' ):
            # have zone and net type, not version.
            my_where = zone_where + " AND " + type_where
        else:
            # have only zone.
            my_where = zone_where
    else:
        # do not have zone.
        if ( ip_version in [4, 6] ):
            if ( type_name > '' ):
                # have version and type.
                my_where = ver_where + " AND " + type_where
            else:
                # have only ip version
                my_where = ver_where
        elif ( type_name > '' ):
            # no zone, no version, have net type
            my_where = type_where
        else:
            # no zone, no version, no net type.
            my_where = ''

    return my_where
## end _build_range_where()


#\L

# 14 Aug 2014   No longer returns a list since with the zone description
#               table there should only ever be one match.
# 20 May 2013   initial creation.

############################################################################
# get_zone_desc
# Entry: the_zone_id - the zone id to get the description for.

def get_zone_desc(the_zone_id):
    global _zone_descriptions

    my_where = "net_zone = " + str(the_zone_id)
    zoneInfo = db_access.get_table_rows(_zone_descriptions, my_where)
    if ( len(zoneInfo) == 0 ):
        raise NetException("NoZone",
            "the zone id " + str(the_zone_id) + " has not been defined yet.")
    
    return(zoneInfo[0])
## end get_zone_desc()


#\L

# 25 Apr 2013        initial creation.

############################################################################
# get_zone_desc_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_zone_desc_fields():
    global _zone_descriptions

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_zone_descriptions)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_zone_desc_fields()


#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_range
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        zone entry in the database.

def new_range():
    global _zone_ranges

    result = db_access.new_table_row(_zone_ranges)

    return(result)
## end new_range()


#

# 20 May 2014   Check for presence of the provided net_zone in the
#               zone descriptions table.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 25 Apr 2013   initial writing.

############################################################################
# add_range
# Add the new zone to the database.
# Entry: new_zone - the dictionary of the zone to add to the database.

def add_range(new_zone):
    """
    # Start with an empty template
    new_zone = new_template()
    # Update the fields you want to change.
    new_zone['network_range'] = ipaddr.IPNetwork("192.0.2.0/24")
    # optionally set the network zone. the default is zero.
    new_zone['net_zone'] = 777
    # Subnet 0 is where all links and loopback addresses are assigned from.
    # The whole thing is broken into /112 chunks where the '0' chunk is for
    # loopback addresses.
    new_zone['next_loop'] = 1
    # link addresses are allocated starting at 2 just to be even.
    new_zone['next_link'] = 2
    # Under normal circumstances, these fields should start with the defaults.
    del new_zone['next_bldg']
    del new_zone['next_link']
    del new_zone['next_loop']
    del new_zone['max_net']
    # 
    add_range(new_zone)
    """
    global _zone_descriptions, _zone_ranges

    # If we have a net_zone, make sure it is valid.
    if ( 'net_zone' in new_zone ):
        my_where = 'net_zone = ' + str(new_zone['net_zone'])
        result = db_access.get_row_count(_zone_descriptions, my_where)
        if ( result == 0 ):
            raise NetException("NoZone",
                "the zone id " + str(new_zone['net_zone']) + 
                " has not been defined yet.")

    # Insert the new network into the table.
    try:
        result = db_access.set_table_row(_zone_ranges, new_zone)
    except:
        raise NetException("DuplicateZone", \
            "this range already exists in the database.")

    return(result)
## end def add_range():


#\L

#  6 Nov 2014   Field name change from network to network_range.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
# 18 Apr 2013   initial creation.

############################################################################
# update_range
def update_range(range_info):
    global _zone_ranges

    # Update an existing network with new values.
    my_where = "network_range = '" + str(range_info['network_range']) + "'"
    result = db_access.set_table_row(_zone_ranges, range_info, my_where)

    return(result)
## end update_range()


#

# 25 Apr 2013        initial creation.

############################################################################
# delete_range
# Given a zone dictionary, delete the zone from the database.
# Entry:
#        the_range - the zone dictionary to delete.
# Exit: the zone has been deleted from the database.

# 4 Feb 2012    initial creation

def delete_range(the_range):
    global _zone_ranges

    myWhere = "network_range = '" + str(the_range['network_range']) + "'"
    db_access.delete_table_rows(_zone_ranges, myWhere)

    return()
## end def delete_range():


#\L

# 25 Apr 2013        initial creation.

############################################################################
# get_range_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_range_fields():
    global _zone_ranges

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_zone_ranges)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_range_fields()


#\L

# 22 Aug 2014   column name in the range has changed.
# 15 Nov 2013   initial creation.

############################################################################
# find_user_range
# Get the network zone/address space from which user addresses are
# to be taken from.

def find_user_range(zone_id, ip_version):
    global _zone_ranges

    # Order is always the same.
    my_order = "network_range"

    # 
    my_where = "family(network_range) = " + str(ip_version)
    my_where += " AND (net_type_list = '' OR net_type_list LIKE '%user%')"
    my_where1 = my_where + " AND net_zone = " + str(zone_id)
    my_where2 = my_where + " AND net_zone = 0"

    # Start by looking for a zone specific address space.
    db_result = db_access.get_table_rows(_zone_ranges, my_where1, my_order)

    # If we didn't find one, look for one in zone 0, the default zone.
    if ( len(db_result) == 0 ):
        db_result = db_access.get_table_rows(_zone_ranges, my_where2, my_order)

    # If we still don't have a result, return an error.
    if ( len(db_result) == 0 ):
        raise ValueError("unable to find zone address space for the request.")
    else:
        result = db_result[0]

    return(result)
## end find_user_range()


#\L

# 26 Sep 2014   Networks have become network_ranges to separte the
#               zone descriptions from the address spaces.
# 10 Jul 2013   update to return the entire zone record instead
#               of just the zone id.
# 14 Feb 2013   initial creation.

############################################################################
# find_range
# Entry: the_network - the network to determine the zone for.

def find_range(the_network):
    global _zone_ranges

    my_where = "network_range >>= '" + str(the_network) + "'"
    result = db_access.get_table_rows(_zone_ranges, my_where)
    
    return(result)
## end find_range()


#\L

# 11 Feb 2015   Changed the result from a network record to an
#               ipaddress.ip_network class.
# 26 Jan 2015   Improve error diagnostics.
#  6 Nov 2014   initial creation

############################################################################
# get_range_net
# Given building and type information, compute a range net based on
# the address range for the type.
#
# Entry:
#       bldg_info - the building to get a network for.
#       type_info - the type of network to create
#       ip_ver - the IP version to create a network for
#       net_index - the index of the network to create.
# Exit:
#       A network has been computed and and returned as result.
#    

def get_range_net(bldg_info, type_info, ip_ver, net_index):

    # Get the address range to use for this type and zone.
    zone_range = get_range_info(bldg_info['net_zone'], 
            type_info['net_type'], ip_ver)

    if ( len(zone_range) == 0 ):
        raise NetException("NoRange", "No IPv" + str(ip_ver) 
            + " address range was found for type '" + type_info['net_type'] 
            + "' in zone " + str(bldg_info['net_zone']) + ".")
    elif ( len(zone_range) > 1 ):
        raise NetException("TooManyRanges", "More than one IPv" + str(ip_ver)
            + "address range was found for type '" + type_info['net_type']  
            + "' in zone " + str(bldg_info['net_zone']) + ".") 
    else:
        zone_range = zone_range[0]

    result = calc_range_net(zone_range['network_range'],
            #network_types.get_prefix_size(type_info, ip_ver),
            get_prefix_size(type_info, ip_ver),
            bldg_info['ipv6_netid'], net_index)

    return(result)
## end get_range_net()


#\L

# 21 Aug 2014   initial creation.

############################################################################
# calc_range_net
#
# Determine a network using the 'late mask' method. This is a method where
# the routing mask which can be applied to a building is a fixed determination
# only when the entire address space has been exhausted.
# 
# Entry: 
#       base_net - the base address space to compute an address for.
#       prefix_len - the size of network to split base_net into.
#       bldg_index - the index of the building.
#       net_index - the index of the subnet within the building.
#
# Exit:
#
#
def calc_range_net(base_net, prefix_len, bldg_index, net_index):
    
    if ( base_net.version == 6 ):
        # prefixlen for IPv6 is always /64.
        bits_of_net = prefix_len - base_net.prefixlen
        if ( bits_of_net % 4 != 0 ):
            raise ValueError(str(base_net) +
                 " is not masked on a nibble boundary.")
        # Convert the building index into binary:    bin(ipNetId)
        # drop the 0b prefixed on the binary:       [2:]
        # zero pad on the left to a length of bits_of_net:   .zfill(bits_of_net)
        # reverse the order of the bits:            [::-1]
        # convert the reversed binary number to an integer: int(n)
        # add index of next building network:        + ipNextNet
        # convert the result to hex:                hex( i )
        # drop the 0x prefixed on the hex:          [2:]
        hex_net = hex(int(bin(bldg_index)[2:].zfill(bits_of_net)[::-1],2) \
            + net_index)[2:]
        # Convert to a long hex string:
        addr_blocks = base_net.network_address.exploded.split(':')
        base_hex = ""
        for a_block in addr_blocks:
            base_hex += a_block
        # Take the assigned prefix portion and add our network portion
        base_alloc = base_hex[0:int(base_net.prefixlen/4)] + hex_net
        # Fill up to max with zero.
        net_as_hex = base_alloc + '0'.zfill(32-len(base_alloc))
        # Insert colons to make it look like an IPv6 address again.
        start_index = 0
        ip_addr_str = net_as_hex[0:4]
        for index in list(range(4,32,4)):
            ip_addr_str = ip_addr_str + ":" + net_as_hex[index:index+4]
        # Add the CIDR prefix
        ip_addr_str += "/" + str(prefix_len)
        # Make it back into an ipaddress object.
        result = ipaddress.ip_network(ip_addr_str)
    else:
        bits_of_net = prefix_len - base_net.prefixlen
        if ( bldg_index > (bits_of_net ** 2) ):
            raise ValueError(str(base_net) + " is exhausted.")

        # Convert our base_net into binary:
        addr_bytes = str(base_net.network_address).split('.')
        bin_net = ""
        for a_byte in addr_bytes:
            bin_net = bin_net + bin(int(a_byte))[2:].zfill(8)
        base_bits = bin_net[0:base_net.prefixlen]
        # Convert the building index into binary:           bin(bldg_index)
        # drop the 0b prefixed on the binary:               [2:]
        # zero pad on the left to a length of bits_of_net:   .zfill(bits_of_net)
        # reverse the order of the bits:                    [::-1]
        # convert the reversed binary number to an integer: int(n)
        # add index of next network in the building:        + ipNextNet
        net_bits = \
            int(bin(bldg_index)[2:].zfill(bits_of_net)[::-1],2) + net_index
        # Form our new subnet in binary:
        net_addr = base_bits + bin(net_bits)[2:]
        net_addr += '0'.zfill(32-len(network_bits))
        # Now make it look like an IP network
        net_addr_str = str(ipaddress.ip_address(int(net_addr,2))) + \
                "/" + str(prefix_len)
        result = ipaddress.ip_network(net_addr_str)

    return(result)
## end calc_range_net()


#\L

# 11 Feb 2015   Changed the result from a network record to an
#               ipaddress.ip_network class.
#               Changed name of variable update_range to update_addr_range
#               to remove conflict with function of the same name.
#  5 Nov 2014   complete rewrite to use calc_indexed_net.

############################################################################
# get_indexed_net
# Given building and type information, compute an indexed net based on
# an address range for the type.
#
# Entry:
#       bldg_info - the building to get a network for.
#       type_info - the type of network to create
#       ip_ver - the IP version to create a network for
#       update_addr_range - should the range index be updated ?
# Exit:
#       A network has been computed and and returned as result.
#    

def get_indexed_net(bldg_info, type_info, ip_ver, update_addr_range):

    # Get the address range to use for this type and zone.
    zone_range = get_range_info(bldg_info['net_zone'], 
            type_info['net_type'], ip_ver)

    if ( len(zone_range) == 0 ):
        raise NetException("NoRange", "No IPv" + str(ip_ver) 
                + " address range was found for type '" + type_info['net_type']
                + "' in zone " + str(bldg_info['net_zone']) + ".")
    elif ( len(zone_range) > 1 ):
        raise NetException("TooManyRanges", "More than one IPv" + str(ip_ver) 
            + "address range was found for type '" + type_info['net_type'] 
            + "' in zone " + str(bldg_info['net_zone']) + ".")
    else:
        zone_range = zone_range[0]
        zone_range['next_bldg']

    if ( update_addr_range ):
        zone_range['next_bldg'] += 1
        update_range(zone_range)

    result = calc_indexed_net(
            zone_range['network_range'],
            #network_types.get_prefix_size(type_info, ip_ver),
            get_prefix_size(type_info, ip_ver),
            zone_range['next_bldg'] )

    return(result)
## end get_indexed_net()


#\L

#  5 Nov 2014   Renamed to calc_indexed_net.
# 25 Mar 2013   Fix recursive calls so the appropriate subnet is
#               generated. Results were incorrect until I started using
#               the binary representation of the index to determine the
#               direction to branch down the tree.
# 12 Mar 2013   Convert from an interative loop to a recursive call which
#               This means the worst case is to compute a /128 address
#               from a /64 would be 63 deep as opposed to 2 ** 64 loops.
# 22 Oct 2013   initial creation.

############################################################################
# calc_indexed_net
# Given a base network, a prefix length, and an index, find the subnet
# which is at the specified index when base network is split into subnets
# of size prefix length.
# Entry:
#    base_net - the base network
#    prefix_len - the size of networks to generate
#    net_index - the index of the network to find.
# Exit:
#    The net_index network has been computed and is returned as a result.

def calc_indexed_net(base_net, prefix_len, net_index):

    # Check that we have subnets to generate:
    if ( base_net.prefix_len == prefix_len ):
        raise ValueError("cannot subnet " + str(base_net) + \
                " into multiple subnets of /" + str(prefix_len))

    # Determine how many bits are added to the network portion
    # of the address. ie. change in prefix length.
    bitsOfNet = prefix_len - base_net.prefix_len

    # Determine if the net_index is possible in the size of bitsOfNet
    if ( (2 ** bitsOfNet) < net_index ):
        raise ValueError("there are fewer than " + str(net_index) + \
                " networks of size /" + str(prefix_len) + " in " + str(base_net))

    # Turn the index into a binary value of bitsOfNet bits:
    bin_index = bin(net_index)[2:].zfill(bitsOfNet)

    # Now that we have everything checked out, figure out the network.
    result = _calc_indexed_net(base_net, prefix_len, bin_index)

    return(result)
## end calc_indexed_net()

# Recurse down the tree to find the answer rather than looping through all
# the subnets.

def _calc_indexed_net(base_net, prefix_len, bin_index):

    # Compute all of the networks - gives us a 21 digit number of networks
    # for computing a /128 address from a /64 block!
    # ipaddress.ip_network('2001:db8::/112').num_addresses = 65536
    # netList = list(base_net.subnets(prefix_len_diff=bitsOfNet))
    # Using python 3.3 ipaddress library this could be:
    #   prefix_diff = prefix_len - base_net.prefix_len
    #   list(base_net.subnets(prefix_len_diff=prefix_diff))[subnet_index]
    # The above can take seconds to run for IPv6.

    lower_half, upper_half = list(base_net.subnets())

    # Are we on the last step?
    if ( lower_half.prefix_len == prefix_len ):
        # Yes. Which half do we want?
        if ( bin_index[0] == '0' ):
            result = lower_half
        else:
            result = upper_half
    else:
        # Which half do we need to recurse down?

        if ( bin_index[0] == '0' ):
            result = _calc_indexed_net(lower_half, prefix_len, bin_index[1:])
        else:
            result = _calc_indexed_net(upper_half, prefix_len, bin_index[1:])

    return(result)
## end _calc_indexed_net()


#\L

#  2 Jun 2014   Change function call update_zone to update_zone_net.
# 23 Oct 2013    Result is now just the netid since the calculation done
#                by net_id_2_bldg_net is broken for IPv4 and would be duplication
#                of networks routines get_net_index and get_indexed_net.
# 17 Oct 2013    Add the option to not update the zone.
# 13 Sep 2013    initial creation.

############################################################################
# get_next_bldg
# Determine the next network block to assign to a building. The default
# behaviour is to update the zone to show that a block has been assigned.
# To prevent the update from happening but return what would happen instead,
# set the no_update value to True.
# Entry:
#    range_info - the address range record to get information from.
#    no_update - do not update the range, just return information.
# Exit:
#    the id/index to use for the next building is returned. As a side
#    effect the id stored in the database is incremented.

def get_next_bldg(range_info, no_update=False):

    # Get the next building id.
    try:
        bldg_id = range_info['next_bldg']
    except:
        raise ValueError("Zone does not have user address space assigned.")

    # Extract the id of the next building to assign.
    result = range_info['next_bldg']

    if ( no_update == False ):
        # Update the zone record.
        range_info['next_bldg'] += 1
        update_zone_net(range_info)

    return(result)
## end get_next_bldg()


#\L

# 21 Aug 2014   Update to allow IPv4 addresses and use new functionality
#               in the python 3.3 ipaddress class.
#  2 Jun 2014   Change function call update_zone to update_zone_net.
# 29 Apr 2014   Fix construction of address to remove the 0x
#               generated by hex(n)
# 28 Apr 2014   Add noUpdate flag to allow getting the next loopback
#               address without assuming it is being used.
# 25 Sep 2013   initial creation.

############################################################################
# get_next_loop
#
# Calculate the next loopback address to use in the zone.
#
# Entry:
#       range_info - the information about the address range.
#       prefix_size - the size of network to return. (from typeInfo)
#       start_addr - ip network to use as base for computation.
#       update_zone - should the zone be updated to show a loopback address
#                  has been consumed?
#
# Exit:
#       The new loopback address has been determined.
#       When noUpdate is True, as a side effect the zone is updated
#       in the database to reflect the fact that a loopback address
#       has been used.
#
# Exceptions:
#       RangeExhaustion - the address range has been exhausted.
#    
def get_next_loop(range_info,
        start_addr=ipaddress.ip_address('::'), update_zone=False):

    # Get the next loop index to use:
    address_offset = range_info['next_loop']
    
    # If the start_addr has only one address then use the range info.
    if ( start_addr == ipaddress.ip_address('::') ):
        # This is the default behaviour since if start_addr is not
        # provided as an argument we have a /128.
        start_addr = range_info['network_range'].network_address

    # start_addr must be an IPNetwork with useable mask.
    #if ( address_offset > start_addr.num_addresses ):
    #    raise network_error.NetException( "RangeExhaustion",
    #            "The address range " + str(start_addr) +
    #            " has fewer than " + str(address_offset) + " addresses." )

    # What network mask to apply for loopback address?
    # Theoretically this should come from the typeInfo record for loop
    # but loopback addresses are always a single address so hard code here.
    if ( start_addr.version == 4 ):
        mask = '/32'
    else:
        mask = '/128'

    result = ipaddress.ip_network( str(start_addr + address_offset) + mask )

    # Update the zone to show a loopback address has been used.
    if ( update_zone ):
        range_info['next_loop'] += 1
        update_zone_net(range_info)

    return(result)
## end def get_next_loop():


#\L

# 21 Aug 2014   Rewrite to use new ipaddress functionality and allow
#               for both IPv4 and IPv6 results.
#  2 Jun 2014   Change function call update_zone to update_zone_net.
# 28 Apr 2014   Add link_count to allow getting multiple links if desired.
#               Add noUpdate flag to allow getting information without
#               actually updating the zone to show a link has been used.
# 25 Sep 2013   initial creation.

############################################################################
# get_next_link
# Compute a list of addresses to use for network links.
#
# Entry:
#       zone_info - the information on the zone.
#       prefix_size - from typeInfo - the list.
#       start_addr - address to start computing addresses from.
#       link_count - the number of link addresses to return.
#       update_zone - should the zone be updated to show link_count
#                   links have been used?
#
# Exit:
#       A list of addresses which can be used for network links has
#       been determined and is returned.

def get_next_link(rangeInfo, prefix_size,
        start_addr=ipaddress.ip_address('::'), link_count=1, update_zone=True):

    if ( start_addr == ipaddress.ip_address('::') ):
        start_addr = rangeInfo['network_range'].network_address

    # Get the next link index to use:
    address_offset = rangeInfo['next_link']
    
    # How many addresses are in a link network?
    addr_mult = ipaddress.ip_network(str(start_addr) + "/" +
            str(prefix_size)).num_addresses

    # Create link_count link addresses.
    result = []
    for line_ctr in list(range(link_count)):
        one_addr = ipaddress.ip_network( 
            str(start_addr + address_offset * addr_mult) + "/" +
            str(prefix_size) )
    
    # Update the zone to show a link has been used.
    if ( update_zone ):
        zone_info['next_link'] += link_count
        update_zone_net(zone_info)

    return(result)
## end def get_next_link():


#\L

# 05 Apr 2016   initial creation.

############################################################################
# as_to_glop
# Entry: asn - Autonomous System Number.
#       v6_prefix - IPv6 prefix to use for multicast address calculation.
#       scop - IPv6 multicast address scope - site, org, global.
# Exit: returns touple (ipv4 glop net, ipv6 glop net)
# Exceptions: Cannot be used for 32 bit ASN :-(

def as_to_glop(asn, v6_prefix, scop):

    # ASN's need to follow these rules:
    # 0: reserved.
    # 1-64.495: public AS numbers.
    # 64.496 – 64.511 – reserved to use in documentation.
    # 64.512 – 65.534 – private AS numbers.
    # 65.535 – reserved.
    if ( asn >= 64496 and asn <= 64511 ):
        raise ValueError("ASN value is reserved for documentation.")
    elif ( asn >= 64512 and asn <= 65534 ):
        raise ValueError("Private ASN numbers not valid for GLOP calculation.")
    elif ( asn == 0 or asn == 65535 ):
        raise ValueError("ASN values 0 and 65535 cannot be used.")
    elif ( asn > 65535 ):
        raise ValueError("GLOP range cannot be calculated for 32 bit ASNs")

    # Entire IPv4 GLOP range is 233/8, ASN specific part is 233.ASN.ASN,0/24
    # where the ASN part is the ASN converted to HEX and then fit into
    # the two octects of the address.
    asn_part = hex(asn)[2:]
    if ( len(asn_part) < 4 ):
        for i in range(0, 4 - len(asn_part)):
            asn_part = '0' + asn_part
    glop_v4_int = int( hex(233) + asn_part + '00', 16)
    glop_v4 = ipaddress.ip_network(glop_v4_int).supernet(8)

    # RFC 4291 is the start and info is updated by
    # 5952, 6052, 7136, 7346, 7371.
    # For IPv6, the range FF3x::/32 is defined for SSM services [SSM-IPV6].
    # +--------+----+----+----+----+--------+----------------+----------+
    # |   8    |  4 |  4 |  4 |  4 |    8   |       64       |    32    |
    # +--------+----+----+----+----+--------+----------------+----------+
    # |11111111|ff1 |scop|ff2 |rsvd|  plen  | network prefix | group ID |
    # +--------+----+----+----+----+--------+----------------+----------+
    #
    #                             +-+-+-+-+
    # ff1 is a set of 4 flags:    |X|Y|P|T|
    #                             +-+-+-+-+
    # X and Y may each be set to 0 or 1.  Note that X is for future
    # assignment, while a meaning is associated with Y in RFC 3956.
    #
    # - P = 0 indicates a multicast address that is not assigned
    #   based on the network prefix. This indicates a multicast
    #   address as defined in RFC 4291.
    # - P = 1 indicates a multicast address that is assigned based
    #   on the network prefix.
    # - If P = 1, T MUST be set to 1, otherwise the setting of the T
    #   bit is defined in Section 2.7 of RFC 4291.
    #
    #                                                +-+-+-+-+
    # ff2 (flag field 2) is a set of 4 flags:        |r|r|r|r|
    #                                                +-+-+-+-+
    # where "rrrr" are for future assignment as additional flag bits.
    # r bits MUST each be sent as zero and MUST be ignored on receipt.
    # Flag bits denote both ff1 and ff2.
    #
    # If the flag bits in ff1 are set to 0011, these settings create an
    # SSM range of ff3x::/32 (where 'x' is any valid scope value).  The
    # source address field in the IPv6 header identifies the owner of
    # the multicast address.  ff3x::/32 is not the only allowed SSM
    # prefix range.  For example, if the most significant flag bit in
    # ff1 is set, then we would get the SSM range ffbx::/32.
    #
    # scop is a 4-bit multicast scope value used to limit the scope of
    # the multicast group.  The values are:
    #   0  reserved
    #   1  node-local scope
    #   2  link-local scope
    #   3  (unassigned)
    #   4  (unassigned)
    #   5  site-local scope
    #   6  (unassigned)
    #   7  (unassigned)
    #   8  organization-local scope
    #   9  (unassigned)
    #   A  (unassigned)
    #   B  (unassigned)
    #   C  (unassigned)
    #   D  (unassigned)
    #   E  global scope
    #   F  reserved
    #
    # reserved should be zero.
    #
    # The unicast prefix-based IPv6 multicast address format supports
    # Source-specific multicast addresses, as defined by [SSM ARCH].  To
    # accomplish this, a node MUST:
    #      o  Set P = 1.
    #      o  Set plen = 0.
    #      o  Set network prefix = 0.
    # These settings create an SSM range of FF3x::/32 (where 'x' is any
    # valid scope value). The source address field in the IPv6 header
    # identifies the owner of the multicast address.
    #
    # group ID identifies the multicast group, either permanent or
    # transient, within the given scope.
    #
    # Given the above, it is likely that most will be part of:
    # FF35:0000:prefix::/96 site-local
    # FF38:0000:prefix::/96 organization-local
    # FF3E:0000:prefix::/96 global.

    # if you have 2001:db8:1234:5678::/64 then your prefix is:
    # FF3x:0030:2001:db8:1234:5678::/96
    # See RFC3306 / RFC3956 / RFC 4607 for the details.
    try:
        have_v6 = v6_prefix.version == 6
        if ( have_v6 ):
            if ( scop == 'site' ):
                v6_group = 'ff35:0:' + str(v6_prefix)[:-3] + '/96'
            elif ( scop == 'org' ):
                v6_group = 'ff38:0:' + str(v6_prefix)[:-3] + '/96'
            elif ( scop == 'global' ):
                v6_group = 'ff3e:0:' + str(v6_prefix)[:-3] + '/96'
            glop_v6 = ipaddress.ip_network(v6_group)
            result = (glop_v4,glop_v6)
        else:
            result = (glop_v4,)
    except:
        result = (glop_v4,)

    return(result)
## end as_to_glop()


# vim: syntax=python ts=4 sw=4 showmatch et :
