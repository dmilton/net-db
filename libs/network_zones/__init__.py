#
g_version=''

import ipaddress

from network_error import NetException
import db_access

# need network_types.get_prefix_size
from network_types import get_prefix_size

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network Description Table
# This table contains a description of the zones defined and what/where
# they should be used.

_zone_descriptions = {
    'database': 'networking',
    'table': 'zone_descriptions',
    'role': 'netmon',
    'index': 'net_zone'
}

###############################################################################
# Network Zones Table
# This table contains large network blocks that are assigned to specific
# zones of the campus.

_zone_ranges = {
    'database': 'networking',
    'table': 'zone_ranges',
    'role': 'netmon',
    'index': 'network_range'
}

# vim: syntax=python ts=4 sw=4 showmatch et :

