#
g_version=''

# libraries used by the debug module.
from sys import version_info
import platform
from os import uname
from os.path import basename
from getpass import getuser
from cgitb import text, html
from getpass import getuser
import smtplib

# debug level to honour
_gDebug = 0

# mail server hostname
SMTPSERVER = "smtp.cc.umanitoba.ca"

# mail message 'from'
BUGFROM = "netmon@cc.umanitoba.ca"

# mail message 'to'
BUGTO = "milton"

# vim: syntax=python ts=4 sw=4 showmatch et :

