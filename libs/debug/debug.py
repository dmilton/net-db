#
g_version='28 May 2014 - 2.0.8'

# libraries used by the debug module.
from sys import version_info
import platform
from os import uname
from os.path import basename
from getpass import getuser
from cgitb import text, html
from getpass import getuser
import smtplib

# debug level to honour
_gDebug = 0

# mail server hostname
SMTPSERVER = "smtp.cc.umanitoba.ca"

# mail message 'from'
BUGFROM = "netmon@cc.umanitoba.ca"

# mail message 'to'
BUGTO = "milton"


#\L

# dd mmm yyyy   initial creation.

############################################################################
# ver_info
# Entry:
#       cmdName - name of the command.
#       modVer - version of the module.
#       wrapVer - version of the command template wrapper.
# Exit: a dictionary of values regarding the current environment.
# Exceptions:

def ver_info(cmdName, modVer, wrapVer, schemaVer, dbVerInfo):
    result = {}

    # Collect as much version information as we can.
    pUname = platform.uname()
    result['os_info'] = "Host: " + pUname.node + "\n"
    result['os_info'] += "OS: " + pUname.system + " " + pUname.release+ "\n"
    result['os_info'] += "Hardware: " + pUname.machine
    pBuild = platform.python_build()
    result['py_info'] = "Python version: " + pBuild[0] + " " + pBuild[1]
    if ( cmdName > '' and modVer > '' and wrapVer > '' ):
        result['cmd_info'] = cmdName + ": module " + modVer
        result['cmd_info'] += ", main " + wrapVer
    else:
        result['cmd_info'] = "command version unavailable."
    result['schema_version'] = "Schema: " + str(schemaVer['major_version']) \
            + "." + str(schemaVer['minor_version']) \
            + " - " + str(schemaVer['reference_date'])
    if ( type(dbVerInfo) == dict ):
        for dbName in list(dbVerInfo.keys()):
            result[dbName] = dbName + ": " + dbVerInfo[dbName]

    return(result)
## end ver_info()



#
############################################################################
# set_debug(level)
# Change the debul level to 'level'
# Entry:
#       level - the new level to set debug to.
# Exit:
#       the debug level has been set. If not-zero a message is displayed.

def set_debug(level):
    """ Set the debug level for the debugging output.
    """
    global _gDebug

    _gDebug = int(level)

    print( "debug level set to: " + str(_gDebug) )

##end set_debug


############################################################################
# get_debug()                                                               #

def get_debug():
    """ Return the current debug level for the debugging output.
    """
    global _gDebug
    return(_gDebug)
##end get_debug



############################################################################
# debug_msg(level, message)

def debug_msg(debugMsgLevel, debugMessage):
    """Produce output for debugging purposes.
        The debug level is the level of debugging output to generate.
        As long as the the set debug level is greater than the level
        provided as argument, output is generated.
    """
    global _gDebug

    if ( _gDebug >=  debugMsgLevel ):
        print(debugMessage)

##end debug_msg


#

# 24 Apr 2014   Expand entire command line as part of the message.
# 16 Apr 2014   Change version to use a dictionary containing more
#               details about the environment.
# 3 Jan 2012    Changed from/to code to use variables.

##############################################################################
# crash...
def crash(cmdLine, traceInfo, cmdVersion=""):
    global BUGFROM, BUGTO, SMTPSERVER

    # Find the name of the server we are running on.
    kernel, hostname, release, version, hardware = uname()
    try:
        host, domain = hostname.split('.', 1)
    except ValueError:
        # solaris doesn't return a host with domain name in the hostname field.
        host = hostname
    ## end try
    
    # Get the name of the command.
    cmdName = basename(cmdLine[0])

    # Get the user name.
    cmdUser = getuser()

    # Change the argument list into a space separated string.
    first  = True
    cmdArgs = ""
    for anArg in cmdLine[1:]:
        if first:
            cmdArgs = anArg
            first = False
        else:
            cmdArgs = cmdArgs + ' ' + anArg

    # Make a reasonable email message subject line.
    subject = cmdUser + '@' + host + '% ' + cmdName + ' traceback'

    # Compose our email message.
    global BUGFROM, BUGTO
    message = "From: " + BUGFROM + "\n"
    message += "To: " + BUGTO + "\n"
    message += "Subject: " + subject + "\n\n"

    # spit out the entire command line.
    message += ' '.join(cmdLine) + "\n"
    if ( type(cmdVersion) == dict ):
        for aKey in list(cmdVersion.keys()):
            message += aKey + ":" + cmdVersion[aKey] + "\n"
    else:
        message += cmdVersion + "\n"
        message += "Hardware Info:" + kernel + "\n" + hostname + "\n" \
                    + release + "\n" + version + "\n" + hardware

    message += "\n"
    message += text(traceInfo)

    # translate bugTo into a list.
    recipientList = []
    for aRecipient in BUGTO.split(','):
        recipientList += [aRecipient]

    # Now send using SMTP.
    server = smtplib.SMTP(SMTPSERVER)
    server.sendmail(BUGFROM, recipientList, message)
    server.quit()

    # Print something for the user.
    print("\nA fatal error has occurred and the action(s) you requested cannot")
    print("be performed. The crash has been recorded or further investigation.")

## end crash()


# vim: syntax=python ts=4 sw=4 showmatch et :
