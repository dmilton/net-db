#
g_version='04 Sep 2014 - 2.0.11'

# string library.
import string

# date time library.
from datetime import datetime

# ip address library.
import ipaddress

# name service lookup routines.
from socket import getfqdn, gethostbyaddr

#

# 22 Jan 2014   format not working properly because division now returns
#               a real rather than an integer. Force to be an integer.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 27 Mar 2013   Make output display a bit nicer.
# 25 Mar 2013   Initial creation.

##############################################################################
# list_input_keys(theTable)
# Entry: theTable - dictionary of information about a db table.
# Accept the key information and list all keys and the description.
# This allows display of the keys specific to this command.

def list_input_keys(fieldInfo, flagFieldList=[]):

    print("Command input keys and their types.")

    # get our field names and sort them.
    fieldNames = list(fieldInfo.keys())
    fieldNames.sort()

    # Loop through to find the maximum length field name.
    maxLen = 0
    for aKey in fieldNames:
        if ( len(aKey) > maxLen ):
            maxLen = len(aKey)
    # Everything below is indented 4 spaces, assume tabs are 8 spaces.
    maxLen += 4
    # calculate position of first tab
    firstTab = int( ( ( maxLen / 8 ) + 1) * 8 )
    format = "    %-" + str(firstTab) + "s"

    # Now loop through and display the field names and their types.
    for aKey in fieldNames:
        # translate underscores to hyphens for easier input.
        inKey = aKey.translate(str.maketrans('_','-'))
        # we have type, size, default, update, and pkey to check.
        aField = fieldInfo[aKey]
        if ( 'update' in aField ):
            msg = format % inKey
            if ( aField['type'] == 'character varying' ):
                fieldType = 'char'
            else:
                fieldType = aField['type']
            if ( aField['size'] == None ):
                msg += fieldType
            else:
                msg += fieldType + "(" + str(aField['size']) + ")"
            if ( aField['default'] != None ):
                msg += " default=" + aField['default'].split(':')[0]
            if ( aField['pkey'] == True ):
                msg += " KEY"
            print(msg)

## end list_input_keys()


#

# 15 Aug 2014   Add better diagnostics when an invalid input keyword has
#               been used on the command line.
# 11 Aug 2014   Add udt_type processing so handling of ARRAY of X can be
#               managed using a comma separated list.
#  1 Apr 2013   Allow a key value of 0x0 to represent an empty/null value.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

##############################################################################
# process_input_keys()
#
# Entry:
#       keyValueList - the list of keyword/value pairs from the command line.
#       inputKeyInfo - list of valid keywords and the field type.
#

def process_input_keys(keyValueList, inputKeyInfo):

    # Start with an empty result.
    result = {}

    # Process the keyword/value pairs.
    argPtr = 0
    totalArgs = len(keyValueList)
    for argPtr in range(0, totalArgs, 2):
        # The keyword provided on the command line.
        inKeyName = keyValueList[argPtr]
        # The value for the keyword provided on the command line.
        keyValue = keyValueList[argPtr+1]
        # translate hyphen to underscore in the keyword name from command line.
        keyName = inKeyName.translate(str.maketrans('-','_'))
        if ( keyName in inputKeyInfo ):
            valueType = inputKeyInfo[keyName]['type']
            valueTypeName = inputKeyInfo[keyName]['udt_name']
        else:
            raise ValueError("invalid input keyword '" + inKeyName + "'")
        try:
            # result should use db field names (underscores)
            result[keyName] = \
                    check_input_info(keyValue, valueType, valueTypeName)
        except ValueError as why:
            if ( 'unknown type' in str(why) ):
                print("invalid value for input keyword '" + inKeyName + "'")
            raise
        ## end try
    ## end for argPtr in range(1, len(keyValueList[1:]), 2):

    if ( argPtr + 2 < totalArgs ):
        print(("Fatal error: missing value for keyword '" + \
            keyValueList[argPtr+2] + "'."))
        raise CmdException("incomplete command line input.")
    ## end if ( argPtr + 2 <= totalArgs ):
    
    return(result)
## end process_input_keys()


#\L

# 11 Aug 2014   initial creation.

############################################################################
# check_array
# Entry: inputList - a comma separated list of 'things'
# Exit: a list of 'things'
# Exceptions:

def check_array(inputList, listItemType):
    result = []

    # Produce a list of items ot type listItemType
    itemsInList = inputList.split(',')
    for anItem in itemsInList:
        result.append(check_input_info(anItem, listItemType, listItemType))

    return(result)
## end check_array()


#

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# check_bldg_code(theCode)
# Verify the value looks like a building code. No verification is
# done to ensure it is valid, just that it looks like a building code.

def check_bldg_code(theCode):

    result = theCode
    if ( len(result) < 2 or theCode.isalnum() == False ):
        raise ValueError('invalid building code ' + theCode)
    ## end building code check.

    return(result)
## end check_bldg_code()


#

#  5 Feb 2014   Correct lower call for python3 syntax.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 18 Aug 2010   Allow input of 1 or 0 values.
#               zero or one rather than F and T.
# 13 Jul 2010   Initial creation.

############################################################################
# check_boolean(theBoolean)
# Verify the value input represents a boolean.

def check_boolean(theBoolean):

    theBoolean = str(theBoolean).lower()
    if ( theBoolean in ('1', 'true', 'yes', 't', 'y') ):
        result = 'T'
    elif ( theBoolean in ('0', 'false', 'no', 'f', 'n') ):
        result = 'F'
    else:
        raise ValueError('invalid boolean value' + theBoolean)
    ## end else if ( theBoolean in xxxx ):

    return(result)
## end check_boolean()

#

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# check_campus(theCampus)
# Verify the campus input matches either a string or number value.

def check_campus(checkCampus):

    theCampus = str(checkCampus)
    if ( theCampus in ('1', 'fortgarry', 'fort-garry', 'fg') ):
        result = 1
    elif ( theCampus in ('2', 'bannatyne', 'bannatyne', 'bc') ):
        result = 2
    elif ( theCampus in ('4', 'wpgedctr', 'wec') ):
        result = 4
    elif ( theCampus in ('5', 'extendededucationdowntown', 'eed') ):
        result = 5
    elif ( theCampus in ('6', 'smartpark', 'smart-park') ):
        result = 6
    elif ( theCampus in ('7', 'glenlea') ):
        result = 7
    else:
        raise ValueError('invalid value')

    return(result)
## end check_campus()


#

#  3 Sep 2014   Add now as a valid input and use the current time.
# 24 Jan 2014   Allow full timestamp values with microseconds.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 21 Mar 2013   Rewrite to use datetime class.
# 18 Mar 2013   reformat using tabs.
#  4 Aug 2010   Correct check_date so it converts to integers.
#               Improve checking so it verifies the day of month
#               properly even for leap years.
# 20 Jul 2010   Add new input type bldgCode.

############################################################################
# check_date(theDate)
# Verify the value input looks like a date of the form yyyy-mm-dd

def check_date(inputDate):

    # Check to see if we want a current time.
    if ( "now" in inputDate.lower() ):
        inputDate = str(datetime.now())

    # Check to see if we have one of these: 2010-04-15 14:30:40.742226
    try:
        (theDate, fracSecs) = inputDate.split('.')
        microsec = int(fracSecs)
    except ValueError:
        # Nope, have a regular date.
        theDate = inputDate
        microsec = 0

    # Now start with most complex case. yyyy-mm-dd HH:MM:SS
    try:
        result = datetime.strptime(theDate, '%Y-%m-%d %H:%M:%S')
        result.replace(microsecond=microsec)
        done = True
    except ValueError:
        done = False

    if ( not done ):
        # Next up, drop the seconds. yyyy-mm-dd HH:MM
        try:
            result = datetime.strptime(theDate, '%Y-%m-%d %H:%M')
            done = True
        except ValueError:
            done = False

    if ( not done ):
        # Now try just the date. yyyy-mm-dd
        try:
            result = datetime.strptime(theDate, '%Y-%m-%d')
            done = True
        except ValueError:
            done = False
            year = 0; month = 0; day = 0

    if ( not done ):
        # How about a time? hh:mm:ss
        try:
            result = datetime.strptime(theDate, '%H:%M:%S')
            done = True
        except ValueError:
            done = False

    if ( not done ):
        # How about a time? hh:mm
        try:
            result = datetime.strptime(theDate, '%H:%M')
            done = True
        except ValueError:
            done = False


    return(result)
## end check_date()


#

# 28 Nov 2013   Change to obtain an fqdn and then the hostinfo which is
#               returned as a result.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
#  5 Aug 2010   Correct documentation in check_host_name.
# 29 Jul 2010   initial creation.

############################################################################
# check_host_name(theName)
# Verify the value resolves to a valid DNS host name.

def check_host_name(theName):
    try:
        fqdn = getfqdn(theName)
    except:
        raise ValueError("invalid host name " + theName)

    try:
        result = gethostbyaddr(fqdn)
    except:
        raise ValueError("invalid host name " + theName)

    return(result)
## end check_host_name()


#

#  4 Sep 2014   Allow inet and cidr to check for an ISO address too.
# 11 Aug 2014   New argument required so ARRAYs can be processed.
# 24 Apr 2014   Remove old google ipaddress library code.
#  7 Mar 2014   Add use of python 3.3 ipaddress library.
# 24 Jan 2014   Add better date type handling.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  1 Apr 2013   allow 0x0 to represent an empty string.
# 30 Mar 2013   update character processing.
# 18 Mar 2013   reformat using tabs.
# 20 Jul 2010   Add new input type bldgCode.
# 13 Jul 2010   Initial creation.

##############################################################################
# check_input_info()
# Entry:
#       theValue - the input value from the command line.
#       dataType - the data_type describing what the value should be.
#       dataTypeName - the udt_name for this column.

# udt_names which are NOT currently handled are:
#   real    -   float4      x   -   int2vector
#   double precision    -   float8
#   bytea   -   bytea       x   -   _text
#   abstime -   abstime     x   _   _float4
#   name    -   name        x   -   _name
#   xid     -   xid         x   -   _regtype    _anytype
#   interval    -   x       
#   timestamp with timezone -   timestampz
##################################################
#   bool, int2, _char, _text, int4, int8
#   smallint, integer, bigint
#   character varying   -   varchar
#   text    -   text        
#   cidr    -   cidr
##################################################
#   These may cause a problem:
#   character   -   bpchar
def check_input_info(theValue, dataType, dataTypeName):

    # Process the key value based on the identified type.
    if ( "int" in dataType ):
        result = int(theValue)
    elif ( "char" in dataType ):
        if ( theValue == "0x0" ):
            result = ""
        else:
            result = theValue
    elif ( "bool" in dataType ):
        result = check_boolean(theValue)
    elif ( dataType == "date" ):
        result = check_date(theValue)
    elif ( "timestamp" in dataType ):
        result = check_date(theValue)
    elif ( dataType == "ARRAY" ):
        result = check_array(theValue, dataTypeName)
    elif ( dataType == "hostName" ):
        result = check_host_name(theValue)
    elif ( dataType == "macaddr" ):
        result = check_mac_addr(theValue)
    elif ( dataType == "inet" ):
        try:
            result = check_iso_addr(theValue)
        except ValueError:
            result = ipaddress.ip_address(theValue)
    elif ( dataType == "cidr" ):
        try:
            result = check_iso_addr(theValue)
        except ValueError:
            result = ipaddress.ip_network(theValue)
    elif ( dataType == "sensorModel" ):
        result = check_sensor_model(theValue)
    elif ( dataType == "bldgCode" ):
        result = check_bldg_code(theValue)
    elif ( dataType == "campus" ):
        result = check_campus(theValue)
    else:
        # We don't know this type.
        raise ValueError("unknown type " + dataType)
    ## end if ( dataType['type'] == 'x' ):

    return(result)
## end check_input_info():


#

# 18 Feb 2013   Python3 uses different lower() syntax.
# 28 Nov 2013   Change checks to allow forms where leading zeros are not
#               in the list (like Solaris) and still parse correctly.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 13 Jul 2010   Initial creation.

############################################################################
# check_mac_addr(theMacAddr)
# Verify the MAC address received is of the correct format.

def check_mac_addr(theMacAddr):
    hexChars = '0123456789abcdef'

    # get the list of all of the 'invalid characters' by stripping
    # the good ones from the MAC address.
    theMacAddr = theMacAddr.lower()
    badChars = theMacAddr.strip(hexChars + ':.')
    if ( len(badChars) != 0 ):
        raise ValueError("invalid MAC address " + theMacAddr)

    macForm1 = theMacAddr.split('.')
    macForm2 = theMacAddr.split(':')
    if ( len(macForm1) == 3 ):
        # handle the 0123.4567.89ab form
        macString = ''
        for theBits in macForm1:
            macString += theBits.rjust(4,'0')
    elif ( len(macForm2) == 6 ):
        # handle the 01:23:45:67:89:ab form(s)
        macString = ''
        for aByte in macForm2:
            macString += aByte.rjust(2,'0')
    else:
        raise ValueError("invalid MAC address " + theMacAddr)

    if ( len(macString) != 12 ):
        raise ValueError('invalid MAC')

    # We now have a string which is all hex characters and is of
    # the correct length. Translate this into the form 00:00:00:00:00:00.
    result = macString[0:2] + ':' + macString[2:4] + ':' +\
             macString[4:6] + ':' + macString[6:8] + ':' +\
             macString[8:10] + ':' + macString[10:12]

    return(result)
## end check_mac_addr()


#\L

#  4 Sep 2014   initial creation.

############################################################################
# check_iso_addr
# Entry: isoAddr - an ISO address.
# Exit: returns true if this appears to be an ISO address
# 

def check_iso_addr(isoAddr):

    # Check that an address is of the form:
    #   49.0001.0000.0000.002a.00
    isoAddrChars = "0123456789abcdef."

    # get the list of all of the 'invalid characters' by stripping
    # the valid ones from the address.
    result = isoAddr.lower()
    badChars = result.strip(isoAddrChars)
    if ( len(badChars) > 0 or len(isoAddr) != 20 or not (
        ( isoAddr[:3] == "49." and isoAddr[-3:] == ".00" ) and
            ( len(isoAddr.split(".")) == 5 )) ):
        raise ValueError("invalid ISO address " + isoAddr)

    return(result)
## end check_iso_addr()


#\L

# 18 Apr 2013   initial creation.

############################################################################
# check_name
def check_name(theName):
    # Names must start with a letter and consist of 
    # letters, numbers, periods hyphens and underscores.
    alphaLower = "abcdefghijklmnopqrstuvwxyz"
    alphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphaChars = alphaLower + alphaUpper
    allChars = alphaChars + "12345567890._-"

    if ( len(theName) == 0 ):
        raise ValueError('invalid name')

    if ( theName[0] not in alphaChars ):
        raise ValueError('invalid name')

    for aChar in theName[1:]:
        if ( aChar not in allChars ):
            raise ValueError('invalid name')

    return()
## end check_name()


#

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Mar 2013   reformat using tabs.
# 30 Aug 2010   Added A5205 model.
# 29 Jul 2010   initial creation.

############################################################################
# check_sensor_model(theModel)
# Verify the value looks like a sensor model.

def check_sensor_model(theModel):

    result = theModel
    if ( (theModel in ('A5010', 'A5020', 'A5205')) != True ):
        raise ValueError('invalid sensor model: ' + theModel)
    ## end sensor model check.

    return(result)
## end check_sensor_model()


# vim: syntax=python ts=4 sw=4 showmatch et :
