#
g_version=''

# string library.
import string

# date time library.
from datetime import datetime

# ip address library.
import ipaddress

# name service lookup routines.
from socket import getfqdn, gethostbyaddr

