#
g_version='16 Apr 2015 - 5f805f4'

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Building Types table.
#
global _building_types
_building_types = {
	'database': 'networking',
	'role': 'netmon',
	'table': 'building_types',
	'index': 'bldg_type_id'
}

#

# 26 Apr 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _building_types, _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _building_types.update( db_access.init_db_table(_building_types) )

## end initialize()


#\L

# 18 Feb 2015   initial creation.

############################################################################
# get_building_types
# Return a list of defined building types.
# Entry:    bldg_type_name - optional, if present, restrict to just this type.
# Exit:     A list of defined building types is returned.

def get_building_types(bldg_type_name=""):
    global _building_types

    if ( bldg_type_name != "" ):
        my_where = "bldg_type = '" + bldg_type_name + "'"
    else:
        my_where = ""
    # Get the list of defined building types
    result = db_access.get_table_rows(_building_types, my_where)

    if ( len(result) == 0 ):
        raise ValueError("The building type: '" + bldg_type_name
                + "' does not exist.")

    return(result)
## end get_building_types()


#\L

# 24 Mar 2014   initial creation.

############################################################################
# get_building_type_fields
# Entry: none
# Exit: returns the list of fields in the building_types table
# Exceptions:

def get_building_type_fields():

    global _building_types

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_building_types)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can
        # accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_building_type_fields()


#\L

# 16 Apr 2015   initial creation.

############################################################################
# _check_building_type
# Check that the building type does not exist and that a current type
# does not share the same description.
# Entry:    bldg_type_info - building type to verify.
# Exit:     If the building type is unique, return True.

def _check_building_type(bldg_type_info):
    global _building_types

    # Check that we have a unique description.
    my_where = "lower(bldg_type_description) = lower('" + \
            bldg_type_info['bldg_type_description'] + "')"
    dup_descr = db_access.get_table_rows(_building_types, my_where)
    if ( len(dup_descr) > 0 ):
        raise NetException("BldgTypeDescrExists",
                "The building type '" + dup_descr['bldg_type'] +
                "' already has that description.")

    # Check that we have a unique bldg type id if provided
    if ( 'bldg_type_id' in bldg_type_info ):
        my_where = "bldg_type_id = '" + bldg_type_info['bldg_type_id'] + "'"
        id_count = db_access.get_row_count(_building_types, my_where)
        if ( id_count > 0 ):
            raise NetException("BldgTypeIDExists", "The building type ID " 
                    + bldg_type_info['bldg_type_id'] + " already exists.")

    # Check that we have a unique name.
    my_where = "lower(bldg_type) = lower('" + bldg_type_info['bldg_type'] + "')"
    name_count = db_access.get_row_count(_building_types, my_where)
    if ( name_count > 0 ):
        raise NetException("BldgTypeExists",
                "A building type named '" + bldg_type_info['bldg_type'] + \
                "' already exists.")

    return
## end _check_bldg_type():


#\L

# 18 Feb 2015   initial creation.

############################################################################
# new_bldg_type
# Create a new empty template for a building.
def new_bldg_type():

    global _building_types

    # Ask the DB what fields are in a building_type record.
    result = db_access.new_table_row(_building_types)

    return(result)
## end new_bldg_type()


#

# 18 Feb 2015   Initial creation

##############################################################################
# add_building_type(bldg_type_info)
# Add a new building to the building_detail table.
# Entry:    bldg_type_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_building_type(bldg_type_info):

    global _building_types

    # Verify this building type is unique.
    _check_building_type(bldg_type_info)

    # Create a new record.
    new_bldg_type = db_access.new_table_row(_building_types)

    # Copy matching values from bldg_type_info into our new record.
    # This eliminates any extraneous vaules in bldg_type_info.
    for a_key in list(new_bldg_type.keys()):
        if ( a_key in bldg_type_info ):
            new_bldg_type[a_key] = bldg_type_info[a_key]
        else:
            del new_bldg_type[a_key]

    # Add the record into the database.
    result = db_access.set_table_row(_building_types, new_bldg_type)[0]

    return(result)
## end add_building_type():


#

# 18 Feb 2015   Initial creation

##############################################################################
# update_bldg_type(bldg_type_info)
# Update a building type.
# Entry:   bldg_type_info - dictionary of values from the building type.
# Exit:    the building type record has been updated.

def update_bldg_type(bldg_type_info):

    global _building_types

    # Check to see if the type is unique.
    try:
        _check_bldg_type(bldg_type_info)
    except network_error.NetException as net_err:
        if (net_err.name == "BldgTypeExists" ):
            # this is as it should be, we have something to update.
            pass
    else:
        raise network_error.NetException("BldgTypeNotFound",
                "The building type '" + bldg_type_info['bldg_type']
                + "' was not found.")

    # Update the record in the database.
    my_where = "bldg_type = '" + bldg_type_info['bldg_type'] + "'"
    result = db_access.set_table_row(_building_types, 
                                bldg_type_info, my_where)[0]

    return(result)
## end update_bldg_type():


#

#  9 Sep 2014   Add exception for a delete that deleted zero rows.
# 18 Sep 2013   Initial creation

##############################################################################
# delete_bldg_type(bldg_type_info)
# Delete the building type record.
# Entry:
#       bldg_type_info - dictionary of values from the building type
# Exit:
#       a new building record has been added to the database.
# Exceptions:
#       A UnknownCampusID NetException is raised if no rows are deleted.

def delete_bldg_type(bldg_type_info):

    global _building_types

    try:
        _check_building_type(bldg_type_info)
    except network_error.NetException as net_err:
        if (net_err.name == "BldgTypeExists" ):
            # this is as it should be, we have something to delete.
            pass
    else:
        raise network_error.NetException("BldgTypeNotFound",
                "The building type '" + bldg_type_info['bldg_type']
                + "' was not found.")

    # Build a where clause to identify this building.
    my_where = "bldg_type = '" + bldg_type_info['bldg_type'] + "'"

    # Delete the record.
    row_count = db_access.delete_table_rows(_building_types, my_where)

    return
## end delete_bldg_type():


#\L

# 18 Feb 2015   initial creation.

############################################################################
# verify_building_type
# Verify that the building type exists.
# Entry:    bldg_type_name - building type to verify.
# Exit:     If the building type is valid, return True.

def verify_building_type(bldg_type_name):
    global _building_types

    # Get a row count for building types with the specified name.
    my_where = "lower(bldg_type) = lower('" + bldg_type_name + "')"
    row_count = db_access.get_row_count(_building_types, my_where)

    return(row_count == 1)
## end verify_building_type()


# vim: syntax=python ts=4 sw=4 showmatch et :
