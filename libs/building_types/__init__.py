#
g_version=''

from network_error import NetException
import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Building Types table.
#
global _building_types
_building_types = {
	'database': 'networking',
	'role': 'netmon',
	'table': 'building_types',
	'index': 'bldg_type_id'
}

