#

# 21 Mar 2013        reformat to use tabs.
# 9 Sep 2010        Change query building so the where clause is a simple
#                    expression. The "WHERE" string is added in SelectQuery.

##############################################################################
# find_sensorByCsid(theCsid)
# Find a sensor in the database and return the resulting record.
# Entry: theCsid - the csid of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByCsid(theCsid):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    result = {}

    myWhere = "csid = '" + theCsid + "'"
    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorCSID", theCsid)

    return(result)
## end find_sensorByCsid()


# vim: syntax=python ts=4 sw=4 showmatch et :

