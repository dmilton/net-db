#
g_version=''

from network_error import NetException
import db_access
from cmd_output import output_format

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

# vim: syntax=python ts=4 sw=4 showmatch et :

