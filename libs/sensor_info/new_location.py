#\L

# 24 May 2013        initial creation.

############################################################################
# new_location
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_location():
    global _sensorLocations

    result = db_access.new_table_row(_sensorLocations)

    return(result)
## end new_location()


# vim: syntax=python ts=4 sw=4 showmatch et :

