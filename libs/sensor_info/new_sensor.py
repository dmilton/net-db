#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_sensor
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_sensor():
    global _sensorDetail

    result = db_access.new_table_row(_sensorDetail)

    return(result)
## end new_sensor()


# vim: syntax=python ts=4 sw=4 showmatch et :

