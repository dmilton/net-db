#

# 24 May 2013        rewrite to use new db_access.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Seems the addition of a function call result
#                    (WhereClause) to a string causes a type error.
#                    Changed the calls to assign to a separate variable
#                    and then add it to the rest of the query.
#  9 Sep 2010        Change query building to use the WhereClause
#                    db_access call rather than building it manually.

##############################################################################
# add_sensor(newInfo)
# Entry:
#        newInfo - the information to be updated in in the database.
# Exit: A check is made to ensure an entry for this sensor does not
#        exist in the table. Once the check completes an Insert is made
#        to the sensor_detail table. The result is the sensor record
#        of the newly added sensor.

def add_sensor(newInfo):

    global _sensorDetail, _sensorLocations, _sensorView

    myWhere = "serial_nbr = '" + newInfo['serial_nbr']
    sensorCount = db_access.get_row_count(_sensorDetail, myWhere)
    if ( sensorCount > 0 ):
        print("A sensor with this serial number already exists.")
    else:
        result = db_access.set_table_row(_sensorDetail, newInfo, myWhere)

    return(result)
## end add_sensor()


# vim: syntax=python ts=4 sw=4 showmatch et :

