#

# 24 May 2013        rewrite for new db_access.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Initial creation for set-sensor-location command.

##############################################################################
# delete_location
# Entry: aSensorLocation - the host name to use for the sensor location.
# Exit: result is the record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def delete_location(aLocation):

    global _sensorLocations

    # Now remove it from the database.
    myWhere = "name = '" + aLocation['name'] + "'"
    db_access.delete_table_rows(_sensorLocations, myWhere)

    return
## end delete_location()


# vim: syntax=python ts=4 sw=4 showmatch et :

