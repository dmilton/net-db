#

# 24 May 2013        rewrite to use new db_access.
# 21 Mar 2013        reformat to use tabs.
# 31 Aug 2010        initial creation

############################################################################
# delete_sensor(theSensor)
# Remove a sensor from the database.
def delete_sensor(theSensor):
    
    global _sensorDetail, _sensorLocations

    dropSensor = DropQuery('sensor_detail', sensorDetail, theSensor)
    dropLocation = DropQuery('sensor_locations', sensorLocations, theSensor)

    return
## end delete_sensor():


# vim: syntax=python ts=4 sw=4 showmatch et :

