#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building of the where clause to be a
#                    simple expression so SelectQuery can add "WHERE".

##############################################################################
# find_sensorByMac(theMacAddr)
# Find a sensor in the database and return the resulting record.
# Entry: theMacAddr - the theMacAddr of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByMac(theMacAddr):
    """
    Find a sensor in the sensor table.
    """
    global _sensorDetail

    result = {}

    # Construct our query.
    myWhere = "lan_mac = '" + theMacAddr + "'"
    myWhere += " OR wlan_mac = '" + theMacAddr + "'"

    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len (result) == 0 ):
        raise NetException("UnknownSensorMac", theMacAddr)

    return(result)
## end find_sensorByMac()


# vim: syntax=python ts=4 sw=4 showmatch et :

