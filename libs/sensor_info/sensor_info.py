#
g_version='20 Jan 2014 - 2.0.0'

from network_error import NetException
import db_access
from cmd_output import output_format

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False


#\

# 24 May 2013        Initial creation.

###############################################################################
# sensorDetail table.
#
_sensorDetail = {
    'database': 'networking',
    'table': 'sensor_detail',
    'role': 'netmon',
    'index': 'serial-nbr'
}

###############################################################################
# The sensor locations table.
#
_sensorLocations = {
    'database': 'networking',
    'table': 'sensor_locations',
    'role': 'netmon',
    'index': 'name'
}

###############################################################################
# Sensors View
#
_sensorView = {
    'database': 'networking',
    'table': 'sensors',
    'role': 'netview',
    'index': 'serial-nbr'
}



#

# 24 May 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _sensorDetail, _sensorLocations, _sensorView
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _sensorDetail.update(db_access.init_db_table(_sensorDetail))
    _sensorLocations.update(db_access.init_db_table(_sensorLocations))
    _sensorView.update(db_access.init_db_table(_sensorView))

    _tables_initialized = True

## end initialize()



#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building to use the WhereClause
#                    db_access call rather than building it manually.

##############################################################################
# find_sensor(theSerial)
# Find a sensor in the database and return the resulting record.
# Entry: theSerial - the serial number of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensor(theSerial):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    result = {}

    # build our where clause.
    myWhere = "serial_nbr = '" + theSerial + "'"
    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorSerialNbr", theSerial)

    return(result)
## end find_sensor()



#

# 21 Mar 2013        reformat to use tabs.
# 9 Sep 2010        Change query building so the where clause is a simple
#                    expression. The "WHERE" string is added in SelectQuery.

##############################################################################
# find_sensorByCsid(theCsid)
# Find a sensor in the database and return the resulting record.
# Entry: theCsid - the csid of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByCsid(theCsid):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    result = {}

    myWhere = "csid = '" + theCsid + "'"
    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorCSID", theCsid)

    return(result)
## end find_sensorByCsid()



#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building of the where clause to be a
#                    simple expression so SelectQuery can add "WHERE".

##############################################################################
# find_sensorByMac(theMacAddr)
# Find a sensor in the database and return the resulting record.
# Entry: theMacAddr - the theMacAddr of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByMac(theMacAddr):
    """
    Find a sensor in the sensor table.
    """
    global _sensorDetail

    result = {}

    # Construct our query.
    myWhere = "lan_mac = '" + theMacAddr + "'"
    myWhere += " OR wlan_mac = '" + theMacAddr + "'"

    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len (result) == 0 ):
        raise NetException("UnknownSensorMac", theMacAddr)

    return(result)
## end find_sensorByMac()



#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building so the where clause is a
#                    simple expression. This way we don't run into
#                    errors when the db_access.WhereClause is used.

##############################################################################
# find_sensorByName(theSerial)
# Find a sensor in the database and return the resulting record.
# Entry: theSerial - the serial number of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByName(theName):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    result = {}

    myWhere = "name = '" + theName + "'"
    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorName", theName)

    return(result)
## end find_sensorByName()



#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building so the where part is a
#                    simple expression.

##############################################################################
# find_sensorByReplaced(theSerial)
# Find a sensor in the database and return the resulting record.
# Entry: theSerial - the csid of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByReplaced(theSerial):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    myWhere = "replaced_by '" + theSerial + "'"

    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorName", theName)

    return(result)
## end find_sensorByReplaced()



#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_sensor
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_sensor():
    global _sensorDetail

    result = db_access.new_table_row(_sensorDetail)

    return(result)
## end new_sensor()



#

# 24 May 2013        rewrite to use new db_access.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Seems the addition of a function call result
#                    (WhereClause) to a string causes a type error.
#                    Changed the calls to assign to a separate variable
#                    and then add it to the rest of the query.
#  9 Sep 2010        Change query building to use the WhereClause
#                    db_access call rather than building it manually.

##############################################################################
# add_sensor(newInfo)
# Entry:
#        newInfo - the information to be updated in in the database.
# Exit: A check is made to ensure an entry for this sensor does not
#        exist in the table. Once the check completes an Insert is made
#        to the sensor_detail table. The result is the sensor record
#        of the newly added sensor.

def add_sensor(newInfo):

    global _sensorDetail, _sensorLocations, _sensorView

    myWhere = "serial_nbr = '" + newInfo['serial_nbr']
    sensorCount = db_access.get_row_count(_sensorDetail, myWhere)
    if ( sensorCount > 0 ):
        print("A sensor with this serial number already exists.")
    else:
        result = db_access.set_table_row(_sensorDetail, newInfo, myWhere)

    return(result)
## end add_sensor()



#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building to use the WhereClause
#                    db_access call rather than building it manually.

##############################################################################
# update_sensor(newInfo)
# Entry:
#        newInfo - the information to be updated in in the database.
# Exit: the database has been updated.

def update_sensor(newInfo):

    global sensorDetail, sensorLocations
    detailDbInfo = sensorDetail['sensor_detail']
    sensorUpdateQuery = ''
    locationUpdateQuery = ''

    # Do we have enough information to perform an update
    # of the sensorDetail table?
    doUpdateDetail = db_access.VerifyRequired(newInfo, sensorDetail)
    if ( doUpdateDetail == True ):
        # Yes, we can update the sensor_detail table.
        # Check to see if this sensor already exists.
        #sensorWhere = sensorDetail['serial-nbr']['dbField'] + " = " + \
        #        db_access.QuoteField(newInfo['serial-nbr'],
        #                sensorDetail['serial-nbr']['type'])
        #myQuery = "SELECT count(*) FROM sensor_detail" + \
        #        " WHERE " + sensorWhere
        myQuery = "SELECT count(*) FROM " + detailDbInfo['table'] + " WHERE " +\
            db_access.WhereClause(detailDbInfo['table'], sensorDetail, newInfo)
        dbResult = db_access.PgQuery(myQuery, dbName=detailDbInfo['database'])
        sensorCount = dbResult[0][0]

        if ( sensorCount == 0 ):
            print("A sensor with this serial number does not exist.")
        else:
            sensorUpdateQuery = db_access.UpdateQuery(detailDbInfo['table'], \
                    newInfo, sensorDetail, 'serial-nbr')
        ## end else if ( sensorCount > 0 ):
    ## end if ( updateSensor == True ):

    doupdate_location = db_access.VerifyRequired(newInfo, sensorLocations)
    if ( doupdate_location == True ):
        # Check to see if this location already exists.
        #locationWhere = sensorLocations['name']['dbField'] + " = " + \
        #        db_access.QuoteField(newInfo['name'],
        #                sensorLocations['name']['type'])
        #myQuery = "SELECT count(*) FROM sensor_locations" + \
        #        " WHERE " + locationWhere
        locnDbInfo = sensorLocations['sensor_locations']
        myQuery = "SELECT count(*) FROM " + locnDbInfo['table'] + " WHERE " + \
            db_access.WhereClause(locnDbInfo['table'], sensorLocations, newInfo)
        dbResult = db_access.PgQuery(myQuery, dbName=detailDbInfo['database'])
        locationCount = dbResult[0][0]

        if ( locationCount == 0 ):
            print("A sensor with this name does not exist.")
        else:
            locationUpdateQuery = db_access.UpdateQuery('sensor_locations', \
                    newInfo, sensorLocations, 'name')
        ## end else if ( locationCount == 0 ):
    ## end if ( locationUpdate == True ):
    
    if ( sensorUpdateQuery > '' or locationUpdateQuery > '' ):
        dbResult = db_access.PgQuery(sensorUpdateQuery + locationUpdateQuery, 
            dbName=detailDbInfo['database'],
            dbUser=detailDbInfo['role'],
            dbPass=detailDbInfo['pass'])
    ## end if ( sensorUpdateQuery > '' or locationUpdateQuery > '' ):

    # Now grab the entire record from the database tables.
    sensorQuery = \
        db_access.SelectQuery('sensors', sensorView, sensorWhere, 1)
    dbResult = \
        db_access.PgQuery(sensorQuery, dbName=detailDbInfo['database'])
    try:
        result = db_access.MakeDict(dbResult[0], sensorView['sensors'])
    except TypeError:
        # We didn't get an answer from PgQuery.
        result = {}
    except:
        raise
    ## end try

    return(result)
## end update_sensor()



#

# 24 May 2013        rewrite to use new db_access.
# 21 Mar 2013        reformat to use tabs.
# 31 Aug 2010        initial creation

############################################################################
# delete_sensor(theSensor)
# Remove a sensor from the database.
def delete_sensor(theSensor):
    
    global _sensorDetail, _sensorLocations

    dropSensor = DropQuery('sensor_detail', sensorDetail, theSensor)
    dropLocation = DropQuery('sensor_locations', sensorLocations, theSensor)

    return
## end delete_sensor():



#

# 21 Mar 2013        reformat to use tabs.
#  1 Sep 2010        Change the maintenance contract information printing
#                    to use the maint-status value. Add the printing of
#                    rma_sensor when one exists.
# 31 Aug 2010        Add printing of the removal reason when
#                    the field has been set.

##############################################################################
# print_sensor(theSensor)
# Entry: theSensor - sensor record to display for the user.
# Exit: 

def print_sensor(theSensor):

    if ( len(theSensor) > 0 ):
        print("Sensor Information:")

        # Start with the information from sensor_detail
        if ( 'name' in theSensor ):
            serialString = "Serial Number (name): " + \
                    theSensor['serial-nbr'] + " (" + theSensor['name'] + ")"
        else:
            serialString = "Serial Number: " + theSensor['serial-nbr']
        print(serialString)

        if ( 'model' in theSensor ):
            modelString = "Model: " + theSensor['model']
        else:
            modelString = "Model: NOT SET."
        print(modelString)

        macString = "LAN/WLAN MAC Address: "
        if ( 'lan-mac' in theSensor ):
           macString = macString + theSensor['lan-mac']
        else:
           macString = macString + "NULL"
        if ( 'wlan-mac' in theSensor ):
           macString = macString + ' / ' + theSensor['wlan-mac']
        else:
           macString = macString + " / NULL"
        print(macString)

        flagString = "Active/Maintenance State: "
        if ( 'active' in theSensor ):
            flagString = flagString + str(theSensor['active'])
        else:
            flagString = flagString + "NULL"

        if ( 'maint-state' in theSensor ):
            if ( theSensor['maint-state'] == 'add' ):
                flagString = flagString + " / Add to next years contract."
            elif ( theSensor['maint-state'] == 'on' ):
                flagString = flagString + " / On contract."
            elif ( theSensor['maint-state'] == 'remove' ):
                flagString = flagString + " / Remove from contract."
            elif ( theSensor['maint-state'] == 'off' ):
                flagString = flagString + " / Sent back to manfacturer."
            else:
                flagString = flagString + "unknown state: " + \
                        theSensor['maint-state']
            ## end elif ( theSensor['maint-state'] == 'XXX' ):
        else:
            flagString = flagString + " / NULL"
        print(flagString)

        csidString = "CSID: "
        if ( 'csid' in theSensor ):
            csidString = csidString + str(theSensor['csid'])
        else:
            csidString = csidString + "NULL"
        print(csidString)

        poString = "Purchase Order: "
        if ( 'po' in theSensor ):
            poString = poString + str(theSensor['po'])
        else:
            poString = poString + "NULL"
        print(poString)

        serialKeyString = "Serial Key: "
        if ( 'serial-key' in theSensor ):
            serialKeyString = serialKeyString + str(theSensor['serial-key'])
        else:
            serialKeyString = serialKeyString + "NULL"
        print(serialKeyString)

        installString = "Installed on: "
        if ( 'install-date' in theSensor ):
            installString = installString + str(theSensor['install-date'])
        else:
            installString = installString + "NULL"
        print(installString)

        if ( 'campusid' in theSensor ):
            print(("Inventoried for the " + \
                    theSensor['campus-name'] + " campus."))

        if ( 'replaced-by' in theSensor ):
            print(("Removed from service on: " + \
                    str(theSensor['removal-date'])))
            print(("Replaced by: " + str(theSensor['replaced-by'])))
            if ( 'removal-reason' in theSensor ):
                print(("Removal Reason:" + str(theSensor['removal-reason'])))

        if ( 'rma-sensor' in theSensor ):
            print(("This sensor was RMA'd to factory and the replacement is: "\
                    + theSensor['rma-sensor']))
        
        # Add the information from sensor_locations
        if ( 'building-name' in theSensor ):
            print(("Building: " + theSensor['building-name']))

        if ( 'location' in theSensor ):
            print(("Location: " + theSensor['location']))

    ## end if ( len(theSensor) > 0 ):

## end print_sensor()



#

# 21 Mar 2013        reformat to use tabs.
# 19 Aug 2010        Add the ability to print a header for the
#                    fields being output.
# 18 Aug 2010        Fix problems with dump_sensor and 'null' fields where
#                    a sensor has no location information. ie. inventoried
#                    but not installed or removed/returned/failed.
# 16 Aug 2010        Create dump_sensor routine to produce CSV format output.

##############################################################################
# dump_sensor(theSensor)
# Entry: theSensor - sensor record to display for the user.
#         dumpFields - list of fields to dump.
#         printHeader - print a header consisting of the fields being dumped.
# Exit: 
#

def dump_sensor(theSensor, dumpFields, printHeader):

    global sensorView

    if ( len(theSensor) > 0 ):
        first = True
        for aField in dumpFields:
            if ( aField in theSensor ):
                theField = \
                    output_format(theSensor[aField], sensorView[aField]['type'])
            else:
                theField = "NULL"
            ## end else if ( theSensor.has_key(aField) == True ):
            if ( first == True ):
                dumpLine = theField
                if ( printHeader == True ):
                    headLine = aField
                ## end if ( printHeader == True ):
                first = False
            else:
                dumpLine = dumpLine + "," + theField
                if ( printHeader == True ):
                    headLine = headLine + "," + aField
                ## end if ( printHeader == True ):
            ## end else if ( first == True ):
        ## end for aField in dumpFields:
    ## end if ( len(theSensor) > 0 ):

    # Now that we have our output lines, print them.
    if ( printHeader == True ):
        print(headLine)
    ## end if ( printHeader == True ):
    print(dumpLine)

## end dump_sensor()



#

#  4 Jun 2013        rewrite to use new db_access routines.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        The try clause surrounding MakeDict was to catch
#                    a type error in the reference dbResult[0]. Other
#                    code errors caused this exception to trigger and
#                    an incorrect diagnostic was gererated. Moved the
#                    single piece to be tested out of the MakeDict call
#                    so bugs in MakeDict do not trigger this exception.
# 17 Sep 2010        Initial creation for set-sensor-location command.

##############################################################################
# find_location
# Entry: aSensorLocation - the host name to use for the sensor location.
# Exit: result is the record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def find_location(aLocation):

    global sensorLocations

    # build our query.
    myWhere = "name = '" + aLocation + "'"
    result = db_access.get_table_rows(sensorLocations, myWhere)

    if ( len(result) == 0 ):
        raise NetException("LocationNotFound", "the sensor " +
                aLocation['name'] + " was not found.")

    return(result)
## end find_location()



#\L

# 24 May 2013        initial creation.

############################################################################
# new_location
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_location():
    global _sensorLocations

    result = db_access.new_table_row(_sensorLocations)

    return(result)
## end new_location()



#

# 24 May 2013        rewrite to use new db_access.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010         Initial creation for set-sensor-location command.

##############################################################################
# add_location
# Entry: aSensorLocation - the host name to use for the sensor location.
# Exit: result is the record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def add_location(newLocation):

    global _sensorLocations

    myWhere = "name = '" + newLocation['name'] + "'"
    result = db_access.set_table_row(_sensorLocations, newLocation, myWhere)

    return(result)
## end add_location()


#

# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Correct PgQuery call to identify the user and
#                    password variables rather than by position.
# 17 Sep 2010        Initial creation for set-sensor-location command.

##############################################################################
# update_location
# Entry: aLocation - the information to update.
# Exit: result is the updated record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def update_location(theLocation):

    global sensorLocations;

    # Find the location. If it doesn't exist this fails.
    thisLocation = find_location(theLocation);

    # Build the update query.
    dbTableInfo = sensorLocations['sensor_locations'];
    myQuery = db_access.UpdateQuery(dbTableInfo['table'], \
            theLocation, sensorLocations, dbTableInfo['index']);

    # Perform the update.
    dbResult = db_access.PgQuery(myQuery, \
            dbName=sensorLocations['sensor_locations']['database'],
            dbUser=sensorLocations['sensor_locations']['role'],
            dbPass=sensorLocations['sensor_locations']['pass']);

    # Fetch the updated record.
    result = find_location(theLocation);

    return(result);
## end update_location()



#

# 24 May 2013        rewrite for new db_access.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Initial creation for set-sensor-location command.

##############################################################################
# delete_location
# Entry: aSensorLocation - the host name to use for the sensor location.
# Exit: result is the record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def delete_location(aLocation):

    global _sensorLocations

    # Now remove it from the database.
    myWhere = "name = '" + aLocation['name'] + "'"
    db_access.delete_table_rows(_sensorLocations, myWhere)

    return
## end delete_location()



#

# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Initial creation.

##############################################################################
# print_location(theLocation)
# Entry:
#        theLocation = location record to print.
# Exit: the information present in the location record has been printed.

def print_location(theLocation):

    print(("Sensor name: " + theLocation['name']))
    if ( 'sensor' in theLocation ):
        sensor = "Sensor Serial Number: " + theLocation['sensor']
    else:
        sensor = "Sensor Serial Number: Not Set"
    print(sensor)

    if ( 'location' in theLocation ):
        location = "Location: " + theLocation['location']
    else:
        location = "Location: Not Set"
    print(location)

    if ( 'notes' in theLocation ):
        notes = "Notes: " + theLocation['notes']
    else:
        notes = "Notes: Not Set"
    print(notes)

## end print_location():



# vim: syntax=python ts=4 sw=4 showmatch et :
