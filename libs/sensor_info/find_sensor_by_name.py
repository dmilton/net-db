#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building so the where clause is a
#                    simple expression. This way we don't run into
#                    errors when the db_access.WhereClause is used.

##############################################################################
# find_sensorByName(theSerial)
# Find a sensor in the database and return the resulting record.
# Entry: theSerial - the serial number of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByName(theName):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    result = {}

    myWhere = "name = '" + theName + "'"
    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorName", theName)

    return(result)
## end find_sensorByName()


# vim: syntax=python ts=4 sw=4 showmatch et :

