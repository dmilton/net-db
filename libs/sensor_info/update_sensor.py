#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building to use the WhereClause
#                    db_access call rather than building it manually.

##############################################################################
# update_sensor(newInfo)
# Entry:
#        newInfo - the information to be updated in in the database.
# Exit: the database has been updated.

def update_sensor(newInfo):

    global sensorDetail, sensorLocations
    detailDbInfo = sensorDetail['sensor_detail']
    sensorUpdateQuery = ''
    locationUpdateQuery = ''

    # Do we have enough information to perform an update
    # of the sensorDetail table?
    doUpdateDetail = db_access.VerifyRequired(newInfo, sensorDetail)
    if ( doUpdateDetail == True ):
        # Yes, we can update the sensor_detail table.
        # Check to see if this sensor already exists.
        #sensorWhere = sensorDetail['serial-nbr']['dbField'] + " = " + \
        #        db_access.QuoteField(newInfo['serial-nbr'],
        #                sensorDetail['serial-nbr']['type'])
        #myQuery = "SELECT count(*) FROM sensor_detail" + \
        #        " WHERE " + sensorWhere
        myQuery = "SELECT count(*) FROM " + detailDbInfo['table'] + " WHERE " +\
            db_access.WhereClause(detailDbInfo['table'], sensorDetail, newInfo)
        dbResult = db_access.PgQuery(myQuery, dbName=detailDbInfo['database'])
        sensorCount = dbResult[0][0]

        if ( sensorCount == 0 ):
            print("A sensor with this serial number does not exist.")
        else:
            sensorUpdateQuery = db_access.UpdateQuery(detailDbInfo['table'], \
                    newInfo, sensorDetail, 'serial-nbr')
        ## end else if ( sensorCount > 0 ):
    ## end if ( updateSensor == True ):

    doupdate_location = db_access.VerifyRequired(newInfo, sensorLocations)
    if ( doupdate_location == True ):
        # Check to see if this location already exists.
        #locationWhere = sensorLocations['name']['dbField'] + " = " + \
        #        db_access.QuoteField(newInfo['name'],
        #                sensorLocations['name']['type'])
        #myQuery = "SELECT count(*) FROM sensor_locations" + \
        #        " WHERE " + locationWhere
        locnDbInfo = sensorLocations['sensor_locations']
        myQuery = "SELECT count(*) FROM " + locnDbInfo['table'] + " WHERE " + \
            db_access.WhereClause(locnDbInfo['table'], sensorLocations, newInfo)
        dbResult = db_access.PgQuery(myQuery, dbName=detailDbInfo['database'])
        locationCount = dbResult[0][0]

        if ( locationCount == 0 ):
            print("A sensor with this name does not exist.")
        else:
            locationUpdateQuery = db_access.UpdateQuery('sensor_locations', \
                    newInfo, sensorLocations, 'name')
        ## end else if ( locationCount == 0 ):
    ## end if ( locationUpdate == True ):
    
    if ( sensorUpdateQuery > '' or locationUpdateQuery > '' ):
        dbResult = db_access.PgQuery(sensorUpdateQuery + locationUpdateQuery, 
            dbName=detailDbInfo['database'],
            dbUser=detailDbInfo['role'],
            dbPass=detailDbInfo['pass'])
    ## end if ( sensorUpdateQuery > '' or locationUpdateQuery > '' ):

    # Now grab the entire record from the database tables.
    sensorQuery = \
        db_access.SelectQuery('sensors', sensorView, sensorWhere, 1)
    dbResult = \
        db_access.PgQuery(sensorQuery, dbName=detailDbInfo['database'])
    try:
        result = db_access.MakeDict(dbResult[0], sensorView['sensors'])
    except TypeError:
        # We didn't get an answer from PgQuery.
        result = {}
    except:
        raise
    ## end try

    return(result)
## end update_sensor()


# vim: syntax=python ts=4 sw=4 showmatch et :

