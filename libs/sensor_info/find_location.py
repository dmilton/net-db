#

#  4 Jun 2013        rewrite to use new db_access routines.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        The try clause surrounding MakeDict was to catch
#                    a type error in the reference dbResult[0]. Other
#                    code errors caused this exception to trigger and
#                    an incorrect diagnostic was gererated. Moved the
#                    single piece to be tested out of the MakeDict call
#                    so bugs in MakeDict do not trigger this exception.
# 17 Sep 2010        Initial creation for set-sensor-location command.

##############################################################################
# find_location
# Entry: aSensorLocation - the host name to use for the sensor location.
# Exit: result is the record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def find_location(aLocation):

    global sensorLocations

    # build our query.
    myWhere = "name = '" + aLocation + "'"
    result = db_access.get_table_rows(sensorLocations, myWhere)

    if ( len(result) == 0 ):
        raise NetException("LocationNotFound", "the sensor " +
                aLocation['name'] + " was not found.")

    return(result)
## end find_location()


# vim: syntax=python ts=4 sw=4 showmatch et :

