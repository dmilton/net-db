#

# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Correct PgQuery call to identify the user and
#                    password variables rather than by position.
# 17 Sep 2010        Initial creation for set-sensor-location command.

##############################################################################
# update_location
# Entry: aLocation - the information to update.
# Exit: result is the updated record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def update_location(theLocation):

    global sensorLocations;

    # Find the location. If it doesn't exist this fails.
    thisLocation = find_location(theLocation);

    # Build the update query.
    dbTableInfo = sensorLocations['sensor_locations'];
    myQuery = db_access.UpdateQuery(dbTableInfo['table'], \
            theLocation, sensorLocations, dbTableInfo['index']);

    # Perform the update.
    dbResult = db_access.PgQuery(myQuery, \
            dbName=sensorLocations['sensor_locations']['database'],
            dbUser=sensorLocations['sensor_locations']['role'],
            dbPass=sensorLocations['sensor_locations']['pass']);

    # Fetch the updated record.
    result = find_location(theLocation);

    return(result);
## end update_location()


# vim: syntax=python ts=4 sw=4 showmatch et :

