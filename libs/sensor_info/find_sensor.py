#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building to use the WhereClause
#                    db_access call rather than building it manually.

##############################################################################
# find_sensor(theSerial)
# Find a sensor in the database and return the resulting record.
# Entry: theSerial - the serial number of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensor(theSerial):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    result = {}

    # build our where clause.
    myWhere = "serial_nbr = '" + theSerial + "'"
    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorSerialNbr", theSerial)

    return(result)
## end find_sensor()


# vim: syntax=python ts=4 sw=4 showmatch et :

