#

# 24 May 2013        rewrite to use new db_access.
# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010         Initial creation for set-sensor-location command.

##############################################################################
# add_location
# Entry: aSensorLocation - the host name to use for the sensor location.
# Exit: result is the record as found in the database.
# Exception: If no sensor location is found, an exception is returned.

def add_location(newLocation):

    global _sensorLocations

    myWhere = "name = '" + newLocation['name'] + "'"
    result = db_access.set_table_row(_sensorLocations, newLocation, myWhere)

    return(result)
## end add_location()

# vim: syntax=python ts=4 sw=4 showmatch et :

