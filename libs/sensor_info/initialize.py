#

# 24 May 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _sensorDetail, _sensorLocations, _sensorView
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _sensorDetail.update(db_access.init_db_table(_sensorDetail))
    _sensorLocations.update(db_access.init_db_table(_sensorLocations))
    _sensorView.update(db_access.init_db_table(_sensorView))

    _tables_initialized = True

## end initialize()


# vim: syntax=python ts=4 sw=4 showmatch et :

