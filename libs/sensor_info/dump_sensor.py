#

# 21 Mar 2013        reformat to use tabs.
# 19 Aug 2010        Add the ability to print a header for the
#                    fields being output.
# 18 Aug 2010        Fix problems with dump_sensor and 'null' fields where
#                    a sensor has no location information. ie. inventoried
#                    but not installed or removed/returned/failed.
# 16 Aug 2010        Create dump_sensor routine to produce CSV format output.

##############################################################################
# dump_sensor(theSensor)
# Entry: theSensor - sensor record to display for the user.
#         dumpFields - list of fields to dump.
#         printHeader - print a header consisting of the fields being dumped.
# Exit: 
#

def dump_sensor(theSensor, dumpFields, printHeader):

    global sensorView

    if ( len(theSensor) > 0 ):
        first = True
        for aField in dumpFields:
            if ( aField in theSensor ):
                theField = \
                    output_format(theSensor[aField], sensorView[aField]['type'])
            else:
                theField = "NULL"
            ## end else if ( theSensor.has_key(aField) == True ):
            if ( first == True ):
                dumpLine = theField
                if ( printHeader == True ):
                    headLine = aField
                ## end if ( printHeader == True ):
                first = False
            else:
                dumpLine = dumpLine + "," + theField
                if ( printHeader == True ):
                    headLine = headLine + "," + aField
                ## end if ( printHeader == True ):
            ## end else if ( first == True ):
        ## end for aField in dumpFields:
    ## end if ( len(theSensor) > 0 ):

    # Now that we have our output lines, print them.
    if ( printHeader == True ):
        print(headLine)
    ## end if ( printHeader == True ):
    print(dumpLine)

## end dump_sensor()


# vim: syntax=python ts=4 sw=4 showmatch et :

