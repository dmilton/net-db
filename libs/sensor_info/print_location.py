#

# 21 Mar 2013        reformat to use tabs.
# 17 Sep 2010        Initial creation.

##############################################################################
# print_location(theLocation)
# Entry:
#        theLocation = location record to print.
# Exit: the information present in the location record has been printed.

def print_location(theLocation):

    print(("Sensor name: " + theLocation['name']))
    if ( 'sensor' in theLocation ):
        sensor = "Sensor Serial Number: " + theLocation['sensor']
    else:
        sensor = "Sensor Serial Number: Not Set"
    print(sensor)

    if ( 'location' in theLocation ):
        location = "Location: " + theLocation['location']
    else:
        location = "Location: Not Set"
    print(location)

    if ( 'notes' in theLocation ):
        notes = "Notes: " + theLocation['notes']
    else:
        notes = "Notes: Not Set"
    print(notes)

## end print_location():


# vim: syntax=python ts=4 sw=4 showmatch et :

