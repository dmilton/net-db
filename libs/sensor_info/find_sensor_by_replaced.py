#

# 21 Mar 2013        reformat to use tabs.
#  9 Sep 2010        Change query building so the where part is a
#                    simple expression.

##############################################################################
# find_sensorByReplaced(theSerial)
# Find a sensor in the database and return the resulting record.
# Entry: theSerial - the csid of the sensor to find.
# Exit: sensor record if found, exception if not.

def find_sensorByReplaced(theSerial):
    """
    Find a sensor in the sensor table.
    """

    global _sensorDetail

    myWhere = "replaced_by '" + theSerial + "'"

    result = db_access.get_table_rows(_sensorDetail, myWhere)

    if ( len(result) == 0 ):
        raise NetException("UnknownSensorName", theName)

    return(result)
## end find_sensorByReplaced()


# vim: syntax=python ts=4 sw=4 showmatch et :

