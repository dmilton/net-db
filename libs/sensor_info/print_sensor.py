#

# 21 Mar 2013        reformat to use tabs.
#  1 Sep 2010        Change the maintenance contract information printing
#                    to use the maint-status value. Add the printing of
#                    rma_sensor when one exists.
# 31 Aug 2010        Add printing of the removal reason when
#                    the field has been set.

##############################################################################
# print_sensor(theSensor)
# Entry: theSensor - sensor record to display for the user.
# Exit: 

def print_sensor(theSensor):

    if ( len(theSensor) > 0 ):
        print("Sensor Information:")

        # Start with the information from sensor_detail
        if ( 'name' in theSensor ):
            serialString = "Serial Number (name): " + \
                    theSensor['serial-nbr'] + " (" + theSensor['name'] + ")"
        else:
            serialString = "Serial Number: " + theSensor['serial-nbr']
        print(serialString)

        if ( 'model' in theSensor ):
            modelString = "Model: " + theSensor['model']
        else:
            modelString = "Model: NOT SET."
        print(modelString)

        macString = "LAN/WLAN MAC Address: "
        if ( 'lan-mac' in theSensor ):
           macString = macString + theSensor['lan-mac']
        else:
           macString = macString + "NULL"
        if ( 'wlan-mac' in theSensor ):
           macString = macString + ' / ' + theSensor['wlan-mac']
        else:
           macString = macString + " / NULL"
        print(macString)

        flagString = "Active/Maintenance State: "
        if ( 'active' in theSensor ):
            flagString = flagString + str(theSensor['active'])
        else:
            flagString = flagString + "NULL"

        if ( 'maint-state' in theSensor ):
            if ( theSensor['maint-state'] == 'add' ):
                flagString = flagString + " / Add to next years contract."
            elif ( theSensor['maint-state'] == 'on' ):
                flagString = flagString + " / On contract."
            elif ( theSensor['maint-state'] == 'remove' ):
                flagString = flagString + " / Remove from contract."
            elif ( theSensor['maint-state'] == 'off' ):
                flagString = flagString + " / Sent back to manfacturer."
            else:
                flagString = flagString + "unknown state: " + \
                        theSensor['maint-state']
            ## end elif ( theSensor['maint-state'] == 'XXX' ):
        else:
            flagString = flagString + " / NULL"
        print(flagString)

        csidString = "CSID: "
        if ( 'csid' in theSensor ):
            csidString = csidString + str(theSensor['csid'])
        else:
            csidString = csidString + "NULL"
        print(csidString)

        poString = "Purchase Order: "
        if ( 'po' in theSensor ):
            poString = poString + str(theSensor['po'])
        else:
            poString = poString + "NULL"
        print(poString)

        serialKeyString = "Serial Key: "
        if ( 'serial-key' in theSensor ):
            serialKeyString = serialKeyString + str(theSensor['serial-key'])
        else:
            serialKeyString = serialKeyString + "NULL"
        print(serialKeyString)

        installString = "Installed on: "
        if ( 'install-date' in theSensor ):
            installString = installString + str(theSensor['install-date'])
        else:
            installString = installString + "NULL"
        print(installString)

        if ( 'campusid' in theSensor ):
            print(("Inventoried for the " + \
                    theSensor['campus-name'] + " campus."))

        if ( 'replaced-by' in theSensor ):
            print(("Removed from service on: " + \
                    str(theSensor['removal-date'])))
            print(("Replaced by: " + str(theSensor['replaced-by'])))
            if ( 'removal-reason' in theSensor ):
                print(("Removal Reason:" + str(theSensor['removal-reason'])))

        if ( 'rma-sensor' in theSensor ):
            print(("This sensor was RMA'd to factory and the replacement is: "\
                    + theSensor['rma-sensor']))
        
        # Add the information from sensor_locations
        if ( 'building-name' in theSensor ):
            print(("Building: " + theSensor['building-name']))

        if ( 'location' in theSensor ):
            print(("Location: " + theSensor['location']))

    ## end if ( len(theSensor) > 0 ):

## end print_sensor()


# vim: syntax=python ts=4 sw=4 showmatch et :

