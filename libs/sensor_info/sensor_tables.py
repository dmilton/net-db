#\

# 24 May 2013        Initial creation.

###############################################################################
# sensorDetail table.
#
_sensorDetail = {
    'database': 'networking',
    'table': 'sensor_detail',
    'role': 'netmon',
    'index': 'serial-nbr'
}

###############################################################################
# The sensor locations table.
#
_sensorLocations = {
    'database': 'networking',
    'table': 'sensor_locations',
    'role': 'netmon',
    'index': 'name'
}

###############################################################################
# Sensors View
#
_sensorView = {
    'database': 'networking',
    'table': 'sensors',
    'role': 'netview',
    'index': 'serial-nbr'
}


# vim: syntax=python ts=4 sw=4 showmatch et :

