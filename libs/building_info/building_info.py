#
g_version='16 Apr 2015 - 5f805f4'

# Libraries used in this module.
import db_access
from network_error import NetException
from network_zones import get_zone_desc
from building_types import verify_building_type

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Building Campus table.
#
global _campus_locations
_campus_locations = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'campus_locations',
    'index': 'campus_id'
};

###############################################################################
# Building Detail table.
#
global _building_detail
_building_detail = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'building_detail',
    'index': 'bldg_code'
};

###############################################################################
# Building Voice/Data Rooms Table
#
global _voice_data_rooms
_voice_data_rooms = {
    'database': 'networking',
    'table': 'voice_data_rooms',
    'role': 'netmon',
    'index': 'room_id'
};

#

# 13 Nov 2012   Change to use internal variables (_ prefix) for the
#               tables. Add note that the database itself must be
#               initialized.
#  5 Nov 2012   initial writing

##############################################################################
# initialize()
# Perform initialization process for building information tables. 
# Note that the parent routine must have initialized the networking database
# in order for this routine to work.
# Entry: none. initializes internal variables for use.
# Exit: building information tables are now ready to use.

def initialize():
    """
    initialize the building table access field variables.
    """

    # Our view and associated tables.
    global _campus_locations, _building_detail, _voice_data_rooms
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Note that the parent routine must have initialized the
    # database (networking) in order for this routine to work.
    # initialize the fieldList (for SELECT) and keyList (query dictionary)

    _campus_locations.update(db_access.init_db_table(_campus_locations))

    _building_detail.update(db_access.init_db_table(_building_detail))

    _voice_data_rooms.update(db_access.init_db_table(_voice_data_rooms))

    _tables_initialized = True

## end initialize()


#

# 19 Nov 2014   Change the addition of building_campus table info so that
#               all fields are added.
# 15 Aug 2014   Expand the error detail to make diagnostics to the user
#               easier to understand.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 21 Jun 2013   Update to use two table queries so the buildings view
#               is not required for the code to work.
#  7 Jun 2012   Add new fields.
# 20 Dec 2010   changed to use db_access module for query generation
#               and making the dictionary result.
#               initial creation, copied from network.py

############################################################################
# get_building_info
#
# Input: bldg_code - the building code to get information about.
# Output: dictionary of information regarding the specific building.
# Exception: UnknownBuildingCode.
#
def get_building_info(bldg_code):
    """
    Check the database for a building code.
    try:
        bldg_info = get_building_info(bldg_code)
    except:
        print("unknown building code")
    """

    global _building_detail, _campus_locations

    try:
        # Get the building_detail table entry.
        my_where = "bldg_code = '" + bldg_code + "'"
        result = db_access.get_table_rows(_building_detail, my_where)[0]
        # Add the corresponding building_campus table entry.
        my_where = "campus_id = " + str(result['campus_id'])
        campus_info = db_access.get_table_rows(_campus_locations, my_where)[0]
        for a_key in list(campus_info.keys()):
            result[a_key] = campus_info[a_key]
    except TypeError:
        # or dbResult does not have a length - it is an integer.
        raise NetException("UnknownBuildingCode", 
                "Invalid building code '" + bldg_code + "'")
    except IndexError:
        # we get here from the dbResult[0] since the result is not an array.
        raise NetException("UnknownBuildingCode", 
                "Invalid building code '" + bldg_code + "'")
    except Exception:
        # something unexpected happened, raise it so we can debug the problem.
        raise
    ## end try

    return(result)
## end get_building_info()


#

# 17 Jun 2014   Add check for campus_id > 0 so we can get a list of
#               all defined buildings.
# 15 May 2013   initial creation

############################################################################
# get_campus_buildings
#
# Input: campus_id - the campus-id to get a list of buildings for.
# Output: list of building info dictionaries.
#
def get_campus_buildings(campus_id):
    """
    Get the list of building on the specified campus.
    Using -1 for the campus_id will get a list of all buildings.
    bldg_list = building_info.get_campus_buildings(campus_id)
    """

    global _building_detail

    if ( campus_id > 0 ):
        my_where = "campus_id = " + str(campus_id)
    else:
        my_where = ""

    my_order = "campus_id, bldg_code"

    result = db_access.get_table_rows(_building_detail, my_where, my_order)

    return(result)
## end get_campus_buildings()


#

# 20 May 2014   initial creation

############################################################################
# get_zone_buildings
#
# Input: net_zone - the network zone to get a list of buildings for.
# Output: list of building info dictionaries.
#
def get_zone_buildings(net_zone):
    """
    Get the list of building using net_zone address spaces.
    bldg_list = building_info.get_zone_buildings(net_zone)
    """

    global _building_detail

    my_where = "net_zone = " + str(net_zone)

    result = db_access.get_table_rows(_building_detail, my_where)

    return(result)
## end get_zone_buildings()


#

# 10 Sep 2014   Sort output by building code.
# 18 Jun 2014   add check of both bldg_name and bldg_code for the string
#               to catch instances of where a building code is used
#               with --find. Remove refenece to the building view and
#               use the building detail table like everything else.
#  4 Jun 2014   change building_detail column name to bldg_name.
#  6 Nov 2012   rewrite to use new db_access routines that return dictionaries.
# 19 Oct 2012   missed the extraction of the field name from the
#               table field dictionary which caused errors.
#               PgQuery was missing the db_access. prefix.
# 31 May 2012   Reformat using tabs instead of spaces and tabs.
# 21 Feb 2012   initial creation.

############################################################################
# find_building_code
#
# Entry: name - the name of the building to search for.
# Exit: A list of building info dictionaries.
# Exception: UnknownBuilding.
#
def find_building_code(bldg_name):
    """
    Check the database for buildings that match the given name. 
    try:
        bldg_info = find_building_code(bldg_name)
    except:
        print("unknown building name")
    """

    global _building_detail

    # build the appropriate were clause to find this building.
    my_where = "LOWER(bldg_name) LIKE LOWER(\'%" + bldg_name + "%\')"
    my_where += " OR LOWER(bldg_code) LIKE LOWER(\'%" + bldg_name + "%\')"

    # Now grab the appropriate row from the table.
    db_result = db_access.get_table_rows(_building_detail, my_where)

    if ( len(db_result) == 0 ):
        raise NetException("UnknownBuilding", bldg_name)

    # Sort the list by building code.
    result = sorted( db_result , key=lambda elem: elem['bldg_code'] )

    return(result)
## end find_building_code()


#\L

# 17 Apr 2013   initial creation.

############################################################################
# find_router_buildings
# Entry: the_router - which router to search buildings for.
# Exit: A search of all buildings is made, looking for the specified router.
#        All building which use that router are returned.

def find_router_buildings(the_router):
    global _building_detail

    my_where = "router = '" + the_router + "'"
    result = db_access.get_table_rows(_building_detail, my_where)

    if ( len(result) == 0 ):
        raise NetException("UnknownBuilding", \
            "could not find " + the_router + " listed in any building." )

    return(result)
## end find_router_buildings()


#\L

# 21 Mar 2013   define result.
# 18 Mar 2013   initial creation.

############################################################################
# get_building_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_building_fields():

    global _building_detail

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_building_detail)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_building_fields()


#\L

# 14 Mar 2013   initial creation.

############################################################################
# new_building
# Create a new empty template for a building.
def new_building():

    global _building_detail

    # Do something here.
    result = db_access.new_table_row(_building_detail)

    return(result)
## end new_building()


#

# 23 Sep 2013   set_table_rows now returns a list (via RETURNING clause)
#               but the result here should be a single item.
# 15 Apr 2013   get_table_rows returs a list but this function always
#               returns a single building entry.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
# 26 Feb 2013   add check to ensure bldg_info has all of the keys
#               that are in a building_detail record and delete
#               the keys in the new record that aren't in bldg_info.
#               Remove the lower case compare so that a case sensitive
#               building code comparison can take place.
#  6 Nov 2012   Rewrite to use db_access.
# 19 Oct 2012   The PgQuery call was missing db_access, changed
#               indentation to use tabs exclusively.
# 10 Dec 2010   Changed to use db_access library for query generation.
#               Initial creation, copied from network_rw.py

##############################################################################
# add_building(bldg_info)
# Add a new building to the building_detail table.
# Entry:    bldg_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_building(bldg_info):

    global _building_detail

    # Create a new record.
    new_bldg = db_access.new_table_row(_building_detail)

    # Copy matching values from bldg_info into our new record.
    # This eliminates any extraneous vaules in bldg_info.
    for a_key in list(new_bldg.keys()):
        if ( a_key in bldg_info ):
            new_bldg[a_key] = bldg_info[a_key]
        else:
            del new_bldg[a_key]

    # Build a where clause to identify this building.
    my_where = "bldg_code = \'" + new_bldg['bldg_code'] + "\'"

    # Add the record into the database.
    result = db_access.set_table_row(_building_detail, new_bldg, my_where)[0]

    return(result)
## end add_building():


#

# 23 Sep 2013   set_table_row now returns a list (using RETURNING clause)
#               so dereference list to a single item.
# 15 Apr 2013   get_table_rows returns a list but this function always
#               return a single building entry.
#  8 Apr 2013   db_access.set_table_row no longer returns a result so
#               a call to get_table_rows is required.
# 15 Mar 2013   Improve checks to eliminate keys that have no value.
# 26 Feb 2013   Add checks to eliminate keys in the detail record
#               that aren't in teh bldg_info record we received.
#               Remove LOWER so case sensitive checks can be made.
#  6 Nov 2012   Rewrite to use dictionary based db_access routines.
# 31 May 2012   initial creation.

##############################################################################
# update_building(theInfo)
# Update a building in the building_detail table.
# Entry:    theInfo building_detail dictionary
# Exit:        the table row as updated is returned.

def update_building(bldg_info):

    global _building_detail

    # Get the current record.
    my_where = "bldg_code = '" + bldg_info['bldg_code'] + "'"
    curr_bldg_list = db_access.get_table_rows(_building_detail, my_where)
    if ( len(curr_bldg_list) == 0 ):
        raise NetException("UnknownBuildingCode", bldg_info['bldg_code'])
    else:
        curr_bldg = curr_bldg_list[0]

    # Create a new empty record.
    update_info = db_access.new_table_row(_building_detail)

    # Copy matching values from bldg_info into our new record.
    # This eliminates extraneous vaules in bldg_info.
    for a_key in list(update_info.keys()):
        key_exists = a_key in bldg_info
        if ( key_exists == True ):
            key_not_none = ( bldg_info[a_key] != None )
            key_not_dup = (bldg_info[a_key] != curr_bldg[a_key])
        if ( key_exists and key_not_none and key_not_dup ):
            update_info[a_key] = bldg_info[a_key]
        else:
            del update_info[a_key]

    # Update the record in the database.
    result = db_access.set_table_row(_building_detail, update_info, my_where)[0]

    return(result)
## end update_building():


#

# 31 May 2012   initial creation.

##############################################################################
# change_zones(theInfo)
# Update a building in the building_detail table.
# Entry:    theInfo building_detail dictionary
# Exit:        the table row as updated is returned.

def change_zones(old_zone, new_zone):

    global _building_detail

    # Get the current record.
    my_where = "net_zone = " + str(old_zone)
    nbr_bldgs_in_zone = db_access.get_row_count(_building_detail, my_where)
    if ( nbr_bldgs_in_zone == 0 ):
        raise NetException("NoZone",
                "No buildings were found in zone " + str(old_zone))

    # We found some buildings to update.
    update_row_count = db_access.change_table_rows(
            _building_detail, 'net_zone', old_zone, new_zone, False)

    if ( update_row_count != nbr_bldgs_in_zone ):
        raise NetException("UpdateFailure",
                "for some reason " + str(update_row_count) +
                " rows were updated but " + str(nbr_bldgs_in_zone) +
                " were originally found.")

    return(update_row_count)
## end change_zones():


#

#  9 Sep 2014   Add exception for deleting zero buildings.
# 18 Sep 2013   initial creation

##############################################################################
# delete_building(bldg_info)
# Delete the building record. All dependancies must be removed before calling
# this routine. That means any allocated networks must be deleted or returned
# to the free pool.
# Entry:    bldg_info - dictionary of values from the building.
# Exit:    all rooms associated with this building, and the building row itself
#    have been removed from the database.

def delete_building(bldg_info):

    global _building_detail, _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "bldg_code = \'" + bldg_info['bldg_code'] + "\'"

    # Delete the voice/data rooms in the building.
    ignore = db_access.delete_table_rows(_voice_data_rooms, my_where)

    # Delete the building row.
    rows = db_access.delete_table_rows(_building_detail, my_where)
    if ( rows == 0 ):
        raise NetException("UnknownBuildingCode", 
                "Building with code" + bldg_info['bldg_code'] + "not found.")

    return
## end delete_building():


#

# 16 Apr 2015   Initial creation

##############################################################################
# _check_building(campus_info)
# Verify that the campus info is unique.
# Entry:    campus_info - dictionary of values from the building.
# Exit:     the location (and id if provided) is verified to be unique.
#    if the values are not unique an exception is raised.

def _check_building(building_info):

    global _campus_locations, _building_detail

    # Check that we have a unique campus id if provided
    if ( 'campus_id' in building_info ):
        my_where = "campus_id = '" + campus_info['campus_id'] + "'"
        id_count = db_access.get_row_count(_campus_locations, my_where)
        if ( id_count == 0 ):
            raise NetException("InvalidCampusId",
                    "The campus ID " + building_info['campus_id'] + \
                    " does not exist.")

    # Check that the building type is valid.
    if ( 'bldg_type' in building_info ):
        valid = building_types.verify_building_type(building_info['bldg_type'])
        if ( not valid ):
            raise NetException("InvalidBldgType",
                    "The building type '" + building_info['bldg_type'] +
                    "' does not exist.")

    # Check that the network zone is valid.
    zone_desc = network_zones.get_zone_descr(building_info['net_zone'])

    # Check that we have a unique building code.
    my_where = "bldg_code = '" + building_info['bldg_code'] + "'"
    bldg_code_count = db_access.get_row_count(_building_detail, my_where)
    if ( bldg_code_count > 0 ):
        raise NetException("BldgCodeExists",
                "The building code '" + building_info['bldg_code'] + \
                "' already exists.")

    return
## end _check_building():


#\L

# 23 Aug 2014   Default to only showing campus names with an id > 0
#               to hide the two internal ones. Add flag to permit
#               display of the default too.
#               list doesn't display extraneous information.
# 17 Jun 2014   Check for campus_id > 0 rather than != 0 so we can
#               pass a negative value as well as zero.
# 14 May 2013   initial creation.

############################################################################
# get_campus_info
# Given a campus_id, extract information from the database about that campus.
# Entry:
#       campus_id   the campus id to obtain information on.
#       show_any    if true, show any campus location, including internal ones.
#       
# Exit: A list of campus records is returned.

def get_campus_info(campus_id, show_any):

    global _campus_locations

    # Default campus is id 0, only show that when specifically requested.
    if ( show_any or campus_id >= 0 ):
        my_where = "campus_id = " + str(campus_id)
    else:
        my_where = "campus_id > 0"
    my_order = 'campus_id'
    result = db_access.get_table_rows(_campus_locations, my_where, my_order)

    return(result)
## end get_campus_info()


#\L

# 19 Sep 2013   initial creation.

############################################################################
# get_campus_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_campus_fields():
    global _campus_locations

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_campus_locations)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_campus_fields()


#\L

# 19 Sep 2013   initial creation.

############################################################################
# new_campus
# Create a new empty template for a building.
def new_campus():

    global _campus_locations

    # Do something here.
    result = db_access.new_table_row(_campus_locations)

    return(result)
## end new_campus()


#

# 27 Nov 2013   Add checks for duplicate id.
# 20 Sep 2013   set_table_row now returns a result after the insert or
#               update query has completed. The get_table_rows call is no
#               longer necessary.
# 18 Sep 2013   Initial creation

##############################################################################
# add_campus(campus_info)
# Add a new building to the building_detail table.
# Entry:    campus_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_campus(campus_info):

    global _campus_locations

    # Create a new record.
    new_campus = db_access.new_table_row(_campus_locations)

    # Copy matching values from campus_info into our new record.
    # This eliminates any extraneous vaules in campus_info.
    for a_key in list(new_campus.keys()):
        if ( a_key in campus_info ):
            new_campus[a_key] = campus_info[a_key]
        else:
            del new_campus[a_key]
        ## end else if ( a_key in campus_info ):
    ## end for a_key in new_campus.keys():

    # Check what we have - name (and id if provided) must be unique.
    _check_campus(new_campus)

    # Build a where clause to identify this building.
    my_where = ""

    # Add the record into the database.
    result = db_access.set_table_row(_campus_locations, new_campus, my_where)[0]

    return(result)
## end add_campus():


#

# 25 Sep 2014   Check the campus exists and raise an exception if not.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 27 Nov 2013   Add check to ensure the location is unique.
# 18 Sep 2013   Initial creation

##############################################################################
# update_campus(campus_info)
# Add a new building to the building_detail table.
# Entry:    campus_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def update_campus(campus_info):

    global _campus_locations

    # Build a where clause to identify this building.
    my_where = "campus_id = " + str(campus_info['campus_id'])

    # Check the name is unique
    name_check = {'campus_name':campus_info['campus_name']}
    _check_campus(name_check)

    # Make sure the record exists:
    row_count = db_access.get_row_count(_campus_locations, my_where)

    if ( row_count == 0 ):
        raise NetException("not found",
                "The campus ID " + str(campus_info['campus_id']) +
                " was not found in the database.")

    # Update the record in the database.
    result = db_access.set_table_row(
            _campus_locations, campus_info, my_where)[0]

    return(result)
## end update_campus():


#

#  9 Sep 2014   Add exception for a delete that deleted zero rows.
# 18 Sep 2013   Initial creation

##############################################################################
# delete_campus(campus_info)
# Delete the campus record. All dependencies must be resolved BEFORE calling.
# Entry:
#       campus_info - dictionary of values from the building.
# Exit:
#       a new building record has been added to the database.
# Exceptions:
#       A UnknownCampusID NetException is raised if no rows are deleted.

def delete_campus(campus_info):

    global _campus_locations

    # Build a where clause to identify this building.
    my_where = "campus_id = " + str(campus_info['campus_id'])

    # Delete the record, cascade to get other rows dependant on this one.
    rows = db_access.delete_table_rows(_campus_locations, my_where)
    if ( rows == 0 ):
        raise NetException("UnknownCampusID", 
            "Campus with ID " + str(campus_info['campus_id']) + " not found.")

    return
## end delete_campus():


#

# 11 Aug 2013   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 27 Nov 2013   Initial creation

##############################################################################
# _check_campus(campus_info)
# Verify that the campus info is unique.
# Entry:    campus_info - dictionary of values from the building.
# Exit:     the location (and id if provided) is verified to be unique.
#    if the values are not unique an exception is raised.

def _check_campus(campus_info):

    global _campus_locations

    # Check that we have a unique name
    my_where = "lower(campus_name) = lower('" 
    my_where += campus_info['campus_name'] + "')"
    name_count = db_access.get_row_count(_campus_locations, my_where)
    if ( name_count > 0 ):
        raise NetException("duplicate",
                "A campus named '" + campus_info['campus_name'] + \
                "' already exists.")

    # Check that we have a unique campus id if provided
    if ( 'campus_id' in campus_info ):
        my_where = "campus_id = '" + campus_info['campus_id'] + "'"
        id_count = db_access.get_row_count(_campus_locations, my_where)
        if ( id_count > 0 ):
            raise NetException("duplicate",
                    "The campus ID " + campus_info['campus_id'] + \
                    " already exists.")

    return
## end _check_campus():


#

#  7 Jun 2012   Update to use generic db_access routines.
# 31 May 2012   initial creation.

############################################################################
# get_vd_rooms
#
# Input: bldg_code - building to obtain list of V/D rooms for.
# Output: list of V/D rooms in the building.
#
def get_vd_rooms(bldg_code):
    """
    Get the list of voice data rooms in the building.
    vd_room_list = get_vd_rooms(bldg_code)
    """

    global _voice_data_rooms

    my_where = "bldg_code = '" + bldg_code + "'"

    # Now construct the full select query for this building code.
    db_result = db_access.get_table_rows(_voice_data_rooms, my_where);

    return db_result
## end get_vd_rooms()


#\L

#  6 Dec 2013   initial creation.

############################################################################
# get_room_info
# Given a room id, return the name of the room.
# Entry:
#    room_id - the id of the room.
# Exit:
#    if the room id is in the database the record is returned.

def get_room_info(room_id):
    global _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "room_id = " + str(room_id)

    # Update the record in the database.
    db_result = db_access.get_table_rows(_voice_data_rooms, my_where)

    if ( len(db_result) == 1 ):
        result = db_result[0]
    else:
        raise ValueError("A room with id " + str(room_id) + \
                " could not be found.")

    return(result)
## end get_room_info()


#\L

# 21 Mar 2013   define result.
# 18 Mar 2013   initial creation.

############################################################################
# get_vdroom_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to building rooms.

def get_vdroom_fields():

    global _voice_data_rooms

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_voice_data_rooms)

    # Copy the table information so we can modify it.
    for a_key in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = a_key.translate(str.maketrans('-','_'))
        result[key2] = table_info[a_key]

    return(result)
## end get_vdroom_fields()


#

# 28 Nov 2013   Add call to _check_vd_room to verify what we are given.
# 11 Oct 2013   Initial creation

##############################################################################
# add_vd_room(room_info)
# Add a new building to the building_detail table.
# Entry:    room_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_vd_room(room_info):

    global _voice_data_rooms

    # Create a new record.
    new_room = db_access.new_table_row(_voice_data_rooms)

    # Copy matching values from room_info into our new record.
    # This eliminates any extraneous vaules in room_info.
    for a_key in list(new_room.keys()):
        if ( a_key in room_info ):
            new_room[a_key] = room_info[a_key]
        else:
            del new_room[a_key]

    _check_vd_room(new_room)

    # Add the record into the database.
    result = db_access.set_table_row(_voice_data_rooms, new_room, '')[0]

    return(result)
## end add_vd_room():


#

# 11 Oct 2013   Initial creation

##############################################################################
# update_vd_room(room_info)
# Add a new building to the building_detail table.
# Entry:    room_info - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def update_vd_room(room_info):

    global _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "room_id = " + str(room_info['room_id'])

    # Update the record in the database.
    result = db_access.set_table_row(_voice_data_rooms, room_info, my_where)[0]

    return(result)
## end update_vd_room():


#

#  9 Sep 2014   Add exception for deleting zero rows.
# 11 Oct 2013   Initial creation

##############################################################################
# delete_vd_room(room_info)
# Delete the room record. All dependencies must be resolved BEFORE calling.
# Entry:    room_info - dictionary containing the room_id to delete.
# Exit:    the room has been removed from the database.

def delete_vd_room(room_info):

    global _voice_data_rooms

    # Build a where clause to identify this building.
    my_where = "room_id = " + str(room_info['room_id'])

    # Delete the record.
    rows = db_access.delete_table_rows(_voice_data_rooms, my_where)
    if ( rows == 0 ):
        raise NetException("UnknownRoomID",
            "Unable to delete room: " + str(room_info))

    return
## end delete_vd_room():


#

# 28 Nov 2013   Initial creation

##############################################################################
# _check_vd_room(room_info)
# Verify that the campus info is unique.
# Entry:    room_info - dictionary of values from the building.
# Exit:    the location (and id if provided) is verified to be unique.
#    if the values are not unique an exception is raised.

def _check_vd_room(room_info):

    global _voice_data_rooms

    # Check that we have a unique name
    my_where = "lower(room) = lower('" + room_info['room'] + "')"
    my_where += " AND bldg_code = '" + room_info['bldg_code'] + "'"
    room_count = db_access.get_row_count(_voice_data_rooms, my_where)
    if ( room_count > 0 ):
        raise ValueError("Voice/Data Room " + room_info['room'] + \
            " already exists" + " in building " + room_info['bldg_code'])

    return
## end _check_vd_room():


# vim: syntax=python ts=4 sw=4 showmatch et :
