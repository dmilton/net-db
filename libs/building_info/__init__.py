#
g_version=''

# Libraries used in this module.
import db_access
from network_error import NetException
from network_zones import get_zone_desc
from building_types import verify_building_type

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Building Campus table.
#
global _campus_locations
_campus_locations = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'campus_locations',
    'index': 'campus_id'
};

###############################################################################
# Building Detail table.
#
global _building_detail
_building_detail = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'building_detail',
    'index': 'bldg_code'
};

###############################################################################
# Building Voice/Data Rooms Table
#
global _voice_data_rooms
_voice_data_rooms = {
    'database': 'networking',
    'table': 'voice_data_rooms',
    'role': 'netmon',
    'index': 'room_id'
};

# vim: syntax=python ts=4 sw=4 showmatch et :
