#
g_version=''

from network_error import NetException

from textwrap import wrap
from getpass import getuser

# All entry types must be in this list of the log command will bail. This
# will prevent getting log entry types into the database which we don't
# know how to format for output.
logEntryTypes = [ 'netlog', 'unixlog', 'phonelog' ]

# Size of our output 'window'
pgWidth = 76
pgLines = 24

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

# vim: syntax=python ts=4 sw=4 showmatch et :
