#\

# 20 Feb 2010        Initial creation.

###############################################################################
# Network Types table.
#
_networkLog = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'network_log',
    'index': 'who, logtime'
}

# All entry types must be in this list of the log command will bail. This
# will prevent getting log entry types into the database which we don't
# know how to format for output.
logEntryTypes = [ 'up', 'down', 'config', 'netlog', 'unixlog', 'phonelog' ]


# vim: syntax=python ts=4 sw=4 showmatch et :

