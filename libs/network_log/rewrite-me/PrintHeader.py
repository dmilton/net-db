#
############################################################################
# PrintHeader(who, logtime):
#   Print a log entry header.

def PrintHeader(who, logtime):
    debug.debug(1, "PrintHeader: entry")

    # print the header itself...
    print"------    Done by %s on %s     ------" % (who, logtime)

    # print a blank line after the header...
    print ""

    debug.debug(1, "PrintHeader: exit")
## end PrintHeader()

