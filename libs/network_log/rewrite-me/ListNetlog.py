#
############################################################################
# ListNetlog(start, count)
#   List entries in the activity log. Start at entry 'start' and go
#   for 'count' entries. If who is provided, restrict to entries for that
#   specific user. If date is provided, restrict entries to that day.

def ListNetlog(start, count, who="", date=""):
    debug.debug(1, "ListNetlog: entry")
    debug.debug(1, "ListNetlog: start=%s, count=%s" % (start, count))

    global pgDateFormat

    # What fields do we want and from what table...
    q1 = 'SELECT who, ' + pgDateFormat % ('logtime') + \
	    ', note FROM network_log'

    # Select only network change entries...
    q2 = 'WHERE logtype = \'netlog\''
    if ( who != "" and date == "" ):
	q2 = q2 + ' AND who=\'%s\'' % (who)
    elif ( who == "" and date != "" ):
	q2 = q2 + ' AND date=\'%s\'' % (date)
    elif ( who != "" and date != "" ):
	q2 = ' AND who=\'%s\' AND date=\'%s\'' % (who, date)
    ## end if ( who != "" and date == "" )

    # Set the order...
    q3 = 'ORDER BY logtime DESC'

    # Limit the number of entries and set where we want to start.
    if ( count > 0 and start == 0 ):
	q4 = 'LIMIT %s' % (count)
    elif ( count == 0 and start > 0 ):
	q4 = 'OFFSET %s' % (offset)
    elif ( count > 0 and start > 0 ):
	q4 = 'LIMIT %s OFFSET %s' % (count, start)
    else: # ( count == 0 and start == 0 )
	q4 = ''
    ## end if ( count > 0 and start == 0 )

    # Now put it all together.
    query = q1 + " " + q2 + " " + q3 + " " + q4

    netlog = PgQuery(query, dbName="networking")

    for anEntry in netlog:
	PrintNetlog(anEntry)

    debug.debug(1, "ListNetlog: exit")
## end ListNetlog()
