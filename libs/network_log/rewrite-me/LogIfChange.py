#
############################################################################
# LogIfChange(who, switch, ifName, newState, note=""):
# Log an interface state change.
# 

def LogIfChange(who, switch, ifName, newState, note=""):
    """
    GRANT INSERT,SELECT ON network_log TO netlog;
    Make a log entry in the database to document a switch port
    change in state.
    LogIfChange(who, switch="", ifName="", newState="", note="");
	who should be the userid of the person making the change.
	switch is the host name of the device.
	ifName is the name of the switch port changed.
	newState can be one of up, down, config.
	note is free form text which describes the change.
    """
    debug.debug(1, "LogIfChange: who=" + who + ", switch=" + switch +
	    switch + ", ifName=" + ifName + ", newState=" + newState + 
	    ", note=" + note)

    # take a fully qualified host name and chop it down.
    from string import split
    switchName = split(switch, '.', 1)[0]

    q1 = 'INSERT INTO network_log'
    q3 = '(\'%s\', \'%s\', \'%s\', \'%s\'' % (who, newState, switchName, ifName)
    if ( note == "" ):
	q2='(who, logtype, switch, ifname) VALUES'
	q4 = ')'
    else:
	q2 = '(who, logtype, switch, ifname, note) VALUES'
	q4 = ', \'%s\')' % (note)
    ## end else if ( note == "" ):
    query = q1 + " " + q2 + " " + q3 + " " + q4
    PgQuery(query, dbName="networking", dbUser="netlog")

## end LogIfChange()

