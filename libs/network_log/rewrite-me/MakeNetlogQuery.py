#
############################################################################
# MakeNetlogQuery(logtype, who="")
# Input:
#	logtype - type of log message to select.
#	who - look for changes made by this user.
#	date - limit changes to after a specific date.
# Output:
#	A query suitable for extracting the specified log messages.
# Exceptions:
#	An invalid logtype.

def MakeNetlogQuery(logtype, who="", date=""):
    """
    Create a query for extracting specific log entry types from the database.
    MakeNetlogQuery(logtype);
	who should be the userid of the person making the change.
	note is free form text which describes the change.
    """
    debug.debug(1, "LogNetlog: who=%s" % who)
    debug.debug(1, "MakeNetlogQuery: logtype=" + logtype)
    debug.debug(1, "MakeNetlogQuery: who=" + who)

    global netlogFields

    # get all fields from the table.
    q1 = 'SELECT ' + netlogFields + ' FROM ' + netlogTable

    if ( logtype == "netlog" ):
	# Select only network change entries...
	q2 = 'WHERE logtype = \'netlog\''
	if ( who != "" and date == "" ):
	    q2 = q2 + ' AND who=\'%s\'' % (who)
	elif ( who == "" and date != "" ):
	    q2 = q2 + ' AND date>=\'%s\'' % (date)
	elif ( who != "" and date != "" ):
	    q2 = ' AND who=\'%s\' AND date=>\'%s\'' % (who, date)
	## end if ( who != "" and date == "" )
    elif ( logtype == "switch" ):
	# Select only switch interface change entries...
	q2 = 'WHERE ' + \
	    '(logtype = \'up\' OR logtype = \'down\' OR logtype = \'config\')'
	if ( switchName != "" and ifName != "" ):
	    q2 = q2 + ' AND switch=\'%s\' AND ifname=\'%s\'' \
		    % (switchName, ifName)
	elif ( who != "" ):
	    q2 = q2 + ' AND who = \'%s\'' % (who)
	## end if ( switchName != "" and ifName != "" )
    else:
	raise

    # Set the order...
    q3 = 'ORDER BY logtime DESC'

    # Limit the number of entries and set where we want to start.
    if ( entries > 0 and start == 0 ):
	q4 = 'LIMIT %s' % (entries)
    elif ( entries == 0 and start > 0 ):
	q4 = 'OFFSET %s' % (offset)
    elif ( entries > 0 and start > 0 ):
	q4 = 'LIMIT %s OFFSET %s' % (entries, start)
    else: # ( entries == 0 and start == 0 )
	q4 = ''
    ## end if ( entries > 0 and start == 0 )

    # Now put it all together.

## end MakeNetlogQuery()

