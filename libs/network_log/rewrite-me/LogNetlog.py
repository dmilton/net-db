#
############################################################################
# LogNetlog(who, note)
#    Create a 'netchanges' log entry.
def LogNetlog(who, note):
    """
    GRANT INSERT,SELECT ON network_log TO netlog;
    Make a log entry in the database to document a switch port
    change in state.
    LogNetlog(who, note);
	who should be the userid of the person making the change.
	note is free form text which describes the change.
    """
    debug.debug(1, "LogNetlog: who=%s" % who)
    debug.debug(1, "LogNetlog: note=%s" % note)

    query = \
	'INSERT INTO network_log (who, logtype, note) VALUES ' + \
	'(\'%s\', \'netlog\', \'%s\')' % (who, note)
    PgQuery(query, dbName="networking", dbUser="netlog")
    debug.debug(1, "LogNetlog: exit")
## end LogNetlog()

