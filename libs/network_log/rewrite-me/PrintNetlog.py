#
############################################################################
# PrintNetlog
#   A new function template. does nothing.
def PrintNetlog(theChange):
    debug.debug(1, "PrintNetlog: theChange=" + str(theChange) )

    global pgWidth
    # Fields must be selected in this order...
    # 'SELECT who, logtime, note, logtype, switch, ifname'

    # Create a log entry header line.
    PrintHeader(theChange[0], theChange[1])

    # Format and print it.
    for aLine in wrap(theChange[2], pgWidth):
	print aLine

    # Add a blank line at the end...
    print ""

    debug.debug(1, "PrintNetlog: exit")
## end PrintNetlog()

