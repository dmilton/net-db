#
############################################################################
# ListChanges(start, entries, who="", switchName="", ifName="")
#   List any interface changes from the log starting at entry 'start' and
#   going for a maximum of 'entry' entries. If ifName is provided, list
#   only entries for that interface.

def ListChanges(start, entries, who="", switchName="", ifName=""):
    debug.debug(1, "ListChanges: entry")

    global _pgDateFormat

    # What fields do we want and from what table...
    q1 = 'SELECT who, ' + pgDateFormat % ('logtime') + \
	    ', note, logtype, switch, ifname FROM network_log'

    # Do we only want specific entries?
    if ( (switchName != "" and ifName != "") or who != ""):
	q2a = 'switch=\'%s\' AND ifname=\'%s\'' % (switchName, ifName)
	q2b = 'who = \'%s\'' % (who)
    if ( switchName != "" and ifName != "" and who != ""):
	q2 = "WHERE " +  q2a + " AND " + q2b
    elif ( switchName != "" and ifName != "" ):
	q2 = "WHERE " + q2a
    elif ( who != "" ):
	q2 = "WHERE " + q2b
    else:
	q2 = ''

    # We want the order most recent first.
    q3 = 'ORDER BY logtime DESC'

    # Limit the number of entries and set where we want to start.
    if ( entries > 0 and start == 0 ):
	q4 = 'LIMIT %s' % (entries)
    elif ( entries == 0 and start > 0 ):
	q4 = 'OFFSET %s' % (start)
    elif ( entries > 0 and start > 0 ):
	q4 = 'LIMIT %s OFFSET %s' % (entries, start)
    else: # ( entries == 0 and start == 0 )
	q4 = ''
    ## end if ( entries > 0 and start == 0 )

    # Now put it all together.
    query = q1 + " " + q2 + " " + q3 + " " + q4

    activity = PgQuery(query, dbName="networking")

    # 'SELECT who, logtime, note, logtype, switch, ifname'
    for anEntry in activity:
	if ( anEntry[3] == 'netlog' ):
	    PrintNetlog(anEntry)
	else:
	    PrintIfChange(anEntry)

    debug.debug(1, "ListChanges: exit")
## end ListChanges()

