#
############################################################################
# ListIfChanges(start, entries, who="", switchName="", ifName="")
#   List any interface changes from the log starting at entry 'start' and
#   going for a maximum of 'entry' entries. If ifName is provided, list
#   only entries for that interface.

def ListIfChanges(start, entries, who="", switchName="", ifName=""):
    debug.debug(1, "ListIfChanges: entry")

    global netlogFields

    # What fields do we want and from what table...
    q1 = 'SELECT ' + netlogFields + ' FROM ' + netlogTable

    # Select only switch interface change entries...
    q2 = 'WHERE ' + \
	    '(logtype = \'up\' OR logtype = \'down\' OR logtype = \'config\')'
    if ( switchName != "" and ifName != "" ):
	q2 = q2 + ' AND switch=\'%s\' AND ifname=\'%s\'' % (switchName, ifName)
    elif ( who != "" ):
	q2 = q2 + ' AND who = \'%s\'' % (who)
    ## end if ( switchName != "" and ifName != "" )

    # Set the order...
    q3 = 'ORDER BY logtime DESC'

    # Limit the number of entries and set where we want to start.
    if ( entries > 0 and start == 0 ):
	q4 = 'LIMIT %s' % (entries)
    elif ( entries == 0 and start > 0 ):
	q4 = 'OFFSET %s' % (offset)
    elif ( entries > 0 and start > 0 ):
	q4 = 'LIMIT %s OFFSET %s' % (entries, start)
    else: # ( entries == 0 and start == 0 )
	q4 = ''
    ## end if ( entries > 0 and start == 0 )

    # Now put it all together.
    query = q1 + " " + q2 + " " + q3 + " " + q4

    activity = PgQuery(query, dbName="networking")

    # 'SELECT who, logtime, logtype, switch, ifname, note'
    for anEntry in activity:
	PrintIfChange(anEntry)

    debug.debug(1, "ListIfChanges: exit")
## end ListIfChanges()

