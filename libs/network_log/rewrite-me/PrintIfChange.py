#
############################################################################
# PrintIfChange(theChange)
#   Format and print a single interface change log entry.

def PrintIfChange(theChange):
    debug.debug(1, "PrintIfChange: theChange=%s" % (str(theChange)) )

    global pgWidth

    # Fields must be selected in this order...
    # 'SELECT who, logtime, note, logtype, switch, ifname'

    # Create a log entry header line.
    PrintHeader(theChange[0], theChange[1])

    # Now build the entry itself.
    logtype = theChange[3]
    if ( logtype == 'config' ):
	note = 'configure interface %s of %s for %s' % \
		(theChange[5], theChange[4], theChange[3])
    elif ( logtype == 'up' ):
	note = 'enable interface %s of %s' % \
		(theChange[5], theChange[4])
    elif ( logtype == 'down' ):
	note = 'disable interface %s of %s' % \
		(theChange[5], theChange[4])
    else:
	note = 'unknown state change ' + logtype + ' made ... ' + \
		str(theChange[4:])
    ## end if ( logtype == 'config' ):

    # Add any notes found...
    if ( theChange[2] != "" ):
	note = note + ' (' + theChange[2] + ')'
    ## end if ( theChange[2] != "" )

    # Format and print it.
    for aLine in wrap(note, pgWidth):
	print aLine

    # Add a blank line at the end...
    print ""

    debug.debug(1, "PrintIfChange: exit")
## end PrintIfChange()

