#
g_version='23 Jan 2014 - 1.0.21'

from network_error import NetException

from textwrap import wrap
from getpass import getuser

# All entry types must be in this list of the log command will bail. This
# will prevent getting log entry types into the database which we don't
# know how to format for output.
logEntryTypes = [ 'netlog', 'unixlog', 'phonelog' ]

# Size of our output 'window'
pgWidth = 76
pgLines = 24

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

#\

# 20 Feb 2010        Initial creation.

###############################################################################
# Network Types table.
#
_networkLog = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'network_log',
    'index': 'who, logtime'
}

# All entry types must be in this list of the log command will bail. This
# will prevent getting log entry types into the database which we don't
# know how to format for output.
logEntryTypes = [ 'up', 'down', 'config', 'netlog', 'unixlog', 'phonelog' ]



#

# 20 Feb 2013    Initial writing.

##############################################################################
# initialize()
# Perform initialization process for network log table.
# Entry: none. initializes internal variables for use.
# Exit: network log fields are now ready to use.

def initialize():
    """
    initialize the network log table access field variables.
    """

    # Tables used to maintain network assignments to buildings and links.
    global _networkLog
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Table for the various types on network address allocations.
    _networkLog.update(db_access.init_db_table(_networkLog))

    _tables_initialized = True

## end initialize()



#
############################################################################
# find_who()
#   figure out who is running this command.
# 

def find_who():
    """
    Determine the user name of the person running this command.
    """

    # Get the username from the environment...
    try:
        result = getuser()
    except:
        print("Something is wrong with your account.")
        raise
    ## end try

    return(result)
## end find_who()


# vim: syntax=python ts=4 sw=4 showmatch et :
