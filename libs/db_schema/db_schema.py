#
g_version='07 Apr 2015 - 1.0.0'

import datetime

import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Schema version table.
#

global _schemaVersion
_schemaVersion = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'schema_version',
    'index': 'schema_version'
}

#

# 27 Jan 2014   initial creation

##############################################################################
# initialize()
# Perform initialization process for the db_version table. 
# Entry: none. initializes internal variables for use.
# Exit: db_version table is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _schemaVersion
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _schemaVersion.update(db_access.init_db_table(_schemaVersion))

    _tables_initialized = True

## end initialize()


#

# 27 Jan 2014   initial creation.

############################################################################
# get_schema_version
# Extract the current schema version information from the database.
# Input:  none.
# Output: The version number is returned.
# 
def get_schema_version():
    global _schemaVersion

    # Initialize the table,
    initialize()

    # Fetch the information from the database.
    dbResult = db_access.get_table_rows(_schemaVersion, '', '')

    if ( len(dbResult) == 0 ):
        # No version found.
        result = {'minor_version': 0, 'major_version': 0}
    else:
        result = dbResult[0]
        if ( 'version' in result ):
            result['major_version'] = result['version']
            del result['version']

    return(result)
## end def get_schema_version():


#\L

# 27 Jan 2014   initial creation.

############################################################################
# set_schema_version
# Entry: none
# Exit: 
# Exceptions:

def set_schema_version(new_major,new_minor):
    global _schemaVersion

    # Get the current version.
    theVer = get_schema_version()
    if ( new_major > 0 ):
        theVer['major_version'] = new_major
    if ( new_minor > 0 ):
        theVer['minor_version'] = new_minor

    # Now set a new date.
    theVer['reference_date'] = datetime.datetime.now().strftime('%Y%j')

    return(theVer)
## end set_schema_version()


# vim: syntax=python ts=4 sw=4 showmatch et :
