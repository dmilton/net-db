#
g_version=''

import datetime

import db_access

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Schema version table.
#

global _schemaVersion
_schemaVersion = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'schema_version',
    'index': 'schema_version'
}

# vim: syntax=python ts=4 sw=4 showmatch et :
