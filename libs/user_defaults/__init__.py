#
g_version=''

import db_access
import getpass

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Default Settings Table
#
global _userDefaults
_userDefaults = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'user_info',
    'index': 'user_name'
};

## End

# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

