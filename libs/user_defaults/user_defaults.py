#
g_version='29 Sep 2014 - 1.0.7'

import db_access
import getpass

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Default Settings Table
#
global _userDefaults
_userDefaults = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'user_info',
    'index': 'user_name'
};

## End


#

#  9 Apr 2014   initial writing

##############################################################################
# initialize()
# Perform initialization process for user default information table. 
# Entry: none. initializes internal variables for use.
# Exit: user default information table is now ready to use.

def initialize():
    """
    initialize the user defaults table.
    """

    # Our view and associated tables.
    global _userDefaults
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # Note that the parent routine must have initialized the
    # database (networking) in order for this routine to work.
    # initialize the fieldList (for SELECT) and keyList (query dictionary)
    _userDefaults.update(db_access.init_db_table(_userDefaults))

    _tables_initialized = True

## end initialize()


#\L

# 19 Sep 2013        initial creation.

############################################################################
# get_defaults_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to user defaults.

def get_defaults_fields():
    global _userDefaults

    result = {}

    # Get the list of fields/input values for this table.
    tableInfo = db_access.get_table_keys(_userDefaults)

    # Copy the table information so we can modify it.
    for aKey in list(tableInfo.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = tableInfo[aKey]

    return(result)
## end get_defaults_fields()


#\L

# 26 Sep 2014   Update to accept the username as an argument.
#               Rewrite logic to allow individual list entries to
#               be changed rather than replacing the entire list.
#  9 Apr 2014   initial creation.

############################################################################
# get_defaults
# Determine the default values for a given session.
# Entry: userName - the username to retrieve defaults for.
# Exit: A list of campus records is returned.

def get_defaults(userName=getpass.getuser()):

    global _userDefaults

    # Initialie the table.
    initialize()

    # Start with an empty record.
    result = new_template()

    userNameList = ["net-defaults"]
    if ( userName != None ):
        userNameList.append( userName )

    for aName in userNameList:
        lastUserName = aName # save the last user name entry in the list.
        myWhere = "user_name = '" + aName + "'"
        updateSet = db_access.get_table_rows(_userDefaults, myWhere)
        if ( len(updateSet) == 1 ):
            updateDict = updateSet[0]
            # Override the core basics with the the db values.
            for aKey in result.keys():
                if ( aKey in updateDict ):
                    checkValue = updateDict[aKey]
                    changeValue = _update_value_check(updateDict[aKey])
                    if ( changeValue and type(checkValue) == list ):
                        if ( result[aKey] == None ):
                            # Start with a list of 'None' values.
                            result[aKey] = []
                            for aVal in checkValue:
                                result[aKey].append(None)
                        # Now update any values in the list that aren't None.
                        for valIndex in list(range(len(checkValue))):
                            changeValue = _update_value_check(
                                    checkValue[valIndex])
                            if ( changeValue ):
                                result[aKey][valIndex] = checkValue[valIndex]
                    elif ( changeValue ):
                        result[aKey] = updateDict[aKey]

    # Now force the user value.
    result['user_name'] = lastUserName

    return(result)
## end get_defaults()

def _update_value_check(checkValue):
    if ( checkValue == None ):
        changeValue = False
    elif ( type(checkValue) in [str, list] ):
        if ( len(checkValue) > 0 ):
            changeValue = True
    elif ( type(checkValue) == int ):
        if ( checkValue > 0 ):
            changeValue = True
    else:
        changeValue = True
    return( changeValue )
## end def _update_value_check()


#\L

# 27 Aug 2014        initial creation.

############################################################################
# new_template
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        user default entry in the database.

def new_template():
    global _userDefaults

    result = db_access.new_table_row(_userDefaults)

    return(result)
## end new_template()


#

# 9 Apr 2014    Initial creation

##############################################################################
# add_defaults(userDefaults)
# Add a new building to the building_detail table.
# Entry:    campusInfo - dictionary of values from the building.
# Exit:    a new building record has been added to the database.

def add_defaults(userDefaults):

    global _userDefaults

    # Create a new record.
    newDefaults = db_access.new_table_row(_userDefaults)

    # Copy matching values from campusInfo into our new record.
    # This eliminates any extraneous vaules in campusInfo.
    for aKey in list(newDefaults.keys()):
        if ( aKey in userDefaults ):
            newDefaults[aKey] = userDefaults[aKey]
        else:
            del newDefaults[aKey]
        ## end else if ( aKey in campusInfo ):
    ## end for aKey in newDefaults.keys():

    # Build a where clause to identify this default setting.
    myWhere = ""

    # Add the record into the database.
    result = db_access.set_table_row(_userDefaults, newDefaults, myWhere)[0]

    return(result)
## end add_defaults():


#

#  9 Apr 2014   initial creation

##############################################################################
# delete_defaults(defaultInfo)
# Delete the default info record.
# Entry:    defaultInfo - dictionary of values for the default settings
# Exit:        the specified default info record has been deleted.

def delete_defaults(defaultInfo):

    global _userDefaults

    # Build a where clause to identify this building.
    myWhere = "user_name = " + str(defaultInfo['user_name'])

    # Delete the record.
    db_access.delete_table_rows(_userDefaults, myWhere)

    return()
## end delete_defaults():


# vim: syntax=python ts=4 sw=4 showmatch et :
