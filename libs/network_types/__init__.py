#
g_version=''

from network_error import NetException
import db_access
from building_types import get_building_types, verify_building_type

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network Types table.
#
global _network_types
_network_types = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'network_types',
    'index': 'net_type'
}

# vim: syntax=python ts=4 sw=4 showmatch et :
