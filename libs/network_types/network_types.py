#
g_version=''

from network_error import NetException
import db_access
from building_types import get_building_types, verify_building_type

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network Types table.
#
global _network_types
_network_types = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'network_types',
    'index': 'net_type'
}

#

# 26 Apr 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _network_types
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _network_types.update( db_access.init_db_table(_network_types) )

    _tables_initialized = True

## end initialize()


#

#  9 Sep 2014   Change exception for consistiency.
# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
#  3 Jun 2014   Improve error message when the requested type is not found.
# 11 Sep 2013   Change default for ip_ver to be 0 (ignored in query)
#               since most types have type specific names. This corrects
#               for type 'iso' which is not IP and therefore has no version.
# 11 Jul 2013   add ip_ver argument and default to using IPv4.
#               this change meant sorting by version no longer makes sense.
# 25 Apr 2013   initial creation from networks library

############################################################################
# get_type_info
# Build a list of network type dictionaries.
# The list determines which network types can allocated. The list is
# ordered based on their allocation priority.
#
# Input:  the_type_name - the value to find a type for.
# Output: The type information.
# Exception: If the_type_name is specified and that type is not found in the db
#             then 'InvalidNetType' is returned.
# 
def get_type_info(the_type_name='', ip_ver=0):
    global _network_types

    # Generate a where clause.
    have_where = False
    my_where = ""
    if ( ip_ver != 0 ):
        my_where = "net_family = ipv" + str(ip_ver)
        have_where = True
    if ( the_type_name > "" ):
        if ( have_where == True ):
            my_where += " AND "
        my_where += "net_type = '" + the_type_name + "'"

    # Order table by priority of allocation to building and the ip version.
    sort_order = "assign_priority"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_network_types, my_where, sort_order)

    if ( len(result) == 0 ):
        raise NetException("NetTypeNotFound", 
            "The network type '" + the_type_name + "' is not defined.")

    return(result)
## end def get_type_info():


#\L

# 11 Aug 2014   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 29 Apr 2014   initial creation.

############################################################################
# get_type_detail
# Build a dictionary of information regarding types defined.
# Entry:
#       ip_version - which version of ip? 4 or 6? or 0=generic.
#       use_ISO - should ISO network be included?
# Exit:
#       The result contains a dictionary of information:
#           required_type_names - A dictionary with each entry containing
#               a list of the required type names for each building type.
#           multi_type_names - set of types which can have multiple in bldg.
#           uniq_type_names - set of types which need to be unique in a bldg.
#           family_ip - network types with family ip
#           family_ipv4 - network types with family ipv4
#           family_ipv6 - network types with family ipv6
#           family_other - network types with non-ip family
#           type_info_by_name - dictionary of all types, key is type name.
# Exceptions:
#       

def get_type_detail(ip_version, use_ISO=False):
    result = {}

    # Key for ip family column in network types.
    if ( ip_version == 0 ):
        ip_family = ['ip','ipv4','ipv6']
    else:
        ip_family = ['ip', 'ipv' + str(ip_version)]
        if ( ip_version == 6 ):
            use_ISO = True

    # Key for required_types; comes from building_types table.
    bldg_type_list = building_types.get_building_types()

    # Get the list of all network types we can allocate, default is
    # to have them sorted by their assignment priority.
    net_type_list = get_type_info()

    # Loop through to create the detailed information.
    types_by_priority = []
    required_types = set()
    multi_types = set()
    uniq_types = set()
    v4family = set()
    v6family = set()
    v46family = set()
    other_family = set()
    all_type_info = {}
    for one_type in net_type_list:
        this_type_name = one_type['net_type']
        this_type_family = one_type['net_family']
        all_type_info[this_type_name] = one_type
        types_by_priority.append(this_type_name)
        for a_bldg_type in bldg_type_list:
            if ( this_type_family in ip_family ):
                if ( a_bldg_type in one_type['bldg_required'] ):
                    required_types[a_bldg_type].add(this_type_name)
            if ( one_type['net_type'] == 'iso' and use_ISO ):
                required_types[a_bldg_type].add(this_type_name)

        # Can there be more than one of this type in a building?
        if ( one_type['base_vlan_tag'] >= 0 ):
            multi_types.add(this_type_name)
        else:
            uniq_types.add(this_type_name)

        # Build family specific lists
        if ( this_type_family == 'ip' ):
            v46family.add(this_type_name)
        elif ( this_type_family == 'ipv4' ):
            v4family.add(this_type_name)
        elif ( this_type_family == 'ipv6' ):
            v6family.add(this_type_name)
        else:
            other_family.add(this_type_name)

    result['types_by_priority'] = types_by_priority
    result['required_type_names'] = {}
    for a_bldg_type in required_types.keys():
        result['required_type_names'][a_bldg_type] = \
                frozenset(required_types[a_bldg_type])
    result['multi_type_names'] = frozenset(multi_types)
    result['uniq_type_names'] = frozenset(uniq_types)
    result['family_ip'] = frozenset(v46family)
    result['family_ipv4'] = frozenset(v4family)
    result['family_ipv6'] = frozenset(v6family)
    result['family_other'] = frozenset(other_family)
    result['type_info_by_name'] = all_type_info

    return(result)
## end get_type_detail()


#\L

# 18 Aug 2014        initial creation.

############################################################################
# get_type_tables
# Given a network type, determine the assigned and free tables
# the network should be placed into.

def get_type_tables(the_type_info):

    active_table = None
    free_table = None
    if ( 'net_table_list' in the_type_info ):
        if ( len(the_type_info['net_table_list']) == 2 ):
            (active_table,free_table) = the_type_info['net_table_list']
        elif ( len(the_type_info['net_table_list']) == 1 ):
            active_table = the_type_info['net_table_list'][0]
    elif ( 'rel_table' in the_type_info ):
        (active_table,free_table) = the_type_info['rel_table'].split(',')
    else:
        raise ValueError("Unable to determine which tables '" +\
                the_type_info['net_type'] + "' networks belong in.")

    return((active_table,free_table))
## end get_type_tables()


#\L

#  5 Mar 2014        initial creation.

############################################################################
# get_prefix_size
# Given a network type return the prefix size for that type.

def get_prefix_size(the_type_info, ip_ver):

    # start with an invalid result.
    result = 0

    # Start by using the prefix_size if it is available:
    if ( 'prefix_size' in the_type_info ):
        if ( type(the_type_info['prefix_size']) is list ):
            # We have a list.
            if ( ip_ver == 4 and len(the_type_info['prefix_size']) > 0 ):
                # Element 0 is for IPv4
                result = the_type_info['prefix_size'][0]
            elif ( ip_ver == 6 and len(the_type_info['prefix_size']) > 1 ):
                # if it exists, element 1 is for IPv6
                result = the_type_info['prefix_size'][1]
        else:
            # We don't have a list so this is just for IPv4
            if ( ip_ver == 4 ):
                result = the_type_info['prefix_size']

    # We didn't get a value from prefix_size so pick some defaults
    # based on the type.
    if ( result == 0 and ip_ver == 4 ):
        if ( the_type_info['net_type'] == 'loop' ):
            result = 32
        elif ( the_type_info['net_type'] == 'link' ):
            result = 30
        elif ( the_type_info['net_type'] == 'camera' ):
            result = 24
        elif ( the_type_info['net_type'] == 'prot' ):
            result = 28
        else:
            result = 26

    if ( result == 0 and ip_ver == 6 ):
        if ( the_type_info['net_type'] == 'loop' ):
            result = 128
        elif ( the_type_info['net_type'] == 'link' ):
            result = 112
        else:
            result = 64

    if ( result == 0 ):
        raise ValueError("Unable to determine prefix for '" +\
                the_type_info['net_type'] + "' networks.")

    return(result)
## end get_prefix_size()


#

# 11 Aug 2013   Numerous columns renamed for uniqueness and better
#               description of how they are used.
#  5 Mar 2014    Change query to exclude only vlans that are optional and
#                have a non-zero vlan tag value. This allows easy addition
#                of 'user' type networks with private address allocations
#                without the requirement of the vlan being unique in that
#                building code.
# 16 Oct 2013    initial creation

############################################################################
# get_opt_types
# Build a list of network type dictionaries for use on campus.
# The list contains only network types which are unique within the
# building and are optionally assigned.
#
# Input:  none.
# Output: The list of type information.
# 
def get_opt_types():
    global _network_types

    # Generate a where clause.
    my_where = "type_required = 'f' AND base_vlan_tag != 0"

    # Order table by priority of allocation to building
    sort_order = "assign_priority"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_network_types, my_where, sort_order)

    if ( len(result) == 0 ):
        # No codes found.
        raise NetException("NetTypeNotFound", "no types found.")

    return(result)
## end def get_opt_types():


#\L

#  5 Mar 2014        initial creation.

############################################################################
# get_bldg_required
# Given a particular network type and building type determine if a 
# if a type is required there.

def get_bldg_required(net_type_info, bldg_type):

    # start with an empty result.
    result = {}

    if ( bldg_type in net_type_info['bldg_required'] ):
        result = True
    else:
        result = False

    return(result)
## end get_bldg_required()


#\L

# 25 Apr 2013        initial creation.

############################################################################
# get_net_type_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_net_type_fields():
    global _network_types

    result = {}

    # Get the list of fields/input values for this table.
    table_info = db_access.get_table_keys(_network_types)

    # Copy the table information so we can modify it.
    for aKey in list(table_info.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = table_info[aKey]

    return(result)
## end get_net_type_fields()


#\L

# 16 Apr 2015   initial creation.

############################################################################
# _check_net_type
# Entry: net_type_info - The network type information to check.
# Exit: result
# Exceptions:

def _check_net_type(net_type_info):
    global _network_types

    # Check that the assignment method is valid.
    if ( 'assign_method' in net_type_info ):
        if ( net_type_info['assign_method'] not in ["manual", "next", "nextn",
                        "calc", "private", "range", "index"] ):
            raise NetException("InvalidAssignMethod",
                "The assignment method '" + net_type_info['assign_method']
                + "' is invalid.")

    # Check that the prefix size is valid.
    if ( 'prefix_size' in net_type_info ):
        prefix_size = net_type_info['prefix_size']
        if ( len(prefix_size) == 0 ):
            pass
        elif ( len(prefix_size) == 1 ):
            if ( prefix_size[0] > 32 or prefix_size[0] < 8 ):
                raise NetException("InvalidPrefix",
                        "The network prefix " + str(prefix_size[0])
                        + " is not valid for IPv4 networks.")
        elif ( len(prefix_size) == 2 ):
            if ( prefix_size[1] > 128 or prefix_size[1] < 32 ):
                raise NetException("InvalidPrefix",
                        "The network prefix " + str(prefix_size[1])
                        + " is not valid for IPv6 networks.")

    # Check the building types. Cannot have a constraint which checks
    # the individual elements of an array as a foreign key so we must
    # check each type in the list individually.
    if ( 'bldg_type_list' in net_type_info ):
        for a_bldg_type_name in net_type_info['bldg_type_list']:
            if ( not verify_building_type(a_bldg_type_name) ):
                raise NetException("InvalidBldgType",
                        "The building type '" + a_bldg_type_name +
                        "' is not defined.")

    # Check that the name is unique.
    my_where = "lower(net_type) = lower('" + net_type_info['net_type'] + "')"
    name_count = db_access.get_row_count(_network_types, my_where)
    if ( name_count > 0 ):
        raise NetException("NetTypeExists",
                "A network type named '" + net_type_info['net_type'] +
                "' already exists.")

    return
## end _check_net_type()


#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_net_type
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_net_type():
    global _network_types

    result = db_access.new_table_row(_network_types)

    return(result)
## end new_net_type()


#

# 16 Apr 2015   The type_required value is gone and bldg_type_list replaced
#               it as a list of building types which identify the building
#               types where this network type is required.
# 11 Aug 2013   Numerous columns renamed for uniqueness and better
#               description of how they are used.
# 20 Sep 2013   set_table_row now returns the updated/inserted
#               values from the database. get_table_rows call not needed.
#               Change to use get_row_count rather than attempting
#               to select the record directly.
# 21 Jun 2013   result should be a dict, not a list. This provides
#               consistient results between Add and Update.
# 25 Apr 2013   initial writing.

############################################################################
# add_net_type
# Add the new type to the database.
# Entry: new_net_type - the dictionary of the type to add to the database.

def add_net_type(new_net_type):
    """
    # Start with an empty template
    new_net_type = NewTypeTemplate()
    # The only mandatory field
    new_net_type['net_type'] = 'user'
    # The remainder can be deleted for default or set as required.
    # What vlan should networks of this type start numbering at?
    new_net_type['base_vlan_tag'] = 40    # default is 0
    # How should new networks be determined?
    new_net_type['assign_method'] = 'manual'
    # What order should this network be assigned to a building in?
    new_net_type['assign_priority'] = 0
    # What building types should this network be automatically assigned to.
    new_net_type['bldg_type_list'] = ["office", "dc"]
    # What extension should be used to identify type in the name?
    new_net_type['name_extension'] = ''
    # What tables should be used for this type of network?
    # one or two entries, first is assigned table, second is free table.
    new_net_type['net_table_list'] = ['network_bldgs', 'network_free']
    # what family does this network belong to?
    # ip, ipv4, ipv6, iso, bri (bridged)
    new_net_type['net_family'] = 'ip'
    # prefix size - list - for ip, needs two entries ipv4 and ipv6
    new_net_type['prefix_size'] = [ 30, 112 ]
    addedType = add_net_type(new_net_type)
    """
    global _network_types

    # Make sure what we got is valid.
    _check_net_type(new_net_type)

    # Everything checked out so insert the new network into the table.
    result = db_access.set_table_row(_network_types, new_net_type)

    return(result)
## end def add_net_type():


#\L

# 20 Sep 2013        set_table_row now returns the updated/inserted
#                    values from the database. get_table_rows call not needed.
# 18 Apr 2013        initial creation.

############################################################################
# update_net_type
# Accept a net_type_info dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_net_type(net_type_info):
    global _network_types

    # Make sure what we got is valid.
    try:
        _check_net_type(net_type_info)
    except NetException as net_err:
        if ( net_err.name == "NetTypeExists" ):
            # this is as expected, it should exist.
            pass
    else:
        raise NetException("NetTypeNotFound",
                "The network type '" + net_type_info['net_type'] +
                "' was not found.")

    # Update an existing network with new values.
    my_where = "net_type = '" + net_type_info['net_type'] + "'"
    curr_info = db_access.get_table_rows(_network_types, my_where)[0]

    for a_key in list(curr_info.keys()):
        if ( a_key in net_type_info ):
            # remove information that has not changed.
            if ( net_type_info[a_key] == curr_info[a_key] 
                    and a_key != 'net_type' ):
                del net_type_info[a_key]

    if ( len(net_type_info) > 1 ):
        # If there is something to update, update it.
        result = db_access.set_table_row(_network_types,
                net_type_info, my_where)
    else:
        result = curr_info

    return(result)
## end update_net_type()


#

#  9 Sep 2014   Add exception for deleting zero rows.
# 25 Apr 2013   initial creation.

############################################################################
# delete_net_type
# Given a type dictionary, delete the type from the database.
# Entry:
#        the_net_type - the type dictionary to delete.
# Exit: the type has been deleted from the database.

# 4 Feb 2012    initial creation

def delete_net_type(the_net_type):
    global _network_types

    my_where = "net_type = '" + the_net_type['net_type'] + "'"
    rows = db_access.delete_table_rows(_network_types, my_where)
    if ( rows == 0 ):
        raise NetException("NetTypeNotFound",
                "The network type " + the_net_type['net_type'] + " not found.")

    return()
## end def delete_net_type():


# vim: syntax=python ts=4 sw=4 showmatch et :
