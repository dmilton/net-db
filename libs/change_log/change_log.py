#
g_version='24 Apr 2014 - 1.0.0'

import db_access
from getpass import getuser

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network Types table.
#
global _change_log
_change_log = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'change_log',
    'index': 'net_type'
}


#

# 24 Apr 2014        initial creation

##############################################################################
# initialize()
# Perform initialization process for the change log table. 
# Entry: none. initializes internal variables for use.
# Exit: network change log is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _changeLog, _tablesInitialized

    if ( _tablesInitialized == True ):
        return

    # initialize the table for use.
    _changeLog.update(db_access.init_db_table(_changeLog))

## end initialize()

#

# 24 Apr 2014   initial writing.

############################################################################
# add_change
# Add the new type to the database.
# Entry: theChange - the change that was made.
# Exit: the change has been logged.

def add_change(theChange):
    """
    theChange = "made a change to the database"
    # 
    add_change(theChange)
    """

    global _changeLog

    newChange = db_access.new_table_row(_changeLog)
    newChange['who'] = getuser()
    newChange['when'] = 'now()'
    newChange['what'] = theChange

    # Insert the change into the table.
    db_access.set_table_row(_changeLog, newChange)

    return()
## end def add_change():

# vim: syntax=python ts=4 sw=4 showmatch et :
