#
g_version=''

import db_access
from getpass import getuser

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

###############################################################################
# Network Types table.
#
global _change_log
_change_log = {
    'database': 'networking',
    'role': 'netmon',
    'table': 'change_log',
    'index': 'net_type'
}

# vim: syntax=python ts=4 sw=4 showmatch et :

