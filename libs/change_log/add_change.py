#

# 24 Apr 2014   initial writing.

############################################################################
# add_change
# Add the new type to the database.
# Entry: theChange - the change that was made.
# Exit: the change has been logged.

def add_change(theChange):
    """
    theChange = "made a change to the database"
    # 
    add_change(theChange)
    """

    global _changeLog

    newChange = db_access.new_table_row(_changeLog)
    newChange['who'] = getuser()
    newChange['when'] = 'now()'
    newChange['what'] = theChange

    # Insert the change into the table.
    db_access.set_table_row(_changeLog, newChange)

    return()
## end def add_change():

# vim: syntax=python ts=4 sw=4 showmatch et :
