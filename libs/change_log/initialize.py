#

# 24 Apr 2014        initial creation

##############################################################################
# initialize()
# Perform initialization process for the change log table. 
# Entry: none. initializes internal variables for use.
# Exit: network change log is now ready to use.

def initialize():
    """
    initialize the network type table access field variables.
    """

    # The table of AFL license assignments.
    global _changeLog, _tablesInitialized

    if ( _tablesInitialized == True ):
        return

    # initialize the table for use.
    _changeLog.update(db_access.init_db_table(_changeLog))

## end initialize()

# vim: syntax=python ts=4 sw=4 showmatch et :
