#
g_version='13 Apr 2015 - aca4442'

import db_access
from network_error import NetException

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False


#\

# 2 May 2013		Initial creation.

###############################################################################
# column information table
# This table provides documentation for all columns in the database.

_columnInfo = {
	'database': 'networking',
	'table': 'column_info',
	'role': 'netmon',
	'index': 'table_name, table_column'
}

#

#  2 May 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the column information/description table field variables.
    """

    # The table of AFL license assignments.
    global _columnInfo
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _columnInfo.update(db_access.init_db_table(_columnInfo))

    _tables_initialized = True
    
## end initialize()


#\L

#  2 May 2013		initial creation.

############################################################################
# new_db_column
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#		type entry in the database.

def new_db_column():
	global _columnInfo

	result = db_access.new_table_row(_columnInfo)

	return(result)
## end new_db_column()


#\L

#  2 May 2013   initial creation.

############################################################################
# get_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to buildings.

def get_fields():
    global _columnInfo

    result = {}

    # Get the list of fields/input values for this table.
    tableInfo = db_access.get_table_keys(_columnInfo)

    # Copy the table information so we can modify it.
    for aKey in list(tableInfo.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = tableInfo[aKey]

    return(result)
## end get_fields()


#

#  2 May 2013		initial writing.

############################################################################
# add_column
# Add a column description to the database
# Entry: newColumn - the dictionary of the column description to add
#		to the database.
# Exit: The column description as identified by the table and column names
#		has been added to the database.

def add_column(newColumn):
	"""
	# Start with an empty template
	newColumn = new_template()
	# The only mandatory field
	newColumn['table_name'] = 'network_zones'
	newColumn['table_column'] = 'network'
	newColumn['column_descr'] = "Network address space to use for zone."
	# 
	add_column(newColumn)
	"""
	# generate a where clause.
	myWhere = "table_name = '" + newColumn['table_name'] + "'"
	myWhere += "AND table_column = '" + newColumn['table_column'] + "'"

	# Check if the type already exists.
	columnInfo = db_access.get_table_rows(_columnInfo, myWhere)
	if ( len(columnInfo) > 0 ):
		raise NetException("duplicate", \
				"the table column " + newColumn['table_name'] + ":" + \
				newColumn['table_column'] + " already exists.")

	# Insert the new network into the table.
	result = db_access.set_table_row(_columnInfo, newColumn)

	return result
## end def add_column():


#\L

# 20 Sep 2013		set_table_row now returns the updated/inserted
#					values so a get_table_rows is not required.
#  2 May 2013		initial creation.

############################################################################
# update_column
def update_column(theColumn):
	global _columnInfo

	# generate a where clause.
	myWhere = "table_name = '" + newColumn['table_name'] + "'"
	myWhere += "AND table_column = '" + newColumn['table_column'] + "'"

	# Update an existing network with new values.
	columnExists = db_access.get_row_count(_theColumn, myWhere) == 1

	if ( columnExists == False ):
		raise NetException("not found", \
			"the table column " + newColumn['table_name'] + ":" + \
			newColumn['table_column'] + " already exists.")

	# If there is something to update, update it.
	result = db_access.set_table_row(_columnInfo, theColumn, myWhere)

	return(result)
## end update_column()


#

#  2 May 2013		initial creation.

############################################################################
# delete_column
# Given a type dictionary, delete the type from the database.
# Entry:
#		theColumn - the type dictionary to delete.
# Exit: the type has been deleted from the database.

# 4 Feb 2012	initial creation

def delete_column(theColumn):
	global _columnInfo

	# Create the where clause
	myWhere = "table_name = '" + theColumn['table_name'] + "'"
	myWhere += "AND table_column = '" + theColumn['table_column'] + "'"

	# delete the row from the table.
	db_access.delete_table_rows(_columnInfo, myWhere)

	return()
## end def delete_column():


# vim: syntax=python ts=4 sw=4 showmatch et :
