#
g_version='May 4, 2013 - 1.0.0'

import db_access
from network_error import NetException

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

# vim: syntax=python ts=4 sw=4 showmatch et :

