#\

# 2 May 2013		Initial creation.

###############################################################################
# column information table
# This table provides documentation for all columns in the database.

_columnInfo = {
	'database': 'networking',
	'table': 'column_info',
	'role': 'netmon',
	'index': 'table_name, table_column'
}

