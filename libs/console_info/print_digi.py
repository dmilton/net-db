#\L

# 28 Nov 2013        initial creation.

############################################################################
# print_digi
#
# Entry:
#        digiList - list of digis to print. If a dict then only
#                one digis is expected and dump is implied.
#        dumpFlag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_digi(digiList, dumpFlag=False):

    # If we only have one digis, print the entrie thing.
    if ( str(type(digiList)) == "<type 'dict'>" or len(digiList) == 1 ):
        if ( str(type(digiList)) == "<type 'dict'>" ):
            digiInfo = digiList
        else:
            digiInfo = digiList[0]
        keyList = list(digiInfo.keys())
        keyList.sort()
        # dump all fields in the record.
        print(("\nDigi MAC: " + digiInfo['digi_mac']))
        for aKey in keyList:
            if ( aKey != 'digi_mac' ):
                print((aKey + " = " + str(digiInfo[aKey])))
    else:

        if ( len(digiList) > 1 ):
            if ( dumpFlag == False ):
                # Convert our list of afl license dictionaries into a table.
                digiKeys = ["digi_mac","digi_type","digi_code","digi_ip",\
                        "digi_nbr","room"]
                digiHead = ("MAC", "Type", "Code", "IP", \
                        "#", "Room")
                digiTitle = "Digi units installed:"
                print_table(digiTitle, digiHead, digiKeys, digiList)
            else:
                keyList = list(digiList[0].keys())
                keyList.sort()
                for aDigi in digiList:
                    # dump all fields in the record.
                    print(("\nDigi MAC: " + aDigi['digi_mac']))
                    for aKey in keyList:
                        if ( aKey != 'digi_mac' ):
                            print((aKey + " = " + str(aDigi[aKey])))

## end def print_digi():

# vim: syntax=python ts=4 sw=4 showmatch et :

