#\L

#  4 Dec 2013        initial creation.

############################################################################
# conserver_config
# Given the digi info and list of ports for the digi, produce the console
# server configuration for that digi.
#
# Entry:
#    bldgInfo - dictionary of info about the building the digi is installe in.
#    digiInfo - dictionary describing the digi itself.
#    portList - list of ports for the digi.

def conserver_config(bldgInfo, digiInfo, portList):

    # Produce something like the following for every port.
    # console a2d12 {
    #    master jed.cc.umanitoba.ca;
    #    include a2;
    #    device /dev/dty/fc002s;
    # }

    # First, figure out what the server name is.
    serverName = get_server_info(digiInfo['server_id'])

    for aPort in portList:
        if ( aPort['active'] ):
            print(("console " + aPort['console_name'] + " {"))
            print(("\tmaster " + serverName + ";"))
            if ( aPort['console_type'] == "serial9600" ):
                incLine = bldgInfo['bldg_code']
                if ( aPort['console_dev'] == "ap" ):
                    incLine += "-ap"
                elif ( aPort['console_dev'] == "sensor" ):
                    incLine += "-s"
                print(("\tinclude " + incLine + \
                        " # " + aPort['console_inc'] + ";"))
                print(("\tdevice /dev/dty/" + aPort['digi_code'] + \
                        str(aPort['port_nbr']).zfill(3) + "s;" + \
                        " # " + aPort['device']))
            elif ( aPort['console_type'] in ( "ipmi", "ilom", "rsc", "drac") ):
                print(("\tinclude " + aPort['console_inc'] + ";"))
            print("}")

    return()
## end conserver_config()

# vim: syntax=python ts=4 sw=4 showmatch et :

