#\L

#  6 Dec 2013    initial creation.

############################################################################
# update_port
# Accept a portInfo dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_port(portInfo):
    global _consoleDigi

    # Update an existing port with new values.
    myWhere = "digi_code = '" + portInfo['digi_code'] + "'"
    myWhere += " AND port_nbr = " + str(portInfo['port_nbr'])
    currInfo = db_access.get_table_rows(_consoleDigi, myWhere)

    if ( len(currInfo) == 0 ):
        raise ValueError("port " + portInfo['port_nbr'] + " for digi code " + \
            portInfo['digi_code'] + " could not be found.")
    else:
        currInfo = currInfo[0]

    # If the console name is being updated to a non-null value
    # the port should me marked as active.
    if ( 'console_name' in portInfo ):
        portInfo['active'] = ( portInfo['console_name'] != None \
                and portInfo['console_name'] > '' )

    # Loop through and eliminate anything that's unchanged.
    for aKey in list(currInfo.keys()):
        if ( (aKey in portInfo) == True ):
            if ( portInfo[aKey] == currInfo[aKey] and aKey != 'digi_code' 
                    and aKey != 'port_nbr' ):
                del portInfo[aKey]

    if ( len(portInfo) > 2 ):
        # If there is something to update, update it.
        result = db_access.set_table_row(_consolePort, portInfo, myWhere)
    else:
        result = currInfo

    return(result)
## end update_port()

# vim: syntax=python ts=4 sw=4 showmatch et :

