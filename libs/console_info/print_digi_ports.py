#\L

# 28 Nov 2013        initial creation.

############################################################################
# print_digi_ports
#
# Entry:
#        portList - list of serial ports to print.
# Exit: the list has been printed to the terminal.

def print_digi_ports(portList):

    if ( len(portList) >= 1 ):
        # Convert our list of afl license dictionaries into a table.
        typeKeys = ["port_nbr","device","console_name","active"]
        typeHead = ("Port", "Device", "Console", "active")
        typeTitle = ""
        print_table(typeTitle, typeHead, typeKeys, portList)
    else:
        print("No serial ports were found for this digi.")

## end def print_digi_ports():

# vim: syntax=python ts=4 sw=4 showmatch et :

