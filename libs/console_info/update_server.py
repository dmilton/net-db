#\L

#  5 Dec 2013        initial creation.

############################################################################
# update_server
# Accept a serverInfo dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_server(serverInfo):
    global _consoleServer

    # Update an existing digi with new values.
    myWhere = "server_id = '" + serverInfo['server_id'] + "'"
    currInfo = db_access.get_table_rows(_consoleServer, myWhere)

    if ( len(currInfo) == 0 ):
        raise ValueError("the server " + serverInfo['server'] + \
                " could not be found.")

    result = db_access.set_table_row(_consoleServer, serverInfo, myWhere)

    return(result)
## end update_server()

# vim: syntax=python ts=4 sw=4 showmatch et :

