#

#  5 Dec 2013        initial writing.

############################################################################
# add_server
# Add the new console server to the database.
# Entry: serverName - the name of the server to add.

def add_server(serverName):
    """
    serverName = "jed.cc.umanitoba.ca"
    addedServer = add_server(serverName)
    """

    global _consoleServer

    # generate a where clause.
    myWhere = "server = '" + serverName + "'"

    # Check if the server already exists.
    serverCount = db_access.get_row_count(_consoleServer, myWhere)
    if ( serverCount > 0 ):
        raise ValueError("the server " + serverName + " already exists.")

    # Insert the new server into the table.
    newServer = {}
    newServer['server'] = serverName
    result = db_access.set_table_row(_consoleServer, newServer)

    return(result)
## end def add_server():

# vim: syntax=python ts=4 sw=4 showmatch et :

