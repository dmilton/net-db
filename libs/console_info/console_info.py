#
g_version='13 Apr 2015 - aca4442'

# Libraries used in this module.
import db_access
from cmd_output import print_table

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False


#\

#  5 Dec 2013    Add tables for server, group and type.
# 20 Nov 2013    Initial creation.

###############################################################################
# Console server table.
# This table contains an entry for every server running the console daemon.

_consoleServer = {
    'database': 'networking',
    'table': 'console_server',
    'role': 'netmon',
    'index': 'console_server_id'
}

###############################################################################
# Console user groups table.
# This table contains an entry for every user group configured in conserver.

_consoleGroup = {
    'database': 'networking',
    'table': 'console_group',
    'role': 'netmon',
    'index': 'console_group_id'
}

###############################################################################
# Console types table.
# This table contains an entry for every type of console configured.
# This table contains an entry for each type of device. Translates
# into a default clause used for includes in the console definition.

_consoleType = {
    'database': 'networking',
    'table': 'console_type',
    'role': 'netmon',
    'index': 'console_type_id'
}

###############################################################################
# Console devices table.
# This table contains an entry for every console device which can be accessed
# through a console server.

_consoleDev = {
    'database': 'networking',
    'table': 'console_digi',
    'role': 'netmon',
    'index': 'digi_mac'
}

###############################################################################
# Installed DIGI table
# This table contains an entry for every digi installed on campus.

_consoleDigi = {
    'database': 'networking',
    'table': 'console_digi',
    'role': 'netmon',
    'index': 'digi_mac'
}

###############################################################################
# DIGI port table.
# Contains an entry for available every serial port on a digi.
# Identified the port number and an active state for the port.

_consoleDigiPort = {
    'database': 'networking',
    'table': 'console_digi_port',
    'role': 'netview',
    'index': 'digi_port_id'
}

###############################################################################
# Console installed digi view.
# This view merges the information from building_rooms to make building code
# determination easier.

_consoleInstalledView = {
    'database': 'networking',
    'table': 'console_digi_installs',
    'role': 'netview',
    'index': 'digi_mac'
}



# 26 Apr 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the console information tables.
    """

    # The table of AFL license assignments.
    global _consoleServer, _consoleGroup, _consoleDigi;
    global _consoleType, _consoleDigiPort, _consoleInstalledView
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _consoleServer.update(db_access.init_db_table(_consoleServer))
    _consoleDigi.update(db_access.init_db_table(_consoleDigi))
    _consoleType.update(db_access.init_db_table(_consoleType))
    _consoleGroup.update(db_access.init_db_table(_consoleGroup))
    _consoleDigiPort.update(db_access.init_db_table(_consoleDigiPort))
    _consoleInstalledView.update(db_access.init_db_table(_consoleInstalledView))

    _tables_initialized = True

## end initialize()


#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_digi
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_digi():
    global _consoleDigi

    result = db_access.new_table_row(_consoleDigi)

    return(result)
## end new_digi()


#

# 20 Nov 2013        initial creation

############################################################################
# get_digi_info
# Given a digi code, get the the entire record from the database.
#
# Input:  digiCode - the value to find a digi for. If digiCode is an
#        empty string then all records in the database are returned.
# Output: The digi information.
# 
def get_digi_info(digiCode):
    global _consoleDigi

    # Generate a where clause.
    if ( digiCode > "" ):
        myWhere = "digi_code = '" + digiCode + "'"
    else:
        myWhere = ""

    # Order table by priority of allocation to building and the ip version.
    sortOrder = "bldg_code,digi_code"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_consoleDigi, myWhere, sortOrder)

    return(result)
## end def get_digi_info():


#

# 20 Nov 2013        initial creation

############################################################################
# find_digis
# Get the list of digi units installed in a building.
#
# Entry:
#    bldgCode - the building code to get digi units for.
# Output: A list of digi units installed in the building.
# Exception: If theDigi is specified and that type is not found in the db
#             then 'InvalidNetType' is returned.
# 
def find_digis(bldgCode):
    global _consoleInstalledView

    # Generate a where clause.
    myWhere = "bldg_code = '" + bldgCode + "'"
    sortOrder = "digi_code"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_consoleInstalledView, myWhere, sortOrder)

    return(result)
## end def find_digis():


#\L

# 28 Nov 2013        initial creation.

############################################################################
# find_digi_mac
def find_digi_mac(digiMac):
    global _consoleDigi, _consoleInstalledView

    myWhere = "digi_mac = '" + digiMac + "'"
    result = db_access.get_table_rows(_consoleInstalledView, myWhere)

    return(result)
## end find_digi_mac()


#\L

# 28 Nov 2013        initial creation.

############################################################################
# find_digi_name
# Entry:
#    digiName - the name of the digi to find information for.
# Exit:
#    the digi information has been extracted and returned as a result.

def find_digi_name(digiName):
    global _consoleDigi, _consoleInstalledView

    # We don't have a name, but we do have an IP.
    from socket import getaddrinfo
    digiIpAddr = getaddrinfo(digiName,0)[0][4][0]

    myWhere = "digi_ip = '" + digiIpAddr + "'"
    result = db_access.get_table_rows(_consoleInstalledView, myWhere)

    return(result)
## end find_digi_name()


#\L

#  6 Dec 2013        initial creation.

############################################################################
# get_next_digi_info
# Determine the next two letter code and IP address for a digi.
# Entry: none
# Exit: digi dictionary with the code and IP set.

def get_next_digi_info():
    global _consoleDigi

    from string import ascii_lowercase
    
    myWhere = ""
    myOrder = "digi_code desc"
    myLimit = 1

    # When sorted by digi_code in descending order the first result is the
    # highest value of two letter digi code in the database. That can be
    # used to compute what the next one should be. Since the IP address and
    # digi code increment together, we can use the IP address from the same
    # record no compute the next one.
    dbResult = db_access.get_table_rows(_consoleDigi, myWhere, myOrder, myLimit)

    if ( len(dbResult) == 0 ):
        raise ValueError("No digi could be found in the database.")

    if ( dbResult[0]['digi_code'] == 'zz' ):
        raise ValueError("The code range for digi units is exhausted.")

    # Remove the stuff the caller needs to set.
    result = db_access.new_table_row(_consoleDigi)
    del result['room_id']
    del result['server_id']
    del result['digi_mac']
    del result['digi_nbr']
    result['digi_type'] = 'el16'

    # Use the next availble IP address.
    # Should really check that the next IP is still within the subnet.
    result['digi_ip'] = dbResult[0]['digi_ip'] + 1

    # Figure out the next digi code to use.
    lastCode = dbResult[0]['digi_code']
    codeL = lastCode[0]
    codeR = lastCode[1]

    if ( codeR == 'z' ):
        codeR = 'a'
        codeL = ascii_lowercase[(ascii_lowercase.find(codeL) + 1)]
    else:
        codeR =    ascii_lowercase[(ascii_lowercase.find(codeR) + 1)]
    result['digi_code'] = codeL + codeR

    return(result)
## end get_next_digi_info()


#\L

#  6 Dec 2013        initial creation.

############################################################################
# get_digi_fields
# Create a list of i/o keys useful for commands that add/update fields and
# records related to building rooms.

def get_digi_fields():

    global _consoleDigi

    result = {}

    # Get the list of fields/input values for this table.
    tableInfo = db_access.get_table_keys(_consoleDigi)

    # Copy the table information so we can modify it.
    for aKey in list(tableInfo.keys()):
        # translate a key with a hyphen so we can accept input with a hyphen.
        key2 = aKey.translate(str.maketrans('-','_'))
        result[key2] = tableInfo[aKey]

    return(result)
## end get_digi_fields()


#

#  6 Dec 2013    Add checks for existance in db of ip and code if present.
# 20 Nov 2013    initial writing.

############################################################################
# add_digi
# Add the new digi to the database.
# Entry: newDigi - the dictionary of the type to add to the database.

def add_digi(newDigi):
    """
    # Start with an empty template
    newDigi = new_template()
    # Mandatory fields
    newDigi['digi_mac'] = '00:00:00:00:00:00'
    newDigi['digi_type'] = 'el16'    # or el32
    newDigi['room_id'] = room_id from building_rooms.
    # optional fields
    newDigi['digi_code'] = 'ZZ'
    newDigi['digi_name'] = '192.168.40.99'
    # 
    addedDigi = add_digi(newDigi)
    """

    global _consoleDigi

    # generate a where clause.
    myWhere = "digi_mac = '" + newDigi['digi_mac'] + "'"
    if ( 'digi_code' in newDigi ):
        myWhere += " OR digi_code = '" + newDigi['digi_code'] + "'"
    if ( 'digi_ip' in newDigi ):
        myWhere += " OR digi_ip = '" + str(newDigi['digi_ip']) + "'"

    # Check if the digi already exists.
    digiCount = db_access.get_row_count(_consoleDigi, myWhere)
    if ( digiCount > 0 ):
        raise ValueError("the digi " + newDigi['digi_mac'] + " already exists.")

    # Insert the new network into the table.
    result = db_access.set_table_row(_consoleDigi, newDigi)

    return(result)
## end def add_digi():


#\L

# 20 Nov 2013        initial creation.

############################################################################
# update_digi
# Accept a digiInfo dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_digi(digiInfo):
    global _consoleDigi

    # Update an existing digi with new values.
    myWhere = "digi_mac = '" + digiInfo['digi_mac'] + "'"
    currInfo = db_access.get_table_rows(_consoleDigi, myWhere)

    if ( len(currInfo) == 0 ):
        raise NetException("not found", \
            "the digi " + digiInfo['digi_mac'] + " could not be found.")
    else:
        currInfo = currInfo[0]

    for aKey in list(currInfo.keys()):
        if ( aKey in digiInfo ):
            if ( digiInfo[aKey] == currInfo[aKey] and aKey != 'digi_mac' ):
                del digiInfo[aKey]

    if ( len(digiInfo) > 1 ):
        # If there is something to update, update it.
        result = db_access.set_table_row(_consoleDigi, digiInfo, myWhere)
    else:
        result = currInfo

    return(result)
## end update_digi()


#

#  9 Sep 2014   Add exception for deleting zero rows.
# 20 Nov 2013   initial creation.

############################################################################
# delete_digi
# Given a digi dictionary, delete the digi from the database.
# Entry:
#        theDigi - the digi to delete.
# Exit: the digi has been deleted from the database.


def delete_digi(theDigi):
    global _consoleDigi, consolePort

    # Delete the digi itself, this automatically cascades to the
    # ports which get deleted too.
    myWhere = "digi_mac = '" + theDigi['digi_mac'] + "'"
    rows = db_access.delete_table_rows(_consoleDigi, myWhere)
    if ( rows == 0 ):
        raise NetException("UnknownDIGI",
                "Could not delete DIGI with MAC: " + theDigi['digi_mac'])

    return()
## end def delete_digi():


#\L

# 28 Nov 2013        initial creation.

############################################################################
# print_digi
#
# Entry:
#        digiList - list of digis to print. If a dict then only
#                one digis is expected and dump is implied.
#        dumpFlag - print all fields of the record.
# Exit: the list has been printed to the terminal.

def print_digi(digiList, dumpFlag=False):

    # If we only have one digis, print the entrie thing.
    if ( str(type(digiList)) == "<type 'dict'>" or len(digiList) == 1 ):
        if ( str(type(digiList)) == "<type 'dict'>" ):
            digiInfo = digiList
        else:
            digiInfo = digiList[0]
        keyList = list(digiInfo.keys())
        keyList.sort()
        # dump all fields in the record.
        print(("\nDigi MAC: " + digiInfo['digi_mac']))
        for aKey in keyList:
            if ( aKey != 'digi_mac' ):
                print((aKey + " = " + str(digiInfo[aKey])))
    else:

        if ( len(digiList) > 1 ):
            if ( dumpFlag == False ):
                # Convert our list of afl license dictionaries into a table.
                digiKeys = ["digi_mac","digi_type","digi_code","digi_ip",\
                        "digi_nbr","room"]
                digiHead = ("MAC", "Type", "Code", "IP", \
                        "#", "Room")
                digiTitle = "Digi units installed:"
                print_table(digiTitle, digiHead, digiKeys, digiList)
            else:
                keyList = list(digiList[0].keys())
                keyList.sort()
                for aDigi in digiList:
                    # dump all fields in the record.
                    print(("\nDigi MAC: " + aDigi['digi_mac']))
                    for aKey in keyList:
                        if ( aKey != 'digi_mac' ):
                            print((aKey + " = " + str(aDigi[aKey])))

## end def print_digi():


#

# 20 Nov 2013        initial creation

############################################################################
# find_digi_ports
# Get the list of digi units installed in a building.
#
# Entry:
#    digiCode - the digi code to get serial ports for.
#    active - when true, only return active ports.
# Output: A list of serial ports for the specified digi.

def find_digi_ports(digiCode, active=False):
    global _consolePort

    # Generate a where clause.
    myWhere = "digi_code = '" + digiCode + "'"
    if ( active ):
        myWhere += " AND active = 'T'"
    sortOrder = "port_nbr"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_consolePort, myWhere, sortOrder)

    return(result)
## end def find_digi_ports():


#

#  6 Dec 2013    initial writing.

############################################################################
# add_ports
# Add the serial ports to the db for the digi provided.
# Entry: theDigi - the dictionary of the digi to create ports for.
# Exit: the correct number of ports has been added.

def add_ports(theDigi):
    """
    portList = add_ports(theDigi)
    """

    global _consoleDigiPort

    # generate a where clause.
    myWhere = "digi_code = '" + theDigi['digi_code'] + "'"

    # Check if ports for the digi already exist.
    portCount = db_access.get_row_count(_consoleDigiPort, myWhere)
    if ( portCount > 0 ):
        raise ValueError("ports for digi code " + \
                theDigi['digi_code'] + " already exist.")

    # How many ports do we need to add?
    if ( theDigi['digi_type'] == 'el16' ):
        portCount = 16
    elif ( theDigi['digi_type'] == 'el32' ):
        portCount = 32
    else:
        raise ValueError("unknown digi type " + theDigi['digi_type'])

    # Loop through and create the appropriate number of ports.
    newPort = db_access.new_table_row(_consoleDigiPort)
    newPort['digi_code'] = theDigi['digi_code']
    newPort['active'] = False
    for portNbr in range(portCount):
        newPort['port_nbr'] = portNbr
        ignore = db_access.set_table_row(_consoleDigiPort, newPort)

    result = find_digi_ports(theDigi['digi_code'])

    return(result)
## end def add_ports():


#\L

#  6 Dec 2013    initial creation.

############################################################################
# update_port
# Accept a portInfo dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_port(portInfo):
    global _consoleDigi

    # Update an existing port with new values.
    myWhere = "digi_code = '" + portInfo['digi_code'] + "'"
    myWhere += " AND port_nbr = " + str(portInfo['port_nbr'])
    currInfo = db_access.get_table_rows(_consoleDigi, myWhere)

    if ( len(currInfo) == 0 ):
        raise ValueError("port " + portInfo['port_nbr'] + " for digi code " + \
            portInfo['digi_code'] + " could not be found.")
    else:
        currInfo = currInfo[0]

    # If the console name is being updated to a non-null value
    # the port should me marked as active.
    if ( 'console_name' in portInfo ):
        portInfo['active'] = ( portInfo['console_name'] != None \
                and portInfo['console_name'] > '' )

    # Loop through and eliminate anything that's unchanged.
    for aKey in list(currInfo.keys()):
        if ( (aKey in portInfo) == True ):
            if ( portInfo[aKey] == currInfo[aKey] and aKey != 'digi_code' 
                    and aKey != 'port_nbr' ):
                del portInfo[aKey]

    if ( len(portInfo) > 2 ):
        # If there is something to update, update it.
        result = db_access.set_table_row(_consolePort, portInfo, myWhere)
    else:
        result = currInfo

    return(result)
## end update_port()


#\L

# 28 Nov 2013        initial creation.

############################################################################
# print_digi_ports
#
# Entry:
#        portList - list of serial ports to print.
# Exit: the list has been printed to the terminal.

def print_digi_ports(portList):

    if ( len(portList) >= 1 ):
        # Convert our list of afl license dictionaries into a table.
        typeKeys = ["port_nbr","device","console_name","active"]
        typeHead = ("Port", "Device", "Console", "active")
        typeTitle = ""
        print_table(typeTitle, typeHead, typeKeys, portList)
    else:
        print("No serial ports were found for this digi.")

## end def print_digi_ports():


#

#  5 Dec 2013        initial creation

############################################################################
# get_server_info
# Get the list of digi units installed in a building.
#
# Entry:
#    serverId - the id of the server to get the name of.
# Output: The name of the server is returned.
# Exception: If the server id doesn't exist a ValueError is generated.
# 

def get_server_info(serverId):
    global _consoleServer

    # Generate a where clause.
    myWhere = "server_id = " + str(serverId)

    # Fetch the information from the database.
    dbResult = db_access.get_table_rows(_consoleServer, myWhere)
    if ( len(dbResult) == 1 ):
        result = dbResult[0]['server']
    else:
        raise ValueError("invalid server id " + str(serverId))

    return(result)
## end def get_server_info():


#

#  5 Dec 2013        initial writing.

############################################################################
# add_server
# Add the new console server to the database.
# Entry: serverName - the name of the server to add.

def add_server(serverName):
    """
    serverName = "jed.cc.umanitoba.ca"
    addedServer = add_server(serverName)
    """

    global _consoleServer

    # generate a where clause.
    myWhere = "server = '" + serverName + "'"

    # Check if the server already exists.
    serverCount = db_access.get_row_count(_consoleServer, myWhere)
    if ( serverCount > 0 ):
        raise ValueError("the server " + serverName + " already exists.")

    # Insert the new server into the table.
    newServer = {}
    newServer['server'] = serverName
    result = db_access.set_table_row(_consoleServer, newServer)

    return(result)
## end def add_server():


#\L

#  5 Dec 2013        initial creation.

############################################################################
# update_server
# Accept a serverInfo dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_server(serverInfo):
    global _consoleServer

    # Update an existing digi with new values.
    myWhere = "server_id = '" + serverInfo['server_id'] + "'"
    currInfo = db_access.get_table_rows(_consoleServer, myWhere)

    if ( len(currInfo) == 0 ):
        raise ValueError("the server " + serverInfo['server'] + \
                " could not be found.")

    result = db_access.set_table_row(_consoleServer, serverInfo, myWhere)

    return(result)
## end update_server()


#

#  9 Sep 2014   Add exception for deleting zero rows.
# 20 Nov 2013   initial creation.

############################################################################
# delete_server
# Given a server dictionary, delete the server from the database.
# Entry:
#        theServer - the server to delete.
# Exit: the server has been deleted from the database.


def delete_server(theServer):
    global _consoleServer

    # Delete all of the ports for this server.
    myWhere = "server_id = '" + theServer['server_id'] + "'"
    ignore = db_access.delete_table_rows(_consoleDigiPort,
                        myWhere, cascade=True)

    # Delete the server itself.
    myWhere = "server_mac = '" + theServer['server_mac'] + "'"
    rows = db_access.delete_table_rows(_consoleServer, myWhere, cascade=True)
    if ( rows == 0 ):
        raise NetException("UnknownServer",
                "Could not delete server with MAC: " + theDigi['digi_mac'])

    return()
## end def delete_server():


#\L

#  4 Dec 2013        initial creation.

############################################################################
# conserver_config
# Given the digi info and list of ports for the digi, produce the console
# server configuration for that digi.
#
# Entry:
#    bldgInfo - dictionary of info about the building the digi is installe in.
#    digiInfo - dictionary describing the digi itself.
#    portList - list of ports for the digi.

def conserver_config(bldgInfo, digiInfo, portList):

    # Produce something like the following for every port.
    # console a2d12 {
    #    master jed.cc.umanitoba.ca;
    #    include a2;
    #    device /dev/dty/fc002s;
    # }

    # First, figure out what the server name is.
    serverName = get_server_info(digiInfo['server_id'])

    for aPort in portList:
        if ( aPort['active'] ):
            print(("console " + aPort['console_name'] + " {"))
            print(("\tmaster " + serverName + ";"))
            if ( aPort['console_type'] == "serial9600" ):
                incLine = bldgInfo['bldg_code']
                if ( aPort['console_dev'] == "ap" ):
                    incLine += "-ap"
                elif ( aPort['console_dev'] == "sensor" ):
                    incLine += "-s"
                print(("\tinclude " + incLine + \
                        " # " + aPort['console_inc'] + ";"))
                print(("\tdevice /dev/dty/" + aPort['digi_code'] + \
                        str(aPort['port_nbr']).zfill(3) + "s;" + \
                        " # " + aPort['device']))
            elif ( aPort['console_type'] in ( "ipmi", "ilom", "rsc", "drac") ):
                print(("\tinclude " + aPort['console_inc'] + ";"))
            print("}")

    return()
## end conserver_config()


# vim: syntax=python ts=4 sw=4 showmatch et :
