
# 26 Apr 2013        initial creation

##############################################################################
# initialize()
# Perform initialization process for the network type information table. 
# Entry: none. initializes internal variables for use.
# Exit: network type table is now ready to use.

def initialize():
    """
    initialize the console information tables.
    """

    # The table of AFL license assignments.
    global _consoleServer, _consoleGroup, _consoleDigi;
    global _consoleType, _consoleDigiPort, _consoleInstalledView
    global _tables_initialized

    if ( _tables_initialized == True ):
        return

    # initialize the table for use.
    _consoleServer.update(db_access.init_db_table(_consoleServer))
    _consoleDigi.update(db_access.init_db_table(_consoleDigi))
    _consoleType.update(db_access.init_db_table(_consoleType))
    _consoleGroup.update(db_access.init_db_table(_consoleGroup))
    _consoleDigiPort.update(db_access.init_db_table(_consoleDigiPort))
    _consoleInstalledView.update(db_access.init_db_table(_consoleInstalledView))

    _tables_initialized = True

## end initialize()

# vim: syntax=python ts=4 sw=4 showmatch et :

