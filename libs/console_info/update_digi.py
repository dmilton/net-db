#\L

# 20 Nov 2013        initial creation.

############################################################################
# update_digi
# Accept a digiInfo dictionary and update the database to reflect the
# contents of the dictionary. Return the updated row contents as a result.

def update_digi(digiInfo):
    global _consoleDigi

    # Update an existing digi with new values.
    myWhere = "digi_mac = '" + digiInfo['digi_mac'] + "'"
    currInfo = db_access.get_table_rows(_consoleDigi, myWhere)

    if ( len(currInfo) == 0 ):
        raise NetException("not found", \
            "the digi " + digiInfo['digi_mac'] + " could not be found.")
    else:
        currInfo = currInfo[0]

    for aKey in list(currInfo.keys()):
        if ( aKey in digiInfo ):
            if ( digiInfo[aKey] == currInfo[aKey] and aKey != 'digi_mac' ):
                del digiInfo[aKey]

    if ( len(digiInfo) > 1 ):
        # If there is something to update, update it.
        result = db_access.set_table_row(_consoleDigi, digiInfo, myWhere)
    else:
        result = currInfo

    return(result)
## end update_digi()

# vim: syntax=python ts=4 sw=4 showmatch et :

