#
g_version=''

# Libraries used in this module.
import db_access
from cmd_output import print_table

# Flag to track if we have been initialized.
global _tables_initialized
_tables_initialized = False

# vim: syntax=python ts=4 sw=4 showmatch et :

