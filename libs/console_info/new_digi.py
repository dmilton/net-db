#\L

# 25 Apr 2013        initial creation.

############################################################################
# new_digi
# Entry: none
# Exit: a dictionary containing the fields necessary for creation of a new
#        type entry in the database.

def new_digi():
    global _consoleDigi

    result = db_access.new_table_row(_consoleDigi)

    return(result)
## end new_digi()

# vim: syntax=python ts=4 sw=4 showmatch et :

