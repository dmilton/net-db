#\L

#  6 Dec 2013        initial creation.

############################################################################
# get_next_digi_info
# Determine the next two letter code and IP address for a digi.
# Entry: none
# Exit: digi dictionary with the code and IP set.

def get_next_digi_info():
    global _consoleDigi

    from string import ascii_lowercase
    
    myWhere = ""
    myOrder = "digi_code desc"
    myLimit = 1

    # When sorted by digi_code in descending order the first result is the
    # highest value of two letter digi code in the database. That can be
    # used to compute what the next one should be. Since the IP address and
    # digi code increment together, we can use the IP address from the same
    # record no compute the next one.
    dbResult = db_access.get_table_rows(_consoleDigi, myWhere, myOrder, myLimit)

    if ( len(dbResult) == 0 ):
        raise ValueError("No digi could be found in the database.")

    if ( dbResult[0]['digi_code'] == 'zz' ):
        raise ValueError("The code range for digi units is exhausted.")

    # Remove the stuff the caller needs to set.
    result = db_access.new_table_row(_consoleDigi)
    del result['room_id']
    del result['server_id']
    del result['digi_mac']
    del result['digi_nbr']
    result['digi_type'] = 'el16'

    # Use the next availble IP address.
    # Should really check that the next IP is still within the subnet.
    result['digi_ip'] = dbResult[0]['digi_ip'] + 1

    # Figure out the next digi code to use.
    lastCode = dbResult[0]['digi_code']
    codeL = lastCode[0]
    codeR = lastCode[1]

    if ( codeR == 'z' ):
        codeR = 'a'
        codeL = ascii_lowercase[(ascii_lowercase.find(codeL) + 1)]
    else:
        codeR =    ascii_lowercase[(ascii_lowercase.find(codeR) + 1)]
    result['digi_code'] = codeL + codeR

    return(result)
## end get_next_digi_info()

# vim: syntax=python ts=4 sw=4 showmatch et :

