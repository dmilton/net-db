#

# 20 Nov 2013        initial creation

############################################################################
# get_digi_info
# Given a digi code, get the the entire record from the database.
#
# Input:  digiCode - the value to find a digi for. If digiCode is an
#        empty string then all records in the database are returned.
# Output: The digi information.
# 
def get_digi_info(digiCode):
    global _consoleDigi

    # Generate a where clause.
    if ( digiCode > "" ):
        myWhere = "digi_code = '" + digiCode + "'"
    else:
        myWhere = ""

    # Order table by priority of allocation to building and the ip version.
    sortOrder = "bldg_code,digi_code"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_consoleDigi, myWhere, sortOrder)

    return(result)
## end def get_digi_info():

# vim: syntax=python ts=4 sw=4 showmatch et :

