#

# 20 Nov 2013        initial creation

############################################################################
# find_digis
# Get the list of digi units installed in a building.
#
# Entry:
#    bldgCode - the building code to get digi units for.
# Output: A list of digi units installed in the building.
# Exception: If theDigi is specified and that type is not found in the db
#             then 'InvalidNetType' is returned.
# 
def find_digis(bldgCode):
    global _consoleInstalledView

    # Generate a where clause.
    myWhere = "bldg_code = '" + bldgCode + "'"
    sortOrder = "digi_code"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_consoleInstalledView, myWhere, sortOrder)

    return(result)
## end def find_digis():

# vim: syntax=python ts=4 sw=4 showmatch et :

