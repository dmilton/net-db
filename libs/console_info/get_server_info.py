#

#  5 Dec 2013        initial creation

############################################################################
# get_server_info
# Get the list of digi units installed in a building.
#
# Entry:
#    serverId - the id of the server to get the name of.
# Output: The name of the server is returned.
# Exception: If the server id doesn't exist a ValueError is generated.
# 

def get_server_info(serverId):
    global _consoleServer

    # Generate a where clause.
    myWhere = "server_id = " + str(serverId)

    # Fetch the information from the database.
    dbResult = db_access.get_table_rows(_consoleServer, myWhere)
    if ( len(dbResult) == 1 ):
        result = dbResult[0]['server']
    else:
        raise ValueError("invalid server id " + str(serverId))

    return(result)
## end def get_server_info():

# vim: syntax=python ts=4 sw=4 showmatch et :

