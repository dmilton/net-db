#

#  6 Dec 2013    Add checks for existance in db of ip and code if present.
# 20 Nov 2013    initial writing.

############################################################################
# add_digi
# Add the new digi to the database.
# Entry: newDigi - the dictionary of the type to add to the database.

def add_digi(newDigi):
    """
    # Start with an empty template
    newDigi = new_template()
    # Mandatory fields
    newDigi['digi_mac'] = '00:00:00:00:00:00'
    newDigi['digi_type'] = 'el16'    # or el32
    newDigi['room_id'] = room_id from building_rooms.
    # optional fields
    newDigi['digi_code'] = 'ZZ'
    newDigi['digi_name'] = '192.168.40.99'
    # 
    addedDigi = add_digi(newDigi)
    """

    global _consoleDigi

    # generate a where clause.
    myWhere = "digi_mac = '" + newDigi['digi_mac'] + "'"
    if ( 'digi_code' in newDigi ):
        myWhere += " OR digi_code = '" + newDigi['digi_code'] + "'"
    if ( 'digi_ip' in newDigi ):
        myWhere += " OR digi_ip = '" + str(newDigi['digi_ip']) + "'"

    # Check if the digi already exists.
    digiCount = db_access.get_row_count(_consoleDigi, myWhere)
    if ( digiCount > 0 ):
        raise ValueError("the digi " + newDigi['digi_mac'] + " already exists.")

    # Insert the new network into the table.
    result = db_access.set_table_row(_consoleDigi, newDigi)

    return(result)
## end def add_digi():

# vim: syntax=python ts=4 sw=4 showmatch et :

