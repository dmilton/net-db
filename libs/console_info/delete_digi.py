#

#  9 Sep 2014   Add exception for deleting zero rows.
# 20 Nov 2013   initial creation.

############################################################################
# delete_digi
# Given a digi dictionary, delete the digi from the database.
# Entry:
#        theDigi - the digi to delete.
# Exit: the digi has been deleted from the database.


def delete_digi(theDigi):
    global _consoleDigi, consolePort

    # Delete the digi itself, this automatically cascades to the
    # ports which get deleted too.
    myWhere = "digi_mac = '" + theDigi['digi_mac'] + "'"
    rows = db_access.delete_table_rows(_consoleDigi, myWhere)
    if ( rows == 0 ):
        raise NetException("UnknownDIGI",
                "Could not delete DIGI with MAC: " + theDigi['digi_mac'])

    return()
## end def delete_digi():

# vim: syntax=python ts=4 sw=4 showmatch et :

