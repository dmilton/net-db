#\

#  5 Dec 2013    Add tables for server, group and type.
# 20 Nov 2013    Initial creation.

###############################################################################
# Console server table.
# This table contains an entry for every server running the console daemon.

_consoleServer = {
    'database': 'networking',
    'table': 'console_server',
    'role': 'netmon',
    'index': 'console_server_id'
}

###############################################################################
# Console user groups table.
# This table contains an entry for every user group configured in conserver.

_consoleGroup = {
    'database': 'networking',
    'table': 'console_group',
    'role': 'netmon',
    'index': 'console_group_id'
}

###############################################################################
# Console types table.
# This table contains an entry for every type of console configured.
# This table contains an entry for each type of device. Translates
# into a default clause used for includes in the console definition.

_consoleType = {
    'database': 'networking',
    'table': 'console_type',
    'role': 'netmon',
    'index': 'console_type_id'
}

###############################################################################
# Console devices table.
# This table contains an entry for every console device which can be accessed
# through a console server.

_consoleDev = {
    'database': 'networking',
    'table': 'console_digi',
    'role': 'netmon',
    'index': 'digi_mac'
}

###############################################################################
# Installed DIGI table
# This table contains an entry for every digi installed on campus.

_consoleDigi = {
    'database': 'networking',
    'table': 'console_digi',
    'role': 'netmon',
    'index': 'digi_mac'
}

###############################################################################
# DIGI port table.
# Contains an entry for available every serial port on a digi.
# Identified the port number and an active state for the port.

_consoleDigiPort = {
    'database': 'networking',
    'table': 'console_digi_port',
    'role': 'netview',
    'index': 'digi_port_id'
}

###############################################################################
# Console installed digi view.
# This view merges the information from building_rooms to make building code
# determination easier.

_consoleInstalledView = {
    'database': 'networking',
    'table': 'console_digi_installs',
    'role': 'netview',
    'index': 'digi_mac'
}

# vim: syntax=python ts=4 sw=4 showmatch et :

