#\L

# 28 Nov 2013        initial creation.

############################################################################
# find_digi_name
# Entry:
#    digiName - the name of the digi to find information for.
# Exit:
#    the digi information has been extracted and returned as a result.

def find_digi_name(digiName):
    global _consoleDigi, _consoleInstalledView

    # We don't have a name, but we do have an IP.
    from socket import getaddrinfo
    digiIpAddr = getaddrinfo(digiName,0)[0][4][0]

    myWhere = "digi_ip = '" + digiIpAddr + "'"
    result = db_access.get_table_rows(_consoleInstalledView, myWhere)

    return(result)
## end find_digi_name()

# vim: syntax=python ts=4 sw=4 showmatch et :

