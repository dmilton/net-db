#

# 20 Nov 2013        initial creation

############################################################################
# find_digi_ports
# Get the list of digi units installed in a building.
#
# Entry:
#    digiCode - the digi code to get serial ports for.
#    active - when true, only return active ports.
# Output: A list of serial ports for the specified digi.

def find_digi_ports(digiCode, active=False):
    global _consolePort

    # Generate a where clause.
    myWhere = "digi_code = '" + digiCode + "'"
    if ( active ):
        myWhere += " AND active = 'T'"
    sortOrder = "port_nbr"

    # Fetch the information from the database.
    result = db_access.get_table_rows(_consolePort, myWhere, sortOrder)

    return(result)
## end def find_digi_ports():

# vim: syntax=python ts=4 sw=4 showmatch et :

