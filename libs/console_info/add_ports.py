#

#  6 Dec 2013    initial writing.

############################################################################
# add_ports
# Add the serial ports to the db for the digi provided.
# Entry: theDigi - the dictionary of the digi to create ports for.
# Exit: the correct number of ports has been added.

def add_ports(theDigi):
    """
    portList = add_ports(theDigi)
    """

    global _consoleDigiPort

    # generate a where clause.
    myWhere = "digi_code = '" + theDigi['digi_code'] + "'"

    # Check if ports for the digi already exist.
    portCount = db_access.get_row_count(_consoleDigiPort, myWhere)
    if ( portCount > 0 ):
        raise ValueError("ports for digi code " + \
                theDigi['digi_code'] + " already exist.")

    # How many ports do we need to add?
    if ( theDigi['digi_type'] == 'el16' ):
        portCount = 16
    elif ( theDigi['digi_type'] == 'el32' ):
        portCount = 32
    else:
        raise ValueError("unknown digi type " + theDigi['digi_type'])

    # Loop through and create the appropriate number of ports.
    newPort = db_access.new_table_row(_consoleDigiPort)
    newPort['digi_code'] = theDigi['digi_code']
    newPort['active'] = False
    for portNbr in range(portCount):
        newPort['port_nbr'] = portNbr
        ignore = db_access.set_table_row(_consoleDigiPort, newPort)

    result = find_digi_ports(theDigi['digi_code'])

    return(result)
## end def add_ports():

# vim: syntax=python ts=4 sw=4 showmatch et :

