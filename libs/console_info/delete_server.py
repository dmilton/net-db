#

#  9 Sep 2014   Add exception for deleting zero rows.
# 20 Nov 2013   initial creation.

############################################################################
# delete_server
# Given a server dictionary, delete the server from the database.
# Entry:
#        theServer - the server to delete.
# Exit: the server has been deleted from the database.


def delete_server(theServer):
    global _consoleServer

    # Delete all of the ports for this server.
    myWhere = "server_id = '" + theServer['server_id'] + "'"
    ignore = db_access.delete_table_rows(_consoleDigiPort,
                        myWhere, cascade=True)

    # Delete the server itself.
    myWhere = "server_mac = '" + theServer['server_mac'] + "'"
    rows = db_access.delete_table_rows(_consoleServer, myWhere, cascade=True)
    if ( rows == 0 ):
        raise NetException("UnknownServer",
                "Could not delete server with MAC: " + theDigi['digi_mac'])

    return()
## end def delete_server():

# vim: syntax=python ts=4 sw=4 showmatch et :

