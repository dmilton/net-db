#

# Test each routine in the module.

import debug
import ipaddr
from datetime import datetime

# Tables require various details to be accessed. Here are the dictionary
# layouts which are required to deal with the tables.

# tableInfo = {
#   'database'      the name of the database the table resides in.
#   'table'         the name of the table.
#   'role'          the userid/role which has read/write access.
#   'pass'          passwod for 'role'
#   'index'         the field name of the index. If compound then there
#                   are multiple fields in the table which uniquely
#                   identify a single record in the table.
# * 'selectFields'  list of fields in the table in the form useable for
#                   a SELECT query.
# * 'selectKeys'    list of input keys and dictionary keys for the fields
#                   in this table. Used to translate the results of a SELECT
#                   query (using selectFields) into a dictionary.
# * 'tableFields'   a dictionary of information, one entry for every field
#                   in the table itself. This is used for formatting to ensure
#                   values in a select, update, or insert are quoted when
#                   necessary. Checks against length are also made.
# * 'cidrFields'    a list of fields in the table which are type postgres cidr.
# * 'dateFields'    a list of fields in the table which are type postgres date.
# }
# * --> field is added by db_access.init_db_table call.
#
# Once a table has been iniitalized the tableFields dictionary contains
# one key/tag for each column in the table. Each dictionary contains two
# values which are taken from a query against the DB schema itself:
# SELECT column_name,data_type,character_maximum_length,column_default
#     FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'theTable'
# The keys are 'type', 'size', and 'default'.

############################################################################
# Module global variables:

# What host/port is to be used for database access?
# A dictionary with one key for each database that is initialized.
# Each entry in the dictionary is another dictionary containing
# the following:
#   theServer  FQDN:TCP-PORT
#   For each table in the database the following entry must exist:
#   role        the user name/role for access to this table.
#   password    the password for the above role.


if __name__ == "__main__":
    import sys

    try:
        from sys import version_info
    except:
        print("Your version of python is too old. Require Python 3")
        sys.exit(1)

    if version_info < 3
        print("Your version of python is too old. Require Python 3")
        sys.exit(1)
    
    try:
        import psycopg2
    except:
        print("This code requires psycopg2.")
        print("see: http://initd.org/psycopg/docs/")
        sys.exit(1)

    try:
        import ipaddr
    except:
        print("This code requires ipaddr.")
        print("see: http://code.google.com/p/ipaddr-py/")
        sys.exit(1)

    try:
        import db_access
    except:
        print("This code requires db_access to be installed.")
        sys.exit(1)


# vim: syntax=python ts=4 sw=4 tw=78 showmatch et :

