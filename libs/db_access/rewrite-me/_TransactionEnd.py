#\L
# 23 Feb 2012				initial creation.

############################################################################
# _TransactionEnd
#
# Finishes the transaction started by TransactionStart.
# See the following for details:
# http://wiki.postgresql.org/wiki/Transactions_recovering_failures_in_scripts
#
# Entry:
#		dbName - the name of the database.
#		dbHost - the host where the database resides.
#		dbUser - the user to run the queries with.
#		dbPass - the user password for the queries.
#		sp  - the name of the save point or netSavePoint by default.
# Exit: none, a savepoint has been created.
#

def _TransactionEnd(dbName, \
		dbHost="", dbUser="",dbPass="", sp="netSavePoint"):
	"""
	Close of a transaction block and commit the changes.
	"""
	debug.debug(1, "_TransactionEnd: dbName=" + dbName)
	debug.debug(1, "_TransactionEnd: sp=" + sp)

	global _dbServer

	# Python PostgreSQL DB-API module.
	import pgdb

	# Our query is:
	sqlQuery = "RELEASE %s; SAVEPOINT %s;" + \
				"ROLLBACK TO SAVEPOINT %s; " + \
				"RELEASE %s; COMMIT;" % (sp, sp, sp, sp)

	if ( networkingConfig.dbEnabled == True ):
		if ( dbHost == "" ):
			dbHost = dbServer[dbName]['host']
		if ( dbUser == "" ):
			dbUser = dbServer[dbName]['look']
		if ( dbPass == "" ):
			dbPass = networkingConfig.dbPasswd[dbUser]

		debug.debug(2, "PgQuery: dbName=" + dbName)
		debug.debug(2, "PgQuery: dbHost=" + str(dbHost))
		debug.debug(2, "PgQuery: dbUser=" + dbUser)
		# debug.debug(2, "PgQuery: dbPass=" + dbPass)

		# Get a connection to the database.
		try:
			db = pgdb.connect(database=dbName, host=dbHost,
					user=dbUser, password=dbPass)
		except pgdb.InternalError, err:
			import re
			errChk = re.match("could not connect to server", str(err))
			if ( errChk != None ):
				raise DbException, ("DbNoConnect", str(err))
			else:
				raise DbException, ("PgDbError", str(err))
			## end else if ( errChk != None ):
		## end try

		cursor = db.cursor()

		# Now that we have a connection, send our query.
		cursor.execute(sqlQuery)
		debug.debug(2, "PgQuery: Query result has %s rows" % \
				(cursor.rowcount))
		# Get the result of the query.
		if ( cursor.rowcount > 0 ):
			try:
				result = cursor.fetchall()
			except pgdb.DatabaseError, err:
				if ( str(err) == "last query did not return tuples." ):
					result = cursor.rowcount
				else:
					raise DbException, ("PgDbError", str(err))
				## end if ( str(err) == "query did not return tuples." ):
			except:
				raise
			## end try
		else:
			result = cursor.rowcount
		## end else if ( cursor.rowcount() > 0 ):
		db.commit()
	else: ## dbEnabled == False
		raise DbException, ("DatabaseDisabled", "")
	## end if ( dbEnabled == True ):

	debug.debug(1, "_TransactionEnd: exit")
	return()
## end _TransactionEnd()

