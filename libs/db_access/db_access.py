#
g_version='24 Sep 2014 - 3.0.12'

# 20 May 2014   Remove google ipaddr library support.
#  7 Mar 2014   Add use of python3.3 ipaddress library.
# 18 Oct 2013   Remove debug import, no longer required.
# 28 Feb 2013   Add import for datetime class.
# 30 Jan 2013   Add import for ipaddr.
# 21 Jan 2013   Nothing to import from networkingConfig any more.
# 13 Nov 2012   Only import netLib from networkingConfig.
#  7 Nov 2012   Expand/change the definition for dbServer so a more
#               granular access to tables could be defined.
#  5 Nov 2012   Update to reflect new functionality where the table
#               structure is taken directly from the schema rather
#               than from a user defined dictionary.
#  1 Sep 2010   Add information about having compound and sequence
#               key types defined for a table.
#               Add WhereClause to provide generic building of the
#               where clause of an SQL query.
# 31 Aug 2010   Need more of networking config. Replace the single
#               value import with the entire module. Add global
#               variable dbServer.

import csv
import os
from datetime import datetime

# Python 3.3 ipaddress library.
import ipaddress

# postgres database library.
import psycopg2

# Get the user name.
from getpass import getuser

# Tables require various details to be accessed. Here are the dictionary
# layouts which are required to deal with the tables.

# tableInfo = {
#   'database'      the name of the database the table resides in.
#   'table'         the name of the table.
#   'role'          the userid/role which has read/write access.
#   'index'         the field name of the index. If compound then there
#                   are multiple fields in the table which uniquely
#                   identify a single record in the table.
# * 'tableFields'   a dictionary of information, one entry for every field
#                   in the table itself. This is used for formatting to ensure
#                   values in a select, update, or insert are quoted when
#                   necessary. Checks against length are also made.
# }
# * --> field is added by db_access.init_db_table call.
#
# Once a table has been iniitalized the tableFields dictionary contains
# one key/tag for each column in the table. Each dictionary contains two
# values which are taken from a query against the DB schema itself:
# SELECT column_name,data_type,character_maximum_length,column_default
#     FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'theTable'
# The keys are 'type', 'size', and 'default'.

############################################################################
# Module global variables:

# What host/port is to be used for database access?
# A dictionary with one key for each database that is initialized.
# Each entry in the dictionary is another dictionary containing
# the following:
#   theServer  FQDN:TCP-PORT
#   For each table in the database the following entry must exist:
#   role        the user name/role for access to this table.
#   password    the password for the above role.

_dbServer = { } 

_dbEnabled = True

_dbPasswords = []


#
# 15 Nov 2012   moved to the error handler where it belongs.

############################################################################
# Database Exception class.

class DbException(Exception):
    """Class for all exceptions raised by the database."""

    def __init__(self, name, detail):
        self.name = name
        self.detail = detail
        return
    ## end def __init__(self)

    def __str__(self):
        return repr(self.detail)
    ## end def __str__(self):

## end class DbException():


#

# 29 Mar 2013   Added new error messages.
# 11 Jul 2011   initial creation.

############################################################################
# Database Error handler.
# Use exit code values of 100 through 199

def db_error_handler(theError):
    result = 0

    if ( theError.name == "DbNoConnect" ):
        print("The database server may be down.")
        print("The error from the database was:")
        result = 100
    elif ( theError.name == "InvalidParameter" ):
        print("Invalid data type in parameters, list required.")
        result = 101
    elif ( theError.name == "NoSchemaAccess" ):
        print("The table permissions are incorrect, cannot read schema.")
        result = 102
    elif ( theError.name == "NoIndexAccess" ):
        print("The table has no index information.")
        result = 103
    elif ( theError.name == "DbFieldTooLong" ):
        print("The field was longer that the table column can hold.")
        result = 104
    elif ( theError.name == "NothingToUpdate" ):
        print("The update request did not contain anything to update.")
        result = 105
    else:
        print("An unknown database error error has occurred...")
        result = 199
    ## end if ( theError.name = X ):
    print( (str(theError.detail)) )

    return(result)
## end db_error_handler():


############################################################################
# check_db_active()

def check_db_active():
    global _dbEnabled

    if ( _dbEnabled == False ):
        print("The networking database is currently shut down.")
        print("Please try again later.")
        sys.exit(0)
    ## end if ( _dbEnabled == False ):

## end check_db_active():


#\L

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 02 Mar 1013   initial creation.

############################################################################
# read_pg_pass
def read_pg_pass():

    global _dbPasswords

    # Initialize our password variable.
    _dbPasswords = []

    # Find the .pgpass file in the user home directory.
    pgPass = os.environ['HOME'] + '/.pgpass'

    # Read the file and make a dictionary of it.
    try:
        f = open(pgPass, 'rb')
        reader = csv.DictReader(f, delimiter=':', quoting=csv.QUOTE_NONE)
        for row in reader:
            _dbPasswords.append(row)
    except:
        print(("Unable to open " + pgPass + " file. Cannot continue.."))
        raise

    return
## end read_pg_pass()


#

# 22 Apr 2014   Add code to extract the version of the database server.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  2 Mar 2013   Remove dbPass. Use .pgapass instead.
#  7 Nov 2012   extend the dbServer value so multiple databases could
#               be opened during the same session.
#  2 Nov 2012   Initial writing.

############################################################################
# init_db_server(serverName, serverPort, dbName, dbUser)
#
# Entry:
#        serverName - the name of the server to establish a connection to.
#        serverPort - the TCP port number the postgres server is running on.
#        dbName - the name of the database to open.
#        dbUser - the name of the user to use for connections.
#
# Set the server to use.

def init_db_server(serverName, serverPort, dbName, dbUser):

    global _dbServer, _dbPasswords

    # Extract the password from _dbPasswords (.pgpass)
    if ( len(_dbPasswords) > 0 ):
        for pwEntry in _dbPasswords:
            if ( (pwEntry['username'] == dbUser) \
                    and (pwEntry['database'] == dbName) \
                    and (pwEntry['hostname'] == serverName) ):
                if ( pwEntry['port'] == '*' or pwEntry == serverPort ):
                    dbPass = pwEntry['password']
    else:
        # try an empty password. this shouldn't work!
        dbPass = ''

    # The design assumption here is that the tables in the database
    # all have select and/or update capability by the same user. This
    # allows for a single connection to be created now and then individual
    # cursor's for each query.
    if ( (dbName in _dbServer) == False ):
        # We haven't seen this database before.
        _dbServer[dbName] = {}
        _dbServer[dbName]['serverName'] = serverName
        _dbServer[dbName]['serverPort'] = serverPort
        _dbServer[dbName]['dbUser'] = dbUser
        _dbServer[dbName]['dbConn'] = \
            psycopg2.connect(host=serverName, port=serverPort, \
                database=dbName, user=dbUser, password=dbPass)

        # now that the database connection is open, get the version.
        cur = _dbServer[dbName]['dbConn'].cursor()
        cur.execute("SELECT version();")
        dbResult = cur.fetchall()
        cur.close()
        _dbServer[dbName]['dbVer'] = dbResult[0][0]
    
## end init_db_server()


#\L

# 16 Apr 2014   initial creation.

############################################################################
# get_db_version
# Entry: dbName - name of the database.
# Exit: result - the output of SELECT version();
# Exceptions:

def get_db_version(dbName):
    global _dbServer

    if ( dbName in _dbServer ):
        if ( 'dbVer' in _dbServer[dbName] ):
            result = _dbServer[dbName]['dbVer']
        else:
            raise ValueError("The database " + dbName + 
                " is not initialized.")
    else:
        raise ValueError("The database " + dbName + " is unknown.")

    return(result)
## end get_db_version()


#\L

# 11 Aug 2014   Add udt_name to the information for each column so
#               the type for values of an array can be determined.
#               For clarity, change the column information extract
#               to use the column names from the schema rather than
#               renaming them. Did not change the name in the dictionary
#               as this will potentially cause problems elsewhere.
# 24 Oct 2013   Add is_nullable to the information for each column.
# 29 Mar 2013   Views do not have index information so failure on
#               an attempt is incorrect behaviour.
#               Date fields being converted to datetime objects
#               so sql format needs to be removed.
#  6 Mar 2013   Add check for inet fields and build a list for conversion.
# 27 Feb 2013   Add check for date fields and build a list for conversion.
# 30 Jan 2013   Add check for cidr fields and build a list so these
#               values can be converted into ipaddr objects.
# 30 Oct 2012   initial creation.

############################################################################
# init_db_table
# Perform Initialization of the table structure information given the
# database and table name.
# Entry: tableInfo - the table information dictionary.
# Exit: The tableInfo dictionary has new entries which are for the
#        database routines:
#    1)    tableFields - a dictionary of information, one entry for every
#        field in the table itself. This is used for i/o formatting to
#        ensure that columns that require quoting are actually quoted.
#    2)    cidrFields - list of fields which are of type cidr.
#    3)    inetFields - list of fields which are of type inet.
#    4)    indexFields - primary key

def init_db_table(tableInfo):

    global _dbServer

    # extract the table name from tableInfo.
    dbName = tableInfo['database']
    tableName = tableInfo['table']
    result = {}

    # Get the information regarding this table from the database.
    colNames = "column_name,data_type,udt_name,character_maximum_length,"
    colNames += "column_default,is_updatable,is_nullable"
    myQuery = "SELECT " + colNames + " FROM INFORMATION_SCHEMA.COLUMNS "
    myQuery += "WHERE table_name = '" + tableName + "';"
    
    cur = _dbServer[dbName]['dbConn'].cursor()
    cur.execute(myQuery)
    tableFieldInfo = cur.fetchall()
    cur.close()

    # If we get no response, exit with an error.
    if len(tableFieldInfo) == 0:
        raise DbException ("NoSchemaAccess", \
                dbName + "/" + tableName + '\n' + \
                str(_dbServer[dbName]['dbConn']) )
    ## end if len(tableFieldInfo) == 0:

    # The table information is similar to:
    # +-------------+--------------------------------+----------------------+
    # | column_name |          data_type             | character_max_length |
    # +-------------+--------------------------------+----------------------+
    # | a-char-var     | character varying           |                   32 |
    # | an-int-var     | integer                     |                      |
    # | active         | boolean                     |                      |
    # | lan_mac        | macaddr                     |                      |
    # | network        | cidr                        |                      |
    # | removal_date   | date                        |                      |
    # | create_date    | timestamp without time zone |                      |
    # | assign_date    | timestamp with time zone    |                      |

    # Now collect the index information.
    colNames = "i.relname as index_name,"
    colNames += "array_to_string(array_agg(a.attname), ', ') as column_names"
    myQuery = "SELECT " + colNames + " "
    myQuery += "FROM pg_class t, pg_class i, pg_index ix, pg_attribute a "
    myQuery += "WHERE t.oid = ix.indrelid AND i.oid = ix.indexrelid "
    myQuery += "AND a.attrelid = t.oid AND a.attnum = ANY(ix.indkey) "
    myQuery += "AND t.relkind = 'r' AND t.relname = '" + tableName + "' "
    myQuery += "GROUP BY t.relname, i.relname "
    myQuery += "ORDER BY t.relname, i.relname;"

    cur = _dbServer[dbName]['dbConn'].cursor()
    cur.execute(myQuery)
    tableIndexInfo = cur.fetchall()
    cur.close()

    # The index query result looks like this:
    # +----------------------+--------------+
    # |      index_name      | column_names |
    # +----------------------+--------------+
    # | network_assigns_bldg | bldg_code    |
    # | network_assigns_pkey | name         |
    # +----------------------+--------------+

    # The index query can also look like this:
    # +------------------+--------------------------+
    # |    index_name    |       column_names       |
    # +------------------+--------------------------+
    # | column_info_pkey | table_column, table_name |
    # +------------------+--------------------------+

    # If we get no response we cannot determine the primary key.
    if len(tableIndexInfo) == 0:
        primaryKey = ""
    else:
        # We only care about the primary key at the moment.
        for anIndex in tableIndexInfo:
            (indexName, colNames) = anIndex
            if "_pkey" in indexName:
                primaryKey = colNames

    # Initialize our field information.
    cidrFields = []        # These need conversion manually on each select.
    inetFields = []
    tableFields = {}    # details of the table itself
    first = True
    for aField in tableFieldInfo:
        (column_name, data_type, udt_name, character_maximum_length,
                column_default, is_updatable, is_nullable) = aField

        # Create the field info dictionary.
        fieldInfo = {}
        fieldInfo['type'] = data_type
        # Field prefixed with an _ if the type is an array.
        fieldInfo['udt_name'] = udt_name
        fieldInfo['size'] = character_maximum_length
        fieldInfo['default'] = column_default
        fieldInfo['update'] = ( is_updatable == "YES" )
        fieldInfo['notNull'] = ( is_nullable == "NO" )
        if ( column_name in primaryKey ):
            fieldInfo['pkey'] = True
            fieldInfo['update'] = False
        else:
            fieldInfo['pkey'] = False
        if ( data_type == 'cidr' ):
            cidrFields.append(column_name)
        elif ( data_type == 'inet' ):
            inetFields.append(column_name)
        ## end elif.
        tableFields[column_name] = fieldInfo

    ## end for aField in tableFieldInfo:

    # Create a dictionary to add to tableInfo.
    result['tableFields'] = tableFields
    result['cidrFields'] = cidrFields
    result['inetFields'] = inetFields
    result['indexFields'] = primaryKey

    return(result)
## end init_db_table()


#\L

# 13 May 2014   Add call to close the change log should it be open.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 12 Mar 2013   Add check for key incase close called on alredy closed db.
#  8 Nov 2012   initial creation.

############################################################################
# close_db_conn
def close_db_conn(dbName):

    global _dbServer

    if ( 'dbConn' in _dbServer[dbName] ):
        _dbServer[dbName]['dbConn'].close()
        del _dbServer[dbName]['dbConn']

    try:
        _close_change_log()
    except:
        pass

    return
## end close_db_conn()


#\L
#  5 Nov 2012   initial creation.

############################################################################
# new_table_row
# Create a dictionary containing empty values for a row in the specified table.

def new_table_row(theTable):

    # Start with an empty dictionary.
    result = {}

    # Loop through the valid keys of the table and assign each one None.
    for aRow in list(theTable['tableFields'].keys()):
        result[aRow] = None

    return(result)
## end new_table_row()


#

# 20 Sep 2013   Move the code that converts the db result from a list of lists
#               into a list of dictionaries into a separate routine so it
#               can be used to convert the INSERT/UPDATE queries RETURNING.
#  3 May 2013   Inconsistiencies with the order of items returned by .keys()
#               caused minor changes here and in table initialization.
# 29 Mar 2013   Tables with date fields are converted by psycopg if
#               a format is not used on the select. No need to convert
#               to a datetime object.
# 21 Feb 2013   When None is the value of a cidr field (ie a vlan that
#               is not routed) conversion using ipaddr cannot occur.
# 11 Feb 2013   Ipaddr conversion must occur after the row is turned
#               into a dictionary.
# 30 Jan 2013   add ipaddr type conversion for cidr table values.
#  6 Nov 2012   update to allow a sorting order.
#  1 Nov 2012   initial creation.

############################################################################
# get_table_rows
# Get a row (or rows) from theTable which match the key value keyValue.
# Entry:    theTable - dictionary describing the table.
#            whereClause - clause to use to restrict the query to some rows.
#            sortOrder - order by clause to sort the resulting rows.
# The result is a list containing one dictionary of values for each row
# returned from the database. Each dictionary has values with keys that
# match the names of the columns in the table.

def get_table_rows(theTable, whereClause, sortOrder = '', limit = 0):

    global _dbServer

    # Start with an empty list.
    result = []

    #
    # build the SQL query.
    #
    tableKeys = list(theTable['tableFields'].keys())
    myQuery = "SELECT " + ','.join(tableKeys) + " FROM " + theTable['table']
    # Add a where clause.
    if ( whereClause > '' ):
        myQuery += " WHERE " + whereClause

    # Add the order by clause.
    if ( sortOrder > '' ):
        myQuery += " ORDER BY " + sortOrder

    # Add the limit clause.
    if ( limit > 0 ):
        myQuery += " LIMIT " + str(limit)

    # Terminate the query statement.
    myQuery += ";\n"

    #
    # Get the rows from the table.
    #
    cur = _dbServer[theTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[theTable['database']]['dbConn'].commit()
    cur.close()

    # Now process the result, converting the lists into dictionaries.
    result = _convert_result(theTable, dbResult)
    
    return(result)
## end get_table_rows()


#\L

# 24 Apr 2014   Add a log of the query.
# 20 Sep 2013   Return a result by adding a RETURNING clause to the
#               INSERT/UPDATE query.
#  8 Apr 2013   No longer return a result. Since whereClause is optional
#               there is no way to reliably determine what was just
#               inserted or updated. This task must be the responsibility
#               of the calling routines.
# 25 Feb 2013   Make whereClause optional and test for an empty string
#               so an insert of a new value in a table with SERIAL
#               indexes will work cleanly.
#  5 Nov 2012   initial creation.

############################################################################
# set_table_row
# Set the values in a table row. Will perform an INSERT or UPDATE as
# appropriate. The result is the contents of the row after the update.
# Entry:theTable - dictionary describing the table.
#        rowData - dictionary of column values for a single row.
#        whereClause - uniquely identify a row in the table to update.
# Exit: the data has been inserted or updated in the database. It is up
#        to the calling routines to fetch the updated data if this is required.

def set_table_row(theTable, rowData, whereClause=''):
    global _dbServer, _changeLog

    # Start by attempting to get the row to see if it already exists.
    rowCount = 0
    if ( whereClause != '' ):
        rowCount = get_row_count(theTable, whereClause)
    ## end if ( whereClause != '' ):

    # Did we get a result?
    if ( rowCount == 0 ):
        # Row does not exist so this is an INSERT.
        myQuery = _insert_query(theTable, rowData)
    else:
        # Row exists so this is an UPDATE.
        try:
            myQuery = _update_query(theTable, rowData, whereClause)
        except:
            # If we get here there's nothing to do as the only failure
            # result from _update_query is there are no field to update.
            myQuery = ''
    ## end else if ( len(dbResult) == 0 ):
    
    if (myQuery > ''):
        # Process the query we generated, result is newly inserted/updated
        # table values.
        cur = _dbServer[theTable['database']]['dbConn'].cursor()
        cur.execute(myQuery)
        dbResult = cur.fetchall()
        _dbServer[theTable['database']]['dbConn'].commit()
        cur.close()
    ## end if (myQuery > ''):

    result = _convert_result(theTable, dbResult)

    try:
        _log_change(myQuery)
    except:
        pass

    return(result)
## end set_table_row()


#\L

# 12 Aug 2014   Update to pass udt_name value to _quote_field so array's
#               can be formatted correctly.
# 21 May 2014   initial creation.

############################################################################
# change_table_rows
# Change the value of all rows matching one value with another.
# Entry:
#       the_table   dictionary describing the table.
#       column_name name of column to change the value for.
#       old_value   the old value to use for the update.
#       new_value   the new value to use for the update.
#       cascade     cascade the change to all effected tables.
# Exit:
#       all rows which have the old value in the specified column
#       now have the new value.

def change_table_rows(the_table, column_name, old_value, new_value, cascade):
    global _dbServer, _changeLog

    # Create an update query.
    updateColumn = {}
    updateColumn[column_name] = new_value

    # Update query
    myQuery = "UPDATE " + the_table['table'] + " SET " + column_name
    myQuery += " = " + _quote_field(new_value,
            the_table['tableFields'][column_name]['type'],
            the_table['tableFields'][column_name]['udt_name'])
    # where clause:
    myQuery += " WHERE " + column_name + " = " + _quote_field(old_value,
            the_table['tableFields'][column_name]['type'],
            the_table['tableFields'][column_name]['udt_name'])
    myQuery += ";"

    # Process the query we generated.
    cur = _dbServer[the_table['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[the_table['database']]['dbConn'].commit()
    cur.close()

    try:
        _log_change(myQuery)
    except:
        pass

    return(dbResult)
## end change_table_rows()


#\L

#  9 Sep 2014   Add result of the number of rows deleted.
# 24 Apr 2014   Add a log of the query.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  4 May 2013   Remove limit as it is not applicable to delete.
# 25 Feb 2013   Change the query creation to simplfy reading.
#               Removed the fetchall as there's nothing to fetch when
#               a row is deleted from a table.
#  6 Nov 2012   initial creation.

############################################################################
# delete_table_rows
# Delete a row (or rows) from the table. Returns count of the number of
# rows deleted.
#
# Entry:theTable - dictionary describing the table.
#        whereClause - where clause to uniquely identify the row(s) to delete.
# Exit:    the rows specified have been deleted.

def delete_table_rows(theTable, whereClause):

    global _dbServer

    myQuery = "DELETE FROM " + theTable['table']
    myQuery += " WHERE " + whereClause + ";"

    cur = _dbServer[theTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    result = cur.rowcount;
    _dbServer[theTable['database']]['dbConn'].commit()
    cur.close()

    try:
        _log_change(myQuery)
    except:
        pass

    return(result)
## end delete_table_rows()


#\L

# 24 Apr 2014   Add a log of the query.
# 23 Sep 2013   Add code to use the returning clause of insert
#               so a second select is no longer required.
# 18 Apr 2013   initial creation.

############################################################################
# copy_table_rows
# Copy a record from one table to another. The default behaviour is to
# move only one record based on the where clause provided.
#
# Entry:
#        srcTable - dictionary describing the source table.
#        destTable - dictionary describing the destination table.
#        whereClause - where clause describing source rows.
#        sortOrder - the order rows are to be selected from srcTable.
#        limit - restrict source rows to this number.
# Exit:    The row has been copied from srcTable to destTable.
#        The response returned is the values from the destTable.

def copy_table_rows(srcTable, destTable, whereClause, sortOrder='', limit=1):
    result = []
    
    # insert into network_links 
    #        ( name,vlan_tag,network,bldg_code,prime_net,assign_date,assign_by,
    #        net_type)
    #    (select name,vlan_tag,network,bldg_code,prime_net,assign_date,
    #        assign_by,net_type 
    #        from network_free
    #        where net_type = 'link'
    #        order by network
    #        limit 2
    #    ) RETURNING 
    #        name,vlan_tag,network,bldg_code,prime_net,
    #        assign_date,assign_by,net_type;

    # extract the basics of the table. tableFields, index, database
    # cidrFields, role, table.
    if ( ('tableFields' in srcTable) == False ):
        srcTable.update(init_db_table(srcTable))
        print(("Missing table initialization for table: " + srcTable['table']))
    if ( ('tableFields' in destTable) == False ):
        destTable.update(init_db_table(destTable))
        print(("Missing table initialization for table: " + destTable['table']))

    # Figure out the field names that need to be updated.
    # set(a) & set(b) returns common items between two lists a and b.
    # list(x) returns a list from set x.
    commonFields = ', '.join( list( set( srcTable['tableFields'].keys() ) \
            & set( destTable['tableFields'].keys() ) ) )

    # Start with the insert.
    myQuery = "INSERT INTO " + destTable['table'] + " \n"
    # Add the list of fields.
    myQuery += " (" + commonFields + ") \n"
    # Now construct the SELECT inside parenthesis.
    myQuery += "(SELECT " + commonFields + " \n"
    myQuery += "FROM " + srcTable['table'] + " \n"
    myQuery += "WHERE " + whereClause + " \n"
    if ( sortOrder > '' ):
        myQuery += "ORDER BY " + sortOrder + " \n"
    if ( limit > 0 ):
        myQuery += "LIMIT " + str(limit)
    myQuery += ") RETURNING " + \
        ','.join(list(destTable['tableFields'].keys())) + ";"

    # Process the query we generated, result is new row list in destTable.
    # This duplicates the rows from srcTable into destTable.
    cur = _dbServer[destTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[destTable['database']]['dbConn'].commit()
    cur.close()

    result = _convert_result(destTable, dbResult)

    try:
        _log_change(myQuery)
    except:
        pass

    return(result)
## end copy_table_rows()


#\L

# 18 Apr 2013   initial creation.

############################################################################
# move_table_rows
# Move rows from one table to another.
# Entry:
#       srcTable - dictionary describing the source table.
#       destTable - dictionary describing the destination table.
#       whereClause - where clause describing source rows.
#       sortOrder - the order rows are to be selected from srcTable.
#       limit - restrict source rows to this number.
# Exit: The row has been copied from srcTable to destTable.
#       The response returned is the values from the destTable.

def move_table_rows(srcTable, destTable, whereClause, sortOrder='', limit=1):

    # Copy the rows.
    result = copy_table_rows(srcTable, destTable, whereClause, sortOrder, limit)

    # Noww delete them from srcTable
    delete_table_rows(srcTable, whereClause)

    return(result)
## end move_table_rows()


#\L

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 18 Dec 2012   initial creation.

############################################################################
# get_row_count
# Get the count of rows which match the where clause
# Entry:    theTable - dictionary describing the table.
#            whereClause - clause to use to restrict the query to some rows.
# The result is a list containing one dictionary of values for each row
# returned from the database. Each dictionary has values with keys that
# match the names of the columns in the table.

def get_row_count(theTable, whereClause=''):

    global _dbServer

    # assume zero rows.
    result = 0

    #
    # build the SQL query.
    #
    myQuery = "SELECT count(*) FROM %s" % (theTable['table'],)
    # Add a where clause.
    if ( whereClause > '' ):
        myQuery += " WHERE " + whereClause
    ## end if ( whereClause > '' ):

    # Terminate the query statement.
    myQuery += ";\n"

    # Run the query against the database.
    cur = _dbServer[theTable['database']]['dbConn'].cursor()
    cur.execute(myQuery)
    dbResult = cur.fetchall()
    _dbServer[theTable['database']]['dbConn'].commit()
    cur.close()

    # Query results are always a list of tuples.
    result = int(dbResult[0][0])

    return(result)
## end get_row_count()


#\

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 17 Jan 2013   Check all values in aTableRow which appear in keyList
#               so a 'None' is set to an empty string.
#  6 Nov 2012   Initial creation.

##############################################################################
# make_list(aTableRow, keyList)
# Entry:
#        aTableRow - dictionary of a single row's data.
#        keyList - list of fields desired in result.
# Exit: a list of the values for keys in keyList taken from aTableRow.

def make_list(aTableRow, keyList):

    result = []
    for i in range(len(keyList)):
        if ( aTableRow[ keyList[i] ] == None ):
            result.append( '' )
        else:
            result.append( aTableRow[ keyList[i] ] )
        ## end else if ( aTableRow[ keyList[i] ] == None ):
    ## end for i in range(len(keyList))

    return(result)
## end make_list()


#\L

# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
# 30 Oct 2012   initial creation.

############################################################################
# get_table_keys
# Build a dictionary of keys and types for use to help parsing and
# verification in cmd_line_input.
# Entry: tableInfo - the table information dictionary.
#    3)    tableFields - a dictionary of information, one entry for every
#        field in the table itself. This is used for i/o formatting to
#        ensure that columns that require quoting are actually quoted.

def get_table_keys(tableInfo):

    global _dbServer

    result = tableInfo['tableFields']

    return(result)
## end get_table_keys()


#\L

# 24 Oct 2013   initial creation.

############################################################################
# get_reqd_cols
# Return a list of table columns that are "NOT NULL" values.

def get_reqd_cols(theTable):

    # field Info:
    notNullFields = []
    fieldInfo = theTable['tableFields']
    for aField in list(fieldInfo.keys()):
        if ( fieldInfo[aField]['notNull'] ):
            notNullFields.append(aField)

    return(notNullFields)
## end get_reqd_cols()


#

# 12 Aug 2014   Update to pass udt_name value to _quote_field so array's
#               can be formatted correctly.
# 28 Feb 2013   filter out any keys which are not common between the
#               columns in the table and the newRow to be added.
# 25 Feb 2013   add initialization of tableFields if it does not exist.
#               This prevents errors when a table has restricted rights
#               for some users but not others.
# 21 Feb 2013   initial creation.

##############################################################################
# _insert_query(theTable, newInfo)
# Generate an insert query for the table described by theTable which
# will have the values of rowData.
#
# Entry:
#        theTable - dictionary describing the table.
#        rowData - a dictionary containing the information to be inserted
#        returning - list of fields to return after insert completes.
#
# Exit: returns the SQL query to perform an insert into the table.

def _insert_query(theTable, newRow, returning=""):

    # extract the basics of the table. tableFields, index, database
    # cidrFields, inetFields, role, table.
    if ( ('tableFields' in theTable) == False ):
        theTable.update(init_db_table(theTable))
        print(("Missing table initialization for table: " + theTable['table']))

    tableFieldInfo = theTable['tableFields']
    indexField = theTable['index']
    # Figure out the field names that need to be updated.
    commonKeys = _common_keys(tableFieldInfo, indexField, newRow)

    # Start with the insert.
    insertQuery = "INSERT INTO " + theTable['table'] + "\n"
    # Add the list of fields.
    insertQuery += " (" + ', '.join(commonKeys) + ") \n"

    # Construct our list of field values.
    valueList = " VALUES\n("
    first = True
    for aKey in commonKeys:
        fieldInfo = tableFieldInfo[aKey]
        # if this is the second and subsequent field to update...
        if ( first == True ):
            first = False
        else:
            valueList += ", "
        ## end else if ( first == True ):
        valueList += _quote_field(newRow[aKey],
                fieldInfo['type'], fieldInfo['udt_name'])
    ## end for aKey in tableInfo.keys()
    valueList = valueList + ") \n"

    # Add our list of values to our insert query.
    insertQuery += valueList

    insertQuery += _returning(tableFieldInfo)

    # Terminate the SQL statement.
    insertQuery += ";\n"

    return(insertQuery)
## end _insert_query():


#

# 12 Aug 2014   Update to pass udt_name value to _quote_field so array's
#               can be formatted correctly.
# 20 Sep 2013   Update to add a returning clause.
#  4 Apr 2013   Argument to _common_keys has been renamed to indexRmForce.
# 25 Feb 2013   add initialization of tableFields if it does not exist.
#               This prevents errors when a table has restricted rights
#               for some users but not others.
# 18 Dec 2012   boolean, numeric, mac and cidr values do not have sizes.
# 18 Dec 2012   initial creation.

##############################################################################
# _update_query(theTable, rowData, whereClause)
# The record to update must exist in the database.
# Entry:
#        theTable - dictionary of information describing the table.
#        newInfo - a dictionary containing the information to be updated
#        whereClause - the comparison string to uniquely identify the row or
#                rows in the table to update.
# Exit:
#        an SQL query to update fields in the table is returned.

def _update_query(theTable, rowData, whereClause):

    # Add fields to update that are present in rowData.
    if ( ('tableFields' in theTable) == False ):
        theTable.update(init_db_table(theTable))

    tableFieldInfo = theTable['tableFields']
    indexField = theTable['index']

    commonKeys = _common_keys(tableFieldInfo, indexField, rowData)
    nothingToUpdate = ( len(commonKeys) == 0 )

    # Construct our UPDATE statement.
    updateQuery = "UPDATE " + theTable['table'] + " SET "

    first = True
    for aKey in commonKeys:
        aFieldInfo = tableFieldInfo[aKey]
        # if this is the second and subsequent field to update...
        if ( first == False ):
            updateQuery += ", "
        ## end if ( first == False )
        updateQuery += aKey + " = "
        updateQuery += _quote_field(rowData[aKey],
                aFieldInfo['type'], aFieldInfo['udt_name'])
        # boolean, numeric, mac and cidr values do not have sizes.
        if ( aFieldInfo['size'] != None and 
                len(rowData[aKey]) > aFieldInfo['size'] ):
            err = aKey + ", " + len(rowData[aKey]) 
            err += " > " + str(aFieldInfo['size'])
            raise DbException("DbFieldTooLong", err)
        ## end if ( aFieldInfo['size'] < size(newInfo[aKey] ):
        first = False
    ## end for aKey in theTable['selectKeys']:

    if ( nothingToUpdate == True ):
        raise DbException ("NothingToUpdate", \
                "No fields were provided to update")
    else: 
        if ( whereClause != '' ):
            updateQuery += " WHERE " + whereClause
        ## end if ( whereClause != '' ):
    ## end else if ( nothingUpdated == True ):

    updateQuery += _returning(tableFieldInfo)

    return(updateQuery)
## end _update_query():


#\L

# 20 Sep 2013   initial creation.

############################################################################
# _returning
# Create a returning clause which fetches all of the fields of the
# record that was just inserted/updated.

def _returning(tableFieldInfo):

    result = ' RETURNING ' + ','.join(list(tableFieldInfo.keys()))

    return(result)
## end _returning()


#\L
# 20 May 2014   Remove google ipaddr library support.
#  7 Mar 2013   Start using python 3.3 ipaddress library.
# 20 Sep 2013   initial creation.

############################################################################
# _convert_result
# Take the result from the database and convert to a dictionary. Also
# convert the postgres inet and cidr data types to ipaddr types.

def _convert_result(theTable, dbResult):

    result = []

    # Now process the result, converting lists into dictionaries.
    cidrFields = theTable['cidrFields']
    inetFields = theTable['inetFields']
    for aRow in dbResult:
        # Make the row values list into a dictionary.
        dictRow = dict(list(zip(list(theTable['tableFields'].keys()), aRow)))
        # Check and convert any cidr fields.
        if ( len(cidrFields) > 0 ):
            for aField in cidrFields:
                # If we have a value for the cidr field, convert it.
                if ( dictRow[aField] != None ):
                    dictRow[aField] = ipaddress.ip_network(dictRow[aField])
        # Check and convert any inet fields.
        if ( len(inetFields) > 0 ):
            for aField in inetFields:
                # If we have a value for the cidr field, convert it.
                if ( dictRow[aField] != None ):
                    dictRow[aField] = ipaddress.ip_address(dictRow[aField])
        result.append( dictRow )
    ## end for aRow in dbResult:

    return(result)
## end _convert_result()


#

# 23 May 2013   for x in y: will skip items in y if you delete
#               an item from y inside the loop. A copy of y is
#               required in order to allow field deletion.
# 14 May 2013   Change loop through the common keys so index out of range
#               errors cannot occur when a key gets deleted from the list.
#  4 May 2013   Update to handle multi-field keys properly.
#               Removed the indexRmForce argument as it doesn't make
#               sense. The only time an Index should not be part of
#               a query is when it is an insert with a sequence.
# 28 Feb 2013   initial creation.

##############################################################################
# _common_keys(tableFieldInfo, indexField, columnData)
# Determine the set of keys which are common to both the table columns
# and the data to be inserted or updated into the table.
# will have the values of rowData.
#
# Entry:
#        tableFieldInfo - dictionary describing the columns of the table.
#        indexField - name of the field which is the primary table index.
#        columnData - a dictionary containing the values to be inserted/updated.
#
# Exit:
#        Returns a list of field names which are common to the table itself,
#        have valid values in columnData, and the index value will be excluded
#        if it is a sequence.

def _common_keys(tableFieldInfo, indexField, columnData):

    # Find the keys in common between the table and the new column data.
    commonKeys = list( set(tableFieldInfo.keys()) & set(columnData.keys()) )

    # Assume the index does not need to be removed.
    excludeIndex = False
    indexFieldList = indexField.split(',')
    if ( len(indexFieldList) == 1 ):
        # Sequence index fields should be excluded from a typical insert.
        if ( tableFieldInfo[indexField]['default'] != None ):
            if ( "nextval" in tableFieldInfo[indexField]['default'] ):
                # This is a sequence, exclude the field from the Insert.
                excludeIndex = True

    # Copy common keys to the result
    result = list(commonKeys)
    for aKey in commonKeys:
        if ( aKey == indexField and excludeIndex == True ):
            # Remove the index key from the set.
            del result[result.index(aKey)]
        elif ( columnData[aKey] == None ):
            # Remove columnData keys that have no value.
            del result[result.index(aKey)]

    return(result)
## end _common_keys():


#

# 12 Aug 2014   Update to use udt_name value to _quote_field so array's
#               can be formatted correctly. Now a recursive routine since
#               this calls itself for each item in an array.
# 18 Oct 2013   Remove debug print calls since pdb is much easier to use.
#  1 Sep 2010   Add check for compound index types and also
#               add SERIAL types.

##############################################################################
# _quote_field(field_data, data_type)
# The assumption made is that any field that is not numeric in
# nature requires quotes.
#
# Entry:
#       field_data - the field to quote if necessary.
#       data_type - the type (data_type) of field so we know to quote or not.
#       data_type_name - the udt_name field from the database table info.
# Exit:
#        The result is field_data as a string. If the field itself is
#        a string in the table then the result will contain quotes.

def _quote_field(field_data, data_type, data_type_name):

    is_number = ( data_type == 'SERIAL' or 'real' in data_type or
            'int' in data_type_name or 'float' in data_type_name )
    # assume the data needs quotes.
    quote_char = "'"
    if ( data_type == 'ARRAY' ):
        # Array needs '{ nbr, nbr }' as result for numeric and
        # '{ "str", "str" }' as result for all others.
        if ( len(field_data) == 0 ):
            # was passed a list with zero elements.
            result = 'NULL'
        else:
            result = "'{ " + _quote_it(field_data[0], '"', is_number)
            for a_field_item in field_data[1:]:
                result += ', ' + _quote_it(a_field_item, '"', is_number)
            result += " }'"

    else:

        result = _quote_it(field_data, "'", is_number)

    return(result)
## end _quote_field()

# This was split out since it needed to be in
# two places in the code.
def _quote_it(the_data, the_quote, is_number):
    if ( is_number ):
        result = str(the_data)
    else: 
        result = the_quote + str(the_data) + the_quote
    return(result)
## end def _quote_it():


#

# 13 May 2014   Build the query directly so the normal table initialization
#               is not required. This allows a connection to the db to be
#               established using the 'netlog' user.
# 24 Apr 2014   initial writing.

###############################################################################
# Change log table.
#
_changeLog = {
    'database': 'networking',
    'role': 'netlog',
    'table': 'change_log',
    'index': 'change_who,change_when'
}

############################################################################
# _log_change
# Add the change log to the database.
# Entry: theChange - the change that was made.
# Exit: the change has been logged.

def _log_change(theChange):
    """
    theChange = "made a change to the database"
    # 
    _log_change(theChange)
    """

    global _dbServer, _changeLog

    if ( 'dbConn' not in _changeLog ):
        _open_change_log()

    # Make the query to update the change table.
    myQuery = "INSERT INTO " + _changeLog['table'] + "\n"
    myQuery += " (change_who,change_what) VALUES\n("
    myQuery += "'" + getuser() + "', %s);\n"

    # Insert the change into the table.
    curs = _changeLog['dbConn'].cursor()
    curs.execute(myQuery, (theChange,))
    dbResult = cur.fetchall()
    _changeLog['dbConn'].commit()
    curs.close()

    return
## end def _log_change():

############################################################################
# _open_change_log
# Create a connection to the networking database for the change log.
# Entry: none.
# Exit: a connection has been established.

def _open_change_log():
    global _dbServer, _changeLog

    if ( 'dbConn' not in _changeLog ):
        # Open a separate connection to the database for the netlog user.
        _changeLog['serverName'] = _dbServer['networking']['serverName']
        _changeLog['serverPort'] = _dbServer['networking']['serverPort']
        _changeLog['dbUser'] = 'netlog'
        _changeLog['dbConn'] = psycopg2.connect(
                host=serverName, port=serverPort, database='networking',
                user='netlog', password='qr3fyd4z')

## end def _open_change_log():

############################################################################
# _close_change_log
# Close a connection to the networking database for the change log.
# Entry: none.
# Exit: any existing connection for the change log is closed.

def _close_change_log():
    global _dbServer, _changeLog

    if ( 'dbConn' in _changeLog ):
        _changeLog['dbConn'].close()
        del _changeLog['dbConn']

## end def _close_change_log():


# vim: syntax=python ts=4 sw=4 showmatch et :
