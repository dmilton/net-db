# Update stuff:
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install build-essential libncursesw5-dev libgdbm-dev libc6-dev 
sudo apt-get install zlib1g-dev libsqlite3-dev tk-dev
sudo apt-get install libssl-dev openssl

# Downoad and make:
cd ~
wget https://www.python.org/ftp/python/3.4.2/Python-3.4.2.tgz
tar -zxvf Python-3.4.2.tgz
cd Python-3.4.2
./configure
make
sudo make install

# Get Pip
cd ~
wget https://bootstrap.pypa.io/get-pip.py
sudo python3.4 get-pip.py

# Test Install
pip3 --version

# Make just built one the version you use:
sudo ln -sf /usr/local/bin/python3.4 /usr/local/bin/python3

