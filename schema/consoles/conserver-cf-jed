###########
# Step 1. #  Instruction updated April 17/06 BT
#		Updated Jan 14, 2013 DM
###########
# When installing a new etherlite unit you need to first assign an
# IP address and two letter code to the unit itself. Add an entry
# to the table below for that.
#
# To actually assign the IP address to the etherlite you need to
# have the unit connected to the network and powered up. You require
# the MAC address of the unit to assign the IP for the etherlite.
#
##########################################################################
## April 17/06 BT - ignore this section - old instructions
##########################################################################
## Once you have everything, as root run the setup utility on jed:
##
## Sample use:
##    /opt/realport/sparc64/dgipserv -store 00-a0-e7-23-29-d1 192.168.40.28
##
## Command options:
##	/opt/realport/sparc64/dgipserv
##
## usage: /opt/realport/sparc64/dgipserv [flags] hw_addr ip_addr
##  flags can include:
##    -reset    to reset the Etherlite before doing anything else
##    -store    to store the IP address in the module's EEPROM
##    -broadcast same as store but uses broadcast reply
##    -erase    to erase a stored IP from the module's EEPROM
##    -host auth_ip [ auth_netmask ]
##              to add an IP address or list of IP addresses
##              to the module's authorized host list
##              (may be used up to eight times)
##    -firmware filename
##              to upload a firmware image to the EtherLite
##              module once the IP address has been served
## NOTE: the gateway and netmask need only be set for tftp firmware
## updates on complex networks. Not used for port operation.
##    -gateway unit_gateway
##    -netmask unit_netmask
## NOTE: the port parameter is only needed for EtherLite modules
## with RealPort firmware NOT using the default RealPort TCP port.
##    -port tcp_port
##
##  hw_addr is the hardware (MAC) address of the device to
##    be served its IP address (e.g. 00-a0-e7-00-00-00).
##  ip_addr is the IP address to be served. (e.g. 192.168.0.1)
##
## Here is a sample of what you will see when configuring the ethelite IP.
###
## jed$ /opt/realport/sparc64/dgipserv -store 00-a0-e7-23-1a-78 192.168.40.13
##
##############################################################################
#
###########
# Step 2. #
###########
# Run the add-dig-cf script 
#usage: add-digi-cf bldg-code etherlite-code
#        bldg-code       - two letter building code 
#        etherlite-code  - two letter code for etherlite unit.
###############
# NEW Step 2. #
###############
# Run GNU make. Substitute e3 and zz as appropriate for the building code
# and the code you assigned to the DIGI in step 1.
#
# jed:console> gmake new-digi BLDG_CODE=e3 DIGI_CODE=zz
#
# You do not need to use sudo to run this command.
#
# In this example building-code=fa etherlite-code=co
# jed:~/cons> add-digi-cf fa co   
# Now running dgipserv with sudo. You may need to enter your password
# to authenticate to sudo.
# Password:
# Awaiting a BOOTP request from HW address 00:A0:E7:23:4D:1F...
# (This may take up to a minute.)
# None yet...
# None yet...
# None yet...
# None yet...
# None yet...
# None yet...
# Don't give up hope!
# None yet...
# None yet...
# Got one--responding...
#   +---------------------------------------------------------------------+
#   | If the EtherLite module did not have a stored IP address, or if the |
#   | address to be stored is different than the one currently stored,    |
#   | the EtherLite module is now reprogramming itself.  This will take   |
#   | about 10 seconds.  DO NOT DISRUPT POWER TO THE MODULE UNTIL THE     |
#   | "ON" INDICATOR HAS REMAINED STEADILY LIT FOR A FULL 5 SECONDS.      |
#   | Failure to do so could render the EtherLite module inoperable.      |
#   | Once reprogramming has finished, the EtherLite module will be ready.|
#   | (If store doesn't work, try using -broadcast.)                      |
#   +---------------------------------------------------------------------+
# Address successfully served.
# 
###########
# Step 3. #
###########
#
# Now that the etherlite has an IP address assigned, you need to build
# the device entries which represent the serial ports on the unit.
#
# jed:~/cons> sudo drpadmin
# 
# Please select an option (a)dd (d)elete (s)how (r)eset (q)uit : a
# 
# Enter the IP address or network name of the unit: 192.168.40.74
# Enter the number of ports: 16
# Enter the tty device ID (only 2 chars allowed) : cn
# 
# 
# Would you like this RealPort session to be encrypted?
# 
# NOTE: Not all RealPort products support encrypted RealPort sessions.
# Please check your RealPort product's firmware release notes
# or product literature before selecting "always".
# If in doubt, select "never".
# 
# (always/never) : (never): 
# 
# The following device will be configured,
# 63      192.168.40.74    16      cn      secure (never)
# 
# Is this correct (y to add or x to abort) ? y
# 
# Please select an option (a)dd (d)elete (s)how (r)eset (q)uit : a
# 
# Enter the IP address or network name of the unit: 192.168.40.75
# Enter the number of ports: 16
# Enter the tty device ID (only 2 chars allowed) : co
# 
# 
# Would you like this RealPort session to be encrypted?
# 
# NOTE: Not all RealPort products support encrypted RealPort sessions.
# Please check your RealPort product's firmware release notes
# or product literature before selecting "always".
# If in doubt, select "never".
# 
# (always/never) : (never): never
# 
# The following device will be configured,
# 64      192.168.40.75    16      co      secure (never)
# 
# Is this correct (y to add or x to abort) ? y
# 
# Please select an option (a)dd (d)elete (s)how (r)eset (q)uit : q
# 
# 
# 
###########
# Step 4. #
###########
#
# That completes the setup of the etherlite itself. Now continue and
# edit the configuration file for the digi you are installing.
#
# The configuration files will be in /opt2/local/share/console
# In our example, you would edit fa.cf  (Fletcher Argue)
# 
###########
# Step 5. #
###########
#
# Once you edit the building configuration file, create the consolidated
# configuration file...
# on /local/share/cons to check the files and stop and restart the 
# server.
#jed: cd /opt2/local/share/console
#jed: gmake
#
# If there were any errors in the generated configuration file the make
# will fail. Correct those errors and try again.
#
###########
# Step 5. #
###########
# Install the configuration file and restart the console server daemon.
#jed: gmake install
#
###########
# It is possible to upgrade the firmware of our DIGI units. The latest
# file must be in /tftpboot.
#
# dgipserv -g 192.168.40.1 -n 255.255.255.0 -t 192.168.40.2 -f 
#	/tftpboot/elr160.prm -h 130.179.16.160 255.255.255.240 
#	-h 192.168.40.0 255.255.255.0 -r
#	-s 00:A0:E7:23:56:85 192.168.40.69
###########
# This rest of this file is a simple table which maps the two letter device
# id/code assigned to an etherlite to an ip address and building.
# Note that the serial port devices will take the form of:
#   /dev/dty/YY001s through /dev/dty/YY015s (for an el16)
# where YY is the two letter code assigned to the etherlite.
#
# Code	MAC		   	IP		Type	Building
#zz	00-a0-e7-23-1F-1B	192.168.1.11	el16	Pembina CO. 
#zz Removed and re-assigned to Dafoe Trailer Park
at	00-a0-e7-23-1c-14	192.168.40.12	el32	E3-625 EITC
aa	00-a0-e7-23-1A-78	192.168.40.13	el32	E3-625 EITC #2
ac	00-a0-e7-23-26-ce	192.168.40.14	el16	235 Engineering
ad	00-a0-e7-23-26-cd	192.168.40.15	el16	351B Engineering
ae	00-a0-e7-23-26-a6	192.168.40.16	el16	530E Engineering
af	00-a0-e7-23-29-e7	192.168.40.17	el16	107A Admin
ag	00-a0-e7-23-29-ec	192.168.40.18	el16	502 Admin
ah	00-a0-e7-23-29-d2	192.168.40.19	el16	525 UCentre
ai	00-a0-e7-23-26-84	192.168.40.20	el16	114A Machray
aj	00-a0-e7-23-29-c6	192.168.40.21	el16	125 Machray
ak	00-a0-e7-23-29-f4	192.168.40.22	el16	358 Machray
al	00-a0-E7-23-2a-0c	192.168.40.23	el16	525 Machray
am	00-a0-e7-23-29-f5	192.168.40.24	el16	008C Dafoe
an	00-a0-e7-23-29-f2	192.168.40.25	el16	126 Dafoe
ao	00-a0-e7-23-26-cb	192.168.40.26	el16	102F UCentre
ap	00-a0-e7-23-26-a0	192.168.40.27	el16	107A UCentre
aq	00-a0-e7-23-29-d1	192.168.40.28	el16	323 UCentre
ar	00-a0-e7-23-2a-05	192.168.40.29	el16	400L UCentre
as	00-a0-e7-23-11-58	192.168.40.11	el16	253 Helen Glass
#at	see above, installs done before two letter code scheme developed.
au	00-a0-e7-23-2d-0b	192.168.40.30	el16	104B Robson Hall
av	00-a0-e7-23-b2-35	192.168.40.31	el16	P422A Duff Roblin
aw	00-a0-e7-23-26-cc	192.168.40.32	el16	020 Frank Kennedy
ax	00-a0-e7-23-26-c2	192.168.40.33	el16	104 Max Bell
az	00-a0-e7-23-26-c1	192.168.40.34	el16	123 Max Bell
ba	00-a0-e7-23-29-e8	192.168.40.35	el16	213 Human Ecology #2
bb	00-a0-e7-23-11-ad	192.168.40.36	el16	176L ExtEd
bc	00-a0-e7-23-11-41	192.168.40.37	el16	179E ExtEd
bd	00-a0-e7-23-1b-63	192.168.40.38	el16	199C ExtEd
be	00-a0-e7-23-56-81	192.168.40.39	el16	122 IGAC
bf	00-a0-e7-23-2c-c6	192.168.40.40	el16	107 Services
bg	00-a0-e7-23-2c-c2	192.168.40.41	el16	107 PPEM
bh	00-a0-e7-23-3b-2e	192.168.40.42	el16	B20 Arthur Mauro
bi	00-a0-e7-23-3b-2f	192.168.40.43	el16	351B Engineering #2
bj	00-a0-e7-23-2a-06	192.168.40.44	el16	159V Frank Kennedy
bk	00-a0-e7-23-2a-0a	192.168.40.45	el16	The Point (Veg Storage)
bl	00-a0-e7-23-9b-69	192.168.40.46	el16	E3-625B EITC #2
bm	00-a0-e7-23-43-eb	192.168.40.47	el16	122E Tache Hall
bn	00-a0-e7-23-46-0c	192.168.40.48	el16	116A Armes
bo	00-a0-e7-23-3b-62	192.168.40.49	el16	126 Dafoe #2
bp	00-a0-e7-23-43-ed	192.168.40.50	el16	133 Education
bq	00-a0-e7-23-43-e7	192.168.40.51	el16	295 Education
br	00-a0-e7-23-2a-d7	192.168.40.52	el16	124 St Johns
bs	00-a0-e7-23-43-e6	192.168.40.53	el16	105B Allen 
bt	00-a0-e7-23-43-ef	192.168.40.54	el16	210 Buller
bu	00-a0-e7-23-47-97	192.168.40.55	el16	104V Tier
bv	00-a0-e7-23-47-b9	192.168.40.56	el16	248B Drake
bw	00-a0-e7-23-4f-39	192.168.40.57	el16	111 Wallace
bx	00-a0-e7-23-55-e9	192.168.40.58	el16	152 Eng2
by	00-a0-e7-23-55-e8	192.168.40.59	el16	254 Eng2
bz	00-a0-e7-23-55-eb	192.168.40.60	el16	354 Eng2
ca	00-a0-e7-23-48-29	192.168.40.61	el16	454 Eng2
cb	00-a0-e7-23-4e-db	192.168.40.62	el16	554 Eng2
cc	00-a0-e7-23-4e-dc	192.168.40.63	el16	654 Eng2
cd	00-a0-e7-23-4f-47	192.168.40.64	el16	305V Wallace
ce	00-a0-e7-23-4f-73	192.168.40.65	el16	318V Wallace
cf	00-a0-e7-23-53-4d	192.168.40.66	el16	107cor Music
cg	00-a0-e7-23-52-3d	192.168.40.67	el16	127V RCFFN
ch	00-a0-e7-23-56-84	192.168.40.68	el16	227V RCFFN
ci	00-a0-e7-23-56-85	192.168.40.69	el16	E3-625b EITC #1
cj	00-a0-e7-23-50-4c	192.168.40.70	el16	002 Plant Science
ck	00-a0-e7-23-62-06	192.168.40.71	el16	202A CTC
cl	00-a0-e7-23-64-20	192.168.40.72	el16	300E High-Voltage
cm	00-a0-e7-23-5d-6e 	192.168.40.73	el16	209 Fitzgerald
cn	00-a0-e7-23-4f-83 	192.168.40.74	el16	264 St Pauls'
co	00-a0-e7-23-4d-1f 	192.168.40.75	el16	100E Fletcher Argue
cp	00-a0-e7-23-69-71	192.168.40.76	el16	323 Ucentre #2 
cq	00-a0-e7-23-4e-c0	192.168.40.77	el16	105V Russell
cr	00-a0-e7-23-69-6e	192.168.40.78	el16	213 Human Ecology #2
cs	00-a0-e7-23-47-a9	192.168.40.79	el16	527V Allen
ct	00-a0-e7-23-5d-9d	192.168.40.80	el16	104B Robson Hall #2
cu	00-a0-e7-23-56-30	192.168.40.81	el16	204 Drake
cv	00-a0-e7-23-49-a8	192.168.40.82	el16	405 Drake
cw	00-a0-e7-23-5d-87	192.168.40.83	el16	E1-493 EITC
cx	00-a0-e7-23-6b-6b	192.168.40.84	el16	E1-425 EITC
cy	00-a0-e7-23-69-8c	192.168.40.85	el16	E1-293 EITC
cz	00-a0-e7-23-69-63	192.168.40.86	el16	E1-393 EITC
da	00-a0-e7-23-4a-d2	192.168.40.87	el16	E1-593 EITC
db	00-a0-e7-23-5d-6d	192.168.40.88	el16	317 Parker
dc	00-a0-e7-23-1b-4f	192.168.40.89	el16	213V St Andrews.
dd	00-a0-e7-23-68-51	192.168.40.90	el16	105V Russell #2
de	00-a0-e7-23-4a-d2	192.168.40.91	el16	234 Isbister
df	00-a0-e7-23-4f-2e	192.168.40.92	el16	207V Fletcher Argue
dg	00-a0-e7-23-50-4d	192.168.40.93	el16	541V Fletcher Argue
dh	00-a0-e7-23-5d-84	192.168.40.94	el16	101V St. Paul's
di	00-a0-e7-23-5d-83	192.168.40.95	el16	166V St. Paul's
dj	00-a0-e7-23-75-16	192.168.40.96	el16	121C Arch 2
dk	00-a0-e7-23-75-1b	192.168.40.97	el16	171 UCollege
dl	00-a0-e7-23-74-f8	192.168.40.98	el16	135B UCollege
dm	00-a0-e7-23-77-22	192.168.40.99	el16	204 Grains Storage
dn	00-a0-e7-23-78-46	192.168.40.100	el16	005V Animal Science
do	00-a0-e7-23-78-47	192.168.40.101	el16	004 Ag Lecture Blk
dp      00-a0-e7-23-68-50       192.168.40.102   el16    077 Agriculture
dq      00-a0-e7-23-70-a7       192.168.40.103   el16    141 Agriculture
dr      00-a0-e7-23-70-06       192.168.40.104   el16    243 Agriculture
ds      00-a0-e7-23-77-0c       192.168.40.105   el16    346 Agriculture
dt	00-a0-e7-23-7b-d8	192.168.40.106	el16	234 Isbister #2
du	00-a0-e7-23-5d-6b	192.168.40.107	el16	214v Ellis
dv	00-a0-e7-23-80-2f	192.168.40.108	el16	214v Ellis #2
dw	00-a0-e7-23-80-20	192.168.40.109	el16	358 Machray Hall #2
dx	00-a0-e7-23-7f-de	192.168.40.110	el16	305V Wallace #2
dy	00-a0-e7-23-7f-e0	192.168.40.111	el16	318V Wallace #2
dz	00-a0-e7-23-80-29	192.168.40.112	el16	253 Helen Glass #2
ea	00-a0-e7-23-75-35	192.168.40.113	el16	349 Helen Glass
eb	00-a0-e7-23-80-21	192.168.40.114	el16	449 Helen Glass
ec	00-a0-e7-23-7b-ed	192.168.40.115	el16	248B Drake #2
ed	00-a0-e7-23-76-c1	192.168.40.116	el16	104B Robson Hall #3
ee	00-a0-e7-23-42-8a	192.168.40.117	el16	229V Dafoe
ef	00-a0-e7-23-82-45	192.168.40.118	el16	207V Fletcher Argue #2
eg	00-a0-e7-23-82-11	192.168.40.119	el16	541V Fletcher Argue #2
eh	00-a0-e7-23-80-11	192.168.40.120	el16	E1-425 EITC #2
ei	00-a0-e7-23-80-e3	192.168.40.121	el16	135B U College #2
ej	00-a0-e7-23-80-1d	192.168.40.122	el16	317 Parker #2
ek	00-a0-e7-23-77-0f	192.168.40.123	el16	E2-254 EITC #2
el	00-a0-e7-23-84-37	192.168.40.124	el16	234 Isbister #3
em	00-a0-e7-23-84-8a	192.168.40.125	el16	317 Parker #3
en	00-a0-e7-23-80-1e	192.168.40.126	el16	100E Fletcher Argue #2
eo	00-a0-e7-23-75-3e	192.168.40.127	el16	464V U College
ep	00-a0-e7-23-80-df	192.168.40.128	el16	464V U College #2
eq	00-a0-e7-23-9b-6b	192.168.40.129	el16	108B Chancellor's Hall
er	00-a0-e7-23-9b-71	192.168.40.130	el16	002 Plant Science #2
es	00-a0-e7-23-80-1f	192.168.40.131	el16	209 Fitzgerald #2
et	00-a0-e7-23-b2-5a	192.168.40.132	el16	P422A Duff Roblin #2
eu	00-a0-e7-23-b2-4f	192.168.40.133	el16	Z319 Duff Roblin
ev	00-a0-e7-23-b3-91	192.168.40.134	el16	Z319 Duff Roblin #2
ew	00-a0-e7-23-9b-67	192.168.40.135	el16	144 Aboriginal SC
ex	00-a0-e7-23-80-28	192.168.40.136	el16	204 Grains Storage #2
ey	00-a0-e7-23-a4-11	192.168.40.137	el16	133 Education #2
ez	00-a0-e7-23-a3-fb	192.168.40.138	el16	295 Education #2
fa	00-a0-e7-23-9f-b7	192.168.40.139	el16	008C Dafoe #2
fb	00-a0-e7-23-88-4e	192.168.40.140	el16	215 CAST
fc	00-a0-e7-23-9f-d3	192.168.40.141	el16	121C Arch2 #2
fd	00-a0-e7-23-a4-14	192.168.40.142	el16	210 Buller #2
fe	00-a0-e7-23-84-eb	192.168.40.143	el16	103 Sinnott
ff	00-a0-e7-23-a3-da	192.168.40.144	el16	131 Welcome Center
fg	00-a0-e7-23-1f-1b	192.168.40.145	el16	Dafoe Trailer Park
fh	00-a0-e7-23-a9-61	192.168.40.146	el16	305 Bio Sciences
fi	00-a0-e7-23-9f-d9	192.168.40.147	el16	144V Animal Science
fj	00-a0-e7-23-a5-73	192.168.40.148	el16	110V St. Johns
fk	00-a0-e7-23-ad-32	192.168.40.149	el16	001 West Grid
fl	00-a0-e7-23-b3-0c	192.168.40.150	el16	365A E3-EITC #3
fm	00-a0-e7-23-3f-02	192.168.40.151	el16	104 Tier #2 
fn	00-a0-e7-23-b2-4d	192.168.40.152	el16	104 Tier #3
fo	00-a0-e7-23-b3-eb	192.168.40.153	el16	317 Bio Sciences #2
fp	00-a0-e7-23-b3-96	192.168.40.154	el16	001 St. Andrews
fq	00-a0-e7-23-b3-f2	192.168.40.155	el16	106 Smart Park Meeting Centre
fr	00-a0-e7-23-b2-45	192.168.40.156	el16	E3-625 EITC #3
fs	00-a0-e7-23-56-79	192.168.40.157	el16	409 Pembina Residence #1
ft	00-a0-e7-23-60-46	192.168.40.158	el16	409 Pembina Residence #2
fu	00-a0-e7-23-3b-57	192.168.40.159	el16	709 Pembina Residence #1
fv	00-a0-e7-23-40-1a	192.168.40.160	el16	709 Pembina Residence #2
fw	00-a0-e7-23-0d-aa	192.168.40.161	el16	1009 Pembina Res. #1
fx	00-a0-e7-23-32-cd	192.168.40.162	el16	1009 Pembina Res. #2
fy	00-a0-e7-21-f2-44	192.168.40.163	el16	1309 Pembina Res. #1
fz	00-a0-e7-22-8a-3e	192.168.40.164	el16	1309 Pembina Res. #2
ga	00-a0-e7-23-b2-2d	192.168.40.165	el16	124 Pembina Hall
gb	00-a0-e7-23-ae-cd	192.168.40.166	el16	005 Animal Science #2
gc	00-a0-e7-23-b2-3a	192.168.40.167	el16	124 St Johns #2
gd	00-a0-e7-23-b2-3f	192.168.40.168	el16	110V St Johns #2
ge	00-a0-e7-23-b2-3b	192.168.40.169	el16	121C Arch 2 #3
gf	00-a0-e7-22-b6-23	192.168.40.170	el16	216 ArtLab
gg	00-a0-e7-22-01-48	192.168.40.171	el16	216 ArtLab #2
gh	00-a0-e7-23-3e-d9	192.168.40.172	el16	420 ArtLab
gi	00-a0-e7-22-b5-e9	192.168.40.173	el16	420 ArtLab #2
gj	00-a0-e7-22-ac-92	192.168.40.174	el16	295 Education #3
gk	00-a0-e7-23-b3-5d	192.168.40.175	el16	133 Education #3
gl	00-a0-e7-22-05-e0	192.168.40.176	el16	003 Mary Speechly
gm	00-a0-e7-23-3f-d3	192.168.40.177	el16	107A Ucentre #2
gn	00-a0-e7-23-3e-bf	192.168.40.178	el16	525 Ucentre #2
go	00-a0-e7-23-5a-52	192.168.40.179	el16	E3-625 EITC #4
gp	00-a0-e7-21-52-18	192.168.40.180	el16	210 Buller #3
gq	00-a0-e7-23-6f-80	192.168.40.181	el16	210 Buller #4
gr	00-a0-e7-23-27-d9	192.168.40.182	el16	210 Buller #5
gs	00-a0-e7-23-9b-48	192.168.40.183	el16	114A Machray Hall #2
gt	00-a0-e7-23-35-61	192.168.40.184	el16	103 Sinnott #2
gu	00-a0-e7-23-80-2c	192.168.40.185	el16	116A Armes #2
gv	00-a0-e7-21-ff-f1	192.168.40.186	el16	107B Admin #2
gw	00-a0-e7-23-9d-44	192.168.40.187	el16	502 Admin #2
gx	00-a0-e7-23-9d-41	192.168.40.188	el16	105B Allen #2
gy	00-a0-e7-23-89-e3	192.168.40.189	el16	295 Education #4
gz	00-a0-e7-21-f6-dd	192.168.40.190	el16	527V Allen #2	
ha	00-a0-e7-23-5d-90	192.168.40.191	el16	077 Agriculture #2
hb	00-a0-e7-21-ee-13	192.168.40.192	el16	540 Wallace #1
hc	00-a0-e7-23-3d-b0	192.168.40.193	el16	P422A Duff Roblin #3
hd	00-a0-e7-23-a3-dc	192.168.40.194	el16	P422A Duff Roblin #4
he	00-a0-e7-23-72-4b	192.168.40.195	el16	P422A Duff Roblin #5
hf	00-a0-e7-23-51-2f	192.168.40.196	el16	202 Dairy Science
hg	00-a0-e7-23-12-52	192.168.40.197	el16	540 Wallace #2
hh	00-a0-e7-23-b3-ed	192.168.40.198	el16	B074A Bison Stadium 
hi	00-a0-e7-23-25-71	192.168.40.199	el16	229V Dafoe #2
hj	00-a0-e7-23-48-bb	192.168.40.200	el16	109 Stores
hk	00-a0-e7-23-51-1e	192.168.40.201	el16	177 SmartPark
hl	00-a0-e7-22-08-73	192.168.40.202	el16	008C Dafoe #3
hm	00-a0-e7-22-85-ee	192.168.40.203	el16	Z319 Duff Roblin #3
hn	00-a0-e7-23-5a-da	192.168.40.204	el16	B20 Arthur Mauro #2
ho	00-a0-e7-23-9c-96	192.168.40.205	el16	B20 Arthur Mauro #3
hp	00-a0-e7-22-87-c4	192.168.40.206	el16	005 St. John's Rez 
