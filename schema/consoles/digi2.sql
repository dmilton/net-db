INSERT INTO console_digi
	(bldg_code, digi_code, digi_mac, digi_ip, digi_type, location)
VALUES
('e3','at','00-a0-e7-23-1c-14','192.168.40.12','el32','E3-625 EITC'),
('e3','aa','00-a0-e7-23-1A-78','192.168.40.13','el32','E3-625 EITC #2'),
('e3','ac','00-a0-e7-23-26-ce','192.168.40.14','el16','235 Engineering'),
('e3','ad','00-a0-e7-23-26-cd','192.168.40.15','el16','351B Engineering'),
('e3','ae','00-a0-e7-23-26-a6','192.168.40.16','el16','530E Engineering'),
('ad','af','00-a0-e7-23-29-e7','192.168.40.17','el16','107A Admin'),
('ad','ag','00-a0-e7-23-29-ec','192.168.40.18','el16','502 Admin'),
('um','ah','00-a0-e7-23-29-d2','192.168.40.19','el16','525 UCentre'),
('mh','ai','00-a0-e7-23-26-84','192.168.40.20','el16','114A Machray'),
('mh','aj','00-a0-e7-23-29-c6','192.168.40.21','el16','125 Machray'),
('mh','ak','00-a0-e7-23-29-f4','192.168.40.22','el16','358 Machray'),
('mh','al','00-a0-E7-23-2a-0c','192.168.40.23','el16','525 Machray'),
('da','am','00-a0-e7-23-29-f5','192.168.40.24','el16','008C Dafoe'),
('da','an','00-a0-e7-23-29-f2','192.168.40.25','el16','126 Dafoe'),
('um','ao','00-a0-e7-23-26-cb','192.168.40.26','el16','102F UCentre'),
('um','ap','00-a0-e7-23-26-a0','192.168.40.27','el16','107A UCentre'),
('um','aq','00-a0-e7-23-29-d1','192.168.40.28','el16','323 UCentre'),
('um','ar','00-a0-e7-23-2a-05','192.168.40.29','el16','400L UCentre'),
('hg','as','00-a0-e7-23-11-58','192.168.40.11','el16','253 Helen Glass'),
('rh','au','00-a0-e7-23-2d-0b','192.168.40.30','el16','104B Robson Hall'),
('rh','av','00-a0-e7-23-b2-35','192.168.40.31','el16','P422A Duff Roblin'),
('fk','aw','00-a0-e7-23-26-cc','192.168.40.32','el16','020 Frank Kennedy'),
('mb','ax','00-a0-e7-23-26-c2','192.168.40.33','el16','104 Max Bell'),
('mb','az','00-a0-e7-23-26-c1','192.168.40.34','el16','123 Max Bell'),
('he','ba','00-a0-e7-23-29-e8','192.168.40.35','el16','213 Human Ecology #2'),
('ee','bb','00-a0-e7-23-11-ad','192.168.40.36','el16','176L ExtEd'),
('ee','bc','00-a0-e7-23-11-41','192.168.40.37','el16','179E ExtEd'),
('ee','bd','00-a0-e7-23-1b-63','192.168.40.38','el16','199C ExtEd'),
('ig','be','00-a0-e7-23-56-81','192.168.40.39','el16','122 IGAC'),
('m2','bf','00-a0-e7-23-2c-c6','192.168.40.40','el16','107 Music 2'),
('pp','bg','00-a0-e7-23-2c-c2','192.168.40.41','el16','107 PPEM'),
('am','bh','00-a0-e7-23-3b-2e','192.168.40.42','el16','B20 Arthur Mauro'),
('e3','bi','00-a0-e7-23-3b-2f','192.168.40.43','el16','351B Engineering #2'),
('fk','bj','00-a0-e7-23-2a-06','192.168.40.44','el16','159V Frank Kennedy'),
('ve','bk','00-a0-e7-23-2a-0a','192.168.40.45','el16','The Point (Veg Storage)'),
('e3','bl','00-a0-e7-23-9b-69','192.168.40.46','el16','E3-625B EITC #2'),
('ta','bm','00-a0-e7-23-43-eb','192.168.40.47','el16','122E Tache Hall'),
('ar','bn','00-a0-e7-23-46-0c','192.168.40.48','el16','116A Armes'),
('da','bo','00-a0-e7-23-3b-62','192.168.40.49','el16','126 Dafoe #2'),
('ed','bp','00-a0-e7-23-43-ed','192.168.40.50','el16','133 Education'),
('ed','bq','00-a0-e7-23-43-e7','192.168.40.51','el16','295 Education'),
('sj','br','00-a0-e7-23-2a-d7','192.168.40.52','el16','124 St Johns'),
('al','bs','00-a0-e7-23-43-e6','192.168.40.53','el16','105B Allen '),
('bu','bt','00-a0-e7-23-43-ef','192.168.40.54','el16','210 Buller'),
('ti','bu','00-a0-e7-23-47-97','192.168.40.55','el16','104V Tier'),
('dc','bv','00-a0-e7-23-47-b9','192.168.40.56','el16','248B Drake'),
('wa','bw','00-a0-e7-23-4f-39','192.168.40.57','el16','111 Wallace'),
('e2','bx','00-a0-e7-23-55-e9','192.168.40.58','el16','152 Eng2'),
('e2','by','00-a0-e7-23-55-e8','192.168.40.59','el16','254 Eng2'),
('e2','bz','00-a0-e7-23-55-eb','192.168.40.60','el16','354 Eng2'),
('e2','ca','00-a0-e7-23-48-29','192.168.40.61','el16','454 Eng2'),
('e2','cb','00-a0-e7-23-4e-db','192.168.40.62','el16','554 Eng2'),
('e2','cc','00-a0-e7-23-4e-dc','192.168.40.63','el16','654 Eng2'),
('wa','cd','00-a0-e7-23-4f-47','192.168.40.64','el16','305V Wallace'),
('wa','ce','00-a0-e7-23-4f-73','192.168.40.65','el16','318V Wallace'),
('mu','cf','00-a0-e7-23-53-4d','192.168.40.66','el16','107cor Music'),
('rc','cg','00-a0-e7-23-52-3d','192.168.40.67','el16','127V RCFFN'),
('rc','ch','00-a0-e7-23-56-84','192.168.40.68','el16','227V RCFFN'),
('e3','ci','00-a0-e7-23-56-85','192.168.40.69','el16','E3-625b EITC #1'),
('ps','cj','00-a0-e7-23-50-4c','192.168.40.70','el16','002 Plant Science'),
('ct','ck','00-a0-e7-23-62-06','192.168.40.71','el16','202A CTC'),
('hv','cl','00-a0-e7-23-64-20','192.168.40.72','el16','300E High-Voltage'),
('fz','cm','00-a0-e7-23-5d-6e','192.168.40.73','el16','209 Fitzgerald'),
('sp','cn','00-a0-e7-23-4f-83','192.168.40.74','el16','264 St Pauls'),
('fa','co','00-a0-e7-23-4d-1f','192.168.40.75','el16','100E Fletcher Argue'),
('um','cp','00-a0-e7-23-69-71','192.168.40.76','el16','323 Ucentre #2 '),
('ru','cq','00-a0-e7-23-4e-c0','192.168.40.77','el16','105V Russell'),
('he','cr','00-a0-e7-23-69-6e','192.168.40.78','el16','213 Human Ecology #2'),
('al','cs','00-a0-e7-23-47-a9','192.168.40.79','el16','527V Allen'),
('rh','ct','00-a0-e7-23-5d-9d','192.168.40.80','el16','104B Robson Hall #2'),
('dc','cu','00-a0-e7-23-56-30','192.168.40.81','el16','204 Drake'),
('dc','cv','00-a0-e7-23-49-a8','192.168.40.82','el16','405 Drake'),
('e1','cw','00-a0-e7-23-5d-87','192.168.40.83','el16','E1-493 EITC'),
('e1','cx','00-a0-e7-23-6b-6b','192.168.40.84','el16','E1-425 EITC'),
('e1','cy','00-a0-e7-23-69-8c','192.168.40.85','el16','E1-293 EITC'),
('e1','cz','00-a0-e7-23-69-63','192.168.40.86','el16','E1-393 EITC'),
('e1','da','00-a0-e7-23-4a-d2','192.168.40.87','el16','E1-593 EITC'),
('pk','db','00-a0-e7-23-5d-6d','192.168.40.88','el16','317 Parker'),
('sa','dc','00-a0-e7-23-1b-4f','192.168.40.89','el16','213V St Andrews.'),
('ru','dd','00-a0-e7-23-68-51','192.168.40.90','el16','105V Russell #2'),
('is','de','00-a0-e7-23-4a-d2','192.168.40.91','el16','234 Isbister'),
('fa','df','00-a0-e7-23-4f-2e','192.168.40.92','el16','207V Fletcher Argue'),
('fa','dg','00-a0-e7-23-50-4d','192.168.40.93','el16','541V Fletcher Argue'),
('sp','dh','00-a0-e7-23-5d-84','192.168.40.94','el16','101V St. Pauls'),
('sp','di','00-a0-e7-23-5d-83','192.168.40.95','el16','166V St. Pauls'),
('a2','dj','00-a0-e7-23-75-16','192.168.40.96','el16','121C Arch 2'),
('uc','dk','00-a0-e7-23-75-1b','192.168.40.97','el16','171 UCollege'),
('uc','dl','00-a0-e7-23-74-f8','192.168.40.98','el16','135B UCollege'),
('gs','dm','00-a0-e7-23-77-22','192.168.40.99','el16','204 Grains Storage'),
('an','dn','00-a0-e7-23-78-46','192.168.40.100','el16','005V Animal Science'),
('lb','do','00-a0-e7-23-78-47','192.168.40.101','el16','004 Ag Lecture Blk'),
('ag','dp','00-a0-e7-23-68-50','192.168.40.102','el16','077 Agriculture'),
('ag','dq','00-a0-e7-23-70-a7','192.168.40.103','el16','141 Agriculture'),
('ag','dr','00-a0-e7-23-70-06','192.168.40.104','el16','243 Agriculture'),
('ag','ds','00-a0-e7-23-77-0c','192.168.40.105','el16','346 Agriculture'),
('is','dt','00-a0-e7-23-7b-d8','192.168.40.106','el16','234 Isbister #2'),
('el','du','00-a0-e7-23-5d-6b','192.168.40.107','el16','214v Ellis'),
('el','dv','00-a0-e7-23-80-2f','192.168.40.108','el16','214v Ellis #2'),
('mh','dw','00-a0-e7-23-80-20','192.168.40.109','el16','358 Machray Hall #2'),
('wa','dx','00-a0-e7-23-7f-de','192.168.40.110','el16','305V Wallace #2'),
('wa','dy','00-a0-e7-23-7f-e0','192.168.40.111','el16','318V Wallace #2'),
('hg','dz','00-a0-e7-23-80-29','192.168.40.112','el16','253 Helen Glass #2'),
('hg','ea','00-a0-e7-23-75-35','192.168.40.113','el16','349 Helen Glass'),
('hg','eb','00-a0-e7-23-80-21','192.168.40.114','el16','449 Helen Glass'),
('dc','ec','00-a0-e7-23-7b-ed','192.168.40.115','el16','248B Drake #2'),
('rh','ed','00-a0-e7-23-76-c1','192.168.40.116','el16','104B Robson Hall #3'),
('da','ee','00-a0-e7-23-42-8a','192.168.40.117','el16','229V Dafoe'),
('fa','ef','00-a0-e7-23-82-45','192.168.40.118','el16','207V Fletcher Argue #2'),
('fa','eg','00-a0-e7-23-82-11','192.168.40.119','el16','541V Fletcher Argue #2'),
('e1','eh','00-a0-e7-23-80-11','192.168.40.120','el16','E1-425 EITC #2'),
('uc','ei','00-a0-e7-23-80-e3','192.168.40.121','el16','135B U College #2'),
('pk','ej','00-a0-e7-23-80-1d','192.168.40.122','el16','317 Parker #2'),
('e2','ek','00-a0-e7-23-77-0f','192.168.40.123','el16','E2-254 EITC #2'),
('is','el','00-a0-e7-23-84-37','192.168.40.124','el16','234 Isbister #3'),
('pk','em','00-a0-e7-23-84-8a','192.168.40.125','el16','317 Parker #3'),
('fa','en','00-a0-e7-23-80-1e','192.168.40.126','el16','100E Fletcher Argue #2'),
('uc','eo','00-a0-e7-23-75-3e','192.168.40.127','el16','464V U College'),
('uc','ep','00-a0-e7-23-80-df','192.168.40.128','el16','464V U College #2'),
('ch','eq','00-a0-e7-23-9b-6b','192.168.40.129','el16','108B Chancellors Hall'),
('ps','er','00-a0-e7-23-9b-71','192.168.40.130','el16','002 Plant Science #2'),
('fz','es','00-a0-e7-23-80-1f','192.168.40.131','el16','209 Fitzgerald #2'),
('dr','et','00-a0-e7-23-b2-5a','192.168.40.132','el16','P422A Duff Roblin #2'),
('dr','eu','00-a0-e7-23-b2-4f','192.168.40.133','el16','Z319 Duff Roblin'),
('dr','ev','00-a0-e7-23-b3-91','192.168.40.134','el16','Z319 Duff Roblin #2'),
('ab','ew','00-a0-e7-23-9b-67','192.168.40.135','el16','144 Aboriginal SC'),
('gs','ex','00-a0-e7-23-80-28','192.168.40.136','el16','204 Grains Storage #2'),
('ed','ey','00-a0-e7-23-a4-11','192.168.40.137','el16','133 Education #2'),
('ed','ez','00-a0-e7-23-a3-fb','192.168.40.138','el16','295 Education #2'),
('da','fa','00-a0-e7-23-9f-b7','192.168.40.139','el16','008C Dafoe #2'),
('as','fb','00-a0-e7-23-88-4e','192.168.40.140','el16','215 CAST'),
('a2','fc','00-a0-e7-23-9f-d3','192.168.40.141','el16','121C Arch2 #2'),
('bu','fd','00-a0-e7-23-a4-14','192.168.40.142','el16','210 Buller #2'),
('si','fe','00-a0-e7-23-84-eb','192.168.40.143','el16','103 Sinnott'),
('wc','ff','00-a0-e7-23-a3-da','192.168.40.144','el16','131 Welcome Center'),
('tp','fg','00-a0-e7-23-1f-1b','192.168.40.145','el16','Dafoe Trailer Park'),
('bi','fh','00-a0-e7-23-a9-61','192.168.40.146','el16','305 Bio Sciences'),
('an','fi','00-a0-e7-23-9f-d9','192.168.40.147','el16','144V Animal Science'),
('sj','fj','00-a0-e7-23-a5-73','192.168.40.148','el16','110V St. Johns'),
('wg','fk','00-a0-e7-23-ad-32','192.168.40.149','el16','001 West Grid'),
('e3','fl','00-a0-e7-23-b3-0c','192.168.40.150','el16','365A E3-EITC #3'),
('ti','fm','00-a0-e7-23-3f-02','192.168.40.151','el16','104 Tier #2 '),
('ti','fn','00-a0-e7-23-b2-4d','192.168.40.152','el16','104 Tier #3'),
('bi','fo','00-a0-e7-23-b3-eb','192.168.40.153','el16','317 Bio Sciences #2'),
('sa','fp','00-a0-e7-23-b3-96','192.168.40.154','el16','001 St. Andrews'),
('sm','fq','00-a0-e7-23-b3-f2','192.168.40.155','el16','106 Smart Park Meeting Centre'),
('e3','fr','00-a0-e7-23-b2-45','192.168.40.156','el16','E3-625 EITC #3'),
('pr','fs','00-a0-e7-23-56-79','192.168.40.157','el16','409 Pembina Residence #1'),
('pr','ft','00-a0-e7-23-60-46','192.168.40.158','el16','409 Pembina Residence #2'),
('pr','fu','00-a0-e7-23-3b-57','192.168.40.159','el16','709 Pembina Residence #1'),
('pr','fv','00-a0-e7-23-40-1a','192.168.40.160','el16','709 Pembina Residence #2'),
('pr','fw','00-a0-e7-23-0d-aa','192.168.40.161','el16','1009 Pembina Res. #1'),
('pr','fx','00-a0-e7-23-32-cd','192.168.40.162','el16','1009 Pembina Res. #2'),
('pr','fy','00-a0-e7-21-f2-44','192.168.40.163','el16','1309 Pembina Res. #1'),
('pr','fz','00-a0-e7-22-8a-3e','192.168.40.164','el16','1309 Pembina Res. #2'),
('ph','ga','00-a0-e7-23-b2-2d','192.168.40.165','el16','124 Pembina Hall'),
('an','gb','00-a0-e7-23-ae-cd','192.168.40.166','el16','005 Animal Science #2'),
('sj','gc','00-a0-e7-23-b2-3a','192.168.40.167','el16','124 St Johns #2'),
('sj','gd','00-a0-e7-23-b2-3f','192.168.40.168','el16','110V St Johns #2'),
('a2','ge','00-a0-e7-23-b2-3b','192.168.40.169','el16','121C Arch 2 #3'),
('at','gf','00-a0-e7-22-b6-23','192.168.40.170','el16','216 ArtLab'),
('at','gg','00-a0-e7-22-01-48','192.168.40.171','el16','216 ArtLab #2'),
('at','gh','00-a0-e7-23-3e-d9','192.168.40.172','el16','420 ArtLab'),
('at','gi','00-a0-e7-22-b5-e9','192.168.40.173','el16','420 ArtLab #2'),
('ed','gj','00-a0-e7-22-ac-92','192.168.40.174','el16','295 Education #3'),
('ed','gk','00-a0-e7-23-b3-5d','192.168.40.175','el16','133 Education #3'),
('my','gl','00-a0-e7-22-05-e0','192.168.40.176','el16','003 Mary Speechly'),
('um','gm','00-a0-e7-23-3f-d3','192.168.40.177','el16','107A Ucentre #2'),
('um','gn','00-a0-e7-23-3e-bf','192.168.40.178','el16','525 Ucentre #2'),
('e3','go','00-a0-e7-23-5a-52','192.168.40.179','el16','E3-625 EITC #4'),
('bu','gp','00-a0-e7-21-52-18','192.168.40.180','el16','210 Buller #3'),
('bu','gq','00-a0-e7-23-6f-80','192.168.40.181','el16','210 Buller #4'),
('bu','gr','00-a0-e7-23-27-d9','192.168.40.182','el16','210 Buller #5'),
('mh','gs','00-a0-e7-23-9b-48','192.168.40.183','el16','114A Machray Hall #2'),
('si','gt','00-a0-e7-23-35-61','192.168.40.184','el16','103 Sinnott #2'),
('ar','gu','00-a0-e7-23-80-2c','192.168.40.185','el16','116A Armes #2'),
('ad','gv','00-a0-e7-21-ff-f1','192.168.40.186','el16','107B Admin #2'),
('ad','gw','00-a0-e7-23-9d-44','192.168.40.187','el16','502 Admin #2'),
('al','gx','00-a0-e7-23-9d-41','192.168.40.188','el16','105B Allen #2'),
('ed','gy','00-a0-e7-23-89-e3','192.168.40.189','el16','295 Education #4'),
('al','gz','00-a0-e7-21-f6-dd','192.168.40.190','el16','527V Allen #2'),
('ag','ha','00-a0-e7-23-5d-90','192.168.40.191','el16','077 Agriculture #2');
