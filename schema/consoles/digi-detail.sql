-- table for digi installations.
-- part of schema version 43.

-- table for digi etherlite units.
CREATE TABLE digi_detail (
	digi_mac			MACADDR PRIMARY KEY,
	digi_code			VARCHAR(2) UNIQUE,
	digi_nbr			INTEGER DEFAULT 1,
	digi_ip				INET UNIQUE,
	digi_type			CHAR(4) DEFAULT 'el16',
	console_server_id	INTEGER
						REFERENCES console_server (console_server_id)
						ON UPDATE CASCADE ON DELETE CASCADE,
	room_id				INTEGER
						REFERENCES building_rooms (room_id)
						ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE INDEX digi_detail_ip ON digi_detail (digi_ip);
CREATE INDEX digi_detail_code ON digi_detail (digi_code);
CREATE INDEX digi_detail_server ON digi_detail (server_id);

GRANT ALL ON digi_detail TO netmon;
GRANT SELECT,REFERENCES ON digi_detail TO netview;

CREATE TABLE digi_port (
	digi_port_id		SERIAL PRIMARY KEY,
	digi_mac			MACADDR
						REFERENCES digi_detail (digi_mac)
						ON UPDATE CASCADE ON DELETE CASCADE,
	port_nbr			INTEGER DEFALUT(-1),
	port_active			BOOLEAN DEFAULT('F')
);

GRANT ALL ON digi_port TO netmon;
GRANT SELECT,REFERENCES ON digi_port TO netview;

-- Installed digi view.

CREATE OR REPLACE VIEW console_ports AS
	SELECT
		console_dev.console_name AS console_name,
		console_dev.console_dev AS console_dev,
		console_server.console_master AS console_master,
		console_group.group_name AS group_name,
		console_group.group_users AS group_users,
		console_type.console_type AS console_type,
		console_type.type_config AS type_config,
		building_rooms.bldg_code AS bldg_code,
		building_rooms.room AS room,
		digi_detail.digi_type AS digi_type,
		digi_detail.digi_mac AS digi_mac,
		digi_detail.digi_code AS digi_code,
		digi_detail.digi_nbr AS digi_nbr,
		digi_detail.digi_ip AS digi_ip,
		digi_port.port_nbr AS port_nbr,
		digi_port.port_active AS port_active,
		digi_detail.console_server_id AS console_server_id,
		digi_port.digi_port_id AS digi_port_id,
		digi_detail.room_id AS room_id,
		console_group.console_group_id AS console_group_id,
		console_group.console_type_id AS console_type_id
	FROM
		(console_dev JOIN console_server
			ON console_dev.console_server_id = console_server.console_server_id)
		JOIN console_group
			ON console_dev.console_group_id = console_group.console_group_id
		JOIN console_type
			ON console_dev.console_type_id = console_type.console_type_id
		LEFT JOIN digi_detail, digi_port, building_rooms
			ON digi_detail.room_id = building_rooms.room_id
			AND digi_detail.server_id = console_server.server_id
			AND digi_detail.digi_mac = digi_port.digi_mac;

GRANT ALL ON console_ports TO netmon;
GRANT SELECT,REFERENCES ON console_ports TO netview;

