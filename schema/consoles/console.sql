-- table for console server installations.
-- part of schema version 42.

CREATE TABLE console_server (
	console_server_id		SERIAL PRIMARY KEY,
	console_master			VARCHAR(64) UNIQUE
);

GRANT ALL ON console_server TO netmon;
GRANT SELECT,REFERENCES ON console_server TO netview;

-- table for console user groups.
-- Every user will have an entry for every group they are a member of.
-- To figure out the list of all users in a group, select by group name.
-- To figure out all the groups a user is a member of, select by group user.
CREATE TABLE console_group (
	console_group_id		SERIAL PRIMARY KEY,
	group_name				VARCHAR(32),
	group_user				VARCHAR(32)
);

-- a user should only be listed once per group.
CREATE UNIQUE INDEX console_group_user ON console_group (group_name,group_user);

GRANT ALL ON console_group TO netmon;
GRANT SELECT,REFERENCES ON console_group TO netview;

-- table for types of consoles (digi, ilom, drac, etc.)
CREATE TABLE console_type (
	console_type_id			SERIAL PRIMARY KEY,
	console_type			VARCHAR(32) UNIQUE,
	type_config				VARCHAR(512)
);

GRANT ALL ON console_type TO netmon;
GRANT SELECT,REFERENCES ON console_type TO netview;

-- table for console entries.
CREATE TABLE console_entry (
	console_id     			SERIAL PRIMARY KEY,
	console_name    		VARCHAR(32) UNIQUE,
	console_server_id       INTEGER
							REFERENCES console_server (server_id)
							ON UPDATE CASCADE ON DELETE CASCADE,
	console_type_id    		INTEGER
							REFERENCES console_type (console_type_id)
							ON UPDATE CASCADE ON DELETE CASCADE,
	console_group_id		INTEGER
							REFERENCES console_group (console_group_id)
							ON UPDATE CASCADE ON DELETE RESTRICT,
	digi_port_id			INTEGER DEFALUT NULL
							REFERENCES console_port (digi_port_id)
							ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE console_config (
	config_id       SERIAL,
	config_name     VARCHAR(16),
	config_info     VARCHAR(65530),
	PRIMARY KEY (config_id)
);

GRANT ALL ON console_config TO netmon;
GRANT SELECT,REFERENCES ON console_config TO netview;

