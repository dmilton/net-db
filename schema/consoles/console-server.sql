-- table for console server digi installations.
-- part of schema version 43.

CREATE TABLE console_server (
	console_server_id		SERIAL PRIMARY KEY,
	server					VARCHAR(64) UNIQUE
);

GRANT ALL ON console_server TO netmon;
GRANT SELECT,REFERENCES ON console_server TO netview;

-- table for console user groups.
CREATE TABLE console_group (
	console_group_id		SERIAL PRIMARY KEY,
	group_name				VARCHAR(32) UNIQUE,
	group_users				VARCHAR(512)
);

GRANT ALL ON console_group TO netmon;
GRANT SELECT,REFERENCES ON console_group TO netview;

-- table for types of consoles (digi, ilom, drac, etc.)
CREATE TABLE console_type (
	console_type_id			SERIAL PRIMARY KEY,
	console_type			VARCHAR(32) UNIQUE,
	type_config				VARCHAR(512)
);

GRANT ALL ON console_type TO netmon;
GRANT SELECT,REFERENCES ON console_type TO netview;

-- table for console entries.
CREATE TABLE console_dev (
	console_id     			SERIAL PRIMARY KEY,
	console_server_id       INTEGER
							REFERENCES console_server (console_server_id)
							ON UPDATE CASCADE ON DELETE CASCADE,
	console_type_id    		INTEGER
							REFERENCES console_type (console_type_id)
							ON UPDATE CASCADE ON DELETE CASCADE,
	console_group_id		INTEGER
							REFERENCES console_group (console_group_id)
							ON UPDATE CASCADE ON DELETE RESTRICT,
	digi_port_id			INTEGER DEFALUT NULL
							REFERENCES console_port (digi_port_id)
							ON UPDATE CASCADE ON DELETE CASCADE,
	console_name    		VARCHAR(32) UNIQUE,
	console_dev     		VARCHAR(64)
);

-- table for digi etherlite units.
CREATE TABLE digi_install (
	digi_mac			MACADDR PRIMARY KEY,
	digi_code			VARCHAR(2) UNIQUE,
	digi_nbr			INTEGER DEFAULT 1,
	digi_ip				INET UNIQUE,
	digi_type			CHAR(4) DEFAULT 'el16',
	console_server_id	INTEGER
						REFERENCES console_server (console_server_id)
						ON UPDATE CASCADE ON DELETE CASCADE,
	room_id				INTEGER
						REFERENCES building_rooms (room_id)
						ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE INDEX digi_install_ip ON digi_install (digi_ip);
CREATE INDEX digi_install_code ON digi_install (digi_code);
CREATE INDEX digi_install_server ON digi_install (server_id);

GRANT ALL ON digi_install TO netmon;
GRANT SELECT,REFERENCES ON digi_install TO netview;

CREATE TABLE digi_port (
	digi_port_id		SERIAL PRIMARY KEY,
	digi_mac			MACADDR
						REFERENCES digi_install (digi_mac)
						ON UPDATE CASCADE ON DELETE CASCADE,
	port_nbr			INTEGER DEFALUT(-1),
	port_active			BOOLEAN DEFAULT('F')
);

GRANT ALL ON digi_port TO netmon;
GRANT SELECT,REFERENCES ON digi_port TO netview;

CREATE TABLE console_config (
	config_id		SERIAL,
	config_name		VARCHAR(16),
	config_info		VARCHAR(65530),
	PRIMARY KEY (config_id)
);

GRANT ALL ON console_config TO netmon;
GRANT SELECT,REFERENCES ON console_config TO netview;

-- Installed digi view.

CREATE OR REPLACE VIEW digi_ports AS
	SELECT
		console_server.server AS server,
		building_rooms.bldg_code AS bldg_code,
		building_rooms.room AS room,
		digi_install.digi_type AS digi_type,
		digi_install.digi_mac AS digi_mac,
		digi_install.digi_code AS digi_code,
		digi_install.digi_nbr AS digi_nbr,
		digi_install.digi_ip AS digi_ip,
		digi_port.port_nbr AS port_nbr,
		digi_port.port_active AS port_active,
		digi_install.console_server_id AS console_server_id,
		digi_port.digi_port_id AS digi_port_id,
		digi_install.room_id AS room_id
	FROM
		console_server, digi_install, digi_port, building_rooms
	WHERE
		digi_install.room_id = building_rooms.room_id
		AND digi_install.server_id = console_server.server_id
		AND digi_install.digi_mac = digi_port.digi_mac;

GRANT ALL ON digi_ports TO netmon;
GRANT SELECT,REFERENCES ON digi_ports TO netview;

