-- Changes to networking database: 2010-09-01

\c networking

ALTER TABLE sensor_detail ADD COLUMN rma_sensor VARCHAR(32) DEFAULT NULL;
ALTER TABLE sensor_detail ADD COLUMN maint_status VARCHAR(16) DEFAULT 'add';

DROP VIEW sensors;

-- Sensors View:
-- Incorporate information from sensor_detail, sensor_locations,
-- building_campus, and building_detail.
-- 
-- Joins performed so all information from sensor_detail will
-- be seen. Information from sensor_locations will be added
-- when it exists.
-- To that, information from building_campus and building_detail
-- will be added if appropriate matches exist in the sensor data.

CREATE OR REPLACE VIEW sensors AS 
    SELECT
	sensor_locations.name AS name,
	building_campus.location AS campus,
	building_detail.name AS building_name,
       	sensor_locations.location AS location,
	sensor_detail.serial_nbr AS serial_nbr,
	sensor_detail.model AS model,
	sensor_detail.lan_mac AS lan_mac,
	sensor_detail.wlan_mac AS wlan_mac,
	sensor_detail.install_date AS install_date,
	sensor_detail.csid AS csid,
	sensor_detail.po AS po,
	sensor_detail.serial_key AS serial_key,
	sensor_detail.active AS active,
	sensor_detail.maint_status AS maint_status,
       	sensor_detail.replaced_by AS replaced_by,
	sensor_detail.removal_date AS removal_date,
	sensor_detail.removal_reason AS removal_reason,
	sensor_detail.rma_sensor AS rma_sensor,
       	sensor_locations.notes AS notes,
       	sensor_locations.bldg_code AS bldg_code,
	sensor_detail.campusid AS campusid
    FROM
	(
	  (sensor_locations RIGHT JOIN sensor_detail
	      ON sensor_locations.sensor = sensor_detail.serial_nbr )
          LEFT JOIN
	    building_detail 
	  ON sensor_locations.bldg_code = building_detail.bldg_code
	)
	LEFT JOIN
	  building_campus
	    ON sensor_detail.campusid = building_campus.id;

-- Grant table rights to netview and netmon.
GRANT SELECT,REFERENCES ON sensors to netview;
GRANT ALL ON sensors to netmon;

TRUNCATE sensor_detail CASCADE;
