# installedSensors:
#	hostname - DNS host name of the sensor.
#	sensorID - id of the sensor.
#	locationID - id of the location.
#
CREATE TABLE wirelessInfo.installedSensors (
    hostname		char(16) NOT NULL,
    sensorID		int(10) unsigned NOT NULL,
    locationID		int(10) unsigned NOT NULL,
    PRIMARY KEY  	(sensorID,locationID),
    KEY hostname	(hostname)
) ENGINE=MyISAM;
