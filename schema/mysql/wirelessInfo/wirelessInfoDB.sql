CREATE USER 'wirelessInfo'@'localhost'
    IDENTIFIED BY 'Zz7.zR8Y5zN8SKmQ'

CREATE DATABASE `wirelessInfo` ;

GRANT SELECT , INSERT , UPDATE , DELETE , EXECUTE 
    ON `wirelessInfo` . *
    TO 'wirelessInfo'@'localhost';
GRANT SELECT
    ON buildingInfo.buildings
    TO 'wirelessInfo'@'localhost';
GRANT SELECT
    ON buildingInfo.campus
    TO 'wirelessInfo'@'localhost';

GRANT ALL PRIVILEGES
    ON `wirelessInfo` . *
    TO 'todd'@'miranda.cc.umanitoba.ca'
    WITH GRANT OPTION ;
GRANT ALL PRIVILEGES
    ON `wirelessInfo` . *
    TO 'todd'@'titan.cc.umanitoba.ca'
    WITH GRANT OPTION ;

