# Sensors:
#    id - automatic value used for indexing.
#    typeID - index into sensorTypes table.
#    lanMac - LAN MAC address of the sensor.
#    wlanMac - Wireless LAN MAC address of the sensor.
#    serialNbr - serial number of this sensor.
#    csid - CSID number of this sensor.
#    ce - Capital equipment number of this sensor.

CREATE TABLE wirelessInfo.sensors (
    id		int(10) unsigned NOT NULL auto_increment,
    typeID	int(10) unsigned NOT NULL,
    lanMac	char(17) NOT NULL,
    wlanMac	char(17) NOT NULL,
    serialNbr	varchar(32) default NULL,
    installDate	date default NULL,
    failureDate	date default NULL,
    csid	int(8) unsigned default NULL,
    ce		int(8) unsigned default NULL,
    PRIMARY KEY	(id)
) ENGINE=MyISAM;

