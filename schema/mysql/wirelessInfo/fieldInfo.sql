use dbInfo;

delete from dbInfo.fieldInfo where tableName = 'sensorInfo.sensorTypes';
INSERT INTO dbInfo.fieldInfo ( tableName, fieldName, longName, description )
    VALUES 
	('sensorInfo.sensorTypes', 'id', 'ID',
	    'automatic value used for linking to other tables'),
        ('sensorInfo.sensorTypes', 'model', 'Model Number',
	    'the model number for this sensor'),
	('sensorInfo.sensorTypes', 'description', 'Description',
	    'free form text describing this sensor');

delete from dbInfo.fieldInfo where tableName = 'sensorInfo.sensors';
INSERT INTO dbInfo.fieldInfo ( tableName, fieldName, longName, description )
    VALUES 
        ('sensorInfo.sensors', 'id', 'ID',
	    'automatic value used for indexing.'),
	('sensorInfo.sensors', 'typeID', 'Sensor Type',
	    'index into sensorTypes table.'),
	('sensorInfo.sensors', 'hostname', 'Host Name',
	    'DNS host name of the sensor.'),
	('sensorInfo.sensors', 'lanMac', 'LAN MAC Address',
	    'LAN MAC address of the sensor.'),
	('sensorInfo.sensors', 'wlanMac', 'WLAN MAC Address',
	    'Wireless LAN MAC address of the sensor.'),
	('sensorInfo.sensors', 'serialNbr', 'Serial Number',
	    'serial number of this sensor.'),
	('sensorInfo.sensors', 'installDate', 'Install Date',
	    'Date of installation.'),
	('sensorInfo.sensors', 'failed', 'Has senors failed',
	    'Has this sensor been returned for replacement.'),
	('sensorInfo.sensors', 'csid', 'CSID',
	    'CSID number of this sensor.'),
	('sensorInfo.sensors', 'ce', 'Capital Equipment Nbr',
	    'Capital equipment number of this sensor.');


