# Sensor Types:
#    id - automatic value used for linking to other tables.
#    model - the model number string of this type of sensor.
#    description - a free form character description of the sensor.

CREATE TABLE wirelessInfo.sensorTypes (
    id 		int(10) unsigned NOT NULL auto_increment,
    model	varchar(16) NOT NULL,
    description	varchar(64) default NULL,
    PRIMARY KEY	(id)
) ENGINE=MyISAM;

