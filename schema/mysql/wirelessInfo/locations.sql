# Locations:
#	id - automatic value used for indexing.
#	location - text description of this location.
#
CREATE TABLE wirelessInfo.locations (
    id		int(10) unsigned NOT NULL auto_increment,
    location	varchar(64) collate armscii8_bin default NULL,
    PRIMARY KEY (id)
) ENGINE=MyISAM;
