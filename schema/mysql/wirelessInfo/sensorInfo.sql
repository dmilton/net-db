# sensorInfo:
#	view which ties the tables together.
use wirelessInfo;

CREATE OR REPLACE VIEW wirelessInfo.sensorInfo AS
    SELECT installedSensors.hostname,
	sensorTypes.model,
        sensors.lanMac,
        sensors.wlanMac,
        sensors.serialNbr,
	sensors.serialKey,
        sensors.installDate,
        sensors.failureDate,
        sensors.csid,
        sensors.purchaseOrder,
	locations.bldgCode,
	buildings.name AS buildingName,
	locations.location,
	sensorTypes.description,
	sensors.id AS sensorID,
	locations.id AS locationID
    FROM installedSensors,
	sensors,
	sensorTypes,
	locations,
	buildingInfo.buildings
    WHERE installedSensors.sensorID = sensors.id
      AND installedSensors.locationID = locations.id
      AND sensors.typeID = sensorTypes.id
      AND locations.bldgCode = buildings.code;

