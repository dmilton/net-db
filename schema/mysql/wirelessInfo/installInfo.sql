CREATE OR REPLACE VIEW wirelessInfo.installInfo AS 
    SELECT
	installLocations.id AS id,
	installLocations.bldgCode AS bldgCode,
	campus.location AS campus,
       	buildings.name AS building,
	installLocations.location AS location,
	installLocations.installDate AS installDate,
	installLocations.status AS status,
	installLocations.radio AS radio
    FROM
	buildingInfo.buildings,
	buildingInfo.campus,
	wirelessInfo.installLocations
    WHERE 
	buildings.code = installLocations.bldgCode AND
	buildings.campus = campus.id;

