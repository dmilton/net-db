CREATE TABLE etherliteUnits (
    id		INT( 10 ) NOT NULL AUTO_INCREMENT ,
    bldgCode	CHAR( 2 ) NOT NULL ,
    elCode	CHAR( 2 ) NOT NULL ,
    VDroom	VARCHAR( 32 ) NOT NULL ,
    ipAddr	INT( 10 ) NOT NULL ,
    macAddr	INT( 6 ) NOT NULL ,
    PRIMARY KEY ( id )
) TYPE = MYISAM ;

