#!/bin/bash


#
# Verify the integrety of the networks table...

# 1) Loop through the networks table and list all networks which do not
# have a reference in the bldgVlan table.
# 2) Loop through the vlans table and list all the vlans which do not
# have a reference in the bldgVlan table.
# 3) Loop through the bldgVlan table and find all references to networks
# that do not exist.
# 4) Loop through the bldgVlan table and find all references to vlans
# that do not exist.

. /opt/local/lib/db-functions.bash
. /opt/local/lib/debug.bash


echo "Searching for orphaned network entries:"

QueryDB 'SELECT COUNT(id) FROM networkInfo.ipv4networks'
gNetworkCount=$rQueryDB

echo "ipv4networks contains $gNetworkCount entries"

loop=0
while [ $loop -lt $gNetworkCount ]; do
    # Get a network id
    QueryDB "SELECT id FROM networkInfo.ipv4networks LIMIT $loop, 1"
    netID=$rQueryDB
    # echo "Checking netID $netID"
    # Check for the network id in the bldgVlan table.
    QueryDB "SELECT COUNT(netid) FROM networkInfo.bldgVlan WHERE netid=$netID"
    if [ $rQueryDB -eq 0 ]; then
	q1="SELECT id, network, netNumber, cidr, searchable"
	q2="FROM networkInfo.ipv4networks"
	q3="WHERE id = $netID"
	query="$q1 $q2 $q3"
	QueryDB "$query"
	echo "	$rQueryDB"
    fi
    loop=`expr $loop + 1`
done

echo "Searching for orphaned vlan entries:"

QueryDB 'SELECT COUNT(id) FROM networkInfo.vlans'
gVlanCount=$rQueryDB

echo "vlans contains $gVlanCount entries"

loop=0
while [ $loop -lt $gVlanCount ]; do
    # Get a vlan id
    QueryDB "SELECT id FROM networkInfo.vlans LIMIT $loop, 1"
    vlanID=$rQueryDB
    # echo "Checking vlanID $vlanID"
    # Check for the vlans id in the bldgVlan table.
    query="SELECT COUNT(vlanid) FROM networkInfo.bldgVlan WHERE vlanid=$vlanID"
    QueryDB "$query"
    if [ $rQueryDB -eq 0 ]; then
	q1="SELECT id, name, number, active, description"
	q2="FROM networkInfo.vlans"
	q3="WHERE id = $vlanID"
	query="$q1 $q2 $q3"
	QueryDB "$query"
	echo "	$rQueryDB"
    fi
    loop=`expr $loop + 1`
done

echo "Searching for missing networks or vlans:"

QueryDB "SELECT COUNT(vlanid) from networkInfo.bldgVlan"
gBldgVlanCount=$rQueryDB

echo "bldgVlan contains $gBldgVlanCount entries"

loop=0
while [ $loop -lt $gBldgVlanCount ]; do
    # Get a vlan id
    q1="SELECT bldgcode, vlanid, netid"
    q2="FROM networkInfo.bldgVlan LIMIT $loop, 1"
    QueryDB "$q1 $q2"
    set -- $rQueryDB
    bldgCode=$1
    vlanID=$2
    netID=$3
    # echo "Checking bldgCode $bldgCode"
    # Check for the building code in the buildings table.
    q1="SELECT COUNT(code) FROM buildingInfo.buildings"
    q2="WHERE code=\"$bldgCode\""
    QueryDB "$q1 $q2"
    if [ $rQueryDB -eq 0 ]; then
	echo "	building code entry missing: $bldgCode $vlanID $netID"
    fi
    # Check for the network id in the networks table.
    # echo "Checking vlanID $vlanID"
    # Check for the vlan id in the vlans table.
    query="SELECT COUNT(id) FROM networkInfo.vlans WHERE id=$vlanID"
    QueryDB "$query"
    if [ $rQueryDB -eq 0 ]; then
	echo "	vlan entry missing: $bldgCode $vlanID $netID"
    fi
    # Check for the network id in the networks table.
    # echo "Checking netID $netID"
    query="SELECT COUNT(id) FROM networkInfo.ipv4networks WHERE id=$netID"
    QueryDB "$query"
    if [ $rQueryDB -eq 0 ]; then
	echo "	network entry missing: $bldgCode $vlanID $netID"
    fi
    loop=`expr $loop + 1`
done

#
