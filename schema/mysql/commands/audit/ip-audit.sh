#!/bin/bash

. /opt/local/lib/network/db-functions.bash

gDebug=0

gAuditCidr=28
gAuditNet="130.179.%"

# Audit the IP address assignment, producing counts for the networks
# showing location, current size, ports installed, active IP addresses.

# Determine how many active networks we have.
q1="SELECT COUNT(bldgCode)"
q2="FROM networkInfo.networkInfo"
q3="WHERE network LIKE '$gAuditNet' AND active = 'T' AND cidr < $gAuditCidr"
QueryDB "$q1 $q2 $q3"
activeNets=$rQueryDB

# We need a unique list of IP addresses so create a temporary place
# to hold this data...
# q1="CREATE TABLE networkInfo.ipUse"
# q2="( ipAddress int(10) unsigned NOT NULL , primary key (ipAddress) );"
# q3="TRUNCATE ipUse;"
# q4="INSERT IGNORE INTO networkInfo.ipUse"
# q5="SELECT ipAddress FROM nodeInfo.activeARP;"
# QueryDB "$q1 $q2 $q3 $q4 $q5"

# Loop through the database and grab networks, determining the information
# we want for each network.
loop=0
q1="SELECT bldgCode, name, network, netID, cidr, size"
q2="FROM networkInfo.networkInfo"
q3="WHERE network LIKE '$gAuditNet' AND active = 'T' AND cidr < $gAuditCidr"
q4="ORDER by bldgCode, netID"

while [ $loop -lt $activeNets ]; do
    # Get the network info itself.
    q5="LIMIT $loop, 1"
    QueryDB "$q1 $q2 $q3 $q4 $q5"
    set -- $rQueryDB
    bldgCode=$1
    name=$2
    network=$3
    netnumber=$4
    cidr=$5
    size=$6
    debug 1 "bldgCode=$bldgCode, name=$name, network=$network"
    debug 1 "netnumber=$netnumber, cidr=$cidr, size=$size"
    # Figure out what the next network is
    firstAddress="$netnumber"
    lastAddress=`expr $netnumber + $size - 1`
    debug 1 "firstAddress=$firstAddress, lastAddress=$lastAddress"
    # Figure out how many ip addresses we have in that range.
    iq1="SELECT count(ipAddress) FROM networkInfo.ipUse"
    iq2="WHERE ipAddress >= $firstAddress AND ipAddress <= $lastAddress;"
    QueryDB "$iq1 $iq2"
    activeIP=$rQueryDB
    echo "$bldgCode	$name	$network/$cidr	$activeIP of $size"

    loop=`expr $loop + 1`
done

# q1="DROP TABLE networkInfo.ipUse;"
# QueryDB "$q1"
