#!/usr/bin/python

gVersion = "2.0.0 March 12, 2008"

# This script accepts one argument - a MAC address.
# From the MAC address it will try and determine where on campus that
# MAC address has been located, what IP addresses that have been assigned
# and any network login sessions associated with that MAC address.

# 10 Mar 2008		initial writing for netdisco database.


############################################################################
# debug(level, message)                                                    #
# Requires two global variables:                                           #
gDebug = 0                      # debug level to honour
gCGIMode = 0                    # generate html output?

def debug(debugMsgLevel, debugMessage):
    """Produce output for debugging purposes.
	The debug level is the level of debugging output to generate.
	    0 - no debug output
	    1 - entry and exit of any fuction or routine.
	    2 - intermediate processing within functions.
	the message is any free form text but should take a general
	format indentifying the name of the function/routine followed
	by any information desired.
    """
    global gDebug
    if ( gDebug >= debugMsgLevel ):
	print debugMessage
	if ( gCGIMode  ==  1 ):
	    print '<br>'
	## end if
    ## end if
##end debug

############################################################################
# Usage()

def Usage():
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print """
usage: %s [-h|--help] [-v|--version] | [-d|--debug N] mac-address
    -h    display this help message
    -v    show version information and exit.
    -d N  debug at level N.
    mac-address formatted as 00:00:00:00:00:00
    """ % ( gCommand )
    sys.exit(1)
## end usage()

############################################################################
# host
def host(myIP):
    import socket
    """
    Perform a host name lookup on the provide IP address.
    """
    debug(1, "host: myIP=" + myIP)
    lookup = socket.gethostbyaddr(myIP)[0]
    debug(1, "host: lookup=" + str(lookup))
    return(lookup)
## end host()

############################################################################
# PgQuery(theQuery, [ dbName = "x", ] [ dbUser = "y", ] [ dbPass = "z" ])
# send a SQL query to the database. The query should only return a single
# row from the query which is the result of this function call.

def PgQuery(sqlQuery, myDB="netdisco", myUser="netdisco",myPass="im2co3j0"):
    """Send a SQL query to the postgres database.
        Any query is sent to the database and results are returned.
	Default values for database, user and password are netdisco,
	netdisco, and netdisco respectively. override as required.
    """
    debug(1, "PgQuery: sqlQuery=" + sqlQuery)
    # Python PostgreSQL DB-API module.
    import pgdb

    # assume we do not get a result.
    result = None

    # Get a connection to the database.
    if ( sqlQuery == "" ):
	print "Invalid or missing query"
	sys.exit(2)
    else:
	try:
	    db = pgdb.connect(database=myDB, host="localhost", 
		user=myUser, password=myPass)
	except pgdb.InternalError, err:
	    print str(err)
	    sys.exit(2)
	## end try

	cursor = db.cursor()

	# Now that we have a connection, send our query.
	cursor.execute(sqlQuery)
	# Get the result of the query.
	result = cursor.fetchall()
    # end if
    return(result)
## end PgQuery()

def ActiveState(asActive):
    debug(1, "ActiveState: asActive=" + str(asActive))

    if asActive == True:
	rActiveState = "online"
    elif asActive == False:
	rActiveState = "offline"
    else:
	rActiveState = "unknown"
    ## end if

    debug(1, "ActiveState: rActiveState=" + rActiveState)
    return(rActiveState)
## end ActiveState()

def PrintArpEntry(paArgs):
    debug(1, "PrintArpEntry: paArgs=" + str(paArgs))
    paIP=paArgs[0]
    paMac=paArgs[1]
    paActive=paArgs[2]
    paTimeFirst=paArgs[3]
    paTimeLast=paArgs[4]
    debug(1, "PrintArpEntry: paIP=" + paIP + 
	", paMac=" + paMac + ", paActive=" + str(paActive))
    debug(1, "PrintArpEntry: paTimeFirst=" + paTimeFirst +
	", paTimeLast=" + paTimeLast)
    paActive = ActiveState(paActive)
    debug(2, "PrintArpEntry: paActive=" + paActive)

    print "MAC Address " + paMac + " was seen using IP"
    print "   " + paIP + " at " + paTimeFirst
    print "   and was last seen " + paTimeLast + "."
    print "   The current state is " + paActive + "."

    debug(1, "PrintArpEntry: exit")
## end PrintArpEntry()

# Define routine to get arp table entry information.
def FindArpEntries(faMacAddr):
    debug(1, "FindArpEntries: faMacAddr=" + faMacAddr)

    q1="SELECT ip,mac,active,time_first,time_last"
    q2="FROM node_ip"
    q3="WHERE mac = '" + faMacAddr + "'"
    arpEntries = PgQuery(q1 + " " + q2 + " " + q3)
    locnCount = len(arpEntries)
    loop = 0
    while loop < locnCount:
	debug(2, "FindArpEntries: loop=" + str(loop))

	PrintArpEntry(arpEntries[loop])

	debug(2, "FindArpEntries: arpEntry=" + str(arpEntries[loop]))
	loop += 1
    ## end while

    debug(1, "FindArpEntries: exit")
## end FindArpEntries()

def PrintSwitchPort(psArgs):
    debug(1, "PrintSwitchPort: psArgs=" + str(psArgs[0:]))
    psMac=psArgs[0]
    psIP=psArgs[1]
    psPort=psArgs[2]
    psActive=psArgs[3]
    psTimeFirst=psArgs[4]
    psTimeRecent=psArgs[5]
    psTimeLast=psArgs[6]
    psActive = ActiveState(psActive)
    ##psDns="`host $psIP | cut -f5 '-d ' | cut -f1 '-d.'`"
    psDns = host(psIP)
    debug(2, "PrintSwitchPort: psMac=" + psMac)
    debug(2, "PrintSwitchPort: psIp=" + psIP)
    debug(2, "PrintSwitchPort: psDns=" + psDns)
    debug(2, "PrintSwitchPort: psPort=" + psPort)
    debug(2, "PrintSwitchPort: psActive=" + psActive)
    debug(2, "PrintSwitchPort: psTimeFirst=" + psTimeFirst)
    debug(2, "PrintSwitchPort: time_recent=" + psTimeRecent)
    debug(2, "PrintSwitchPort: psTimeLast=" + psTimeLast)
    print "MAC Address " + psMac + " was seen on switch " + psDns
    print "   port " + psPort + " at " + psTimeFirst
    print "   It was last seen at " + psTimeLast + "."
    print "   The current state is " + psActive + "."
    debug(1, "PrintSwitchPort: exit")
## end PrintSwitchPort

# Find port entries
def FindNodeEntries(fnMacAddr):
    debug(1, "FindNodeEntries: fnMacAddr=" + fnMacAddr)

    q1="SELECT mac,switch,port,active,time_first,time_recent,time_last"
    q2="FROM node"
    q3="WHERE mac = '" + fnMacAddr + "'"
    nodeInfo = PgQuery(q1 + " " + q2 + " " + q3)
    locnCount = len(nodeInfo)
    debug(2, "FindNodeEntries: locnCount=" + str(locnCount))
    loop = 0
    while loop < locnCount:
	debug(2, "FindNodeEntries: loop=" + str(loop))

	PrintSwitchPort(nodeInfo[loop])

	debug(2, "FindNodeEntries: nodeInfo=" + str(nodeInfo[loop]))
	loop += 1
    ## end while

    debug(1, "FindNodeEntries: exit")
## end FindNodeEntries()

############################################################################
# Process command line arguments.

import getopt, sys
from os.path import basename

gCommand = basename(sys.argv[0])

def main():
    global gDebug 
    try:
	optList, argList = getopt.getopt(sys.argv[1:], "hvd:", 
		["help", "version", "debug="])
    except getopt.GetoptError, err:
	# print help information and exit:
	print str(err)
	Usage()
	sys.exit(2)
    ## end try

    gDebug = 0
    for option, argument in optList:
	if option in ("-v", "--version"):
	    print gVersion
	    sys.exit(0)
	elif option in ("-h", "--help"):
	    Usage()
	    sys.exit(0)
	elif option in ("-d", "--debug"):
	    gDebug = argument
	else:
	    assert False, "unhandled option"
	## end if
    ## end for

    if ( len(argList) == 1 ):
	gMyMAC = argList[0]
    else:
	Usage()
    ## end if

    debug(1, "gMyMAC=" + gMyMAC)

    # Do the work...
    # Start by looking for switch port locations...
    FindNodeEntries(gMyMAC)
    FindArpEntries(gMyMAC)
## end main()

# Call main to do the work.
if __name__ == "__main__":
    main()

