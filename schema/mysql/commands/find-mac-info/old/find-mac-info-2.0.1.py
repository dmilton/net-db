#!/usr/bin/python

gVersion = "2.0.1 June 10, 2008"

# This script accepts one argument - a MAC address.
# From the MAC address it will try and determine where on campus that
# MAC address has been located, what IP addresses that have been assigned
# and any network login sessions associated with that MAC address.

# 10 June 2008		Removed debug, host and used standard include
#			modules instead.
# 12 March 2008		put into production.
# 10 March 2008		initial writing for netdisco database.

############################################################################
# Usage()

def Usage():
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print """
usage: %s [-h|--help] [-v|--version] | [-d|--debug N] mac-address
    -h    display this help message
    -v    show version information and exit.
    -d N  debug at level N.
    mac-address formatted as 00:00:00:00:00:00
    """ % ( gCommand )
    sys.exit(1)
## end usage()

def ActiveState(asActive):
    debug.debug(1, "ActiveState: asActive=" + str(asActive))

    if asActive == True:
	rActiveState = "online"
    elif asActive == False:
	rActiveState = "offline"
    else:
	rActiveState = "unknown"
    ## end if

    debug.debug(1, "ActiveState: rActiveState=" + rActiveState)
    return(rActiveState)
## end ActiveState()

def PrintArpEntry(paArgs):
    debug.debug(1, "PrintArpEntry: paArgs=" + str(paArgs))
    paIP=paArgs[0]
    paMac=paArgs[1]
    paActive=paArgs[2]
    paTimeFirst=paArgs[3]
    paTimeLast=paArgs[4]
    debug.debug(1, "PrintArpEntry: paIP=" + paIP + 
	", paMac=" + paMac + ", paActive=" + str(paActive))
    debug.debug(1, "PrintArpEntry: paTimeFirst=" + paTimeFirst +
	", paTimeLast=" + paTimeLast)
    paActive = ActiveState(paActive)
    debug.debug(2, "PrintArpEntry: paActive=" + paActive)

    print "MAC Address " + paMac + " was seen using IP"
    print "   " + paIP + " at " + paTimeFirst
    print "   and was last seen " + paTimeLast + "."
    print "   The current state is " + paActive + "."

    debug.debug(1, "PrintArpEntry: exit")
## end PrintArpEntry()

# Define routine to get arp table entry information.
def FindArpEntries(faMacAddr):
    debug.debug(1, "FindArpEntries: faMacAddr=" + faMacAddr)

    q1="SELECT ip,mac,active,time_first,time_last"
    q2="FROM node_ip"
    q3="WHERE mac = '" + faMacAddr + "'"
    arpEntries = dbAccess.PgQuery(dbHost="miranda.cc.umanitoba.ca",
	    dbName="netdisco", dbUser="netdisco", dbPass="im2co3j0",
	    sqlQuery=q1 + " " + q2 + " " + q3)
    locnCount = len(arpEntries)
    loop = 0
    while loop < locnCount:
	debug.debug(2, "FindArpEntries: loop=" + str(loop))

	PrintArpEntry(arpEntries[loop])

	debug.debug(2, "FindArpEntries: arpEntry=" + str(arpEntries[loop]))
	loop += 1
    ## end while

    debug.debug(1, "FindArpEntries: exit")
## end FindArpEntries()

def PrintSwitchPort(psArgs):
    debug.debug(1, "PrintSwitchPort: psArgs=" + str(psArgs[0:]))
    psMac=psArgs[0]
    psIP=psArgs[1]
    psPort=psArgs[2]
    psActive=psArgs[3]
    psTimeFirst=psArgs[4]
    psTimeRecent=psArgs[5]
    psTimeLast=psArgs[6]
    psActive = ActiveState(psActive)
    ##psDns="`host $psIP | cut -f5 '-d ' | cut -f1 '-d.'`"
    psDns = host(psIP)
    debug.debug(2, "PrintSwitchPort: psMac=" + psMac)
    debug.debug(2, "PrintSwitchPort: psIp=" + psIP)
    debug.debug(2, "PrintSwitchPort: psDns=" + str(psDns[0]))
    debug.debug(2, "PrintSwitchPort: psPort=" + psPort)
    debug.debug(2, "PrintSwitchPort: psActive=" + psActive)
    debug.debug(2, "PrintSwitchPort: psTimeFirst=" + psTimeFirst)
    debug.debug(2, "PrintSwitchPort: time_recent=" + psTimeRecent)
    debug.debug(2, "PrintSwitchPort: psTimeLast=" + psTimeLast)
    print "MAC Address " + psMac + " was seen on switch " + str(psDns[0])
    print "   port " + psPort + " at " + psTimeFirst
    print "   It was last seen at " + psTimeLast + "."
    print "   The current state is " + psActive + "."
    debug.debug(1, "PrintSwitchPort: exit")
## end PrintSwitchPort

# Find port entries
def FindNodeEntries(fnMacAddr):
    debug.debug(1, "FindNodeEntries: fnMacAddr=" + fnMacAddr)

    q1="SELECT mac,switch,port,active,time_first,time_recent,time_last"
    q2="FROM node"
    q3="WHERE mac = '" + fnMacAddr + "'"
    nodeInfo = dbAccess.PgQuery(q1 + " " + q2 + " " + q3)
    locnCount = len(nodeInfo)
    debug.debug(2, "FindNodeEntries: locnCount=" + str(locnCount))
    loop = 0
    while loop < locnCount:
	debug.debug(2, "FindNodeEntries: loop=" + str(loop))

	PrintSwitchPort(nodeInfo[loop])

	debug.debug(2, "FindNodeEntries: nodeInfo=" + str(nodeInfo[loop]))
	loop += 1
    ## end while

    debug.debug(1, "FindNodeEntries: exit")
## end FindNodeEntries()

############################################################################
# Process command line arguments.

import getopt, sys
from os.path import basename

sys.path.append('/usr/local/lib/python')

import debug
from host import host
import dbAccess
#from dbAccess import PgQuery

gCommand = basename(sys.argv[0])

def main():
    global gDebug 
    try:
	optList, argList = getopt.getopt(sys.argv[1:], "hvd:", 
		["help", "version", "debug="])
    except getopt.GetoptError, err:
	# print help information and exit:
	print str(err)
	Usage()
	sys.exit(2)
    ## end try

    gDebug = 0
    for option, argument in optList:
	if option in ("-v", "--version"):
	    print gVersion
	    sys.exit(0)
	elif option in ("-h", "--help"):
	    Usage()
	    sys.exit(0)
	elif option in ("-d", "--debug"):
	    debug.setDebug(argument)
	else:
	    assert False, "unhandled option"
	## end if
    ## end for

    if ( len(argList) == 1 ):
	gMyMAC = argList[0]
    else:
	Usage()
    ## end if

    debug.debug(1, "gMyMAC=" + gMyMAC)

    # Do the work...
    # Start by looking for switch port locations...
    FindNodeEntries(gMyMAC)
    FindArpEntries(gMyMAC)
## end main()

# Call main to do the work.
if __name__ == "__main__":
    main()

