#!/bin/bash

gVersion="2.0.0 March 7, 2008"

. /opt/local/lib/network/debug.bash

# This script accepts one argument - a MAC address.
# From the MAC address it will try and determine where on campus that
# MAC address has been located, what IP addresses that have been assigned
# and any network login sessions associated with that MAC address.

# 30 Jan 2008		removed references to the routers table and
#			substituted use of the switches table instead.
# 20 Nov 2007           Changed location of db commands scripts.
# 20 Sep 2007		Found problem where MAC address does not actually
#			return any results.

Usage() {
    echo "Usage: $gCommand [-v] | [-d N] mac-address"
    echo "    -v    show version information and exit."
    echo "    -d N  debug at level N."
    echo "    mac-address formatted as 00:00:00:00:00:00"
    exit 1
}

args=`getopt vd: $*`
errcode=$?
if [ $errcode -ne 0 ]; then
    Usage
fi

set -- $args
for i; do
    case "$i" in
	-d)	# debug
		gDebug=$2
		if [ $gDebug -ge 2 ]; then
		    gDebug=2
		fi
		shift; shift ;;
	-v)	# show version information and exit.
		echo $gCommand: Version $gVersion
		exit 0
		;;
	--)
		shift ; break ;;
    esac
done

gMyMAC=$1

if [ "X$gMyMAC" = "X" ]; then
    Usage
fi

debug 1 "gMyMAC=$gMyMAC"

QueryDB() {
    myQuery="$1"
    debug 1 "QueryDB: myQuery=$myQuery"

    if [ "X$myQuery" = "X" ]; then
	echo "Invalid or missing query"
	exit 2
    else
	rQueryDB=`echo $myQuery | psql -t -U netdisco netdisco`
	result=$?
    fi

    if [ $result -eq 0 ]; then
	if [ "X$rQueryDB" = "X" ]; then
	    result=-9999
	fi
    fi
    debug 1 "QueryDB: result=$result, rQueryDB=$rQueryDB"
    return $result
}

ActiveState() {
    asActive=$1
    debug 1 "ActiveState: asActive=$asActive"

    case $asActive in
	*t) rActiveState="online" ;;
	*f) rActiveState="offline" ;;
	*) rActiveState="unknown" ;;
    esac

    debug 1 "ActiveState: rActiveState=$rActiveState"
}

PrintArpEntry() {
    paArgs="$1"
    debug 1 "PrintArpEntry: paArgs=$paArgs"
    OLDIFS=$IFS
    IFS='|'
    set -- $paArgs
    paIP=$1
    paMac=$2
    paActive=$3
    paTimeFirst=$4
    paTimeLast=$5
    IFS=$OLDIFS
    debug 1 "PrintArpEntry: paIP=$paIP, paMac=$paMac, paActive=$paActive"
    debug 1 "PrintArpEntry: paTimeFirst=$paTimeFirst, paTimeLast=$paTimeLast"
    ActiveState $paActive
    paActive=$rActiveState
    debug 2 "PrintArpEntry: paActive=$paActive"

    debug 1 "PrintArpEntry: exit"
}

# Define routine to arp table entry information.
FindArpEntries() {
    faMacAddr=$1
    debug 1 "FindArpEntries: faMacAddr=$faMacAddr"

    q1="SELECT count(mac)"
    q2="FROM node_ip"
    q3="WHERE mac = '$faMacAddr'"
    QueryDB "$q1 $q2 $q3"
    locnCount=$rQueryDB
    loop=0
    while [ $loop -lt $locnCount ]; do
	debug 2 "FindArpEntries: loop=$loop"

	q1="SELECT ip,mac,active,time_first,time_last"
	q2="FROM node_ip"
	q3="WHERE mac = '$faMacAddr'"
	q4="LIMIT 1 OFFSET $loop"
	QueryDB "$q1 $q2 $q3 $q4"
	arpEntry="$rQueryDB"
	echo $arpEntry

	debug 2 "FindArpEntries: ipLocation=$ipLocation"
	loop=`expr $loop + 1`
    done

    debug 1 "FindArpEntries: exit"
}

PrintSwitchPort() {
    psArgs="$1"
    debug 1 "PrintSwitchPort: psArgs=$psArgs"
    OLDIFS=$IFS
    IFS='|'
    set -- $args
    psMac=$1
    psIP=$2
    psPort="$3"
    psActive=$4
    psTimeFirst="$5"
    psTimeRecent="$6"
    psTimeLast="$7"
    IFS=$OLDIFS
    ActiveState $psActive
    psActive=$rActiveState
    psDns="`host $psIp | cut -f5 '-d ' | cut -f1 '-d.'`"
    debug 2 "PrintSwitchPort: psMac=$psMac"
    debug 2 "PrintSwitchPort: psIp=$psIp"
    debug 2 "PrintSwitchPort: psDns=$psDns"
    debug 2 "PrintSwitchPort: psPort=$psPort"
    debug 2 "PrintSwitchPort: psActive=$psActive"
    debug 2 "PrintSwitchPort: psTimeFirst=$psTimeFirst"
    debug 2 "PrintSwitchPort: time_recent=$psTimeRecent"
    debug 2 "PrintSwitchPort: psTimeLast=$psTimeLast"
    echo "MAC Address${psMac}first seen on switch $psDns $psPort"
    echo "   at${psTimeFirst}and last seen at${psTimeLast}"
    echo "   The current state is $psActive"
    debug 1 "PrintSwitchPort: exit"
}

# Find port entries
FindNodeEntries() {
    fnMacAddr=$1
    debug 1 "FindNodeEntries: fnMacAddr=$fnMacAddr"

    q1="SELECT count(mac)"
    q2="FROM node"
    q3="WHERE mac = '$fnMacAddr'"
    QueryDB "$q1 $q2 $q3"
    locnCount=$rQueryDB
    loop=0
    while [ $loop -lt $locnCount ]; do
	debug 2 "FindNodeEntries: loop=$loop"

	q1="SELECT mac,switch,port,active,time_first,time_recent,time_last"
	q2="FROM node"
	q3="WHERE mac = '$fnMacAddr'"
	q4="LIMIT 1 OFFSET $loop"
	QueryDB "$q1 $q2 $q3 $q4"
	nodeInfo="$rQueryDB"
	PrintSwitchPort "$nodeInfo"

	debug 2 "FindNodeEntries: ipLocation=$ipLocation"
	loop=`expr $loop + 1`
    done

    debug 1 "FindNodeEntries: exit"
}

# Start by looking for switch port locations...
FindNodeEntries $gMyMAC
FindArpEntries $gMyMAC
exit 0

