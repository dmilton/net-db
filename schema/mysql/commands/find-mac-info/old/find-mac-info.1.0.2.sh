#!/bin/bash

gVersion="1.0.2 30 Jan 2008"

# This script accepts one argument - a MAC address.
# From the MAC address it will try and determine where on campus that
# MAC address has been located, what IP addresses that have been assigned
# and any network login sessions associated with that MAC address.

# 30 Jan 2008		removed references to the routers table and
#			substituted use of the switches table instead.
# 20 Nov 2007           Changed location of db commands scripts.
# 20 Sep 2007		Found problem where MAC address does not actually
#			return any results.

. /opt/local/lib/network/db-functions.bash

Usage() {
    echo "Usage: $gCommand [-v] | [-d N] mac-address"
    echo "    -v    show version information and exit."
    echo "    -d N  debug at level N."
    echo "    mac-address formatted as 0000.0000.0000"
    exit 1
}

args=`getopt vd: $*`
errcode=$?
if [ $errcode -ne 0 ]; then
    Usage
fi

set -- $args
for i; do
    case "$i" in
	-d)	# debug
		gDebug=$2
		if [ $gDebug -ge 2 ]; then
		    gDebug=2
		fi
		shift; shift ;;
	-v)	# show version information and exit.
		echo $gCommand: Version $gVersion
		exit 0
		;;
	--)
		shift ; break ;;
    esac
done

gMyMAC=$1

if [ "X$gMyMAC" = "X" ]; then
    Usage
fi

debug 1 "gMyMAC=$gMyMAC"

gHexMac=0x"`echo $gMyMAC | tr -d ':.'`"
debug 1 "gHexMac=$gHexMac"

QueryDB2() {
    myQuery="$1"
    debug 1 "QueryDB: myQuery=$myQuery"

    if [ "X$myQuery" = "X" ]; then
	echo "Invalid or missing query"
	exit 2
    else
	rQueryDB=`mysql --batch --skip-column-names \
	    --database=$gMySqlDB --user=$gMySqlUser --password=$gMySqlUserPW\
	    --execute="$myQuery" | tr '\011' '~'`
	result=$?
    fi

    if [ $result -eq 0 ]; then
	if [ "X$rQueryDB" = "X" ]; then
	    result=-9999
	fi
    fi
    debug 1 "QueryDB: result=$result, rQueryDB=$rQueryDB"
    return $result
}

# Define routine to print location information.
GetArpLocations() {
    pArpID=$1
    debug 1 "GetArpLocations: pArpID=$pArpID"

    q1="SELECT count(switch)"
    q2="FROM nodeInfo.nodeInfo"
    q3="WHERE arpID = $pArpID"
    QueryDB "$q1 $q2 $q3"
    locnCount=$rQueryDB
    loop=0
    while [ $loop -lt $locnCount ]; do
	debug 2 "GetArpLocations: loop=$loop"

	q1="SELECT switch,ifName,ifAlias,clientID,arpLastSeen,lastLinkDown"
	q2="FROM nodeInfo.nodeInfo"
	q3="WHERE arpID = $pArpID"
	q4="LIMIT $loop, 1"
	QueryDB2 "$q1 $q2 $q3 $q4"
	ipLocation="$rQueryDB"

	debug 2 "GetArpLocations: ipLocation=$ipLocation"
	loop=`expr $loop + 1`
    done

    debug 1 "GetArpLocations: exit"
}

# Start by looking for any arp entries...
q1="SELECT hex(ipAddress) FROM nodeInfo.activeARP"
q2="WHERE macAddress = $gHexMac"
query="$q1 $q2"
QueryDB "$query"
result=$?
if [ $result -ne 0 ]; then
    echo Unable to locate MAC address \"$gMyMAC\" in nodeInfo database.
    exit 2
else
    ipAddrList=$rQueryDB
fi
debug 1 "ipAddrList=$ipAddrList"

# Now we have a list of IP addresses this MAC address has had.
for hexIP in $ipAddrList; do
    # Convert the HEX representation of the IP address into the normal
    # decimal format we typically see.
    anIP=`hex2ip 0x$hexIP`
    debug 2 "anIP: $anIP"
    # Find any records for this ARP entry...
    q1="SELECT id FROM nodeInfo.activeARP"
    q2="WHERE macAddress = $gHexMac AND ipAddress = 0x$hexIP"
    query="$q1 $q2"
    QueryDB "$query"
    arpID=$rQueryDB
    debug 2 "arpID=$arpID"
    GetArpLocations $arpID
done

exit 0

q1="SELECT bldgCode,name,number,network,cidr FROM networkInfo.networkInfo"
q2="WHERE netNumber < $gHexIP ORDER BY netNumber DESC LIMIT 1"
query="$q1 $q2"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    set -- $rQueryDB
    bldgCode=$1
    name=$2
    number=$3
    network=$4
    cidr=$5
else
    echo Unable to locate IP address \"$gMyIP\" in networkInfo database.
    exit 2
fi
debug 1 "bldgCode=$bldgCode"
debug 1 "name=$name"
debug 1 "number=$number"
debug 1 "network=$network"
debug 1 "cidr=$cidr"

query="SELECT name FROM buildingInfo.buildings WHERE code = \"$bldgCode\""
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    bldgName="$rQueryDB"
else
    echo "Unable to find building info for code \"$bldgCode\""
    exit 3
fi
debug 1 "bldgName=$bldgName"

q1="SELECT location FROM buildingInfo.buildings, buildingInfo.campus"
q2="WHERE buildings.campus = campus.id"
q3="AND code = \"$bldgCode\""
query="$q1 $q2 $q3"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    bldgCampus="$rQueryDB"
else
    echo "Unable to extract location for code \"$bldgCode\""
    exit 4
fi
debug 1 "bldgCampus=$bldgCampus"

q1="SELECT name FROM switchInfo.switches"
q2="WHERE bldgCode = \"${bldgCode}\""
q3="  AND deviceType = 'Router' AND class = 'b';"
query="$q1 $q2 $q3"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    routerName="$rQueryDB"
else
    echo "Unable to extract router name for code \"$bldgCode\""
    exit 5
fi
debug 1 "routerName=$routerName"

# Extract what we can from our nodeInfo database...
gNodeInfo=0	# assume we don't get anything.
q1="SELECT HEX(ipAddress),"
q2="    CONCAT("
q3="	    SUBSTR(HEX(macAddress),1,4), \".\","
q4="        SUBSTR(HEX(macAddress),5,4), \".\","
q5="        SUBSTR(HEX(macAddress),9,4)),"
q6="    arpLastSeen, ipLastSeen, macLastSeen, lastLinkUp, lastLinkDown,"
q7="    linkFlaps, switch, ifName, ifAlias, ifIndex, clientID"
q8="FROM nodeInfo.nodeInfo"
q9="WHERE ipAddress = $gHexIP"
query="$q1 $q2 $q3 $q4 $q5 $q6 $q7 $q8 $q9"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    OLDIFS=$IFS
    IFS='	'
    set -- $rQueryDB
    ipAddress=`hex2ip $1`
    macAddress=$2
    arpLastSeen="$3"
    ipLastSeen="$4"
    macLastSeen="$5"
    lastLinkUp="$6"
    lastLinkDown="$7"
    linkFlaps=$8
    switch=$9
    ifName=${10}
    ifAlias=${11}
    ifIndex=${12}
    clientID=${13}
    gNodeInfo=1
    debug 1 "ipAddress=$ipAddress"
    debug 1 "macAddress=$macAddress"
    debug 1 "arpLastSeen=$arpLastSeen"
    debug 1 "ipLastSeen=$ipLastSeen"
    debug 1 "macLastSeen=$macLastSeen"
    debug 1 "lastLinkUp=$lastLinkUp"
    debug 1 "lastLinkDown=$lastLinkDown"
    debug 1 "linkFlaps=$linkFlaps"
    debug 1 "switch=$switch"
    debug 1 "ifName=$ifName"
    debug 1 "ifAlias=$ifAlias"
    debug 1 "ifIndex=$ifIndex"
    debug 1 "clientID=$clientID"
fi

echo Database location information by address:
echo "    Building: $bldgName at $bldgCampus campus"
echo "    Router: $routerName VlanID: $number"
echo "    Network: $network/$cidr"

if [ $gNodeInfo -eq 0 ]; then
    echo "Unable to find any location details for address $gMyIP."
    exit 6
else
    OID="1.3.6.1.2.1.2.2.1.8.$ifIndex"
    ifStatus="`snmpget -c uofmbb -v 2c -Oqv $switch $OID`"
fi

echo "    IP Address: $gMyIP"
echo "    MAC Address: $macAddress"
echo "    Switch: $switch"
echo "    Interface: $ifName"
echo "    Description: $ifAlias"

echo Other Details:
echo "    Current Interface Status: $ifStatus"
echo "    Last Link Up on interface: $lastLinkUp"
echo "    Last Link Down on interface: $lastLinkDown"
echo "    Link Down transitions for interface: $linkFlaps"
echo "    ARP entry was last recorded on: $arpLastSeen"
echo "    IP Address was last recorded on: $ipLastSeen"
echo "    MAC Address was last recorded on: $macLastSeen"
echo "    DHCP client ID: $clientID"

echo Hardware location history:
echo "    not yet implemented."











-------------------------------------------------------------------

