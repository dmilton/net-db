#!/bin/sh

gDebug=0
gCGIMode=0

gCommand=`basename $0`

# Macro to generate output for debugging purposes.
debug() {
    debugMsgLevel=$1
    debugMessage=$2
    if [ $gDebug -ge $debugMsgLevel ]; then
	echo $debugMessage > /dev/stderr
	if [ $gCGIMode -eq 1 ]; then
	    echo '<br>'
	fi
    fi
}

