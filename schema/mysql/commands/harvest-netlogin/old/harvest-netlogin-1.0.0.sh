#!/bin/sh
#
PATH=/usr/bin:/usr/sfw/bin

gVersion="1.0.0"

if [ "X$1" = "X" ]; then
    gLogFile=/var/log/extreme.log
else
    gLogFile=$1
fi

if [ ! -r $gLogFile ]; then
    echo $gLogFile is not readable
    exit 1
fi

# This ID only works from procyon.
gMySQLHost=miranda.cc.umanitoba.ca
gMySQLDB=loginInfo
gMySQLUser=logins
gMySQLPass=juy6fdb

gDbUpdate=/tmp/netlogin-$$.txt

# Syslog does not log the year.
gYear=`date +%Y`

#
# Build the table of entries 
#	date-time  host  port  username  mac-address
#
# Log record has multiple formats!
#
# Jun 10 18:56:00 ucd13.cc.umanitoba.ca
#    USER: Network Login User umzubani logged in
#    (192.168.0.20) Mac 00:c0:9f:a1:a4:db Port 12 Vlan temporary

# Jun 12 22:18:56 e2d13.cc.umanitoba.ca 
#     Jun 12 22:18:56 USER: Network Login HTTP User umlau3 logged in
#     (192.168.0.22) Mac 00:10:c6:cf:51:87 Port 31 Vlan temporary
#
grep 'USER: Network Login' $gLogFile | grep 'logged in' | \
    nawk '
	function MonthNumber(month) {
	    if ( month == "Jan" ) { result = 1; }
	    if ( month == "Feb" ) { result = 2; }
	    if ( month == "Mar" ) { result = 3; }
	    if ( month == "Apr" ) { result = 4; }
	    if ( month == "May" ) { result = 5; }
	    if ( month == "Jun" ) { result = 6; }
	    if ( month == "Jul" ) { result = 7; }
	    if ( month == "Aug" ) { result = 8; }
	    if ( month == "Sep" ) { result = 9; }
	    if ( month == "Oct" ) { result = 10; }
	    if ( month == "Nov" ) { result = 11; }
	    if ( month == "Dec" ) { result = 12; }
	    return result;
	}
	function FormatMacAddr(rawAddr) {
	    z = split(rawAddr, addrParts, ":");
	    result = "0x";
	    for ( i = 0; i <= z; i++ ) {
		result = result addrParts[i];
	    }
	    # printf("Raw:\t\t%s\nFormat:\t%s\n", rawAddr, result);
	    return result;
	}
	function DumpRecord(year,month,day,time,fullHost,user,rawMacAddr,port) {
	    mthNbr = MonthNumber(month);
	    z = split(fullHost, hostParts, ".");
	    host = hostParts[1];
	    macAddr = FormatMacAddr(rawMacAddr);
	    printf("insert ignore into loginInfo.netlogin\n");
	    printf("    (loginTime, host, port, user, macAddress) values\n");
	    printf("    (\"%s-%d-%d %s\", \"%s\", %d, \"%s\", %s);\n",
		year, mthNbr, day, time, host, port, user, macAddr);
	    printf("insert into loginInfo.hardwareAddr\n");
	    printf("    (macAddr, lastSeen) values\n");
	    printf("    (%s, \"%d-%d-%d %s\")\n",
		macAddr, year, mthNbr, day, time);
	    printf("  on duplicate key update\n");
	    printf("    lastSeen = \"%s-%d-%d %s\";\n",
		year, mthNbr, day, time);
	}
	/HTTP/ {
	    month = $5; day = $6; time = $7;
	    fullHost = $4;
	    user = $13; rawMacAddr = $18; port = $20;
	    DumpRecord(year,month,day,time,fullHost,user,rawMacAddr,port);
	}
	! /HTTP/ {
	    month = $1; day = $2; time = $3;
	    fullHost = $4;
	    user = $9; rawMacAddr = $14; port = $16;
	    DumpRecord(year,month,day,time,fullHost,user,rawMacAddr,port);
	}
    ' year=$gYear > $gDbUpdate

cat $gDbUpdate | \
    mysql --no-defaults --batch \
	--host=$gMySQLHost --database=$gMySQLDB \
	--user="$gMySQLUser" --password="$gMySQLPass"

rm $gDbUpdate

