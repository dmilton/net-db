#!/usr/bin/env python

import sys
sys.path.append('/usr/local/lib/python')
import debug

# 11 Apr 2008	DM	initial writing.

#
############################################################################
# snmpget
# Get an SNMP OID from a switch.
# 

def snmpget(community, switch, oid):
    from socket import gethostbyaddr
    """
    Get an SNMP OID from a switch.
    var = snmpget(community, switch, oid)
    """
    debug.debug(1, "snmpget: community=" + community + ", switch=" +
	    switch + ", oid=" + oid)
    lookup = gethostbyaddr(switch)
    debug.debug(1, "snmpget: lookup=" + str(lookup))
    if ( lookup == "" ):
	result = "0"
    else:
	# time to do the actual work.
	result = "0"
    ## end if ( lookup == "") else:

    return(result)

## end snmpget()

if __name__ == "__main__":
    debug.setDebug(2)
    getResult = snmpget("public", "t1d02", ".1.3.6.1.2.1.1.5.0")

    print "snmpget result=" + str(getResult)

