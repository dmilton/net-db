#!/usr/bin/env python

# import with:
#	from debug import *
# to ensure you get the local name space for the global variables
# and the debug function definition.
#
# Alternatively, use
#	import debug
# and use the setDebug routine to set the debug level.

# Requires global variables:
global gDebug, gCGIMode

gDebug = 0			# debug level to honour
gCGIMode = 0			# generate html output?

############################################################################
# setDebug(level)                                                          #

def setDebug(level):
    """ Set the debug level for the debugging output.
	    0 - no debug output
	    1 - entry and exit of any fuction or routine.
	    2 - intermediate processing within functions.
    """
    global gDebug
    gDebug = level
##end setDebug

############################################################################
# setCGIMode(level)                                                          #

def setCGIMode(level):
    """ Set the html/cgi output flag.
	    0 - do not generate html output
	    1 - do not generate html output
    """
    global gCGIMode
    gCGIMode = level
##end setCGIMode

############################################################################
# debug(level, message)                                                    #

def debug(debugMsgLevel, debugMessage):
    """Produce output for debugging purposes.
	The debug level is the level of debugging output to generate.
	    0 - no debug output
	    1 - entry and exit of any fuction or routine.
	    2 - intermediate processing within functions.
	the message is any free form text but should take a general
	format indentifying the name of the function/routine followed
	by any information desired.
    """
    global gDebug, gCGIMode
    if ( gDebug >=  debugMsgLevel ):
	print debugMessage
	if ( gCGIMode  ==  1 ):
	    print '<br>'
        ## end if
    ## end if
##end debug

