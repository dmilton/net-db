#!/usr/bin/env python

import sys
sys.path.append('/usr/local/lib/python')
import debug

############################################################################
# host
# Lookup a host name from an IP address.
# Always returns a host name.

def host(myIP):
    from socket import gethostbyaddr
    """
    Perform a host name lookup on the IP address provided.
    """
    debug.debug(1, "host: myIP=" + myIP)
    lookup = gethostbyaddr(myIP)
    debug.debug(1, "host: lookup=" + str(lookup))
    return(lookup)
## end host()

if __name__ == "__main__":
    print dir()
    debug.setDebug(2)
    lookupResult = host(sys.argv[1])
    print lookupResult[0] + str(' has address ') + str(lookupResult[2][0])

