#!/usr/bin/env python

import sys
sys.path.append('/usr/local/lib/python')
import debug

# 11 Apr 2008	DM	initial writing.

#
############################################################################
# LogIfChange
# Log an interface state change.
# 

def LogIfChange(who, switch="", ifName="", newState="", note=""):
    from dbAccess import PgQuery
    """
    Make a log entry in the database to document a switch port
    change in state.
    LogIfChange(who, switch="", ifName="", newState="", note="");
	who should be the userid of the person making the change.
	switch is the host name of the device.
	ifName is the name of the switch port changed.
	newState can be one of up, down, config.
	note is free form text which describes the change.
    """
    debug.debug(1, "LogIfChange: who=" + who + ", switch=" + switch +
	    switch + ", ifName=" + ifName + ", newState="" + newState + 
	    ", note=" + note)

    q1 = "INSERT INTO switchInfo.ifStateChange"
    q3 = "(\"" + who + "\", \"" + switch + "\", \"" + ifName + "\", " + \
	    newState + "\""
    if ( note == "" ):
	q2="VALUES (who, switch, ifName, newState)"
	q4 = ")"
    else:
	q2 = "VALUES (who, switch, ifName, newState, note)"
	q4 = ", \"" + note + "\")"
    query = q1 + q2 + q3 + q4
    PgQuery(query)
## end LogIfChange()

if __name__ == "__main__":
    debug.gDebug = 2
    
    LogIfChange(sys.argv[1], sys.argv[2], sys.argv[3])

