#! /usr/bin/env python

gNoUpdate = 0

import sys
sys.path.append('/usr/local/lib/python')
import debug

############################################################################
# MyQuery(theQuery)

def MyQuery(sqlQuery, dbHost="localhost", \
	dbName="networkInfo", dbUser="viewer", dbPass=""):
    """Send a query to the mysql database.
	Any query is sent to the database and results are returned.
	The query should return only a single row from the database
	and this result is returned by this function.
	The arguments dbHost, dbName, dbUser, and dbPass are optional and 
	have the default values of localhost, networkInfo, viewer, and NULL
	respectively. Override as required.
    """
    # Python MySql DB-API module.
    import MySQLdb

    # assume we do not get a result.
    result = None

    debug.debug(1, "MyQuery: sqlQuery=" . sqlQuery)
    if ( sqlQuery  ==  "" ):
	print "Invalid or missing query"
	sys.exit(2)
    else:
	try:
	    db = MySqlDB.connect(database=dbName, host=dbHost,
		    user=dbUser, password=dbPass)
	except MySqlDB.InternalError, err:
	    debug.debug(1, 'MyQuery: error:' + str(err))
	    sys.exit(2)
	## end try

	cursor = db.cursor()

	# now that we have a connection, send our query.
	cursor.execute(sqlQuery)

	# Get the result of the query.
	result = cursor.fetchall()
    ## end if ( sqlQuery == "" ) else:

    debug.debug(1, 'MyQuery: result=' + result)
    return(result)
## end MyQuery()

############################################################################
# PgQuery(theQuery, [ dbName = "x", ] [ dbUser = "y", ] [ dbPass = "z" ,]
#	[ dbFile="" ])
# send a SQL query to the database. The query should only return a single
# row from the query which is the result of this function call.
# Queries can also be written to the file specified. In this case a query
# can return multiple rows.

def PgQuery(sqlQuery, dbHost="miranda.cc.umanitoba.ca", \
	dbName="netdisco", dbUser="netdisco",dbPass="im2co3j0", \
	dbFile=""):
    """
    Send a SQL query to the postgres database.
    Any query is sent to the database and results are returned.
    Default values for database, user and password are netdisco,
    netdisco, and netdisco respectively. override as required.
    """
    debug.debug(1, "PgQuery: sqlQuery=" + sqlQuery)
    # Python PostgreSQL DB-API module.
    import pgdb

    # assume we do not get a result.
    result = None

    # Get a connection to the database.
    if ( sqlQuery == "" ):
	print "Invalid or missing query"
	sys.exit(2)
    else:
	try:
	    db = pgdb.connect(database=dbName, host=dbHost,
		    user=dbUser, password=dbPass)
	except pgdb.InternalError, err:
	    print str(err)
	    sys.exit(2)
	## end try

	cursor = db.cursor()

	# Now that we have a connection, send our query.
	cursor.execute(sqlQuery)
	# Get the result of the query.
	# handle file output too...
	if ( dbFile == "" ):
	    result = cursor.fetchall()
	else:
	    # write output to a file.
	    print "Unable to perform file output at this time."
	    result = ""
    # end if ( sqlQuery == "" ) else:

    debug.debug(1, 'PgQuery: result=' + str(result))
    return(result)
## end PgQuery()

