#!/bin/sh

gVersion = ""

#  7 Jul 2008           Added version information.

# remove a switch and all associated switch ports from the database.
# This is the cleanest way to update the database after a building
# upgrade has been completed.

gCommand=`basename $0`

gSwitchName=$1

if [ "X$gSwitchName" = "X" ]; then
    echo "Usage: $gCommand switch-name"
    exit 1
fi

q1="delete from switchInfo.ports where switchID = "
q2="    (select id from switchInfo.switches where name = '$gSwitchName');"
q3="delete from switchInfo.switches where name = '$gSwitchName';"

echo $q1 $q2 $q3 | mysql --host=miranda.cc.umanitoba.ca \
    --user=switchInfo --password=kif78gdx

