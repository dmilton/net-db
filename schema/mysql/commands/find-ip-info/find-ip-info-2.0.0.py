#!/usr/bin/python

gVersion = "2.0.0 July 7, 2008"

# This script accepts one argument - an IP address.
# From the IP address it will determine the most likely IP network for
# that IP address.

#  7 Jul 2008           Rewrote to use the netdisco database information
#			instead of the network info/node info database.
# 20 Nov 2007           Changed location of db commands scripts.
# 25 Oct 2007	DM	Fixed more bad table variable references.
# 24 Oct 2007	DM	Fixed some bad table references and moved some
#			of the macros into the global macro script.
# 23 Oct 2007	DM	Changed database table references to use variables
#			so table and database names can change easily.
# 18 Oct 2007	DM	Created a limited output option. Changed many of
#			the variable names so they remained local to the
#			routine OR were named as global variables.
#			Changed database queries to use inet_ntoa for
#			IP address information.
# 24 Aug 2007	DM	When there are multiple entries in the database
#			the order is by addition so the most current entry
#			could be in the middle of the list. Sort the list
#			by last link up to get the most current entry first.
# 23 Aug 2007	DM	Changed database view fields, updated query.
#			Wrote routine to extract the ARP table information
#			in the database when location entries do not exist.
# 16 Jan 2007	DM	Update script to check the ARP table information
#			in the database in case we have ARP entries but
#			do not have physical location.

############################################################################
# Usage()

def Usage():
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print """
usage: %s [-h|--help] [-v|--version] | [-d|--debug N] mac-address
    -h    display this help message
    -v    show version information and exit.
    -d N  debug at level N.
    mac-address formatted as 00:00:00:00:00:00
    """ % ( gCommand )
    sys.exit(1)
## end usage()

def ActiveState(asActive):
    debug.debug(1, "ActiveState: asActive=" + str(asActive))

    if asActive == True:
	rActiveState = "online"
    elif asActive == False:
	rActiveState = "offline"
    else:
	rActiveState = "unknown"
    ## end if

    debug.debug(1, "ActiveState: rActiveState=" + rActiveState)
    return(rActiveState)
## end ActiveState()

def PrintArpEntry(paArgs):
    debug.debug(1, "PrintArpEntry: paArgs=" + str(paArgs))
    paIP=paArgs[0]
    paMac=paArgs[1]
    paActive=paArgs[2]
    paTimeFirst=paArgs[3]
    paTimeLast=paArgs[4]
    debug.debug(1, "PrintArpEntry: paIP=" + paIP + 
	", paMac=" + paMac + ", paActive=" + str(paActive))
    debug.debug(1, "PrintArpEntry: paTimeFirst=" + paTimeFirst +
	", paTimeLast=" + paTimeLast)
    paActive = ActiveState(paActive)
    debug.debug(2, "PrintArpEntry: paActive=" + paActive)

    print "IP Address " + paIP + " was seen with MAC Address "
    print paMac + " at " + paTimeFirst
    print "   and was last seen " + paTimeLast + "."
    print "   The current state is " + paActive + "."

    debug.debug(1, "PrintArpEntry: exit")
## end PrintArpEntry()

# Define routine to get arp table entry information.
def FindArpEntries(faMacAddr, faIpAddr):
    debug.debug(1, "FindArpEntries: faIpAddr=" + faIpAddr)
    debug.debug(1, "FindArpEntries: faMacAddr=" + faMacAddr)

    q1="SELECT ip,mac,active,time_first,time_last"
    q2="FROM node_ip"
    if faIpAddr != "":
	q3 = "WHERE ip = '" + faIpAddr + "'"
    else:
	q3 = "WHERE mac = '" + faMacAddr + "'"
    arpEntries = dbAccess.PgQuery(dbHost="miranda.cc.umanitoba.ca",
	    dbName="netdisco", dbUser="netdisco", dbPass="im2co3j0",
	    sqlQuery=q1 + " " + q2 + " " + q3)
    locnCount = len(arpEntries)
    loop = 0
    while loop < locnCount:
	debug.debug(2, "FindArpEntries: loop=" + str(loop))

	PrintArpEntry(arpEntries[loop])

	debug.debug(2, "FindArpEntries: arpEntry=" + str(arpEntries[loop]))
	loop += 1
    ## end while

    debug.debug(1, "FindArpEntries: exit")
## end FindArpEntries()

############################################################################
# Process command line arguments.

import getopt, sys
from os.path import basename

sys.path.append('/usr/local/lib/python')

import debug
from host import host
import dbAccess

gCommand = basename(sys.argv[0])

def main():
    global gDebug 
    try:
	optList, argList = getopt.getopt(sys.argv[1:], "hvd:", 
		["help", "version", "debug="])
    except getopt.GetoptError, err:
	# print help information and exit:
	print str(err)
	Usage()
	sys.exit(2)
    ## end try

    gDebug = 0
    for option, argument in optList:
	if option in ("-v", "--version"):
	    print gVersion
	    sys.exit(0)
	elif option in ("-h", "--help"):
	    Usage()
	    sys.exit(0)
	elif option in ("-d", "--debug"):
	    debug.setDebug(argument)
	else:
	    assert False, "unhandled option"
	## end if
    ## end for

    if ( len(argList) == 1 ):
	gMyIpAddr = argList[0]
    else:
	Usage()
    ## end if

    debug.debug(1, "gMyIpAddr=" + gMyIpAddr)

    # Do the work...
    # Start by looking for switch port locations...
    gMyMAC = ""
    FindArpEntries(gMyMAC, gMyIpAddr)
## end main()

# Call main to do the work.
if __name__ == "__main__":
    main()

