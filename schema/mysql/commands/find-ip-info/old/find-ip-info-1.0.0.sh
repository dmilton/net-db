#!/bin/bash

gVersion="1.0.0 13 Nov 2006"

# This script accepts one argument - an IP address.
# From the IP address it will determine the most likely
# IP network for that IP address.

. /opt/local/lib/network/db-functions.bash

Usage() {
    echo "Usage: $gCommand [-v] | [-d N] ip-address"
    echo "    -v    show version information and exit."
    echo "    -d N  debug at level N."
    exit 1
}

args=`getopt vd: $*`
errcode=$?
if [ $errcode -ne 0 ]; then
    Usage
fi

set -- $args
for i; do
    case "$i" in
	-d)	# debug
		gDebug=$2
		if [ $gDebug -ge 2 ]; then
		    gDebug=2
		fi
		shift; shift ;;
	-v)	# show version information and exit.
		echo $gCommand: Version $gVersion
		exit 0
		;;
	--)
		shift ; break ;;
    esac
done

gMyIP=$1

if [ "X$gMyIP" = "X" ]; then
    Usage
fi

debug 1 "gMyIP=$gMyIP"

gHexIP=`ip2hex $gMyIP`
debug 1 "gHexIP=$gHexIP"

q1="SELECT bldgCode,name,number,network,cidr FROM networkInfo.networkInfo"
q2="WHERE netNumber < $gHexIP ORDER BY netNumber DESC LIMIT 1"
query="$q1 $q2"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    set -- $rQueryDB
    bldgCode=$1
    name=$2
    number=$3
    network=$4
    cidr=$5
else
    echo Unable to locate IP address \"$gMyIP\" in networkInfo database.
    exit 2
fi
debug 1 "bldgCode=$bldgCode"
debug 1 "name=$name"
debug 1 "number=$number"
debug 1 "network=$network"
debug 1 "cidr=$cidr"

query="SELECT name FROM buildingInfo.buildings WHERE code = \"$bldgCode\""
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    bldgName="$rQueryDB"
else
    echo "Unable to find building info for code \"$bldgCode\""
    exit 3
fi
debug 1 "bldgName=$bldgName"

q1="SELECT location FROM buildingInfo.buildings, buildingInfo.campus"
q2="WHERE buildings.campus = campus.id"
q3="AND code = \"$bldgCode\""
query="$q1 $q2 $q3"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    bldgCampus="$rQueryDB"
else
    echo "Unable to extract location for code \"$bldgCode\""
    exit 4
fi
debug 1 "bldgCampus=$bldgCampus"

q1="SELECT routerName FROM switchInfo.routers"
q2="WHERE bldgCode = \"${bldgCode}\""
query="$q1 $q2"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    routerName="$rQueryDB"
else
    echo "Unable to extract router name for code \"$bldgCode\""
    exit 5
fi
debug 1 "routerName=$routerName"

# Extract what we can from our nodeInfo database...
gNodeInfo=0	# assume we don't get anything.
q1="SELECT HEX(ipAddress),"
q2="    CONCAT("
q3="	    SUBSTR(HEX(macAddress),1,4), \".\","
q4="        SUBSTR(HEX(macAddress),5,4), \".\","
q5="        SUBSTR(HEX(macAddress),9,4)),"
q6="    arpLastSeen, ipLastSeen, macLastSeen, switch, ifName, ifAlias, clientID"
q7="FROM nodeInfo.nodeInfo"
q8="WHERE ipAddress = $gHexIP"
query="$q1 $q2 $q3 $q4 $q5 $q6 $q7 $q8"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    OLDIFS=$IFS
    IFS='	'
    set -- $rQueryDB
    ipAddress=`hex2ip $1`
    macAddress=$2
    arpLastSeen="$3"
    ipLastSeen="$4"
    macLastSeen="$5"
    switch=$6
    ifName=$7
    ifAlias=$8
    clientID=$9
    gNodeInfo=1
    debug 1 "ipAddress=$ipAddress"
    debug 1 "macAddress=$macAddress"
    debug 1 "arpLastSeen=$arpLastSeen"
    debug 1 "ipLastSeen=$ipLastSeen"
    debug 1 "macLastSeen=$macLastSeen"
    debug 1 "switch=$switch"
    debug 1 "ifName=$ifName"
    debug 1 "ifAlias=$ifAlias"
    debug 1 "clientID=$clientID"
fi

echo Database location information by address:
echo "    Building: $bldgName at $bldgCampus campus"
echo "    Router: $routerName VlanID: $number"
echo "    Network: $network/$cidr"

if [ $gNodeInfo -eq 0 ]; then
    echo "Unable to find any location details for address $gMyIP."
    exit 6
fi

echo "    IP Address: $gMyIP"
echo "    MAC Address: $macAddress"
echo "    Switch: $switch"
echo "    Interface: $ifName"
echo "    Description: $ifAlias"

echo Other Details:
echo "    ARP entry was last recorded on: $arpLastSeen"
echo "    IP Address was last recorded on: $ipLastSeen"
echo "    MAC Address was last recorded on: $macLastSeen"
echo "    DHCP client ID: $clientID"

echo Hardware location history:
echo "    not yet implemented."
