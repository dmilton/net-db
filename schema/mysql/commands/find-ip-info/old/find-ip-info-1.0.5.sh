#!/bin/bash

gVersion="1.0.5 - 07 Jul 2008"

# This script accepts one argument - an IP address.
# From the IP address it will determine the most likely
# IP network for that IP address.

#  7 Jul 2008           Changed lib directory to /usr/local/lib/network.
#			Fixed the query to look up the router name for a
#			building. The router table was removed in favour
#			of having it listed in the switch table with a
#			flag to indicate it was a router.
# 20 Nov 2007           Changed location of db commands scripts.
# 25 Oct 2007	DM	Fixed more bad table variable references.
# 24 Oct 2007	DM	Fixed some bad table references and moved some
#			of the macros into the global macro script.
# 23 Oct 2007	DM	Changed database table references to use variables
#			so table and database names can change easily.
# 18 Oct 2007	DM	Created a limited output option. Changed many of
#			the variable names so they remained local to the
#			routine OR were named as global variables.
#			Changed database queries to use inet_ntoa for
#			IP address information.
# 24 Aug 2007	DM	When there are multiple entries in the database
#			the order is by addition so the most current entry
#			could be in the middle of the list. Sort the list
#			by last link up to get the most current entry first.
# 23 Aug 2007	DM	Changed database view fields, updated query.
#			Wrote routine to extract the ARP table information
#			in the database when location entries do not exist.
# 16 Jan 2007	DM	Update script to check the ARP table information
#			in the database in case we have ARP entries but
#			do not have physical location.

. /usr/local/lib/network/db-functions.bash
. /usr/local/lib/network/debug.bash

# Basic/simplified output? 0 = no, 1 = yes.
gBasicOutput=0

Usage() {
    echo "Usage: $gCommand [-v] | [-d N] ip-address"
    echo "    -v    show version information and exit."
    echo "    -d N  debug at level N."
    exit 1
}

GetNodeInfoByIP() {
    gniOffset=$1
    gniNodeIP=$2
    debug 1 "GetNodeInfoByIP: gniOffset=$gniOffset gniNodeIP=$gniNodeIP"
    
    # The first time this is called we need to find out if 
    # there is any history to display. This is done by counting
    # the number of entries for this IP address.
    if [ $gniOffset -eq 0 ]; then
	# How many entries have we got for this IP?
	gniQ1="SELECT count(ipAddress) FROM $gNodeInfoView"
	gniQ2="WHERE ipAddress = inet_aton(\"$gniNodeIP\")"
	gniQuery="$gniQ1 $gniQ2"
	QueryDB "$gniQuery"
	gniResult=$?
	if [ $gniResult -eq 0 ]; then
	    gNodeEntries=$rQueryDB
	else
	    gNodeEntries=0
	fi
	debug 2 "GetNodeInfoByIP: gNodeEntries=$gNodeEntries"
    fi

    # Extract what we can from our nodeInfo database...
    gniQ1="SELECT"
    gniQ2="    inet_ntoa(ipAddress),"
    gniQ3="    CONCAT("
    gniQ4="        SUBSTR(HEX(macAddress),1,4), \".\","
    gniQ5="        SUBSTR(HEX(macAddress),5,4), \".\","
    gniQ6="        SUBSTR(HEX(macAddress),9,4)),"
    gniQ7="    arpLastSeen, macLastSeen, lastLinkUp, lastLinkDown,"
    gniQ8="    linkFlaps, switch, ifName, ifAlias, ifIndex, clientID"
    gniQ9="FROM $gNodeInfoView"
    gniQa="WHERE ipAddress = inet_aton(\"$gniNodeIP\")"
    gniQb="ORDER BY lastLinkUp DESC"
    gniQc="LIMIT $gniOffset,1"
    gniQuery="$gniQ1 $gniQ2 $gniQ3 $gniQ4 $gniQ5 $gniQ6"
    gniQuery="$gniQuery $gniQ7 $gniQ8 $gniQ9 $gniQa $gniQb $gniQc"
    if [ $gNodeEntries -gt 0 ]; then
	QueryDB "$gniQuery"
	gniResult=$?
	if [ $gniResult -eq 0 ]; then
	    OLDIFS=$IFS
	    IFS='	'
	    set -- $rQueryDB
	    ipAddress=$1
	    macAddress=$2
	    arpLastSeen="$3"
	    macLastSeen="$4"
	    lastLinkUp="$5"
	    lastLinkDown="$6"
	    linkFlaps=$7
	    switch=$8
	    ifName=${9}
	    ifAlias=${10}
	    ifIndex=${11}
	    clientID=${12}
	    debug 2 "ipAddress=$ipAddress"
	    debug 2 "macAddress=$macAddress"
	    debug 2 "arpLastSeen=$arpLastSeen"
	    debug 2 "macLastSeen=$macLastSeen"
	    debug 2 "lastLinkUp=$lastLinkUp"
	    debug 2 "lastLinkDown=$lastLinkDown"
	    debug 2 "linkFlaps=$linkFlaps"
	    debug 2 "switch=$switch"
	    debug 2 "ifName=$ifName"
	    debug 2 "ifAlias=$ifAlias"
	    debug 2 "ifIndex=$ifIndex"
	    debug 2 "clientID=$clientID"
	    # we have some node information, check out the switch port.
	    OID="1.3.6.1.2.1.2.2.1.8.$ifIndex"
	    ifStatus="`snmpget -c uofmbb -v 2c -Oqv $switch $OID`"
	    debug 2 "ifStatus=$ifStatus"
	fi
    fi

    debug 1 "GetNodeInfoByIP: gNodeEntries=$gNodeEntries, gniResult=$gniResult"
    return $gniResult
}

GetArpInfoByIP() {
    gaiOffset=$1
    gaiNodeIP=$2
    debug 1 "GetArpInfoByIP: gaiOffset=$gaiOffset gaiNodeIP=$gaiNodeIP"
    
    # The first time this is called we need to find out if 
    # there is any history to display. This is done by counting
    # the number of entries for this IP address.
    if [ $gaiOffset -eq 0 ]; then
	gArpEntries=0
	# How many entries have we got for this IP?
	gaiQ1="SELECT count(ipAddress) FROM $gNodeInfoArp"
	gaiQ2="WHERE ipAddress = inet_aton(\"$gaiNodeIP\");"
	gaiQuery="$gaiQ1 $gaiQ2"
	QueryDB "$gaiQuery"
	gaiResult=$?
	if [ $gaiResult -eq 0 ]; then
	    gArpEntries=$rQueryDB
	fi
	debug 2 "GetArpInfoByIP: gArpEntries=$gArpEntries"
    fi

    # Extract what we can from our activeARP table...
    gaiQ1="SELECT"
    gaiQ2="    inet_ntoa(ipAddress),"
    gaiQ3="    CONCAT("
    gaiQ4="        SUBSTR(HEX(macAddress),1,4), \".\","
    gaiQ5="        SUBSTR(HEX(macAddress),5,4), \".\","
    gaiQ6="        SUBSTR(HEX(macAddress),9,4)),"
    gaiQ7="    lastSeen"
    gaiQ8="FROM $gNodeInfoArp"
    gaiQ9="WHERE ipAddress = inet_aton(\"$gaiNodeIP\")"
    gaiQa="ORDER BY lastSeen DESC"
    gaiQb="LIMIT $gaiOffset,1"
    gaiQuery="$gaiQ1 $gaiQ2 $gaiQ3 $gaiQ4 $gaiQ5 $gaiQ6"
    gaiQuery="$gaiQuery $gaiQ7 $gaiQ8 $gaiQ9 $gaiQa $gaiQb"
    if [ $gArpEntries -gt 0 ]; then
	QueryDB "$gaiQuery"
	gaiResult=$?
	if [ $gaiResult -eq 0 ]; then
	    OLDIFS=$IFS
	    IFS='	'
	    set -- $rQueryDB
	    ipAddress=$1
	    macAddress=$2
	    arpLastSeen="$3"
	    debug 2 "ipAddress=$ipAddress"
	    debug 2 "macAddress=$macAddress"
	    debug 2 "arpLastSeen=$arpLastSeen"
	fi
    fi

    debug 1 "GetArpInfoByIP: gArpEntries=$gArpEntries, gaiResult=$gaiResult"
    return $gaiResult
}

DisplayNodeInfo() {
    echo " "
    echo "    Workstation Information: $gMyIP/$macAddress"
    if [ $gBasicOutput -eq 0 ]; then
	echo "    Switch: $switch - $ifName - $ifAlias (link is $ifStatus)"
    else
	case $ifStatus in
	down)
	    state="machine offline" ;;
	up)
	    state="machine online" ;;
	*)
	    state="" ;;
	esac
	echo "    Communications Outlet: $ifAlias ($state)"
    fi
    echo "    Last Link Up on interface: $lastLinkUp"
    echo "    Last Link Down on interface: $lastLinkDown"
    echo "    Link Up/Down transitions for interface: $linkFlaps"
    echo " "
    echo "    ARP entry was last recorded on: $arpLastSeen"
    echo "    IP Address was last recorded on: $ipLastSeen"
    echo "    MAC Address was last recorded on: $macLastSeen"
    echo "    DHCP client ID: $clientID"
}

DisplayArpInfo() {
    echo " "
    echo "    ARP Entry: $gMyIP/$macAddress"
    echo "    ARP entry last recorded on: $arpLastSeen"
}

args=`getopt vd: $*`
errcode=$?
if [ $errcode -ne 0 ]; then
    Usage
fi

set -- $args
for i; do
    case "$i" in
	-d)	# debug
		gDebug=$2
		if [ $gDebug -ge 2 ]; then
		    gDebug=2
		fi
		shift; shift ;;
	-v)	# show version information and exit.
		echo $gCommand: Version $gVersion
		exit 0
		;;
	--)
		shift ; break ;;
    esac
done

gMyIP=$1

if [ "X$gMyIP" = "X" ]; then
    Usage
fi

debug 1 "gMyIP=$gMyIP"

# Use the networkInfo table to determine building information for
# this IP address.
q1="SELECT bldgCode,name,number,network,cidr"
q2="FROM $gNetInfoView"
q3="WHERE netID < inet_aton(\"$gMyIP\")"
q4="ORDER BY netID DESC LIMIT 1"
query="$q1 $q2 $q3 $q4"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    set -- $rQueryDB
    bldgCode=$1
    name=$2
    number=$3
    network=$4
    cidr=$5
else
    echo Unable to locate IP address \"$gMyIP\" in networkInfo database.
    exit 2
fi
debug 1 "bldgCode=$bldgCode"
debug 1 "name=$name"
debug 1 "number=$number"
debug 1 "network=$network"
debug 1 "cidr=$cidr"

# Now that we have a building code we can get the campus location and
# other details about the building itself.

q1="SELECT name, location"
q2="FROM $gBldgInfoBuildings, $gBldgInfoCampus"
q3="WHERE buildings.campus = campus.id"
q4="AND code = \"$bldgCode\""
query="$q1 $q2 $q3 $q4"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    OLDIFS=$IFS
    IFS='	'
    set -- $rQueryDB
    bldgName="$1"
    bldgCampus="$2"
    IFS=$OLDIFS
else
    echo "Unable to find building and location info for code \"$bldgCode\""
    exit 3
fi
debug 1 "bldgName=$bldgName"
debug 1 "bldgCampus=$bldgCampus"

q1="SELECT name"
q2="FROM $gSwitchInfoSwitches"
q3="WHERE bldgCode = \"${bldgCode}\""
q4="AND deviceType = \"router\""
QueryDB "$q1 $q2 $q3 $q4"
result=$?
if [ $result -eq 0 ]; then
    routerName="$rQueryDB"
else
    echo "Unable to extract router name for code \"$bldgCode\""
    exit 4
fi
debug 1 "routerName=$routerName"

echo Campus location information for IP address $gMyIP:
echo "    Building: $bldgName at $bldgCampus campus"
echo "    Network: $network/$cidr"
if [ $gBasicOutput -eq 0 ]; then
    echo "    Router: $routerName VlanID: $number ($name)"
else
    echo "    Router: $routerName"
fi

gNodeEntries=0
gArpEntries=0

GetNodeInfoByIP 0 $gMyIP
debug 1 "gNodeEntries=$gNodeEntries"
if [ $gNodeEntries -eq 0 ]; then
    GetArpInfoByIP 0 $gMyIP
    debug 1 "gArpEntries=$gArpEntries"
fi

if [ $gNodeEntries -gt 0 ]; then
    DisplayNodeInfo
    echo " "
    echo IP Address location history:
    count=1
    while [ $count -lt $gNodeEntries ]; do
	GetNodeInfoByIP $count $gMyIP
	result=$?
	if [ $result -eq 0 ]; then
	    DisplayNodeInfo
	fi
	count=`expr $count + 1`
    done
fi

if [ $gArpEntries -gt 0 ]; then
    DisplayArpInfo
    echo " "
    if [ $gArpEntries -gt 1 ]; then
	echo ARP history for this IP:
	count=2
	while [ $count -lt $gArpEntries ]; do
	    GetArpInfoByIP $count $gMyIP
	    result=$?
	    if [ $result -eq 0 ]; then
		DisplayArpInfo
	    fi
	    count=`expr $count + 1`
	done
    fi
fi

