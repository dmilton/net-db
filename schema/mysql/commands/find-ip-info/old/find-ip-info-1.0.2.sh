#!/bin/bash

gVersion="1.0.2 23 Aug 2007"

# This script accepts one argument - an IP address.
# From the IP address it will determine the most likely
# IP network for that IP address.

# 23 Aug 2007	DM	Changed database view fields, updated query.
#			Wrote routine to extract the ARP table information
#			in the database when location entries do not exist.
# 16 Jan 2007	DM	Update script to check the ARP table information
#			in the database in case we have ARP entries but
#			do not have physical location.

. /opt/local/lib/db-functions.bash

Usage() {
    echo "Usage: $gCommand [-v] | [-d N] ip-address"
    echo "    -v    show version information and exit."
    echo "    -d N  debug at level N."
    exit 1
}

GetNodeInfoByIP() {
    offset=$1
    nodeIP=$2
    debug 1 "GetNodeInfoByIP: offset=$offset nodeIP=$nodeIP"
    
    # The first time this is called we need to find out if 
    # there is any history to display. This is done by counting
    # the number of entries for this IP address.
    if [ $offset -eq 0 ]; then
	# How many entries have we got for this IP?
	q1="SELECT count(ipAddress) FROM nodeInfo.nodeInfo"
	q2="WHERE ipAddress = $nodeIP"
	query="$q1 $q2"
	QueryDB "$query"
	result=$?
	if [ $result -eq 0 ]; then
	    nodeEntries=$rQueryDB
	else
	    nodeEntries=0
	fi
	debug 2 "GetNodeInfoByIP: nodeEntries=$nodeEntries"
    fi

    # Extract what we can from our nodeInfo database...
    q1="SELECT"
    q2="    CONCAT('0x', HEX(ipAddress)),"
    q3="    CONCAT("
    q4="	    SUBSTR(HEX(macAddress),1,4), \".\","
    q5="        SUBSTR(HEX(macAddress),5,4), \".\","
    q6="        SUBSTR(HEX(macAddress),9,4)),"
    q7="    arpLastSeen, macLastSeen, lastLinkUp, lastLinkDown,"
    q8="    linkFlaps, switch, ifName, ifAlias, ifIndex, clientID"
    q9="FROM nodeInfo.nodeInfo"
    qa="WHERE ipAddress = $nodeIP"
    qb="LIMIT $offset,1"
    query="$q1 $q2 $q3 $q4 $q5 $q6 $q7 $q8 $q9 $qa $qb"
    if [ $nodeEntries -gt 0 ]; then
	QueryDB "$query"
	result=$?
	if [ $result -eq 0 ]; then
	    OLDIFS=$IFS
	    IFS='	'
	    set -- $rQueryDB
	    ipAddress=`hex2ip $1`
	    macAddress=$2
	    arpLastSeen="$3"
	    macLastSeen="$4"
	    lastLinkUp="$5"
	    lastLinkDown="$6"
	    linkFlaps=$7
	    switch=$8
	    ifName=${9}
	    ifAlias=${10}
	    ifIndex=${11}
	    clientID=${12}
	    debug 2 "ipAddress=$ipAddress"
	    debug 2 "macAddress=$macAddress"
	    debug 2 "arpLastSeen=$arpLastSeen"
	    debug 2 "macLastSeen=$macLastSeen"
	    debug 2 "lastLinkUp=$lastLinkUp"
	    debug 2 "lastLinkDown=$lastLinkDown"
	    debug 2 "linkFlaps=$linkFlaps"
	    debug 2 "switch=$switch"
	    debug 2 "ifName=$ifName"
	    debug 2 "ifAlias=$ifAlias"
	    debug 2 "ifIndex=$ifIndex"
	    debug 2 "clientID=$clientID"
	    # we have some node information, check out the switch port.
	    OID="1.3.6.1.2.1.2.2.1.8.$ifIndex"
	    ifStatus="`snmpget -c uofmbb -v 2c -Oqv $switch $OID`"
	    debug 2 "ifStatus=$ifStatus"
	fi
    fi

    debug 1 "GetNodeInfoByIP: nodeEntries=$nodeEntries, result=$result"
    return $result
}

GetArpInfoByIP() {
    offset=$1
    nodeIP=$2
    debug 1 "GetArpInfoByIP: offset=$offset nodeIP=$nodeIP"
    
    # The first time this is called we need to find out if 
    # there is any history to display. This is done by counting
    # the number of entries for this IP address.
    if [ $offset -eq 0 ]; then
	arpEntries=0
	# How many entries have we got for this IP?
	q1="SELECT count(ipAddress) FROM nodeInfo.activeARP"
	q2="WHERE ipAddress = $nodeIP"
	query="$q1 $q2"
	QueryDB "$query"
	result=$?
	if [ $result -eq 0 ]; then
	    arpEntries=$rQueryDB
	fi
	debug 2 "GetArpInfoByIP: arpEntries=$arpEntries"
    fi

    # Extract what we can from our activeARP table...
    q1="SELECT"
    q2="    CONCAT('0x', HEX(ipAddress)),"
    q3="    CONCAT("
    q4="	    SUBSTR(HEX(macAddress),1,4), \".\","
    q5="        SUBSTR(HEX(macAddress),5,4), \".\","
    q6="        SUBSTR(HEX(macAddress),9,4)),"
    q7="    lastSeen"
    q8="FROM nodeInfo.activeARP"
    q9="WHERE ipAddress = $nodeIP"
    qa="LIMIT $offset,1"
    query="$q1 $q2 $q3 $q4 $q5 $q6 $q7 $q8 $q9 $qa"
    if [ $arpEntries -gt 0 ]; then
	QueryDB "$query"
	result=$?
	if [ $result -eq 0 ]; then
	    OLDIFS=$IFS
	    IFS='	'
	    set -- $rQueryDB
	    ipAddress=`hex2ip $1`
	    macAddress=$2
	    arpLastSeen="$3"
	    debug 2 "ipAddress=$ipAddress"
	    debug 2 "macAddress=$macAddress"
	    debug 2 "arpLastSeen=$arpLastSeen"
	fi
    fi

    debug 1 "GetArpInfoByIP: arpEntries=$arpEntries, result=$result"
    return $result
}

DisplayNodeInfo() {
    echo " "
    echo "    Workstation Information: $gMyIP/$macAddress"
    echo "    Switch: $switch - $ifName - $ifAlias (link is $ifStatus)"
    echo "    Last Link Up on interface: $lastLinkUp"
    echo "    Last Link Down on interface: $lastLinkDown"
    echo "    Link Up/Down transitions for interface: $linkFlaps"
    echo " "
    echo "    ARP entry was last recorded on: $arpLastSeen"
    echo "    IP Address was last recorded on: $ipLastSeen"
    echo "    MAC Address was last recorded on: $macLastSeen"
    echo "    DHCP client ID: $clientID"
}

DisplayArpInfo() {
    echo " "
    echo "    ARP Entry: $gMyIP/$macAddress"
    echo "    ARP entry last recorded on: $arpLastSeen"
}

args=`getopt vd: $*`
errcode=$?
if [ $errcode -ne 0 ]; then
    Usage
fi

set -- $args
for i; do
    case "$i" in
	-d)	# debug
		gDebug=$2
		if [ $gDebug -ge 2 ]; then
		    gDebug=2
		fi
		shift; shift ;;
	-v)	# show version information and exit.
		echo $gCommand: Version $gVersion
		exit 0
		;;
	--)
		shift ; break ;;
    esac
done

gMyIP=$1

if [ "X$gMyIP" = "X" ]; then
    Usage
fi

debug 1 "gMyIP=$gMyIP"

gHexIP=`ip2hex $gMyIP`
debug 1 "gHexIP=$gHexIP"

# Use the networkInfo table to determine building information for
# this IP address.
q1="SELECT bldgCode,name,number,network,cidr"
q2="FROM networkInfo.networkInfo"
q3="WHERE netNumber < $gHexIP"
q4="ORDER BY netNumber DESC LIMIT 1"
query="$q1 $q2 $q3 $q4"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    set -- $rQueryDB
    bldgCode=$1
    name=$2
    number=$3
    network=$4
    cidr=$5
else
    echo Unable to locate IP address \"$gMyIP\" in networkInfo database.
    exit 2
fi
debug 1 "bldgCode=$bldgCode"
debug 1 "name=$name"
debug 1 "number=$number"
debug 1 "network=$network"
debug 1 "cidr=$cidr"

# Now that we have a building code we can get the campus location and
# other details about the building itself.

q1="SELECT name, location"
q2="FROM buildingInfo.buildings, buildingInfo.campus"
q3="WHERE buildings.campus = campus.id"
q4="AND code = \"$bldgCode\""
query="$q1 $q2 $q3 $q4"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    OLDIFS=$IFS
    IFS='	'
    set -- $rQueryDB
    bldgName="$1"
    bldgCampus="$2"
    IFS=$OLDIFS
else
    echo "Unable to find building and location info for code \"$bldgCode\""
    exit 3
fi
debug 1 "bldgName=$bldgName"
debug 1 "bldgCampus=$bldgCampus"

q1="SELECT routerName"
q2="FROM switchInfo.routers"
q3="WHERE bldgCode = \"${bldgCode}\""
query="$q1 $q2 $q3"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    routerName="$rQueryDB"
else
    echo "Unable to extract router name for code \"$bldgCode\""
    exit 4
fi
debug 1 "routerName=$routerName"

echo Campus location information for IP address $gMyIP:
echo "    Building: $bldgName at $bldgCampus campus"
echo "    Network: $network/$cidr"
echo "    Router: $routerName VlanID: $number ($name)"

nodeEntries=0
arpEntries=0

GetNodeInfoByIP 0 $gHexIP
debug 1 "nodeEntries=$nodeEntries"
if [ $nodeEntries -eq 0 ]; then
    GetArpInfoByIP 0 $gHexIP
    debug 1 "arpEntries=$arpEntries"
fi

if [ $nodeEntries -gt 0 ]; then
    DisplayNodeInfo
    echo " "
    echo IP Address location history:
    count=1
    while [ $count -lt $nodeEntries ]; do
	GetNodeInfoByIP $count $gHexIP
	result=$?
	if [ $result -eq 0 ]; then
	    DisplayNodeInfo
	fi
	count=`expr $count + 1`
    done
fi

if [ $arpEntries -gt 0 ]; then
    DisplayArpInfo
    echo " "
    if [ $arpEntries -gt 1 ]; then
	echo ARP history for this IP:
	count=2
	while [ $count -lt $arpEntries ]; do
	    GetArpInfoByIP $count $gHexIP
	    result=$?
	    if [ $result -eq 0 ]; then
		DisplayArpInfo
	    fi
	    count=`expr $count + 1`
	done
    fi
fi

