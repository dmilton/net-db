#!/bin/bash

gVersion="1.0.1 7 Dec 2006"

# This script accepts two arguments - a switch name and interface name.
# From that information it will return any known IP/MAC addresses seen
# on that interface.

# 18 Dec 2007	DM	Initial writing.

. /opt/local/lib/db-functions.bash

Usage() {
    echo "Usage: $gCommand [-v] | [-d N] switch-name interface-name"
    echo "    -v    show version information and exit."
    echo "    -d N  debug at level N."
    exit 1
}

args=`getopt vd: $*`
errcode=$?
if [ $errcode -ne 0 ]; then
    Usage
fi

set -- $args
for i; do
    case "$i" in
	-d)	# debug
		gDebug=$2
		if [ $gDebug -ge 2 ]; then
		    gDebug=2
		fi
		shift; shift ;;
	-v)	# show version information and exit.
		echo $gCommand: Version $gVersion
		exit 0
		;;
	--)
		shift ; break ;;
    esac
done

ExtractNodeInfo() {
    dbIndex=$1
    debug 1 "ExtractNodeInfo: dbIndex=$dbIndex"

    # Query the nodeInfo database for this switch port.
    q1="SELECT inet_ntoa(ipAddress),"
    q2="    CONCAT("
    q3="	    SUBSTR(HEX(macAddress),1,4), \".\","
    q4="        SUBSTR(HEX(macAddress),5,4), \".\","
    q5="        SUBSTR(HEX(macAddress),9,4)),"
    q6="    arpLastSeen, macLastSeen, clientID"
    q7="FROM nodeInfo.nodeInfo"
    q8="WHERE switch = \"$gMySwitch\""
    q9="AND ifName = \"$gMyInterface\""
    qA="LIMIT $dbIndex,1"
    query="$q1 $q2 $q3 $q4 $q5 $q6 $q7 $q8 $q9"
    QueryDB "$query"
    result=$?
    rExtractNodeInfo="$rQueryDB"
    if [ $result -eq 0 ]; then
	OLDIFS=$IFS
	IFS='	'
	set -- $rQueryDB
	ipAddress=$1
	macAddress=$2
	arpLastSeen="$3"
	macLastSeen="$5"
	clientID="$6"
	gNodeInfo=1
	debug 2 "ipAddress=$ipAddress"
	debug 2 "hexIP=$hexIP"
	debug 2 "macAddress=$macAddress"
	debug 2 "arpLastSeen=$arpLastSeen"
	debug 2 "ipLastSeen=$ipLastSeen"
	debug 2 "macLastSeen=$macLastSeen"
	debug 2 "clientID=$clientID"
	echo "IP Address/Hardware Address: $ipAddress/$macAddress"
	echo "IP Address Last seen: $arpLastSeen"
	if [ "X$clientID" != "X" ]; then
	    echo "DHCP Client ID: $clientID"
	fi
    fi
    debug 1 "ExtractNodeInfo: result=$result"
}

# Test the switch name.
gMySwitch=$1
if [ "X$gMySwitch" = "X" ]; then
    Usage
fi
debug 1 "gMySwitch=$gMySwitch"

# Test the interface name.
gMyInterface=$2
if [ "X$gMyInterface" = "X" ]; then
    Usage
fi
debug 1 "gMyInterface=$gMyInterface"

# Extract the information we can from the switchInfo.switchPorts table.
q1="SELECT ifAlias, ifAdminStatus, lastLinkUp, lastLinkDown"
q2="FROM switchInfo.switchPorts"
q3="WHERE switch = \"$gMySwitch\""
q4="AND ifName = \"$gMyInterface\""
query="$q1 $q2 $q3 $q4"
QueryDB "$query"
result=$?
if [ $result -eq 0 ]; then
    OLDIFS=$IFS
    IFS="	"
    set -- $rQueryDB
    ifAlias="$1"
    ifAdminStatus="$2"
    lastLinkUp="$3"
    lastLinkDown="$4"
    IFS=$OLDIFS
    debug 1 "ifAlias=$ifAlias"
    debug 1 "ifAdminStatus=$ifAdminStatus"
    debug 1 "lastLinkUp=$lastLinkUp"
    debug 1 "lastLinkDown=$lastLinkDown"
else
    echo "Could not find this switch port in the database"
    exit 2
fi

echo "Interface: $gMySwitch $gMyInterface"
echo "Description: $ifAlias"
echo "Last Link Up: $lastLinkUp"
echo "Last Link Down: $lastLinkDown"
echo ""

# Assume we do not have nodeInfo information in the database.
gNodeInfoCount=0

# Figure out how many entries we may have in the nodeInfo database.
q1="SELECT count(*) from nodeInfo.nodeInfo"
q2="WHERE switch = \"$gMySwitch\""
q3="AND ifName = \"$gMyInterface\""
query="$q1 $q2 $q3"
QueryDB "$query"
gNodeInfoCount=$rQueryDB

if [ $gNodeInfoCount -ge 1 ]; then
    nodeIndex=0
    while [ $nodeIndex -lt $gNodeInfoCount ]; do
	ExtractNodeInfo $nodeIndex
	echo ""
	nodeIndex=`expr $nodeIndex + 1`
    done
else
    echo "Limited information is available. No device has been recorded"
    echo "on this interface."
fi

