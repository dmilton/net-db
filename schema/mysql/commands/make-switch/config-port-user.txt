 ! description DESCRIPTION
 ! switchport access vlan USER_VLAN
 switchport mode access
 switchport voice vlan 260
 switchport port-security maximum 3
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation protect
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
end
