#!/bin/sh

PATH=/bin:/usr/bin:/usr/local/mysql/bin

# This script should be called from InterMapper. It will alter the
# database link state change fields for a given switch and ifindex.
# In order for this script to work a "Command Line Notifier" needs to
# be created and this command needs to be installed in the InterMapper
# Tools directory.

# In this instance the notifier name is 
#	zTrap-handler
# The Notifier Type is
#	Command Line
# The Command is
#	link-trap-handler "${MESSAGE}"
# You must edit the message for the following:
#	<Device Name> <Device Condition>

# These two arguments are always present, the rest are variable
# depending upon the trap being processed.

# In order to get all traps handled properly we need to quote the
# arguments in InterMapper. This just splits them back out again.
set -- $1

# Now grap the command line arguments we want.
switch=$1
trapType=$2
ifIndex=$5

# If there are no arguments, exit silently.
if [ "X$switch" = "X" ]; then
    exit 0
fi

# Make sure we have a trap type:
if [ "X$trapType" = "X" ]; then
    exit 0
fi

# We have two query template variables; qUpdate and qWhere are constant
# throughout all of the database updates done here.
qUpdate="UPDATE switchInfo.switchPorts SET"
qWhere="WHERE switch = \"$switch\" AND ifIndex = $ifIndex;"

# create a time stamp value for our lastLinkUp or lastLinkDown field.
gTime=`date "+%Y-%m-%d %H:%M:%S"`

#
# Handle a link up trap...
# Whenever a link goes up update the following:
#	lastLinkUp = current timestamp
#	ifOperStatus = up
#	linkFlaps = linkFlaps + 1
#
LinkUpTrap() {
    q1="lastLinkUp = \"$gTime\","
    q2="ifOperStatus = 1,"
    q3="linkFlaps = linkFlaps + 1"
    query="$qUpdate $q1 $q2 $q3 $qWhere"

    echo $query | \
	mysql --host=titan.cc.umanitoba.ca --database=switchInfo \
	    --user="switchInfo" --password="4p5y6f7g"
}

#
# Handle a link down trap...
# There is more work to do here since there are various things that can cause
# a link down condition. Various fields need to be updated depending on why
# the port is going down.
# For adminDown to adminUp
#	lastLinkDown = current timestamp - shows when port was enabled.
#	ifAdminStatus = up
#	ifOperStatus = down
#	ifAlias = update from switch config.
# For adminUp to adminDown, regardless of operStatus
#	lastLinkUp = NULL
#	lastLinkDown = NULL
#	linkFlaps = 0
#	ifAdminStatus = down (2)
#	ifOperStatus = down (2)
# For operUp to operDown
#	lastLinkDown = current timestamp
#	ifOperStatus = down (2)
LinkDownTrap() {
    ifIndex=$2

    # Now figure out if this was an administrative status change.
    OID="1.3.6.1.2.1.2.2.1.7.$ifIndex"
    ifAdminStatus=`snmpget -c uofmbb -v 2c -Oqsev $theSwitch $OID`
    if [ $ifAdminStatus -eq 2 ]; then
	# Port is admin down. Port moved from admin up to admin down.
	q1="lastLinkUp = NULL,"
	q2="lastLinkDown = NULL,"
	q3="linkFlaps = 0,"
	q4="ifAdminStatus = 2,"
	q5="ifOperStatus = 2"
	query="$qUpdate $q1 $q2 $q3 $q4 $q5 $qWhere"
    else
	# Port is admin up. What was the previous admin status?
	q1="SELECT ifAdminStatus FROM switchInfo.switchPorts"
	query="$q1 $qWhere"
	dbAdminStatus=`echo $query | \
	    mysql --host=titan.cc.umanitoba.ca --database=switchInfo \
		--user="switchInfo" --password="4p5y6f7g"`
	if [ $dbAdminStatus = "down" ]; then
	    # port was admin up and is now admin down.
	    OID=".1.3.6.1.2.1.31.1.1.1.18.$ifIndex"
	    ifAlias="`snmpget -c uofmbb -v 2c -Oqsev $theSwitch $OID`"
	    # port changed from admin down to admin up.
	    q1="lastLinkDown = \"$gTime\","
	    q2="ifAdminStatus = 1,"
	    q3="ifOperStatus = 2,"
	    q4="ifAlias = \"$ifAlias\""
	    query="$qUpdate $q1 $q2 $q3 $q4 $qWhere"
	else
	    # port was admin up and is still admin up.
	    # This means the link went from up to down.
	    q1="lastLinkDown = \"$gTime\","
	    q2="ifOperStatus = 2"
	    query="$qUpdate $q1 $q2 $qWhere"
	fi
    fi

    # Perform the database update for this switch port.
    echo $query | \
	mysql --host=titan.cc.umanitoba.ca --database=switchInfo \
	    --user="switchInfo" --password="4p5y6f7g"
}

MacAddressTrap() {
    switch=$1
    ifIndex=$2
    macAddr=$3

    # insert into nodeInfo.macLocation 
    #     select 0x0000398dfad6 as macAddress, portID
    #     where switch = t1d02 and ifIndex = 10501;
    q1="REPLACE INTO nodeInfo.macLocation"
    q2="SELECT $macAddr as macAddress, portID"
    q3="FROM switchInfo.switchPorts"
    q4="WHERE switch = \"$switch\" AND ifIndex = $ifIndex"
    query="$q1 $q2 $q3 $q4"
    echo $query | \
	mysql --host=titan.cc.umanitoba.ca --database=nodeInfo \
	    --user="logins" --password="juy6fdb"
}

#
# Now dispatch to routines based on the trap type.
#
case $trapType in
    linkUp)
	LinkUpTrap ;;
    linkDown)
	LinkDownTrap $ifIndex ;;
    coldStart)
	# just switch and coldStart as arguments.
       	;;
    *mib-2.17)
	# arg 1 = t1b1
	# arg 2 = SNMPv2-SMI::mib-2.17
	# arg 3 = (2)
	# arg 5 = SNMPv2-SMI::enterprises.9.9.46.1.3.1.1.1.1.377
	# arg 7 = 377,
	# arg 8 = IF-MIB::ifName.5003
       	;;
    *mib-2.47.2)
	# arg 1 = tec1
	# arg 2 = SNMPv2-SMI::mib-2.47.2
	# arg 3 = (1)
	# arg 5 = SNMPv2-SMI::mib-2.47.1.4.1.0
	# arg 7 = 5654
	;;
    *enterprises.9)
	# Reload requested
	# arg 1 = t1d02
	# arg 2 = SNMPv2-SMI::enterprises.9
	# arg 3 = (0)
	# arg 5 = DISMAN-EVENT-MIB::sysUpTimeInstance
	# arg 7 = 319740622,
	# arg 8 = SNMPv2-SMI::enterprises.9.2.1.2.0
	# arg 10 = Reload
	;;
    *enterprises.9.9.13.3)
	# arg 1 = tec1
	# arg 2 = SNMPv2-SMI::enterprises.9.9.13.3
	# arg 3 = (7)
	# arg 5 = SNMPv2-SMI::enterprises.9.9.13.1.3.1.2.149
	# arg 7 = module
	;;
    *enterprises.9.9.43.2)
	# configuration changed trap.
	;;
    *enterprises.9.9.46.2)
	# arg 1 = t1b1
	# arg 2 = SNMPv2-SMI::enterprises.9.9.46.2
	# arg 3 = (7)
	# arg 5 = SNMPv2-SMI::enterprises.9.9.46.1.6.1.1.14.5003
	# arg 7 = 1 | 2
	;;
    *enterprises.9.9.117.2)
	# arg 1 = tec1
	# arg 2 = SNMPv2-SMI::enterprises.9.9.117.2
	# arg 3 = (3)
	# arg 5 = SNMPv2-SMI::mib-2.47.1.1.1.1.4.2030
	# arg 7 = 2029,
	# arg 8 = SNMPv2-SMI::mib-2.47.1.1.1.1.2.2030
	;;
    *enterprises.9.9.161.1.3.1.1.4.2)
	# csm generated health notification.
	;;
    # *enterprises.9.9.215.2)
	# unknown
	;;
    *enterprises.9.9.315.0)
	# mac address on interface.
	ifIndex=`echo $7 | tr -d ','`
	macOid=`echo $11 | cut -d: -f3`
	rawMacAddr="`snmpget -c uofmbb -v 2c -Oqs $switch $macOid`"
	macAddr=`echo 0x $rawMacAddr | tr -d ' :.'`
	MacAddressTrap $switch $ifIndex $macAddr
	;;
    *)		
        (
	    echo unknown trap type
	    i=1
	    for arg in $*; do
		echo arg $i = $arg
		i=`expr $i + 1`
	    done 
	) > /tmp/trap-handler.$$.txt
	;;
esac

