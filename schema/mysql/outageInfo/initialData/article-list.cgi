#!/bin/sh
#
# Writted by David Milton, ACN.
#
# This cgi accepts an optional two part query which indicates the
# maximum number of articles to display and the number of days history
# which is to be shown. The two parts must be seperated by a period.
#
# Articles themselves must have file names which are a date of
# the form YYYY-MM-DD.HH-MM. Where YYYY is a four digit year, MM is
# a two digit month number, DD is a two digit day number, HH is the
# hour start time of the outage using the 24 hour clock, and MM is
# the minutes of the start time. The article files themselves are free
# format but should be short since the contents of the entire
# file will be inserted into the table generated here.
#
# There are a number of default behaviors...
# 1) The path to the requested document is where the article files
#    are expected to be (minus '/history' if that is part of the path).
# 2) If the path to the requested document contains 'history'
#    then all files are listed regardless of dates.
# 3) If the path to the requested document does not contain 'history'
#    then only files whose dates are in the future (or n days old if
#    specified by the query) will be listed and an history link will
#    be provided.
# 4) Files are listed, most recent first.
#
# Change history:
#
# 11 Sep 2001   DM	Fixed a bug where the history shows the no system
#			outages message at the very end.
# 11 Sep 2001   DM      Change the HTML generated code so that the date
#			is all on one line and those articles which have
#			todays date have the date and time in bold type.
# 06 Sep 2001   DM      Modify to allow multiple outages on the same day
#			by forcing a time into the file name.
# 26 Jun 2001   DM	Modify to accept a query giving the number of
#			days history to list in addition to future events.
# 30 May 2001	DM	Initial modification from remote access news
#			listing to have date specific listings.

# Temporary file for file listing.
TMP=/tmp/article-list.$$

# Uncomment the next three lines only for testing:
#DOCUMENT_ROOT="/www/data"
#DOCUMENT_URI="/campus/acn/outages/index.html"
#QUERY_STRING="30.3"

#
# Take a date of the form yyyy-mm-dd and produce
# the Julian Day from the mm-dd with the output format of yyyy-jday
#
jday() {
    Y=$1 ; M=$2; D=$3 ;
    # Number of days up to the end of the previous month.
    case $M in
	01) JDAY=0  ;;
	02) JDAY=31 ;;
	03) JDAY=59 ;;
	04) JDAY=90 ;;
	05) JDAY=120 ;;
	06) JDAY=151 ;;
	07) JDAY=181 ;;
	08) JDAY=212 ;;
	09) JDAY=243 ;;
	10) JDAY=273 ;;
	11) JDAY=304 ;;
	12) JDAY=334 ;;
    esac
    # add in the number of days this month.
    JDAY=`expr $JDAY + $D`
    # correct for leap years.
    if [ $M -gt 2 ]; then
	LY1=`expr \( $Y % 4 \) = 0`
	LY2=`expr \( $Y % 100 \) \> 0`
	LY3=`expr \( $Y % 400 \) = 0`
	if [ \( $LY1 -eq 1 -a $LY2 -eq 1 \) -o \( $LY3 -eq 1 \) ]; then
            JDAY=`expr $JDAY + 1`
        fi
    fi
}

# Figure out the Julian day for today's date.
set   `/bin/date "+%Y %m %d"`
YEAR=$1
MONTH=$2
DAY=$3
jday $YEAR $MONTH $DAY
TODAY=$JDAY

# Figure out where we get our file listings from...
echo $DOCUMENT_URI | /bin/grep history > /dev/null 2>&1
STAT=$?

URL_PATH=`dirname "$DOCUMENT_URI"`
if [ $STAT -eq 1 ]; then
    # grep for 'history' failed, a history link is required.
    ARCHIVE=1
else
    # we found history in the path so we don't display the link.
    ARCHIVE=0
    # Chop off the 'history' part of the path.
    URL_PATH=`dirname "$URL_PATH"`
fi
# More debug code:
#echo grep result: $STAT    ARCHIVE=$ARCHIVE

# Figure out about our query if one exists.
if [ "X" = "X$QUERY_STRING" ]; then
    HIST_DAYS=0
else
    OLDIFS=$IFS
    IFS=.
    set  $QUERY_STRING
    HIST_DAYS=$1
    COUNT=$2
    IFS=$OLDIFS
fi

# default the count to everything if it's not already set.
if [ "X$COUNT" = "X" ]; then
    COUNT=9999
fi

# build a list of all file names which look like dates with a time.
PROFILE='[12][0-9][0-9][0-9]-[012][0-9]-[0-3][0-9]\.[012][0-9]-[0-5][0-9]'

#
# build a list of candidate files to put into our web page.
#
/bin/ls -1 -r "$DOCUMENT_ROOT$URL_PATH" | \
    /bin/egrep "$PROFILE\$" | \
    /bin/tr '.-' '  ' > $TMP

# More debug code
#echo Files to examine/display.
#cat $TMP
#echo End of file listing.
#echo today=$TODAY yr=$YEAR max=$COUNT range=$HIST_DAYS
#echo base=$DOCUMENT_ROOT path=$URL_PATH shLink=$ARCHIVE

#
# Generate a web page!
#
/bin/cat $TMP | /bin/nawk '
    function julianDay(y, m, d) {
	# calculate julian day for y, m, d.
	# start with the basic calculation.
	julian = jday[mth] + day;
	# correct for leap years.
	if ( mth > 2 ) {
	    ly1 = ( year % 4 ) == 0;
	    ly2 = ( year % 100 ) > 0;
	    ly3 = ( year % 400 ) == 0;
	    if ( ( ly1 == 1 && ly2 == 1 ) || ( ly3 == 1 ) ) {
		julian = julian + 1;
	    }
	}
	return(julian);
    }
    function tableEntry(now, articleDate) {
	printf("  <tr>\n    <td valign=middle>\n");
	printf("      <font size=2><center>\n");
	if ( now == articleDate ) {
	    printf("        <strong>%s %d %d<br>%02d:%02d</strong>\n",
		month[mth], day, year, hour, minute);
	} else {
	    printf("        %s %d %d<br>%02d:%02d\n",
		month[mth], day, year, hour, minute);
	}
        printf("      </center></font>\n");
	printf("    </td>\n");
	printf("    <td>\n");
	cmd="cat " base path "/" year "-" mth "-" day "." hour "-" minute;
	system(cmd);
	printf("    </td>\n");
	#printf("<td>startDate: %d<br>dateNbr %d</tr>\n",
	#    startDate, dateNbr);
	printf("  </tr>\n");
    }
    BEGIN {
	# prepare by setting up variables for execution.
	# count how many articles we are listing.
	count = 0;
	# What is the earliest day we want to list articles for:
	startDate = 0; # list everything.
	# The maximum number of articles to list (0 means all)
	if ( max == 0 ) {
	    max = 9999;
	}
	# A table of month names for nicer date display.
	month["01"] = "Jan";  month["02"] = "Feb";  month["03"] = "Mar";
	month["04"] = "Apr";  month["05"] = "May";  month["06"] = "Jun";
	month["07"] = "Jul";  month["08"] = "Aug";  month["09"] = "Sep";
	month["10"] = "Oct";  month["11"] = "Nov";  month["12"] = "Dec";
	# A table of days gone by up to the end of the previous month.
	jday["01"] = 0;   jday["02"] = 31;  jday["03"] = 59;
        jday["04"] = 90;  jday["05"] = 120; jday["06"] = 151;
        jday["07"] = 181; jday["08"] = 212; jday["09"] = 243;
        jday["10"] = 273; jday["11"] = 304; jday["12"] = 334;
	printf("Content-type: text/html\n\n");
	printf("<table border=1 cellpadding=6>\n");
    }
    shLink == 0 {
	year = $1; mth = $2; day = $3; hour = $4; minute = $5;
	# no history link, show absolutely everything.
	tableEntry(0, 1);
	count += 1;
    }
    count < max {
	if ( startDate == 0 ) {
	    if ( range > 0 ) {
		# Determine what the earliest date we should list articles for.
		startDate = yr * 1000 + today - range;
		if ( today < range ) {
		    startDate = startDate - 635;
		}
		#printf("startDate: %d\nyr: %d\ntoday: %d\nrange: %d\n",
		#    startDate, yr, today, range);
	    }
	}
	year = $1; mth = $2; day = $3; hour = $4; minute = $5;
	# create a composite number including the year and julian day.
	dateNbr = year * 1000 + julianDay(year, mth, day);
	# is that date in the appropriate range?
	if ( dateNbr >= startDate ) {
	    tableEntry(startDate + range, dateNbr);
	    count += 1;
	}
    }
    END {
	if ( count == 0 ) {
	    printf("  <tr><td colspan=2 valign=top>\n    ");
	    printf("There are no system outages scheduled at this time.\n");
	    printf("  </td></tr>\n");
	}
	if ( shLink != 0 ) {
	    printf("  <tr>\n    <td colspan=2 valign=top>\n");
	    printf("      <a href=\"http://www.umanitoba.ca%s/history\">",
		path);
	    printf("History</a>\n");
	    printf("    </td>\n  </tr>\n");
	}
	printf("</table>\n");
    }
' today=$TODAY yr=$YEAR max=$COUNT range=$HIST_DAYS \
  base=$DOCUMENT_ROOT path=$URL_PATH shLink=$ARCHIVE

# Clean up our temporary file.
rm -f $TMP
