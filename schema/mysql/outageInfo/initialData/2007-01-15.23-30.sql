insert into outageInfo.scheduledOutages set
service = "Isbister network",
startTime = "2007-01-15 23:30",
endTime = "2007-01-16 00:00",
description = "
<p>Network Services and VoIP Telephone Services will be unavailable during 
the Isbister Bldg service interruption. 
<p>If you have a VoIP phone: If your phone is not working after a network 
interruption such as this one, reset the phone by unplugging/plugging in 
at the data port.

</ul><p> 
<p>Reason: Voice/Data Room Transfer from 110V to 208V Power 

<p>If you have any concerns, contact the ACN Network Trouble Desk at 
474-8484 
";
