insert into outageInfo.scheduledOutages set
service = "Isbister network",
startTime = "2007-01-24 17:00",
endTime = "2007-01-24 21:30",
description = "
<p>Network Services and VoIP Telephone Services will be unavailable during 
the Isbister Bldg service interruption. 
<p>If you have a VoIP phone: If your phone is not working after a network 
interruption such as this one, reset the phone by unplugging/plugging in 
at the data port.

</ul><p> 
<p>Reason: Network Upgrade: Installation of new desktop and building 
switches.

<p>If you have any concerns, contact the ACN Network Trouble Desk at 
474-8484 
";
