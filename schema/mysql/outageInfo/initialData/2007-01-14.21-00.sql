insert into outageInfo.scheduledOutages set
service = "UNIX System",
startTime = "2007-01-14.21-00",
endTime = "2007-01-14.21-00",
description = "
<p>UNIX System will be unavailable while the UNIX Central Fileserver is upgraded

<p>If you have any concerns, contact the UNIX Data Management Team @ dm-team@cc.umanitoba.ca
";
