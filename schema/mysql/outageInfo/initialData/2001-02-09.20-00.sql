insert into outageInfo.scheduledOutages set
service = "Network in various buildings, see detail",
startTime = '2001-02-09 20:00:00',
endTime = '2001-02-10 00:00:00',
description = "
Network services in Continuing Education Complex, Frank Kennedy,
and Max Bell will be unavailable while the network is upgraded.
";
