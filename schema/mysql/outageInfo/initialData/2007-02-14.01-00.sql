insert into outageInfo.scheduledOutages set
service = "MTS-MRNet service interruption",
startTime = "2007-02-14 01:00",
endTime = "2007-02-14 05:00",
description = "
<p>MRNet will not be available a 15 minute period during this scheduled outage. 
This outage has been scheduled by MTS and approved by ACN.
MTS Change Ticket # 195.
</ul><p> 
<p>Reason: Upgrading software on core MTS network devices.

<p>If you have any concerns, contact the ACN Network Trouble Desk at 
474-8484
";
