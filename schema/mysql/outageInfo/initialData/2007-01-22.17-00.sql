insert into outageInfo.scheduledOutages set
service = "Fitzgerald network",
startTime = "2007-01-22 17:00",
endTime = "2007-01-22 17:30",
description = "
<p>Network Services will be unavailable during the Fitzgerald Bldg service 
interruption. 
<p>If you have a VoIP phone: If your phone is not working after a network 
interruption such as this one, reset the phone by unplugging/plugging in 
at the data port.

</ul><p> 
<p>Reason: Power Upgrade: The Voice/Data room is being upgraded to 208v 
power.

<p>If you have any concerns, contact the ACN Network Trouble Desk at 
474-8484 
";
