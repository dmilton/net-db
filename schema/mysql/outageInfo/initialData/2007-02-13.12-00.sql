insert into outageInfo.scheduledOutages set
service = "Campus Network service interruption",
startTime = "2007-02-14 00:00",
endTime = "2007-02-14 01:00",
description = "
<p>Network Services and VoIP Telephone Services will be unavailable during 
the Network service interruption. 
<p>This will affect the U of M as well as TRlabs, ITC/VRC and
Incubat.(Smartpark)
<p>If you have a VoIP phone: If your phone is not working after a network 
interruption such as this one, reset the phone by unplugging/plugging in 
at the data port.

</ul><p> 
<p>Reason: Upgrading GigEAccess: Updrading Network Bandwidth

<p>If you have any concerns, contact the ACN Network Trouble Desk at 
474-8484 
";
