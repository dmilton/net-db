insert into outageInfo.scheduledOutagges set
service = "St. Andrews College network",
startTime = "2007-02-09 16:30",
endTime = "2007-02-10 12:00",
description = "
<p>Network Services and VoIP Telephone Services will be unavailable during 
the Network service interruption. 
<p>If you have a VoIP phone: If your phone is not working after a network 
interruption such as this one, reset the phone by unplugging/plugging in 
at the data port.

</ul><p> 
<p>Reason: Network Modification: Fibre Optic Upgrade and Splicing.

<p>If you have any concerns, contact the ACN Network Trouble Desk at 
474-8484 
";
