insert into outageInfo.scheduledOutages set
service = "ACN UNIX servers",
startTime = "2001-06-15 00:00:00",
endTime = "2001-06-15 02:00:00",
description="
The ACN UNIX system will have sporadic server outages of roughly 10 minutes
for each server as server connections are moved from old network switches
to new switches.
<br>
If you have further questions or concerns about these outages, please contact
either David Milton or Doug Dennis.
";
