insert into outageInfo.scheduledOutages set
service = "Network in various buildings, see detail",
startTime = "2001-06-14 22:00:00",
endTime = "2001-06-14 22:00:00",
description="
During the outage, th following buildings will not have connectivity to the
campus data network; Machray Hall, Armes, Duff Roblin, Robson Hall,
Chancellors Lodge, and Human Ecology. The outage is to permit replacement of
faulty network switch hardware.
<br>
If you have further questions or concerns about these outages, please contact
either David Milton or Doug Dennis.
";
