insert into outageInfo.scheduledOutages set
service = "ACN UNIX Servers",
startTime = "2001-06-03 08:00:00",
endTime = "2001-06-03 11:00:00",
description = "The ACN UNIX system will have sporadic server outages 
of roughly 1 minutes for each server as server connections are upgraded
from old network switches to new switches. If you have further questions
or concerns about this outage, please contact either David Milton or
Doug Dennis.";
