CREATE TABLE outageInfo.unscheduledOutages (
    id		int(10) NOT NULL auto_increment,
    bldgCode	char(2) collate ascii_bin NOT NULL,
    device	varchar(32) collate ascii_bin NOT NULL,
    startTime	datetime NOT NULL,
    endTime	datetime default NULL,
    PRIMARY KEY (id),
    INDEX	startTime (startTime),
    INDEX	endTime (endTime)
)
    ENGINE=MyISAM
    DEFAULT CHARSET=ascii
    COLLATE=ascii_bin
    COMMENT='listing of unscheduled system outages';

