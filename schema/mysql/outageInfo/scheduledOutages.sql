DROP TABLE outageInfo.scheduledOutages;

CREATE TABLE outageInfo.scheduledOutages (
    id		int(10) unsigned NOT NULL auto_increment,
    service	varchar(80) collate ascii_bin default null,
    startTime	datetime NOT NULL,
    endTime	datetime NOT NULL,
    description	text collate ascii_bin default null,
    PRIMARY KEY	(id),
    KEY		startTime (startTime)
)   
    ENGINE=MyISAM
    DEFAULT CHARSET=ascii
    COLLATE=ascii_bin
    COMMENT='scheduled system outages';

