CREATE TABLE nodeInfo.ipLocation (
    arpID INTEGER( 10 ) NOT NULL UNSIGNED default 0,
    portID int( 10 ) NOT NULL UNSIGNED default 0,
    PRIMARY KEY ( arpID, portID ),
 ) ENGINE = MyISAM
    DEFAULT CHARSET = ascii
    COLLATE = ascii_bin
    COMMENT = 'table mapping IP, MAC, and switch ports together.';

CREATE VIEW nodeInfo.nodeInfo AS
    select
	INET_NTOA( nodeInfo.ipLocation.ipAddress ) AS ipAddress ,
       	loginInfo.activeIP.lastSeen AS ipLastSeen ,
       	formatMac( nodeInfo.ipLocation.macAddress ) AS macAdress ,
       	loginInfo.activeMAC.lastSeen AS macLastSeen ,
       	switchInfo.switches.name AS switch ,
       	switchInfo.ports.ifIndex AS ifIndex ,
       	switchInfo.ports.ifName AS ifName ,
       	switchInfo.ports.ifAlias AS ifAlias ,
       	switchInfo.ports.ifDescr AS ifDescr ,
       	switchInfo.switches.id AS switchID ,
       	switchInfo.ports.id AS portID 
    from
	nodeInfo.ipLocation,
	loginInfo.activeIP,
	loginInfo.activeMAC,
	switchInfo.switches,
       	switchInfo.ports
    where
	switchInfo.switches.id = switchInfo.ports.switchID and 
	nodeInfo.ipLocation.portID = switchInfo.ports.id and
       	nodeInfo.ipLocation.ipAddress = loginInfo.activeIP.ipAddr and 
	nodeInfo.ipLocation.macAddress = loginInfo.activeMAC.macAddr;

CREATE VIEW nodeInfo.macLocations AS
    select
	hex( nodeInfo.ipLocation.macAddress ) AS macAddress,
       	loginInfo.activeMAC.lastSeen AS macLastSeen,
	switchInfo.switches.name AS switch,
	switchInfo.ports.ifIndex AS ifIndex ,
	switchInfo.ports.ifName AS ifName ,
       	switchInfo.ports.ifAlias AS ifAlias ,
       	switchInfo.ports.ifDescr AS ifDescr ,
       	switchInfo.switches.id AS switchID ,
       	switchInfo.ports.id AS portID
    from 
	nodeInfo.ipLocation,
	loginInfo.activeMAC,
	switchInfo.switches, 
	switchInfo.ports 
    where 
	switchInfo.switches.id = switchInfo.ports.switchID and 
	nodeInfo.ipLocation.portID = switchInfo.ports.id and 
        ( nodeInfo.ipLocation.ipAddress = 0x0 or 
          nodeInfo.ipLocation.ipAddress = NULL ) and
	nodeInfo.ipLocation.macAddress = loginInfo.activeMAC.macAddr;

