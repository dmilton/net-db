DROP TABLE nodeInfo.ipStateChange;
CREATE TABLE nodeInfo.ipStateChange (
      who 	VARCHAR(8) COLLATE ASCII_BIN NOT NULL,
      ip 	INT(10) NOT NULL,
      ifId 	INT(10) NOT NULL,
      changeTime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
      newState 	ENUM('up','down','blackHole') COLLATE ASCII_BIN NOT NULL,
      reason 	VARCHAR(512) COLLATE ASCII_BIN DEFAULT NULL,
      PRIMARY KEY  (ip,changeTime)
) ENGINE=MyISAM DEFAULT CHARSET=ascii COLLATE=ascii_bin;

