# Table of ARP entries.
CREATE TABLE nodeInfo.activeARP (
    id		INT(10) UNSIGNED NOT NULL auto_increment,
    ipAddress	INT(10) UNSIGNED NOT NULL DEFAULT '0',
    macAddress	BINARY(6) NOT NULL,
    lastSeen	timestamp NOT NULL
		default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    PRIMARY KEY	(id),
    KEY arpEntry	(ipAddress,macAddress),
    KEY macAddress	(macAddress)
) ENGINE=MyISAM DEFAULT CHARSET=ascii COLLATE=ascii_bin
    COMMENT='active ARP entries';

