DROP FUNCTION nodeInfo.formatMac;
delimiter //
CREATE FUNCTION nodeInfo.formatMac(macNbr BINARY(6))
    RETURNS CHAR(15)
    DETERMINISTIC LANGUAGE SQL READS SQL DATA

    formatMac: BEGIN
	DECLARE hexMac CHAR(12);
	DECLARE result CHAR(15);

	SET hexMac = HEX(macNbr);
	RETURN CONCAT(SUBSTR(hexMac,1,4), ".", 
	    SUBSTR(hexMac,5,4), ".", SUBSTR(hexMac,9,4));
    END formatMac
//
delimiter ;
