CREATE TABLE nodeInfo.activeMAC (
    macAddress	binary(6) NOT NULL default '0x0\0\0\0',
    type	enum('wired','wireless') collate ascii_bin default 'wired',
    clientID	varchar(32) collate ascii_bin default NULL,
    lastSeen	timestamp NOT NULL default
		CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    PRIMARY KEY  (macAddress)
) ENGINE=MyISAM DEFAULT CHARSET=ascii COLLATE=ascii_bin
    COMMENT='active MAC addresses';

