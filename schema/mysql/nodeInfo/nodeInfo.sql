# Create a view which takes uses the activeARP table as
# the basis and merge in all the other tables referenced.
# This produces a comprehensive view of any given IP/MAC
# address pair and the switch port associated with it.
CREATE OR REPLACE VIEW nodeInfo.nodeInfo AS
    SELECT
	inet_ntoa(nodeInfo.activeARP.ipAddress) AS ipAddress,
       	nodeInfo.formatMac(nodeInfo.activeARP.macAddress) AS macAddress,
	nodeInfo.activeARP.lastSeen as arpLastSeen,
       	switchInfo.switches.name AS switch,
       	switchInfo.ports.ifIndex AS ifIndex,
       	switchInfo.ports.ifName AS ifName,
       	switchInfo.ports.ifAlias AS ifAlias,
       	switchInfo.ports.lastLinkUp AS lastLinkUp,
       	switchInfo.ports.lastLinkDown AS lastLinkDown,
       	switchInfo.ports.linkFlaps AS linkFlaps,
       	switchInfo.ports.ifAdminStatus AS ifAdminStatus,
       	switchInfo.ports.ifOperStatus AS ifOperStatus,
       	switchInfo.switches.id AS switchID,
       	switchInfo.ports.id AS portID,
	nodeInfo.activeARP.id as arpID
    FROM
	switchInfo.switches,
       	switchInfo.ports,
	nodeInfo.ipLocation,
	nodeInfo.activeARP
    WHERE
	switchInfo.switches.id = switchInfo.ports.switchID AND 
	nodeInfo.ipLocation.portID = switchInfo.ports.id AND
       	nodeInfo.ipLocation.arpID = nodeInfo.activeARP.id;

