CREATE TABLE nodeInfo.activeIP (
    ipAddress	binary(4) NOT NULL, 
    lastSeen	timestamp NOT NULL default 
		CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
    PRIMARY KEY  (ipAddress)
) ENGINE=MyISAM DEFAULT CHARSET=ascii COLLATE=ascii_bin
    COMMENT='active IP addresses';

