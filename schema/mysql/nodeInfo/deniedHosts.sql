# Create a table of hosts which are black holed on campus...
CREATE TABLE nodeInfo.deniedHosts (
    ipAddress	INT( 10 ) NOT NULL ,
    firstSeen	DATETIME NOT NULL ,
    lastSeen	TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    timesSeen	INT NOT NULL DEFAULT '1',
    PRIMARY KEY	( ipAddress ) ,
    INDEX	( firstSeen , lastSeen )
) ENGINE = MYISAM CHARACTER SET ascii COLLATE ascii_bin;

