CREATE TABLE nodeInfo.netlogin (
    id 			nt( 10 ) unsigned NOT NULL AUTO_INCREMENT,
    loginTime 		atetime NOT NULL default '0000-00-00 00:00:00',
    host 		har( 15 ) COLLATE ascii_bin default NULL,
    port 		nt( 11 ) default NULL,
    user		char( 8 ) COLLATE ascii_bin default NULL,
    macAddress		binary( 6 ) NOT NULL default '0x0\0\0\0',
    PRIMARY KEY		( id ),
    UNIQUE KEY login	( loginTime, host, user ),
    KEY user ( user, macAddress )
) ENGINE = MYISAM DEFAULT CHARSET = ascii COLLATE = ascii_bin;

