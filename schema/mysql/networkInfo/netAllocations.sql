CREATE TABLE networkInfo.netAllocations (
    bldgCode	CHAR(2) collate ascii_bin NOT NULL,
    vlanID	INT(10) UNSIGNED NOT NULL default '0',
    network	INT(10) UNSIGNED NOT NULL default '0',
    PRIMARY KEY ( bldgCode, vlanID ),
    KEY		vlanID (vlanID),
    KEY		network (network)
)   TYPE = MYISAM
    DEFAULT CHARSET=ascii COLLATE=ascii_bin
    COMMENT = 'Table mapping building codes, vlans, and networks.';

