#
# Table of IP networks on campus.
#
CREATE TABLE ipv4networks (
    network	INT( 10 ) UNSIGNED NOT NULL default '0',
    cidr	TINYINT(3) UNSIGNED NOT NULL ,
    searchable	ENUM( 'F', 'T' ) collate ascii_bin NOT NULL default 'F',
    PRIMARY KEY ( network )
) TYPE = MYISAM DEFAULT CHARSET=ascii COLLATE=ascii_bin;

