#
# Table of community strings...
#
create table community (
	id		int auto_increment not null,
	class		enum('building','desktop','wireless'),
	type		enum('ro','rw') not null,
	string		char(15) binary not null,
	primary key	( id )
) TYPE = MYISAM;

insert into community (class, type, string) values
    ( 'building', 'ro', 'uofmbb'),
    ( 'building', 'rw', 'v1gp4s97'),
    ( 'desktop', 'ro', 'uofmbb'),
    ( 'desktop', 'rw', 'v1gp4s97'),
    ( 'wireless', 'ro', 'uofmbb'),
    ( 'wireless', 'rw', 'fu42oqeg'),
    ( 'findip', 'ro', 'UMfindIP'),
    ( 'CompSci', 'ro', 'csnetwork'),
    ( 'Engineering', 'ro', 'eenetwork')
    ;

