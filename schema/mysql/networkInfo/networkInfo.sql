CREATE OR REPLACE VIEW networkInfo.networkInfo AS 
    SELECT
       	netAllocations.bldgCode AS bldgCode,
	vlans.name AS name,
	vlans.number AS number,
	inet_ntoa(ipv4networks.network) AS network,
	ipv4networks.cidr AS cidr,
	inet_ntoa(netmasks.mask) AS mask,
	netmasks.size AS size,
	vlans.active AS active,
	ipv4networks.searchable AS searchable,
	netAllocations.vlanID as vlanID,
	ipv4networks.network AS netID,
	vlans.description as description
    FROM
	networkInfo.netAllocations,
	networkInfo.vlans,
	networkInfo.ipv4networks,
	networkInfo.netmasks
    WHERE 
	netAllocations.vlanID = vlans.id AND
	netAllocations.network = ipv4networks.network AND
        netmasks.cidr = ipv4networks.cidr;

