#
# Table of virtual lan networks, assigned ID numbers, assigned
# IP networks on campus.
#
CREATE TABLE vlans (
    id		INT UNSIGNED NOT NULL AUTO_INCREMENT ,
    name	VARCHAR( 32 ) BINARY NOT NULL ,
    number	INT UNSIGNED DEFAULT '0' NOT NULL ,
    description	VARCHAR( 64 ) BINARY,
    PRIMARY KEY	( id ),
    INDEX	( name ),
    INDEX       ( number )
) TYPE = MYISAM ;

