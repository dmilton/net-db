
INSERT INTO fieldInfo ( tableName, fieldName, longName, description )
    VALUES (
	'networkInfo.netmasks', 'cidr',
	    '',
	'networkInfo.netmasks', 'mask',
	    '',
	'networkInfo.netmasks', 'size',
	    '',
	'networkInfo.vlans', 'id',
	    '',
	'networkInfo.vlans', 'name',
	    '',
	'networkInfo.vlans', 'number',
	    '',
	'networkInfo.vlans', 'active',
	    '',
	'networkInfo.vlans', 'description',
	    '',
	'networkInfo.ipv4networks', 'network',
	    '',
	'networkInfo.ipv4networks', 'cidr',
	    '',
	'networkInfo.ipv4networks', 'searchable',
	    ''
    );


