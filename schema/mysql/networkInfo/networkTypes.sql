#
# Table of reserved networks, standard in all buildings.
#
CREATE TABLE networkInfo.networkTypes (
    vlanNbr	int(10) unsigned NOT NULL,
    freeCode	char(2) NOT NULL,
    required	enum('T','F') NOT NULL default 'T',
    assignType	enum('none', 'next','next2','nextGrow','best fit','private')
		NOT  NULL default 'none',
    description	varchar(32) NOT NULL default "",
    PRIMARY KEY	(vlanNbr,freeCode)
)   ENGINE=MyISAM
    DEFAULT CHARSET=ascii COLLATE=ascii_bin 

insert into networkInfo.networkTypes values
    (2,		"",	"T",	"private",	"network management"),
    (256,	"",	"T",	"private",	"Sunray terminals"),
    (258,	"",	"T",	"private",	"card access, keyboxes, etc."),
    (260,	"",	"T",	"private",	"VoIP"),
    (270,	"z6",	"F",	"next",		"Network Login"),
    (272,	"z2",	"F",	"next",		"classrooms, podiums"),
    (274,	"z3",	"F",	"best fit",	"Open Area PC/Mac"),
    (276,	"z5",	"F",	"best fit",	"Open Area Unix"),
    (280,	"z7",	"T",	"next",		"Protected"),
    (290,	"wi",	"F",	"private",	"console/digi"),
    (1000,	"z1",	"T",	"best fit",	"User Workstations"),
    (1001,	"z0",	"T",	"next2",	"Building Up Link")
;

