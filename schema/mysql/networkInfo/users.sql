#
# networkInfo database users:
#

#    
# User network:
#    Can access the system from localhost, titan, and oberon.
#    Can show databases globally.
#    Can select, insert, update, or delete for the networkInfo database.

GRANT USAGE ON *.*
    TO 'network'@'localhost'
    IDENTIFIED BY 'cft6yhn'
    WITH MAX_QUERIES_PER_HOUR 0
	MAX_CONNECTIONS_PER_HOUR 0
	MAX_UPDATES_PER_HOUR 0;

GRANT SELECT, INSERT, UPDATE, DELETE,
    ON `networkInfo`.*
    TO 'network'@'localhost';

GRANT USAGE ON *.*
    TO 'network'@'titan.cc.umanitoba.ca'
    IDENTIFIED BY 'cft6yhn'
    WITH MAX_QUERIES_PER_HOUR 0
	MAX_CONNECTIONS_PER_HOUR 0
	MAX_UPDATES_PER_HOUR 0;

GRANT SELECT, INSERT, UPDATE, DELETE
    ON networkInfo.*
    TO network@titan.cc.umanitoba.ca;

GRANT USAGE ON *.*
    TO 'network'@'oberon.cc.umanitoba.ca'
    IDENTIFIED BY 'cft6yhn'
    WITH MAX_QUERIES_PER_HOUR 0
	MAX_CONNECTIONS_PER_HOUR 0
	MAX_UPDATES_PER_HOUR 0;

GRANT SELECT, INSERT, UPDATE, DELETE
    ON networkInfo.*
    TO network@oberon.cc.umanitoba.ca;

