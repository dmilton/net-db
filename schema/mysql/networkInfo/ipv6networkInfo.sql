CREATE OR REPLACE VIEW networkInfo.IPv6networkInfo AS 
    SELECT
       	bldgVlan.bldgCode,
	vlans.name,
	vlans.number,
	ipv6networks.network,
	ipv6networks.netNumber,
	ipv6networks.cidr,
	netmasks.mask,
	ipv6networks.active,
	ipv6networks.searchable,
	bldgVlan.ipv6netID,
	bldgVlan.vlanID,
	vlans.description
    FROM networkInfo.bldgVlan,
	networkInfo.vlans,
	networkInfo.ipv6networks,
	networkInfo.netmasks
    WHERE 
	bldgVlan.vlanID = vlans.id AND
	bldgVlan.ipv6netID = ipv6networks.id AND
        netmasks.cidr = ipv6networks.cidr;

