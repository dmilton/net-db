#
# Table of network masks. Allows easy translation from cidr format
# to interface configuration format.
#
create table networkInfo.netmasks (
    cidr TINYINT(3) UNSIGNED NOT NULL,
    mask INT(10) UNSIGNED NOT NULL,
    mask2 INT(10) UNSIGNED NOT NULL,
    size INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY ( `cidr` )
) TYPE = MYISAM ;

insert into netmasks values
    (16,inet_aton("255.255.0.0"), inet_aton("0.0.255.255"), 65536),
    (17,"inet_aton(255.255.128.0"), inet_aton("0.0.127.255"), 32768),
    (18,inet_aton("255.255.192.0"), inet_aton("0.0.63.255"), 16384),
    (19,inet_aton("255.255.224.0"), inet_aton("0.0.31.255"), 8192),
    (20,inet_aton("255.255.240.0"), inet_aton("0.0.15.255"), 4096),
    (21,inet_aton("255.255.248.0"), inet_aton("0.0.7.255"), 2048),
    (22,inet_aton("255.255.252.0"), inet_aton("0.0.3.255"), 1024),
    (23,inet_aton("255.255.254.0"), inet_aton("0.0.1.255"), 512),
    (24,inet_aton("255.255.255.0"), inet_aton("0.0.0.255"), 256),
    (25,inet_aton("255.255.255.128"), inet_aton("0.0.0.127"), 128),
    (26,inet_aton("255.255.255.192"), inet_aton("0.0.0.63"), 64),
    (27,inet_aton("255.255.255.224"), inet_aton("0.0.0.31"), 32),
    (28,inet_aton("255.255.255.240"), inet_aton("0.0.0.15"), 16),
    (29,inet_aton("255.255.255.248"), inet_aton("0.0.0.7"), 8),
    (30,inet_aton("255.255.255.252"), inet_aton("0.0.0.3"), 4)
;

