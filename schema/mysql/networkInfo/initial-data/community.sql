drop table community;
#
# Table of community strings...
#
create table community (
	id		int auto_increment not null,
	class		enum('building','desktop','wireless', 'external'),
	admin		enum('acn', 'bc', 'cs', 'ee', 'ar', 'ph'),
	type		enum('ro','rw') not null,
	string		char(15) binary not null,
	primary key	( id )
) TYPE = MYISAM;

insert into community (class, type, string) values
    ( 'building', 'acn', 'ro', 'uofmbb'),
    ( 'building', 'acn', 'rw', 'v1gp4s97'),
    ( 'desktop', 'acn', 'ro', 'uofmbb'),
    ( 'desktop', 'acn', 'rw', 'v1gp4s97'),
    ( 'wireless', 'acn', 'ro', 'uofmbb'),
    ( 'wireless', 'acn', 'rw', 'fu42oqeg'),
    ( 'building', 'bc', 'ro', 'bcampus'),
    ( 'desktop', 'bc', 'ro', 'bcampus'),
    ( 'wireless', 'bc', 'ro', 'bcampus'),
    ( 'wireless', 'bc', 'rw', 'fu42oqeg'),
    ( 'external', 'cs', 'ro', 'csnetwork'),
    ( 'external', 'ee', 'ro', 'eenetwork'),
    ( 'external', 'ar', 'ro', 'arnetwork'),
    ( NULL, 'ro', '');

