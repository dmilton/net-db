#
# Table of IP networks on campus.
#
CREATE TABLE networkInfo.networks (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT ,
    network CHAR( 15 ) NOT NULL ,
    cidr TINYINT UNSIGNED NOT NULL ,
    active ENUM( 'T', 'F' ) DEFAULT 'F' NOT NULL ,
    searchable ENUM( 'T', 'F' ) DEFAULT 'F' NOT NULL ,
    PRIMARY KEY ( id ) ,
    UNIQUE ( network )
) TYPE = MYISAM ;

#
# Table of virtual lan networks, assigned ID numbers, assigned
# IP networks on campus.
#
CREATE TABLE networkInfo.vlans (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT ,
    name VARCHAR( 32 ) BINARY NOT NULL ,
    number INT UNSIGNED DEFAULT '0' NOT NULL ,
    description VARCHAR( 64 ) BINARY,
    PRIMARY KEY ( name ) ,
    INDEX ( id )
) TYPE = MYISAM ;

CREATE TABLE networkInfo.bldgVlan (
    bldgCode CHAR( 2 ) BINARY NOT NULL ,
    netID INT NOT NULL ,
    vlanID INT NOT NULL ,
    PRIMARY KEY ( bldgCode, vlanID ) ,
    INDEX ( netID, vlanID )
) TYPE = MYISAM COMMENT = 'Mapping of vlans, networks, and building codes.';

CREATE VIEW networkInfo.ipv6networkInfo AS 
    SELECT
       	bldgVlan.bldgCode,
	vlans.name,
	vlans.number,
	networks.network,
	networks.netNumber,
	networks.cidr,
	netmasks.mask,
	networks.active,
	networks.searchable,
	bldgVlan.netID,
	bldgVlan.vlanID,
	vlans.description
    FROM networkInfo.bldgVlan,
	networkInfo.vlans,
	networkInfo.ipv4networks,
	networkInfo.netmasks
    where 
	bldgVlan.vlanID = vlans.id and
	bldgVlan.ipv4netID = ipv4networks.id and
	bldgVlan.ipv6netID = ipv6networks.id and
        netmasks.cidr = networks.cidr;

