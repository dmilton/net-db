#
# A table describing the different types of building network designs.
# Allows translation of a type value into a description.
#
create table bldgModels (
    type		tinyint(3) not null,
    description		varchar(32),
    primary key		( type )
) TYPE=MYISAM;

insert into bldgModels values
    (0,			"No network services"),
    (1,			"Unknown"),
    (2,			"Core"),
    (3,			"WAN Link"),
    (4,			"Wireless"),
    (5,			"Test"),
    (10,		"No building router"),
    (11,		"router stack, desktop stacks"),
    (12,		"router desktop stack"),
    (13,		"dual router, desktop switches"),
    (14,		"single router/switch"),
    (15,		"single switch"),
    (16,		"single router, multiple switches"),
    (17,		"single stack"),
    (100,		"single router, one or more coax segments"),
    (101,		"single switch, one or more coax segments"),
    (102,		"single router, desktop switches, coax segments"),
    (103,		"single building feed, no switches"),
    (104,		"Multiple Switches, coax segment(s)"),
    (666,		"Cinema Works")
;

