#
# Table of reserved networks, standard in all buildings.
#
create table networkTypes (
    vlanNbr	int unsigned not null,
    freeCode	char(2) not null,
    required	enum('T', 'F') default 'T' not null,
    assignType	enum('next', 'next2', 'nextGrow', 'best fit', 'private'),
    description	varchar(32),
    primary key	(vlanNbr, freeCode)
) TYPE=MYISAM;

insert into networkTypes values
    (1000,	"z1", "T",	"best fit",	"User Workstations"),
    (1001,	"z0", "T",	"next2",	"Building Up link"),
    (1002,	"z8", "F",	"best fit",	"Residence Users"),
    (2,		"", "T",	"private",	"network management"),
    (256,	"", "F",	"private",	"Sunray terminals"),
    (258,	"", "T",	"private",	"card access,dvr,etc."),
    (260,	"", "T",	"private",	"VoIP Phones"),
    (270,	"z6", "F",	"best fit",	"Network Login"),
    (272,	"z2", "F",	"best fit",	"classrooms, podiums"),
    (274,	"z3", "F",	"best fit",	"Open Area PC/Mac"),
    (276,	"z5", "F",	"best fit",	"Open Area Unix"),
    (280,	"z7", "T",	"next",		"Protected/Setup"),
    (290,	"wi", "F",	"private",	"console/digi")
;

