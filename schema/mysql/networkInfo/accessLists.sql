create table internet-access-list (
	id		int unsigned not null auto_increment,
	port		int unsigned,
	protocol	ENUM('IP', 'TCP', 'UDP'),
	description	varchar( 32 ),
	PRIMARY KEY ( 'id' ),
	INDEX ( 'port' )
) TYPE = MYISAM COMMENT = 'Internet blocked protocols';

insert into internet-access-list values
    (1,		0,	'IP',	"ICMP timestamp request"),
    (2,		0,	'IP',	"ICMP mask request"),
    (3,		69,	'UDP',	"Trivial File Transfer Protocol (TFTP)"),
    (4,		111,	'TCP',	"Sun Remote Procedure Call"),
    (5,		111,	'UDP',	"Sun Remote Procedure Call"),
    (6,		135,	'TCP',	"Performance Monitor (perfmon)"),
    (7,		135,	'UDP',	"Performance Monitor (perfmon)"),
    (8,		137,	'TCP',	"NETBIOS Name Service (netbiso-ns)"),
    (9,		137,	'UDP',	"NETBIOS Name Service (netbiso-ns)"),
    (10,	138,	'TCP',	"NETBIOS Datagram Service (netbios-dgm)"),
    (11,	138,	'UDP',	"NETBIOS Datagram Service (netbios-dgm)"),
    (12,	139,	'TCP',	"NETBIOS Session Service (netbios-ssn)"),
    (13,	139,	'UDP',	"NETBIOS Session Service (netbios-ssn)"),
    (14,	161,	'TCP',	"SNMP"),
    (15,	161,	'UDP',	"SNMP"),
    (16,	162,	'TCP',	"SNMP TRAP"),
    (17,	162,	'UDP',	"SNMP TRAP"),
    (18,	445,	'TCP',	"Server Message Block (SMB)"),
    (19,	445,	'UDP',	"Server Message Block (SMB)"),
    (20,	593,	'TCP',	"HTTP RPC Ep Map"),
    (21,	445,	'UDP',	"HTTP RPC Ep Map"),
    (22,	1433,	'TCP',	"Microsoft SQL Server"),
    (23,	1433,	'UDP',	"Microsoft SQL Server"),
    (24,	1434,	'TCP',	"Microsoft SQL Server"),
    (25,	1434,	'UDP',	"Microsoft SQL Server"),
    (26,	2049,	'TCP',	"Network File System (NFS)"),
    (27,	2049,	'UDP',	"Network File System (NFS)");


