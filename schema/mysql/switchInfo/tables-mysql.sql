#
# Table of different switch model types and their basic features.
#
create table switchInfo.switchTypes (
	id		int auto_increment not null,
	model		char(32),
	ru		int,
	desktop_ports	int,
	uplink_ports	int,
	stack		enum ('T', 'F') default 'T',
	router		enum ('T', 'F') default 'F',
	primary key ( id )
) TYPE=MYISAM;

INSERT INTO switchInfo.switchTypes 
    ( model , ru , desktop_ports , uplink_ports , stack, router )
    values
    ("WS-C1924C-EN",		 1, 24,  2, 'F', 'F'),
    ("WS-C2924-XL-EN",		 1, 24,  0, 'F', 'F'),
    ("WS-C2924M-XL-EN",		 2, 24,  2, 'F', 'F'),
    ("WS-C3524-XL-EN",		 1, 24,  2, 'F', 'F'),
    ("WS-C3524-PWR-XL",		 1, 24,  2, 'F', 'F'),
    ("WS-C3548-XL-EN",		 1, 48,  2, 'F', 'F'),
    ("WS-C3550-12G",		 2,  0, 10, 'F', 'T'),
    ("WS-C3550-24-SMI",		 1, 24,  2, 'F', 'F'),
    ("WS-C3550-24-EMI",		 1, 24,  2, 'F', 'T'),
    ("WS-C3750G-12S-E",		 1,  0, 12, 'T', 'T'),
    ("WS-C3750G-24T-S",		 1, 24,  0, 'T', 'T'),
    ("WS-C3750-24TS-S",		 1, 24,  2, 'T', 'F'),
    ("WS-C3750-24TS-E",		 1, 24,  2, 'T', 'T'),
    ("WS-C3750-24PS-S",		 1, 24,  2, 'T', 'F'),
    ("WS-C3750-24PS-E",		 1, 24,  2, 'T', 'T'),
    ("WS-C3550-24-FX-SMI",	 1, 24,  2, 'F', 'F'),
    ("Summit 48si",		 1, 48,  2, 'F', 'F')
;

CREATE TABLE switches (
	id		int unsigned auto_increment not null,
	name		VARCHAR( 8 ) BINARY NOT NULL ,
	model_id	TINYINT UNSIGNED DEFAULT '0' NOT NULL ,
	stack_size	TINYINT UNSIGNED DEFAULT '0' NOT NULL ,
	mac_addr	CHAR( 14 ) BINARY,
	vd_room		INT UNSIGNED DEFAULT '0' NOT NULL ,
	PRIMARY KEY 	( id ) ,
	INDEX 		( name, vd_room )
) TYPE = MYISAM COMMENT = 'Table of installed desktop switches';

create table ports (
	id		int unsigned auto_increment not null,
	switchID	int unsigned not null,
	ifIndex		int unsigned not null,
	ifName		varchar(32) binary not null,
	ifAlias		varchar(64) binary not null,
	PRIMARY KEY 	( id ),
	INDEX 		( switch_id, ifAlias )
) TYPE = MYISAM COMMENT = "Table of switch ports";

CREATE TABLE machines (
	id		int unsigned not null auto_increment,
	mac-addr	char( 14 ) binary default '0000.0000.0000' not null,
	ip-addr		char( 15 ) binary default '000.000.000.000' not null,
	PRIMARY KEY ( `id` ) ,
	INDEX ( `mac-addr` , `ip-addr` )
) TYPE = MYISAM COMMENT = 'Table of machines and their port';

create table machine_ports (
	id		int unsigned not null auto_increment,
	mac_id		int unsigned not null,
	port_id		int unsigned not null,
	primary key	( id )
) TYPE = MYISAM COMMENT = "Table of machines and the associated port(s).";

create table internet-access-list (
	id		int unsigned not null auto_increment,
	port		int unsigned,
	protocol	ENUM('IP', 'TCP', 'UDP'),
	description	varchar( 32 ),
	PRIMARY KEY ( 'id' ),
	INDEX ( 'port' )
) TYPE = MYISAM COMMENT = 'Internet blocked protocols';

insert into internet-access-list values
    (1,		0,	'IP',	"ICMP timestamp request"),
    (2,		0,	'IP',	"ICMP mask request"),
    (3,		69,	'UDP',	"Trivial File Transfer Protocol (TFTP)"),
    (4,		111,	'TCP',	"Sun Remote Procedure Call"),
    (5,		111,	'UDP',	"Sun Remote Procedure Call"),
    (6,		135,	'TCP',	"Performance Monitor (perfmon)"),
    (7,		135,	'UDP',	"Performance Monitor (perfmon)"),
    (8,		137,	'TCP',	"NETBIOS Name Service (netbiso-ns)"),
    (9,		137,	'UDP',	"NETBIOS Name Service (netbiso-ns)"),
    (10,	138,	'TCP',	"NETBIOS Datagram Service (netbios-dgm)"),
    (11,	138,	'UDP',	"NETBIOS Datagram Service (netbios-dgm)"),
    (12,	139,	'TCP',	"NETBIOS Session Service (netbios-ssn)"),
    (13,	139,	'UDP',	"NETBIOS Session Service (netbios-ssn)"),
    (14,	161,	'TCP',	"SNMP"),
    (15,	161,	'UDP',	"SNMP"),
    (16,	162,	'TCP',	"SNMP TRAP"),
    (17,	162,	'UDP',	"SNMP TRAP"),
    (18,	445,	'TCP',	"Server Message Block (SMB)"),
    (19,	445,	'UDP',	"Server Message Block (SMB)"),
    (20,	593,	'TCP',	"HTTP RPC Ep Map"),
    (21,	445,	'UDP',	"HTTP RPC Ep Map"),
    (22,	1433,	'TCP',	"Microsoft SQL Server"),
    (23,	1433,	'UDP',	"Microsoft SQL Server"),
    (24,	1434,	'TCP',	"Microsoft SQL Server"),
    (25,	1434,	'UDP',	"Microsoft SQL Server"),
    (26,	2049,	'TCP',	"Network File System (NFS)"),
    (27,	2049,	'UDP',	"Network File System (NFS)");


