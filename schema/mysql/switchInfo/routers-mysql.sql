CREATE TABLE switchInfo.routers (
    id		int(10) NOT NULL auto_increment,
    bldgCode	char(2) collate ascii_bin NOT NULL,
    name	varchar(8) collate ascii_bin NOT NULL,
    PRIMARY KEY	(id),
    KEY name	(name),
    KEY	offline	(offline)
)
    ENGINE=MyISAM
    DEFAULT CHARSET=ascii
    COLLATE=ascii_bin;

