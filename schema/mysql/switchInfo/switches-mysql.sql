CREATE TABLE switchInfo.switches (
	name VARCHAR( 8 ) BINARY NOT NULL ,
	model-id TINYINT UNSIGNED DEFAULT '0' NOT NULL ,
	stack-size TINYINT UNSIGNED DEFAULT '0' NOT NULL ,
	mac-addr CHAR( 14 ) BINARY,
	vd-room INT UNSIGNED DEFAULT '0' NOT NULL ,
	PRIMARY KEY ( name ) ,
	INDEX ( vd-room )
) TYPE = MYISAM COMMENT = 'Table of installed desktop switches';

insert into switches values
    ("e3b1", 	10,	2,	"000D:BD5B:E900",	15),
    ("e3d61",	12,	3,	"000E:84A2:4380",	16),
    ("e3d62",	12,	2,	"000E:84AF:FF80",	16),
    ("e3d63",	12,	3,	NULL,			16),
    ("e3d64",	12,	2,	"000E:84AF:DF00",	16),
    ("e3d51", 	12,	4,	"0011:21DC:8B00",	15),
    ("e3d52",	12,	3,	"000E:84A2:0100",	15),
    ("e3d53",	12,	4,	"000E:84B0:2380",	15),
    ("e3d54",	17,	0,	NULL,			15),
    ("e3d31",	12,	4,	"000E:84AF:F880",	14),
    ("e3d32",	17,	0,	NULL,			14),
    ("e3d33",	12,	3,	"000E:84AF:E080",	14),
    ("e3d34",	17,	0,	NULL,			14),
    ("e3d21",	12,	4,	"000E:84A1:E080",	13),
    ("e3d22",	12,	3,	"000F:34CF:1300",	13)
;

