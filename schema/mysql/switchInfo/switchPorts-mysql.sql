CREATE OR REPLACE VIEW switchInfo.switchPorts AS
    SELECT switchInfo.switches.name as switch,
	switchInfo.ports.ifIndex as ifTemplate,
	switchInfo.ports.ifIndex as ifGroup,
	switchInfo.ports.ifIndex as ifIndex,
	switchInfo.ports.ifName as ifName,
	switchInfo.ports.ifAlias as ifAlias,
	switchInfo.ports.ifAdminStatus as ifAdminStatus,
	switchInfo.ports.ifOperStatus as ifOperStatus,
	switchInfo.ports.lastLinkUp as lastLinkUp,
	switchInfo.ports.lastLinkDown as lastLinkDown,
	switchInfo.ports.linkFlaps as linkFlaps,
	switchInfo.switches.online as switchOnline, 
	switchInfo.switches.statseeker as statseeker,
	switchInfo.switches.id as switchID,
	switchInfo.ports.id as portID
    FROM switchInfo.switches, switchInfo.ports
    WHERE ports.switchID = switches.id;

