\c switchInfo

CREATE TABLE "ifTemplates" (
    name	char(16) NOT NULL,
    description	varchar(64) NOT NULL,
    config	varchar(1024) NOT NULL,
    PRIMARY KEY	(name)
);

truncate "ifTemplates";

insert into "ifTemplates" values
    ( 'default', 'reset to factory default values, shutdown', 
'default interface INTERFACE
 interface INTERFACE
 shutdown
 end
' );

insert into "ifTemplates" values
    ( 'user', 'User Workstation including both data and voice',
'interface INTERFACE
 description DESCRIPTION
 switchport access vlan USER_VLAN
 switchport mode access
 switchport voice vlan 260
 switchport port-security maximum 3
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
' );

insert into "ifTemplates" values
    ( 'workstation', 'User workstation, data only, no voice service.',
'interface INTERFACE
 description DESCRIPTION
 switchport access vlan USER_VLAN
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
' );

insert into "ifTemplates" values
    ( 'ap', 'Wireless access point',
'interface INTERFACE
 description DESCRIPTION - ap
 switchport access vlan 2
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'kiosk', 'Internet kiosk, hardware restricted, data only.',
'interface INTERFACE
 description DESCRIPTION - kiosk
 switchport access vlan 289
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'protected', 'Protected vlan, restricted, data only.',
'interface INTERFACE
 description DESCRIPTION - prot
 switchport access vlan 280
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'private', 'Private, network, user defined restrictions.',
'interface INTERFACE
 description DESCRIPTION - priv
 switchport access vlan USER_VLAN
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'netlogin', 'Cisco based network login.',
'interface INTERFACE
 description DESCRIPTION - netlogin
 switchport access vlan 270
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 ip access-group netlogin in
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 ip admission netlogin
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'oa', 'Open area workstation, hardware address restricted.',
'interface INTERFACE
 description DESCRIPTION - open area
 switchport access vlan 274
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'phone', 'IP phone service, no data service.', 
'interface INTERFACE
 description DESCRIPTION - phone
 switchport access vlan 260
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'sensor', 'Wireless Air Magnet sensor',
'interface INTERFACE
 description DESCRIPTION - sensor
 switchport access vlan 2
 switchport mode access
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'sunray', 'Sunray terminal.',
'interface INTERFACE
 description DESCRIPTION - sunray
 switchport access vlan 256
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

insert into "ifTemplates" values
    ( 'unix', 'Open area UNIX workstation',
'interface INTERFACE
 description DESCRIPTION - Open Area Unix
 switchport access vlan 276
 switchport mode access
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
');

