CREATE TABLE switchInfo.ports (
    id			int(10) unsigned NOT NULL auto_increment,
    switchID		int(10) unsigned NOT NULL default '0',
    portVerified	enum('T','F') collate ascii_bin NOT NULL default 'F',
    ifIndex 		int(10) unsigned NOT NULL default '0',
    ifName 		varchar(16) collate ascii_bin NOT NULL,
    ifDescr 		varchar(32) collate ascii_bin default NULL,
    ifAlias 		varchar(64) collate ascii_bin default NULL,
    ifAdminStatus	enum('up','down','testing')
			collate ascii_bin default NULL,
    ifOperStatus	enum('up','down','testing','unknown','dormant',
			    'notpresent','lowerlayerdown')
			collate ascii_bin default NULL,
    lastLinkUp		datetime default NULL,
    lastLinkDown	datetime default NULL,
    linkFlaps		int(10) NOT NULL default '0',
    configID		int(10),

    PRIMARY KEY		(id),
    UNIQUE KEY ifIndex	(ifIndex,switchID),
    UNIQUE KEY ifName	(ifName,switchID),
    KEY lastLinkUp	(lastLinkUp),
    KEY lastLinkDown	(lastLinkDown),
    KEY switchID	(switchID)
)
    ENGINE=MyISAM 
    DEFAULT CHARSET=ascii
    COLLATE=ascii_bin
    COMMENT='Table of switch ports';
