CREATE TABLE ifStateChange (
      who		varchar(8) collate ascii_bin NOT NULL,
      switch		varchar(16) collate ascii_bin NOT NULL,
      ifName		varchar(16) collate ascii_bin NOT NULL,
      changeTime	timestamp NOT NULL,
      newState		enum('up','down','config') collate ascii_bin NOT NULL,
      note varchar(128) collate ascii_bin default NULL,
      PRIMARY KEY  (ifId,changeTime)
) ENGINE=MyISAM DEFAULT CHARSET=ascii COLLATE=ascii_bin | 

