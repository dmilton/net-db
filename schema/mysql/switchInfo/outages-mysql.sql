CREATE TABLE switchInfo.outages (
    id		int(10) NOT NULL auto_increment,
    bldgCode	char(2) collate ascii_bin NOT NULL,
    device	varchar(32) collate ascii_bin NOT NULL,
    startTime	datetime,
    endTime	datetime,
    PRIMARY KEY	(id),
    KEY bldgCode (bldgCode),
    KEY device (device),
    KEY duration (startTime,endTime)
)
    ENGINE=MyISAM
    DEFAULT CHARSET=ascii
    COLLATE=ascii_bin;

