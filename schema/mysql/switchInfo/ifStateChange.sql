CREATE TABLE "switchInfo.ifStates" (
    id		SERIAL, 
    state	varchar(16),
    PRIMARY KEY (id),
    UNIQUE	(state)
);

insert into "switchInfo.ifStates" values
    ( 1, 'up'), ( 2, 'down'), ( 3, 'config' );

CREATE TABLE "switchInfo.ifStateChange" (
      who		varchar(8) NOT NULL,
      switch		varchar(16) NOT NULL,
      ifName		varchar(16) NOT NULL,
      changeTime	timestamp NOT NULL,
      newState		integer references "ifStates"(id)
		       	ON UPDATE CASCADE ON DELETE RESTRICT,
      note 		varchar(128) default NULL,
      PRIMARY KEY  (switch,ifName,changeTime)
);

