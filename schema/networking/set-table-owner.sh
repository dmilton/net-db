#!/bin/sh -x

DB_NAME=networking
DB_OWNER=netmon

# Get list of tables.
for tbl in `psql -qAt -c "select tablename from pg_tables where schemaname = 'public';" $DB_NAME` ; do  psql -c "alter table $tbl owner to $DB_OWNER" $DB_NAME ; done

# Get list of sequences.
for tbl in `psql -qAt -c "select sequence_name from information_schema.sequences where sequence_schema = 'public';" $DB_NAME` ; do  psql -c "alter table $tbl owner to $DB_OWNER" $DB_NAME ; done

# Get list views..
for tbl in `psql -qAt -c "select table_name from information_schema.views where table_schema = 'public';" $DB_NAME` ; do  psql -c "alter table $tbl owner to $DB_OWNER" $DB_NAME ; done


