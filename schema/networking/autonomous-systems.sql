
-- Autonomous Systems

CREATE TABLE "autonomous_systems" (
	asn			INTEGER DEFAULT 66635 PRIMARY KEY,
	as_name		VARCHAR(32)
	org_id		varchar(16) NOT NULL UNIQUE
	org_name		varchar(64) NOT NULL UNIQUE
);

GRANT SELECT,REFERENCES on autonomous_systems to netview;

INSERT INTO autonomous_systems VALUES (0,'default campus');

