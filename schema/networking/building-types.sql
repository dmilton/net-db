-- Define a building type table so network types can be assigned
-- differently based on the building type.

-- Building type table.

CREATE TABLE building_types (
	-- building type id, index into network_types -> type_required[]
	bldg_type_id			SERIAL PRIMARY KEY,
	-- primary key, code used to identify a building uniquely.
	bldg_type				VARCHAR(8) NOT NULL UNIQUE,
	-- human readable description of the building type
	bldg_type_description	VARCHAR(64)
);

CREATE UNIQUE INDEX building_types_bldg_type ON building_types (bldg_type);

GRANT ALL ON building_types TO netmon;
GRANT SELECT,REFERENCES ON building_types TO netview;

INSERT INTO building_types 
	(bldg_type_id, bldg_type, bldg_type_description)
	VALUES (0, 'office', 'Buildings where workstations are installed.');
INSERT INTO building_types 
	(bldg_type, bldg_type_description)
	VALUES ('core',
			'Core router/buidings. No workstations or servers, only links.'),
		('dc', 'Data Centre router/buildings. Systems are typically servers');

INSERT INTO column_info VALUES 
	('bldg_type_id',
		'primary key, code used to identify a building type uniquely.'),
	('bldg_type',
		'short/abbreviated form of the building type.'),
	('bldg_type_description',
		'long description of this building type.');

