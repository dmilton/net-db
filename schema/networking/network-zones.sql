\c networking

-- Network zone descriptions - what is the zone for.

	-- net_zone - ID to identify this zone.
	-- zone_description - description of the zone.

-- Network Zones/Ranges
	-- This table contains one record for each IPv4/IPv6 prefix to
	-- indicate what the current allocation is for our IPv6 address spaces. 

	-- net_zone ZERO is the default and will be used for links and loopback
	-- addresses in the zone if no other network is specified.

	-- net_zone - zone id which addresses from this block are to be used.

	-- network_range - the ARIN allocation or block of addresses assigned
	-- to that zone. For IPv6 the code currently depends on this being a
	-- micro allocation (/48).

	-- next_loop - the next available loopback address. begin counting in
	-- the first quad of the host address. always a /128

	-- next_link - the next available link address. begin counting up in
	-- the second quad of the host address. always a /112

	-- next_bldg - index of the next availble building net id
	-- (building_detail:ipv6_netid) the index of the next available building
	-- subnet address.	start at 1, 0 is used for loopback and links.
	-- Bit counting is from the left to right and forms the subnet id portion
	-- of an IPv6 /48 network allocation.
	-- ie 1 = 1000 (1000 0000 0000 0000), 2 = 4000 (0100 0000 0000 0000)

	-- max_net - maximum number of networks assigned to a building.
	-- can be used to compute network address space exhaustion.

	-- net_type_list - list of types (network_types) which can be assigned
	-- from this block. If the list is empty, any type can be assigned from
	-- the block. If more than one block is assigned to a zone, the most
	-- specific match will be used. There should never be two blocks available
	-- in any zone for one network type.

	-- range_description - description of the address space - what is it for?

CREATE TABLE zone_descriptions (
	net_zone			SERIAL PRIMARY KEY,
	zone_descr			VARCHAR(64)
);

CREATE UNIQUE INDEX zone_descr_lower
    ON zone_descriptions(lower(zone_descr));

GRANT SELECT,REFERENCES ON zone_descriptions to netview;
GRANT ALL ON zone_descriptions to netmon;

GRANT USAGE, SELECT ON zone_descriptions_net_zone_seq to netview;
GRANT USAGE, SELECT, UPDATE ON zone_descriptions_net_zone_seq to netmon;

CREATE TABLE zone_ranges (
    net_zone			INTEGER DEFAULT -1
						REFERENCES zone_descriptions (net_zone)
						ON UPDATE CASCADE ON DELETE SET DEFAULT,
    network_range		CIDR NOT NULL PRIMARY KEY,
    next_loop			INTEGER DEFAULT 1,
    next_link			INTEGER DEFAULT 2,
    next_bldg			INTEGER DEFAULT 1,
    max_net         	INTEGER DEFAULT 1,
	net_type_list		VARCHAR(16) ARRAY,
	freed_bldg_list		INTEGER ARRAY,
	range_descr			VARCHAR(64) DEFAULT ''
);

GRANT SELECT,REFERENCES ON zone_ranges to netview;
GRANT ALL ON zone_ranges to netmon;

-- These two zones are special since a SERIAL starts at 1.
INSERT INTO zone_descriptions (net_zone,zone_descr)
    VALUES (-1, 'invalid zone');
INSERT INTO zone_descriptions (net_zone,zone_descr)
    VALUES (0, 'default zone when building zone is empty.');

INSERT INTO column_info VALUES
    -- zone_descriptions
    ('net_zone', 'ID number for this zone'),
	('zone_descr', 'description of the zone.'),
	-- zone_ranges
	('range_descr', 'description of the address range.'),
	('network_range', 'The ARIN allocation or block of addresses assigned to that zone. For IPv6 the code currently depends on this being a micro allocation (/48).'),
	('next_loop', 'The next available loopback address in the range. Begin counting in the first quad of the host address. always a /128'),
	('next_link', 'the next available link address. begin counting up in the second quad of the host address. always a /112'),
	('next_bldg', 'index of the next availble building net id (building_detail:ipv6_netid). The index of the next available building subnet address. Start counting at 1. Bit counting is from the left to right and forms the subnet id portion of an IPv6 /48 network allocation. ie 1 = 1000 (1000 0000 0000 0000), 2 = 4000 (0100 0000 0000 0000)'),
	('max_net', 'Maximum number of networks assigned to a building. Can be used to compute network address space exhaustion.'),
	('net_type_list', 'List of network types which can be assigned from this network range. If the list is empty, any type can be assigned from the block. If more than one block is assigned to a zone, the most specific match will be used. There should never be two blocks available in any zone for any network type.'),
	('range_description', 'Description of the address space - what is it for?');

