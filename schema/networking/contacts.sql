--
-- vim: ts=4 sw=4 noet syntax=sql :
-- This table provides a way to store some default values as it
-- pertains to the entrie system and also to specific user.
--
CREATE TABLE contacts (
	contact_id			SERIAL PRIMARY KEY,
	-- organization
	organization_name	VARCHAR(64),
	-- persons name.
	first_name			VARCHAR(32),
	last_name			VARCHAR(32),
	-- persons user id.
	userid				VARCHAR(32),
	-- Default to allocation of IPv4 networks.
	email_address		VARCHAR(128),
	phone_nbr			VARCHAR(32)
);

GRANT SELECT,REFERENCES ON contacts to netview;
GRANT ALL ON contacts to netmon;

-- default user is not restricted.
INSERT INTO contacts VALUES
    ('*',4,'smtp.cc.umanitoba.ca','netmon@cc.umanitoba.ca',
		        '{"milton@cc.umanitoba.ca"}', 0, -1, '{"F", "F", "F"}' );

-- a user that can do everything.
INSERT INTO contacts (user_name,campus_id,net_zone) VALUES
    ('netmon',0,0);

