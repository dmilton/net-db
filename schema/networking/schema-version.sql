-- Table to hold information about the schema. 

CREATE TABLE "schema_version" (
	-- major version number - change when adding new tables.
	major_version 		INTEGER NOT NULL,
	-- minor version number - change for add/change to existing tables.
	minor_version	INTEGER NOT NULL,
	-- date the change was made for production databases. date +%Y%j
	reference_date	integer NOT NULL,
	-- date the database was created.
	create_time		TIMESTAMP WITH TIME ZONE DEFAULT now(),
	PRIMARY KEY (major_version, minor_version)
);

GRANT SELECT,REFERENCES on schema_version to netview;
GRANT ALL ON schema_version to netmon;

-- put something into the table. This will get set correctly when the
-- remainder of the tables are initialized.
INSERT INTO schema_version (major_version,minor_version,reference_date)
	VALUES ( 1, 0, 2015026 );

