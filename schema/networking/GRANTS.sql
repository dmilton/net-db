--
-- Networking Database:
-- Grants for all tables.
-- no assumptions about current permissions are being made.
-- revoke everything and then grant only rights as appropriate.

-- Start by revoking everything from everybody.
revoke all on campus_locations from netmon;
revoke all on building_detail from netmon;
revoke all on building_models from netmon;
revoke all on voice_data_rooms from netmon;
revoke all on buildings from netmon;
revoke all on network_allocations from netmon;
revoke all on network_blocks from netmon;
revoke all on network_detail from netmon;
revoke all on network_log from netmon;
revoke all on network_types from netmon;
revoke all on network_vlans from netmon;
revoke all on network_zap from netmon;
revoke all on networks from netmon;
revoke all on outage_detail from netmon;
revoke all on outage_types from netmon;
revoke all on outages from netmon;
revoke all on sensor_detail from netmon;
revoke all on sensor_locations from netmon;
revoke all on sensors from netmon;
revoke all on switch_community from netmon;
revoke all on switch_detail from netmon;
revoke all on switch_if_templates from netmon;
revoke all on switches from netmon;
revoke all on wireless_installs from netmon;
revoke all on wireless_locations from netmon;
--
revoke all on campus_locations from netview;
revoke all on building_detail from netview;
revoke all on building_models from netview;
revoke all on voice_data_rooms from netview;
revoke all on buildings from netview;
revoke all on network_allocations from netview;
revoke all on network_blocks from netview;
revoke all on network_detail from netview;
revoke all on network_log from netview;
revoke all on network_types from netview;
revoke all on network_vlans from netview;
revoke all on network_zap from netview;
revoke all on networks from netview;
revoke all on outage_detail from netview;
revoke all on outage_types from netview;
revoke all on outages from netview;
revoke all on sensor_detail from netview;
revoke all on sensor_locations from netview;
revoke all on sensors from netview;
revoke all on switch_community from netview;
revoke all on switch_detail from netview;
revoke all on switch_if_templates from netview;
revoke all on switches from netview;
revoke all on wireless_installs from netview;
revoke all on wireless_locations from netview;
--
<<<<<<< HEAD
=======
revoke all on campus_locations from netlog;
revoke all on building_detail from netlog;
revoke all on building_models from netlog;
revoke all on voice_data_rooms from netlog;
revoke all on buildings from netlog;
revoke all on netlogin from netlog;
revoke all on network_allocations from netlog;
revoke all on network_blocks from netlog;
revoke all on network_detail from netlog;
revoke all on network_log from netlog;
revoke all on network_types from netlog;
revoke all on network_vlans from netlog;
revoke all on network_zap from netlog;
revoke all on networks from netlog;
revoke all on outage_detail from netlog;
revoke all on outage_types from netlog;
revoke all on outages from netlog;
revoke all on sensor_detail from netlog;
revoke all on sensor_locations from netlog;
revoke all on sensors from netlog;
revoke all on switch_community from netlog;
revoke all on switch_detail from netlog;
revoke all on switch_if_templates from netlog;
revoke all on switches from netlog;
revoke all on wireless_installs from netlog;
revoke all on wireless_locations from netlog;
--
>>>>>>> add-building-type
-- Now grant rights as appropriate
--
-- netmon is the updater.
grant select,insert,update,delete on campus_locations to netmon;
grant select,insert,update,delete on building_detail to netmon;
grant select,insert,update,delete on building_models to netmon;
grant select,insert,update,delete on voice_data_rooms to netmon;
grant select,insert,update,delete,references  on buildings to netmon;
grant select,insert,update,delete on netlogin to netmon;
grant select,insert,update,delete on router_afl to netmon;
grant select,insert,update,delete on network_allocations to netmon;
grant select,insert,update,delete on network_detail to netmon;
grant select,insert,update,delete on network_log to netmon;
grant select,insert,update,delete on network_types to netmon;
grant select,insert,update,delete on network_vlans to netmon;
grant select,insert,update,delete on network_zap to netmon;
grant select,insert,update,delete,references  on networks to netmon;
grant select,insert,update,delete on switch_if_templates to netmon;
grant select,insert,update,delete on wireless_installs to netmon;
grant select,insert,update,delete,references  on wireless_locations to netmon;
-- netview can only look.
grant select on voice_data_rooms to netview;
grant select,references on buildings to netview;
grant select on netlogin to netview;
grant select on router_afl to netview;
grant select on network_log to netview;
grant select on network_types to netview;
grant select,references on networks to netview;
grant select on outage_types to netview;
grant select,references on outages to netview;
grant select,references on sensors to netview;
grant all on switch_community to netview;
grant all on switch_detail to netview;
grant select on switch_if_templates to netview;
grant all on switches to netview;
grant all on wireless_installs to netview;
grant all on wireless_locations to netview;
