--
-- Rooms where equipment is installed.
--

CREATE TABLE voice_data_rooms (
	-- ID to identify this specific room.
	room_id		SERIAL PRIMARY KEY,
	-- what building code is the room in?
	bldg_code	VARCHAR(8) DEFAULT '00'
				REFERENCES building_detail (bldg_code)
				ON UPDATE CASCADE ON DELETE CASCADE,
	-- the actual room number
	room		CHAR(8),
	-- is this the MDC (Main Data Closet) in the building.
	mdr			BOOLEAN DEFAULT ('F')
);

CREATE UNIQUE INDEX voice_data_rooms_room ON voice_data_rooms (bldg_code, room);
CREATE INDEX voice_data_rooms_mdr ON voice_data_rooms ( mdr );

GRANT SELECT,REFERENCES ON voice_data_rooms TO netview;
GRANT ALL ON voice_data_rooms TO netmon;

GRANT USAGE,SELECT ON voice_data_rooms_room_id_seq TO netview;
GRANT USAGE,SELECT,UPDATE ON voice_data_rooms_room_id_seq TO netmon;

INSERT INTO column_info VALUES 
    ('room_id',
		'ID number to identify this specific room'),
	-- ('bldg_code',
	-- 	'what building is this room located in?'),
	('room',
		'the room number itself.'),
	('mdr',
		'is this the Main Data Closet for the building.');

