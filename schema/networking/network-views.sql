-- Network views:
--	Provides a simple way to view the data in the networking database.
--	Primary reasoning is to make the information available from a
--	web page without having to duplicate the entire networking utility
--	code base in a web page.
-- There are two views:
--	one view showing all assigned networks.
--	the second view includes free networks as well.

-- Provide a view of all assigned networks.

CREATE OR REPLACE VIEW assigned_networks AS 
	SELECT net_name, vlan_tag, network::VARCHAR,
	   	bldg_code, net_type, assign_date, assign_by,
		prime_net, NULL AS ep_code, NULL AS remote_code
	FROM network_bldgs
  UNION
	SELECT net_name, vlan_tag, network::VARCHAR,
	   	bldg_code, net_type, assign_date, assign_by,
		NULL AS prime_net, ep_code, NULL AS remote_code
	FROM network_links
  UNION
	SELECT net_name, vlan_tag, network::VARCHAR,
	   	bldg_code, net_type, assign_date, assign_by,
		NULL AS prime_net, NULL AS ep_code, remote_code
	FROM network_extern
  UNION
	SELECT net_name, 1 AS vlan_tag, network::VARCHAR,
	   	bldg_code, 'iso' AS net_type, assign_date, assign_by,
		NULL AS prime_net, NULL AS ep_code, NULL AS remote_code
	FROM network_iso
  ORDER BY bldg_code, vlan_tag, network;

CREATE OR REPLACE VIEW ip_networks AS 
	SELECT net_name, vlan_tag, network,
	   	bldg_code, net_type, assign_date, assign_by,
		prime_net, NULL AS ep_code, NULL AS remote_code,
		NULL AS net_zone, NULL AS free_date
	FROM network_bldgs
  UNION
	SELECT net_name, vlan_tag, network,
	   	bldg_code, net_type, assign_date, assign_by,
		NULL AS prime_net, ep_code, NULL AS remote_code,
		NULL AS net_zone, NULL AS free_date
	FROM network_links
  UNION
	SELECT net_name, vlan_tag, network,
	   	bldg_code, net_type, assign_date, assign_by,
		NULL AS prime_net, NULL AS ep_code, remote_code,
		NULL AS net_zone, NULL AS free_date
	FROM network_extern
  UNION
	SELECT net_name, vlan_tag, network,
	   	bldg_code, net_type, assign_date, assign_by,
		NULL AS prime_net, NULL AS ep_code, NULL AS remote_code,
		net_zone::VARCHAR, free_date::VARCHAR
	FROM network_free
  ORDER BY network;

GRANT ALL ON assigned_networks TO netmon;
GRANT SELECT,REFERENCES ON assigned_networks TO netview;

GRANT ALL ON ip_networks TO netmon;
GRANT SELECT,REFERENCES ON ip_networks TO netview;

