--
-- Network access enabled via
--    /opt/local/var/db/postgresql83/defaultdb/postgresql.conf
--	
-- Host access granted via:
--    /opt/local/var/db/postgresql83/defaultdb/pg_hba.conf

-- Owner of the entire networking database.
-- User netmon:
CREATE ROLE netmon LOGIN, INHERIT;
ALTER ROLE netmon WITH ENCRYPTED PASSWORD 'xxxxxxxx';
GRANT CREATE, DROP, ALTER, INHERIT;

-- Admin Role for the buildings tables.
CREATE ROLE buildings NOLOGIN NOINHERIT;
ALTER ROLE buildings WITH NOLOGIN;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON campus_locations TO buildings WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON building_detail TO buildings WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON building_models TO buildings WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON voice_data_rooms TO buildings WITH GRANT OPTION;
-- Viewer just gets the buildings view...
GRANT SELECT, REFERENCES 
    ON buildings TO public;

-- Role for network administrators.
CREATE ROLE netadmin NOLOGIN INHERIT;
\c networking;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON network_types TO netadmin;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON network_vlans TO netadmin;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON network_detail TO netadmin;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON network_allocations TO netadmin;
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, REFERENCES 
    ON networks TO netadmin;

-- this one is for the scripts which update the database.
CREATE ROLE network LOGIN NOINHERIT;
ALTER ROLE network WITH ENCRYPTED PASSWORD 'xxxxxxxx';
GRANT netadmin TO network;

GRANT ALL PRIVILEGES on network_allocations TO network;
GRANT ALL PRIVILEGES on network_masks TO network;
GRANT ALL PRIVILEGES on network_vlans TO network;
GRANT ALL PRIVILEGES on network_vlans_id_seq TO network;
GRANT ALL PRIVILEGES on network_detail TO network;
GRANT ALL PRIVILEGES on networks TO network;

\c intermapper;
-- GRANT SELECT ON

-- Viewer can select from key tables and views but not the raw data.
CREATE ROLE viewer NOLOGIN NOINHERIT;
ALTER ROLE viewer ENCRYPTED PASSWORD 'xxxxxxxx';
GRANT SELECT, REFERENCES ON buildings TO viewer;
GRANT SELECT, REFERENCES ON voice_data_rooms to viewer;
GRANT SELECT, REFERENCES ON switches TO viewer;
GRANT SELECT, REFERENCES ON switch_if_templates to viewer;
GRANT SELECT, REFERENCES ON networks TO viewer;

-- System administrator  - NOT SUPERUSER.
CREATE USER netmon WITH CREATEDB CREATEROLE CREATEUSER LOGIN;

-- Owner of the wireless database.
CREATE ROLE todd LOGIN INHERIT;
CREATE DATABASE wireless OWNER todd;

-- Typical user...
CREATE ROLE anybody LOGIN;
ALTER ROLE anybody ENCRYPTED PASSWORD 'xxxxxxxx';
-- allow updates to the networking tables...
--GRANT netadmin to anybody;

---
grant select,references on buildings to public;
grant select,references on networks to public;
grant select,references on sensors to public;
grant select,references on switches to public;
grant select,references on wireless_locations to public;


