--
-- vim: ts=4 sw=4 noet syntax=sql :
-- This table provides a way to store some default values as it
-- pertains to the entrie system and also to specific user.
--
CREATE TABLE user_info (
	-- the user name this row applies to. (use * for entire system)
	user_name			VARCHAR(32) PRIMARY KEY,
	-- Default to allocation of IPv4 networks.
	ip_version			INTEGER DEFAULT 4,
	-- SMTP server to use for sending email.
	smtp_server			VARCHAR(128),
	-- SMTP mail sender
	mail_sender			VARCHAR(64),
	-- SMTP mail receiver
	mail_receiver		VARCHAR(64) ARRAY[10],
	-- campus to use for user created buildings.
    campus_id       	INTEGER DEFAULT -1
	                    REFERENCES campus_locations (campus_id)
						ON UPDATE CASCADE ON DELETE SET DEFAULT, 
	-- zone to use for user created buildings. 
    net_zone        	INTEGER DEFAULT -1 CHECK(net_zone >= -1)
						REFERENCES zone_descriptions (net_zone)
						ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- what is this user allowed to do?
    -- 0 = restrict this user to viewing only?
    -- 1 = restrict this user to their default campus?
    -- 2 = restrict this user to their default zone?
	-- User authorizations start with the default and are then modified
	-- from the default by the settings in the user record.
    authorizations      BOOLEAN ARRAY[3]
);

GRANT SELECT,REFERENCES ON user_info to netview;
GRANT ALL ON user_info to netmon;

-- default user is not restricted.
INSERT INTO user_info VALUES
    ('*',4,'smtp.cc.umanitoba.ca','netmon@cc.umanitoba.ca',
		        '{"milton@cc.umanitoba.ca"}', 0, -1, '{"F", "F", "F"}' );

-- a user that can do everything.
INSERT INTO user_info (user_name,campus_id,net_zone) VALUES
    ('netmon',0,0);

