--
-- Create roles used to maintain the database.
--

--
-- Create the database.
--
CREATE DATABASE networking WITH OWNER netmon;
REVOKE CREATE ON SCHEMA public FROM PUBLIC;

-- This is only for development, not on production systems.
GRANT CREATE ON SCHEMA public TO netmon;

--
-- connect to networking database as netmon.
--
\c networking netmon

--
-- Create the tables.
--

-- table to track the version of the schema
\i schema-version.sql

-- table for information about tables
\i column-info.sql

-- table for tracking database changes
\i change-log.sql

-- fundamental tables referenced from buildings and networks and network_types
\i campus-locations.sql
\i building-types.sql
\i network-zones.sql

-- table for users of the system
\i user-info.sql

-- building information referenced from all networks.
\i building-detail.sql
\i voice-data-rooms.sql
-- \i buildings.sql

-- network type definitions referenced from all networks.
\i network-types.sql

-- network information
\i network-endpoints.sql
\i network-assigns.sql
\i network-views.sql

-- router/switch information
\i router-afl.sql
-- \i switch_if_templates.sql
-- \i switches.sql

-- wireless network locatons.
-- \i sensor-detail.sql
-- \i sensor-locations.sql
-- \i sensors.sql
-- \i wireless-installs.sql
-- \i wireless-locations.sql


