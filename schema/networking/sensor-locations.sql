-- Sensor Locations:
--	id - automatic value used for indexing.
--	name - host name of the sensor
--	location - text description of this location.
--	bldg_code - building code
--	sensor - serial number of the sensor installed at this location.
--	notes - notes specific to this sensor location.
--

CREATE TABLE sensor_locations (
    sensor_name		VARCHAR(16) PRIMARY KEY,
    location		VARCHAR(256) DEFAULT NULL,
    bldg_code   	VARCHAR(8) DEFAULT '00' NOT NULL
                	REFERENCES building_detail (bldg_code)
					ON UPDATE CASCADE ON DELETE CASCADE,
    sensor			VARCHAR(32) NOT NULL
               		REFERENCES sensor_detail (serial_nbr)
                	ON DELETE RESTRICT,
    notes			VARCHAR(1024) DEFAULT NULL
);

-- Create additional indexes.
CREATE UNIQUE INDEX sensor_loc_serial ON sensor_locations (sensor);

-- Grant table rights to netview and netmon.
GRANT ALL ON sensor_locations TO netmon;
GRANT SELECT,REFERENCES ON sensor_locations TO netview;

