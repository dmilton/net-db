--if exists wireless_installs

--ALTER TABLE wireless_installs 
--	ALTER COLUMN location SET DEFAULT 'general coverage';

CREATE TABLE wireless_installs (
	install_id		SERIAL,
	bldg_code		VARCHAR(8) NOT NULL 
					REFERENCES building_detail (bldg_code)
					ON UPDATE CASCADE ON DELETE CASCADE,
	location		VARCHAR( 32 ) NOT NULL DEFAULT 'general coverage',
	active_date		DATE DEFAULT '-infinity',
	status			VARCHAR(16) DEFAULT 'coming'
					CHECK (status IN 
						('coming', 'delayed', 'canceled', 'test', 'active',
						'disabled', 'compromised')),
	radio 			VARCHAR(16) DEFAULT '802.11g'
					CHECK ( radio IN
						( '802.11a', '802.11b', '802.11g', '802.11n',
						'802.11g+n' )),
	PRIMARY KEY	(id)
);

CREATE UNIQUE INDEX wi_locn ON wireless_installs ( bldg_code , location );
CREATE INDEX wi_bldg_date ON wireless_installs 
	(bldg_code, active_date, location);
CREATE INDEX wi_radio ON wireless_installs ( radio );

GRANT SELECT,REFERENCES ON wireless_installs to netview;
GRANT ALL ON wireless_installs TO netmon;
GRANT ALL ON wireless_installs_id_seq TO netmon;

SELECT setval('wireless_installs_id_seq', 
	(SELECT count(*) from wireless_installs));

