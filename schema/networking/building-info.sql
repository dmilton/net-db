\c networking
--
-- This is one of the root views of the entire networking database.
-- It details the information about a building. Everything revolves
-- around what goes on in a building.
--

CREATE OR REPLACE VIEW building_info AS
    SELECT building_detail.bldg_code as bldg_code,
		building_detail.bldg_name as bldg_name,
		building_detail.bldg_router as bldg_router,
		building_detail.bldg_type as bldg_type,
		campus_locations.campus_location as campus_location,
		building_detail.net_zone as net_zone,
		building_detail.next_user_vlan as next_user_vlan,
		building_detail.next_prot_vlan as next_prot_vlan,
		building_detail.ipv6_netid as ipv6_netid,
		building_detail.ipv6_nextnet as ipv6_nextnet,
		building_detail.ospf_area as ospf_area,
		building_detail.vtp_domain as vtp_domain,
		building_detail.web_visible as web_visible,
		building_detail.web_dir as web_dir,
		building_detail.campus_id as campus_id
		campus_locations.campus_code as campus_code
    FROM building_detail,
		campus_locations
    WHERE 
		building_detail.campus_id = campus_locations.campus_id;

GRANT SELECT,REFERENCES ON building_info to netview;
GRANT ALL ON building_info to netmon;

