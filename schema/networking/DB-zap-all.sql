
drop table schema_version;
drop table column_info;

--drop view buildings;
drop view assigned_networks;
drop view ip_networks;

--drop table core_buildings;
--drop table office_buildings;
--drop table building_info;

drop table network_bldgs;
drop table network_extern;
drop table network_free;
drop table network_links;
drop table network_descriptions;
drop table network_assigns;

drop table network_iso;
drop table network_zap;

drop table network_endpoints;

drop table network_types;
drop table router_afl;

drop table voice_data_rooms;
drop table building_detail;

drop table user_info;
drop table change_log;;

drop table zone_ranges;
drop table zone_descriptions;

drop table campus_locations;
drop table building_types;

