--
-- Table of scheduled outages.
--

CREATE TABLE outage_types (
    outage_type_id		SERIAL,
    outage_descr		varchar(64) NOT NULL,
    PRIMARY KEY	("id")
); 

GRANT ALL ON outage_types TO netmon;

