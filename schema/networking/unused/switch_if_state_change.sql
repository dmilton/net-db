CREATE TYPE t_ifState AS
    ENUM ('down', 'up', 'config');

CREATE TABLE "switch_if_state_change" (
      who		varchar(8) NOT NULL,
      switch		varchar(16) NOT NULL,
      ifname		varchar(16) NOT NULL,
      logtime		timestamp NOT NULL DEFAULT current_timestamp,
      newstate		t_ifState,
      comment 		varchar(128) default NULL,
      PRIMARY KEY  (switch,ifname,logtime)
);

GRANT SELECT,REFERENCES ON switch_if_state_change to netview;

