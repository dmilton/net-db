--
-- Tables to store various system events.
--

CREATE TABLE "event_log_category" (
    id			SERIAL,
    short_name		varchar(8),
    event_descr		varchar(256),
    PRIMARY KEY		(id)
);

INSERT INTO event_log_category (short_name, event_descr) VALUES
    (1, 'unknown', 'unclassified event'),
    (2, 'imweb', 'intermapper web access event'),
    (3, 'imremote', 'intermapper remote access event'),
    (4, 'syslog', 'syslog generated event'),
    (5, 'trap', 'snmp trap generated event'),
    (6, 'config', 'device configuration change'),
    (7, 'event', 'device event'),
    (8, 'error', 'switch port error'),
    (9, 'down', 'device down'),
    (10, 'up', 'device up');

GRANT SELECT,REFERENCES ON event_log_category to netview;
GRANT SELECT,REFERENCES ON event_log_category to netlog;

CREATE TABLE "event_log" (
    id			SERIAL,
    category		integer NOT NULL
			REFERENCES event_log_category (id)
			ON UPDATE CASCADE ON DELETE RESTRICT,
    who			varchar(8),
    logtime		timestamp NOT NULL DEFAULT current_timestamp,
    switch		varchar(16) DEFAULT NULL,
    ifname		varchar(16) DEFAULT NULL,
    message 		varchar(2048) DEFAULT NULL,
    PRIMARY KEY  (id)
);

CREATE INDEX event_cat ON event_log (category);
CREATE INDEX event_who ON event_log (who);
CREATE INDEX event_switch ON event_log (switch);

GRANT SELECT,REFERENCES ON event_log TO netview;
GRANT SELECT,REFERENCES,INSERT ON event_log TO netlog;

