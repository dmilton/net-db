
CREATE TABLE netlogin (
    id 			SERIAL PRIMARY KEY,
    loginTime		timestamp, 
    host 		char(32) default NULL,
    port		char(15) default NULL,
    userId		char(8) default NULL,
    macAddress		macaddr
);

GRANT SELECT,INSERT ON netlogin to netlog;
GRANT SELECT,REFERENCES ON netlogin to netview;

CREATE UNIQUE INDEX netlogin_session ON netlogin ( loginTime, host, userId );
CREATE INDEX netlogin_user ON netlogin ( userId, macAddress );

