-- Network BGP Peering table.

CREATE TABLE bgp_peer (
	-- description of the peering
	peer_descr		VARCHAR(64) DEFAULT '',
	-- host name of the router
	router			VARCHAR(64) NOT NULL,
	-- local AS
	local_as		INTEGER DEFAULT -1,
	-- address of the local peer (ie what does the remote peer to?)
	local_ip			INET,
	-- AS of the peer
	peer_as			INTEGER DEFAULT -1,
	-- address of the neighbour
	peer_ip			INET,
	-- maximum number of prefixes received from peer.
	prefix_limit	INTEGER DEFAULT -1,
	-- export_policy
	export_policy	INTEGER DEFAULT -1,
	-- import_policy
	import_policy	INTEGER DEFAULT -1,
	-- full config - simple block of text to comprise the entire configuration
	-- for the peer. Use the native form for the platform.
	peer_config		VARCHAR(10240) DEFAULT '',
	-- Key is compound so need to list it here.
	PRIMARY KEY	(router,local_as,peer_as)
);

-- Create indexes.
CREATE INDEX bgp_peer_router on bgp_peer (router);
CREATE INDEX bgp_peer_neighbour on bgp_peer (neighbour);

-- Grant table rights to netview and netmon.
GRANT SELECT,REFERENCES ON bgp_peer to netview;
GRANT ALL ON bgp_peer TO netmon;

-- table of BGP policies.
CREATE TABLE bgp_policy (
	-- id to track the policy itself.
	bcp_policy_id		SERIAL PRIMARY KEY,
	-- name of the policy.
	bgp_policy_name		VARCHAR(64) DEFAULT '',
	-- pointer to first clause in the policy.
	bgp_first_clause	INTEGER DEFAULT -1
						
);

-- Linked list of policy clauses.
CREATE TABLE bgp_policy_clause (
	bgp_clause_id		SERIAL PRIMARY KEY,
	-- what clause is next in the policy. (-1 = no next policy)
	policy_next			INTEGER DEFAULT -1,
	-- what clause is previous in the policy. (-1 = no previous policy)
	policy_prev			INTEGER DEFAULT -1,
	-- name to use for this clause
	policy_name			VARCHAR(32) DEFAULT '',
	-- detail of the clause itself
	policy_clause		VARCHAR(512) DEFAULT ''
);

CREATE UNIQUE INDEX bgp_policy_name_idx
	ON bgp_policy(policy_name, policy_clause);

-- Grant table rights to netview and netmon.
GRANT SELECT,REFERENCES ON bgp_policy TO netview;
GRANT ALL ON bgp_policy TO netmon;

