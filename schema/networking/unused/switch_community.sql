CREATE TABLE switch_community (
	id		SERIAL,
	class		varchar(16) NOT NULL,
	type		char(2) NOT NULL,
	string		char(32) not null,
	primary key	( id )
);

GRANT SELECT,REFERENCES ON switch_community to netview;

