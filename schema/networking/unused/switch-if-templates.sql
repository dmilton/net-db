--
CREATE TABLE switch_if_templates (
	name			VARCHAR(16) NOT NULL,
	class			VARCHAR(16) NOT NULL
					CHECK ( class IN ('router', 'switch')),
	config_descr	VARCHAR(64) NOT NULL,
	config			VARCHAR(1024),
	jconfig			VARCHAR(1024),
	jconfig2		VARCHAR(1024),
	PRIMARY KEY	(name)
);

INSERT INTO switch_if_templates (name, class,config_descr) VALUES
  ( 'default', 'switch', 'Reset to factory default values and shut down'),
  ( 'user', 'switch', 'User workstation including data and voice'),
  ( 'workstation', 'switch', 'User workstation, data only, no voice service'),
  ( 'classroom', 'switch', 'Podium computer, hardware restricted, data only'),
  ( 'oa', 'switch', 'Open area workstation, hardware restricted, data only'),
  ( 'unix', 'switch', 'Open area UNIX workstation, hardware restricted, data'),
  ( 'kiosk', 'switch', 'Internet kiosk, hardware restricted, data only'),
  ( 'protected', 'switch', 'Protected vlan, access restricted, data only'),
  ( 'phone', 'switch', 'IP phone service, no data service'),
  ( 'sunray', 'switch', 'Sunray terminal, no voice service'),
  ( 'private', 'switch', 'Private, user defined restrictions, no voice '),
  ( 'netlogin', 'switch', 'Network Login'),
  ( 'ap', 'switch', 'Wireless access point'),
  ( 'sensor', 'switch', 'Wireless Air Magnet sensor'),
  ( 'bridged', 'switch', 'private vlan that is not routed'),
  ( 'link2', 'switch', 'Layer 2 link for desktop switch uplink');

update switch_if_templates set config =
'default interface INTERFACE
 interface INTERFACE
 shutdown
 end
'
where name = 'default';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION
 switchport access vlan USER_VLAN
 switchport mode access
 switchport voice vlan 260
 switchport port-security maximum 3
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
' where name = 'user';


update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION
 switchport access vlan USER_VLAN
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'workstation';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - ap
 switchport access vlan 2
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
' where name = 'ap';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - kiosk
 switchport access vlan 289
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'kiosk';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - prot
 switchport access vlan 280
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'protected';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - priv
 switchport access vlan PRIV_VLAN
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'private';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - netlogin
 switchport access vlan 270
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security aging time 1
 switchport port-security violation restrict
 ip access-group netlogin in
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 ip admission netlogin
 no shutdown
 end
'
where name = 'netlogin';


update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - open area
 switchport access vlan 274
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'oa';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - phone
 switchport access vlan 260
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'phone';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - sensor
 switchport access vlan 2
 switchport mode access
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'sensor';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - sunray
 switchport access vlan 256
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 snmp trap mac-notification added
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'sunray';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - Open Area Unix
 switchport access vlan 276
 switchport mode access
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'unix';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION - classroom
 switchport access vlan 270
 switchport mode access
 switchport port-security maximum 1
 switchport port-security
 switchport port-security violation restrict
 switchport port-security mac-address sticky
 no mdix auto
 spanning-tree portfast
 spanning-tree guard root
 no shutdown
 end
'
where name = 'classroom';

update switch_if_templates set config =
'interface INTERFACE
 description DESCRIPTION
 switchport trunk encapsulation dot1q
 switchport mode trunk
 storm-control broadcast level bps 1m
 storm-control action trap
 channel-group CHANNEL mode on
 no shutdown
 end
'
where name = 'link2';
