
CREATE TABLE "log_entry" (
	who			varchar(8) NOT NULL,
	logtime		timestamp NOT NULL DEFAULT current_timestamp,
	logtype		varchar(16) DEFAULT 'netlog',
	switch		varchar(16) DEFAULT NULL,
	ifname		varchar(16) DEFAULT NULL,
	note 		varchar(4096) DEFAULT NULL,
	PRIMARY KEY	(who,logtime)
);

CREATE INDEX netlog_date ON log_entry (logtime);
CREATE INDEX netlog_dev ON log_entry (switch);

GRANT SELECT,REFERENCES ON log_entry to netview;
GRANT SELECT,REFERENCES,INSERT ON log_entry to netlog;
GRANT ALL ON log_entry TO netmon;

CREATE TABLE "log_types" (
	log_id				SERIAL PRIMARY KEY,
	log_type			VARCHAR(16) NOT NULL,
	log_descr			VARCHAR(64)
);

CREATE UNIQUE INDEX netlog_bytype ON netlog_types (logtype);

GRANT SELECT,REFERENCES ON log_types TO netview;
GRANT SELECT,REFERENCES,INSERT,UPDATE ON log_types TO netlog;
GRANT ALL ON log_types TO netmon;

