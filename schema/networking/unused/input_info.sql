-- Input Info:
--    table_name - name of table.
--    table_field - name of field in table.
--    field_desc - description of the field.

CREATE TABLE input_info (
    table_name		VARCHAR(64) NOT NULL,
    table_field		VARCHAR(64) NOT NULL,
    field_desc		VARCHAR(64) NOT NULL,
	input_form		VARCHAR(64)
    PRIMARY KEY	(table_name, table_field)
); 

CREATE INDEX input_info_table ON input_info (table_name);

-- Grant table rights to netview and netmon.
GRANT SELECT,REFERENCES ON input_info to netview;
GRANT ALL ON input_info TO netmon;

