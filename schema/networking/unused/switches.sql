
CREATE OR REPLACE VIEW switches AS
    SELECT
	switch_detail.name AS name,
       	switch_detail.bldg_code AS bldg_code,
	switch_detail.class AS class,
	switch_detail.code AS code,
	switch_detail.deviceType AS deviceType,
	switch_detail.stackSize AS stackSize,
	switch_detail.ifNumber AS ifNumber,
	switch_detail.macAddress AS macAddress,
	switch_detail.desktopPorts AS desktopPorts,
	switch_detail.lastColdStart AS lastColdStart,
	switch_detail.online AS online,
	building_detail.visible AS visible,
	building_detail.vtp_domain AS vtp_domain,
	building_detail.ospf_area AS ospf_area,
	building_detail.name AS building_name,
	building_campus.location AS campus,
	building_detail.webdir AS webdir,
	building_detail.map_code AS map_code,
	building_models.description AS building_type,
	building_detail.campus AS campus_id,
	building_detail.type AS type_id,
	switch_detail.id AS switch_id
    FROM building_detail,
	building_campus,
	building_models,
	switch_detail
    WHERE 
	switch_detail.bldg_code = building_detail.bldg_code AND
	building_detail.type = building_models.type AND
	building_detail.campus = building_campus.id;

