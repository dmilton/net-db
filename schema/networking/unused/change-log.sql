-- users who can update the state table change inherit from this role.
-- Role netlog:
CREATE ROLE netlog LOGIN NOINHERIT;
GRANT SELECT, INSERT ON switch_if_state_change to netlog;
GRANT SELECT, INSERT ON network_log to netlog;

-- grant these permissions to the network admin role.
GRANT netlog TO netadmin;

--
-- Create the change log table. This tracks who changes what and when.
--

CREATE TABLE "change_log" (
	change_who			VARCHAR(64) NOT NULL,
	change_when			TIMESTAMP NOT NULL DEFAULT 'now()',
	change_what 		VARCHAR(4096) DEFAULT NULL,
	PRIMARY KEY	(change_who,change_when)
);

CREATE INDEX change_log_who ON change_log (change_who);
CREATE INDEX change_log_when ON change_log (change_when);

GRANT SELECT,REFERENCES,INSERT ON change_log to netview;
GRANT SELECT,REFERENCES,INSERT ON change_log to netlog;
GRANT ALL ON change_log TO netmon;

