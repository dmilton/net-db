--
-- Table of scheduled outages.
--

CREATE TABLE outage_detail (
    id		SERIAL,
    visible	boolean default true, 
    bldg_code	varchar(16) NOT NULL, 
    type_id	integer NOT NULL default 1
		references outage_types (id)
		ON UPDATE CASCADE ON DELETE RESTRICT, 
    start_date	date default NOW(), 
    start_time	time default '23:00', 
    duration	varchar(64) default NULL,
    details	varchar(2048),
    PRIMARY KEY	("id")
); 
CREATE INDEX outage_index ON outage_detail (bldg_code);
CREATE INDEX outage_time ON outage_detail (start_date, start_time);

-- CREATE ROLE netoutage;
GRANT SELECT,INSERT,UPDATE ON outage_detail TO netoutage;

