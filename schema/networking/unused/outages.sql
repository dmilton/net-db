-- Include these to create the view.
\i outage_types.sql
\i outage_detail.sql
--
-- View definition:
--

CREATE OR REPLACE VIEW outages AS
    SELECT outage_detail.bldg_code as bldg_code,
	outage_detail.visible as visible,
	outage_detail.type_id as type_id,
	outage_detail.start_date as start_date,
	outage_detail.start_time as start_time,
	outage_detail.duration as duration,
	outage_detail.details as details,
	outage_detail.id as outage_id,
	building_detail.name as location,
	outage_types.outage_descr as outage_type
    FROM outage_detail,
	outage_types,
	building_detail
    WHERE 
	outage_detail.type_id = outage_types.id AND
        outage_detail.bldg_code = building_detail.bldg_code;

GRANT SELECT,REFERENCES ON outages to netview;
GRANT ALL ON outages to netmon;

