-- 
-- This table serves numerous functions.
--
-- 1. in network_assigns table family type (network_types=name) 
--	identifies how the network is used and what to do
--	when this network no longer needs to be allocated
--	to the building.
--	primary key here is the name.
-- 2. the network allocation script
--	deterimines if this network type needs to be allocated (required)
--	identifies the vlan 'number' to use when assigning this network type
--	identifies how to allocate (class)
--	identifies the type (private, public) of the network to assign.
--	identifies what name extension to use for the vlan name.
--	primary key used here is the name.
-- 3. the free-networks script
--	using the network_detail 'type' field it provides a way to
--	determine what to do when returning a network to unused (free_action)
--	delete or add to the free networks table.
-- 4. the split-netoworks script
--	helps determine what vlan number to use when splitting a network
--	into multiple subnetworks.
--

CREATE TABLE network_types (
	-- name assigned to the network type. Try to keep as short as possible
	-- to keep the show-bldg-info inside of 80 columns. 4 is ideal.
	net_type				VARCHAR(16) NOT NULL PRIMARY KEY,
	-- what is the vlan tag this network type should be assigned.
	-- if < 0 then the network type should be unique in the building.
	base_vlan_tag			INTEGER DEFAULT 0,
	-- how should the network be assigned/computed. IPv4 networks
	-- can be reserved in the free table and allocated on a next available
	-- basis.
	-- manual - assign manually, there isn't a good way to automate
	-- 		the decision making process for these types of networks.
	-- next, nextn -  are sequentially assigned 1 or n of them. These
	-- 		are similar to the range method but the origin is different.
	-- calc - means it is computed based on another network (user typically)
	-- 		this applies to IPv4 networks and ISO networks.
	-- late_mask - take the appropriate zone address space for the type and
	-- 		compute the next available network so that the summary mask
	--		applied to a building will be determined at address pool
	--		exhaustion time rather than determined at the outset.
	-- range - For most IPv6 addresses this is the assignment method used.
	--		there are a couple of counters maintained in a zone range which
	--		determine what the next network will be. For loopback and link
	--		networks this is an index into a list of networks starting
	--		from a known origin. For IPv4 in zone 1 the origin is 
	--		130.179.40.1 for loopback addresses, 130.179.41.0 for link
	--		networks. For IPv6 networks, subnet 0 of the /48 allocation
	--		is used in blocks of /112. The first /112 is used for loopback
	--		addresses. The second and following are assigned for links.
	-- IPv6 assignment is automated and calculated from the appropriate
	-- network zone address space. The base IPv6 network assigned to a
	-- building is computed using the late_mask methodology. After that,
	-- all assignments are done in sequence within that building.
	assign_method			VARCHAR(16) NOT NULL DEFAULT 'manual',
	-- when assigning required networks, what order should they be assigned
	-- in. Note the 'user' network type must pre-exist.
	-- If one network type is computed (calc) base on another, the base
	-- type must have a lower (assign sooner) priority than the computed one.
	assign_priority			INTEGER DEFAULT 0,
	-- list of building types where this network is mandatory.
	bldg_type_list			VARCHAR(8) ARRAY[1] DEFAULT '{"office"}',
							--ELEMENT REFERENCES building_types (bldg_type),
	-- for networks that are computed, what is the CIDR mask value
	-- that should be used [0] = IPv4 = 26, [1] = IPV6 = 64.
	prefix_size				INTEGER ARRAY[2] DEFAULT '{24,64}',
	-- what extension should be added to the name to identify the type
	-- when assigned to a building.
	name_extension			VARCHAR(16) DEFAULT '',
	-- what family? ip, ipv4, ipv6, iso, br.
	-- this allows a network type to be generic (ip = ipv4 and/or ipv6),
	-- protocol specific (ipv4, ipv6, iso), or layer-2 (eth).
	net_family				VARCHAR(4) DEFAULT 'ip',
	-- description of how the network is intended to be used.
	type_descr				VARCHAR(1024),
	-- what network_xxx table should networks of this type be put into?
	-- an array containing one or more values. The first value is the
	-- assigned/allocated table. The second if it exists is the free/ready
	-- to be assigned table. If the second value does not exist then a network
	-- is deleted when it is freed.
	net_table_list			VARCHAR(32) ARRAY[2]
);

GRANT SELECT,REFERENCES ON network_types to netview;
GRANT ALL ON network_types to netmon;

-- 
-- These are the basic types without which not much of anything
-- can be done with this system.
--
INSERT INTO network_types 
	( net_type, net_family, base_vlan_tag, assign_method, bldg_type_list, 
		name_extension, assign_priority, prefix_size, net_table_list )
VALUES
	('user', 'ip', 40, 'manual', '{ "office", "dc" }',
		'', 1, '{26,64}', '{ "network_bldgs", "network_free" }'),
	('loop', 'ip', -1, 'next', '{ "office", "dc", "core" }',
		'b1', 5, '{32,128}', '{ "network_bldgs", "network_free" }'),
	('link', 'ip', 401, 'nextn', '{ "office", "dc", "core" }',
		'-link', 10, '{30,112}', '{ "network_links", "network_free" }'),
	('iso', 'iso', -1, 'calc', '{ "office", "dc", "core" }',
		'-iso', 15, NULL, '{ "network_iso" }'),
	('mgmt', 'ip', -2, 'calc', '{ "office", "dc" }',
		'-mgmt', 20, NULL, '{ "network_bldgs" }'),
	('voip', 'ipv4', -260, 'calc', '{ "office" }',
		'-voip', 25, NULL, '{ "network_bldgs" }'),
	('2nd', 'ipv4', 0, 'calc', '{ "office", "dc" }',
		'2nd', 30, NULL, '{ "network_bldgs" }'),
	('bridge', 'eth', 0, 'n/a', '{ }',
		'', 65, NULL, '{ "network_bldgs" }'),
	('extern', 'ip', 0, 'n/a', '{ }',
		'', 99, NULL, '{ "network_extern", "network_free" }')
	;

UPDATE network_types
	SET type_descr = 'End user workstations and most systems in a building.'
	WHERE net_type = 'user';
UPDATE network_types
	SET type_descr = 'Loopback interface address(s) for routers.'
	WHERE net_type = 'loop';
UPDATE network_types SET 
type_descr = 'Point to Point link address for connections between routers.'
	WHERE net_type = 'link';
UPDATE network_types 
SET type_descr = 'ISO address for routers.'
	WHERE net_type = 'iso';
UPDATE network_types 
SET type_descr = 'Management network. Used for network devices such as switches and access points.'
	WHERE net_type = 'mgmt';
UPDATE network_types 
	SET type_descr = 'VoIP (Voice over IP) network. Network based phones.'
	WHERE net_type = 'voip';
UPDATE network_types 
	SET type_descr = 'Secondary (or tertiary) network. Can be added to any network/VLAN assigned in a building. These networks have access to the campus but no Internet access. Typically used for various devices such as printers, card access controllers, etc.'
	WHERE net_type = '2nd';

UPDATE network_types 
	SET type_descr = 'A VLAN that does not have a network address space assigned. Used for various special purposes which vary depending upon the building. No access to any device except for those on the same VLAN within the building. In rare (avoid this) instances, used to connect two building networks together. This is how we test from outside the firewall from the QA network. It is also how Extended Education runs their mail and student registration system.'
	WHERE net_type = 'bridge';

UPDATE network_types 
	SET type_descr = 'Networks which connect to external (client managed) network equipment.'
	WHERE net_type = 'extern';

