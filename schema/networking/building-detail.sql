-- Building Detail table.

CREATE TABLE building_detail (
	-- primary key, code used to identify a building uniquely.
	bldg_code			VARCHAR(8) NOT NULL PRIMARY KEY, 
	-- human readable name of the building.
	bldg_name			VARCHAR(64) NOT NULL UNIQUE, 
	-- what is the street address of the building.
    bldg_address        VARCHAR(256),
	-- what campus is the building located on?
	campus_id			INTEGER DEFAULT 0
						REFERENCES campus_locations (campus_id)
						ON UPDATE CASCADE ON DELETE RESTRICT, 
	-- type of building; determines network types that are required.
	bldg_type			VARCHAR(8) DEFAULT('office')
						REFERENCES building_types(bldg_type)
						ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- what zone should networks be allocated from.
	-- cannot reference the network_zones table since net_zone is not unique.
	net_zone			INTEGER DEFAULT -1 CHECK(net_zone >= -1)
						REFERENCES zone_descriptions (net_zone)
						ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- what is the ID of the next available user vlan in the building.
	-- vlan tags start at 40 and count up.
	next_user_vlan		INTEGER DEFAULT 40,
	-- what is the ID of the next available protected (ie ACL) vlan
	-- in the building. vlan tags start at 280 and count up.
	next_prot_vlan		INTEGER DEFAULT 280,
	-- what is the ID of the IPv6 network assigned to the building.
	-- algorithm computes values counting from the left bitwise.
	ipv6_netid			INTEGER DEFAULT -1,
	-- what is the ID of the next IPv6 network to assign to the building.
	-- algorithm increments by 1.
	ipv6_nextnet		INTEGER DEFAULT -1,
	-- name of the router for the building.
	bldg_router			VARCHAR(32) DEFAULT '',
	-- if using OSPF, what area is the building in.
	ospf_area			INTEGER DEFAULT -1,
	-- if using Cisco, what is the VTP domain in the building.
	vtp_domain			VARCHAR(16) DEFAULT NULL, 
	-- for web pages, should this be hidden in building listings?
	web_visible			BOOLEAN DEFAULT TRUE, 
	-- what is the path (from WebRoot) to the building info page.
	web_dir			VARCHAR(128) DEFAULT NULL, 
	-- what is the map code in InterMapper for this building.
	map_code		VARCHAR(64) DEFAULT NULL
); 

CREATE UNIQUE INDEX building_detail_name_lower
		ON building_detail (lower(bldg_name));
CREATE INDEX building_detail_visible on building_detail (web_visible);

GRANT SELECT,REFERENCES ON building_detail to netview;
GRANT ALL ON building_detail to netmon;

INSERT INTO building_detail 
	(bldg_code, campus_id, bldg_name, net_zone, web_visible)
	VALUES ('00', 0, 'default building', -1, 'F');

INSERT INTO column_info VALUES 
	('bldg_code',
		'primary key, code used to identify a building uniquely.'),
	('bldg_name',
		'human readable name of the building'),
	('bldg_router',
		'the name of the router for this building.'),
	('next_user_vlan',
		'what vlan tag/ID should be used for the next user vlan.'),
	('next_prot_vlan',
		'what vlan tag/ID should be used for the next protected vlan.'),
	('ipv6_netid',
		'what is the ID of the IPv6 network block assigned to the building.'),
	('ipv6_nextnet',
		'what is the ID of the next IPv6 network to assign to the building.'),
	('ospf_area',
		'if using OSPF, what area is the building in.'),
	('vtp_domain',
		'if using Cisco, what VTP domain is in the building.'),
	('web_visible',
		'should this be hidden in building listings on the web?'),
	('web_dir',
		'what is the path (from WebRoot) to the building info page.'),
	('map_code',
		'what is the map code in InterMapper for this building.');

