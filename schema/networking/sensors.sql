-- Sensors View:
-- Incorporate information from sensor_detail, sensor_locations,
-- campus_locations, and building_detail.
-- 
-- Joins performed so all information from sensor_detail will
-- be seen. Information from sensor_locations will be added
-- when it exists.
-- To that, information from campus_locations and building_detail
-- will be added if appropriate matches exist in the sensor data.

CREATE OR REPLACE VIEW sensors AS 
	SELECT
		sensor_locations.sensor_name AS sensor_name,
		campus_locations.location AS campus,
		building_detail.bldg_name AS bldg_name,
		sensor_locations.location AS location,
		sensor_detail.serial_nbr AS serial_nbr,
		sensor_detail.model AS model,
		sensor_detail.lan_mac AS lan_mac,
		sensor_detail.wlan_mac AS wlan_mac,
		sensor_detail.install_date AS install_date,
		sensor_detail.csid AS csid,
		sensor_detail.po AS po,
		sensor_detail.serial_key AS serial_key,
		sensor_detail.sensor_online AS sensor_online,
		sensor_detail.maint_status AS maint_status,
		sensor_detail.replaced_by AS replaced_by,
		sensor_detail.removal_date AS removal_date,
		sensor_detail.removal_reason AS removal_reason,
		sensor_detail.rma_sensor AS rma_sensor,
		sensor_locations.notes AS notes,
		sensor_locations.bldg_code AS bldg_code,
		sensor_detail.campus_id AS campus_id
	FROM (
			(sensor_locations RIGHT JOIN sensor_detail
				ON sensor_locations.sensor = sensor_detail.serial_nbr
			) LEFT JOIN building_detail 
				ON sensor_locations.bldg_code = building_detail.bldg_code
		) LEFT JOIN campus_locations
			ON sensor_detail.campus_id = campus_locations.campus_id;

-- Grant table rights to netview and netmon.
GRANT SELECT,REFERENCES ON sensors to netview;
GRANT ALL ON sensors to netmon;

