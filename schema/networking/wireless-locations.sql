--

CREATE OR REPLACE VIEW wireless_locations AS 
	SELECT
		campus_locations.location AS campus,
		building_detail.bldg_name AS bldg_name,
		wireless_installs.location AS location,
		wireless_installs.active_date AS active_date,
		wireless_installs.status AS status,
		wireless_installs.radio AS radio,
		wireless_installs.bldg_code AS bldg_code,
		building_detail.campus_id AS campus_id
	FROM
		wireless_installs, building_detail, campus_locations
	WHERE 
		wireless_installs.bldg_code = building_detail.bldg_code AND
		building_detail.campus_id = campus_locations.campus_id;

GRANT SELECT,REFERENCES ON wireless_locations to netview;
GRANT ALL ON wireless_locations TO netmon;

