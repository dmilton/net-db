! Find networks in network_xxx which are in the network_free table.
select network from network_bldgs as a where exists 
	(select b.network from network_free as b where b.network = a.network);
select network from network_links as a where exists \
	(select b.network from network_free as b where b.network = a.network);
select network from network_extern  as a where exists \
	(select b.network from network_free as b where b.network = a.network);
! Find networks with the old vlan_tag scheme. Should be 40..49 and not
! based on the third octet of the IP address (which is not sensical with IPv6)
select bldg_code, max(vlan_tag) from network_assigns where vlan_tag >= 2 
	and vlan_tag <= 255 and bldg_code != '00' group by bldg_code;

! Find records in 
!    network_vlans
! that do not have a record in
!    network_allocations:

select id,name,number from network_vlans as a where not exists
	(select b.vlanid from network_allocations as b where b.vlanid = a.id);


! Find records in
!    network_detail
! that do not have a record in
!    network_allocations:

select network, typeid from network_detail as a where not exists
(select b.network from network_allocations as b where b.network = a.network);

! Find records in
!    campus_locations
! that do not have records in
!    building_detail:

select id,location from campus_locations as a where not exists
(select b.campus_id from building_detail as b where b.campus_id = a.id);

