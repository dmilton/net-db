-- This table provides column descriptions for all the tables in the
-- network database. Using this table, applications can provide descriptions
-- to the end user without having to have it hard coded.
--
CREATE TABLE column_info (
	-- name of the table column.
	column_name		VARCHAR(64) PRIMARY KEY,
	-- long/detailed description of the table column.
	column_descr	VARCHAR(1024)
);

CREATE UNIQUE INDEX column_name_lower
    ON column_info(lower(column_name));

GRANT SELECT,REFERENCES ON column_info to netview;
GRANT ALL ON column_info to netmon;

