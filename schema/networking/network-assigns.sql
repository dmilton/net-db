--
-- Table to track network assignments to a building.
--

-- This is the parent table, nothing should be inserted here directly.
CREATE TABLE network_assigns (
	-- name assigned to the network, must be unique
	net_name		VARCHAR (32) NOT NULL PRIMARY KEY,
	-- vlan tag assigned to the network in the building.
	vlan_tag		INTEGER DEFAULT '0'
					NOT NULL CHECK(vlan_tag >= 0),
	-- the network address space itself.
	-- cannot be unique because 'bridged' has NULL here.
	network			CIDR,
	-- which building is this network assigned to.
	bldg_code		VARCHAR(8) DEFAULT '00'
					REFERENCES building_detail (bldg_code)
					ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- what type of network is this (network_types table)
	net_type		VARCHAR(16) DEFAULT 'reserved'
					REFERENCES network_types (net_type)
					ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- when was the network assigned to the building.
	assign_date		TIMESTAMP DEFAULT now(),
	-- who assigned the network to the building.
	assign_by		VARCHAR(16) DEFAULT ''
);

CREATE INDEX network_assigns_bldg on network_assigns ( bldg_code, vlan_tag );
CREATE INDEX network_assigns_net on network_assigns ( network );

GRANT SELECT,REFERENCES ON network_assigns to netview;
GRANT ALL ON network_assigns to netmon;

-- Child table to hold networks assigned to a building.
-- All networks which are assigned to a single building are in this table.
CREATE TABLE network_bldgs (
	-- is this the 'prime' network in the building. The prime network is
	-- used to compute other networks like mgmt, voip, etc.
	prime_net 		BOOLEAN DEFAULT 'F',
	PRIMARY KEY		( net_name )
) INHERITS (network_assigns);

CREATE INDEX network_bldgs_bldg ON network_bldgs ( bldg_code, vlan_tag );
CREATE UNIQUE INDEX network_bldgs_net ON
	network_bldgs (bldg_code, vlan_tag, network);

--ALTER TABLE network_bldgs ALTER COLUMN bldg_code SET DEFAULT '00';
-- update a building, update the network.
-- delete a building, set to invalid building code.
ALTER TABLE network_bldgs ADD CONSTRAINT network_bldgs_bldg_code_fkey
    FOREIGN KEY (bldg_code) REFERENCES building_detail (bldg_code)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

--ALTER TABLE network_bldgs ALTER COLUMN net_type SET DEFAULT 'reserved';
-- update the type, update the network.
-- delete the type, make type reserved until dealt with.
ALTER TABLE network_bldgs ADD CONSTRAINT network_bldgs_net_type_fkey
    FOREIGN KEY (net_type) REFERENCES network_types (net_type)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

GRANT SELECT,REFERENCES ON network_bldgs to netview;
GRANT ALL ON network_bldgs to netmon;

-- Child table to hold networks that are links between buildings.
-- Any network that establishes a link between a building and a core
-- building shoud be in this table. The vast majority of these are
-- /30 for IPv4 or /112 for IPv6.
CREATE TABLE network_links (
	-- what is the core endpoint code for the link?
	ep_code			VARCHAR(8) DEFAULT '00'
					REFERENCES network_endpoints (ep_code)
					ON UPDATE CASCADE ON DELETE SET DEFAULT,
	PRIMARY KEY		( net_name )
) INHERITS (network_assigns);

CREATE UNIQUE INDEX network_link_net on network_links (network);
CREATE INDEX network_link_bldg on network_links (bldg_code);
CREATE INDEX network_link_ep on network_links (ep_code);

--ALTER TABLE network_links ALTER COLUMN bldg_code SET DEFAULT '00';
-- update a building, update the network.
-- delete a building, set to invalid building code.
ALTER TABLE network_links ADD CONSTRAINT network_links_bldg_code_fkey
    FOREIGN KEY (bldg_code) REFERENCES building_detail (bldg_code)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

--ALTER TABLE network_links ALTER COLUMN net_type SET DEFAULT 'reserved';
-- update the type, update the network.
-- delete the type, make type reserved until dealt with.
ALTER TABLE network_links ADD CONSTRAINT network_links_net_type_fkey
    FOREIGN KEY (net_type) REFERENCES network_types (net_type)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

GRANT SELECT,REFERENCES ON network_links to netview;
GRANT ALL ON network_links to netmon;

-- Child table to hold networks that are external/extern.
-- networks that are external (ie we are transit for these)
-- They appear in 'two' places. One is the building the network is injected
-- from. The remote_code represents the other organization or group
-- who is actually using the network.
CREATE TABLE network_extern (
	remote_code		VARCHAR(8) DEFAULT NULL
					REFERENCES building_detail (bldg_code)
					ON UPDATE CASCADE ON DELETE RESTRICT,
	PRIMARY KEY		( network )
) INHERITS (network_assigns);

CREATE INDEX network_extern_rmt on network_extern (remote_code);
CREATE INDEX network_extern_bldg on network_extern (bldg_code);

--ALTER TABLE network_extern ALTER COLUMN bldg_code SET DEFAULT '00';
-- update a building, update the network.
-- delete a building, set to invalid building code.
ALTER TABLE network_extern ADD CONSTRAINT network_extern_bldg_code_fkey
    FOREIGN KEY (bldg_code) REFERENCES building_detail (bldg_code)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

--ALTER TABLE network_extern ALTER COLUMN net_type SET DEFAULT 'reserved';
-- update the type, update the network.
-- delete the type, make type reserved until dealt with.
ALTER TABLE network_extern ADD CONSTRAINT network_extern_net_type_fkey
    FOREIGN KEY (net_type) REFERENCES network_types (net_type)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

GRANT SELECT,REFERENCES ON network_extern to netview;
GRANT ALL ON network_extern to netmon;

-- Child table to hold networks that are free for use.
CREATE TABLE network_free ( 
	-- what zone does this free network belong to?
	net_zone		INTEGER DEFAULT 0
					REFERENCES zone_descriptions (net_zone)
					ON UPDATE CASCADE ON DELETE CASCADE,
	-- when was the network made free?
	free_date		timestamp default now(),
	PRIMARY KEY		( network )
) INHERITS (network_assigns);

CREATE INDEX network_free_type on network_free (net_zone, net_type);

--ALTER TABLE network_free ALTER COLUMN bldg_code SET DEFAULT '00';
-- update a building, update the network.
-- delete a building, set to invalid building code.
ALTER TABLE network_free ADD CONSTRAINT network_free_bldg_code_fkey
    FOREIGN KEY (bldg_code) REFERENCES building_detail (bldg_code)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

--ALTER TABLE network_free ALTER COLUMN net_type SET DEFAULT 'reserved';
-- update the type, update the network.
-- delete the type, make type reserved until dealt with.
ALTER TABLE network_free ADD CONSTRAINT network_free_net_type_fkey
    FOREIGN KEY (net_type) REFERENCES network_types (net_type)
	ON UPDATE CASCADE ON DELETE SET DEFAULT;

GRANT SELECT,REFERENCES ON network_free to netview;
GRANT ALL ON network_free to netmon;

--
-- Table to track iso network assignments to a router.
--
CREATE TABLE network_iso (
	-- Like all other networks, name of the network. In this case it is
	-- typically the name of the router with -iso appended to it.
	net_name		VARCHAR (32) NOT NULL PRIMARY KEY,
	-- what ISO network - 49.0000.0000.0000.00. Computed based on the
	-- loopback address of the router - if IPv6 then use the last two
	-- bytes of the address. If IPv4 then use the whole address in HEX.
	network			VARCHAR(25),
	-- what building is this network assigned to?
	bldg_code		VARCHAR(8) DEFAULT '00'
					REFERENCES building_detail (bldg_code)
					ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- when was the network assigned to the building.
	assign_date		TIMESTAMP DEFAULT now(),
	-- who assigned the network to the building.
	assign_by		VARCHAR(16) DEFAULT ''
);

CREATE UNIQUE INDEX network_iso_bldg on network_iso ( bldg_code );
CREATE UNIQUE INDEX network_iso_net on network_iso ( network );

GRANT SELECT,REFERENCES ON network_iso to netview;
GRANT ALL ON network_iso to netmon;

-- This table is to capture any descriptions for a network.
-- Most networks don't have descriptions as their "net_name" and "net_type"
-- is enough to describe their purpose. This is for those networks which
-- are not typical or have a special purpose.

CREATE TABLE network_descriptions (
	-- name of network
	net_name		VARCHAR(32) PRIMARY KEY
					REFERENCES network_assigns (net_name)
					ON UPDATE CASCADE ON DELETE CASCADE,
	-- description of network
	net_descr		VARCHAR(256),
	contact_id		INTEGER DEFAULT -1
);

-- This table is a catch all for networks which are deleted.
-- No external references so it can act as an archive of what
-- was found in the database.
CREATE TABLE network_zap (
	zap_id			SERIAL PRIMARY KEY,
	net_name		VARCHAR (32),
	vlan_tag		INTEGER,
	network			VARCHAR(64),
	bldg_code		VARCHAR(8),
	net_type		VARCHAR(16),
	assign_date		TIMESTAMP,
	assign_by		VARCHAR(8),
	prime_net		BOOLEAN,
	ep_code			VARCHAR(8),
	remote_code		VARCHAR(8),
	net_zone		INTEGER,
	free_date		TIMESTAMP,
	zap_date		TIMESTAMP,
	zap_by			VARCHAR(8)
);

CREATE INDEX network_zap_name on network_zap ( net_name );
CREATE INDEX network_zap_net on network_zap ( network );

GRANT ALL ON network_zap to netmon;
GRANT ALL ON network_zap_zap_id_seq TO netmon;
GRANT SELECT,REFERENCES ON network_zap TO netview;

--INSERT INTO column_info VALUES 
--	('campus_locations', 'location',
--		'Name/Text Description of the campus location');

