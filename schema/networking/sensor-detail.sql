-- Sensor Detail:
--	removal_reason - notes or reason for failure, etc.
--	rma_sensor - sensor received to replace this one which was RMA'd
--	maint_status - states of the maintenance contract as:
--	add - needs to be added to the contract
--	on - is on the current maintenance contract.
--	remove - needs to be removed from the contract.
--	off - has been removed from the contract.

CREATE TABLE sensor_detail (
	-- serial number of this sensor.
	serial_nbr		VARCHAR(32) NOT NULL,
	-- index into sensor_types table.
	model			VARCHAR(16) DEFAULT 'A5020', 
	-- LAN MAC address of the sensor.
	lan_mac			MACADDR,
	-- Wireless LAN MAC address of the sensor.
	wlan_mac		MACADDR,
	-- date of entry into database
	install_date	DATE DEFAULT NOW(),
	-- CSID number of this sensor.
	csid			INT DEFAULT '0',
	-- purchase order number
	purchase_order_nbr		VARCHAR(16) DEFAULT '',
	-- Air Mgnet generated order #
	serial_key		VARCHAR(16) DEFAULT '',
	-- online in the network?
	sensor_online			BOOLEAN DEFAULT 'f',
	-- What is the status? add, on, remove, off.
	maint_status	VARCHAR(16) NOT NULL DEFAULT 'add',
	-- campus_id from campus_locations table.
	campus_id		INT NOT NULL DEFAULT -1
					REFERENCES campus_locations (campus_id)
					ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- what sensor was this sensor replaced by.
	replaced_by		VARCHAR(32) DEFAULT NULL,
	-- date device removed/failed
	removal_date	DATE DEFAULT '-infinity',
	removal_reason	VARCHAR(1024) DEFAULT NULL,
	rma_sensor		VARCHAR(32) DEFAULT NULL,
	PRIMARY KEY	(serial_nbr)
);

-- Create indexes for lan_mac, wlan_mac, csid, serial_key, and replaced_by.
CREATE UNIQUE INDEX sensor_lan_mac on sensor_detail (lan_mac);
CREATE UNIQUE INDEX sensor_wlan_mac on sensor_detail (wlan_mac);
CREATE INDEX sensor_csid on sensor_detail (csid);
CREATE INDEX sensor_serial_key on sensor_detail (serial_key);
CREATE INDEX sensor_replaced on sensor_detail (replaced_by);

-- Grant table rights to netview and netmon.
GRANT SELECT,REFERENCES ON sensor_detail to netview;
GRANT ALL ON sensor_detail TO netmon;

