
-- Campus locations.

CREATE TABLE "campus_locations" (
  campus_id				SERIAL PRIMARY KEY,
  campus_code			varchar(8),
  campus_location		varchar(32) NOT NULL
);

CREATE UNIQUE INDEX campus_locations_locn_lower
		ON campus_locations(lower(campus_location));

GRANT SELECT,REFERENCES on campus_locations to netview;

-- Add our default and invalid campus locations.
INSERT INTO campus_locations VALUES (-1, 'invalid', 'invalid campus');
INSERT INTO campus_locations VALUES (0, 'default','default campus');

INSERT INTO column_info VALUES 
	('campus_id',
		'ID number for the campus (0 = default)'),
	('campus_location',
		'Name/Text Description of the campus location');

