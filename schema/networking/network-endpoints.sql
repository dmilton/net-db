-- Table of valid link endpoint codes.
-- ep_code is a valid building code (must exist in building_detail) which
-- has been assigned to a router in the core of the network.
-- Priority is used to break ties when both ends of a link are valid ep_codes.
-- The ep_code with the highest priority will be assigned the bldg_code
-- side of the link.
CREATE TABLE network_endpoints (
	-- building code for the endpoint.
    ep_code				VARCHAR(8) NOT NULL PRIMARY KEY
						REFERENCES building_detail (bldg_code)
						ON UPDATE CASCADE ON DELETE CASCADE,
	-- This should match the zone in the respective building code.
	-- It is here to avoid two lookups and to allow getting the core
	-- routers for a specific zone; used in assigning links to buildings.
	net_zone			INTEGER DEFAULT -1
						REFERENCES zone_descriptions (net_zone)
						ON UPDATE CASCADE ON DELETE SET DEFAULT,
	-- when assigning redundant links into the core, the zone is taken
	-- into account. The priority determines the order links are assigned
	-- when there are multiple ep_codes defined for that zone. -1 = ignore.
	core_priority		INTEGER DEFAULT -1
); 

GRANT SELECT,REFERENCES ON network_endpoints to netview;
GRANT ALL ON network_endpoints to netmon;

