\c intermapper

grant select,references on acknowledgment to netview;
grant select,references on address to netview;
grant select,references on datapoint to netview;
grant select,references on datasample to netview;
grant select,references on datasampledaily to netview;
grant select,references on datasamplehourly to netview;
grant select,references on dataset to netview;
grant select,references on device to netview;
grant select,references on devicekind to netview;
grant select,references on event to netview;
grant select,references on ianaenterprise to netview;
grant select,references on ianaiftype to netview;
grant select,references on interface to netview;
grant select,references on map to netview;
grant select,references on notification to netview;
grant select,references on notifier to netview;
grant select,references on notifierrule to netview;
grant select,references on retentionpolicy to netview;
grant select,references on schemaversion to netview;
grant select,references on server to netview;
grant select,references on serverpoll to netview;

grant all on acknowledgment   to netmon;
grant all on address          to netmon;
grant all on datapoint        to netmon;
grant all on datasample       to netmon;
grant all on datasampledaily  to netmon;
grant all on datasamplehourly to netmon;
grant all on dataset          to netmon;
grant all on device           to netmon;
grant all on devicekind       to netmon;
grant all on event            to netmon;
grant all on ianaenterprise   to netmon;
grant all on ianaiftype       to netmon;
grant all on interface        to netmon;
grant all on map              to netmon;
grant all on notification     to netmon;
grant all on notifier         to netmon;
grant all on notifierrule     to netmon;
grant all on retentionpolicy  to netmon;
grant all on schemaversion    to netmon;
grant all on server           to netmon;
grant all on serverpoll       to netmon;

