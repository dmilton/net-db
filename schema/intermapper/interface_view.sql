CREATE OR REPLACE VIEW interface_view AS
    SELECT
	interface.server_id AS server_id,
	interface.map_id AS map_id,
	interface.device_id AS device_id,
	interface.interface_id AS interface_id,
	interface.ifindex AS ifindex,
	interface.ifdescr AS ifdescr,
	interface.ifname AS ifname,
	interface.ifalias AS ifalias,
	device.name AS device_name,
	device.ip AS device_ip,
	device.dnsname AS device_dnsname,

    FROM
	interface,
	device,
	map
    WHERE interface.server_id = 2
	AND interface.device_id = device.device_id
	AND interface.map_id = map.map_id;


