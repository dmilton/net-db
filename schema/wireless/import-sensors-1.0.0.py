#!/usr/bin/env python

gVersion="1.0.0 - 11 Dec 2008"

# This is a template python script. It handles version, debug, and help
# command line arguments.

# 11 Dec 2008		initial writing.

############################################################################
# Usage(exitCode)

def Usage(exitCode):
    """Produce a brief explanation of how to use this command.
    """
    global gCommand
    print """
usage: %s [--help] [--version] | [--debug=N] csv-data-file
    --help      display this help message
    --version   show version information and exit.
    --debug=N   debug at level N.
    """ % ( gCommand )
    sys.exit(exitCode)
## end usage()

############################################################################
# Process command line arguments.

import getopt, sys
from os.path import basename

# Get our global settings/configuration file.
sys.path.append("/usr/local/etc/")
from networkingConfig import netLib, dbEnabled

sys.path.append(netLib)

import debug
import wireless

gCommand = basename(sys.argv[0])

if ( dbEnabled == False ):
    print "The networking database is currently shut down."
    print "Please try again later."
    sys.exit(0)
## end if ( dbEnabled == False ):

def main():
    try:
	optList, argList = getopt.getopt(sys.argv[1:], "", 
		["help", "version", "debug=", ""])
    except getopt.GetoptError, err:
	# print help information and exit:
	print str(err)
	Usage(1)
    ## end try

    for option, argument in optList:
	if option in ("--version"):
	    print gVersion
	    sys.exit(0)
	elif option in ("--help"):
	    Usage(0)
	elif option in ("--debug"):
	    debug.setDebug(argument)
	else:
	    assert False, "unhandled option"
	## end if
    ## end for

    debugLevel = debug.getDebug()
    debug.debug(1, "debugLevel=%s" % (debugLevel) )

    # process the positional command line arguments.
    if ( len(argList) != 1 ):
	Usage(2)
    else:
        inputFile = argList[0]
    	debug.debug(1, "inputFile=" + inputFile)
    ## end if ( len(argList != 1 )

    #
    # Done processing command line arguments. Do the real work...
    #

    # begin by opening our file.
    f = open(inputFile, 'r')
    debug.debug(2, "f=" + str(f))

    aSensor = {}
    for inputLine in f:
	sensorInput = inputLine.split(',')
	aSensor['name'] = sensorInput[0]
	aSensor['bldgCode'] = sensorInput[0][0:2]
	aSensor['lanMac'] = sensorInput[2]
	aSensor['wlanMac'] = sensorInput[3]
	aSensor['serialNum'] = sensorInput[4]
	aSensor['serialKey'] = sensorInput[5]
	aSensor['csid'] = sensorInput[6]
	aSensor['po'] = sensorInput[7]
	aSensor['model'] = sensorInput[8]
	aSensor['status'] = sensorInput[12]
	aSensor['onMaint'] = sensorInput[14]
	debug.debug(2, "aSensor=" + str(aSensor))
	newSensor = wireless.AddSensor(aSensor)
	print str(newSensor)
    ## end for inputLine in f:

## end main()

# Call main to do the work.
if __name__ == "__main__":
    main()

