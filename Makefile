#
# If GNU make is run then this file is ignored completely and
# GNUmakefile is referenced. If not, this fill will say to use gmake
# and then exit.
#

.DEFAULT:
	@echo "Please use GNU make (gmake), make does not understand the makefile."

.FAILED:
	@echo "Please use GNU make (gmake), make does not understand the makefile."

%:
	@echo "Please use GNU make (gmake), make does not understand the makefile."
